<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: process_list.php
CREATED ON	: 05-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Process Plans
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET['source_type']))
	{
		$source_type = $_GET['source_type'];
	}
	else
	{
		$source_type = '';
	}
	
	if(isset($_GET['source_type_name']))
	{
		$source_type_name = $_GET['source_type_name'];
	}
	else
	{
		$source_type_name = '';
	}
	
	if(isset($_GET['start']))
	{
		$start_date = $_GET['start'];
	}
	else
	{
		$start_date = '';
	}
	
	if(isset($_GET['end']))
	{
		$end_date = $_GET['end'];
	}
	else
	{
		$end_date = '';
	}
	
	$date_status = get_date_diff($start_date,$end_date);
	$date_diff = abs($date_status['data']);
	
	// Get list of enquiry sources
	$enquiry_source_list = i_get_enquiry_source_list('','1','',$source_type);
	if($enquiry_source_list["status"] == SUCCESS)
	{
		$enquiry_source_list_data = $enquiry_source_list["data"];
	}
	else
	{
		$alert = $enquiry_source_list["data"];
		$alert_type = 0;
	}	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Marketing Report</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header" style="height:50px; padding-top:10px;"> <i class="icon-th-list"></i>
              <h3>Marketing Report&nbsp;&nbsp;&nbsp;<?php echo $source_type_name; ?>&nbsp;&nbsp;&nbsp;Total Leads between <?php echo date('d-M-Y',strtotime($start_date)); ?> and <?php echo date('d-M-Y',strtotime($end_date)); ?>: <span id="total_leads"><i>Calculating</i></span></h3>			  
            </div>
            <!-- /widget-header -->
			
            <div class="widget-content">
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th width="15%" style="word-wrap:break-word;">Source</th>
					<th width="15%" style="word-wrap:break-word;">Source Type</th>
						<?php
						$filt_start_date = $start_date;
						$day_count       = 0;
						while(strtotime($filt_start_date) < strtotime($end_date))
						{
							$filt_start_date = date('Y-m-d',strtotime($start_date.' +'.$day_count.' days'));
							?>
							<th><?php echo date('d',strtotime($start_date.' +'.$day_count.' days')); ?> - <?php echo date('M',strtotime($start_date.' +'.$day_count.' days')); ?> (<?php echo date('D',strtotime($start_date.' +'.$day_count.' days')); ?>)</th>
							<?php
							$day_count++;
						}
						?>
					<th>Total</th>
				</tr>
				</thead>
				<tbody>
				<?php					
				$sl_no = 0;				
				$total_num_leads   = 0;
				$total_num_sales   = 0;
				if($enquiry_source_list["status"] == SUCCESS)
				{														
					for($count = 0; $count < count($enquiry_source_list_data); $count++)
					{
						// No of leads generated from this source
						$enquiry_sresult = i_get_enquiry_list('','','','','','','','',$enquiry_source_list_data[$count]['enquiry_source_master_id'],'','','','',$start_date.' 00:00:00',$end_date.' 23:59:59','','','','','');
						if($enquiry_sresult['status'] == SUCCESS)
						{
							$number_of_leads = count($enquiry_sresult['data']);							
						}
						else
						{
							$number_of_leads = 0;
						}						

						$enquiry_source_list_data[$count]['pcount'] = $number_of_leads;
					}	

					array_sort_conditional($enquiry_source_list_data,'sort_on_number');
					
					for($count = 0; $count < count($enquiry_source_list_data); $count++)
					{
						$sl_no = $sl_no + 1;
						
						// Initialize						
						$number_of_leads = 0;											
																
						// No of leads generated from this source
						$net_leads = $enquiry_source_list_data[$count]['pcount'];
						$total_num_leads = $total_num_leads + $net_leads;	
		
						?>
						<tr>
						<td style="word-wrap:break-word;"><?php echo $enquiry_source_list_data[$count]['enquiry_source_master_name']; ?></td>
						<td style="word-wrap:break-word;"><?php echo $enquiry_source_list_data[$count]['enquiry_source_type_name']; ?></td>
						<?php						
						$filt_start_date = $start_date;
						$day_count       = 0;
						while(strtotime($filt_start_date) < strtotime($end_date))
						{			
							$filt_start_date = date('Y-m-d',strtotime($start_date.' +'.$day_count.' days'));
							$filt_end_date   = date('Y-m-d',strtotime($start_date.' +'.$day_count.' days'));
						
							// No of leads generated from this source
							$enquiry_sresult = i_get_enquiry_list('','','','','','','','',$enquiry_source_list_data[$count]['enquiry_source_master_id'],'','','','',$filt_start_date.' 00:00:00',$filt_end_date.' 23:59:59','','','','','');
							if($enquiry_sresult['status'] == SUCCESS)
							{
								$number_of_leads = count($enquiry_sresult['data']);							
							}
							else
							{
								$number_of_leads = 0;
							}
							?>					
							<td style="word-wrap:break-word; <?php if(strtotime($filt_start_date) == strtotime(date('Y-m-d').' -1 days')){ ?> color:red; <?php } ?>"><?php echo $number_of_leads; ?></td>
							<?php
							$day_count++;
						}
						?>
						<td style="word-wrap:break-word;"><?php echo $net_leads; ?></td>
						</tr>
						<?php
					}
				}
				else
				{
				?>
				<td colspan="4">No marketing source type added!</td>
				<?php
				}
				?>	

                </tbody>
              </table>	
			  <script>
			  document.getElementById('total_leads').innerHTML = <?php echo $total_num_leads; ?>;
			  </script>
            </div>
			
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    
<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

  </body>

</html>
