<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_enquiry_fup_list.php
CREATED ON	: 07-Sep-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Enquiries
*/
define('CRM_ENQUIRY_FOLLOW_UP_DUMP_FUNC_ID','103');
/*
TBD: 
*/$_SESSION['module'] = 'CRM Transactions';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];		// Get permission settings for this user for this page	$add_perms_list     = i_get_user_perms($user,'',CRM_ENQUIRY_FOLLOW_UP_DUMP_FUNC_ID,'1','1');	$view_perms_list    = i_get_user_perms($user,'',CRM_ENQUIRY_FOLLOW_UP_DUMP_FUNC_ID,'2','1');	$edit_perms_list    = i_get_user_perms($user,'',CRM_ENQUIRY_FOLLOW_UP_DUMP_FUNC_ID,'3','1');	$delete_perms_list  = i_get_user_perms($user,'',CRM_ENQUIRY_FOLLOW_UP_DUMP_FUNC_ID,'4','1');

	// Query String / Get form Data
	if(isset($_GET["enquiry"]))
	{
		$enq_id = $_GET["enquiry"];
	}
	else
	{
		$enq_id = "";
	}
	$day = "";
	if(isset($_POST["enquiry_fup_search_submit"]))
	{
		$fup_date_start = $_POST["start_date"];
		$fup_date_end   = $_POST["end_date"];
		$assigned_to    = $_POST["ddl_user"];
	}
	else
	{
		$fup_date_start = "";
		$fup_date_end   = "";
		$assigned_to    = "";
	}

	// Temp data
	$alert = "";

	if(($role == 1) || ($role == 5) || ($enq_id != ""))
	{
		if($assigned_to != "")
		{	
			$added_by = $assigned_to;
		}
		else		
		{
			if($enq_id != '')
			{
				$added_by = "";
			}
			else
			{
				$added_by = '-1';
			}
		}
	}
	else
	{
		$added_by = $user;
	}
	
	$enquiry_fup_list = i_get_enquiry_fup_list($enq_id,'',$added_by,'asc','','','','','assignee_only',$fup_date_start,$fup_date_end,$user);
	if($enquiry_fup_list["status"] == SUCCESS)
	{
		$enquiry_fup_list_data = $enquiry_fup_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$enquiry_fup_list["data"];
	}
	
	// User List
	$user_list = i_get_user_list('','','','','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Enquiry Follow Up List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Enquiry Follow Up List&nbsp;&nbsp;&nbsp;&nbsp;(Count: <?php if($enquiry_fup_list["status"] == SUCCESS){ echo count($enquiry_fup_list["data"]); }else{ echo "0";} ?>)&nbsp;&nbsp;&nbsp;&nbsp;<?php if($enq_id != ""){ ?><a href="crm_add_enquiry_fup.php?enquiry=<?php echo $enq_id; ?>">Add Follow Up</a><?php if(($role == 1) || ($role == 5)) { ?>&nbsp;&nbsp;&nbsp;&nbsp;<a href="crm_add_unqualified_enquiry.php?enquiry=<?php echo $enq_id; ?>">Mark as Unqualified</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="crm_assign_enquiry.php?enquiry=<?php echo $enq_id; ?>">Assign</a><?php } } ?></h3>
            </div>
			
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="post" id="enquiry_fup_search" action="crm_enquiry_fup_list.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="start_date" value="<?php echo $fup_date_start; ?>" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="end_date" value="<?php echo $fup_date_end; ?>" />
			  </span>
			  <select name="ddl_user">
			  <option value="">- - Select STM - -</option>
			  <?php
				for($count = 0; $count < count($user_list_data); $count++)
				{
					if(($user_list_data[$count]["user_role"] == "1") || ($user_list_data[$count]["user_role"] == "5") || ($user_list_data[$count]["user_role"] == "7"))
					{
					?>
					<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php 
					if($assigned_to == $user_list_data[$count]["user_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>								
					<?php
					}
				}
      		  ?>
			  </select>
			  <input type="submit" name="enquiry_fup_search_submit" />
			  </form>			  
            </div>
			
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th>SL No</th>					
					<th>Enquiry No</th>					
					<th>Project</th>					
					<th>Name</th>
					<th>Mobile</th>
					<th>Walk In</th>
					<th>Latest Status</th>
					<th>Next FollowUp</th>
					<th>Assigned To</th>
					<th>Assigned By</th>
					<th>Remarks</th>					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($enquiry_fup_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($enquiry_fup_list_data); $count++)
					{
						$sl_no++;
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $enquiry_fup_list_data[$count]["enquiry_number"]; ?></td>					
					<td><?php echo $enquiry_fup_list_data[$count]["project_name"]; ?></td>					
					<td><?php echo $enquiry_fup_list_data[$count]["name"]; ?></td>
					<td><?php echo $enquiry_fup_list_data[$count]["cell"]; ?></td>
					<td><?php if($enquiry_fup_list_data[$count]["walk_in"] == "1")
					{
						echo "Yes";
					}
					else
					{
						echo "No";
					}?></td>
					<td><?php echo $enquiry_fup_list_data[$count]["crm_cust_interest_status_name"]; ?></td>
					<td><?php echo date("d-M-Y H:i",strtotime($enquiry_fup_list_data[$count]["enquiry_follow_up_date"])); ?></td>
					<td><?php $assigned_to_details = i_get_user_list($enquiry_fup_list_data[$count]["assigned_to"],'','','');					
						echo $assigned_to_details["data"][0]["user_name"];?></td>
					<td><?php echo $enquiry_fup_list_data[$count]["user_name"]; ?></td>
					<td><?php echo $enquiry_fup_list_data[$count]["enquiry_follow_up_remarks"]; ?></td>					
					</tr>
					<?php
					}
				}
				else
				{
				?>
				<td colspan="11">No enquiry follow up!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>