<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: stock_transfer_list.php

CREATED ON	: 09-Dec-2016

CREATED BY	: Lakshmi

PURPOSE     : List of stock for customer withdrawals

*/



/*

TBD: 

*/



/* DEFINES - START */

define('STOCK_TRANSFER_FUNC_ID','355');

/* DEFINES - END */



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');



	$alert_type = -1;

	$alert = "";

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];

	

	// Get permission settings for this user for this page

	$view_perms_list   = i_get_user_perms($user,'',STOCK_TRANSFER_FUNC_ID,'2','1');

	$edit_perms_list   = i_get_user_perms($user,'',STOCK_TRANSFER_FUNC_ID,'3','1');

	$delete_perms_list = i_get_user_perms($user,'',STOCK_TRANSFER_FUNC_ID,'4','1');

	$add_perms_list    = i_get_user_perms($user,'',STOCK_TRANSFER_FUNC_ID,'1','1');



	// Query String Data

	// Nothing

	 // Get Stock Transfer modes already added

	$project_tds_deduction_master_search_data = array();

	$project_tds_list = i_get_project_tds_deduction_master($project_tds_deduction_master_search_data);
	if($project_tds_list['status'] == SUCCESS)

	{

		$project_tds_list_data = $project_tds_list['data'];

	}	

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>Approved Stock Transfer List</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">

   





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    



<div class="main">

  <div class="main-inner">

    <div class="container">

      <div class="row">

       

          <div class="span6" style="width:100%;">

          

          <div class="widget widget-table action-table">

            <div class="widget-header"> <i class="icon-th-list"></i>

              <h3>TDS Deduction Master List</h3>

            </div>

            <!-- /widget-header -->

            <div class="widget-content">

			

              <table class="table table-bordered" style="table-layout: fixed;">

                <thead>

                  <tr>

				    <th width="6%">SL No</th>

					<th style="word-wrap:break-word;">Deduction Type</th>

					<th width="10%">Deduction Percentage</th>

					<th style="word-wrap:break-word;">Remarks</th>

					<th style="word-wrap:break-word;">Added By</th>					

					<th style="word-wrap:break-word;">Added On</th>									

					<th style="word-wrap:break-word">Actions</th>

    					

				</tr>

				</thead>

				<tbody>							

				<?php

				if($project_tds_list["status"] == SUCCESS)

				{

					$sl_no = 0;

					for($count = 0; $count < count($project_tds_list_data); $count++)

					{

						$sl_no++;

		

						

					?>

					<tr>

					<td width="6%"><?php echo $sl_no; ?></td>

					<td style="word-wrap:break-word;"><?php echo $project_tds_list_data[$count]["project_tds_deduction_master_type"]; ?></td>

					<td width="10%"><?php echo $project_tds_list_data[$count]["project_tds_deduction_master_deduction"]; ?></td>
					
					<td style="word-wrap:break-word;"><?php echo $project_tds_list_data[$count]["project_tds_deduction_master_remarks"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo $project_tds_list_data[$count]["user_name"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_tds_list_data[$count][

					"project_tds_deduction_master_added_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php if($edit_perms_list['status'] == SUCCESS){ ?><a style="padding-right:10px" href="#" onclick="return go_to_project_edit_tds_master('<?php echo $project_process_master_list_data[$count]["project_tds_deduction_master_id"]; ?>');">Edit </a><?php } ?></td>
					

					</tr>

					<?php

					}

					

				}

				else

				{

				?>

				<td colspan="9">No TDS Deduction Master condition added yet!</td>

				

				<?php

				}

				 ?>	



                </tbody>

              </table>

            </div>

            <!-- /widget-content --> 

          </div>

          <!-- /widget --> 

         

          </div>

          <!-- /widget -->

        </div>

        <!-- /span6 --> 

      </div>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /main-inner --> 

</div>

    

    

    

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->





    

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    





<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>

<script>

function stock_accept_transfer(transfer_id,s_project,d_project,material_id,qty,count)
{
	var ok = confirm("Are you sure you want to Accept");

	{         

		accepted_qty = document.getElementById('accepted_qty_' + count).value;
		remarks = document.getElementById('remarks_' + count).value;

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{
					alert(xmlhttp.responseText);
					if(xmlhttp.responseText != "SUCCESS")

					{

						 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

						 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

					 window.location = "stock_approved_transfer_list.php";

					}

				}

			}



			xmlhttp.open("POST", "ajax/stock_accept_transfer.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("transfer_id=" + transfer_id + "&action=Accepted" + "&s_project=" + s_project + "&d_project=" + d_project + "&material_id=" + material_id + "&qty=" + qty + "&accepted_qty=" + accepted_qty + "&remarks=" + remarks);

		}

	}	

}

function go_to_project_edit_tds_master(tds_masetr_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "project_edit_tds_master.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","tds_masetr_id");
	hiddenField1.setAttribute("value",tds_masetr_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}
</script>



  </body>



</html>