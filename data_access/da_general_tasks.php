<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
LIST OF TBDs
1. In db_get_task_type_list, name search should be wild card search
*/

/*
PURPOSE : To get task type list
INPUT 	: Task Type Name, Active Status, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of processes
BY 		: Nitin Kashyap
*/
function db_get_gen_task_type_list($task_type_name,$active,$added_by,$start_date,$end_date)
{
	$get_task_type_list_squery_base = "select * from general_task_type_master";
	
	$get_task_type_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_task_type_list_sdata = array();
	
	if($task_type_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." where general_task_type_name=:task_type_name";								
		}
		else
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." and general_task_type_name=:task_type_name";				
		}
		
		// Data
		$get_task_type_list_sdata[':task_type_name']  = $task_type_name;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." where general_task_type_active=:active";								
		}
		else
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." and general_task_type_active=:active";				
		}
		
		// Data
		$get_task_type_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." where general_task_type_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." and general_task_type_added_by=:added_by";				
		}
		
		// Data
		$get_task_type_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." where general_task_type_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." and general_task_type_added_on >= :start_date";				
		}
		
		//Data
		$get_task_type_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." where general_task_type_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_task_type_list_squery_where = $get_task_type_list_squery_where." and general_task_type_added_on <= :end_date";				
		}
		
		//Data
		$get_task_type_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_task_type_list_squery = $get_task_type_list_squery_base.$get_task_type_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_task_type_list_sstatement = $dbConnection->prepare($get_task_type_list_squery);
		
		$get_task_type_list_sstatement -> execute($get_task_type_list_sdata);
		
		$get_task_type_list_sdetails = $get_task_type_list_sstatement -> fetchAll();
		
		if(FALSE === $get_task_type_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_task_type_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_task_type_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To get department list
INPUT 	: Department, Active Status, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of departments
BY 		: Nitin Kashyap
*/
function db_get_gen_task_department_list($department_name,$active,$added_by,$start_date,$end_date)
{
	$get_dep_list_squery_base = "select * from general_task_department_master";
	
	$get_dep_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_dep_list_sdata = array();
	
	if($department_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_dep_list_squery_where = $get_dep_list_squery_where." where general_task_department_name=:department_name";								
		}
		else
		{
			// Query
			$get_dep_list_squery_where = $get_dep_list_squery_where." and general_task_department_name=:department_name";				
		}
		
		// Data
		$get_dep_list_sdata[':department_name']  = $department_name;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_dep_list_squery_where = $get_dep_list_squery_where." where general_task_department_active=:active";								
		}
		else
		{
			// Query
			$get_dep_list_squery_where = $get_dep_list_squery_where." and general_task_department_active=:active";				
		}
		
		// Data
		$get_dep_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_dep_list_squery_where = $get_dep_list_squery_where." where general_task_department_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_dep_list_squery_where = $get_dep_list_squery_where." and general_task_department_added_by=:added_by";				
		}
		
		// Data
		$get_dep_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_dep_list_squery_where = $get_dep_list_squery_where." where general_task_department_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_dep_list_squery_where = $get_dep_list_squery_where." and general_task_department_added_on >= :start_date";				
		}
		
		//Data
		$get_dep_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_dep_list_squery_where = $get_dep_list_squery_where." where general_task_department_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_dep_list_squery_where = $get_dep_list_squery_where." and general_task_department_added_on <= :end_date";				
		}
		
		//Data
		$get_dep_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_dep_list_squery_order = ' order by general_task_department_name asc';
	$get_dep_list_squery = $get_dep_list_squery_base.$get_dep_list_squery_where.$get_dep_list_squery_order;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_dep_list_sstatement = $dbConnection->prepare($get_dep_list_squery);
		
		$get_dep_list_sstatement -> execute($get_dep_list_sdata);
		
		$get_dep_list_sdetails = $get_dep_list_sstatement -> fetchAll();
		
		if(FALSE === $get_dep_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_dep_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_dep_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To get task list
INPUT 	: Task ID, Task Type, User, Department, Planned End Date, Actual Start Date, Actual End Date, Added By, Start Date(for added on), End Date(for added on), Assigner or Assignee (basically to check if user is involved in a task), Those statuses to be excluded
OUTPUT 	: List of tasks
BY 		: Nitin Kashyap
*/
function db_get_gen_task_plan_list($task_id,$task_type,$task_user,$task_department,$planned_end_date,$actual_start_date,$actual_end_date,$added_by,$start_date,$end_date,$status,$assigneer_or_assignee='',$status_exclude='',$project='')
{
	$get_gen_task_plan_legal_list_squery_base = "select *,U.user_name as assignee,AU.user_name as assigner from general_tasks GT";
	
	$get_gen_task_plan_legal_list_squery_join = " inner join general_task_type_master GTTM on GTTM.general_task_type_id = GT.general_task_type inner join users U on U.user_id = GT.general_task_user inner join general_task_department_master DM on DM.general_task_department_id = GT.general_task_department inner join users AU on AU.user_id = GT.general_task_added_by";
	
	$get_gen_task_plan_legal_list_squery_where = "";
	
	$get_gen_task_plan_legal_list_squery_order = "";

	// Data
	$get_gen_task_plan_legal_list_sdata = array();	
	
	$get_gen_task_plan_legal_list_squery_where = " where general_task_delete_status=:delete_status";
	$get_gen_task_plan_legal_list_sdata[':delete_status']  = '0';
	
	$filter_count = 1;
	
	if($task_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." where general_task_id=:id";								
		}
		else
		{
			// Query
			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." and general_task_id=:id";				
		}
		
		// Data
		$get_gen_task_plan_legal_list_sdata[':id']  = $task_id;
		
		$filter_count++;
	}
	
	if($task_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." where general_task_type=:type";								
		}
		else
		{
			// Query
			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." and general_task_type=:type";				
		}
		
		// Data
		$get_gen_task_plan_legal_list_sdata[':type']  = $task_type;
		
		$filter_count++;
	}
	
	if($task_user != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." where general_task_user=:task_user";
		}
		else
		{
			// Query
			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." and general_task_user=:task_user";				
		}
		
		// Data
		$get_gen_task_plan_legal_list_sdata[':task_user']  = $task_user;
		
		$filter_count++;
	}
	
	if($assigneer_or_assignee != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." where (general_task_user=:assigner_assignee or general_task_added_by=:assigner_assignee)";
		}
		else
		{
			// Query
			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." and (general_task_user=:assigner_assignee or general_task_added_by=:assigner_assignee)";				
		}
		
		// Data
		$get_gen_task_plan_legal_list_sdata[':assigner_assignee']  = $assigneer_or_assignee;
		
		$filter_count++;
	}
	
	if($task_department != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." where general_task_department=:task_department";								
		}
		else
		{
			// Query
			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." and general_task_department=:task_department";				
		}
		
		// Data
		$get_gen_task_plan_legal_list_sdata[':task_department']  = $task_department;
		
		$filter_count++;
	}
	
	if($planned_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." where general_task_planned_date=:planned_end";								
		}
		else
		{
			// Query
			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." and general_task_planned_date=:planned_end";				
		}
		
		// Data
		$get_gen_task_plan_legal_list_sdata[':planned_end']  = $planned_end_date;
		
		$filter_count++;
	}
	
	if($actual_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." where general_task_start_date=:actual_start";								
		}
		else
		{
			// Query
			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." and general_task_start_date=:actual_start";				
		}
		
		// Data
		$get_gen_task_plan_legal_list_sdata[':actual_start']  = $actual_start_date;
		
		$filter_count++;
	}
	
	if($actual_end_date != "")
	{
		if($actual_end_date == "9999-99-99")
		{
			if($filter_count == 0)
			{
				// Query
				$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." where general_task_end_date != :actual_end";								
			}
			else
			{
				// Query
				$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." and general_task_end_date != :actual_end";				
			}
			$actual_end_date = "0000-00-00";
		}
		else
		{
			if($filter_count == 0)
			{
				// Query
				$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." where general_task_end_date=:actual_end";								
			}
			else
			{
				// Query
				$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." and general_task_end_date=:actual_end";				
			}
		}
		
		// Data
		$get_gen_task_plan_legal_list_sdata[':actual_end']  = $actual_end_date;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." where general_task_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." and general_task_added_by=:added_by";				
		}
		
		// Data
		$get_gen_task_plan_legal_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." where general_task_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." and general_task_added_on >= :start_date";				
		}
		
		//Data
		$get_gen_task_plan_legal_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." where general_task_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." and general_task_added_on <= :end_date";				
		}
		
		//Data
		$get_gen_task_plan_legal_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." where general_task_completion_status = :status";								
		}
		else
		{
			// Query
			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." and general_task_completion_status = :status";				
		}
		
		//Data
		$get_gen_task_plan_legal_list_sdata[':status']  = $status;
		
		$filter_count++;
	}		if($status_exclude != "")	{		if($filter_count == 0)		{			// Query			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." where general_task_completion_status != :status_exclude";										}		else		{			// Query			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." and general_task_completion_status != :status_exclude";						}				//Data		$get_gen_task_plan_legal_list_sdata[':status_exclude']  = $status_exclude;				$filter_count++;	}		if($project != "")	{		if($filter_count == 0)		{			// Query			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." where general_task_project = :project";										}		else		{			// Query			$get_gen_task_plan_legal_list_squery_where = $get_gen_task_plan_legal_list_squery_where." and general_task_project = :project";						}				//Data		$get_gen_task_plan_legal_list_sdata[':project']  = $project;				$filter_count++;	}
	
	$get_gen_task_plan_legal_list_squery_order = " order by general_task_id asc";
	
	$get_gen_task_plan_legal_list_squery = $get_gen_task_plan_legal_list_squery_base.$get_gen_task_plan_legal_list_squery_join.$get_gen_task_plan_legal_list_squery_where.$get_gen_task_plan_legal_list_squery_order;
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_gen_task_plan_legal_list_sstatement = $dbConnection->prepare($get_gen_task_plan_legal_list_squery);
		
		$get_gen_task_plan_legal_list_sstatement -> execute($get_gen_task_plan_legal_list_sdata);
		
		$get_gen_task_plan_legal_list_sdetails = $get_gen_task_plan_legal_list_sstatement -> fetchAll();
		
		if(FALSE === $get_gen_task_plan_legal_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_gen_task_plan_legal_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_gen_task_plan_legal_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add new legal task plan
INPUT 	: Task Type, Process Plan ID, Planned End Date, Start Date, Actual End Date, Document Path, Added By
OUTPUT 	: Task Plan id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_gen_task_plan($type,$user,$details,$department,$project,$planned_end_date,$added_by)
{
	// Query
    $gen_task_plan_iquery = "insert into general_tasks (general_task_type,general_task_user,general_task_details,general_task_department,general_task_project,general_task_planned_date,general_task_start_date,general_task_end_date,general_task_completion_status,general_task_delete_status,general_task_added_by,general_task_added_on) values (:type,:assignee,:details,:department,:project,:planned_end,:actual_start,:actual_end,:status,:delete_status,:added_by,:now)";
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $gen_task_plan_istatement = $dbConnection->prepare($gen_task_plan_iquery);
        
        // Data		
        $gen_task_plan_idata = array(':type'=>$type,':assignee'=>$user,':details'=>$details,':department'=>$department,':project'=>$project,':planned_end'=>$planned_end_date,':actual_start'=>'',':actual_end'=>'',':status'=>'0',':delete_status'=>'0',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $gen_task_plan_istatement->execute($gen_task_plan_idata);
		$gen_task_plan_id = $dbConnection->lastInsertId();
		$dbConnection->commit();
		
		$return["status"] = SUCCESS;
		$return["data"]   = $gen_task_plan_id;		
	}
    catch(PDOException $e)
    {
		// Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
	}
    return $return;
}

/*
PURPOSE: To update task plan
INPUT  : Task Plan ID, Planned End Date, Start Date, Actual End Date, Document, Applicable, Approved On
OUTPUT : SUCCESS if task plan updated; DB_NO_RECORD if plan ID was invalid; FAILURE otherwise
BY     : Nitin Kashyap
*/
function db_update_gen_task_plan($task_plan_id,$planned_end,$start_date,$end_date,$status)
{
	// Query
    $gen_task_plan_uquery = "update general_tasks set general_task_planned_date=:planned_end,general_task_start_date=:start_date,general_task_end_date=:end_date, general_task_completion_status=:status where general_task_id=:task_plan_id";   	

    try
    {
        $dbConnection = get_conn_handle();
        
        $gen_task_plan_ustatement = $dbConnection->prepare($gen_task_plan_uquery);
        
        // Data
        $gen_task_plan_udata = array(':planned_end'=>$planned_end,':start_date'=>$start_date,':end_date'=>$end_date,':status'=>$status,':task_plan_id'=>$task_plan_id);				
        $gen_task_plan_ustatement -> execute($gen_task_plan_udata);
		
		$updated_rows = $gen_task_plan_ustatement->rowCount();
		
		if($updated_rows >= 1)
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
		else
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add task delay reason
INPUT 	: Task, Remarks, Remarks File, Added By, Is it a sub task
OUTPUT 	: Delay Reason id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_gen_task_remarks($task_id,$remarks,$remarks_file,$added_by,$subtask)
{
	// Query
    $remarks_iquery = "insert into general_task_remarks (general_task_remarks_task_id,general_task_remarks,general_task_remarks_subtask,general_task_remarks_status,general_task_remarks_file,general_task_remarks_added_by,general_task_remarks_added_on) values (:task,:remarks,:subtask,:status,:file,:added_by,:now)";
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $remarks_istatement = $dbConnection->prepare($remarks_iquery);
        
        // Data		
        $remarks_idata = array(':task'=>$task_id,':remarks'=>$remarks,':subtask'=>$subtask,':status'=>'0',':file'=>$remarks_file,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));				
		$dbConnection->beginTransaction();
        $remarks_istatement->execute($remarks_idata);
		$remarks_id = $dbConnection->lastInsertId();
		$dbConnection->commit();
		
		$return["status"] = SUCCESS;
		$return["data"]   = $remarks_id;		
	}
    catch(PDOException $e)
    {
		// Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
	}
    return $return;
}

/*
PURPOSE: To update assignee of a task
INPUT  : Task Plan ID, Assigned To
OUTPUT : SUCCESS if task plan updated; DB_NO_RECORD if plan ID was invalid; FAILURE otherwise
BY     : Nitin Kashyap
*/
function db_gen_update_assignee($task_plan_id,$assigned_to)
{
	// Query
    $task_plan_uquery = "update general_tasks set general_task_user=:assigned_to where general_task_id=:task_plan_id";   

    try
    {
        $dbConnection = get_conn_handle();
        
        $task_plan_ustatement = $dbConnection->prepare($task_plan_uquery);
        
        // Data
        $task_plan_udata = array(':assigned_to'=>$assigned_to,':task_plan_id'=>$task_plan_id);
        
        $task_plan_ustatement -> execute($task_plan_udata);
		
		$updated_rows = $task_plan_ustatement->rowCount();
		
		if($updated_rows >= 1)
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
		else
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new general task type
INPUT 	: Task type, Added By
OUTPUT 	: Task Type id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_task_type_general($task_type,$added_by)
{
	// Query
    $gen_task_type_iquery = "insert into general_task_type_master (general_task_type_name,general_task_type_active,general_task_type_added_by,general_task_type_added_on) values (:task_type,:active,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $gen_task_type_istatement = $dbConnection->prepare($gen_task_type_iquery);
        
        // Data
        $gen_task_type_idata = array(':task_type'=>$task_type,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $gen_task_type_istatement->execute($gen_task_type_idata);
		$gen_task_type_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $gen_task_type_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To add new department
INPUT 	: Department Name, Added By
OUTPUT 	: Department id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_department($department_name,$added_by)
{
	// Query
    $department_iquery = "insert into general_task_department_master (general_task_department_name,general_task_department_active,general_task_department_added_by,general_task_department_added_on) values (:department,:active,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $department_istatement = $dbConnection->prepare($department_iquery);
        
        // Data
        $department_idata = array(':department'=>$department_name,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $department_istatement->execute($department_idata);
		$department_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $department_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get task remarks
INPUT 	: Task Type Name, Active Status, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of processes
BY 		: Nitin Kashyap
*/
function db_get_gen_task_remarks($task_id)
{
	$get_remarks_squery_base = "select * from general_task_remarks";
	
	$get_remarks_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_task_type_list_sdata = array();
	
	if($task_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_remarks_squery_where = $get_remarks_squery_where." where general_task_remarks_task_id=:task";								
		}
		else
		{
			// Query
			$get_remarks_squery_where = $get_remarks_squery_where." and general_task_remarks_task_id=:task";				
		}
		
		// Data
		$get_remarks_sdata[':task']  = $task_id;
		
		$filter_count++;
	}
	
	$get_remarks_squery_order = " order by general_task_remarks_added_on desc";
	
	$get_remarks_squery = $get_remarks_squery_base.$get_remarks_squery_where.$get_remarks_squery_order;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_remarks_sstatement = $dbConnection->prepare($get_remarks_squery);
		
		$get_remarks_sstatement -> execute($get_remarks_sdata);
		
		$get_remarks_sdetails = $get_remarks_sstatement -> fetchAll();
		
		if(FALSE === $get_remarks_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_remarks_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_remarks_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE: To delete task plan
INPUT  : Task Plan ID
OUTPUT : SUCCESS if task plan marked as deleted; DB_NO_RECORD if plan ID was invalid; FAILURE otherwise
BY     : Nitin Kashyap
*/
function db_delete_gen_task_plan($task_plan_id)
{
	// Query
    $gen_task_plan_delete_uquery = "update general_tasks set general_task_delete_status=:status where general_task_id=:task_plan_id";   

    try
    {
        $dbConnection = get_conn_handle();
        
        $gen_task_plan_delete_ustatement = $dbConnection->prepare($gen_task_plan_delete_uquery);
        
        // Data
        $gen_task_plan_delete_udata = array(':status'=>'1',':task_plan_id'=>$task_plan_id);		
        
        $gen_task_plan_delete_ustatement -> execute($gen_task_plan_delete_udata);
		
		$updated_rows = $gen_task_plan_delete_ustatement->rowCount();
		
		if($updated_rows >= 1)
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
		else
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*PURPOSE: To update task detailsINPUT  : Task Plan ID, Task Type, Task Details, Start Date, End Date, Assignee, DepartmentOUTPUT : SUCCESS if task plan updated; DB_NO_RECORD if plan ID was invalid; FAILURE otherwiseBY     : Nitin Kashyap*/function db_update_gen_task_details($task_id,$gen_task_type,$gen_task_details,$gen_task_start_date,$gen_task_end_date,$gen_task_assignee,$department,$status,$project=""){	// Query    $gen_task_plan_uquery = "update general_tasks set general_task_type=:task_type,general_task_details=:task_details,general_task_start_date=:start_date,	general_task_end_date=:end_date,general_task_user=:assignee,general_task_department=:task_department, general_task_completion_status=:status,general_task_project=:project where general_task_id=:task_id"; 	    try    {        $dbConnection = get_conn_handle();                $gen_task_plan_ustatement = $dbConnection->prepare($gen_task_plan_uquery);                // Data        $gen_task_plan_udata = array(':task_type'=>$gen_task_type,':task_details'=>$gen_task_details,':start_date'=>$gen_task_start_date,':end_date'=>$gen_task_end_date,':assignee'=>$gen_task_assignee,':task_department'=>$department,':status'=>$status,':task_id'=>$task_id,':project'=>$project);		        $gen_task_plan_ustatement -> execute($gen_task_plan_udata);				$updated_rows = $gen_task_plan_ustatement->rowCount();				if($updated_rows >= 1)		{			$return["status"] = SUCCESS;			$return["data"]   = '';		}		else		{			$return["status"] = SUCCESS;			$return["data"]   = '';		}    }    catch(PDOException $e)    {        // Log the error        $return["status"] = FAILURE;		$return["data"]   = "";    }		return $return;}
/*
PURPOSE: To update task Remarks
INPUT  : 
OUTPUT : 
BY     : Sonakshi D
*/

function db_update_general_remarks($gen_task_remrks_status,$general_task_remarks_id)
{
	$gen_task_remarks_uquery = "update general_task_remarks set general_task_remarks_status=:task_status where general_task_remarks_id=:general_task_remarks_id";
	
	try
    {
        $dbConnection = get_conn_handle();
        
        $gen_task_remarks_ustatement = $dbConnection->prepare($gen_task_remarks_uquery);
        
        // Data
        $gen_task_remarks_udata = array(':general_task_remarks_id'=>$general_task_remarks_id,':task_status'=>$gen_task_remrks_status);		
        
        $gen_task_remarks_ustatement -> execute($gen_task_remarks_udata);
		
		$updated_rows = $gen_task_remarks_ustatement->rowCount();
		
		if($updated_rows >= 1)
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
		else
		{
			$return["status"] = SUCCESS;
			$return["data"]   = '';
		}
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}


?>