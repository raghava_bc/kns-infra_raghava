<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
  $rowdata = $_POST["val"];
	$vendor_id = $_POST["vendor_id"];
	$length = count($rowdata);

  if($length > 0){
	$start_date   		= "0000-00-00";
	$end_date 		    = "0000-00-00";
	$total_amount 		= "";
	$item_count   		= 0;
	$no_of_hrs    		= 0;
	$total_amount 		= 0;
	$bill_no = p_generate_machine_bill_no($_POST['bill_substr']);
	$payment_machine_iresult = i_add_project_payment_machine($vendor_id,'',$bill_no,'','','','','','','','',$user);
	if($payment_machine_iresult["status"] == SUCCESS)
	{
		$machine_payment_id = $payment_machine_iresult["data"];
	}
	for($item_count = 0 ; $item_count < $length; $item_count++)
	{
			$machine_rate	   = $rowdata[$item_count]['total_amount'];
			$machine_date	   = $rowdata[$item_count]['project_task_actual_machine_plan_start_date_time'];
			$machine_vendor_id = $vendor_id;
			$machine_actual_id = $rowdata[$item_count]['project_task_actual_machine_plan_id'];
      $machine_id = $rowdata[$item_count]['project_machine_master_id'];
			if($start_date == "0000-00-00")
			{
				$start_date = $machine_date;
			}
			else if(strtotime($machine_date) < strtotime($start_date))
			{
				$start_date = $machine_date;
			}

			if($start_date == "0000-00-00")
			{
				$end_date = $machine_date;
			}
			else if(strtotime($machine_date) > strtotime($end_date))
			{
				$end_date = $machine_date;
			}

			$payment_machine_mapping_iresult = i_add_project_payment_machine_mapping($machine_actual_id,$machine_payment_id,'',$user);
			if ($payment_machine_iresult["status"] == SUCCESS) {
			$machine_update_data = array("status1"=>"Bill Generated","end_date_time"=>$end_date,"machine_id"=>$machine_id);
			$manpower_results = i_update_actual_machine_plan($machine_actual_id,$machine_update_data);
		  }
			$total_amount = $total_amount + $machine_rate;
		}

	$machine_start_date = date("Y-m-d",strtotime($start_date)).' '."00:00:00";
	$machine_end_date   = date("Y-m-d",strtotime($end_date)) .' '."00:00:00";

	//Update Payment Manpower
	$project_payment_machine_update_data = array("vendor_id"=>$machine_vendor_id,"amount"=>$total_amount,"from_date"=>$machine_start_date,"to_date"=>$machine_end_date);

	$payment_contract_uresults =  i_update_project_payment_machine($machine_payment_id,$project_payment_machine_update_data);

	 $output = array('data'=> $payment_contract_uresults['data'],'status'=> $payment_contract_uresults['status'],'id'=>$machine_payment_id);

		echo json_encode($output);
 }
}
 else{
   echo "FAILURE";
 }
?>
