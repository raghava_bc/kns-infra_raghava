<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_actual_contract_payment_list.php
CREATED ON	: 09-May-2017
CREATED BY	: Ashwini
PURPOSE     : List of project for actual contract payment
*/

/*
TBD:
*/

/* DEFINES - START */
define('PROJECT_MACHINE_ACTUAL_PAYMENT_FUNC_ID','275');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	$alert_type = -1;
	$alert = "";

	// Get permission settings for this user for this page
	$view_perms_list    = i_get_user_perms($user,'',PROJECT_MACHINE_ACTUAL_PAYMENT_FUNC_ID,'2','1');
	$edit_perms_list    = i_get_user_perms($user,'',PROJECT_MACHINE_ACTUAL_PAYMENT_FUNC_ID,'3','1');
	$approve_perms_list = i_get_user_perms($user,'',PROJECT_MACHINE_ACTUAL_PAYMENT_FUNC_ID,'6','1');


	// Query String Data
	// Nothing

	$search_machine_vendor  = "-1";

	if(isset($_POST["mac_acc_pay_search_submit"]))
	{
		$search_machine_vendor  = $_POST["search_machine_vendor"];
	}
	if(isset($_REQUEST["search_machine_vendor"]))
	{
		$search_machine_vendor  = $_REQUEST["search_machine_vendor"];
	}

	// Get Project Actual Machine Payment already added
	$project_payment_machine_search_data = array("active"=>'1',"status"=>'Approved',"vendor_id"=>$search_machine_vendor);
	$project_actual_machine_payment_list = i_get_project_payment_machine($project_payment_machine_search_data);
	if($project_actual_machine_payment_list['status'] == SUCCESS)
	{
		$project_actual_machine_payment_list_data = $project_actual_machine_payment_list['data'];
	}
	else
	{
		$alert = $alert."Alert: ".$project_actual_machine_payment_list["data"];
	}

	// Project data
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list["status"] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_management_master_list["data"];
	}
	// Machine Vendor data
	$project_machine_vendor_search_data = array("active"=>'1');
	$project_machine_vendor_list = i_get_project_machine_vendor_master_list($project_machine_vendor_search_data);
	if($project_machine_vendor_list["status"] == SUCCESS)
	{
		$project_machine_vendor_list_data = $project_machine_vendor_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_machine_vendor_list["data"];
	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Weekly Machine Payment List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>


<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Weekly Machine Payment List</h3>
            </div>
			<div class="widget-header" style="height:50px; padding-top:10px;">
			  <form method="post" id="file_search_form" action="project_actual_machine_payment_list.php">

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_machine_vendor">
			  <option value="">- - Select Vendor - -</option>
			  <?php
			  for($vendor_count = 0; $vendor_count < count($project_machine_vendor_list_data); $vendor_count++)
			  {
			  ?>
			  <option value="<?php echo $project_machine_vendor_list_data[$vendor_count]["project_machine_vendor_master_id"]; ?>" <?php if($search_machine_vendor == $project_machine_vendor_list_data[$vendor_count]["project_machine_vendor_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_machine_vendor_list_data[$vendor_count]["project_machine_vendor_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <input type="submit" name="mac_acc_pay_search_submit" />
			  </form>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php if($view_perms_list['status'] == SUCCESS)
			{
			?>
                <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th style="width:2%">SL No</th>
				    <th style="width:5%">Project</th>
					<th style="width:5%">Bill No</th>
					<th style="width:3%">From Date</th>
					<th style="width:3%">To Date</th>
					<th style="width:3%">Machine No</th>
					<th style="width:5%">Vendor</th>
					<th style="width:3%">Total</th>
					<th style="width:7%">Remarks</th>
					<th style="width:6%">Added By</th>
					<th style="width:4%">Added On</th>
					<th colspan="2" style="width:5%">Action</th>

				</tr>
				</thead>
				<tbody>
				<?php
				if($project_actual_machine_payment_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($project_actual_machine_payment_list_data); $count++)
					{
						$sl_no++;

						// Get machine payment mapping for project name
						$project_payment_machine_mapping_search_data = array("payment_id"=>$project_actual_machine_payment_list_data[$count]["project_payment_machine_id"]);
						$project_name_sresult = i_get_project_payment_machine_mapping($project_payment_machine_mapping_search_data);
						if($project_name_sresult['status'] == SUCCESS)
						{
							$actual_machine_plan_search_data = array('plan_id'=>$project_name_sresult['data'][0]['project_payment_machine_mapping_machine_actuals_id']);
							$project_name_data = i_get_machine_planning_list($actual_machine_plan_search_data);
							$project_name = $project_name_data['data'][0]['project_master_name'];
							$machine   = $project_name_data['data'][0]['project_machine_master_id_number'];
						}
						else
						{
							$project_name = 'INVALID PROJECT';
							$machine = "";
						}
						// Get billing details
						$project_payment_machine_mapping_search_data = array("payment_id"=>$project_actual_machine_payment_list_data[$count]["project_payment_machine_id"],"active"=>'1');
						$machine_payment_mapping_list = i_get_project_payment_machine_mapping($project_payment_machine_mapping_search_data);
						 if ($machine_payment_mapping_list['status'] == SUCCESS) {
								$machine_payment_mapping_list_data = $machine_payment_mapping_list["data"];
								$amount = 0 ;
								for ($payment_count = 0; $payment_count < count($machine_payment_mapping_list['data']); $payment_count++) {
									$off_hours = $machine_payment_mapping_list['data'][$payment_count]['project_task_actual_machine_plan_off_time']/60;
								$total_hours = (strtotime($machine_payment_mapping_list['data'][$payment_count]['project_task_actual_machine_plan_end_date_time']) - strtotime($machine_payment_mapping_list['data'][$payment_count]['project_task_actual_machine_plan_start_date_time']))/3600;
								$working_hours = round(($total_hours - $off_hours), 2);
								$amount = $amount + (($machine_payment_mapping_list['data'][$payment_count]['project_task_actual_machine_fuel_charges'] * $working_hours) + $machine_payment_mapping_list['data'][$payment_count]['project_task_actual_machine_bata'] + $machine_payment_mapping_list['data'][$payment_count]['project_task_actual_machine_plan_additional_cost'] - $machine_payment_mapping_list['data'][$payment_count]['project_task_actual_machine_issued_fuel']);
							}
							}
							else {
								$amount = "";
							}
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_name; ?></td>
					<td><?php echo $project_actual_machine_payment_list_data[$count]["project_payment_machine_bill_no"]; ?></td>
					<td><?php echo date("d-M-Y",strtotime($project_actual_machine_payment_list_data[$count]["project_payment_machine_from_date"])); ?></td>
					<td><?php echo date("d-M-Y",strtotime($project_actual_machine_payment_list_data[$count]["project_payment_machine_to_date"])); ?></td>
					<td><?php echo $machine ; ?></td>
					<td><?php echo $project_actual_machine_payment_list_data[$count]["project_machine_vendor_master_name"]; ?></td>
					<td><?php echo $project_actual_machine_payment_list_data[$count]["project_payment_machine_amount"]; ?></td>
					<td><?php echo $project_actual_machine_payment_list_data[$count]["project_payment_machine_remarks"]; ?></td>
					<td><?php echo $project_actual_machine_payment_list_data[$count]["added_by"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_actual_machine_payment_list_data[$count][
					"project_payment_machine_added_on"])); ?></td>
					<td><?php if($approve_perms_list['status'] == SUCCESS){ if(($project_actual_machine_payment_list_data[$count]["project_payment_machine_status"] == "Approved")){?><a href="#" onclick="return project_approve_task_actual_machine_payment(<?php echo $project_actual_machine_payment_list_data[$count]["project_payment_machine_id"]; ?>);">Accept</a><?php } } ?></td>
					<td><?php if(($view_perms_list['status'] == SUCCESS)){?><a href="#" target="_blank" onclick="return go_to_machine_print(<?php echo $project_actual_machine_payment_list_data[$count]["project_payment_machine_id"]; ?>);">Print</a><?php } ?></td>
					</tr>
					<?php
					}
				}
				else
				{
				?>
				<td colspan="11">No Machine Payment details added yet!</td>

				<?php
				}
				 ?>

                </tbody>
              </table>
	  <?php } ?>
            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>

function project_approve_task_actual_machine_payment(machine_payment_id)
{
	var ok = confirm("Are you sure you want to Accept?")
	{
		if (ok)
		{
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					alert(xmlhttp.responseText);
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					  window.location = "project_actual_machine_payment_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_approve_machine_payment.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("machine_payment_id=" + machine_payment_id + "&action=Accepted");
		}
	}
}
function go_to_machine_print(machine_payment_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_machine_weekly_print.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","machine_payment_id");
	hiddenField1.setAttribute("value",machine_payment_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}
</script>
</body>

</html>
