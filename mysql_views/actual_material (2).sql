-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 22, 2018 at 01:38 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kns_erp_live`
--

-- --------------------------------------------------------

--
-- Structure for view `actual_material`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `actual_material` AS select `PAM`.`actual_material_indent_id` AS `actual_material_indent_id`,`PAM`.`actual_material_issue_item_id` AS `issue_item_id`,`SI`.`stock_indent_no` AS `stock_indent_no`,`PAM`.`actual_material_project_id` AS `actual_material_project_id`,`PMPM`.`project_master_name` AS `project_name`,`PAM`.`actual_material_task_id` AS `actual_material_task_id`,`PAM`.`actual_material_road_id` AS `actual_material_road_id`,`PAM`.`actual_material_machine_id` AS `machine_id`,`PAM`.`actual_material_master_id` AS `actual_material_master_id`,`PAM`.`actual_material_qty` AS `actual_material_qty`,`SMM`.`stock_material_code` AS `stock_material_code`,`SMM`.`stock_material_price` AS `material_price`,`SMM`.`stock_material_name` AS `stock_material_name`,`PM`.`project_process_master_name` AS `process_name`,`PTM`.`project_task_master_name` AS `project_task_master_name`,`PSLM`.`project_site_location_mapping_master_name` AS `road_name`,(`PAM`.`actual_material_qty` * `SMM`.`stock_material_price`) AS `total_amt` from ((((((((`project_actual_material` `PAM` join `stock_indent` `SI` on((`SI`.`stock_indent_id` = `PAM`.`actual_material_indent_id`))) join `project_management_project_master` `PMPM` on((`PMPM`.`project_management_master_id` = `PAM`.`actual_material_project_id`))) join `stock_material_master` `SMM` on((`SMM`.`stock_material_id` = `PAM`.`actual_material_master_id`))) join `project_plan_process` `PPP` on((`PPP`.`project_plan_process_id` = `PAM`.`actual_material_process_id`))) join `project_process_master` `PM` on((`PM`.`project_process_master_id` = `PPP`.`project_plan_process_name`))) join `project_plan_process_task` `PPPT` on((`PPPT`.`project_process_task_id` = `PAM`.`actual_material_task_id`))) join `project_task_master` `PTM` on((`PTM`.`project_task_master_id` = `PPPT`.`project_process_task_type`))) left join `project_site_location_mapping_master` `PSLM` on((`PSLM`.`project_site_location_mapping_master_id` = `PAM`.`actual_material_road_id`)));

--
-- VIEW  `actual_material`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
