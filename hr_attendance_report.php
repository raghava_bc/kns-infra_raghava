<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: process_list.php
CREATED ON	: 05-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Process Plans
*/

/*
TBD:
1. Date display and calculation
2. Permission management
*/
$_SESSION['module'] = 'HR';
define('DAILY_ATTENDANCE_FUNC_ID','45');

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_attendance_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_employee_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',DAILY_ATTENDANCE_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',DAILY_ATTENDANCE_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',DAILY_ATTENDANCE_FUNC_ID,'4','1');

	// Query String Data
	// Nothing here

	// Temp data
	$alert      = "";
	$alert_type = -1;

	// Logged In Employee
	$employee_filter_data = array("employee_user"=>$user,"user_status"=>'1');
	$search_user_sresult = i_get_employee_list($employee_filter_data);

	// Search parameters
	$is_manager = 0;
	if(isset($_POST["attendance_search_submit"]))
	{
		$start_date  = $_POST["dt_start_attendance_date"];
		$end_date    = $_POST["dt_end_attendance_date"];
		$search_user = $_POST["ddl_employee"];
		$status      = $_POST["ddl_status"];
	}
	else
	{
		$start_date  = date("Y-m")."-01";
		$number_of_days_in_month = cal_days_in_month(CAL_GREGORIAN,date("m"),date("Y"));
		$end_date    = date("Y-m")."-".$number_of_days_in_month;
		$search_user = $search_user_sresult["data"][0]["hr_employee_id"];
		$status      = "";
	}

	if(($role == "13") || ($user == "143620071466608200"))
	{
		$is_manager = 1;
	}
	else
	{
		// Check if this user is a manager to any other user
		$mgr_employee_filter_data = array("employee_manager"=>$user,"user_status"=>'1');
		$mgr_employee_list = i_get_employee_list($mgr_employee_filter_data);
		if($mgr_employee_list["status"] == SUCCESS)
		{
			$is_manager = 1;
		}
		else
		{
			$is_manager = 0;
		}
	}

	// Get list of attendance
	$attendance_filter_data = array();
	if($search_user != "")
	{
		$attendance_filter_data["employee_id"] = $search_user;
	}
	else
	{
		if($is_manager == "1")
		{
			$attendance_filter_data["manager"] = $user;
		}
	}
	if($start_date != "")
	{
		$attendance_filter_data["attendance_start_date"] = $start_date;
	}
	if($end_date != "")
	{
		$attendance_filter_data["attendance_end_date"] = $end_date;
	}
	if($status != "")
	{
		$attendance_filter_data["attendance_type"] = $status;
	}
	
	$attendance_list = i_get_attendance_list($attendance_filter_data);

	if($attendance_list["status"] == SUCCESS)
	{
		$attendance_list_data = $attendance_list["data"];
	}
	else
	{
		$alert      = $alert."Alert: ".$attendance_list["data"];
		$alert_type = "1";
	}

	// Get list of employees
	if(($role == "13") || ($user == "143620071466608200"))
	{
		$employee_filter_data = array("user_status"=>'1');
	}
	else
	{
		$employee_filter_data = array("employee_manager"=>$user,"user_status"=>'1');
	}

	$employee_list = i_get_employee_list($employee_filter_data);

	if($employee_list["status"] == SUCCESS)
	{
		$employee_list_data = $employee_list["data"];
	}
	else
	{
		$alert      = $alert."Alert: ".$employee_list["data"];
		$alert_type = "1";
	}

	// Get list of attendance types
	$attendance_type_filter_data = array("active"=>'1');
	$attendance_type_list = i_get_attendance_type($attendance_type_filter_data);
	if($attendance_type_list["status"] == SUCCESS)
	{
		$attendance_type_list_data = $attendance_type_list["data"];
	}
	else
	{
		$alert      = $alert."Alert: ".$attendance_type_list_data["data"];
		$alert_type = "1";
	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Attendance Report</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header" style="height:80px; padding-top:10px;"> <i class="icon-th-list"></i>
              <h3>Attendance List</h3><br /><h3>Total Present: <span id="total_present"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;Total Absent: <span id="total_absent"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;Total Half Days: <span id="total_half_days"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;Week Off Days: <span id="total_week_off_days"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;Public Holidays: <span id="total_public_holidays"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;Casual Leaves: <span id="total_cl"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;Sick Leaves: <span id="total_sl"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;Comp Offs: <span id="total_comp_off"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;Total Payable: <span id="total_payable"><i>Calculating</i></span></h3>
            </div>
            <!-- /widget-header -->

			<div class="widget-header" style="height:50px; padding-top:10px;">
			  <form method="post" id="attendance_search_form" action="hr_attendance_report.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_start_attendance_date" value="<?php echo $start_date; ?>" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_end_attendance_date" value="<?php echo $end_date; ?>" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_employee">
			  <?php
			  if($is_manager == '1')
			  {
			  ?>
			  <option value="">- - All Reportees - -</option>
			  <?php
			  }
			  ?>
			  <option value="<?php echo $search_user_sresult["data"][0]["hr_employee_id"]; ?>" <?php if($search_user == $search_user_sresult["data"][0]["hr_employee_id"]) { ?> selected="selected" <?php } ?>><?php echo $search_user_sresult["data"][0]["hr_employee_name"]; ?></option>
			  <?php
			  for($count = 0; $count < count($employee_list_data); $count++)
			  {
				if($search_user_sresult["data"][0]["hr_employee_id"] != $employee_list_data[$count]["hr_employee_id"])
				{
			  ?>
			  <option value="<?php echo $employee_list_data[$count]["hr_employee_id"]; ?>" <?php if($search_user == $employee_list_data[$count]["hr_employee_id"]) { ?> selected="selected" <?php } ?>><?php echo $employee_list_data[$count]["hr_employee_name"]; ?></option>
			  <?php
				}
			  }
			  ?>
			  </select>
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_status">
			  <option value="">- - Select Attendance Status - -</option>
			  <?php
			  for($count = 0; $count < count($attendance_type_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $attendance_type_list_data[$count]["hr_attendance_type_id"]; ?>" <?php if($status == $attendance_type_list_data[$count]["hr_attendance_type_id"]) { ?> selected="selected" <?php } ?>><?php echo $attendance_type_list_data[$count]["hr_attendance_type_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <input type="submit" name="attendance_search_submit" />
			  </form>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th>Date</th>
					<th>Day</th>
					<th>Employee</th>
					<th>Emp Code</th>
					<th>Shift</th>
					<th>In Time</th>
				    <th>Out Time</th>
					<th>Out Pass</th>
					<th>Work Duration</th>
				    <th>OT</th>
					<th>Total Duration</th>
					<th>Status</th>
					<?php
					if(($role == "13") || ($user == "143620071466608200"))
					{
					?>
					<th>&nbsp;</th>
					<?php
					}
					?>
				</tr>
				</thead>
				<tbody>
				 <?php
				$total_present   = 0;
				$total_absent    = 0;
				$total_half_days = 0;
				$el_count        = 0;
				$sl_count        = 0;
				$cl_count        = 0;
				$col_count       = 0;
				$week_off        = 0;
				$public_holiday  = 0;
				if($attendance_list["status"] == SUCCESS)
				{
					for($count = 0; $count < count($attendance_list_data); $count++)
					{
					?>
					<tr <?php
					/* Check if this record is editable i.e. is it under the configured edit window - START */
					// Get last day of previous month
					$first_day_this_month = date("Y-m-")."01";
					$last_day_last_month  = date('Y-m-d',strtotime($first_day_this_month.' -1 day'));

					// Get date difference between today and attendance date
					$date_diff_result = get_date_diff($attendance_list_data[$count]["hr_attendance_date"],date('Y-m-d'));
					$last_month_days = cal_days_in_month(CAL_GREGORIAN,date("m",strtotime($last_day_last_month)),date("Y",strtotime($last_day_last_month)));

					// Check for editability
					if(date('m',strtotime($attendance_list_data[$count]["hr_attendance_date"])) == date('m'))
					{
						$editable = true;
					}
					else if((date('m',strtotime($attendance_list_data[$count]["hr_attendance_date"])) == date('m',strtotime($last_day_last_month))) && (date("d") <= ATTENDANCE_RECTIFY_BUFFER_DAYS))
					{
						$editable = true;
					}
					else
					{
						$editable = false;
					}
					/* Check if this record is editable i.e. is it under the configured edit window - END */

					$out_pass_filter_data = array('employee_id'=>$attendance_list_data[$count]["hr_attendance_employee"],"date"=>$attendance_list_data[$count]["hr_attendance_date"],"status"=>'1');
					$out_pass_sresult = i_get_out_pass_list($out_pass_filter_data);

					$op_in_time = false;
					if(($out_pass_sresult['status'] == SUCCESS) && ($attendance_list_data[$count]["hr_attendance_in_time"] != '00:00') && ($attendance_list_data[$count]["hr_attendance_in_time"] != '0:0') && ($attendance_list_data[$count]["hr_attendance_in_time"] != '0:00') && ($attendance_list_data[$count]["hr_attendance_out_time"] != '00:00') && ($attendance_list_data[$count]["hr_attendance_out_time"] != '0:0') && ($attendance_list_data[$count]["hr_attendance_out_time"] != '0:00'))
					{
						$op_in_time = t_check_in_time_proper($out_pass_sresult['data'][0]["hr_out_pass_out_time"],$out_pass_sresult['data'][0]["hr_out_pass_finish_time"]);

						$total_working_hours = i_get_total_working_hours($attendance_list_data[$count]["hr_attendance_in_time"],$attendance_list_data[$count]["hr_attendance_out_time"],$out_pass_sresult['data'][0]["hr_out_pass_out_time"],$out_pass_sresult['data'][0]["hr_out_pass_finish_time"]);
					}
					else
					{
						$total_working_hours = i_get_total_working_hours($attendance_list_data[$count]["hr_attendance_in_time"],$attendance_list_data[$count]["hr_attendance_out_time"],'00:00','00:00');
					}

					$disp_working = $total_working_hours['work_duration'];
					$disp_ot      = $total_working_hours['ot_duration'];
					$disp_total   = $total_working_hours['duration'];

					// Check for clean swipe by employee
					$is_swipe_clean = i_get_employee_swipe($attendance_list_data[$count]["hr_attendance_in_time"],$attendance_list_data[$count]["hr_attendance_out_time"]);

					// Get attendance type
					if($is_swipe_clean != 0)
					{
						$attendance_type = '2';

						$disp_working = '00:00';
						$disp_ot      = '00:00';
						$disp_total   = '00:00';
					}
					else
					{
						$attendance_type = i_get_attendance_type_from_value($total_working_hours['mins']);
					}

					$attendance_type_filter_data = array('type_id'=>$attendance_type);
					$attendance_type_name = i_get_attendance_type($attendance_type_filter_data);

					if((!(t_check_in_time_proper($attendance_list_data[$count]["hr_attendance_in_time"],$attendance_list_data[$count]["hr_attendance_out_time"]))) && ($op_in_time != true))
					{
						?>
						style="color:red;"
						<?php
					}
					?>>
						<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($attendance_list_data[$count]["hr_attendance_date"])); ?></td>
						<td style="word-wrap:break-word;"><?php echo date("D",strtotime($attendance_list_data[$count]["hr_attendance_date"])); ?></td>
						<td style="word-wrap:break-word;"><?php echo $attendance_list_data[$count]["hr_employee_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $attendance_list_data[$count]["hr_employee_code"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $attendance_list_data[$count]["hr_attendance_shift"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $attendance_list_data[$count]["hr_attendance_in_time"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $attendance_list_data[$count]["hr_attendance_out_time"]; ?></td>
						<td style="word-wrap:break-word;"><?php if($out_pass_sresult['status'] == SUCCESS)
						{
							echo $out_pass_sresult['data'][0]["hr_out_pass_out_time"].' to '.$out_pass_sresult['data'][0]["hr_out_pass_finish_time"];
						}
						else
						{
							echo 'NO OUT PASS';
						}?></td>
						<td style="word-wrap:break-word;"><?php echo $disp_working; ?></td>
						<td style="word-wrap:break-word;"><?php echo $disp_ot; ?></td>
						<td style="word-wrap:break-word;"><?php echo $disp_total; ?></td>
						<td style="word-wrap:break-word;"><?php
						switch($attendance_type)
						{
						case ATTENDANCE_TYPE_PRESENT:
						$total_present++;
						$holiday_filter_data = array("date_start"=>$attendance_list_data[$count]["hr_attendance_date"],"date_end"=>$attendance_list_data[$count]["hr_attendance_date"],"status"=>'1');
						$holiday_list = i_get_holiday($holiday_filter_data);
						if(date("l",strtotime($attendance_list_data[$count]["hr_attendance_date"])) == get_day($attendance_list_data[$count]["hr_employee_week_off"]))
						{
							echo "Worked on a Week Off";
						}
						else if($holiday_list["status"] == SUCCESS)
						{
							echo "Worked on Public Holiday (".$holiday_list["data"][0]["hr_holiday_name"].")";
						}
						else
						{
							echo $attendance_type_name['data'][0]["hr_attendance_type_name"];
						}
						break;

						case ATTENDANCE_TYPE_HALFDAY:
						$total_half_days++;
						$holiday_filter_data = array("date_start"=>$attendance_list_data[$count]["hr_attendance_date"],"date_end"=>$attendance_list_data[$count]["hr_attendance_date"],"status"=>'1');
						$holiday_list = i_get_holiday($holiday_filter_data);
						if(date("l",strtotime($attendance_list_data[$count]["hr_attendance_date"])) == get_day($attendance_list_data[$count]["hr_employee_week_off"]))
						{
							if($total_working_hours['mins'] >= 510)
							{
								echo "Worked on a Week Off";
								$week_off = $week_off + 1;
							}
							else
							{
								echo "Half Day Worked on a Week Off";
								$week_off = $week_off + 0.5;
							}
						}
						else if($holiday_list["status"] == SUCCESS)
						{
							echo "Half Day Worked on Public Holiday (".$holiday_list["data"][0]["hr_holiday_name"].")";
							$public_holiday = $public_holiday + 0.5;
						}
						else
						{
							$leave_filter_data = array("employee_id"=>$attendance_list_data[$count]["hr_employee_id"],"date"=>$attendance_list_data[$count]["hr_attendance_date"],"status"=>LEAVE_STATUS_APPROVED);
							$leave_sresult = i_get_leave_list($leave_filter_data);
							if($leave_sresult["status"] == SUCCESS)
							{
								echo $leave_sresult["data"][0]["hr_attendance_type_name"]." ".$attendance_type_name['data'][0]["hr_attendance_type_name"];;

								switch($leave_sresult["data"][0]["hr_absence_type"])
								{
								case LEAVE_TYPE_EARNED:
								$el_count = $el_count + 0.5;
								break;

								case LEAVE_TYPE_SICK:
								$sl_count = $sl_count + 0.5;
								break;

								case LEAVE_TYPE_CASUAL:
								$cl_count = $cl_count + 0.5;
								break;

								case LEAVE_TYPE_COMP_OFF:
								$col_count = $col_count + 0.5;
								break;
								}
							}
							else
							{
								echo $attendance_type_name['data'][0]["hr_attendance_type_name"];
							}
						}
						break;

						case ATTENDANCE_TYPE_ABSENT:
						$holiday_filter_data = array("date_start"=>$attendance_list_data[$count]["hr_attendance_date"],"date_end"=>$attendance_list_data[$count]["hr_attendance_date"],"status"=>'1');
						$holiday_list = i_get_holiday($holiday_filter_data);
						if(date("l",strtotime($attendance_list_data[$count]["hr_attendance_date"])) == get_day($attendance_list_data[$count]["hr_employee_week_off"]))
						{
							echo "Weekly Off";
							$week_off++;
						}
						else if($holiday_list["status"] == SUCCESS)
						{
							echo "Holiday (".$holiday_list["data"][0]["hr_holiday_name"].")";
							$public_holiday++;
						}
						else
						{
							$leave_filter_data = array("employee_id"=>$attendance_list_data[$count]["hr_employee_id"],"date"=>$attendance_list_data[$count]["hr_attendance_date"],"status"=>LEAVE_STATUS_APPROVED);
							$leave_sresult = i_get_leave_list($leave_filter_data);
							if($leave_sresult["status"] == SUCCESS)
							{
								for($lt_count = 0; $lt_count < count($leave_sresult['data']); $lt_count++)
								{
									if($lt_count == 0)
									{
										echo $leave_sresult["data"][$lt_count]["hr_attendance_type_name"];
									}
									else
									{
										echo ' '.$leave_sresult["data"][$lt_count]["hr_attendance_type_name"];
									}

									switch($leave_sresult["data"][$lt_count]["hr_absence_type"])
									{
									case LEAVE_TYPE_EARNED:
									if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_PRESENT)
									{
										$el_count++;
									}
									else if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_HALFDAY)
									{
										$el_count = $el_count + 0.5;
										echo " Half Day";
									}
									break;

									case LEAVE_TYPE_SICK:
									if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_PRESENT)
									{
										$sl_count++;
									}
									else if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_HALFDAY)
									{
										$sl_count = $sl_count + 0.5;
										echo " Half Day";
									}
									break;

									case LEAVE_TYPE_CASUAL:
									if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_PRESENT)
									{
										$cl_count++;
									}
									else if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_HALFDAY)
									{
										$cl_count = $cl_count + 0.5;
										echo " Half Day";
									}
									break;

									case LEAVE_TYPE_COMP_OFF:
									if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_PRESENT)
									{
										$col_count++;
									}
									else if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_HALFDAY)
									{
										$col_count = $col_count + 0.5;
										echo " Half Day";
									}
									break;
									}
								}
							}
							else
							{
								echo $attendance_type_name['data'][0]["hr_attendance_type_name"];
								$total_absent++;
							}
						}
						break;
						}
						?></td>
						<?php
						if(($edit_perms_list['status'] == SUCCESS) && ($editable == true))
						{
						?>
							<td style="word-wrap:break-word;"><a href="hr_edit_attendance.php?attendance=<?php echo $attendance_list_data[$count]["hr_attendance_id"]; ?>">Edit</a></td>
						<?php
						}
						else if($edit_perms_list['status'] != SUCCESS)
						{
							?>
							<td>Can't Edit. No Perms</td>
							<?php
						}
						else if($editable != true)
						{
							?>
							<td>Can't Edit. Too Late</td>
							<?php
						}
						?>
					</tr>
					<?php
					}
				}
				else
				{
				?>
				<td colspan="11">No attendance details!</td>
				<?php
				}
				 ?>

                </tbody>
              </table>
			  <?php
			  // Calculate total payable days in the duration
			  $total_payable = $total_present + ($total_half_days*0.5) + $week_off + $public_holiday + $el_count + $sl_count + $col_count;
			  ?>
			  <script>
			  document.getElementById("total_present").innerHTML = <?php echo $total_present; ?>;
			  document.getElementById("total_absent").innerHTML = <?php echo $total_absent; ?>;
			  document.getElementById("total_half_days").innerHTML = <?php echo $total_half_days; ?>;
			  document.getElementById("total_week_off_days").innerHTML = <?php echo $week_off; ?>;
			  document.getElementById("total_public_holidays").innerHTML = <?php echo $public_holiday; ?>;
			  document.getElementById("total_cl").innerHTML = <?php echo $el_count; ?>;
			  document.getElementById("total_sl").innerHTML = <?php echo $sl_count; ?>;
			  document.getElementById("total_comp_off").innerHTML = <?php echo $col_count; ?>;
			  document.getElementById("total_payable").innerHTML = <?php echo $total_payable;; ?>;
			  </script>
            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function confirm_deletion(file_id)
{
	var ok = confirm("Are you sure you want to delete?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					 window.location = "pending_file_list.php";
				}
			}

			xmlhttp.open("GET", "legal_file_delete.php?file=" + file_id);   // file name where delete code is written
			xmlhttp.send();
		}
	}
}

</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>

</html>
