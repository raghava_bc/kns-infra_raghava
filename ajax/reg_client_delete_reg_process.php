<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'registration'.DIRECTORY_SEPARATOR.'reg_client_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$process_id        = $_POST["process_id"];
	$active            = $_POST["action"];
	
	$reg_client_reg_process_update_data = array("active"=>$active);
	$reg_client_process_result = i_update_reg_client_reg_process($process_id,$reg_client_reg_process_update_data);
	
	if($reg_client_process_result["status"] == FAILURE)
	{
		echo $reg_client_process_result["data"];
	}
	else
	{
		echo "SUCCESS";
	}
}
else
{
	header("location:login.php");
}
?>