<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 22-March-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* DEFINES - START */
//define('REG_CLIENT_DOCUMENT_FUNC_ID','199');
define('REG_CLIENT_DOCUMENT_FUNC_ID','221');
/* DEFINES - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'registration'.DIRECTORY_SEPARATOR.'reg_client_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$add_perms_list    = i_get_user_perms($user,'',REG_CLIENT_DOCUMENT_FUNC_ID,'1','1');
	$view_perms_list   = i_get_user_perms($user,'',REG_CLIENT_DOCUMENT_FUNC_ID,'2','1');
	$delete_perms_list = i_get_user_perms($user,'',REG_CLIENT_DOCUMENT_FUNC_ID,'4','1');

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	// Query String Data
	if(isset($_REQUEST['process_id']))
	{
		$process_id = $_REQUEST['process_id'];
	}
	else
	{
		$process_id = "";
	}
	
	if(isset($_REQUEST['request_id']))
	{
		$request_id = $_REQUEST['request_id'];
	}
	else
	{
		$request_id = "";
	}
	
	
	// Capture the form data
	if(isset($_POST["add_reg_client_document_submit"]))
	{		
		$process_id                 = $_POST["hd_process_id"];
		$request_id                 = $_POST["hd_request_id"];
		$doc 		                = upload("file_remarks_doc",$user);
		
		$reg_client_iresult =  i_add_reg_client_document($process_id,$doc,'',$user);
	}	
	//Get Document List
	$reg_client_document_search_data = array("active"=>'1',"process_id"=>$process_id,"request_id"=>$request_id);
	$reg_client_document_list = i_get_reg_client_document($reg_client_document_search_data);
	if($reg_client_document_list["status"] == SUCCESS)
	{
		$reg_client_document_list_data = $reg_client_document_list["data"];
	}
	else
	{
		
	}
		
}
else
{
	header("location:login.php");
}	

// Functions
function upload($file_id,$user_id) 
{
	if($_FILES[$file_id]["name"]!="")
	{
		if ($_FILES[$file_id]["error"] > 0)
		{
			// echo "Return Code: " . $_FILES[$file_id]["error"] . "<br />";
		}
		else
		{
			$_FILES[$file_id]["name"]=$user_id."_".$_FILES[$file_id]["name"];
			move_uploaded_file($_FILES[$file_id]["tmp_name"], "documents/".$_FILES[$file_id]["name"]);							  
		}
	}
	
	return $_FILES[$file_id]["name"];
}
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Reg Client Process</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Your Reg Client Process</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Reg Client Process</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="reg_client_upload_form" class="form-horizontal" method="post" action="reg_client_upload_documents.php" enctype="multipart/form-data">
								<input type="hidden" name="hd_process_id" value="<?php echo $process_id; ?>" />
								<input type="hidden" name="hd_request_id" value="<?php echo $request_id; ?>" />
									<fieldset>	

										<div class="control-group">											
											<label class="control-label" for="file_remarks_doc">Document</label>
											<div class="controls">
											<input type="file" name="file_remarks_doc" id="file_remarks_doc" />
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
                                                                                                                                                               										 <br />
										
										<div class="form-actions">
										<?php
										if($add_perms_list['status'] == SUCCESS)
										{
										?>
											<input type="submit" class="btn btn-primary" name="add_reg_client_document_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
											<?php
											}
											else
											{
												echo 'You are not authorized to view this page';
											}
											?>
										</div> <!-- /form-actions -->
										
											<div class="form-actions">
											</div> <!-- /form-actions -->
										
									</fieldset>
								</form>
								</div>
							</div> 
							
							<div class="widget-content">
							<?php
							if($view_perms_list['status'] == SUCCESS)
							{
							?>
			
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Document</th>
					<th>Added By</th>				
					<th>Added On</th>
                    				
				</tr>
				</thead>
				<tbody>	
				<?php
					if($reg_client_document_list["status"] == SUCCESS)
					{
					$sl_no = 0;
					for($count = 0; $count < count($reg_client_document_list_data); $count++)
					{
						$sl_no++;
						$doc = $reg_client_document_list_data[$count]["reg_client_document_file_path"];
					if($doc != 0)
					{
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><a href="documents/<?php echo $reg_client_document_list_data[$count]["reg_client_document_file_path"]; ?>" target="_blank"><?php echo $reg_client_document_list_data[$count]["reg_client_document_file_path"]; ?></a></td>
					<td><?php echo $reg_client_document_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($reg_client_document_list_data[$count][
					"reg_client_document_added_on"])); ?></td>
					
					</tr>
					<?php
					}
					}
					
				}
				else
				{
				?>
				<td colspan="6">No Documents added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
			   <?php
			}
			else
			{
				echo 'You are not authorized to view this page';
			}
			?>
            </div>
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function reg_client_delete_document(document_id,process_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "reg_client_upload_documents.php?process_id=" +process_id;
					}
				}
			}

			xmlhttp.open("POST", "ajax/reg_client_delete_document.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("document_id=" + document_id + "&action=0");
		}
	}	
}

</script>

  </body>

</html>
