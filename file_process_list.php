<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: file_process_list.php
CREATED ON	: 06-Nov-2016
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Process Plans for a file
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET['file']))
	{
		$file_id = $_GET['file'];
	}
	else
	{
		header("location:pending_project_list.php");
	}
	
	// Initialization
	$alert_type = -1;
	$alert      = '';

	$type   = '';
	$status = '';
	$order  = '';

	// Get list of process plans for this module
	$legal_process_plan_list = i_get_legal_process_plan_list('',$file_id,$type,'','','','','','',$status,$order);
	if($legal_process_plan_list["status"] == SUCCESS)
	{
		$legal_process_plan_list_data = $legal_process_plan_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$legal_process_plan_list["data"];
	}	
	
	// Get list of bulk processes for this module
	$legal_bulk_search_data['file_id'] = $file_id;
	$legal_bulk_process_plan_list = i_get_bulk_legal_process_plan_details($legal_bulk_search_data);	
	if($legal_bulk_process_plan_list["status"] == SUCCESS)
	{
		$legal_bulk_process_plan_list_data = $legal_bulk_process_plan_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$legal_bulk_process_plan_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Process List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Process List</h3>
            </div>
            <!-- /widget-header -->
						
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th>SL No</th>	
					<th>Process</th>					
				    <th>Start Date</th>
					<th>End Date</th>	
					<th>Variance</th>											
				</tr>
				</thead>
				<tbody>
				<tr>
				<td colspan="5" style="word-wrap:break-word;"><b>SINGLE PROCESS</b></td>
				</tr>
				<?php				
				if($legal_process_plan_list["status"] == SUCCESS)
				{		
					$sl_no = 0;
					for($count = 0; $count < count($legal_process_plan_list_data); $count++)
					{
						$sl_no++;
						
						$task_plan_result = i_get_task_plan_list('','',$legal_process_plan_list_data[$count]["process_plan_legal_id"],'','','','','slno');
						if($task_plan_result["status"] == SUCCESS)
						{
							if(get_formatted_date($task_plan_result["data"][count($task_plan_result['data']) - 1]["task_plan_actual_end_date"],"Y-m-d") == "0000-00-00")
							{
								$end_date         = date('Y-m-d');	
								$end_date_display = '';
							}
							else
							{
								$end_date         = $task_plan_result["data"][count($task_plan_result['data']) - 1]["task_plan_actual_end_date"];
								$end_date_display = date('d-M-Y',strtotime($task_plan_result["data"][count($task_plan_result['data']) - 1]["task_plan_actual_end_date"]));
							}
							
							if(get_formatted_date($task_plan_result["data"][0]["task_plan_actual_start_date"],"Y-m-d") == "0000-00-00")
							{
								$start_date         = date('Y-m-d');	
								$start_date_display = '';								
							}
							else
							{
								$start_date         = $task_plan_result["data"][0]["task_plan_actual_start_date"];
								$start_date_display = date('d-M-Y',strtotime($task_plan_result["data"][0]["task_plan_actual_start_date"]));
							}

							$variance = get_date_diff($start_date,$end_date);							
						}
						else
						{
							$start_date       = "";
							$end_date         = "";
							$variance['data'] = "";
							
							$start_date_display = '';
							$end_date_display   = '';
						}
					?>
					<tr>
						<td style="word-wrap:break-word;"><a href="#" id="view_tasks" data-id='<?php echo $legal_process_plan_list_data[$count]["process_plan_legal_id"]; ?>' data-type="single" data-toggle="modal"><?php echo $sl_no; ?></a></td>
						<td style="word-wrap:break-word;"><?php echo $legal_process_plan_list_data[$count]["process_name"]; ?></td>						
						<td style="word-wrap:break-word;"><?php echo $start_date_display; ?></td>
						<td style="word-wrap:break-word;"><?php echo $end_date_display; ?></td>
						<td style="word-wrap:break-word;"><?php echo $variance['data']; ?></td>
					</tr>
					<?php 												
					}														 				 
				}
				?>
				<tr>
				<td colspan="5" style="word-wrap:break-word;"><b>BULK PROCESS</b></td>
				</tr>
				<?php
				if($legal_bulk_process_plan_list["status"] == SUCCESS)
				{		
					$sl_no = 0;
					for($count = 0; $count < count($legal_bulk_process_plan_list_data); $count++)
					{
						$sl_no++;
						
						$task_plan_result = i_get_task_plan_list('','','','','','','','slno',$legal_bulk_process_plan_list_data[$count]['legal_bulk_process_id']);
						if($task_plan_result["status"] == SUCCESS)
						{
							if(get_formatted_date($task_plan_result["data"][count($task_plan_result['data']) - 1]["task_plan_actual_end_date"],"Y-m-d") == "0000-00-00")
							{
								$end_date 		  = '';	
								$end_date_display = '';
							}
							else
							{
								$end_date 		  = $task_plan_result["data"][count($task_plan_result['data']) - 1]["task_plan_actual_end_date"];
								$end_date_display = date('d-M-Y',strtotime($task_plan_result["data"][count($task_plan_result['data']) - 1]["task_plan_actual_end_date"]));
							}
							
							if(get_formatted_date($task_plan_result["data"][0]["task_plan_actual_start_date"],"Y-m-d") == "0000-00-00")
							{
								$start_date         = '';
								$start_date_display = '';								
							}
							else
							{
								$start_date = $task_plan_result["data"][0]["task_plan_actual_start_date"];
								$start_date_display = date('d-M-Y',strtotime($task_plan_result["data"][0]["task_plan_actual_start_date"]));
							}

							$variance = get_date_diff($start_date,$end_date);							
						}
						else
						{
							$start_date       = "";
							$end_date         = "";
							$variance['data'] = "";
						}
					?>
					<tr>
						<td style="word-wrap:break-word;"><a href="#" id="view_tasks" data-id='<?php echo $legal_bulk_process_plan_list_data[$count]['legal_bulk_process_id']; ?>' data-type="bulk" data-toggle="modal"><?php echo $sl_no; ?></a></td>
						<td style="word-wrap:break-word;"><?php echo $legal_bulk_process_plan_list_data[$count]["process_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $start_date_display; ?></td>
						<td style="word-wrap:break-word;"><?php echo $end_date_display; ?></td>
						<td style="word-wrap:break-word;"><?php echo $variance['data']; ?></td>
					</tr>
					<?php 												
					}														 				 
				}								
				?>								
                </tbody>
				</table>										 				
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Task List</h4>
      </div>
      <div class="modal-body" id="div_task_list">        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>        
      </div>
    </div>
  </div>
</div>  
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function confirm_deletion(file_id)
{
	var ok = confirm("Are you sure you want to delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					 window.location = "pending_file_list.php";
				}
			}

			xmlhttp.open("GET", "legal_file_delete.php?file=" + file_id);   // file name where delete code is written
			xmlhttp.send();
		}
	}	
}

</script>
<script>
$(document).on("click", "#view_tasks", function () {
     var processid = $(this).data('id'); 
	 var type      = $(this).data('type'); 
	 $('#div_task_list').text('');
	 $.ajax({
        url: "ajax/get_process_tasks.php",
        type: "POST",
        data: "process=" + processid + "&type=" + type,
        success: function (response) {
            // Task List               		   
			$('#div_task_list').append(response);		   
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }


    });
	 
	 $('.bs-example-modal-lg').modal('show');     
});
</script>
  </body>

</html>
