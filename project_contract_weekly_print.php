<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'pdf_operations.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

// Session Data
$user 		   = $_SESSION["loggedin_user"];
$role 		   = $_SESSION["loggedin_role"];
$loggedin_name = $_SESSION["loggedin_user_name"];

// Temp Data
$alert_type = -1;
$alert = "";

$contract_payment_id = $_REQUEST["contract_payment_id"];

// Temp data
$project_task_actual_boq_search_data = array("payment_id"=>$contract_payment_id,"active"=>"1");
$contract_payment_work_list = i_get_project_payment_contract_mapping($project_task_actual_boq_search_data);

if($contract_payment_work_list["status"] == SUCCESS)
{
	$contract_payment_work_list_data = $contract_payment_work_list["data"];
	$project = $contract_payment_work_list_data[0]["project_master_name"];
	$process = $contract_payment_work_list_data[0]["project_process_master_name"];
	$task = $contract_payment_work_list_data[0]["project_task_master_name"];
	$from_date = date("d-M-Y",strtotime($contract_payment_work_list_data[0]["project_actual_contract_payment_from_date"]));
	$to_date = date("d-M-Y",strtotime($contract_payment_work_list_data[0]["project_actual_contract_payment_to_date"]));
	$bill_date	     = date("d-M-Y",strtotime($contract_payment_work_list_data[0]["project_actual_contract_payment_approved_on"]));
	$bill_no = $contract_payment_work_list_data[0]["project_actual_contract_payment_bill_no"];
	$billing_address = $contract_payment_work_list_data[0]["stock_company_master_name"];
	$vendor = $contract_payment_work_list_data[0]["project_manpower_agency_name"];
	$security_deposit = $contract_payment_work_list_data[0]["project_actual_contract_deposit_amount"];
}
else
{
	$alert = $alert."Alert: ".$contract_payment_work_list["data"];
}

$po_print_format = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Contract Bill</title>
</head>

<body>
<table width="100%" border="1" cellpadding="5" style="border-collapse:collapse; border:1px solid #333;">
  <tr>
    <td colspan="8" rowspan="4" align="center"><h2>'.$billing_address.'</h2></td>
    <td align="center">Project Name</td>
    <td width="1%">:</td>
    <td width="27%">'.$project.'</td>
  </tr>
  <tr>
    <td align="center">Date</td>
    <td>:</td>
    <td>'.$bill_date.'</td>
  </tr>
  <tr>
    <td align="center">Period</td>
    <td>:</td>
    <td>'.$from_date.' to '.$to_date.'</td>
  </tr>
  <tr>
    <td align="center">Contractor</td>
    <td>:</td>
    <td>'.$vendor.'</td>
  </tr>
  <tr>
    <td colspan="8" align="center"><h3>CONTRACT WORK MESAUREMENT BILLING SHEET</h3></td>
    <td align="center">Bill No</td>
    <td>:</td>
    <td>'.$bill_no.'</td>
  </tr>
  <tr>
    <td width="6%" align="center">Sl.no</td>
    <td width="6%" align="center">Work Type</td>
    <td width="9%" align="center">Date</td>
    <td width="9%" align="center">Units</td>
    <td width="9%" align="center">Nos</td>
    <td width="9%" align="center">Length</td>
    <td width="9%" align="center">Breadth</td>
    <td width="9%" align="center">Depth</td>
    <td width="12%" align="center">Measurement</td>
    <td width="12%" align="center">Rate</td>
    <td width="16%" align="center">Total</td>
  </tr>';
  $grand_total = 0;
   for($count = 0; $count < count($contract_payment_work_list_data); $count++)
   {

		$date = date('d-M-Y',strtotime($contract_payment_work_list_data[$count]["project_task_actual_boq_date"]));
		$number = $contract_payment_work_list_data[$count]["project_task_boq_actual_number"];
		$uom = $contract_payment_work_list_data[$count]["stock_unit_name"];
		$length = $contract_payment_work_list_data[$count]["project_task_actual_boq_length"];
		$breadth = $contract_payment_work_list_data[$count]["project_task_actual_boq_breadth"];
		$depth = $contract_payment_work_list_data[$count]["project_task_actual_boq_depth"];
		$measurement = $contract_payment_work_list_data[$count]["project_task_actual_boq_total_measurement"];
		$engineer = $contract_payment_work_list_data[$count]["checked_by"];
		$remarks = $contract_payment_work_list_data[$count]["project_task_actual_boq_remarks"];
		$contract_process = $contract_payment_work_list_data[$count]["project_contract_process_name"];
		$contract_work = $contract_payment_work_list_data[$count]["project_contract_rate_master_work_task"];
		$location = $contract_payment_work_list_data[$count]["project_site_location_mapping_master_name"];
		$rate = $contract_payment_work_list_data[$count]["project_task_actual_boq_amount"];
		$remarks = $contract_payment_work_list_data[$count]["project_task_actual_boq_remarks"];
		$work_type = $contract_payment_work_list_data[$count]["project_task_actual_boq_work_type"];

		$total = $measurement * $rate;
		$actual_total = $contract_payment_work_list_data[$count]["project_task_actual_boq_lumpsum"];
		if(round($total) != round($actual_total))
		{
			$style = ' style="color:red;"';
		}
		else
		{
			$style = '';
		}
		$po_print_format = $po_print_format.'<tr'.$style.'>
		<td>'.($count+1).'</td>
		<td>'.$work_type.'</td>
		<td>'.$date.'</td>
		<td>'.$uom.'</td>
		<td>'.$number.'</td>
		<td>'.number_format((float)$length,2,'.','').'</td>
		<td>'.number_format((float)$breadth,2,'.','').'</td>
		<td>'.number_format((float)$depth,2,'.','').'</td>
		<td>'.number_format((float)$measurement,2,'.','').'</td>
		<td>'.number_format((float)$rate,2,'.','').'</td>
		<td>'.number_format((float)$actual_total).'</td>
	  </tr>
	  <tr>
	  <td colspan="11"><u>DESCRIPTION : </u><u>Process</u> : '.$process .' <u>Task</u> : '.$task.' <u>Contract Process</u> : '.$contract_process.' <u>Contract Task : </u> '.$contract_work.' <u>Location : </u> '.$location.'
	 <u>Remarks : </u> '.$remarks.'</td>
	  </tr>';

	  $grand_total = $grand_total + $actual_total;

 };

 $net_total = $grand_total - $security_deposit;
 $amount_words = convert_num_to_words($net_total);


 $po_print_format = $po_print_format.'<tr><td align="left" colspan="10"><strong>GRAND TOTAL: </strong></td>
 <td align="left"><strong>'.number_format((float)$grand_total,2,'.','').'</strong></td></tr>';

 $po_print_format = $po_print_format.'<tr><td align="left" colspan="10"><strong>Security Deposit: </strong></td>
 <td align="left"><strong>'.number_format((float)$security_deposit,2,'.','').'</strong></td></tr>';

 $po_print_format = $po_print_format.'<tr><td align="left" colspan="10"><strong>Net Amount: </strong></td>
 <td align="left"><strong>'.number_format((float)$net_total,2,'.','').'</strong></td></tr>';

 $po_print_format = $po_print_format.'<tr><td align="left" colspan="11"><strong>Amount in words:'.$amount_words.'</strong></td>';

 $po_print_format = $po_print_format.'
 <tr>
	  <td colsapn="11">&nbsp;</td>
	  </tr>
	  <tr>
	  <td colspan="6" align="center">(Prepared by)</td>
	  <td colspan="5" align="center">(Checked by)</td>
	  </tr>
	  <tr>
	  <td colspan="6" align="center">'.$contract_payment_work_list_data[0]["adder"].'</td>
	  <td colspan="5" align="center">'.$contract_payment_work_list_data[0]["oker"].'</td>
	  </tr>
	  <tr>
	  <td colspan="6" align="center">Executive</td>
	  <td colspan="5" align="center">Project Engineer</td>
	  </tr>
	  <tr>
	  <td colspan="11" align="center">&nbsp;</td>
	  </tr>
	  <tr>
	  <td colspan="6" align="center">(Verified by)</td>
	  <td colspan="4" align="center">(Signature)</td>
	  <td colspan="3" align="center">(Signature)</td>
	  </tr>
	  <tr>
	  <td colspan="6" align="center">'.$contract_payment_work_list_data[0]["approver"].'</td>
	  <td colspan="4" align="center">&nbsp;</td>
	  <td colspan="1" align="center">&nbsp;</td>
	  </tr>
	  <tr>
	  <td colspan="6" align="center">Project Planning Engineer</td>
	  <td colspan="4" align="center">GM - Civil</td>
	  <td colspan="1" align="center">Accounts</td>
	  </tr>
	</table>
	</body>
	</html>';
	/*$mpdf = new mPDF();
	$mpdf->WriteHTML($po_print_format);
	$mpdf->Output('Contract Measurement Sheet.pdf','I');	*/
	echo $po_print_format;
