<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_assigned_enquiry_list.php
CREATED ON	: 08-Sep-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Assigned Enquiries
*/
define('CRM_ASSIGNED_ENQUIRY_LIST_FUNC_ID','104');
/*
TBD: 
*/$_SESSION['module'] = 'CRM Transactions';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];			// Get permission settings for this user for this page	$add_perms_list     = i_get_user_perms($user,'',CRM_ASSIGNED_ENQUIRY_LIST_FUNC_ID,'1','1');	$view_perms_list    = i_get_user_perms($user,'',CRM_ASSIGNED_ENQUIRY_LIST_FUNC_ID,'2','1');	$edit_perms_list    = i_get_user_perms($user,'',CRM_ASSIGNED_ENQUIRY_LIST_FUNC_ID,'3','1');	$delete_perms_list  = i_get_user_perms($user,'',CRM_ASSIGNED_ENQUIRY_LIST_FUNC_ID,'4','1');

	// Query String / Get form Data	
	if(isset($_POST["assigned_enquiry_search_submit"]))
	{
		$assignee        = $_POST["ddl_sales_person"];	
		$start_date_form = $_POST["dt_start_date"];
		$start_date      = $start_date_form." 00:00:00";
		$end_date_form   = $_POST["dt_end_date"];
		$end_date        = $end_date_form." 23:59:59";
	}
	else
	{
		$assignee      = "";	
		$start_date = "";
		$end_date   = "";
	}

	// Temp data
	$alert = "";

	if(($role == 1) || ($role == 5))
	{
		$assigned_by = "";
	}
	else
	{
		$assigned_by = $user;
	}
	
	$enquiry_list = i_get_enquiry_list('','','','','','','','','','','',$assignee,$assigned_by,'','',$start_date,$end_date,'','','','',$user);
	if($enquiry_list["status"] == SUCCESS)
	{
		$enquiry_list_data = $enquiry_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$enquiry_list["data"];
	}
	
	// User List
	$user_list = i_get_user_list('','','','','1','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Enquiry List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Enquiry List (Total Assigned: <span id="lbl_assigned_count"><?php echo count($enquiry_list_data); ?></span>)</h3>
            </div>
			
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="post" id="assigned_enquiry_search" action="crm_assigned_enquiry_list.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_sales_person">
			  <option value="">- - Select Employee - -</option>
			  <?php
			  for($count = 0; $count < count($user_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php if($assignee == $user_list_data[$count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$count]["user_name"]; ?></option>
			  <?php			  
			  }
			  ?>
			  </select>
			  </span>	
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_start_date" value="<?php echo $start_date_form; ?>" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_end_date" value="<?php echo $end_date_form; ?>" />
			  </span>
			  <input type="submit" name="assigned_enquiry_search_submit" />
			  </form>			  
            </div>
            <!-- /widget-header -->            
            <div class="widget-content">
			
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
					<th>SL No</th>					
					<th>Enquiry No</th>					
					<th>Project</th>					
					<th>Name</th>
					<th>Mobile</th>
					<th>Email</th>
					<th>Company</th>
					<th>Source</th>
					<th>Walk In</th>
					<th>Latest Status</th>
					<th>Enquiry Date</th>
					<th>Assigned Date</th>
					<th>Follow Up Count</th>
					<th>Next FollowUp</th>
					<th>Assignee</th>					
					<th>&nbsp;</th>
					<th>&nbsp;</th>					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($enquiry_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					$remove_count = 0;
					for($count = 0; $count < count($enquiry_list_data); $count++)
					{						
						$latest_follow_up_data = i_get_enquiry_fup_list($enquiry_list_data[$count]["enquiry_id"],'','','desc','0','1');
						$follow_up_data = i_get_enquiry_fup_list($enquiry_list_data[$count]["enquiry_id"],'','','','','');
						if($follow_up_data["status"] == SUCCESS)
						{
							$fup_count = count($follow_up_data["data"]);
						}
						else
						{
							$fup_count = 0;
						}
						if($user != $enquiry_list_data[$count]["assigned_to"])
						{
						$sl_no++;
					?>
					<tr>
					<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
					<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[$count]["enquiry_number"]; ?></td>					
					<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[$count]["project_name"]; ?></td>					
					<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[$count]["name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[$count]["cell"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[$count]["email"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[$count]["company"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[$count]["enquiry_source_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php if($enquiry_list_data[$count]["walk_in"] == "1")
					{
						echo "Yes";
					}
					else
					{
						echo "No";
					}?></td>
					<td style="word-wrap:break-word;"><?php echo $latest_follow_up_data["data"][0]["crm_cust_interest_status_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($enquiry_list_data[$count]["added_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($enquiry_list_data[$count]["assigned_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php echo $fup_count; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($latest_follow_up_data["data"][0]["enquiry_follow_up_date"])); ?></td>
					<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[$count]["user_name"]; ?></td>					
					<td style="word-wrap:break-word;"><a href="crm_enquiry_fup_list.php?enquiry=<?php echo $enquiry_list_data[$count]["enquiry_id"]; ?>">Follow Up</a></td>
					<td style="word-wrap:break-word;"><a href="crm_add_site_visit_plan.php?enquiry=<?php echo $enquiry_list_data[$count]["enquiry_id"]; ?>">Site Visit Plan</a><br /><br /><a href="crm_site_visit_plan_list.php?enquiry=<?php echo $enquiry_list_data[$count]["enquiry_id"]; ?>">Site Visit Plan List</a></td>
					</tr>
					<?php
					}
					else
					{
						$remove_count++;
					}
					}
				}
				else
				{
				?>
				<td colspan="14">No enquiries!</td>
				<?php
				}
				 ?>	
				 <script>
				 document.getElementById("lbl_assigned_count").innerHTML = <?php echo count($enquiry_list_data) - $remove_count; ?>;
				 </script>

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>