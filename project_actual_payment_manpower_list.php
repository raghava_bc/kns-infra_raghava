<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_actual_payment_manpower_list.php
CREATED ON	: 07-Dec-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/
/* DEFINES - START */
define('PROJECT_ACTUAL_PAYMENT_MANPOWER_LIST_FUNC_ID', '256');
/* DEFINES - END */
/*
TBD:
*/
$_SESSION['module'] = 'Projectmgmnt';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    $alert_type = -1;
    $alert = "";

    // Get permission settings for this user for this page
    $view_perms_list   = i_get_user_perms($user, '', PROJECT_ACTUAL_PAYMENT_MANPOWER_LIST_FUNC_ID, '2', '1');
    $edit_perms_list   = i_get_user_perms($user, '', PROJECT_ACTUAL_PAYMENT_MANPOWER_LIST_FUNC_ID, '3', '1');
    $approve_perms_list= i_get_user_perms($user, '', PROJECT_ACTUAL_PAYMENT_MANPOWER_LIST_FUNC_ID, '6', '1');

    if (isset($_GET["page"])) {
        $page = $_GET["page"];
    } else {
        $page = 1;
    }

    $start = (string)($page - 1) * 20;
    $limit = 20;


    $search_project   	 = "";
    if (isset($_REQUEST["search_project"])) {
        $search_project   = $_REQUEST["search_project"];
    } else {
        $search_project   = "";
    }

    $search_vendor   	 = "";

    if (isset($_REQUEST["search_vendor"])) {
        $search_vendor   = $_REQUEST["search_vendor"];
    } else {
        $search_vendor = "";
    }

    // Get Project  Payment ManPower modes already added
    $project_actual_payment_manpower_search_data = array("start"=>$start,"limit"=>$limit,"active"=>'1',"status"=>"Pending","vendor_id"=>$search_vendor);
    $project_actual_payment_manpower_list = i_get_project_actual_payment_manpower($project_actual_payment_manpower_search_data);
    if ($project_actual_payment_manpower_list['status'] == SUCCESS) {
        $project_actual_payment_manpower_list_data = $project_actual_payment_manpower_list['data'];
    }

    // Machine Planning data
    $project_actual_payment_manpower_search_data = array("start"=>($page * 10),"limit"=>$limit,"active"=>'1',"status"=>"Pending");
    $actual_payment_manpower_list_next = i_get_project_actual_payment_manpower($project_actual_payment_manpower_search_data);
    if ($actual_payment_manpower_list_next["status"] == SUCCESS) {
        $show_next = true;
    } else {
        $show_next = false;
    }

    // Project data
    $project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
    $project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
    if ($project_management_master_list["status"] == SUCCESS) {
        $project_management_master_list_data = $project_management_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_management_master_list["data"];
    }

    // Get Project manpower_agency Master modes already added
    $project_manpower_agency_search_data = array("active"=>'1');
    $project_manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
    if ($project_manpower_agency_list['status'] == SUCCESS) {
        $project_manpower_agency_list_data = $project_manpower_agency_list['data'];
    } else {
    }
} else {
    header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Weekly ManPower Payment List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>


<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;  position: relative;">

          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Weekly ManPower Payment List</h3>
            </div>
			<div class="widget-header" style="height:80px; padding-top:10px;">
			  <form method="post" id="file_search_form" action="project_actual_payment_manpower_list.php">

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_vendor">

			  <option value="">- - Select Vendor - -</option>
			  <?php
              for ($project_count = 0; $project_count < count($project_manpower_agency_list_data); $project_count++) {
                  ?>
			  <option value="<?php echo $project_manpower_agency_list_data[$project_count]["project_manpower_agency_id"]; ?>" <?php if ($search_vendor == $project_manpower_agency_list_data[$project_count]["project_manpower_agency_id"]) {
                      ?> selected="selected" <?php
                  } ?>><?php echo $project_manpower_agency_list_data[$project_count]["project_manpower_agency_name"]; ?></option>
			  <?php
              }
              ?>
			  </select>
			  </span>

			  <input type="submit" name="file_search_submit" />
			  </form>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php
            if ($view_perms_list['status'] == SUCCESS) {
                ?>
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th style="width:1%">SL No</th>
					<th style="width:4%">Project</th>
					<th style="width:5%">Bill No</th>
					<th style="width:2%">From Date</th>
					<th style="width:2%">To Date</th>
					<th style="width:5%">Vendor Name</th>
					<th style="width:4%">Men Hrs</th>
					<th style="width:4%">Women Hrs</th>
					<th style="width:4%">Mason Hrs</th>
					<th style="width:2%">Total Hrs</th>
					<th style="width:3%">Amount</th>
					<th style="width:2%">Remarks</th>
					<th style="width:6%">Added By</th>
					<th style="width:3%">Added On</th>
					<th colspan="3" style="width:6%" style="text-align:center;">Actions</th>

				</tr>
				</thead>
				<tbody>
				<?php
                if ($project_actual_payment_manpower_list["status"] == SUCCESS) {
                    $sl_no = 0;
                    for ($count = 0; $count < count($project_actual_payment_manpower_list_data); $count++) {
                        $sl_no++;
                        //Get ManPower  Rate
                        $man_power_men_rate = 0;
                        $man_power_women_rate = 0;
                        $man_power_mason_rate = 0;


                        $man_power_search_data = array("man_power_id"=>$project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_id"]);
                        $actual_manpower_list = i_get_man_power_list($man_power_search_data);
                        if ($actual_manpower_list["status"] == SUCCESS) {
                            $man_power_men_rate = $actual_manpower_list["data"][0]["project_task_actual_manpower_men_rate"];
                            $man_power_women_rate = $actual_manpower_list["data"][0]["project_task_actual_manpower_women_rate"];
                            $man_power_mason_rate = $actual_manpower_list["data"][0]["project_task_actual_manpower_mason_rate"];
                        }

                        /*$project_man_power_rate_search_data = array("vendor_id"=>$project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_vendor_id"]);
                        $manpower_rate_list = i_get_project_man_power_rate_master($project_man_power_rate_search_data);
                        if($manpower_rate_list["status"] == SUCCESS)
                        {
                            for($rate_count =0 ; $rate_count < count($manpower_rate_list["data"]) ; $rate_count++ )
                            {
                                $manpower_rate_list_data = $manpower_rate_list["data"];
                                if($manpower_rate_list_data[$rate_count]["project_man_power_type_id"] == 1)
                                {
                                    $man_power_men_rate = $manpower_rate_list_data[$rate_count]["project_man_power_rate_cost_per_hours"];
                                }
                                if($manpower_rate_list_data[$rate_count]["project_man_power_type_id"] == 2)
                                {
                                    $man_power_women_rate = $manpower_rate_list_data[$rate_count]["project_man_power_rate_cost_per_hours"];
                                }
                                if($manpower_rate_list_data[$rate_count]["project_man_power_type_id"] == 3)
                                {
                                    $man_power_mason_rate = $manpower_rate_list_data[$rate_count]["project_man_power_rate_cost_per_hours"];
                                }
                            }
                        }*/

                        $men = $project_actual_payment_manpower_list_data[$count]["project_actual_payment_men_hrs"]/8;
                        $women = $project_actual_payment_manpower_list_data[$count]["project_actual_payment_women_hrs"]/8;
                        $mason = $project_actual_payment_manpower_list_data[$count]["project_actual_payment_mason_hrs"]/8;
                        $others = $project_actual_payment_manpower_list_data[$count]["project_actual_payment_others_hrs"]/8;

                        // Get project name
                        $project_payment_manpower_mapping_search_data = array('payment_id'=>$project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_id"],"project"=>$search_project);
                        $project_name_sresult = i_get_project_payment_manpower_mapping($project_payment_manpower_mapping_search_data);
                        if ($project_name_sresult['status'] == SUCCESS) {
                            $project_name = $project_name_sresult['data'][0]['project_master_name'];
                        } else {
                            $project_name = 'NOT VALID';
                        } ?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_name; ?></td>
					<td><?php echo $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_bill_no"]; ?></td>
					<td><?php echo date("d-M-Y", strtotime($project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_from_date"])); ?></td>
					<td><?php echo date("d-M-Y", strtotime($project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_to_date"])); ?></td>
					<td><?php echo $project_actual_payment_manpower_list_data[$count]["project_manpower_agency_name"]; ?></td>
					<td><?php echo $project_actual_payment_manpower_list_data[$count]["project_actual_payment_men_hrs"]; ?>&nbsp;&nbsp;Men : <?php echo $men ; ?>Rate :<?php echo $man_power_men_rate ; ?>Total :<?php echo($men* $man_power_men_rate) ; ?></td>
					<td><?php echo $project_actual_payment_manpower_list_data[$count]["project_actual_payment_women_hrs"]; ?>&nbsp;&nbsp;Women : <?php echo $women ; ?>Rate :<?php echo $man_power_women_rate ; ?>Total :<?php echo($women* $man_power_women_rate) ; ?> </td>
					<td><?php echo $project_actual_payment_manpower_list_data[$count]["project_actual_payment_mason_hrs"]; ?>&nbsp;&nbsp;Mason : <?php echo $mason ; ?>Rate :<?php echo $man_power_mason_rate ; ?>Total :<?php echo($mason* $man_power_mason_rate) ; ?> </td>
					<td><?php echo $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_hrs"]; ?></td>
					<td><?php echo $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_amount"]; ?></td>
					<td><?php echo $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_remarks"]; ?></td>
					<td><?php echo $project_actual_payment_manpower_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y", strtotime($project_actual_payment_manpower_list_data[$count][
                    "project_actual_payment_manpower_added_on"])); ?></td>
					<!--<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_project_edit_payment_manpower(<?php echo $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_id"]; ?>);">Edit</a></div></td>
					<td><?php if (($project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_active"] == "1")) {
                        ?><a href="#" onclick="return project_delete_payment_manpower(<?php echo $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_id"]; ?>);">Delete</a><?php
                    } ?></td>-->

					<td style="word-wrap:break-word;"><?php if ($view_perms_list['status'] == SUCCESS) {
                        ?><a style="padding-right:10px" href="#" onclick="return go_to_project_view_payment_manpower(<?php echo $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_id"]; ?>);">View</a><?php
                    } ?></div></td>

					<td><?php if (($project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_status"] == "Pending") && ($approve_perms_list['status'] == SUCCESS)) {
                        ?><a href="#" onclick="return project_approve_payment_manpower('<?php echo $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_id"]; ?>','<?php echo $page ; ?>');">Approve</a><?php
                    } ?></td>

					<td><?php if (($view_perms_list['status'] == SUCCESS)) {
                        ?><a href="#" target="_blank" onclick="return go_to_manpower_print(<?php echo $project_actual_payment_manpower_list_data[$count]["project_actual_payment_manpower_id"]; ?>);">Print</a><?php
                    } ?></td>
					</tr>
					<?php
                    }
                    $next_page = $page + 1;
                    $prev_page = $page - 1;
                } else {
                    ?>
				<td colspan="17">No data added yet!</td>

				<?php
                } ?>

                </tbody>
              </table>
			  </br>
			  </br>
			  <?php
                if ($page > 1) {
                    ?>
				<a href="project_actual_payment_manpower_list.php?page=<?php echo $prev_page; ?>&search_vendor=<?= $search_vendor; ?>" class="pull-left">Prev Page</a>
				<?php
                } ?>
				<?php
                if ($show_next == true) {
                    ?>
				<a href="project_actual_payment_manpower_list.php?page=<?php echo $next_page; ?>&search_vendor=<?= $search_vendor; ?>" class="pull-right">Next Page</a>
				<?php
                } ?>
				<br/>
			  <?php
            } else {
                echo 'You are not authorized to view this page';
            }
            ?>
            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_payment_manpower(manpower_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_actual_payment_manpower_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/project_delete_actual_payment_manpower.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("manpower_id=" + manpower_id + "&action=0");
		}
	}
}
function project_approve_payment_manpower(manpower_id,page)
{
	var ok = confirm("Are you sure you want to Approve?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					alert(xmlhttp.responseText);
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_actual_payment_manpower_list.php?page=" + page;
					}
				}
			}

			xmlhttp.open("POST", "project_approve_actual_payment_manpower.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("manpower_id=" + manpower_id + "&action=Approved");
		}
	}
}
function go_to_project_edit_payment_manpower(manpower_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_edit_actual_payment_manpower.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","manpower_id");
	hiddenField1.setAttribute("value",manpower_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}

function go_to_project_payment_bill_no(manpower_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_add_bill_no.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","manpower_id");
	hiddenField1.setAttribute("value",manpower_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}
function go_to_project_view_payment_manpower(payment_manpower_id,vendor_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_view_manpower_details.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","payment_manpower_id");
	hiddenField1.setAttribute("value",payment_manpower_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","vendor_id");
	hiddenField2.setAttribute("value",vendor_id);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);

	document.body.appendChild(form);
    form.submit();
}

function go_to_manpower_print(manpower_payment_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_manpower_print.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","manpower_payment_id");
	hiddenField1.setAttribute("value",manpower_payment_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}

</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>
  </body>

</html>
