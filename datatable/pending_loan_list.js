$(document).ready(function() {

  var table = $('#example').dataTable({
    "sScrollX": "100%",
    "sScrollXInner": "110%",
    "bScrollCollapse": true,
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": 'datatable/pending_loan_list_datatable.php',
    "iDisplayLength": 10,
    "sScrollY": "50%",
    "bInfo": true,
    "iTabIndex": 1,
    "lengthChange": false,
    language: {
      search: "_INPUT_",
      searchPlaceholder: "Search...",
      "infoFiltered": ""
    },
    "fnServerParams": function(aoData) {
      aoData.push({
        "name": "table",
        "value": "pending_loan_list"
      });
      aoData.push({
        "name": "search_department",
        "value": getVars('search_department')
      });
    },
    "aaSorting": [
      // [1, 'desc']
    ],
    "fnDrawCallback": function(oSettings) {
      /* Need to redo the counters if filtered or sorted */
      if (oSettings.bSorted || oSettings.bFiltered || oSettings.iDraw > 1) {
        for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
          $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + oSettings._iDisplayStart + 1);
        }
      }
    },
    "aoColumnDefs": [{
        "bSortable": false,
        "aTargets": [0]
      },
      {
        "bSortable": false,
        "aTargets": [1],
        "mData": `department_name`
      },
      {
        "bSortable": false,
        "aTargets": [2],
        "mData": 'user_name'
      },
      {
        "bSortable": false,
        "aTargets": [3],
        "mData": `total_amount`
      },
      {
        "bSortable": false,
        "aTargets": [4],
        "mRender": function(data, type, full) {
          return full['total_tenure'] + ' Months';
        }
      },
      {
        "bSortable": false,
        "aTargets": [5],
        "mRender": function(data, type, full) {
          return full['pending_amount'];
        }
      },
      {
        "bSortable": false,
        "aTargets": [6],
        "mRender": function(data, type, full) {
          monthly_amount = parseInt(full['total_amount'] / parseInt(full['total_tenure']));
          pending_tenure = (parseInt(full['pending_amount']) / monthly_amount);
          return Math.ceil(pending_tenure);
        }
      },
      {
        "bSortable": false,
        "aTargets": [7],
        "mRender": function(data, type, full) {
          monthly_amount = parseInt(full['total_amount'] / parseInt(full['total_tenure']));
          pending_tenure = (parseInt(full['pending_amount']) / monthly_amount);
          return Math.ceil(monthly_amount);
        }
      },
      {
        "bSortable": false,
        "aTargets": [8],
        "mRender": function(data, type, full) {
          return full['remarks']
        }
      },
      // {
      //   "bSortable": false,
      //   "aTargets": [9],
      //   "mRender": function(data, type, full) {
      //     return '<a href="#" id="edit">Edit</a>';
      //   }
      // },
    ]
  });

  $('#example tbody').on('click', 'tr', function(event) {

    if (event.target.tagName == 'A' && event.target.id == 'edit') {

      var rowData = table.api().row(this).data();
      go_to_persona_edit_list(rowData['user_id']);
    }
    // else if (event.target.tagName == 'A' && event.target.id == 'delete') {
    //   project_delete_task_actual_manpower(table, table.api().row(this).data()['project_task_actual_manpower_id']);
    //
    // } else if (event.target.tagName == 'A' && event.target.id == 'ok') {
    //
    //   var rowData = table.api().row(this).data();
    //   project_check_task_actual_manpower(table, rowData['project_task_actual_manpower_id'], rowData['project_task_actual_manpower_task_id']);
    //
    // } else if (event.target.tagName == 'A' && event.target.id == 'approve') {
    //   var rowData = table.api().row(this).data();
    //   project_approve_task_actual_manpower(table, rowData['project_task_actual_manpower_id'], rowData['project_task_actual_manpower_task_id']);
    // }

  });
});

// get hidden values saved from filters
function getVars(key) {
  var element = document.getElementById(key);
  console.log('getVars ',  key, element);
  if (element) {
    return element.value;
  } else {
    return '';
  }
}

// set search filters' as hidden value
function setVars(department_id) {
console.log('setVars ', department_id);

  if(department_id) {
    var hiddenField1 = document.createElement("input");
    hiddenField1.setAttribute("type", "hidden");
    hiddenField1.setAttribute("id", 'search_department');
    hiddenField1.setAttribute("value", department_id);
    document.body.appendChild(hiddenField1);
  }
}

function go_to_persona_edit_list(user_id) {
  var form = document.createElement("form");
  form.setAttribute("method", "get");
  form.setAttribute("action", "edit_pending_loan_form.php");
  form.setAttribute("target", "blank")

  var hiddenField1 = document.createElement("input");
  hiddenField1.setAttribute("type", "hidden");
  hiddenField1.setAttribute("name", "user_id");
  hiddenField1.setAttribute("value", user_id);

  form.appendChild(hiddenField1);

  document.body.appendChild(form);
  form.submit();
}
