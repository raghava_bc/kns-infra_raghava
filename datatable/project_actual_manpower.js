var table;

function drawTable() {
  table.api().draw();
}
var columns = [{
    className: 'noVis',
    "orderable": false,
    "data": function() {
      return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
    }
  },
  {
    "orderable": false,
    "data": `project_manpower_agency_name`
  },
  {
    "orderable": false,
    "data": function(data, type, full) {
      return data.project_master_name;
    },
  },
  {
    "orderable": false,
    "data": 'project_process_master_name'
  },
  {
    "orderable": false,
    "data": `project_task_master_name`
  },
  {
    "orderable": false,
    "data": function(data, type, full) {
      if (type !== 'display') {
        return '';
      }
      if (data.project_task_actual_manpower_road_id == 'No Roads') {
        return 'No Roads';
      }
      return data.project_site_location_mapping_master_name;
    }
  },
  {
    "orderable": false,
    "data": `project_task_actual_manpower_work_type`
  },
  {
    "orderable": false,
    "data": function(data, type, full) {
      if (type !== 'display') {
        return '';
      }

      if (data.project_task_actual_manpower_work_type === 'Regular') {
        return data.project_task_actual_manpower_completion_percentage;
      } else {
        return data.project_task_actual_manpower_rework_completion;
      }
    }
  },
  {
    "orderable": false,
    "data": `project_task_actual_manpower_completed_msmrt`
  },
  // Men
  {
    "data": function(data, type, full) {
      if (type !== 'display') {
        return '';
      }
      return data.project_task_actual_manpower_no_of_men;
    },
    "orderable": false
  },

  {
    "data": function(data, type, full) {
      if (type !== 'display') {
        return '';
      }
      return data.project_task_actual_manpower_men_rate;
    },
    "orderable": false
  },

  {
    "data": function(data, type, full) {
      if (type !== 'display') {
        return '';
      }
      return data.project_task_actual_manpower_no_of_men / 8;
    },
    "orderable": false
  },
  // Women
  {
    "data": function(data, type, full) {
      if (type !== 'display') {
        return '';
      }
      return data.project_task_actual_manpower_no_of_women;
    },
    "orderable": false
  },
  {
    "data": function(data, type, full) {
      if (type !== 'display') {
        return '';
      }
      return data.project_task_actual_manpower_women_rate;
    },
    "orderable": false
  },
  {
    "data": function(data, type, full) {
      if (type !== 'display') {
        return '';
      }
      return data.project_task_actual_manpower_no_of_women / 8;
    },
    "orderable": false
  },
  // Mason
  {
    "data": function(data, type, full) {
      if (type !== 'display') {
        return '';
      }
      return data.project_task_actual_manpower_no_of_mason;
    },
    "orderable": false
  },
  {
    "data": function(data, type, full) {
      if (type !== 'display') {
        return '';
      }
      return data.project_task_actual_manpower_mason_rate;
    },
    "orderable": false
  },
  {
    "data": function(data, type, full) {
      if (type !== 'display') {
        return '';
      }
      return data.project_task_actual_manpower_no_of_mason / 8;
    },
    "orderable": false
  },
  {
    // No of People
    "orderable": false,
    "data": function(data, type, full) {
      if (type !== 'display') {
        return '';
      }
      return parseFloat(data.project_task_actual_manpower_no_of_men / 8) + parseFloat(data.project_task_actual_manpower_no_of_women / 8) + parseFloat(data.project_task_actual_manpower_no_of_mason / 8);
    }
  },
  {
    // Amount
    "orderable": false,
    "data": function(data, type, full) {
      if (type !== 'display') {
        return '';
      }
      return (parseFloat(data.project_task_actual_manpower_no_of_men) * parseFloat(data.project_task_actual_manpower_men_rate)) + (parseFloat(data.project_task_actual_manpower_no_of_women) * parseFloat(data.project_task_actual_manpower_women_rate)) + (parseFloat(data.project_task_actual_manpower_no_of_mason) * parseFloat(data.project_task_actual_manpower_mason_rate));
    }
  },
  {
    // Remarks
    "orderable": false,
    "data": `project_task_actual_manpower_remarks`
  },
  {
    "orderable": false,
    "data": `user_name`
  },
  {
    "orderable": true,
    "orderData": [25, 26],
    "data": function(data, type, full) {
      if (type !== 'display') {
        return '';
      }
      return moment(data.project_task_actual_manpower_added_on).format('DD-MM-YYYY');
    }
  },
  {
    "orderable": true,
    "orderData": [26, 25],
    "data": function(data, type, full) {
      if (type !== 'display') {
        return '';
      }
      return moment(data.project_task_actual_manpower_date).format('DD-MM-YYYY');
    }
  },
  {
    "orderable": false,
    "data": function(data, type, full) {
      if (type !== 'display') {
        return '';
      }
      return '<a href="#" id="edit">Edit</a>';
    }
  },
  {
    "orderable": false,
    "data": function(data, type, full) {
      if (type !== 'display') {
        return '';
      }
      return '<a href="#" id="delete">Delete</a>';
    }
  },
  {
    "orderable": false,
    "data": function(data, type, full) {
      if (type !== 'display') {
        return '';
      }
      var percentage;
      if (data.project_task_actual_manpower_work_type == 'Regular') {
        percentage = parseFloat(data.project_task_actual_manpower_completion_percentage);
      } else {
        percentage = parseFloat(data.project_task_actual_manpower_rework_completion);
      }
      if (data.project_task_actual_manpower_check_status == 0 && percentage > 0) {
        if (window.permissions.ok) {
          return '<a href="#" id="ok">OK</a>';
        }
        return 'Ok Pending';
      } else if (data.project_task_actual_manpower_check_status == 1 && data.project_task_actual_manpower_display_status == 'not approved') {
        if (window.permissions.aprove) {
          return '<a href="#" id="approve">Approve</a>';
        }
        return 'Approval Pending';
      } else if (percentage == 0) {
        return 'Enter	completed msrmnt'
      }
    }
  },
];
if (!window.permissions.delete) {
  columns.splice(25, 1);
}

$(document).ready(function() {
  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_actual_manpower_datatable.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "project_task_actual_manpower_list";
      aoData.search_project = $('#search_project').val();
      aoData.search_vendor = $('#search_vendor').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    "fnCreatedRow": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      $(nRow).attr('data-row-index', iDisplayIndex);
    },
    buttons: [{
      extend: 'colvis',
      columns: ':not(.noVis)'
    }],
    fixedColumns: {
      leftColumns: 3,
      rightColumns: ((window.permissions.delete) ? 4 : 3)
    },
    scrollX: true,
    "columns": columns,
  });

  $('#example tbody').on('click', 'tr', function(event) {
    var rowData = table.api().row(this).data();
    if (event.target.tagName == 'A' && event.target.id == 'edit') {
      go_to_project_edit_task_actual_manpower(rowData['project_task_actual_manpower_id'], rowData['project_task_actual_manpower_task_id'], rowData['project_task_actual_manpower_road_id']);
    } else if (event.target.tagName == 'A' && event.target.id == 'delete') {
      project_delete_task_actual_manpower(table, rowData['project_task_actual_manpower_id']);
    } else if (event.target.tagName == 'A' && event.target.id == 'ok') {
      project_check_task_actual_manpower(table, rowData['project_task_actual_manpower_id'], rowData['project_task_actual_manpower_task_id']);
    } else if (event.target.tagName == 'A' && event.target.id == 'approve') {
      project_approve_task_actual_manpower(table, rowData['project_task_actual_manpower_id'], rowData['project_task_actual_manpower_task_id']);
    }
  });
});

function project_approve_task_actual_manpower(table, man_power_id, task_id) {
  var ok = confirm("Are you sure you want to Approve?");

  if (ok) {

    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        alert(xmlhttp.responseText);
        if (xmlhttp.responseText != "SUCCESS") {
          document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
          document.getElementById("span_msg").style.color = "red";
        } else {
          table.fnDraw(false);
        }
      }
    }

    xmlhttp.open("POST", "project_approve_man_power.php"); // file name where delete code is written
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("man_power_id=" + man_power_id + "&task_id=" + task_id + "&action=approved");
  }
}

function project_delete_task_actual_manpower(table, man_power_id) {
  var ok = confirm("Are you sure you want to Delete?");

  if (ok) {

    if (window.XMLHttpRequest) {
      xmlhttp = new XMLHttpRequest();
    } else {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        if (xmlhttp.responseText != "SUCCESS") {
          document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
          document.getElementById("span_msg").style.color = "red";
        } else {
          table.fnDraw(false);
        }
      }
    }

    xmlhttp.open("POST", "project_delete_task_actual_manpower.php");
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("man_power_id=" + man_power_id + "&action=0");
  }
}

function project_check_task_actual_manpower(table, man_power_id, task_id) {
  var ok = confirm("Are you sure you want to Ok?")
  if (ok) {

    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        if (xmlhttp.responseText != "SUCCESS") {
          document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
          document.getElementById("span_msg").style.color = "red";
        } else {
          table.fnDraw(false);
        }
      }
    }
    xmlhttp.open("POST", "project_check_man_power.php"); // file name where delete code is written
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("man_power_id=" + man_power_id + "&task_id=" + task_id + "&action=approved");
  }
}

function go_to_project_edit_task_actual_manpower(man_power_id, task_id, road_id) {
  var form = document.createElement("form");
  form.setAttribute("method", "get");
  form.setAttribute("action", "project_edit_task_actual_manpower.php");
  form.setAttribute("target", "_blank");

  var hiddenField1 = document.createElement("input");
  hiddenField1.setAttribute("type", "hidden");
  hiddenField1.setAttribute("name", "man_power_id");
  hiddenField1.setAttribute("value", man_power_id);

  var hiddenField2 = document.createElement("input");
  hiddenField2.setAttribute("type", "hidden");
  hiddenField2.setAttribute("name", "task_id");
  hiddenField2.setAttribute("value", task_id);

  var hiddenField3 = document.createElement("input");
  hiddenField3.setAttribute("type", "hidden");
  hiddenField3.setAttribute("name", "road_id");
  hiddenField3.setAttribute("value", road_id);

  form.appendChild(hiddenField1);
  form.appendChild(hiddenField2);
  form.appendChild(hiddenField3);

  document.body.appendChild(form);
  form.submit();
}