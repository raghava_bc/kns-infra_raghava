<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: task_list.php
CREATED ON	: 01-April-2017
CREATED BY	: Lakshmi
PURPOSE     : List of Task Plans for a particular process ID
*/
/*
TBD: 
1. Date display and calculation
2. Session management
3. Linking Tasks
*/
$_SESSION['module'] = 'Projectmgmnt';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	if(isset($_GET['process_id']))
	{
		$process_id = $_GET['process_id'];
	}
	else
	{
		$process_id = "";
	}
	if(isset($_REQUEST['project_id']))
	{
		$project_id = $_REQUEST['project_id'];
	}
	else
	{
		$project_id = "";
	}
	$result = array();
	$result['data'] = array();
	$result['data'][0] = array();

	$project_plan_process_search_data = array("active"=>'1',"project_id"=>$project_id);
	$project_plan_process_list = i_get_project_plan_process($project_plan_process_search_data);
	if($project_plan_process_list["status"] == SUCCESS)
	{
		$project_plan_process_list_data = $project_plan_process_list["data"];
		$overall_task_count = 0;
		for($process_count = 0 ; $process_count < count($project_plan_process_list_data); $process_count++)
		{
			
			// Get list of task plans for this process plan
			$project_process_task_search_data = array("active"=>'1',"process_id"=>$project_plan_process_list_data[$process_count]["project_plan_process_id"]);
			$project_process_task_list = i_get_project_process_task($project_process_task_search_data);
			if($project_process_task_list["status"] == SUCCESS)
			{
				$project_process_task_list_data = $project_process_task_list["data"];
				for($task_count = 0; $task_count < count($project_process_task_list_data); $task_count++)
				{	
					if($project_process_task_list_data[$task_count]["project_process_task_location_id"] != "No Roads")
					{
						$road_name = $project_process_task_list_data[$task_count]["project_site_location_mapping_master_name"];
					}
					else
					{
						$road_name= "No Roads";
					}
					$result['data'][0][$overall_task_count][0] = $project_process_task_list_data[$task_count]["project_process_master_name"];
					$result['data'][0][$overall_task_count][1] = $project_process_task_list_data[$task_count]["project_task_master_name"];
					$result['data'][0][$overall_task_count][2] = $project_process_task_list_data[$task_count]["project_process_task_id"];
					$result['data'][0][$overall_task_count][3] = "";
					$result['data'][0][$overall_task_count][4] = "";
					$result['data'][0][$overall_task_count][5] = $road_name;
					$result['data'][0][$overall_task_count][6] = "";
					$result['data'][0][$overall_task_count][7] = "";
					$result['data'][0][$overall_task_count][8] = "";
					$result['data'][0][$overall_task_count][9] = "";
					$result['data'][0][$overall_task_count][10] = "";
					$result['data'][0][$overall_task_count][11] = "";
					$result['data'][0][$overall_task_count][12] = "";
					$result['data'][0][$overall_task_count][13] = "";
					$result['data'][0][$overall_task_count][14] = "";
					$result['data'][0][$overall_task_count][15] = $project_process_task_list_data[$task_count]["project_task_master_id"];
					$overall_task_count++;
				}
				
			}
			else
			{
				//
			}
		}
		
	}
	else
	{
		//Do Nothing
	}
	
	echo (json_encode($result));
}
else
{
	header("location:login.php");
}	
?>