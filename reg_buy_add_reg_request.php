<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 08-Feb-2017
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'registration'.DIRECTORY_SEPARATOR.'reg_buy_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	// Capture the form data
	if(isset($_POST["add_reg_request_submit"]))
	{
		$village            = $_POST["ddl_village"];
		$survey_no          = $_POST["survey_no"];
		$extent             = $_POST["num_extent"];
		$land_lard          = $_POST["land_lard"];
		$request_date       = $_POST["request_date"];
		$request_for        = $_POST["ddl_request_for"];
		$remarks            = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($village != "") && ($survey_no != "") && ($extent != "") && ($land_lard != "") && ($request_date != "") &&($request_for != ""))
		{
			$reg_buy_request_iresult =  i_add_reg_buy_request($village,$survey_no,$extent,$land_lard,$request_date,$request_for,$remarks,$user);
			
			if($reg_buy_request_iresult["status"] == SUCCESS)				
			{	
				$alert_type = 1;
			}
			
			$alert = $reg_buy_request_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get Reg buy Village added
	$reg_buy_village_master_search_data = array("active"=>'1');
	$reg_buy_village_master_list = i_get_reg_buy_village_master($reg_buy_village_master_search_data);
	if($reg_buy_village_master_list['status'] == SUCCESS)
	{
		$reg_buy_village_master_list_data = $reg_buy_village_master_list['data'];
	}	
	 else
	{
		$alert = $reg_buy_village_master_list["data"];
		$alert_type = 0;
	}
			
   // Get BD Own Account Master already added
	$bd_account_list = i_get_account_list('','');
	if($bd_account_list['status'] == SUCCESS)
	{
		$bd_account_list_data = $bd_account_list['data'];
	}
	else
	{
		
	}
}
else
{
	header("location:login.php");
}		
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Reg Buy Add Reg Request</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Reg Buy Add Reg Request</h3><span style="float:right; padding-right:20px;"><a href="reg_buy_reg_request_list.php">Reg Request List</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Reg Buy Add Reg Request</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="reg_buy_add_reg_request_form" class="form-horizontal" method="post" action="reg_buy_add_reg_request.php">
									<fieldset>
									
									<div class="control-group">											
										<label class="control-label" for="ddl_village">Village*</label>
										<div class="controls">
											<select name="ddl_village"  class="span6" required>
											<option value="">- - Select Village - -</option>
											<?php
											for($count = 0; $count < count($reg_buy_village_master_list_data); $count++)
											{
											?>
											<option value="<?php echo $reg_buy_village_master_list_data[$count]["reg_buy_village_master_id"]; ?>"><?php echo $reg_buy_village_master_list_data[$count]["reg_buy_village_master_name"]; ?></option>
											<?php
											}
											?>
											</select>
										</div> <!-- /controls -->					
										</div> <!-- /control-group -->

									    <div class="control-group">											
											<label class="control-label" for="survey_no">Survey No*</label>
											<div class="controls">
												<input type="text" class="span6" name="survey_no" placeholder="Survey No">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="num_extent">Extent*</label>
											<div class="controls">
												<input type="number" class="span6" min="0.01" step="0.01" name="num_extent" placeholder="Extent(in Guntas)">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="land_lard">Land Lord*</label>
											<div class="controls">
												<input type="text" class="span6" name="land_lard" placeholder="Name Of The Land Lord">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
									
										<div class="control-group">											
											<label class="control-label" for="ddl_request_for">Registrant Name*</label>
											<div class="controls">
												<select name="ddl_request_for"  class="span6" required>
												<option value="">- - Select Registrant - -</option>
												<?php
												for($count = 0; $count < count($bd_account_list_data); $count++)
												{
												?>
												<option value="<?php echo $bd_account_list_data[$count]["bd_own_accunt_master_id"]; ?>"><?php echo $bd_account_list_data[$count]["bd_own_account_master_account_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
											</div> <!-- /control-group -->
											
										<div class="control-group">											
											<label class="control-label" for="request_date">Plan Date*</label>
											<div class="controls">
												<input type="date" class="span6" name="request_date" placeholder="Plan Date">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_reg_request_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
