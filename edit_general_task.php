<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Initialization
	$alert            = "";
	$alert_type       = -1; // No alert
	$assigned_to      = "";
	$gen_task_type    = "";
	$department       = "";
	$gen_task_details = "";
	$planned_end      = "";

	// Query String
	if(isset($_GET["task"]))
	{
		$task_id = $_GET["task"];
	}
	else
	{
		$task_id = "";
	}
	if(isset($_REQUEST["source"]))
	{
		$source = $_REQUEST["source"];
	}
	else
	{
		$source = "";
	}
	
	if(isset($_REQUEST["search_user"]))
	{
		$search_user = $_REQUEST["search_user"];
	}
	else
	{
		$search_user = "";
	}
	
	if(isset($_REQUEST["search_project"]))
	{
		$search_project = $_REQUEST["search_project"];
	}
	else
	{
		$search_project = "";
	}
	// Nothing here	

	if(isset($_REQUEST["edit_general_task_submit"]))
	{
		// Capture all form data
		$task_id             = $_REQUEST["hd_task_id"];
		$gen_task_type       = $_REQUEST["ddl_gen_task_type"];		
		$gen_task_details    = $_REQUEST["txt_gen_task_details"];
		$gen_task_start_date = $_REQUEST["dt_start_date"];
		$gen_task_end_date   = $_REQUEST["dt_end_date"];
		$gen_task_assignee   = $_REQUEST["ddl_assignee"];
		$department          = $_REQUEST["ddl_department"];		
		
		if(($gen_task_type != "") && ($gen_task_details != "") && ($department != ""))
		{
			$gen_task_add_result = i_edit_task_plan_general($task_id,$gen_task_type,$gen_task_details,$gen_task_start_date,$gen_task_end_date,$gen_task_assignee,$department,'3');
			
			if($gen_task_add_result["status"] == SUCCESS)
			{
				$alert = "Task Successfully Added";
				$alert_type = 1; // Success
				
				if($source == "desktop")
				{
					header("location:general_task_summary_desktop_view.php?search_user=$gen_task_assignee&search_department=$department&search_project=$search_project");
				}
				else
				{
					header("location:general_task_summary.php");
				}
			}
			else
			{
				$alert = $gen_task_add_result["data"];
				$alert_type = 0; // Failure
			}
		}
		else	
		{
			$alert = "Please fill all the mandatory fields. Mandatory fields are marked with *";
			$alert_type = 0; // Failure
		}
	}
	
	// Get task details
	$gen_task_list = i_get_gen_task_plan_list($task_id,'','','','','','','','');
	if($gen_task_list["status"] == SUCCESS)
	{
		$gen_task_list_data = $gen_task_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$gen_task_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get task type list
	$gen_task_type_list = i_get_gen_task_type_list('','1');
	if($gen_task_type_list["status"] == SUCCESS)
	{
		$gen_task_type_list_data = $gen_task_type_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$gen_task_type_list["data"];
		$alert_type = 0; // Failure
	}

	// Get list of users
	$user_list = i_get_user_list('','','','');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get department list
	$department_list = i_get_department_list('','1');
	if($department_list["status"] == SUCCESS)
	{
		$department_list_data = $department_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$department_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Edit Task Details</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?> 

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Edit Task</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Edit task details</a>
						  </li>						  
						</ul>
						
						<br>
						    <div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="edit_task" class="form-horizontal" method="post" action="edit_general_task.php">
								<input type="hidden" name="hd_task_id" value="<?php echo $task_id; ?>" />
									<fieldset>
										
										<div class="control-group">											
											<label class="control-label" for="ddl_gen_task_type">Task Type</label>
											<div class="controls">
												<select name="ddl_gen_task_type" required>
												<?php 
												for($count = 0; $count < count($gen_task_type_list_data); $count++)
												{
												?>
												<option value="<?php echo $gen_task_type_list_data[$count]["general_task_type_id"]; ?>" <?php 
												if($gen_task_type_list_data[$count]["general_task_type_id"] == $gen_task_list_data[0]["general_task_type"])
												{
												?>												
												selected="selected"
												<?php
												}?>><?php echo $gen_task_type_list_data[$count]["general_task_type_name"]; ?></option>												
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->																														
										<div class="control-group">											
											<label class="control-label" for="txt_gen_task_details">Details</label>
											<div class="controls">
												<textarea name="txt_gen_task_details" rows="8" required><?php echo $gen_task_list_data[0]["general_task_details"]; ?></textarea>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_department">Department</label>
											<div class="controls">
												<select name="ddl_department" required>
												<?php 
												for($count = 0; $count < count($department_list_data); $count++)
												{
												?>
												<option value="<?php echo $department_list_data[$count]["general_task_department_id"]; ?>" <?php
												if($department_list_data[$count]["general_task_department_id"] == $gen_task_list_data[0]["general_task_department"])
												{
												?>												
												selected="selected"
												<?php
												}
												?>><?php echo $department_list_data[$count]["general_task_department_name"]; ?></option>												
												<?php
												}
												?>
												</select>												
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										<br />
										
											
										<div class="form-actions">
											<button type="submit" class="btn btn-primary" name="edit_general_task_submit">Submit</button> 
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
