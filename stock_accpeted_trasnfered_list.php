<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: stock_transfer_list.php

CREATED ON	: 09-Dec-2016

CREATED BY	: Lakshmi

PURPOSE     : List of stock for customer withdrawals

*/



/*

TBD: 

*/



/* DEFINES - START */

define('STOCK_TRANSFER_FUNC_ID','355');

/* DEFINES - END */



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');



	$alert_type = -1;

	$alert = "";

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];

	

	// Get permission settings for this user for this page

	$view_perms_list   = i_get_user_perms($user,'',STOCK_TRANSFER_FUNC_ID,'2','1');

	$edit_perms_list   = i_get_user_perms($user,'',STOCK_TRANSFER_FUNC_ID,'3','1');

	$delete_perms_list = i_get_user_perms($user,'',STOCK_TRANSFER_FUNC_ID,'4','1');

	$add_perms_list    = i_get_user_perms($user,'',STOCK_TRANSFER_FUNC_ID,'1','1');



	// Query String Data

	// Nothing

     

	 //Get Location List

	$stock_location_master_search_data = array();

	$location_list = i_get_stock_location_master_list($stock_location_master_search_data);

	if($location_list["status"] == SUCCESS)

	{

		$location_list_data = $location_list["data"];

	}

	else

	{ 

		$alert = $location_list["data"];

		$alert_type = 0;

	}

	

	

	 // Get Stock Transfer modes already added

	$stock_transfer_search_data = array("status"=>"Accepted","sort"=>'1');

	$stock_transfer_list = i_get_stock_transfer($stock_transfer_search_data);

	if($stock_transfer_list['status'] == SUCCESS)

	{

		$stock_transfer_list_data = $stock_transfer_list['data'];

	}	

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>Approved Stock Transfer List</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">

   





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    



<div class="main">

  <div class="main-inner">

    <div class="container">

      <div class="row">

       

          <div class="span6" style="width:100%;">

          

          <div class="widget widget-table action-table">

            <div class="widget-header"> <i class="icon-th-list"></i>

              <h3>Accepted Stock Transfer List</h3>

            </div>

            <!-- /widget-header -->

            <div class="widget-content">

			

              <table class="table table-bordered" style="table-layout: fixed;">

                <thead>

                  <tr>

				    <th width="2%">SL No</th>

					<th width="7%">Material Name</th>
					<th width="3%">Accepted Quantity</th>


					<th width="5%">Source Project</th>

					<th width="5%">Destination Project</th>

					<th width="3%">Remarks</th>

					<th width="5%">Accepted By</th>					

					<th width="3%">Accepted On</th>					

    					

				</tr>

				</thead>

				<tbody>							

				<?php

				if($stock_transfer_list["status"] == SUCCESS)

				{

					$sl_no = 0;

					for($count = 0; $count < count($stock_transfer_list_data); $count++)

					{

						$sl_no++;

		

						

					?>

					<tr>

					<td width="1%"><?php echo $sl_no; ?></td>

					<td style="word-wrap:break-word;"><?php echo $stock_transfer_list_data[$count]["stock_material_name"]; ?> <br><?php echo $stock_transfer_list_data[$count]["stock_material_code"]; ?></td>

					<td width="10%"><?php echo $stock_transfer_list_data[$count]["stock_accepted_quantity"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo $stock_transfer_list_data[$count]["source"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo $stock_transfer_list_data[$count]["dest"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo $stock_transfer_list_data[$count]["stock_transfer_accepted_remarks"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo $stock_transfer_list_data[$count]["accepted_by"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo get_formatted_date($stock_transfer_list_data[$count][

					"stock_transfer_accepted_on"],"d-M-Y");; ?></td>
					</tr>

					<?php

					}

					

				}

				else

				{

				?>

				<td colspan="9">No Project Master condition added yet!</td>

				

				<?php

				}

				 ?>	



                </tbody>

              </table>

            </div>

            <!-- /widget-content --> 

          </div>

          <!-- /widget --> 

         

          </div>

          <!-- /widget -->

        </div>

        <!-- /span6 --> 

      </div>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /main-inner --> 

</div>

    

    

    

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->





    

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    





<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>

<script>


function check_validity(approved_qty)

{		

	accepted_qty = parseInt(document.getElementById('accepted_qty' + count).value);
	
	if(accepted_qty > approved_qty)
	{
		document.getElementById('accepted_qty' + count).value = 0;

		alert('Cannot Accept greater than Requested quantity');

	}

}
function stock_accept_transfer(transfer_id,s_project,d_project,material_id,qty,count)
{
	var ok = confirm("Are you sure you want to Accept");

	{         

		accepted_qty = document.getElementById('accepted_qty_' + count).value;
		remarks = document.getElementById('remarks_' + count).value;
			
		alert(count);
		alert(accepted_qty);
		alert(remarks);
		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{
					if(xmlhttp.responseText != "SUCCESS")

					{

						 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

						 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

					 window.location = "stock_approved_transfer_list.php";

					}

				}

			}



			xmlhttp.open("POST", "ajax/stock_accept_transfer.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("transfer_id=" + transfer_id + "&action=Accepted" + "&s_project=" + s_project + "&d_project=" + d_project + "&material_id=" + material_id + "&qty=" + qty + "&accepted_qty=" + accepted_qty + "&remarks=" + remarks);

		}

	}	

}

</script>



  </body>



</html>