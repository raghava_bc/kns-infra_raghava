<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';

/* DEFINES - START */
define('PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID', '253');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
require_once dirname(__FILE__) . '/../Legal/utilities/PHPExcel-1.8/Classes/PHPExcel.php';

if (isset($_REQUEST["project_id"])) {
    $project_id   = $_REQUEST["project_id"];
// set here
} else {
    $project_id = " ";
}

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

// upload file
    $uploaddir = dirname(__FILE__).'/temp_uploads/';
    $filename = $uploaddir .'budget_plan_import_file_'.time().'.xlsx';
    echo '<pre>';
    if (move_uploaded_file($_FILES['file']['tmp_name'], $filename)) {


  	$type = PHPExcel_IOFactory::identify($filename);
  	$objReader = PHPExcel_IOFactory::createReader($type);
  	$objPHPExcel = $objReader->load($filename);

  	foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
  	   $rows = $worksheet->toArray();
        break;
  	}

    $header = array();
    foreach ($rows[0] as $index => $value) {
  	   $header[$value] = $index;
  	}

    echo "<pre>";
    // print_r($header);
    $tmp_row = array();
    $executed_query_count = 0 ;
    $ignored_query_count = 0 ;
    for($i=1; $i<count($rows); $i++){
       $row = $rows[$i];
  	   $tmp_row['planned_project_id'] = $row[$header['planned_project_id']];
  	   $tmp_row['planned_process_id'] = $row[$header['planned_process_id']];
  	   $tmp_row['planned_task_id'] = $row[$header['planned_task_id']];
  	   $tmp_row['planned_road_id'] = $row[$header['planned_road_id']];
  	   $tmp_row['uom'] = $row[$header['UOM']];
  	   $tmp_row['manpower'] = $row[$header['Manpower']];
  	   $tmp_row['machine'] = $row[$header['Machine']];
  	   $tmp_row['contrcat'] = $row[$header['Contract']];
  	   $tmp_row['material'] = $row[$header['Material']];
       //
       if($tmp_row['machine'] !=0 || $tmp_row['material'] !=0 || $tmp_row['contrcat'] ||  $tmp_row['manpower'])
       {
         $project_planned_data = array("task_id"=>$tmp_row['planned_task_id'],"road_id"=>$tmp_row['planned_road_id']);
         $get_planned_data = db_get_planned_data($project_planned_data);
         if($get_planned_data["status"] == DB_RECORD_ALREADY_EXISTS)
         {
           $project_delete_data = array("task_id"=>$tmp_row['planned_task_id'],"road_id"=>$tmp_row['planned_road_id']);
           $delete_planned_data = db_delete_planned_data($project_delete_data);
         }
         $planned_iresults = db_add_planned_data($tmp_row['planned_project_id'],
                                                  $tmp_row['planned_process_id'],
                                                  $tmp_row['planned_task_id'],
                                                  $tmp_row['uom'],
                                                  $tmp_row['planned_road_id'],
                                                  $tmp_row['manpower'],
                                                  $tmp_row['machine'],
                                                  $tmp_row['contrcat'],
                                                  $tmp_row['material'],
                                                  $user);
            if($planned_iresults["data"] != 0)
            {
              $executed_query_count ++ ;
              header("location:project_budget_report_list.php?project_id=$project_id");
            }
            else {
              $ignored_query_count ++ ;
            }
          }
                                             // print_r($tmp_row);
  	}
    $total_records = (count($rows) - 1);

    echo "Total Entries".' '. $total_records .' ' . 'Executed query' . ' ' . $executed_query_count.' ' . 'Ignored query' . ' ' . $ignored_query_count;


    } else {
        echo "Possible file upload attack!\n";
    }

} else {
    header("location:login.php");
}
?>
