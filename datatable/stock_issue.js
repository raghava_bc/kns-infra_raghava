function checkFileAttached(){
  $('#formImport').submit();
}

function loadTasksAndRoads(ele) {
  var process_id = $(ele).val();
  var project_id =  $('#pm_project_id').val();
  load_tasks(process_id);
  load_road(process_id, project_id);
}

var table = null;
$(document).ready(function() {
  function createToolTip(str, length){
    var substr = (str.length <= 20)? str : str.substr(0, 20)+'...';
    return '<abbr data-toggle="tooltip" data-placement="right" title="'+str+'">'+substr+'</abbr>'
  }

  table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    pageLength: 10,
    scrollX: true,
    scrollX: true,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    dataSrc: 'aaData',
    ajax: 'datatable/stock_issue_datatable.php',
    fnRowCallback: function(row, full, index){
      $(row).attr('id', 'row_'+index);

      // get the stock qty
      $.ajax({
       url: 'ajax/stock_get_material_stock.php',
       data: "project_id=" + full['project_id'] + "&material_id=" + full['material_id'],
       dataType: 'json',
       success: function(material_stock_quantity) {

         $.ajax({
          url: 'ajax/stock_get_issued_qty.php',
          data: "indent_id=" + full['indent_id'] + "&material_id=" + full['material_id'],
          dataType: 'json',
          success: function(material_issued_quantity) {
            var data = table.api().row(index).data();
            data.material_stock_quantity = material_stock_quantity;
            data.material_issued_quantity = material_issued_quantity;
            data.material_pending_quantity = data.material_qty - material_issued_quantity;

            table.api().row(index).data(data);
          }
        });
       }
       });
    },
    fnServerParams: function(aoData) {
      aoData.project_id = $('#project_id').val();
      aoData.indent_id = $('#indent_id').val();
      aoData.search_status = $('#search_status').val();
      aoData.table = "stock_indent_items_list";
      aoData.search_project = getVars('search_project');
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      if (aoData.search && aoData.search.value) {
     aoData.sSearch = aoData.search.value;
   }
    },
    // fixedColumns: {
    //   leftColumns: 4
    // },
    drawCallback: function(){
      $('[data-toggle="tooltip"]').tooltip();
    },
    columns: [{
        orderable: false,
        data: function(){
          return arguments[3].row + 1;
        }
      },
      {
        orderable: false,
        data: 'project_name'
      },
      {
        orderable: false,
        data: 'indent_no'
      },
      {
        orderable: false,
          data : function(data, type, full) {
              return moment(data.added_on).format('DD-MMM-YYYY');
          }
      },
      {
        orderable: false,
        data: 'material_name'
      },
      {
        orderable: false,
        data: 'material_code'
      },
      {
        orderable: false,
        data: 'material_qty'
      },
      {
        orderable: false,
        data: function(data) {
          var val = data.material_stock_quantity;
          return (val !== undefined)? val : 'loading..';
        },
      },
      {
        orderable: false,
        data: function(data) {
          var val = data.material_issued_quantity;
          return (val !== undefined)? val : 'loading..';
        }
      },
      {
        orderable: false,
        data : function() {
          return '<button onclick="showModal('+arguments[3].row+')">Issue</button>';
        }
      },
    ]
  });
});

function showModal(row_id){
  var rowData = table.api().data()[row_id];
  $('#myModal').modal();

  // set these values to modal
  $('#pm_project_id').val(rowData.project_id);
  $('#selected_row_id').val(row_id);

  if((rowData.material_id == 461) || (rowData.material_id == 462)){
    $('#div-machine-id').removeClass('hide');
  } else {
    $('#div-machine-id').addClass('hide');
  }

  // on modal show
  $('#myModal').on('shown.bs.modal', function(){
    $('span#material_name').html(rowData.material_name);
    $('span#material_code').html(rowData.material_code);
    $('span#material_stock_quantity').html(rowData.material_stock_quantity);
    $('span#material_issued_quantity').html(rowData.material_issued_quantity);
    $('span#material_pending_quantity').html(rowData.material_pending_quantity);
  });

  // remove the Modal content from DOM
  $('#myModal').on('hide.bs.modal', function(){
    $('form#project_add_reason_master_form')[0].reset();
  });


}

// get hidden values saved from filters
function getVars(key) {
  var element = document.getElementById(key);
  if (element) {
    return element.value;
  } else {
    return '';
  }
}

function load_tasks(process_id){
	$.ajax({
		url: 'ajax/project_get_task_master.php',
		data: {process_id: process_id},
		dataType: 'json',
		success: function(response) {
			$("#search_task").empty();
			$("#search_task").append("<option>- - Select Task - -</option>");
			for(var i=0; i< response.length; i++) {
				var id = response[i]['project_process_task_id'];
				var name = response[i]['project_task_master_name'];
				 $("#search_task").append("<option value='"+id+"'>"+name+"</option>");
			}
		}
	});
}

function load_road(process_id,project_id){
	$.ajax({
		url: 'ajax/project_get_roads.php',
		data: {process_id: process_id,project_id:project_id},
		dataType: 'json',
		success: function(response) {
			$("#ddl_road").empty();
			 $("#ddl_road").append("<option>- - Select Road - -</option>");
			  for(var i=0; i< response.length; i++) {
				var id = response[i]['project_site_location_mapping_master_id'];
				var name = response[i]['project_site_location_mapping_master_name'];
				 $("#ddl_road").append("<option value='"+id+"'>"+name+"</option>");
			}
		}
	});
}

function submit_issue_material_stock(ele){
  var row_id = $('#selected_row_id').val()
  var row_data = table.api().data()[row_id];
  var is_kns_project = (parseInt(row_data.project_id) === 1);

  var data = {
    issue_submit: 'issue_submit',
    ddl_process: $('#ddl_process').val() || 0,
    search_task: $('#search_task').val() || 0,
    ddl_road: $('#ddl_road').val() || 0,
    issued_qty: $('#issued_qty').val(),
    hd_machine_id: $('#hd_machine_id').val(),
    stxt_remarks: $('#stxt_remarks').val(),

    project_id: row_data.project_id,
    pm_project_id: $('#pm_project_id').val(),
    hd_indent_id: row_data.indent_id,
    hd_indent_material_id: row_data.material_id,
    hd_issued_qty: row_data.material_issued_quantity,
    hd_indent_qty: row_data.material_qty,
    hd_indent_issued_qty: row_data.material_issued_quantity,
    hd_indent_item_id: row_data.indent_item_id
  };

  data.ddl_process = (data.ddl_process === "- - Select Process - -")? '' : data.ddl_process;
  data.ddl_road = (data.ddl_road === "- - Select Road - -")? '' : data.ddl_road;
  data.search_task = (data.search_task === "- - Select Task - -")? '' : data.search_task;

  // validate required fields
  var erro_msg = 'Please provide required fields';
  var machine_id_required = ((row_data.material_id == 461) || (row_data.material_id == 462))
  var minimum_required_fields = !isEmpty(data.issued_qty) &&
                                (machine_id_required? !isEmpty(data.hd_machine_id) : true) &&
                                !isEmpty(data.stxt_remarks);

  if(!minimum_required_fields){
    alert(erro_msg);
    return false;
  }

  if( !is_kns_project &&
      (isEmpty(data.ddl_process) ||
      isEmpty(data.search_task) ||
      isEmpty(data.ddl_road))
  ){
    alert(erro_msg);
    return false;
  }

  ele.disabled = true;
  $(ele).text("Processing..");

  $.ajax({
    method:'post',
		url: 'stock_process_wise_issue.php',
		data: data,
		success: function(response) {
       $('#myModal').modal('hide');
       ele.disabled = false;
       $(ele).text("Submit");
       table.api().draw();
		}
	});
}

function get_machine_list(material)
{
  var searchstring = document.getElementById('stxt_machine').value;
  if(searchstring.length >= 3)
  {
    if (window.XMLHttpRequest)
    {
      // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    }
    else
    {
      // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function()
    {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
      {
        if(xmlhttp.responseText != 'FAILURE')
        {
          document.getElementById('search_results').style.display = 'block';
          document.getElementById('search_results').innerHTML     = xmlhttp.responseText;
        }
        else
        {
          document.getElementById('issued_qty').disabled     = true;
          document.getElementById('stxt_remarks').disabled     = true;
          document.getElementById('hd_machine_id').value     = 0;
        }
      }
    }
    xmlhttp.open("POST", "ajax/stock_get_machine.php");
    // file name where delete code is written
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("search=" + searchstring + "&material=" + material);
  }
  else
  {
    document.getElementById('search_results').style.display = 'none';
  }
}

function isEmpty(data){
  data = (data || '').toString();
  return !!!data.length;
}


function check_validity()
{
  var row_id = $('#selected_row_id').val()
  var row_data = table.api().data()[row_id];

	current_qty = parseInt(document.getElementById('issued_qty').value);
	cur_stock = parseInt(row_data.material_stock_quantity);
	issued_qty  = parseInt(row_data.material_issued_quantity);
	indent_qty  = parseInt(row_data.material_qty);
	if((issued_qty + current_qty) > indent_qty)
	{
		document.getElementById('issued_qty').value = 0;
		alert('Cannot issue greater than pending quantity');
	}
	if(current_qty > cur_stock)
	{
		document.getElementById('issued_qty').value = 0;
		alert('Cannot issue greater than stock quantity');
	}
}

function select_machine(machine_id,search_machine,number,name,count)
{
	document.getElementById('hd_machine_id').value 	= machine_id;
	var name_code = search_machine.concat('-',number);
	document.getElementById('stxt_machine').value = number;
	document.getElementById('search_results').style.display = 'none';
}
