<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 4th Mar 2016
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'meetings'.DIRECTORY_SEPARATOR.'meeting_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["meeting"]))
	{
		$meeting_id = $_GET["meeting"];
	}
	else
	{
		header("location:scheduled_meeting_list.php");
	}
	
	if(isset($_GET["show"]))
	{
		$show = $_GET["show"];
	}
	else
	{
		$show = "all_points";
	}
	/* QUERY STRING - END */
	
	// Get the meeting details as captured already
	$meeting_data = array("meeting_id"=>$meeting_id);
	$meeting_details = i_get_meetings($meeting_data);
	if($meeting_details["status"] == SUCCESS)
	{
		$meeting_details_list = $meeting_details["data"];
	}
	else
	{
		$alert = $meeting_details["data"];
		$alert_type = 0;
	}
	
	// Get the participant details as captured already
	$participant_data = array("meeting_id"=>$meeting_id);
	$participant_details = i_get_meeting_participants($participant_data);
	if($participant_details["status"] == SUCCESS)
	{
		$participant_details_list = $participant_details["data"];
	}
	else
	{
		$alert = $participant_details["data"];
		$alert_type = 0;
	}
	
	// Get the MOM details as captured already
	$mom_data = array("meeting_id"=>$meeting_id,"status"=>'1');
	$mom_details = i_get_meeting_mom($mom_data);
	if($mom_details["status"] == SUCCESS)
	{
		$mom_details_list = $mom_details["data"];
	}
	
	// Get list of users
	$user_list = i_get_user_list('','','','');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Meeting Details</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				&nbsp;&nbsp;&nbsp;Meeting ID: <strong><?php echo $meeting_details_list[0]["meeting_no"]; ?></strong>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">						
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">							
								<div class="tab-pane active" id="formcontrols">
									<div class="widget-header">
									<i class="icon-user"></i>
									<h3>Meeting Details</h3>
									</div> <!-- /widget-header -->
									<br />									
									<fieldset>
									<?php
									if($meeting_details["status"] == SUCCESS)
									{
									?>
										<table class="table">										  
										  <tbody>
										  <tr>
										  <td>Department</td>
										  <td>:</td>
										  <td><?php echo $meeting_details_list[0]["general_task_department_name"]; ?></td>
										  </tr>
										  <tr>
										  <td>Agenda</td>
										  <td>:</td>
										  <td><?php echo $meeting_details_list[0]["meeting_agenda"]; ?></td>
										  </tr>
										  <tr>
										  <td>Document</td>
										  <td>:</td>
										  <td><?php if($meeting_details_list[0]["meeting_document_path"] != ""){?><a href="docs/<?php echo $meeting_details_list[0]["meeting_document_path"]; ?>">DOWNLOAD</a><?php } ?></td>
										  </tr>
										  <tr>
										  <td>Date Time</td>
										  <td>:</td>
										  <td><?php echo date("d-M-Y",strtotime($meeting_details_list[0]["meeting_date"]))." ".$meeting_details_list[0]["meeting_time"]; ?></td>
										  </tr>
										  <tr>
										  <td>Venue</td>
										  <td>:</td>
										  <td><?php echo $meeting_details_list[0]["meeting_venue"]; ?></td>
										  </tr>
										  <tr>
										  <td>Details</td>
										  <td>:</td>
										  <td><?php echo $meeting_details_list[0]["meeting_details"]; ?></td>
										  </tr>
										  <tr>
										  <td>Meeting Requested By</td>
										  <td>:</td>
										  <td><?php echo $meeting_details_list[0]["user_name"]; ?></td>
										  </tr>
										  <tr>
										  <td>Meeting Request Created On</td>
										  <td>:</td>
										  <td><?php echo date("d-M-Y",strtotime($meeting_details_list[0]["meeting_added_on"])); ?></td>
										  </tr>										  
										  </tbody>
										</table>
									<?php
									}
									else
									{
									?>
									<table class="table">										  										
										<tbody>
										<tr>
										<td>No meeting with these details added yet</td>
										</tr>
										</tbody>
									</table>
									<?php
									}
									?>
									</fieldset>
									<div class="widget-header">
									<i class="icon-user"></i>
									<h3>Participants</h3>
									</div> <!-- /widget-header -->
									<br />									
									<fieldset>
										<table class="table table-bordered" style="table-layout: fixed;">
										<?php
										$count = 0;
										if($participant_details["status"] == SUCCESS)
										{
											while($count < count($participant_details_list))
											{
											?>
											<tr>
											<td><?php echo $participant_details_list[$count]["user_name"]; 
											$count++;?></td>
											<td><?php if($count < count($participant_details_list))
											{
												echo $participant_details_list[$count]["user_name"];
											}
											else
											{
												echo "";
											}
											$count++;?></td>
											<td><?php if($count < count($participant_details_list))
											{
												echo $participant_details_list[$count]["user_name"];
											}
											else
											{
												echo "";
											}
											$count++;?></td>
											<?php
											}
										}
										else
										{
										?>
										<tr>
										<td colspan="3">No meeting participant added</td>
										</tr>
										<?php
										}
										?>
										</table>
										<br />										
									</fieldset>
									<div class="widget-header">
									<i class="icon-user"></i>
									<h3>MOM Points&nbsp;&nbsp;&nbsp;Total Items: <?php echo count($mom_details_list); ?>&nbsp;&nbsp;&nbsp;Total Tasks = <span id="total_tasks"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;Completed Tasks = <span id="total_completed_tasks"><i>Calculating</i></span></h3>
									</div> <!-- /widget-header -->
									<br />									
									<fieldset>	
										<table class="table table-bordered">
										<thead>
										<tr>
										  <th>SL No</th>
										  <th>Item</th>
										  <th>Details</th>
										  <th>Target Date</th>
										  <th>Assigned To</th>
										  <th>Status</th>
										  <th>&nbsp;</th>
										  <th>&nbsp;</th>
										</tr>
										</thead>										
										<tbody>
										<?php
										$sl_count 			   = 0;
										$total_tasks 		   = 0;
										$total_completed_tasks = 0;
										if($mom_details["status"] == SUCCESS)
										{
											for($mom_count = 0; $mom_count < count($mom_details_list); $mom_count++)
											{
												$sl_count++;
												
												if($mom_details_list[$mom_count]["general_task_completion_status"] == "4")
												{
													$total_completed_tasks++;
													$total_tasks++;
												}
												else if($mom_details_list[$mom_count]["general_task_completion_status"] != "")
												{
													$total_tasks++;
												}
												else
												{
													// Nothing here
												}
												
												if(($show == "all_points") || (($show == "all_tasks") && ($mom_details_list[$mom_count]["general_task_completion_status"] != "")) || (($show == "completed_tasks") && ($mom_details_list[$mom_count]["general_task_completion_status"] == "4")) || (($show == "pending_tasks") && ($mom_details_list[$mom_count]["general_task_completion_status"] != "4") && ($mom_details_list[$mom_count]["general_task_completion_status"] != "")))
												{
										?>
										<form method="post" id="edit_mom_data" action="#">
										<input type="hidden" id="hd_mom_id_<?php echo $sl_count; ?>" value="<?php echo $mom_details_list[$mom_count]["mom_id"]; ?>" />										
										<tr>
										  <td style="width:12px;"><?php echo $sl_count; ?></td>
										  <td style="width:150px;"><input type="text" style="width:170px;" id="stxt_mom_item_<?php echo $sl_count; ?>" value="<?php echo $mom_details_list[$mom_count]["mom_item"]; ?>" disabled /></td>					
										  <td style="width:150px;"><textarea style="width:170px;" id="txt_desc_<?php echo $sl_count; ?>" disabled><?php echo $mom_details_list[$mom_count]["mom_description"]; ?></textarea></td>
										  <td style="width:120px;"><?php if($mom_details_list[$mom_count]["mom_target_date"] != "0000-00-00")
										  {
											echo date("d-M-Y",strtotime($mom_details_list[$mom_count]["mom_target_date"])); 
										  }
										  else
										  {
										    echo "";
										  }?></td>
										  <td style="width:80px;"><?php echo $mom_details_list[$mom_count]["user_name"]; ?></td>
										  <td style="width:60px;"><?php echo i_get_task_status_name($mom_details_list[$mom_count]["general_task_completion_status"]); ?></td>
										  <td style="width:60px;"><a href="#" onclick="return edit_submit(<?php echo $sl_count; ?>);">Submit</a></td>
										  <td style="width:80px;"><a href="#" onclick="return enable(<?php echo $sl_count; ?>);"><span style="color:black; text-decoration: underline;">Edit</span></a>&nbsp;&nbsp;&nbsp;<a href="#" onclick="return confirm_deletion(<?php echo $mom_details_list[$mom_count]["mom_id"]; ?>);"><span style="color:black; text-decoration: underline;">Delete</span></a></td>
										</tr>
										</form>
										<?php
												}
											}
										}
										?>
										<form method="post" name="add_mom_item_form" action="add_mom_items.php">		
										<input type="hidden" name="hd_navigate_meeting_id" value="<?php echo $meeting_id; ?>" />
										<input type="hidden" name="hd_navigate_department_id" value="<?php echo $meeting_details_list[0]["meeting_department"]; ?>" />
										<tr>	
											<td style="word-wrap:break-word;"><?php echo ++$sl_count; ?></td>
											<td style="word-wrap:break-word;"><input type="text" style="width:170px;" name="stxt_mom_item" placeholder="Enter the MOM point" required /></td>
											<td style="word-wrap:break-word;"><textarea name="txt_details" style="width:170px;"></textarea></td>
											<td style="word-wrap:break-word;"><input type="date" style="width:120px;" name="dt_target_date" /></td>
											<td style="word-wrap:break-word;"><select name="ddl_assigned_to" style="width:120px;">
											<option value="">- - Select Assignee - -</option>
											<?php 											
											for($count = 0; $count < count($user_list_data); $count++)
											{
											?>
											<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" 
											<?php if($user_list_data[$count]["user_id"] == $user)
											{
											?>
											
											selected="selected"
											
											<?php
											}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>												
											<?php
											}
											?>
											</select></td>
											<td style="word-wrap:break-word;" colspan="3"><button type="submit" class="btn btn-primary" name="add_mom_submit">Submit</button></td>
										</tr>
										</form>
										<tbody>
										</table>
										<script>
										document.getElementById('total_tasks').innerHTML = <?php echo $total_tasks; ?>;
										document.getElementById('total_completed_tasks').innerHTML = <?php echo $total_completed_tasks; ?>;
										</script>
									</fieldset>																								
								
							</div>
						  						  
						</div>
	
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function confirm_deletion(mom_id)
{
	var ok = confirm("Are you sure you want to delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					 window.location = "meeting_details.php?meeting="+<?php echo $meeting_id; ?>;
				}
			}

			xmlhttp.open("GET", "mom_delete.php?mom=" + mom_id + "&meeting=" + <?php echo $meeting_id; ?>);   // file name where delete code is written
			xmlhttp.send();
		}
	}

	return true;
}

function enable(sl_no)
{
	document.getElementById('stxt_mom_item_'+sl_no).disabled = false;
	document.getElementById('txt_desc_'+sl_no).disabled = false;
	
	return true;
}

function edit_submit(sl_no)
{
	if(document.getElementById('stxt_mom_item_'+sl_no).disabled == true)
	{
		alert("You are performing an invalid operation. Please click on Edit before editing and submitting");
	}
	else
	{
		mom_id      = document.getElementById('hd_mom_id_'+sl_no).value;
		mom_item    = document.getElementById('stxt_mom_item_'+sl_no).value;
		description = document.getElementById('txt_desc_'+sl_no).value;		
		
		window.location = "edit_mom.php?hd_mom_id="+mom_id+"&stxt_item="+mom_item+"&txt_description="+description+"&hd_meeting_id="+<?php echo $meeting_id; ?>;
	}
}
</script>


  </body>

</html>