-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 22, 2018 at 01:38 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kns_erp_live`
--

-- --------------------------------------------------------

--
-- Structure for view `issued_items_list`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `issued_items_list` AS select `SII`.`stock_issue_item_id` AS `stock_issue_item_id`,`SII`.`stock_issue_item_issue_id` AS `stock_issue_item_issue_id`,`SII`.`stock_issue_item_material_id` AS `stock_issue_item_material_id`,`SII`.`stock_issue_item_qty` AS `stock_issue_item_qty`,`SII`.`stock_issue_item_machine_details` AS `stock_issue_item_machine_details`,`SII`.`stock_issue_item_project` AS `stock_issue_item_project`,`SII`.`stock_issue_item_active` AS `stock_issue_item_active`,`SII`.`stock_issue_item_remarks` AS `stock_issue_item_remarks`,`SII`.`stock_issue_item_issued_by` AS `stock_issue_item_issued_by`,`SII`.`stock_issue_item_issued_on` AS `stock_issue_item_issued_on`,`SI`.`stock_issue_no` AS `stock_issue_no`,`SI`.`stock_issue_indent_id` AS `stock_issue_indent_id`,`SI`.`stock_issue_active` AS `stock_issue_active`,`U`.`user_id` AS `user_id`,`U`.`user_name` AS `user_name`,`AU`.`user_name` AS `indent_by`,`SIO`.`stock_indent_no` AS `stock_indent_no`,`SIO`.`stock_indent_project` AS `stock_indent_project`,`SIO`.`stock_indent_requested_by` AS `stock_indent_requested_by`,`SIO`.`stock_indent_requested_on` AS `stock_indent_requested_on`,`SIO`.`stock_indent_department` AS `stock_indent_department`,`SIO`.`stock_indent_location` AS `stock_indent_location`,`SIO`.`stock_indent_material_required_date` AS `stock_indent_material_required_date`,`SIO`.`stock_indent_remarks` AS `stock_indent_remarks`,`SIO`.`stock_indent_status` AS `stock_indent_status`,`SIO`.`stock_indent_added_by` AS `stock_indent_added_by`,`SIO`.`stock_indent_added_on` AS `stock_indent_added_on`,`SMM`.`stock_material_code` AS `stock_material_code`,`SMM`.`stock_material_name` AS `stock_material_name`,`SMM`.`stock_material_type` AS `stock_material_type`,`SMM`.`stock_material_unit_of_measure` AS `stock_material_unit_of_measure`,`SMM`.`stock_material_transportation_mode` AS `stock_material_transportation_mode`,`SMM`.`stock_material_manufacturer_part_number` AS `stock_material_manufacturer_part_number`,`SMM`.`stock_material_price` AS `stock_material_price`,`SMM`.`stock_material_active` AS `stock_material_active`,`SMM`.`stock_material_updated_on` AS `stock_material_updated_on`,`SP`.`stock_project_name` AS `stock_project_name`,`SMMM`.`stock_machine_master_id` AS `stock_machine_master_id`,`SMMM`.`stock_machine_master_name` AS `stock_machine_master_name`,`SMMM`.`stock_machine_master_id_number` AS `stock_machine_master_id_number`,`SMMM`.`stock_machine_master_vendor` AS `stock_machine_master_vendor`,`SMMM`.`stock_machine_type` AS `stock_machine_type`,`U`.`user_name` AS `added_by`,`AM`.`process_name` AS `process_name`,`AM`.`project_task_master_name` AS `task_name`,`AM`.`road_name` AS `road_name`,`AM`.`actual_material_road_id` AS `road_id`,`SUMM`.`stock_unit_name` AS `uom_name` from (((((((((`stock_issue_item` `SII` join `stock_issue` `SI` on((`SI`.`stock_issue_id` = `SII`.`stock_issue_item_issue_id`))) join `users` `U` on((`U`.`user_id` = `SII`.`stock_issue_item_issued_by`))) join `stock_indent` `SIO` on((`SIO`.`stock_indent_id` = `SI`.`stock_issue_indent_id`))) join `stock_material_master` `SMM` on((`SMM`.`stock_material_id` = `SII`.`stock_issue_item_material_id`))) join `stock_project` `SP` on((`SP`.`stock_project_id` = `SII`.`stock_issue_item_project`))) left join `stock_machine_master` `SMMM` on((`SMMM`.`stock_machine_master_id` = `SII`.`stock_issue_item_machine_details`))) left join `actual_material` `AM` on((`AM`.`issue_item_id` = `SII`.`stock_issue_item_id`))) join `users` `AU` on((`AU`.`user_id` = `SIO`.`stock_indent_added_by`))) join `stock_unit_measure_master` `SUMM` on((`SUMM`.`stock_unit_id` = `SMM`.`stock_material_unit_of_measure`))) order by `SII`.`stock_issue_item_issued_on` desc;

--
-- VIEW  `issued_items_list`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
