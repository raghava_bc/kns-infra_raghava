<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: kns_Quotation_Compare_list.php

CREATED ON	: 4th-oct-2016

CREATED BY	: Lakshmi

PURPOSE     : List of quotation Compare withdrawals

*/



/*

TBD: 

*/



/* DEFINES - START */

define('QUOTATION_FUNC_ID','218');

/* DEFINES - END */



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_quotation_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];

	

	// Get permission settings for this user for this page

	$view_perms_list   = i_get_user_perms($user,'',QUOTATION_FUNC_ID,'2','1');	

	$add_perms_list    = i_get_user_perms($user,'',QUOTATION_FUNC_ID,'1','1');

	$edit_perms_list   = i_get_user_perms($user,'',QUOTATION_FUNC_ID,'3','1');

	$delete_perms_list = i_get_user_perms($user,'',QUOTATION_FUNC_ID,'4','1');



	// Query String Data

	if(isset($_REQUEST["indent_item_id"]))

	{

		$indent_item_id = $_REQUEST["indent_item_id"];

	}

	else

	{

		$indent_item_id = "-1";

	}

	if(isset($_REQUEST["mode"]))

	{

		$mode = $_REQUEST["mode"];

	}

	else

	{

		$mode = "-1";

	}

	
	if(isset($_REQUEST["project"]))
	{
		$project = $_REQUEST["project"];
	}

	else

	{
		$project = "";
	}

	

	// Get quotation compare already added
	$stock_quotation_compare_search_data = array("active"=>'1',"indent_id"=>$indent_item_id,"status"=>'Waiting',"project"=>$project);
	$quotation_compare_list = i_get_stock_quotation_compare_list($stock_quotation_compare_search_data);

	if($quotation_compare_list['status'] == SUCCESS)

	{

		$quotation_compare_list_data = $quotation_compare_list['data'];

		$quotation_id 	= $quotation_compare_list_data[0]["stock_quotation_id"];

		$indent_item_id = $quotation_compare_list_data[0]["stock_quotation_id"];

	}	

	else

	{

		$quotation_id = "";

	}

	

	$stock_purchase_order_search_data = array("quotation_id"=>$quotation_id);

	$purcahse_order_list = i_get_stock_purchase_order_list($stock_purchase_order_search_data);

	if($purcahse_order_list['status'] == SUCCESS)

	{

		$purcahse_order_list_data = $purcahse_order_list['data'];

		$po_id = $purcahse_order_list_data[0]["stock_purchase_order_id"];

	}	

	else

	{

		$po_id ="-1";

	}
	
	$quote = i_add_quote();
	if($quote["status"] == SUCCESS)
	{
		$alert = "added";
	}

	

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>Quotation Compare List</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">

   





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>

    



<div class="main">

  <div class="main-inner">

    <div class="container">

      <div class="row">

       

          <div class="span6" style="width:100%;">

          

          <div class="widget widget-table action-table">

            <div class="widget-header"> <i class="icon-th-list"></i>

              <h3>Quotation Compare List</h3><span style="float:right; padding-right:20px;"></span>

			  <?php if($mode != -1)

						{?>

						<div class="pull-right"><a style="padding-right:10px" href="stock_po_items_approval.php?" >Back to Po Approval </a></div>

						<?php

						}

						?>

			

	  		</div> <!-- /widget-header -->

						           

            <div class="widget-content">

			

              <table class="table table-bordered">

                <thead>

                  <tr>

				    <th>SL No</th>

					<th>Item</th>

					<th>Quantity</th>

					<th>Amount</th>
					<th>Project</th>
					<th>Quotation No</th>

					<th>Vendor</th>

				    <th>Received Date</th>

					<th>Remarks</th>

					<th>Last Updated By</th>		

					<th>Last Updated On</th>	

					<th>Status</th>	

					<th colspan="2" style="text-align:center;">Action</th>									

				</tr>

				</thead>

				<tbody>							

				<?php

				if($quotation_compare_list["status"] == SUCCESS)

				{

					/*if($purcahse_order_list['status'] == SUCCESS)

					{*/

						$sl_no = 0;

						for($count = 0; $count < count($quotation_compare_list_data); $count++)

						{

							$sl_no++;

							?>

								 <input type="hidden" name="hd_indent_id" value="<?php echo $indent_item_id; ?>" />

								<tr>

								<td><?php echo $sl_no; ?></td>

								<td><?php echo $quotation_compare_list_data[$count]["stock_material_name"].' ('.$quotation_compare_list_data[$count]["stock_material_code"].')'; ?></td>

								<td><?php echo $quotation_compare_list_data[$count]["stock_quotation_quantity"]; ?></td>

								<td><?php echo $quotation_compare_list_data[$count]["stock_quotation_amount"]; ?></td>
								<td><?php echo $quotation_compare_list_data[$count]["stock_project_name"]; ?></td>
								<td><?php echo $quotation_compare_list_data[$count]["stock_quotation_no"]; ?></td>								

								<td style="word-wrap:break-word;"><?php echo $quotation_compare_list_data[$count]["stock_vendor_name"]; ?></td>

								<td style="word-wrap:break-word;"><?php echo get_formatted_date($quotation_compare_list_data[$count]

								["stock_quotation_received_date"],'d-M-Y'); ?></td>								

								<td><?php echo $quotation_compare_list_data[$count]["stock_quotation_remarks"]; ?></td>

								<td style="word-wrap:break-word;"><?php echo $quotation_compare_list_data[$count]["user_name"]; ?></td>

								<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($quotation_compare_list_data[$count]["stock_quotation_added_on"])); ?></td>

								<td style="word-wrap:break-word;"><?php echo $quotation_compare_list_data[$count]["stock_quotation_status"]; ?></td>

								<?php if($mode != "view_quote")

								{?>	

								<td>

								<?php if(($quotation_compare_list_data[$count]["stock_quotation_active"] == "1") || ($edit_perms_list['status'] == SUCCESS)){?><a style="padding-right:10px" href="#" onclick="return go_to_edit_quotation_compare('<?php echo $quotation_compare_list_data[$count]["stock_quotation_id"]; ?>',<?php echo $quotation_compare_list_data[$count]["stock_quotation_indent_id"]; ?>);">Edit </a><?php } ?></td>								

								<td><?php if($quotation_compare_list_data[$count]["stock_quotation_status"] == 
								"Waiting"){?><a href="#" onclick="return approve_quotation(<?php echo $quotation_compare_list_data[$count]["stock_quotation_id"]; ?>,<?php echo $project; ?>);">Approve</a><?php } ?></td>
								<?php

								}

								?>

								</tr>

							<?php							

						//}

					}

				}

				else

				{

				?>

				<td colspan="13">No quotation compare added yet!</td>

				<?php

				}

				 ?>	



                </tbody>

              </table>

            </div>

            <!-- /widget-content --> 

          </div>

          <!-- /widget --> 

         

          </div>

          <!-- /widget -->

        </div>

        <!-- /span6 --> 

      </div>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /main-inner --> 

</div>

    

    

    

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->





    

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    





<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>



<script>

function delete_quotation_compare(quotation_id,indent_item_id)

{

	var ok = confirm("Are you sure you want to Delete?")

	{         

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					alert(xmlhttp.responseText);

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

					 window.location = "http://localhost/kns/Legal/stock_quotation_compare_list.php?indent_item_id=" + indent_item_id;

					}

				}

			}



			xmlhttp.open("POST", "stock_delete_quotation_compare.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("quotation_id=" + quotation_id + "&indent_item_id=" + indent_item_id + "&action=0");

		}

	}	

}

function go_to_edit_quotation_compare(quotation_id)

{		

	var form = document.createElement("form");

    form.setAttribute("method", "post");

    form.setAttribute("action", "stock_edit_quotation_compare.php");

	

	var hiddenField1 = document.createElement("input");

	hiddenField1.setAttribute("type","hidden");

	hiddenField1.setAttribute("name","quotation_id");

	hiddenField1.setAttribute("value",quotation_id);

	

	form.appendChild(hiddenField1);

	

	document.body.appendChild(form);

    form.submit();

}

function go_to_purchase_order(quotation_id)

{		

	var form = document.createElement("form");

    form.setAttribute("method", "post");

    form.setAttribute("action", "stock_add_purchase_order.php");

	

	var hiddenField1 = document.createElement("input");

	hiddenField1.setAttribute("type","hidden");

	hiddenField1.setAttribute("name","quotation_id");

	hiddenField1.setAttribute("value",quotation_id);

	

	form.appendChild(hiddenField1);

	

	document.body.appendChild(form);

    form.submit();

}
function approve_quotation(quotation_id,projectid)
{

	var ok = confirm("Are you sure you want to Approve?")

	{         

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{							

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

					 window.location = "stock_quotation_for_approval.php?ddl_project=" +projectid;

					}

				}

			}



			xmlhttp.open("POST", "stock_approve_quotation.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("quotation_id=" + quotation_id + "&projectid=" + projectid + "&action=Approved");
		}

	}	

}



</script>



  </body>



</html>