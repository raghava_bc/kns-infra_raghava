$(document).ready(function() {

  var table = $('#example').dataTable({
    "sScrollX": "100%",
    "sScrollXInner": "110%",
    "sPaginationType": "full_numbers",
    "bScrollCollapse": true,
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": 'datatable/project_approved_mp_list_datatable.php',
    "iDisplayLength": 10,
    "sScrollY": "50%",
    "bInfo": true,
    "iTabIndex": 1,
    "lengthChange": false,
    language: {
      search: "_INPUT_",
      searchPlaceholder: "Search...",
      "infoFiltered": ""
    },
    "fnServerParams": function(aoData) {
      aoData.push({
        "name": "table",
        "value": "project_task_actual_manpower_list"
      });
      aoData.push({
        "name": "search_project",
        "value": getVars('search_project')
      });
      aoData.push({
        "name": "search_vendor",
        "value": getVars('search_vendor')
      });
      aoData.push({
        "name": "start_date",
        "value": getVars('start_date')
      });
      aoData.push({
        "name": "end_date",
        "value": getVars('end_date')
      });
    },
    "aaSorting": [
      [17, 'desc']
    ],
    "fnDrawCallback": function(oSettings) {
      /* Need to redo the counters if filtered or sorted */
      if (oSettings.bSorted || oSettings.bFiltered || oSettings.iDraw > 1) {
        for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
          this.fnUpdate(i + oSettings._iDisplayStart + 1, oSettings.aiDisplay[i], 0, false, false);
        }
      }
    },
    "aoColumnDefs": [{
        "bSortable": false,
        "aTargets": [0]
      },
      {
        "bSortable": false,
        "aTargets": [1],
        "mData": `project_manpower_agency_name`
      },
      {
        "bSortable": false,
        "aTargets": [2],
        "mData": `project_master_name`,
      },
      {
        "bSortable": false,
        "aTargets": [3],
        "mData": 'project_process_master_name'
      },
      {
        "bSortable": false,
        "aTargets": [4],
        "mData": `project_task_master_name`
      },
      {
        "bSortable": false,
        "aTargets": [5],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          if (data.project_task_actual_manpower_road_id == 'No Roads' || data.project_task_actual_manpower_road_id == '') {
            return 'No Roads';
          }
          return data.project_task_actual_manpower_road_id;
        }
      },
      {
        "bSortable": false,
        "aTargets": [6],
        "mData": `project_task_actual_manpower_work_type`
      },
      {
        "bSortable": false,
        "aTargets": [7],
        "mData": function(data, type, full) {

          if (type !== 'display') {
            return '';
          }

          if (data.project_task_actual_manpower_work_type === 'Regular') {
            return data.project_task_actual_manpower_completion_percentage;
          } else {
            return data.project_task_actual_manpower_rework_completion;
          }
        }
      },
      {
        "bSortable": false,
        "aTargets": [8],
        "mData": `project_task_actual_manpower_completed_msmrt`
      },
      {
        "bSortable": false,
        "aTargets": [9],
        "mData": function(data, type, full) {

          if (type !== 'display') {
            return '';
          }
          return moment(data.project_task_actual_manpower_date).format('DD-MM-YYYY');
        }
      },
      {
        "bSortable": false,
        "aTargets": [10],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return data.project_task_actual_manpower_no_of_men + ' Hours ' + '<br/>' + ' Rate : ' + data.project_task_actual_manpower_men_rate + ' <br/> ' + data.project_task_actual_manpower_no_of_men / 8 + ' Men';
        }
      },
      {
        "bSortable": false,
        "aTargets": [11],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return data.project_task_actual_manpower_no_of_women + ' Hours ' + '<br/>' + ' Rate : ' + data.project_task_actual_manpower_women_rate + '<br/>' + data.project_task_actual_manpower_no_of_women / 8 + ' Women';
        }
      },
      {
        "bSortable": false,
        "aTargets": [12],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return data.project_task_actual_manpower_no_of_mason + ' Hours ' + '<br/>' + ' Rate : ' + data.project_task_actual_manpower_mason_rate + ' <br/> ' + data.project_task_actual_manpower_no_of_mason / 8 + ' Mason';
        }
      },
      {
        // No of People
        "bSortable": false,
        "aTargets": [13],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return parseFloat(data.project_task_actual_manpower_no_of_men / 8) + parseFloat(data.project_task_actual_manpower_no_of_women / 8) + parseFloat(data.project_task_actual_manpower_no_of_mason / 8);
        }
      },
      {
        // Amount
        "bSortable": false,
        "aTargets": [14],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return (parseFloat(data.project_task_actual_manpower_no_of_men) * parseFloat(data.project_task_actual_manpower_men_rate)) + (parseFloat(data.project_task_actual_manpower_no_of_women) * parseFloat(data.project_task_actual_manpower_women_rate)) + (parseFloat(data.project_task_actual_manpower_no_of_mason) * parseFloat(data.project_task_actual_manpower_mason_rate));
        }
      },
      {
        // Remarks
        "bSortable": false,
        "aTargets": [15],
        "mData": `project_task_actual_manpower_remarks`
      },
      {
        // Remarks
        "bSortable": false,
        "aTargets": [16],
        "mData": `user_name`
      },
      {
        "bSortable": true,
        "aTargets": [17],
        "mData": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return moment(data.project_task_actual_manpower_added_on).format('DD-MM-YYYY');
        }
      },
    ],
  });
  new FixedColumns(table, {
    "iLeftColumns": 3,
    "sLeftWidth": 'relative',
    "iLeftWidth": 26,
  });
});

// get hidden values saved from filters
function getVars(key) {
  var element = document.getElementById(key);
  if (element) {
    return element.value;
  } else {
    return '';
  }
}

// set search filters' as hidden value
function setVars(elements) {
  for (var element in elements) {
    if (element && elements[element].length) {
      var hiddenField1 = document.createElement("input");
      hiddenField1.setAttribute("type", "hidden");
      hiddenField1.setAttribute("id", element);
      hiddenField1.setAttribute("value", elements[element]);
      document.body.appendChild(hiddenField1);
    }
  }
}