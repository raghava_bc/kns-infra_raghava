<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 4th oct 2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_quotation_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";

	// Capture the form data
	if(isset($_POST["add_bill_no_submit"]))
	{
		$manpower_id  		  = $_POST["manpower_id"];
		$billing_address    = $_POST["billing_address"];
		$remarks      		  = $_POST[""];
		$approved_by 		  	= $user;
		$approved_on 		  	= date("Y-m-d H:i:s");
		$project_actual_payment_manpower_update_data = array(
			"billing_address" => $billing_address,
			"approved_by" => $user,
			"approved_on" => $approved_on,
			"remarks" => $remarks
		);
		$approve_payment_manpower_result = i_update_project_actual_payment_manpower($manpower_id, $project_actual_payment_manpower_update_data);
		if($approve_payment_manpower_result["status"] == SUCCESS)
		{
			$project_actual_payment_manpower_search_data = array("manpower_id"=>$manpower_id, "start"=>'-1');
			$payment_manpower_list = i_get_project_actual_payment_manpower($project_actual_payment_manpower_search_data);
			if($payment_manpower_list["status"] == SUCCESS){
				$payment_manpower_list_data = $payment_manpower_list["data"];
				$bill_no = $payment_manpower_list_data[0]["project_actual_payment_manpower_bill_no"];
			}

			echo $bill_no; exit;
		} else {
			print_r($approve_payment_manpower_result); exit;
		}
	} else {
		exit;
	}
}
else
{
	header("location:login.php");
}
?>
