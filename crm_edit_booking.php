<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 18th Dec 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["booking"]))
	{
		$booking_id = $_GET["booking"];
	}
	else
	{
		$booking_id = "";
	}	if(isset($_GET["status"]))	{		$status = $_GET["status"];	}	else	{		$status = "";	}	
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["edit_booking_submit"]))
	{
		$booking_id   = $_POST["hd_booking_id"];				$status       = $_POST["hd_status"];
		$booking_date = $_POST["dt_booking_date"];
		$booking_user = $_POST["ddl_user"];
		
		// Check for mandatory fields
		if(($booking_date != "") || ($booking_user != ""))
		{
			$booking_uresult = i_update_booking($booking_id,'','',$booking_date,$booking_user);
			
			if($booking_uresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				$booking_sresult = i_get_site_booking($booking_id,'','','','','','','','','','','','','','','','');
				
				if($booking_sresult["status"] == SUCCESS)
				{
					// Update STM data in enquiry also
					$enquiry_uresult = i_update_enquiry_assignee($booking_sresult["data"][0]['enquiry_id'],$booking_user);
					if($enquiry_uresult["status"] == SUCCESS)
					{
						switch($status)						{							case "4":							header("location:crm_agreement_list.php");							break;							case "5": 						    header("location:crm_registration_list.php");							break;							case "6": 						    header("location:crm_katha_transfer_list.php");							break;													}					}								
					else
					{
						$alert_type = 0;
						$alert      = 'There was an internal error. Please try again later';
					}
				}
				else
				{
					$alert_type = 0;
					$alert      = 'There was an internal error. Please try again later';
				}
			}
			else
			{
				$alert_type = 0;
				$alert      = $booking_uresult["data"];
			}					
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// User List
	$user_list = i_get_user_list('','','','');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get booking data
	$booking_list = i_get_site_booking($booking_id,'','','','','','','','','','','','','');
	if($booking_list["status"] == SUCCESS)
	{
		$booking_list_data = $booking_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$booking_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Edit Booking</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>For Admin Only</h3>
						
						<span style="float:right; padding-right:10px;"><a href="crm_customer_transactions.php?booking=<?php echo $booking_id; ?>">Back to customer profile</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Edit Booking</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="edit_booking" class="form-horizontal" method="post" action="crm_edit_booking.php">
								<input type="hidden" name="hd_booking_id" value="<?php echo $booking_id; ?>" />								<input type="hidden" name="hd_status" value="<?php echo $status; ?>" />
									<fieldset>										
																				
										<div class="control-group">											
											<label class="control-label" for="dt_booking_date">Booking User</label>
											<div class="controls">
												<select name="ddl_user" required>
												<?php
												for($count = 0; $count < count($user_list_data); $count++)
												{
													if(($user_list_data[$count]["user_role"] == "1") || ($user_list_data[$count]["user_role"] == "5") || ($user_list_data[$count]["user_role"] == "6") || ($user_list_data[$count]["user_role"] == "7") || ($user_list_data[$count]["user_role"] == "8") || ($user_list_data[$count]["user_role"] == "9"))
													{
													?>
													<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php 
													if($booking_list_data[0]["crm_booking_added_by"] == $user_list_data[$count]["user_id"])
													{
													?>												
													selected="selected"
													<?php
													}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>								
													<?php
													}
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="dt_booking_date">Booking Date</label>
											<div class="controls">
												<input type="date" class="span6" name="dt_booking_date" value="<?php echo $booking_list_data[0]["crm_booking_date"] ?>" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																				
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_booking_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
