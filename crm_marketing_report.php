<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: process_list.php
CREATED ON	: 05-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Process Plans
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/
$_SESSION['module'] = 'CRM Reports';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_marketing'.DIRECTORY_SEPARATOR.'crm_marketing_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing here

	// Search parameters
	if(isset($_POST["marketing_expense_filter_submit"]))
	{		
		$start_date = $_POST['dt_start_filter'];
		$end_date   = $_POST['dt_end_filter'];
		$added_by   = $_POST['ddl_requester'];
		$source     = $_POST['ddl_source'];
	}
	else
	{	
		$start_date = '';
		$end_date   = '';
		$added_by   = '';
		$source     = '';
	}
	
	// Get list of enquiry sources for dropdown
	$enquiry_source_ddl_list = i_get_enquiry_source_list('','');
	if($enquiry_source_ddl_list["status"] == SUCCESS)
	{
		$enquiry_source_ddl_list_data = $enquiry_source_ddl_list["data"];
	}
	else
	{
		$alert = $enquiry_source_ddl_list["data"];
		$alert_type = 0;
	}
	
	// Get list of enquiry sources for display
	$enquiry_source_list = i_get_enquiry_source_list('','',$source);
	if($enquiry_source_list["status"] == SUCCESS)
	{
		$enquiry_source_list_data = $enquiry_source_list["data"];
	}
	else
	{
		$alert = $enquiry_source_list["data"];
		$alert_type = 0;
	}

	// Get list of users
	$user_list = i_get_user_list('','','','','1','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
	}		
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Marketing Expense Report</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header" style="height:50px; padding-top:10px;"> <i class="icon-th-list"></i>
              <h3>Marketing Expense Report&nbsp;&nbsp;&nbsp;Average Cost Per lead: <span id="avg_cost_per_lead"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;Average Cost Per Sale: <span id="avg_cost_per_sale"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;Target Cost Per Lead (CPL): <?php echo TARGET_COST_PER_LEAD; ?>&nbsp;&nbsp;&nbsp;Target Cost Per Sale (CPS): <?php echo TARGET_COST_PER_SALE; ?></h3>			  
            </div>
            <!-- /widget-header -->
			
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="post" id="marketing_expense_filter_form" action="crm_marketing_report.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_start_filter" value="<?php echo $start_date; ?>" />
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_end_filter" value="<?php echo $end_date; ?>" />
			  </span>			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_requester">
			  <option value="">- - Select Marketing Manager - -</option>
			  <?php
			  for($count = 0; $count < count($user_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php if($added_by == $user_list_data[$count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$count]["user_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_source">
			  <option value="">- - Select Source - -</option>
			  <?php
			  for($count = 0; $count < count($enquiry_source_ddl_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $enquiry_source_ddl_list_data[$count]["enquiry_source_master_id"]; ?>" <?php if($source == $enquiry_source_ddl_list_data[$count]["enquiry_source_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $enquiry_source_ddl_list_data[$count]["enquiry_source_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <input type="submit" name="marketing_expense_filter_submit" />
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th>SL No</th>					
					<th>Enquiry Source</th>	
					<th>Amount Planned</th>
					<th>Amount Paid</th>
					<th>Promised Leads</th>	
					<th>No. of leads</th>
					<th>No. of sales</th>
					<th>Cost per lead</th>
					<th>Cost per sale</th>
					<th>&nbsp;</th>					
				</tr>
				</thead>
				<tbody>
				<?php					
				$sl_no = 0;
				$total_expenditure = 0;
				$total_num_leads   = 0;
				$total_num_sales   = 0;
				if($enquiry_source_list["status"] == SUCCESS)
				{				
					$enquiry_no_leads_list    = array();
					$enquiry_source_name_list = array();
					$enquiry_source_id_list   = array();
					$sorted_array_count = 0;
					for($count = 0; $count < count($enquiry_source_list_data); $count++)
					{
						// No of leads generated from this source
						$enquiry_sresult = i_get_enquiry_list('','','','','','','','',$enquiry_source_list_data[$count]['enquiry_source_master_id'],'','','','',$start_date,$end_date,'','','','','');
						if($enquiry_sresult['status'] == SUCCESS)
						{
							$number_of_leads = count($enquiry_sresult['data']);							
						}
						else
						{
							$number_of_leads = 0;
						}
												
						if($count == 0)
						{												
							// Sort based on number of leads
							$enquiry_no_leads_list[$count]    = $number_of_leads;
							$enquiry_source_name_list[$count] = $enquiry_source_list_data[$count]['enquiry_source_master_name'];
							$enquiry_source_id_list[$count]   = $enquiry_source_list_data[$count]['enquiry_source_master_id'];
						}
						else if($number_of_leads >= $enquiry_no_leads_list[$count - 1])
						{							
							$enquiry_no_leads_list[$count]    = $number_of_leads;
							$enquiry_source_name_list[$count] = $enquiry_source_list_data[$count]['enquiry_source_master_name'];
							$enquiry_source_id_list[$count]   = $enquiry_source_list_data[$count]['enquiry_source_master_id'];
						}
						else
						{							
							$insert_done = false;
							$sort_count	= $count - 1;
							while(($insert_done != true) && ($sort_count >= 0))
							{								
								if($number_of_leads >= $enquiry_no_leads_list[$sort_count])
								{										
									array_splice($enquiry_no_leads_list,$sort_count + 1,0,$number_of_leads);
									array_splice($enquiry_source_name_list,$sort_count+ 1,0,$enquiry_source_list_data[$count]['enquiry_source_master_name']);
									array_splice($enquiry_source_id_list,$sort_count + 1,0,$enquiry_source_list_data[$count]['enquiry_source_master_id']);
									
									$insert_done = true;
								}
								$sort_count = $sort_count - 1;
							}
							
							if($insert_done == false)
							{								
								array_unshift($enquiry_no_leads_list,$number_of_leads);
								array_unshift($enquiry_source_name_list,$enquiry_source_list_data[$count]['enquiry_source_master_name']);
								array_unshift($enquiry_source_id_list,$enquiry_source_list_data[$count]['enquiry_source_master_id']);
							}
						}	

						//var_dump($count.' '.$number_of_leads);
					}					
					
					for($count = 0; $count < count($enquiry_no_leads_list); $count++)
					{
						$sl_no = $sl_no + 1;
						
						// Initialize
						$expense_payable = 0;
						$expense_paid    = 0;
						$number_of_leads = 0;
						$number_of_sales = 0;
						$promised_leads  = 0;
						
						// Get list of marketing expenses
						$marketing_expense_list = i_get_marketing_expenses_list('',$enquiry_source_id_list[$count],$start_date,$end_date,'','','1',$added_by,'','');
						if($marketing_expense_list["status"] == SUCCESS)
						{
							$marketing_expense_list_data = $marketing_expense_list["data"];							
							
							for($expense_count = 0; $expense_count < count($marketing_expense_list_data); $expense_count++)
							{
								$expense_payable = $expense_payable + $marketing_expense_list_data[$expense_count]['crm_marketing_expenses_amount'];
								$promised_leads  = $promised_leads + $marketing_expense_list_data[$expense_count]['crm_marketing_expenses_predicted_leads'];
								
								// Get list of payments done for this marketing plan
								$expense_payment_result = i_get_marketing_payment_list('','','','','','1',$marketing_expense_list_data[$expense_count]['crm_marketing_expenses_id'],'');
								
								if($expense_payment_result['status'] == SUCCESS)
								{
									for($exp_paid_count = 0; $exp_paid_count < count($expense_payment_result['data']); $exp_paid_count++)
									{
										$expense_paid = $expense_paid + $expense_payment_result['data'][$exp_paid_count]['crm_marketing_expenses_payment_details_amount'];
										$total_expenditure = $total_expenditure + $expense_paid;
									}	
								}
							}
						}
						else
						{
							$expense_payable = 0;
						}
						
						// No of leads generated from this source
						$number_of_leads = $enquiry_no_leads_list[$count];
						$total_num_leads = $total_num_leads + $number_of_leads;
						
						// No of sales done through leads generated from this source
						$booking_sresult = i_get_site_booking('','','','','','1','','','','','','','','','','','',$start_date,$end_date,$enquiry_source_id_list[$count]);
						if($booking_sresult['status'] == SUCCESS)
						{
							$number_of_sales = count($booking_sresult['data']);
						}
						else
						{
							$number_of_sales = 0;
						}
						$total_num_sales = $total_num_sales + $number_of_sales;
						
						// Cost per lead
						if($number_of_leads > 0)
						{
							$cost_per_lead = $expense_paid/$number_of_leads;
						}
						else
						{
							$cost_per_lead = 'N.A';
						}
						
						// Cost per sale
						if($number_of_sales > 0)
						{
							$cost_per_sale = $expense_paid/$number_of_sales;
						}
						else
						{
							$cost_per_sale = 'N.A';
						}
												
					?>
					<tr id="row_<?php echo $count; ?>" style="color:black;">
						<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
						<td style="word-wrap:break-word;"><?php echo $enquiry_source_name_list[$count]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $expense_payable; ?></td>
						<td style="word-wrap:break-word;"><?php echo $expense_paid; ?></td>
						<td style="word-wrap:break-word;"><?php echo $promised_leads; ?></td>
						<td style="word-wrap:break-word;"><?php echo $number_of_leads; ?></td>
						<td style="word-wrap:break-word;"><?php echo $number_of_sales; ?></td>
						<td style="word-wrap:break-word;"><?php echo round($cost_per_lead,2); ?></td>
						<td style="word-wrap:break-word;"><?php echo round($cost_per_sale,2); ?></td>
						<td style="word-wrap:break-word;"><?php if($cost_per_lead <= TARGET_COST_PER_LEAD)
						{
							echo '<span style="color:green;">Good CPL</span>';
						}
						else
						{
							echo '<span style="color:red;">Bad CPL</span>';
						}
						?><br />
						<?php
						if($cost_per_sale <= TARGET_COST_PER_SALE)
						{
							echo '<span style="color:green;">Good CPS</span>';
						}
						else
						{
							echo '<span style="color:red;">Bad CPS</span>';
						}?></td>
					</tr>								
					<?php						
					}
				}
				else
				{
				?>
				<td colspan="10">No marketing expenses added yet!</td>
				<?php
				}
				?>	

                </tbody>
              </table>
			  <?php
			  if($total_num_leads > 0)
			  {
				  $avg_cost_per_lead = ($total_expenditure/$total_num_leads);
			  }
			  else
			  {
				  $avg_cost_per_lead = 'N.A';
			  }
			  if($total_num_sales > 0)
			  {
				  $avg_cost_per_sale = ($total_expenditure/$total_num_sales);
			  }
			  else
			  {
				  $avg_cost_per_sale = 'N.A';
			  }
			  ?>
			  <script>
			  document.getElementById("avg_cost_per_lead").innerHTML = <?php echo round($avg_cost_per_lead,2); ?>;
			  </script>
			  <script>
			  document.getElementById("avg_cost_per_sale").innerHTML = <?php echo round($avg_cost_per_sale,2); ?>;
			  </script>
            </div>
			
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    
<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>
  </body>

</html>
