<?php
$base = $_SERVER['DOCUMENT_ROOT'];
session_start(); 
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
                       
$user = $_SESSION["loggedin_user"];

        $machine_payment_id   = $_POST["hd_payment_id"];
        $amount               = $_POST["amount"];
        $machine_vendor       = $_POST["hd_machine_vendor"];
        $payment_mode         = $_POST["ddl_mode"];
        $instrument_details   = $_POST["instrument_details"];
        $remarks               = $_POST["txt_remarks"];
  
$search_machine_vendor  =$machine_vendor;
    // Get Project Actual Machine Payment modes already added
    $project_actual_machine_payment_search_data = array("active"=>'1',"status"=>"Accepted","vendor_id"=>$search_machine_vendor);
    $project_actual_machine_payment_list = i_get_project_payment_machine($project_actual_machine_payment_search_data); 

    if($project_actual_machine_payment_list['status'] == SUCCESS)
    {
        $project_actual_machine_payment_list_data = $project_actual_machine_payment_list['data'];
    } 
    
    
    if($project_actual_machine_payment_list["status"] == SUCCESS)
                {
                    $sl_no = 0;
                    $total_issued_amount = 0;
                    $total_deduction = 0;
                    $total_balance = 0;
                    $total_amount = 0;
                    for($count = 0; $count < count($project_actual_machine_payment_list_data); $count++)
                    {                        
                        //Get Delay
                        $start_date = date("Y-m-d");
                        $end_date = $project_actual_machine_payment_list_data[$count]["project_payment_machine_accepted_on"];
                        $delay = get_date_diff($end_date,$start_date);
                        
                        //Get total amount
                        $amount1 = $project_actual_machine_payment_list_data[$count]["project_payment_machine_amount"];
                        
                        
                        //Get Project Machine Vendor master List
                        $issued_amount = 0;
                        $deduction = 0;
                        $project_machine_issue_payment_search_data = array("active"=>'1',"machine_id"=>$project_actual_machine_payment_list_data[$count]["project_payment_machine_id"]);
                        $project_machine_issue_payment_list = i_get_project_machine_issue_payment($project_machine_issue_payment_search_data);
                        if($project_machine_issue_payment_list["status"] == SUCCESS)
                        {
                            $project_machine_issue_payment_list_data = $project_machine_issue_payment_list["data"];
                            for($issue_count = 0 ; $issue_count < count($project_machine_issue_payment_list_data) ; $issue_count++)
                            {
                                $issued_amount = $issued_amount + $project_machine_issue_payment_list_data[$issue_count]["project_machine_issue_payment_amount"];                                
                            }
                        }
                        else
                        {
                            $issued_amount = 0;                            
                        }
                        $balance_amount = ($amount1 - $issued_amount);

                        $final_payment[$project_actual_machine_payment_list_data[$count]["project_payment_machine_id"]] = $balance_amount ;

                    }
                }
                
               
               
foreach($final_payment as $key => $value){
    if($value > 0 && $amount > 0){
        if($amount >= $value){       

            $project_machine_issue_payment_iresult = i_add_project_machine_issue_payment($key,$value,$machine_vendor,$payment_mode,$instrument_details,$remarks,$user);
           
               $amount = $amount - $value;    
        }else{
           
            $project_machine_issue_payment_iresult = i_add_project_machine_issue_payment($key,$amount,$machine_vendor,$payment_mode,$instrument_details,$remarks,$user);
           
           $amount = $amount - $amount;       
        }
        
        
    }
}                                                   
                    $project_actual_payment_manpower_update_data = array("status"=>'Payment Issued');
                    echo json_encode($project_actual_payment_manpower_update_data);
                   
?>
 
