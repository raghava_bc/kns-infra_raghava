<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: process_list.php
CREATED ON	: 05-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Process Plans
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];	

	// Temp data
	$alert = "";

	if($role == 1)
	{
		$assigned_to = "";
	}
	else if($role == 2)
	{
		$assigned_to = "";		
	}
	else
	{
		$assigned_to = $user;		
	}

	// Get list of process plans for this module
	$legal_bulk_search_data = array("legal_process_assigned_to"=>$assigned_to);
	$legal_process_plan_list = i_get_bulk_legal_process_plan_details($legal_bulk_search_data);

	if($legal_process_plan_list["status"] == SUCCESS)
	{
		$legal_process_plan_list_data = $legal_process_plan_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$legal_process_plan_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Pending Process List - Bulk Processing</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Pending Processes List</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
					<th>SL No</th>
				    <th>File Number(s)</th>
				    <th>Survey Number(s)</th>
					<th>Process Type</th>
					<th>Start Date</th>									
					<th>Village</th>
					<th>Extent</th>					
					<th>Assigned To</th>
					<th>Planned Completion Date</th>
					<th>Actual Completion Date</th>
					<th>Variance</th>
					<th>&nbsp;</th>	
					<th>&nbsp;</th>					
				</tr>
				</thead>
				<tbody>
				 <?php
				if($legal_process_plan_list["status"] == SUCCESS)
				{		
					$sl_no = 0;
					for($count = 0; $count < count($legal_process_plan_list_data); $count++)
					{
						$sl_no++;
						$task_plan_result = i_get_task_plan_list('','','','','','','','desc',$legal_process_plan_list_data[$count]["legal_bulk_process_id"]);
						if($task_plan_result["status"] == SUCCESS)
						{
							if(get_formatted_date($task_plan_result["data"][0]["task_plan_actual_end_date"],"Y-m-d") == "0000-00-00")
							{
								$end_date = date("Y-m-d");
								$actual_date = "0000-00-00";
							}
							else
							{
								$end_date = $task_plan_result["data"][0]["task_plan_actual_end_date"];
								$actual_date = $end_date;
							}
							$start_date = $task_plan_result["data"][0]["task_plan_planned_end_date"];																	
						}
						else
						{
							$start_date = "0000-00-00";
							$end_date   = "0000-00-00";
							
							$actual_date = $end_date;
						}
						
						if($task_plan_result["status"] == SUCCESS)
						{
							$variance = get_date_diff($start_date,$end_date);
							if($variance["status"] == 1)
							{
								if((get_formatted_date($task_plan_result["data"][0]["task_plan_actual_end_date"],"Y-m-d") == "0000-00-00") || (get_formatted_date($task_plan_result["data"][0]["task_plan_actual_end_date"],"Y-m-d") == "1969-12-31"))
								{
									$css_class = "#FF0000";
								}
								else						
								{
									$css_class = "#FFA500";
								}
							}
							else
							{
								if((get_formatted_date($task_plan_result["data"][0]["task_plan_actual_end_date"],"Y-m-d") == "0000-00-00") || (get_formatted_date($task_plan_result["data"][0]["task_plan_actual_end_date"],"Y-m-d") == "1969-12-31"))
								{
									$css_class = "#000000";
								}
								else						
								{
									$css_class = "#00FF00";
								}
							}
						}
						else
						{
							$css_class = "#0000FF";
						}
						
						// Get file details												
						$project_file_number = '';
						$project_survey_number = '';
						$legal_bulk_files_search_data = array("legal_bulk_process_files_process_id"=>$legal_process_plan_list_data[$count]["legal_bulk_process_id"]);
						$process_files_result = i_get_bulk_legal_process_plan_files($legal_bulk_files_search_data);			
						$total_extent = 0;
						for($fcount = 0; $fcount < count($process_files_result['data']); $fcount++)
						{
							$project_file_number   = $project_file_number.$process_files_result['data'][$fcount]["file_number"].',';
							$project_survey_number = $project_survey_number.$process_files_result['data'][$fcount]["file_survey_number"].',';
							$total_extent          = $total_extent + $process_files_result['data'][$fcount]["file_extent"];
							$village               = $process_files_result['data'][$fcount]["village_name"];
							if($process_files_result['data'][$fcount]["village_name"] != "")
							{
								$village = $process_files_result['data'][$fcount]["village_name"]; 
							}
							else
							{
								$village = $process_files_result['data'][$fcount]["file_village"]; 
							}
						}			
						$project_file_number   = trim($project_file_number,',');
						$project_survey_number = trim($project_survey_number,',');
					?>
					<tr style="color:<?php echo $css_class; ?>">
						<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
						<td style="word-wrap:break-word;"><a href="bulk_file_list.php?bprocess=<?php echo $legal_process_plan_list_data[$count]["legal_bulk_process_id"]; ?>">Click</a></td>
						<td style="word-wrap:break-word;"><?php echo $project_survey_number; ?></td>
						<td style="word-wrap:break-word;"><?php echo $legal_process_plan_list_data[$count]["process_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($legal_process_plan_list_data[$count]["legal_process_start_date"],"d-M-Y"); ?></td>										
						<td style="word-wrap:break-word;"><?php echo $village; ?></td>
						<td style="word-wrap:break-word;"><?php echo $total_extent; ?></td>					
						<td style="word-wrap:break-word;"><?php echo $legal_process_plan_list_data[$count]["user_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($start_date,"d-M-Y"); ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($actual_date,"d-M-Y"); ?></td>
						<td style="word-wrap:break-word;"><?php echo $variance["data"]; ?></td>
						<td style="word-wrap:break-word;"><a href="pending_task_list.php?bprocess=<?php echo $legal_process_plan_list_data[$count]["legal_bulk_process_id"]; ?>"><span style="color:black; text-decoration: underline;">View Pending Tasks</span></a><br /><br /><a href="completed_task_list.php?bprocess=<?php echo $legal_process_plan_list_data[$count]["legal_bulk_process_id"]; ?>"><span style="color:black; text-decoration: underline;">View Completed Tasks</span></a></td>	
						<!-- <td><a href="create_task_plan.php?process=<?php echo $legal_process_plan_list_data[$count]["process_plan_legal_id"]; ?>">Add Tasks</a></td> -->
						<td style="word-wrap:break-word;"><a href="download.php?bprocess=<?php echo $legal_process_plan_list_data[$count]["legal_bulk_process_id"]; ?>" target="_blank"><span style="color:black; text-decoration: underline;">Download Documents<span></a><?php if($role == "1") {?><br /><br /><a href="#" onclick="return confirm_deletion(<?php echo $legal_process_plan_list_data[$count]["legal_bulk_process_id"]; ?>);"><span style="color:black; text-decoration: underline;">Delete</span></a><?php } ?></td>
					</tr>
					<?php 
					}
				}
				else
				{
				?>
				<td colspan="14">No Projects added yet</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function confirm_deletion(process)
{
	var ok = confirm("Are you sure you want to delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					 window.location = "pending_project_list.php";
				}
			}

			xmlhttp.open("GET", "legal_process_delete.php?process=" + process);   // file name where delete code is written
			xmlhttp.send();
		}
	}	
}

</script>
</script>

  </body>

</html>
