-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 20, 2018 at 07:52 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kns_erp_live`
--

-- --------------------------------------------------------

--
-- Structure for view `project_contract_work`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `project_contract_work`  AS  select `U`.`user_name` AS `added_by`,`AU`.`user_name` AS `project_task_actual_boq_approved_by`,`PTBA`.`project_task_actual_boq_approved_on` AS `project_task_actual_boq_approved_on`,`PMA`.`project_manpower_agency_name` AS `project_manpower_agency_name`,`SUM`.`stock_unit_name` AS `stock_unit_name`,`PTM`.`project_task_master_name` AS `project_task_master_name`,`PCP`.`project_contract_process_name` AS `project_contract_process_name`,`PCP`.`project_contract_process_id` AS `project_contract_process_id`,`PCRM`.`project_contract_rate_master_work_task` AS `project_contract_rate_master_work_task`,`PCRM`.`project_contract_rate_master_id` AS `project_contract_rate_master_id`,`PMPM`.`project_master_name` AS `project_master_name`,`PMPM`.`project_management_master_id` AS `project_management_master_id`,`PPM`.`project_process_master_name` AS `project_process_master_name`,`PSLMM`.`project_site_location_mapping_master_name` AS `location_name`,`APSL`.`project_site_location_mapping_master_name` AS `road_name`,`PTBA`.`project_task_actual_boq_length` AS `project_task_actual_boq_length`,`PTBA`.`project_task_actual_boq_breadth` AS `project_task_actual_boq_breadth`,`PTBA`.`project_task_actual_boq_depth` AS `project_task_actual_boq_depth`,`PTBA`.`project_task_actual_boq_date` AS `project_task_actual_boq_date`,`PTBA`.`project_task_boq_actual_number` AS `project_task_boq_actual_number`,`PTBA`.`project_task_actual_boq_amount` AS `project_task_actual_boq_amount`,`PTBA`.`project_task_actual_boq_total_measurement` AS `project_task_actual_boq_total_measurement`,`PTBA`.`project_task_actual_boq_remarks` AS `project_task_actual_boq_remarks`,`PTBA`.`project_task_actual_boq_added_on` AS `project_task_actual_boq_added_on`,`PTBA`.`project_task_actual_boq_work_type` AS `project_task_actual_boq_work_type`,`PTBA`.`project_task_actual_boq_rework_completion` AS `project_task_actual_boq_rework_completion`,`PTBA`.`project_task_actual_boq_completion` AS `project_task_actual_boq_completion`,`PTBA`.`project_task_actual_boq_lumpsum` AS `project_task_actual_boq_lumpsum`,`PTBA`.`project_task_boq_actual_id` AS `project_task_boq_actual_id`,`PTBA`.`project_task_actual_boq_status` AS `project_task_actual_boq_status`,`PTBA`.`project_boq_actual_task_id` AS `project_boq_actual_task_id`,`PTBA`.`project_task_actual_boq_active` AS `project_task_actual_boq_active`,`PTBA`.`project_task_actual_boq_road_id` AS `project_task_actual_boq_road_id`,`PTBA`.`project_task_actual_boq_vendor_id` AS `project_task_actual_boq_vendor_id`,`PTBA`.`project_task_actual_contract_task` AS `project_task_actual_contract_task`,`PTBA`.`project_task_boq_actual_contract_process` AS `project_task_boq_actual_contract_process`,`PTBA`.`project_task_actual_boq_location` AS `project_task_actual_boq_location`,`PTBA`.`project_task_actual_boq_bill_status` AS `project_task_actual_boq_bill_status`,`PTBA`.`project_task_actual_boq_uom` AS `project_task_actual_boq_uom` from ((((((((((((((`project_task_boq_actuals` `PTBA` join `users` `U` on((`U`.`user_id` = `PTBA`.`project_task_actual_boq_added_by`))) join `stock_unit_measure_master` `SUM` on((`SUM`.`stock_unit_id` = `PTBA`.`project_task_actual_boq_uom`))) join `project_plan_process_task` `PPPT` on((`PPPT`.`project_process_task_id` = `PTBA`.`project_boq_actual_task_id`))) join `project_task_master` `PTM` on((`PTM`.`project_task_master_id` = `PPPT`.`project_process_task_type`))) join `project_contract_process` `PCP` on((`PCP`.`project_contract_process_id` = `PTBA`.`project_task_boq_actual_contract_process`))) join `project_contract_rate_master` `PCRM` on((`PCRM`.`project_contract_rate_master_id` = `PTBA`.`project_task_actual_contract_task`))) join `project_plan_process` `PPP` on((`PPP`.`project_plan_process_id` = `PPPT`.`project_process_id`))) join `project_plan` `PP` on((`PP`.`project_plan_id` = `PPP`.`project_plan_process_plan_id`))) join `project_management_project_master` `PMPM` on((`PMPM`.`project_management_master_id` = `PP`.`project_plan_project_id`))) join `project_process_master` `PPM` on((`PPM`.`project_process_master_id` = `PPP`.`project_plan_process_name`))) join `project_manpower_agency` `PMA` on((`PMA`.`project_manpower_agency_id` = `PTBA`.`project_task_actual_boq_vendor_id`))) left join `users` `AU` on((`AU`.`user_id` = `PTBA`.`project_task_actual_boq_approved_by`))) left join `project_site_location_mapping_master` `PSLMM` on((`PSLMM`.`project_site_location_mapping_master_id` = `PTBA`.`project_task_actual_boq_location`))) left join `project_site_location_mapping_master` `APSL` on((`APSL`.`project_site_location_mapping_master_id` = `PTBA`.`project_task_actual_boq_road_id`))) ;

--
-- VIEW  `project_contract_work`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
