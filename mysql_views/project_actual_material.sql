-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 22, 2018 at 01:39 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kns_erp_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `project_actual_material`
--

CREATE TABLE IF NOT EXISTS `project_actual_material` (
  `actual_material_id` int(11) NOT NULL AUTO_INCREMENT,
  `actual_material_indent_id` int(11) NOT NULL,
  `actual_material_issue_item_id` int(11) NOT NULL,
  `actual_material_project_id` int(11) NOT NULL,
  `actual_material_process_id` int(11) NOT NULL,
  `actual_material_task_id` int(11) NOT NULL,
  `actual_material_road_id` char(20) NOT NULL,
  `actual_material_master_id` int(11) NOT NULL,
  `actual_material_machine_id` int(11) NOT NULL,
  `actual_material_qty` int(11) NOT NULL,
  `actual_material_remarks` varchar(200) NOT NULL,
  `actual_material_added_by` char(20) NOT NULL,
  `actual_material_added_on` datetime NOT NULL,
  PRIMARY KEY (`actual_material_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `project_actual_material`
--

INSERT INTO `project_actual_material` (`actual_material_id`, `actual_material_indent_id`, `actual_material_issue_item_id`, `actual_material_project_id`, `actual_material_process_id`, `actual_material_task_id`, `actual_material_road_id`, `actual_material_master_id`, `actual_material_machine_id`, `actual_material_qty`, `actual_material_remarks`, `actual_material_added_by`, `actual_material_added_on`) VALUES
(1, 750, 7986, 11, 2, 2, 'No Roads', 253, 0, 12, 'test', '143620071466608200', '2018-06-21 18:20:01'),
(2, 750, 7987, 11, 2, 2, 'No Roads', 253, 0, 1, 'test', '143620071466608200', '2018-06-22 10:27:16'),
(3, 750, 7988, 11, 2, 2, 'No Roads', 253, 0, 1, 'test', '143620071466608200', '2018-06-22 10:29:40'),
(4, 750, 7989, 11, 1, 1, 'No Roads', 253, 0, 1, 'teest', '143620071466608200', '2018-06-22 10:31:54'),
(5, 5448, 7990, 29, 506, 2391, 'No Roads', 1363, 0, 1, 'test', '143620071466608200', '2018-06-22 16:49:33'),
(6, 5448, 7991, 29, 504, 2484, 'No Roads', 1363, 0, 1, 'test', '143620071466608200', '2018-06-22 16:51:44'),
(7, 5133, 7992, 1, 142, 1600, 'No Roads', 297, 0, 1, 'test', '143620071466608200', '2018-06-22 17:01:28');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
