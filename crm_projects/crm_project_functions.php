<?php
/**
 * @author Nitin kashyap
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_crm_projects.php');

/*
PURPOSE : To add project
INPUT 	: Project Name, Address, Client, Location, Authority, USP, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_project($project_name,$address,$client,$location,$authority,$usp,$added_by)
{
	$project_sresult = db_get_project_list($project_name,'','','','');
	
	if($project_sresult["status"] == DB_NO_RECORD)
	{
		$project_iresult = db_add_project($project_name,$address,$client,$location,$authority,$usp,$added_by);
		
		if($project_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Project Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Project already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get project list
INPUT 	: Project Name, Active
OUTPUT 	: Project List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_project_list($project_name,$active)
{
	$project_sresult = db_get_project_list($project_name,$active,'','','');
	
	if($project_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $project_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No project added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add site to project
INPUT 	: Project, Site No, Block, Wing, Dimension, Area, Cost, Site Type, Remarks, Site Status, Release Status, Bank, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_site_to_project($project_id,$site_number,$block,$wing,$dimension_master,$area,$cost,$site_type,$remarks,$site_status,$rel_status,$bank,$added_by)
{
	$site_project_sresult = db_get_site_list($site_number,$project_id,'','','','','','','','','');
	
	if($site_project_sresult["status"] == DB_NO_RECORD)
	{
		$site_project_iresult = db_add_site_to_project($site_number,$project_id,$block,$wing,$dimension_master,$area,$cost,$site_type,$remarks,$site_status,$rel_status,$bank,$added_by);
		
		if($site_project_iresult['status'] == SUCCESS)
		{
			$return["data"]   = $site_project_iresult["data"];
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Site already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get site list
INPUT 	: Site No, Project, Featured, Type, Dimension, Status, Release Status, Site ID
OUTPUT 	: Site List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_site_list($site_number,$project,$featured,$type,$dimension,$status,$release_status,$site_id="",$project_user="")
{
	$site_sresult = db_get_site_list($site_number,$project,$featured,$type,$dimension,$status,$release_status,'','','',$site_id,'0',$project_user);
	
	if($site_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $site_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No site available!"; 
    }
	
	return $return;
}

/*
PURPOSE : To update the status of a site
INPUT 	: Site ID, Status, Updated By
OUTPUT 	: Success or Failure Message
BY 		: Nitin Kashyap
*/
function i_update_site_status($site_id,$status,$updated_by)
{
	$site_uresult = db_change_status($site_id,$status,$updated_by);
	
	if($site_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = "Site status updated successfully!"; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No site available!"; 
    }
	
	return $return;
}

/*
PURPOSE : To add survey to site
INPUT 	: Survey ID, Site ID, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_survey_to_site($survey_id,$site_id,$added_by)
{
	$site_survey_sresult = db_get_site_survey_list($site_id,$survey_id,$added_by,'','');
	
	if($site_survey_sresult["status"] == DB_NO_RECORD)
	{
		$site_survey_iresult = db_add_site_to_survey($survey_id,$site_id,$added_by);
		
		if($site_survey_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Survey Successfully Mapped to Site";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Survey Successfully Mapped to site";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To add survey number
INPUT 	: Survey No, Project, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_survey_no($survey_no,$project,$added_by)
{
	$survey_no_sresult = db_get_survey_no_list($survey_no,$project,'1',$added_by,'','');
	
	if($survey_no_sresult["status"] == DB_NO_RECORD)
	{
		$survey_iresult = db_add_survey_no($survey_no,$project,$added_by);
		
		if($survey_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Survey Number Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Survey Number already exists for this project!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get survey number list
INPUT 	: Survey Number, Project, Active
OUTPUT 	: Survey Number List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_survey_number_list($survey_no,$project,$active)
{
	$survey_sresult = db_get_survey_no_list($survey_no,$project,$active,'','','');
	
	if($survey_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $survey_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No survey number added for this project. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To get site survey details
INPUT 	: Survey ID, Site ID, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_site_survey($survey_id,$site_id,$added_by)
{
	$site_survey_sresult = db_get_site_survey_list($site_id,$survey_id,$added_by,'','');
	
	if($site_survey_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$return["data"]   = $site_survey_sresult["data"];
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "Survey Successfully Mapped to site";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To update the status of a site
INPUT 	: Site Details Array
OUTPUT 	: Success or Failure Message
BY 		: Nitin Kashyap
*/
function i_update_site_details($site_data_array,$user)
{
	$site_uresult = db_update_site_details($site_data_array,$user);
	
	if($site_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = "Site details updated successfully!"; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "There was an internal error. Please contact the admin!"; 
    }
	
	return $return;
}

/*
PURPOSE : To get site summary
INPUT 	: Site Summary Array
OUTPUT 	: Site Summary List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_site_summary($site_summary_array)
{
	$site_summary_sresult = db_get_site_summary($site_summary_array);
	
	if($site_summary_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $site_summary_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No project added. Please contact the system admin"; 
    }
	
	return $return;
}/*PURPOSE : To add new CRM User Project MappingINPUT 	: User ID, Project ID, Remarks, Added ByOUTPUT 	: Mapping ID, success or failure messageBY 		: Lakshmi*/function i_add_crm_user_project_mapping($user_id,$project_id,$remarks,$added_by){	$crm_user_project_mapping_search_data = array("user_id"=>$user_id,"project_id"=>$project_id,"active"=>'1');	$user_mapping_sresult = db_get_crm_user_project_mapping($crm_user_project_mapping_search_data);	if($user_mapping_sresult["status"] == DB_NO_RECORD)	{	$user_mapping_sresult =  db_add_crm_user_project_mapping($user_id,$project_id,$remarks,$added_by);		if($user_mapping_sresult['status'] == SUCCESS)	{		$return["data"]   = "User Project Mapping Successfully Added";		$return["status"] = SUCCESS;		}		else		{			$return["data"]   = "Internal Error. Please try again later";			$return["status"] = FAILURE;		}	}	else	{		$return["data"]   = "This User Mapping is already exist for this Project";		$return["status"] = FAILURE;	}				return $return;}/*PURPOSE : To get CRM User Project Mapping ListINPUT 	: Mapping ID, User ID, Project ID, Active, Added By, Start Date(for added on), End Date(for added on)OUTPUT 	: List of User Project MappingBY 		: Lakshmi*/function i_get_crm_user_project_mapping($crm_user_project_mapping_search_data){	$user_mapping_sresult = db_get_crm_user_project_mapping($crm_user_project_mapping_search_data);		if($user_mapping_sresult['status'] == DB_RECORD_ALREADY_EXISTS)    {		$return["status"] = SUCCESS;        $return["data"]   =$user_mapping_sresult["data"];     }    else    {	    $return["status"] = FAILURE;        $return["data"]   = "No User Project Mapping added. Please Contact The System Admin";     }		return $return; }/*PURPOSE : To update CRM User Project MappingINPUT 	: Mapping ID, User Project Mapping Update ArrayOUTPUT 	: Mapping ID; Message of success or failureBY 		: Lakshmi*/function i_update_crm_user_project_mapping($mapping_id,$user_id,$project_id,$crm_user_project_mapping_update_data){	$crm_user_project_mapping_search_data = array("user_id"=>$user_id,"project_id"=>$project_id,"active"=>'1');	$user_mapping_sresult = db_get_crm_user_project_mapping($crm_user_project_mapping_search_data);		$allow_update = false;	if($user_mapping_sresult["status"] == DB_NO_RECORD)	{		$allow_update = true;	}	else if($user_mapping_sresult["status"] == DB_RECORD_ALREADY_EXISTS)	{		if($user_mapping_sresult['data'][0]['crm_user_project_mapping_id'] == $mapping_id)		{			$allow_update = true;		}	}		if($allow_update == true)	{		$user_mapping_sresult = db_update_crm_user_project_mapping($mapping_id,$crm_user_project_mapping_update_data);				if($user_mapping_sresult['status'] == SUCCESS)		{			$return["data"]   = "User Project Mapping Successfully Updated";			$return["status"] = SUCCESS;									}		else		{			$return["data"]   = "Internal Error. Please try again later";			$return["status"] = FAILURE;		}	}	else	{		$return["data"]   = "User Project Mapping Already Exists";		$return["status"] = FAILURE;	}		return $return;}/*PURPOSE : To Delete CRM User Project MappingINPUT 	: Mapping ID, User Project Mapping Update ArrayOUTPUT 	: Mapping ID; Message of success or failureBY 		: Lakshmi*/function i_delete_crm_user_project_mapping($mapping_id,$crm_user_project_mapping_update_data){       $crm_user_project_mapping_update_data = array('active'=>'0');	$user_mapping_sresult = db_update_crm_user_project_mapping($mapping_id,$crm_user_project_mapping_update_data);		if($user_mapping_sresult['status'] == SUCCESS)	{		$return["data"]   = "User  Project Mapping Successfully Added";		$return["status"] = SUCCESS;								}	else	{		$return["data"]   = "Internal Error. Please try again later";		$return["status"] = FAILURE;	}		return $return;}
?>