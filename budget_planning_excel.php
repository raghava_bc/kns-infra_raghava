<?php
  session_start();
  $_SESSION['module'] = 'PM Masters';

  /* DEFINES - START */
  define('PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID', '359');
  /* DEFINES - END */

  // Includes
  $base = $_SERVER["DOCUMENT_ROOT"];
  require dirname(__FILE__) . '/utilities/PHPExcel-1.8/Classes/PHPExcel.php';
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
  include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

  if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '2', '1');
    $edit_perms_list   = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '3', '1');
    $delete_perms_list = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '4', '1');
    $add_perms_list    = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '1', '1');

    // Query String Data
    // Nothing

    // Temp data
    $alert_type = -1;
    $alert 	    = "";

    if (isset($_REQUEST['project_id'])) {
      $project_id = $_REQUEST['project_id'];
    } else {
      $project_id = "";
    }

    // Get Already added Object Output
    $project_task_planning_search_data = array("active"=>'1',"project_id"=>$project_id);

    $project_task_planning_list = i_get_project_task_planning($project_task_planning_search_data);
    if ($project_task_planning_list["status"] == SUCCESS) {
      $project_task_planning_list_data = $project_task_planning_list["data"];
    } else {
      $alert = $alert."Alert: ".$project_task_planning_list["data"];
    }

    $project_name = $project_task_planning_list["data"][0]['project_master_name'];
    $filename = $project_name."-exported-data_".date("m-d-Y-h-m-s").".xls";

    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // PRINT HEADERS
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Project')
                ->setCellValue('B1', 'planned_project_id')
                ->setCellValue('C1', 'Process_name')
                ->setCellValue('D1', 'planned_process_id')
                ->setCellValue('E1', 'Task')
                ->setCellValue('F1', 'planned_task_id')
                ->setCellValue('G1', 'Road Number')
                ->setCellValue('H1', 'planned_road_id')
                ->setCellValue('I1', 'UOM')
                ->setCellValue('J1', 'Qty')
                ->setCellValue('K1', 'Manpower')
                ->setCellValue('L1', 'Machine')
                ->setCellValue('M1', 'Contract')
                ->setCellValue('N1', 'Material');

      for ($count = 0; $count < count($project_task_planning_list_data); $count++) {
        $project_planned_data = array("task_id"=>$project_task_planning_list_data[$count]["project_task_planning_task_id"],"road_id"=>$project_task_planning_list_data[$count]["project_task_planning_no_of_roads"]);
        $planned_data_list = db_get_planned_data($project_planned_data);
        if($planned_data_list["status"] == DB_RECORD_ALREADY_EXISTS)
        {
          $planned_mp = $planned_data_list["data"][0]["planned_manpower"];
          $planned_mc = $planned_data_list["data"][0]["planned_machine"];
          $planned_cw = $planned_data_list["data"][0]["planned_contract"];
          $planned_material = $planned_data_list["data"][0]["planned_material"];
        }
        else {
          $planned_mp = 0;
          $planned_mc = 0;
          $planned_cw = 0;
          $planned_material = 0;
        }

        if (($project_task_planning_list_data[$count]["project_task_planning_no_of_roads"] != "No Roads")) {
            $road_name = $project_task_planning_list_data[$count]["project_site_location_mapping_master_name"];
        } else {
            $road_name = "No Roads";
        }

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.($count+2), $project_task_planning_list_data[$count]["project_master_name"])
                    ->setCellValue('B'.($count+2), $project_task_planning_list_data[$count]["project_management_master_id"])
                    ->setCellValue('C'.($count+2), $project_task_planning_list_data[$count]["project_process_master_name"])
                    ->setCellValue('D'.($count+2), $project_task_planning_list_data[$count]["project_plan_process_id"])
                    ->setCellValue('E'.($count+2), $project_task_planning_list_data[$count]["project_task_master_name"])
                    ->setCellValue('F'.($count+2), $project_task_planning_list_data[$count]["project_process_task_id"])
                    ->setCellValue('G'.($count+2), $road_name)
                    ->setCellValue('H'.($count+2), $project_task_planning_list_data[$count]["project_task_planning_no_of_roads"])
                    ->setCellValue('I'.($count+2), $project_task_planning_list_data[$count]["project_uom_name"])
                    ->setCellValue('J'.($count+2), $project_task_planning_list_data[$count]["project_task_planning_measurment"])
                    ->setCellValue('K'.($count+2), $planned_mp)
                    ->setCellValue('L'.($count+2), $planned_mc)
                    ->setCellValue('M'.($count+2), $planned_cw)
                    ->setCellValue('N'.($count+2), $planned_material);
    }

    // Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle($project_name);

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
  } else {
    header("location:login.php");
  }
