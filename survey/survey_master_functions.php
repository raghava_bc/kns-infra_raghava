<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_survey_project_masters.php');

/*
PURPOSE : To add new Survey Process Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_survey_process_master($name,$order,$process_type,$remarks,$added_by)
{
	$survey_process_master_search_data = array("name"=>$name,"process_name_check"=>'1',"active"=>'1');
	$survey_process_master_sresult = db_get_survey_process_master($survey_process_master_search_data);
	
	if($survey_process_master_sresult["status"] == DB_NO_RECORD)
	{
		$survey_process_master_iresult = db_add_survey_process_master($name,$order,$process_type,$remarks,$added_by);
		
		if($survey_process_master_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Survey Process Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Survey Process Name already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Survey Process Master List
INPUT 	: Master ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey Process Master, success or failure message
BY 		: Lakshmi
*/
function i_get_survey_process_master($survey_process_master_search_data)
{
	$survey_process_master_sresult = db_get_survey_process_master($survey_process_master_search_data);
	
	if($survey_process_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $survey_process_master_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Survey Process Master Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Survey Process Master 
INPUT 	: Master ID, Survey Process Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_survey_process_master($master_id,$survey_process_master_update_data)
{
	$survey_process_master_search_data = array("name"=>$survey_process_master_update_data['name'],"process_name_check"=>'1',"active"=>'1');
	$survey_process_master_sresult = db_get_survey_process_master($survey_process_master_search_data);
	
	$allow_update = false;
	if($survey_process_master_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($survey_process_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($survey_process_master_sresult['data'][0]['survey_process_master_id'] == $master_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$survey_process_master_sresult = db_update_survey_process_master($master_id,$survey_process_master_update_data);
		
		if($survey_process_master_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Survey Process Master Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Survey Name Already Exists";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To Delete Survey Process Master 
INPUT 	: Master ID, Survey Process Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_survey_process_master($master_id,$survey_process_master_update_data)
{   
    $survey_process_master_update_data = array('active'=>'0');
	$survey_process_master_sresult = db_update_survey_process_master($master_id,$survey_process_master_update_data);
	
	if($survey_process_master_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Survey Process Master Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new Survey Delay Reason Master 
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_survey_delay_reason_master($name,$remarks,$added_by)
{
	$survey_delay_reason_master_search_data = array("name"=>$name,"reason_name_check"=>'1',"active"=>'1');
	$survey_delay_reason_master_sresult = db_get_survey_delay_reason_master($survey_delay_reason_master_search_data);
	
	if($survey_delay_reason_master_sresult["status"] == DB_NO_RECORD)
	{
		$survey_delay_reason_master_iresult =  db_add_survey_delay_reason_master($name,$remarks,$added_by);
		
		if($survey_delay_reason_master_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Survey Delay Reason Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Delay Reason name already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Survey Delay Reason Master List
INPUT 	: Reason ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey Delay Reason Master, success or failure message
BY 		: Lakshmi
*/
function i_get_survey_delay_reason_master($survey_delay_reason_master_search_data)
{
	$survey_delay_reason_master_sresult = db_get_survey_delay_reason_master($survey_delay_reason_master_search_data);
	
	if($survey_delay_reason_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$survey_delay_reason_master_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Survey Delay Reason Master Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Survey Delay Reason Master 
INPUT 	: Reason ID, Survey Delay Reason Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_survey_delay_reason_master($reason_id,$survey_delay_reason_master_update_data)
{
	$survey_delay_reason_master_search_data = array("name"=>$survey_delay_reason_master_update_data['name'],"reason_name_check"=>'1',"active"=>'1');
	$survey_delay_reason_master_sresult = db_get_survey_delay_reason_master($survey_delay_reason_master_search_data);
	
	$allow_update = false;
	if($survey_delay_reason_master_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($survey_delay_reason_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($survey_delay_reason_master_sresult['data'][0]['survey_delay_reason_master_id'] == $reason_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$survey_delay_reason_master_sresult = db_update_survey_delay_reason_master($reason_id,$survey_delay_reason_master_update_data);
		
		if($survey_delay_reason_master_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Survey Delay Reason Details Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Delay Reason Name Already Exists";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To update Survey Delay Reason Master 
INPUT 	: Reason ID, Survey Delay Reason Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_survey_delay_reason_master($reason_id,$survey_delay_reason_master_update_data)
{    
    $survey_delay_reason_master_update_data = array('active'=>'0');
	$survey_delay_reason_master_sresult = db_update_survey_delay_reason_master($reason_id,$survey_delay_reason_master_update_data);
	
	if($survey_delay_reason_master_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Survey Delay Reason Master Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new Survey Master
INPUT 	: Village, Survey No, Land Owner, Extent, Remarks, Added By
OUTPUT 	: Survey ID, success or failure message
BY 		: Lakshmi
*/	
function i_add_survey_master($project,$parent_survey_id,$village,$survey_no,$land_owner,$extent,$remarks,$added_by)
{   
	$survey_master_search_data = array("village"=>$village,"survey_no"=>$survey_no,"land_owner"=>$land_owner,"active"=>'1');
	$survey_master_sresult = db_get_survey_master($survey_master_search_data);
	
	if($survey_master_sresult["status"] == DB_NO_RECORD)
	{
		$survey_master_iresult = db_add_survey_master($project,$parent_survey_id,$village,$survey_no,$land_owner,$extent,$remarks,$added_by);
		
		if($survey_master_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Survey Master Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Survey Master already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}


/*
PURPOSE : To get Survey Master list
INPUT 	: Survey ID, Village, Survey No, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey Master
BY 		: Lakshmi
*/
function i_get_survey_master($survey_master_search_data)
{
	$survey_master_sresult = db_get_survey_master($survey_master_search_data);
	if($survey_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $survey_master_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No  Survey Master Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Survey Master
INPUT 	: Survey ID, Survey Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_survey_master($survey_id,$survey_master_update_data)
{ 
   $survey_master_search_data = array("village"=>$survey_master_update_data['village'],"survey_no"=>$survey_master_update_data['survey_no'],"land_owner"=>$survey_master_update_data['land_owner'],"active"=>'1');
	$survey_master_sresult = db_get_survey_master($survey_master_search_data);
	
	$allow_update = false;
	if($survey_master_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($survey_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($survey_master_sresult['data'][0]['survey_master_id'] == $survey_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$survey_master_sresult = i_update_survey_master($survey_id,$survey_master_update_data);
		
		if($survey_master_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Survey Master Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Survey No Already Exists";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To Delete Survey Master
INPUT 	: Survey ID, Survey Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_delete_survey_master($survey_id,$survey_master_update_data)
{   
    $survey_master_update_data = array('active'=>'0');    
	$survey_master_sresult = db_update_survey_master($survey_id,$survey_master_update_data);
	
	if($survey_master_sresult['status'] == SUCCESS)
	{
		$return["data"]   = " Survey Master Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new Survey Document Type Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Document Type ID, success or failure message
BY 		: Lakshmi
*/
function i_add_survey_document_type_master($name,$remarks,$added_by)
{
	$survey_document_type_master_search_data = array("name"=>$name,"document_type_name_check"=>'1',"active"=>'1');
	$survey_document_type_master_sresult = db_get_survey_document_type_master($survey_document_type_master_search_data);
	
	if($survey_document_type_master_sresult["status"] == DB_NO_RECORD)
	{
		$survey_document_type_master_iresult = db_add_survey_document_type_master($name,$remarks,$added_by);
		
		if($survey_document_type_master_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Survey Document Type Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Survey Document Type Name already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Survey Document Type Master List
INPUT 	: Document Type ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Document Type Master
BY 		: Lakshmi
*/
function i_get_survey_document_type_master($survey_document_type_master_search_data)
{
	$survey_document_type_master_sresult = db_get_survey_document_type_master($survey_document_type_master_search_data);
	
	if($survey_document_type_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $survey_document_type_master_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Survey Document Type Master Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Survey Document Type Master 
INPUT 	: Document Type ID, Document Type Master Update Array
OUTPUT 	: Document Type ID; Message of success or failure
BY 		: Lakshmi
*/
function i_update_survey_document_type_master($document_type_id,$survey_document_type_master_update_data)
{
	$survey_document_type_master_search_data = array("name"=>$survey_process_master_update_data['name'],"document_type_name_check"=>'1',"active"=>'1');
	$survey_document_type_master_sresult = db_get_survey_document_type_master($survey_document_type_master_search_data);
	
	$allow_update = false;
	if($survey_document_type_master_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($survey_document_type_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($survey_document_type_master_sresult['data'][0]['survey_document_type_master_id'] == $document_type_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$survey_document_type_master_sresult = db_update_survey_document_type_master($document_type_id,$survey_document_type_master_update_data);
		
		if($survey_document_type_master_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Survey Document Type Details Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Survey Document Type Already Exists";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To Delete Survey Document Type Master 
INPUT 	: Document Type ID, Survey Document Type Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_survey_document_type_master($document_type_id,$survey_document_type_master_update_data)
{   
    $survey_document_type_master_update_data = array('active'=>'0');
	$survey_document_type_master_sresult = db_update_survey_document_type_master($document_type_id,$survey_document_type_master_update_data);
	
	if($survey_document_type_master_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Survey Document Type Master Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new Survey Project Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Project ID, success or failure message
BY 		: Lakshmi
*/
function i_add_survey_project_master($name,$remarks,$added_by)
{
	$survey_project_master_search_data = array("name"=>$name,"project_name_check"=>'1',"active"=>'1');
	$survey_project_master_sresult = db_get_survey_project_master($survey_project_master_search_data);
	
	if($survey_project_master_sresult["status"] == DB_NO_RECORD)
	{
		$survey_project_master_iresult = db_add_survey_project_master($name,$remarks,$added_by);
		
		if($survey_project_master_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Survey Project Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Survey project Name already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Survey Project Master List
INPUT 	: Project ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey project Master, success or failure message
BY 		: Lakshmi
*/
function i_get_survey_project_master($survey_project_master_search_data)
{
	$survey_project_master_sresult = db_get_survey_project_master($survey_project_master_search_data);
	
	if($survey_project_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $survey_project_master_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Survey Project Master Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Survey Project Master 
INPUT 	: Project ID, Survey Project Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_survey_project_master($project_id,$survey_project_master_update_data)
{
	$survey_project_master_search_data = array("name"=>$survey_project_master_update_data['name'],"project_name_check"=>'1',"active"=>'1');
	$survey_project_master_sresult = db_get_survey_project_master($survey_project_master_search_data);
	
	$allow_update = false;
	if($survey_project_master_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($survey_project_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($survey_project_master_sresult['data'][0]['survey_project_master_id'] == $project_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$survey_project_master_sresult = db_update_survey_project_master($project_id,$survey_project_master_update_data);
		
		if($survey_project_master_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Survey Project Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Survey Project Name Already Exists";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To Delete Survey Project Master 
INPUT 	: Project ID, Survey Project Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_survey_project_master($project_id,$survey_project_master_update_data)
{   
    $survey_project_master_update_data = array('active'=>'0');
	$survey_project_master_sresult = db_update_survey_project_master($project_id,$survey_project_master_update_data);
	
	if($survey_project_master_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Survey Project Master Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}
?>