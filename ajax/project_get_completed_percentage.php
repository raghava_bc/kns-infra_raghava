<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_process_master_list.php
CREATED ON	: 0*-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Query String Data
    if (isset($_REQUEST['process_id'])) {
        $process_id = $_REQUEST['process_id'];
    } else {
        $process_id = "";
    }

    if (isset($_REQUEST['actual_start_date'])) {
        $actual_start_date = $_REQUEST['actual_start_date'];
    } else {
        $actual_start_date = "";
    }

    if (isset($_REQUEST['actual_end_date'])) {
        $actual_end_date = $_REQUEST['actual_end_date'];
    } else {
        $actual_end_date = "";
    }

    if (isset($_REQUEST['pdays'])) {
        $pdays = $_REQUEST['pdays'];
    } else {
        $pdays = "";
    }
    //Get Project Process Task Manpower
    $total_completion_percentage = 0;
    $total_machine_completion_percentage = 0;
    $total_contract_completion_percentage = 0;
    $project_process_task_search_data = array("process_id"=>$process_id,"active"=>'1');
    $project_process_task_list = i_get_project_process_task($project_process_task_search_data);
    if($project_process_task_list["status"] == SUCCESS)
    {
      $project_process_task_list_data = $project_process_task_list["data"];
      $no_of_task = count($project_process_task_list_data);
      $total_value = 0;
      $total_avg_percentage = 0;
      for($t_count = 0 ; $t_count < count($project_process_task_list_data) ; $t_count++)
      {
        $avg_count = 0;
        $task_id = $project_process_task_list_data[$t_count]["project_process_task_id"];
        //Get actual man power List
        $man_power_search_data = array("active"=>'1',"max_percentage"=>'1',"task_id"=>$project_process_task_list_data[$t_count]["project_process_task_id"],"work_type"=>"Regular");
        $man_power_list = i_get_man_power_list($man_power_search_data);
        if($man_power_list["status"] == SUCCESS)
        {
          $man_power_list_data = $man_power_list["data"];
          $manpower_percentage = $man_power_list_data[0]["max_percentage"];
        }
        else
        {
          $manpower_percentage = 0;
        }

        //Get Actual Machine List
        $actual_machine_plan_search_data = array("active"=>'1',"max_machine_percentage"=>'1',"task_id"=>$project_process_task_list_data[$t_count]["project_process_task_id"],"work_type"=>"Regular");
        $actual_machine_plan_list = i_get_machine_planning_list($actual_machine_plan_search_data);
        if($actual_machine_plan_list["status"] == SUCCESS)
        {
          $actual_machine_plan_list_data = $actual_machine_plan_list["data"];
          $machine_percentage = $actual_machine_plan_list_data[0]["max_mpercentage"];
        }
        else
        {
          $machine_percentage = "0";
        }
        //Get contract List data
        $project_task_boq_actual_search_data = array("active"=>'1',"max_contract_percentage"=>'1',"task_id"=>$project_process_task_list_data[$t_count]["project_process_task_id"],"work_type"=>"Regular");
        $project_task_boq_actual_list = i_get_project_task_boq_actual($project_task_boq_actual_search_data);
        if($project_task_boq_actual_list['status'] == SUCCESS)
        {
          $project_task_boq_actual_list_data = $project_task_boq_actual_list['data'];
          $contract_percentage = $project_task_boq_actual_list_data[0]["max_cpercentage"];
        }
        else
        {
          $contract_percentage = 0;
        }

        // Actual Manpower data
        $man_power_search_data = array("active"=>'1',"task_id"=>$project_process_task_list_data[$t_count]["project_process_task_id"],"work_type"=>"Regular");
        $man_power_list = i_get_man_power_list($man_power_search_data);
        if($man_power_list["status"] == SUCCESS)
        {
          $avg_count = $avg_count + 1;
        }
        else
        {
          // Get Project Man Power Estimate modes already added
          $project_man_power_estimate_search_data = array("active"=>'1',"task_id"=>$project_process_task_list_data[$t_count]["project_process_task_id"]);
          $project_man_power_estimate_list = i_get_project_man_power_estimate($project_man_power_estimate_search_data);
          if($project_man_power_estimate_list['status'] == SUCCESS)
          {
            $avg_count = $avg_count + 1;
          }
          else
          {
            //Do not Inncrement
          }
        }

        // Machine Planning data
        $actual_machine_plan_search_data = array("active"=>'1',"task_id"=>$project_process_task_list_data[$t_count]["project_process_task_id"],"work_type"=>"Regular");
        $actual_machine_plan_list = i_get_machine_planning_list($actual_machine_plan_search_data);
        if($actual_machine_plan_list["status"] == SUCCESS)
        {
          $avg_count = $avg_count + 1;
        }
        else
        {
          //Machine Planning
          $project_machine_planning_search_data = array("active"=>'1',"task_id"=>$project_process_task_list_data[$t_count]["project_process_task_id"]);
          $project_machine_planning_list = i_get_project_machine_planning($project_machine_planning_search_data);
          if($project_machine_planning_list["status"] == SUCCESS)
          {
            $avg_count = $avg_count + 1;
          }
          else
          {
            //Do not Increment
          }
        }

        // Actual Contract  Data
        $project_task_boq_actual_search_data = array("active"=>'1',"task_id"=>$project_process_task_list_data[$t_count]["project_process_task_id"],"work_type"=>"Regular");
        $project_task_boq_actual_list = i_get_project_task_boq_actual($project_task_boq_actual_search_data);
        if($project_task_boq_actual_list['status'] == SUCCESS)
        {
          $avg_count = $avg_count + 1;
        }
        else
        {
          $project_task_boq_search_data = array("active"=>'1',"task_id"=>$project_process_task_list_data[$t_count]["project_process_task_id"]);
          $project_task_boq_list = i_get_project_task_boq($project_task_boq_search_data);
          if($project_task_boq_list['status'] == SUCCESS)
          {
            $avg_count = $avg_count + 1;
          }
          else
          {
            ///Do not Increment
          }
        }


        $total_percentage = $manpower_percentage + $machine_percentage + $contract_percentage ;
        if($avg_count != 0)
        {
          $avg_percentage = $total_percentage/$avg_count;
        }
        else
        {
          $avg_percentage = 0;
        }

        $total_avg_percentage = $total_avg_percentage + $avg_percentage;
        $overall_percentage = $total_avg_percentage/$no_of_task ;
        $overall_pending_percentage = 100 - $overall_percentage;
      }

    }
    else
    {
      $overall_percentage = 0;
      $overall_pending_percentage = 100;
    }

    //Calculate actual days
    $today = date("Y-m-d");
    if($overall_percentage == '100' )
    {
      $actual_days = get_date_diff($actual_start_date,$actual_end_date);
      $actual_uend_date  = get_formatted_date($actual_end_date,"d-M-Y") ;

    }
    else
    {
      $actual_days = get_date_diff($actual_start_date,$today);
      $actual_uend_date = " ";

    }
    if($actual_days["status"] !=2 )
    {
      $actual_days = $actual_days["data"] + 1 ;
    }
    else {
      $actual_days = $actual_days ;
    }

    if($actual_days["status"] != "2")
    {
      $varience =  $actual_days - $pdays;
    }
    else{
      echo '';
    }

    $data_value = array(
      "cp"=>round($overall_percentage),
      "pending"=>100-round($overall_percentage),
      "process_id"=>$process_id,
      "element_id"=>"#cp_".$process_id,
      "actual_days" => $actual_days,
      "actual_end_date"=>$actual_uend_date,
      "varience"=>$varience
    );
    echo json_encode($data_value) ;

} else {
    header("location:login.php");
}
