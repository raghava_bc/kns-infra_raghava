<?php

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');



/*

LIST OF TBDs

1. Wherever there is name search, there should be a wildcard

*/

/*

PURPOSE : To add new Stock Purchase Order

INPUT 	: Order Number,Quotation Id,Order Date,Status,Transportation,Deductions,Location,Terms,Remarks,Updated By,Updated On,

          Added By,Added On

OUTPUT 	: Order Id, success or failure message

BY 		: Lakshmi

*/
function db_add_stock_purchase_order($order_number,$quotation_id,$order_date,$transportation,$vendor,$tax_type,$location,$project,$transportation_charges,$excise_duty,$company,$terms,$credit_duration,$due_date,$remarks,$added_by)
{

	// Query
	$stock_purchase_order_iquery = "insert into stock_purchase_order (stock_purchase_order_number,stock_purchase_order_quotation_id,stock_purchase_order_date,stock_purchase_order_status,stock_purchase_order_transportation,
	stock_purchase_order_vendor,stock_purchase_order_tax_type,stock_purchase_order_location,stock_purchase_order_project,stock_purchase_order_transportation_charges,
	stock_purchase_order_excise_duty,stock_purchase_order_company,stock_purchase_order_terms,stock_purchase_order_credit_duration,stock_purchase_order_due_date,
	stock_purchase_order_remarks,stock_purchase_order_updated_by,stock_purchase_order_updated_on,stock_purchase_order_active, stock_purchase_order_added_by,
	stock_purchase_order_added_on) values(:order_number,:quotation_id,:order_date,:status,:transportation,:vendor,:tax_type,:location,:project,:transportation_charges,:excise_duty,:company,:terms,:credit_duration,:due_date,:remarks,:updated_by,:updated_on,:active,:added_by,:added_on)";


    try

    {

		$dbConnection = get_conn_handle();

		$stock_purchase_order_istatement = $dbConnection->prepare($stock_purchase_order_iquery);

        

        // Data

		$stock_purchase_order_idata = array(':order_number'=>$order_number,':quotation_id'=>$quotation_id,':order_date'=>$order_date,':status'=>'Pending',
		':transportation'=>$transportation,':vendor'=>$vendor,':tax_type'=>$tax_type,':location'=>$location,':project'=>$project,':transportation_charges'=>$transportation_charges,':excise_duty'=>$excise_duty,':company'=>$company,':terms'=>$terms,':credit_duration'=>$credit_duration,':due_date'=>$due_date,':remarks'=>$remarks,':updated_by'=>'',':updated_on'=>'',':active'=>'1',':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));      


		$dbConnection->beginTransaction();

        $stock_purchase_order_istatement->execute($stock_purchase_order_idata);

		$stock_purchase_order_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

       

        $return["status"] = SUCCESS;

		$return["data"]   = $stock_purchase_order_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get Stock Purchase Order list

INPUT 	: Order ID,Order Number,Quotation ID,Order Date,Status,Transportation,Vendor Contact Details,Deductions,Location,Terms,Updated By,Updated On,

          Added By,Start Date(for added on), End Date(for added on)

OUTPUT 	: List of Stock Purchase Order

BY 		: Lakshmi

*/

function db_get_stock_purchase_order_list($stock_purchase_order_search_data)

{  

	if(array_key_exists("order_id",$stock_purchase_order_search_data))

	{

		$order_id = $stock_purchase_order_search_data["order_id"];

	}

	else

	{

		$order_id= "";

	}

	

	if(array_key_exists("order_number",$stock_purchase_order_search_data))

	{

		$order_number= $stock_purchase_order_search_data["order_number"];

	}

	else

	{

		$order_number = "";

	}

	

	if(array_key_exists("order_number_check",$stock_purchase_order_search_data))

	{

		$order_number_check= $stock_purchase_order_search_data["order_number_check"];

	}

	else

	{

		$order_number_check = "";

	}

	

	if(array_key_exists("quotation_id",$stock_purchase_order_search_data))

	{

		$quotation_id= $stock_purchase_order_search_data["quotation_id"];

	}

	else

	{

		$quotation_id = "";

	}

	if(array_key_exists("order_date",$stock_purchase_order_search_data))

	{

		$order_date= $stock_purchase_order_search_data["order_date"];

	}

	else

	{

		$order_date= "";

	}

	if(array_key_exists("status",$stock_purchase_order_search_data))

	{

		$status= $stock_purchase_order_search_data["status"];

	}

	else

	{

		$status= "";

	}

	if(array_key_exists("transportation",$stock_purchase_order_search_data))

	{

		$transportation= $stock_purchase_order_search_data["transportation"];

	}

	else

	{

		$transportation= "";

	}

	

	if(array_key_exists("vendor",$stock_purchase_order_search_data))

	{

		$vendor= $stock_purchase_order_search_data["vendor"];

	}

	else

	{

		$vendor= "";

	}



	if(array_key_exists("tax_type",$stock_purchase_order_search_data))

	{

		$tax_type= $stock_purchase_order_search_data["tax_type"];

	}

	else

	{

		$tax_type= "";

	}

	if(array_key_exists("location",$stock_purchase_order_search_data))

	{

		$location= $stock_purchase_order_search_data["location"];

	}

	else

	{

		$location= "";

	}
	
	if(array_key_exists("project",$stock_purchase_order_search_data))
	{
		$project= $stock_purchase_order_search_data["project"];
	}
	else
	{
		$project= "";
	}
	
	if(array_key_exists("terms",$stock_purchase_order_search_data))

	{

		$terms= $stock_purchase_order_search_data["terms"];

	}

	else

	{

		$terms= "";

	}

	

	if(array_key_exists("active",$stock_purchase_order_search_data))

	{

		$active= $stock_purchase_order_search_data["active"];

	}

	else

	{

		$active= "";

	}

	

	if(array_key_exists("updated_by",$stock_purchase_order_search_data))

	{

		$updated_by= $stock_purchase_order_search_data["updated_by"];

	}

	else

	{

		$updated_by= "";

	}

	

	if(array_key_exists("updated_on",$stock_purchase_order_search_data))

	{

		$updated_on= $stock_purchase_order_search_data["updated_on"];

	}

	else

	{

		$updated_on= "";

	}

	

	if(array_key_exists("added_by",$stock_purchase_order_search_data))

	{

		$added_by= $stock_purchase_order_search_data["added_by"];

	}

	else

	{

		$added_by= "";

	}

	if(array_key_exists("start_date",$stock_purchase_order_search_data))

	{

		$start_date= $stock_purchase_order_search_data["start_date"];

	}

	else

	{

		$start_date= "";

	}

	if(array_key_exists("end_date",$stock_purchase_order_search_data))

	{

		$end_date = $stock_purchase_order_search_data["end_date"];

	}

	else

	{

		$end_date = "";

	}

	

	if(array_key_exists("indent_item",$stock_purchase_order_search_data))

	{

		$indent_item = $stock_purchase_order_search_data["indent_item"];

	}

	else

	{

		$indent_item = "";

	}

	
	$get_stock_purchase_order_list_squery_base = "select * from stock_purchase_order SPO inner join stock_location_master SLM on SLM.stock_location_id= SPO.stock_purchase_order_location inner join users U on U.user_id = SPO.stock_purchase_order_added_by inner join stock_quotation_compare SQC on SQC.stock_quotation_id = SPO.stock_purchase_order_quotation_id inner join stock_vendor_master SVM on SVM.stock_vendor_id= SPO.stock_purchase_order_vendor inner join stock_company_master SCM on SCM.stock_company_master_id = SPO.stock_purchase_order_company left outer join stock_project SP on SP.stock_project_id = SPO.stock_purchase_order_project";
	

	$get_stock_purchase_order_list_squery_where = "";

	$get_po_list_squery_order				= "";

	

	$filter_count = 0;

	

	// Data

	$get_stock_purchase_order_list_sdata = array();

	

	if($order_id != "")

	{

		if($filter_count == 0)

		{  

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." where stock_purchase_order_id = :order_id";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." and stock_purchase_order_id = :order_id";				

		}

		

		// Data

		$get_stock_purchase_order_list_sdata[':order_id'] = $order_id;

		

		$filter_count++;

	}

	if($order_number != "")

	{

		if($filter_count == 0)

		{

			// Query

			if($order_number_check == '1')

			{

				$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." where stock_purchase_order_number = :order_number";			

					

				// Data

				$get_stock_purchase_order_list_sdata[':order_number']  = $order_number;

			}

			else if($order_number_check == '2')

			{

				$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." where stock_purchase_order_number like :order_number";						



				// Data

				$get_stock_purchase_order_list_sdata[':order_number']  = '%'.$order_number;

			}

			else

			{

				$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." where stock_purchase_order_number like :order_number";						



				// Data

				$get_stock_purchase_order_list_sdata[':order_number']  = '%'.$order_number.'%';

			}								

		}

		else

		{

			// Query

			if($order_number_check == '1')

			{

				$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." and stock_purchase_order_number = :order_number";	

					

				// Data

				$get_stock_purchase_order_list_sdata[':order_number']  = $order_number;

			}

			else if($order_number_check == '2')

			{

				$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." and stock_purchase_order_number like :order_number";						



				// Data

				$get_stock_purchase_order_list_sdata[':order_number']  = '%'.$order_number;

			}

			else

			{

				$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." and stock_purchase_order_number like :order_number";

				

				// Data

				$get_stock_purchase_order_list_sdata[':order_number']  = '%'.$order_number.'%';

			}			

		}

		

		$filter_count++;

	}

	if($quotation_id != "")

	{

		if($filter_count == 0)

		{

			// Query	

		$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." where stock_purchase_order_quotation_id = :quotation_id";								

		}

		else

		{

			// Query

		$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." and stock_purchase_order_quotation_id = :quotation_id";				

		}

		

		// Data

		$get_stock_purchase_order_list_sdata[':quotation_id']  = $quotation_id;

		

		$filter_count++;

	}

	if($order_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." where stock_purchase_order_date = :order_date";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." and stock_purchase_order_date = :order_date";				

		}

		

		// Data

		$get_stock_purchase_order_list_sdata[':order_date']  = $order_date;

		

		$filter_count++;

	}

	

	if($status != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." where stock_purchase_order_status = :status";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." and stock_purchase_order_status = :status";				

		}

		

		// Data

		$get_stock_purchase_order_list_sdata[':status']  = $status;

		

		$filter_count++;

	}

	if($transportation != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." where stock_purchase_order_transportation = :transportation";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." and stock_purchase_order_transportation = :transportation";				

		}

		

		// Data

		$get_stock_purchase_order_list_sdata[':transportation']  = $transportation;

		

		$filter_count++;

	}

	

	if($vendor != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." where stock_purchase_order_vendor = :vendor";

		}

		else

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." and stock_purchase_order_vendor = :vendor";

		}

		

		// Data

		$get_stock_purchase_order_list_sdata[':vendor']  = $vendor;

		

		$filter_count++;

	}

	

	if($tax_type != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." where stock_purchase_order_tax_type = :tax_type";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." and stock_purchase_order_tax_type = :tax_type";				

		}

		

		// Data

		$get_stock_purchase_order_list_sdata[':tax_type']  = $tax_type;

		

		$filter_count++;

	}

	

	if($location != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." where stock_purchase_order_location = :location";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." and stock_purchase_order_location = :location";				

		}

		

		// Data

		$get_stock_purchase_order_list_sdata[':location']  = $location;

		

		$filter_count++;

	}

	
	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." where stock_purchase_order_project = :project";								
		}
		else
		{
			// Query
			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." and stock_purchase_order_project = :project";				
		}
		
		// Data
		$get_stock_purchase_order_list_sdata[':project']  = $project;
		
		$filter_count++;
	}
	
	if($terms != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." where stock_purchase_order_terms = :terms";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." and stock_purchase_order_terms = :terms";				

		}

		

		// Data

		$get_stock_purchase_order_list_sdata[':terms']  = $terms;

		

		$filter_count++;

	}

	

	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." where stock_purchase_order_active = :active";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." and stock_purchase_order_active = :active";				

		}

		

		// Data

		$get_stock_purchase_order_list_sdata[':active']  = $active;

		

		$filter_count++;

	}

	

	if($updated_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." where stock_purchase_order_updated_by=:updated_by";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." and stock_purchase_order_updated_by=:updated_by";				

		}

		

		// Data

		$get_stock_purchase_order_list_sdata[':updated_by']  = $updated_by;

		

		$filter_count++;

	}

	

	if($updated_on != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." where stock_purchase_order_updated_on=:updated_on";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." and stock_purchase_order_updated_on=:updated_on";				

		}

		

		// Data

		$get_stock_purchase_order_list_sdata[':updated_on']  = $updated_on;

		

		$filter_count++;

	}

	

	if($added_by!= "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." where stock_purchase_order_added_by >= :added_by";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." and stock_purchase_order_added_by >= :added_by";				

		}

		

		//Data

		$get_stock_purchase_order_list_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." where stock_purchase_order_added_on >= :start_date";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." and stock_purchase_order_added_on >= :start_date";				

		}

		

		//Data

		$get_stock_purchase_order_list_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." where stock_purchase_order_added_on <= :end_date";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." and stock_purchase_order_added_on <= :end_date";				

		}

		

		//Data

		$get_stock_purchase_order_list_sdata['end_date']  = $end_date;

		

		$filter_count++;

	}

	

	if($indent_item != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." where SQC.stock_quotation_indent_id = :indent_item";

		}

		else

		{

			// Query

			$get_stock_purchase_order_list_squery_where = $get_stock_purchase_order_list_squery_where." and SQC.stock_quotation_indent_id = :indent_item";				

		}

		

		// Data

		$get_stock_purchase_order_list_sdata[':indent_item']  = $indent_item;

		

		$filter_count++;

	}

	

	if(array_key_exists("sort",$stock_purchase_order_search_data))

	{

		if($stock_purchase_order_search_data['sort'] == '1')

		{

			$get_po_list_squery_order = " order by cast(substring(stock_purchase_order_number,locate('.',stock_purchase_order_number,6)+1) as signed) desc limit 0,1";

		}

		else

		{

			$get_po_list_squery_order = " order by stock_purchase_order_added_on DESC";

		}

	}
	else

	{

		$get_po_list_squery_order = " order by stock_purchase_order_added_on DESC";

	}

	

	$get_stock_purchase_order_list_squery = $get_stock_purchase_order_list_squery_base.$get_stock_purchase_order_list_squery_where.$get_po_list_squery_order;

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_stock_purchase_order_list_sstatement = $dbConnection->prepare($get_stock_purchase_order_list_squery);

		

		$get_stock_purchase_order_list_sstatement -> execute($get_stock_purchase_order_list_sdata);

		

		$get_stock_purchase_order_list_sdetails = $get_stock_purchase_order_list_sstatement -> fetchAll();

		

		if(FALSE === $get_stock_purchase_order_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_stock_purchase_order_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_stock_purchase_order_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}



/*

PURPOSE : To update Purchase Order

INPUT 	: Order ID, Purchase Order Update Array

OUTPUT 	: Order ID, Message of success or failure

BY 		: Lakshmi

*/

function db_update_purchase_order($order_id,$purchase_order_update_data)

{

	if(array_key_exists("order_number",$purchase_order_update_data))

	{	

		$order_number = $purchase_order_update_data["order_number"];

	}

	else

	{

		$order_number = "";

	}

	if(array_key_exists("quotation_id",$purchase_order_update_data))

	{	

		$quotation_id = $purchase_order_update_data["quotation_id"];

	}

	else

	{

		$quotation_id = "";

	}

	if(array_key_exists("order_date",$purchase_order_update_data))

	{	

		$order_date = $purchase_order_update_data["order_date"];

	}

	else

	{

		$order_date = "";

	}

	if(array_key_exists("status",$purchase_order_update_data))

	{	

		$status = $purchase_order_update_data["status"];

	}

	else

	{

		$status = "";

	}

	if(array_key_exists("transportation",$purchase_order_update_data))

	{	

		$transportation = $purchase_order_update_data["transportation"];

	}

	else

	{

		$transportation = "";

	}



	if(array_key_exists("tax_type",$purchase_order_update_data))

	{	

		$tax_type = $purchase_order_update_data["tax_type"];

	}

	else

	{

		$tax_type = "";

	}

	if(array_key_exists("location",$purchase_order_update_data))

	{	

		$location = $purchase_order_update_data["location"];

	}

	else

	{

		$location = "";

	}

	

	if(array_key_exists("company",$purchase_order_update_data))

	{	

		$company = $purchase_order_update_data["company"];

	}

	else

	{

		$company = "";

	}

	
	if(array_key_exists("project",$purchase_order_update_data))
	{	
		$project = $purchase_order_update_data["project"];
	}
	else
	{
		$project = "";
	}
	
	if(array_key_exists("transportation_charges",$purchase_order_update_data))
	{	
		$transportation_charges = $purchase_order_update_data["transportation_charges"];
	}
	else
	{
		$transportation_charges = "";
	}
	
	if(array_key_exists("excise_duty",$purchase_order_update_data))
	{	
		$excise_duty = $purchase_order_update_data["excise_duty"];
	}
	else
	{
		$excise_duty = "";
	}
	
	if(array_key_exists("terms",$purchase_order_update_data))

	{	

		$terms = $purchase_order_update_data["terms"];

	}

	else

	{

		$terms = "";

	}

	

	if(array_key_exists("credit_duration",$purchase_order_update_data))

	{	

		$credit_duration = $purchase_order_update_data["credit_duration"];

	}

	else

	{

		$credit_duration = "";

	}

	

	if(array_key_exists("due_date",$purchase_order_update_data))

	{	

		$due_date = $purchase_order_update_data["due_date"];

	}

	else

	{

		$due_date = "";

	}

	

	

	if(array_key_exists("active",$purchase_order_update_data))

	{	

		$active = $purchase_order_update_data["active"];

	}

	else

	{

		$active = "";

	}

	

	if(array_key_exists("remarks",$purchase_order_update_data))

	{	

		$remarks = $purchase_order_update_data["remarks"];

	}

	else

	{

		$remarks = "";

	}

	

	if(array_key_exists("updated_by",$purchase_order_update_data))

	{	

		$updated_by = $purchase_order_update_data["updated_by"];

	}

	else

	{

		$updated_by = "";

	}

	

	if(array_key_exists("updated_on",$purchase_order_update_data))

	{	

		$updated_on = $purchase_order_update_data["updated_on"];

	}

	else

	{

		$updated_on = "";

	}

	

	if(array_key_exists("added_by",$purchase_order_update_data))

	{	

		$added_by = $purchase_order_update_data["added_by"];

	}

	else

	{

		$added_by = "";

	}

	if(array_key_exists("added_on",$purchase_order_update_data))

	{	

		$added_on = $purchase_order_update_data["added_on"];

	}

	else

	{

		$added_on = "";

	}

	

	// Query

    $purchase_order_update_uquery_base = "update stock_purchase_order set";  

	

	$purchase_order_update_uquery_set = "";

	

	$purchase_order_update_uquery_where = " where stock_purchase_order_id=:order_id";

	

	$purchase_order_update_udata = array(":order_id"=>$order_id);

	

	$filter_count = 0;

	

	if($order_number != "")

	{

		$purchase_order_update_uquery_set = $purchase_order_update_uquery_set." stock_purchase_order_number=:order_number,";

		$purchase_order_update_udata[":order_number"] = $order_number;

		$filter_count++;

	}

	if($quotation_id != "")

	{

		$purchase_order_update_uquery_set = $purchase_order_update_uquery_set." stock_purchase_order_quotation_id=:quotation_id,";

		$purchase_order_update_udata[":quotation_id"] = $quotation_id;

		$filter_count++;

	}

	if($order_date != "")

	{

		$purchase_order_update_uquery_set = $purchase_order_update_uquery_set." stock_purchase_order_date=:order_date,";

		$purchase_order_update_udata[":order_date"] = $order_date;

		$filter_count++;

	}

	if($status != "")

	{

		$purchase_order_update_uquery_set = $purchase_order_update_uquery_set." stock_purchase_order_status=:status,";

		$purchase_order_update_udata[":status"] = $status;

		$filter_count++;

	}

	if($transportation != "")

	{

		$purchase_order_update_uquery_set = $purchase_order_update_uquery_set." stock_purchase_order_transportation=:transportation,";

		$purchase_order_update_udata[":transportation"] = $transportation;

		$filter_count++;

	}

	

	if($tax_type != "")

	{

		$purchase_order_update_uquery_set = $purchase_order_update_uquery_set." stock_purchase_order_tax_type=:tax_type,";

		$purchase_order_update_udata[":tax_type"] = $tax_type;

		$filter_count++;

	}

	if($location != "")

	{

		$purchase_order_update_uquery_set = $purchase_order_update_uquery_set." stock_purchase_order_location=:location,";

		$purchase_order_update_udata[":location"] = $location;

		$filter_count++;

	}

	
	if($project != "")
	{
		$purchase_order_update_uquery_set = $purchase_order_update_uquery_set." stock_purchase_order_project=:project,";
		$purchase_order_update_udata[":project"] = $project;
		$filter_count++;
	}
	
	if($transportation_charges != "")
	{
		$purchase_order_update_uquery_set = $purchase_order_update_uquery_set." stock_purchase_order_transportation_charges=:transportation_charges,";
		$purchase_order_update_udata[":transportation_charges"] = $transportation_charges;
		$filter_count++;
	}
	
	if($excise_duty != "")
	{
		$purchase_order_update_uquery_set = $purchase_order_update_uquery_set." stock_purchase_order_excise_duty=:excise_duty,";
		$purchase_order_update_udata[":excise_duty"] = $excise_duty;
		$filter_count++;
	}
	
	if($company != "")

	{

		$purchase_order_update_uquery_set = $purchase_order_update_uquery_set." stock_purchase_order_company=:company,";

		$purchase_order_update_udata[":company"] = $company;

		$filter_count++;

	}

	

	if($terms != "")

	{

		$purchase_order_update_uquery_set = $purchase_order_update_uquery_set." stock_purchase_order_terms=:terms,";

		$purchase_order_update_udata[":terms"] = $terms;

		$filter_count++;

	}

	

	if($credit_duration != "")

	{

		$purchase_order_update_uquery_set = $purchase_order_update_uquery_set." stock_purchase_order_credit_duration=:credit_duration,";

		$purchase_order_update_udata[":credit_duration"] = $credit_duration;

		$filter_count++;

	}

	if($due_date != "")

	{

		$purchase_order_update_uquery_set = $purchase_order_update_uquery_set." stock_purchase_order_due_date=:due_date,";

		$purchase_order_update_udata[":due_date"] = $due_date;

		$filter_count++;

	}

	

	if($active != "")

	{

		$purchase_order_update_uquery_set = $purchase_order_update_uquery_set." stock_purchase_order_active = :active,";

		$purchase_order_update_udata[":active"] = $active;

		$filter_count++;

	}

	

	if($remarks != "")

	{

		$purchase_order_update_uquery_set = $purchase_order_update_uquery_set." stock_purchase_order_remarks=:remarks,";

		$purchase_order_update_udata[":remarks"] = $remarks;

		$filter_count++;

	}

	

	if($updated_by != "")

	{

		$purchase_order_update_uquery_set = $purchase_order_update_uquery_set." stock_purchase_order_updated_by=:updated_by,";

		$purchase_order_update_udata[":updated_by"] = $updated_by;

		$filter_count++;

	}

	if($updated_on != "")

	{

		$purchase_order_update_uquery_set = $purchase_order_update_uquery_set." stock_purchase_order_updated_on=:updated_on,";

		$purchase_order_update_udata[":updated_on"] = $updated_on;

		$filter_count++;

	}

	

	if($added_by != "")

	{

		$purchase_order_update_uquery_set = $purchase_order_update_uquery_set." stock_purchase_order_added_by=:added_by,";

		$purchase_order_update_udata[":added_by"] = $added_by;		

		$filter_count++;

	}

	if($added_on != "")

	{

		$purchase_order_update_uquery_set = $purchase_order_update_uquery_set." stock_purchase_order_added_on=:added_on,";

		$purchase_order_update_udata[":added_on"] = $added_on;		

		$filter_count++;

	}

	

	if($filter_count > 0)

	{

		$purchase_order_update_uquery_set = trim($purchase_order_update_uquery_set,',');

	}

	

	$purchase_order_update_uquery = $purchase_order_update_uquery_base.$purchase_order_update_uquery_set.$purchase_order_update_uquery_where;




    try

    {

        $dbConnection = get_conn_handle();

        

        $purchase_order_update_ustatement = $dbConnection->prepare($purchase_order_update_uquery);		

        

        $purchase_order_update_ustatement -> execute($purchase_order_update_udata);

        

        $return["status"] = SUCCESS;

		$return["data"]   = $order_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

	

	return $return;

}



/*

PURPOSE : To add new Stock Purchase Order Items

INPUT 	: Order Id, Item, Quantity, Uom,Tax Type, Cost, Transport, Delivery Schedule, Active, Remarks, Added by, Added on

OUTPUT 	: Item Id, success or failure message

BY 		: Lakshmi

*/
function db_add_stock_purchase_order_items($order_id,$item,$quantity,$uom,$excise_duty,$tax_type,$cost,$transport,$delivery_schedule,$remarks,$added_by)
{

	// Query
    $stock_purchase_order_items_iquery = "insert into stock_purchase_order_items (stock_purchase_order_id,stock_purchase_order_item,stock_purchase_order_item_quantity,stock_purchase_order_item_uom,stock_purchase_order_item_excise_duty,
	stock_purchase_order_item_tax_type,stock_purchase_order_item_cost,stock_purchase_order_item_transport,stock_purchase_order_item_delivery_schedule,
	stock_purchase_order_item_active,stock_purchase_order_item_status,stock_purchase_order_item_remarks,stock_purchase_order_item_added_by,stock_purchase_order_item_added_on)
	values(:order_id,:item,:quantity,:uom,:excise_duty,:tax_type,:cost,:transport,:delivery_schedule,:active,:status,:remarks,:added_by,:added_on)"; 
	try

    {

        $dbConnection = get_conn_handle();

        $stock_purchase_order_items_istatement = $dbConnection->prepare($stock_purchase_order_items_iquery);

       

        // Data
        $stock_purchase_order_items_idata = array(':order_id'=>$order_id,':item'=>$item,':quantity'=>$quantity,':uom'=>$uom,':excise_duty'=>$excise_duty,':tax_type'=>$tax_type,
		':cost'=>$cost,':transport'=>$transport,':delivery_schedule'=>$delivery_schedule,':active'=>'1',':status'=>'Waiting',':remarks'=>$remarks,

		':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	

	    

		$dbConnection->beginTransaction();

        $stock_purchase_order_items_istatement->execute($stock_purchase_order_items_idata);

		$stock_purchase_order_item_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

       

        $return["status"] = SUCCESS;

		$return["data"]   = $stock_purchase_order_item_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get Stock Purchase Order Items List

INPUT 	: Item Id, Order Id, Item, Quantity, Uom, Tax Type, Cost, Transport, Delivery Schedule ,Active, Added by,

          Start Date(for added on), End Date(for added on)

OUTPUT 	: List of stock Purchase Order Items

BY 		: Lakshmi

*/

function db_get_stock_purchase_order_items_list($stock_purchase_order_items_search_data)

{  	

	if(array_key_exists("item_id",$stock_purchase_order_items_search_data))

	{

		$item_id = $stock_purchase_order_items_search_data["item_id"];

	}

	else

	{

		$item_id= "";

	}

	

	if(array_key_exists("order_id",$stock_purchase_order_items_search_data))

	{

		$order_id= $stock_purchase_order_items_search_data["order_id"];

	}

	else

	{

		$order_id= "";

	}

	

	if(array_key_exists("item",$stock_purchase_order_items_search_data))

	{

		$item = $stock_purchase_order_items_search_data["item"];

	}

	else

	{

		$item= "";

	}

		

	 if(array_key_exists("quantity",$stock_purchase_order_items_search_data))

	{

		$quantity= $stock_purchase_order_items_search_data["quantity"];

	}

	else

	{

		$quantity= "";

	}

	if(array_key_exists("uom",$stock_purchase_order_items_search_data))

	{

		$uom= $stock_purchase_order_items_search_data["uom"];

	}

	else

	{

		$uom= "";

	}

	

	if(array_key_exists("tax_type",$stock_purchase_order_items_search_data))

	{

		$tax_type= $stock_purchase_order_items_search_data["tax_type"];

	}

	else

	{

		$tax_type= "";

	}

		

	if(array_key_exists("vendor",$stock_purchase_order_items_search_data))

	{

		$vendor= $stock_purchase_order_items_search_data["vendor"];		

	}

	else

	{

		$vendor= "";

	}

	

	if(array_key_exists("cost",$stock_purchase_order_items_search_data))

	{

		$cost= $stock_purchase_order_items_search_data["cost"];

	}

	else

	{

		$cost= "";

	}

	

	if(array_key_exists("transport",$stock_purchase_order_items_search_data))

	{

		$transport= $stock_purchase_order_items_search_data["transport"];

	}

	else

	{

		$transport= "";

	}

	

	if(array_key_exists("delivery_schedule",$stock_purchase_order_items_search_data))

	{

		$delivery_schedule= $stock_purchase_order_items_search_data["delivery_schedule"];

	}

	else

	{

		$delivery_schedule= "";

	}

	 

	if(array_key_exists("active",$stock_purchase_order_items_search_data))

	{

		$active= $stock_purchase_order_items_search_data["active"];

	}

	else

	{

		$active= "";

	}

	

	if(array_key_exists("status",$stock_purchase_order_items_search_data))

	{

		$status= $stock_purchase_order_items_search_data["status"];

	}

	else

	{

		$status= "";

	}

	if(array_key_exists("added_by",$stock_purchase_order_items_search_data))

	{

		$added_by= $stock_purchase_order_items_search_data["added_by"];

	}

	else

	{

		$added_by= "";

	}

	if(array_key_exists("start_date",$stock_purchase_order_items_search_data))

	{

		$start_date= $stock_purchase_order_items_search_data["start_date"];

	}

	else

	{

		$start_date= "";

	}

	if(array_key_exists("end_date",$stock_purchase_order_items_search_data))

	{

		$end_date= $stock_purchase_order_items_search_data["end_date"];

	}

	else

	{

		$end_date= "";

	}
	
	if(array_key_exists("project",$stock_purchase_order_items_search_data))

	{

		$project= $stock_purchase_order_items_search_data["project"];

	}

	else

	{

		$project= "";

	}
	if(array_key_exists("start",$stock_purchase_order_items_search_data))
	{
		$start= $stock_purchase_order_items_search_data["start"];
	}
	else
	{
		$start= "-1";
	}
	
	if(array_key_exists("limit",$stock_purchase_order_items_search_data))
	{
		$limit= $stock_purchase_order_items_search_data["limit"];
	}
	else
	{
		$limit= "";
	}

	$get_stock_purchase_order_items_list_squery_base = "select * from stock_purchase_order_items SPOI inner join users U on U.user_id = SPOI.stock_purchase_order_item_added_by inner join stock_material_master SMM on SMM.stock_material_id= SPOI.stock_purchase_order_item  left outer join stock_tax_type_master STTM on STTM.stock_tax_type_master_id= SPOI.stock_purchase_order_item_tax_type left outer join stock_transportation_master STM on STM.stock_transportation_id=SPOI.stock_purchase_order_item_transport inner join stock_purchase_order SPO on SPO.stock_purchase_order_id = SPOI.stock_purchase_order_id inner join stock_vendor_master SVM on SVM.stock_vendor_id= SPO.stock_purchase_order_vendor inner join stock_unit_measure_master SUMM on SUMM.stock_unit_id=SMM.stock_material_unit_of_measure inner join stock_quotation_compare SQC on SQC.stock_quotation_id=SPO.stock_purchase_order_quotation_id inner join stock_location_master SLM on SLM.stock_location_id = SPO.stock_purchase_order_location inner join stock_company_master SCM on SCM.stock_company_master_id = SPO.stock_purchase_order_company left outer join stock_project SP on SP.stock_project_id = SPO.stock_purchase_order_project";
	

	$get_stock_purchase_order_items_list_squery_where = "";

	

	$filter_count = 0;

	 

	// Data

	$get_stock_purchase_order_items_list_sdata = array();

	

	    if($item_id != "")

	{

		if($filter_count == 0)

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_id = :item_id";								

		}

		else

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_id = :item_id";				

		}

		// Data

		$get_stock_purchase_order_items_list_sdata[':item_id'] = $item_id;

		

		$filter_count++;

	}

	

	    if($order_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where SPOI.stock_purchase_order_id = :order_id";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and SPOI.stock_purchase_order_id=:order_id";				

		}

		

		// Data

		$get_stock_purchase_order_items_list_sdata[':order_id']  = $order_id;

		

		$filter_count++;

	}

	

	if($item != "")

	{

		if($filter_count == 0)

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item = :item";								

		}

		else

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item = :item";				

		}

		

		// Data

		$get_stock_purchase_order_items_list_sdata[':item']  = $item;

		

		$filter_count++;

	}

	  

	    if($quantity != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_quantity = :quantity";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_quantity = :quantity";				

		}

		

		// Data

		$get_stock_purchase_order_items_list_sdata[':quantity']  = $quantity;

		

		$filter_count++;

	}

	 

	 if($uom != "")

	{

		if($filter_count == 0)

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_uom = :uom";								

		}

		else

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_uom = :uom";				

		}

		

		// Data

		$get_stock_purchase_order_items_list_sdata[':uom']  = $uom;

		

		$filter_count++;

	}

	

	if($tax_type != "")

	{

		if($filter_count == 0)

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_tax_type = :tax_type";								

		}

		else

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_tax_type = :tax_type";				

		}

		

		// Data

		$get_stock_purchase_order_items_list_sdata[':tax_type']  = $tax_type;

		

		$filter_count++;

	}

	

	if($vendor != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where SPO.stock_purchase_order_vendor = :vendor";

		}

		else

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and SPO.stock_purchase_order_vendor = :vendor";

		}

		

		// Data

		$get_stock_purchase_order_items_list_sdata[':vendor']  = $vendor;

		

		$filter_count++;

	}

	 

    if($cost != "")

	{

		if($filter_count == 0)

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_cost = :cost";								

		}

		else

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_cost = :cost";				

		}

		

		// Data

		$get_stock_purchase_order_items_list_sdata[':cost']  = $cost;

		

		$filter_count++;

	}

	

	 if($transport != "")

	{

		if($filter_count == 0)

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_transport = :transport";								

		}

		else

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_transport = :transport";				

		}

		

		// Data

		$get_stock_purchase_order_items_list_sdata[':transport']  = $transport;

		

		$filter_count++;

	}

	

	 if($delivery_schedule != "")

	{

		if($filter_count == 0)

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_delivery_schedule = :delivery_schedule";								

		}

		else

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_delivery_schedule = :delivery_schedule";				

		}

		

		// Data

		$get_stock_purchase_order_items_list_sdata[':delivery_schedule']  = $delivery_schedule;

		

		$filter_count++;

	}

	

	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_active = :active";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_active = :active";				

		}

		

		// Data

		$get_stock_purchase_order_items_list_sdata[':active']  = $active;

		

		$filter_count++;

	}

	

	if($status != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_status = :status";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_status = :status";				

		}

		

		// Data

		$get_stock_purchase_order_items_list_sdata[':status']  = $status;

		

		$filter_count++;

	}

	
	if($po_status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where SPO.stock_purchase_order_status = :po_status";								
		}
		else
		{
			// Query
			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and SPO.stock_purchase_order_status = :po_status";				
		}
		
		// Data
		$get_stock_purchase_order_items_list_sdata[':po_status']  = $po_status;
		
		$filter_count++;
	}
	
	if($added_by!= "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_added_by = :added_by";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_added_by = :added_by";				

		}

		

		//Data

		$get_stock_purchase_order_items_list_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_added_on >= :start_date";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_added_on >= :start_date";				

		}

		

		//Data

		$get_stock_purchase_order_items_list_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_added_on <= :end_date";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_added_on <= :end_date";				

		}

		

		//Data

		$get_stock_purchase_order_items_list_sdata['end_date']  = $end_date;

		

		$filter_count++;

	}
	
	if($project != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where SPO.stock_purchase_order_project = :project";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and SPO.stock_purchase_order_project = :project";				

		}

		

		//Data

		$get_stock_purchase_order_items_list_sdata['project']  = $project;

		

		$filter_count++;

	}
	if($start >= 0)
	{
			// Query
			$get_stock_purchase_order_items_list_squery_limit = $get_stock_purchase_order_items_list_squery_limit." limit $start,$limit";	
	}			
	else
	{
		// Query
		$get_stock_purchase_order_items_list_squery_limit = "";				
	}
	
	$get_stock_purchase_order_items_list_squery_order = " order by SPO.stock_purchase_order_id DESC";
	$get_stock_purchase_order_items_list_squery = $get_stock_purchase_order_items_list_squery_base.$get_stock_purchase_order_items_list_squery_where.$get_stock_purchase_order_items_list_squery_order.$get_stock_purchase_order_items_list_squery_limit;
	//var_dump($get_stock_purchase_order_items_list_squery);
	//var_dump($get_stock_purchase_order_items_list_sdata);

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_stock_purchase_order_items_list_sstatement = $dbConnection->prepare($get_stock_purchase_order_items_list_squery);

		

		$get_stock_purchase_order_items_list_sstatement -> execute($get_stock_purchase_order_items_list_sdata);

		

		$get_stock_purchase_order_items_list_sdetails = $get_stock_purchase_order_items_list_sstatement -> fetchAll();

		

		if(FALSE === $get_stock_purchase_order_items_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_stock_purchase_order_items_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_stock_purchase_order_items_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

}

 

 /*

PURPOSE : To get Stock Purchase Order Items List

INPUT 	: Item Id, Order Id, Item, Quantity, Uom, Tax Type, Cost, Transport, Delivery Schedule ,Active, Added by,

          Start Date(for added on), End Date(for added on)

OUTPUT 	: List of stock Purchase Order Items

BY 		: Lakshmi

*/

function db_get_stock_sum_of_purchase_order_items_list($stock_purchase_order_items_search_data)

{  

 

	if(array_key_exists("item_id",$stock_purchase_order_items_search_data))

	{

		$item_id = $stock_purchase_order_items_search_data["item_id"];

	}

	else

	{

		$item_id= "";

	}

	

	if(array_key_exists("order_id",$stock_purchase_order_items_search_data))

	{

		$order_id= $stock_purchase_order_items_search_data["order_id"];

	}

	else

	{

		$order_id= "";

	}

	

	if(array_key_exists("item",$stock_purchase_order_items_search_data))

	{

		$item = $stock_purchase_order_items_search_data["item"];

	}

	else

	{

		$item= "";

	}

		

	 if(array_key_exists("quantity",$stock_purchase_order_items_search_data))

	{

		$quantity= $stock_purchase_order_items_search_data["quantity"];

	}

	else

	{

		$quantity= "";

	}

	if(array_key_exists("uom",$stock_purchase_order_items_search_data))

	{

		$uom= $stock_purchase_order_items_search_data["uom"];

	}

	else

	{

		$uom= "";

	}

	

	if(array_key_exists("tax_type",$stock_purchase_order_items_search_data))

	{

		$tax_type= $stock_purchase_order_items_search_data["tax_type"];

	}

	else

	{

		$tax_type= "";

	}

	if(array_key_exists("cost",$stock_purchase_order_items_search_data))

	{

		$cost= $stock_purchase_order_items_search_data["cost"];

	}

	else

	{

		$cost= "";

	}

	

	if(array_key_exists("transport",$stock_purchase_order_items_search_data))

	{

		$transport= $stock_purchase_order_items_search_data["transport"];

	}

	else

	{

		$transport= "";

	}

	

	if(array_key_exists("delivery_schedule",$stock_purchase_order_items_search_data))

	{

		$delivery_schedule= $stock_purchase_order_items_search_data["delivery_schedule"];

	}

	else

	{

		$delivery_schedule= "";

	}

	 

	if(array_key_exists("active",$stock_purchase_order_items_search_data))

	{

		$active= $stock_purchase_order_items_search_data["active"];

	}

	else

	{

		$active= "";

	}

	

	if(array_key_exists("status",$stock_purchase_order_items_search_data))

	{

		$status= $stock_purchase_order_items_search_data["status"];

	}

	else

	{

		$status= "";

	}

	if(array_key_exists("added_by",$stock_purchase_order_items_search_data))

	{

		$added_by= $stock_purchase_order_items_search_data["added_by"];

	}

	else

	{

		$added_by= "";

	}

	if(array_key_exists("start_date",$stock_purchase_order_items_search_data))

	{

		$start_date= $stock_purchase_order_items_search_data["start_date"];

	}

	else

	{

		$start_date= "";

	}

	if(array_key_exists("end_date",$stock_purchase_order_items_search_data))

	{

		$end_date= $stock_purchase_order_items_search_data["end_date"];

	}

	else

	{

		$end_date= "";

	}



	$get_stock_purchase_order_items_list_squery_base = "select * ,sum(SPOI.stock_purchase_order_item_cost) as total_value from stock_purchase_order_items SPOI inner join Users U on U.user_id = SPOI.stock_purchase_order_item_added_by inner join stock_material_master SMM on SMM.stock_material_id= SPOI.stock_purchase_order_item  left outer join stock_tax_type_master STTM on STTM.stock_tax_type_master_id= SPOI.stock_purchase_order_item_tax_type left outer join stock_transportation_master STM on STM.stock_transportation_id=SPOI.stock_purchase_order_item_transport inner join stock_purchase_order SPO on SPO.stock_purchase_order_id = SPOI.stock_purchase_order_id inner join stock_vendor_master SVM on SVM.stock_vendor_id= SPO.stock_purchase_order_vendor";

	

	$get_stock_purchase_order_items_list_squery_where = "";

	$get_stock_purchase_order_items_list_squery_group_by = "";

	

	$filter_count = 0;

	 

	// Data

	$get_stock_purchase_order_items_list_sdata = array();

	

	    if($item_id != "")

	{

		if($filter_count == 0)

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_id = :item_id";								

		}

		else

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_id = :item_id";				

		}

		// Data

		$get_stock_purchase_order_items_list_sdata[':item_id'] = $item_id;

		

		$filter_count++;

	}

	

	    if($order_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where SPOI.stock_purchase_order_id = :order_id";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and SPOI.stock_purchase_order_id=:order_id";				

		}

		

		// Data

		$get_stock_purchase_order_items_list_sdata[':order_id']  = $order_id;

		

		$filter_count++;

	}

	

	if($item != "")

	{

		if($filter_count == 0)

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item = :item";								

		}

		else

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item = :item";				

		}

		

		// Data

		$get_stock_purchase_order_items_list_sdata[':item']  = $item;

		

		$filter_count++;

	}

	  

	    if($quantity != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_quantity = :quantity";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_quantity = :quantity";				

		}

		

		// Data

		$get_stock_purchase_order_items_list_sdata[':quantity']  = $quantity;

		

		$filter_count++;

	}

	 

	 if($uom != "")

	{

		if($filter_count == 0)

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_uom = :uom";								

		}

		else

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_uom = :uom";				

		}

		

		// Data

		$get_stock_purchase_order_items_list_sdata[':uom']  = $uom;

		

		$filter_count++;

	}

	

	    if($tax_type != "")

	{

		if($filter_count == 0)

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_tax_type = :tax_type";								

		}

		else

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_tax_type = :tax_type";				

		}

		

		// Data

		$get_stock_purchase_order_items_list_sdata[':tax_type']  = $tax_type;

		

		$filter_count++;

	}

	     if($cost != "")

	{

		if($filter_count == 0)

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_cost = :cost";								

		}

		else

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_cost = :cost";				

		}

		

		// Data

		$get_stock_purchase_order_items_list_sdata[':cost']  = $cost;

		

		$filter_count++;

	}

	

	 if($transport != "")

	{

		if($filter_count == 0)

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_transport = :transport";								

		}

		else

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_transport = :transport";				

		}

		

		// Data

		$get_stock_purchase_order_items_list_sdata[':transport']  = $transport;

		

		$filter_count++;

	}

	

	 if($delivery_schedule != "")

	{

		if($filter_count == 0)

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_delivery_schedule = :delivery_schedule";								

		}

		else

		{

			// Query

		$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_delivery_schedule = :delivery_schedule";				

		}

		

		// Data

		$get_stock_purchase_order_items_list_sdata[':delivery_schedule']  = $delivery_schedule;

		

		$filter_count++;

	}

	

	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_active = :active";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_active = :active";				

		}

		

		// Data

		$get_stock_purchase_order_items_list_sdata[':active']  = $active;

		

		$filter_count++;

	}

	

	if($status != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_status = :status";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_status = :status";				

		}

		

		// Data

		$get_stock_purchase_order_items_list_sdata[':status']  = $status;

		

		$filter_count++;

	}

	

	if($added_by!= "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_added_by = :added_by";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_added_by = :added_by";				

		}

		

		//Data

		$get_stock_purchase_order_items_list_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_added_on >= :start_date";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_added_on >= :start_date";				

		}

		

		//Data

		$get_stock_purchase_order_items_list_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." where stock_purchase_order_item_added_on <= :end_date";								

		}

		else

		{

			// Query

			$get_stock_purchase_order_items_list_squery_where = $get_stock_purchase_order_items_list_squery_where." and stock_purchase_order_item_added_on <= :end_date";				

		}

		

		//Data

		$get_stock_purchase_order_items_list_sdata['end_date']  = $end_date;

		

		$filter_count++;

	}

	$get_stock_purchase_order_items_list_squery_group_by = " group by SPOI.stock_purchase_order_id";

	$get_stock_purchase_order_items_list_squery = $get_stock_purchase_order_items_list_squery_base.$get_stock_purchase_order_items_list_squery_where.$get_stock_purchase_order_items_list_squery_group_by;

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_stock_purchase_order_items_list_sstatement = $dbConnection->prepare($get_stock_purchase_order_items_list_squery);

		

		$get_stock_purchase_order_items_list_sstatement -> execute($get_stock_purchase_order_items_list_sdata);

		

		$get_stock_purchase_order_items_list_sdetails = $get_stock_purchase_order_items_list_sstatement -> fetchAll();

		

		if(FALSE === $get_stock_purchase_order_items_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_stock_purchase_order_items_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_stock_purchase_order_items_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

 }

   /*

PURPOSE : To update Purchase Order Items

INPUT 	: Purchase Order Item ID, Purchase Order Items Update Array

OUTPUT 	: Purchase Order Item ID; Message of success or failure

BY 		: Lakshmi

*/

function db_update_purchase_order_items($order_id,$purchase_order_items_update_data)

{

	if(array_key_exists("item",$purchase_order_items_update_data))

	{	

		$item = $purchase_order_items_update_data["item"];

	}

	else

	{

		$item = "";
		
	}
	
	if(array_key_exists("item_id",$purchase_order_items_update_data))
		
	{	
	
		$item_id = $purchase_order_items_update_data["item_id"];
		
	}
	
	else
		
	{
		
		$item_id = "";
		
	}

	

	if(array_key_exists("quantity",$purchase_order_items_update_data))

	{	

		$quantity = $purchase_order_items_update_data["quantity"];

	}

	else

	{

		$quantity = "";

	}

	

	if(array_key_exists("uom",$purchase_order_items_update_data))

	{	

		$uom = $purchase_order_items_update_data["uom"];

	}

	else

	{

		$uom = "";

	}

	
	if(array_key_exists("excise_duty",$purchase_order_items_update_data))
	{	
		$excise_duty = $purchase_order_items_update_data["excise_duty"];
	}
	else
	{
		$excise_duty = "";
	}
	
	if(array_key_exists("tax_type",$purchase_order_items_update_data))

	{	

		$tax_type = $purchase_order_items_update_data["tax_type"];

	}

	else

	{

		$tax_type = "";

	}

	

	if(array_key_exists("cost",$purchase_order_items_update_data))

	{	

		$cost = $purchase_order_items_update_data["cost"];

	}

	else

	{

		$cost = "";

	}

	

	if(array_key_exists("transport",$purchase_order_items_update_data))

	{	

		$transport = $purchase_order_items_update_data["transport"];

	}

	else

	{

		$transport = "";

	}

	

	if(array_key_exists("delivery_schedule",$purchase_order_items_update_data))

	{	

		$delivery_schedule = $purchase_order_items_update_data["delivery_schedule"];

	}

	else

	{

		$delivery_schedule = "";

	}

	

	if(array_key_exists("active",$purchase_order_items_update_data))

	{	

		$active = $purchase_order_items_update_data["active"];

	}

	else

	{

		$active = "";

	}

	

	if(array_key_exists("status",$purchase_order_items_update_data))

	{	

		$status = $purchase_order_items_update_data["status"];

	}

	else

	{

		$status = "";

	}

	

	if(array_key_exists("approved_by",$purchase_order_items_update_data))

	{	

		$approved_by = $purchase_order_items_update_data["approved_by"];

	}

	else

	{

		$approved_by = "";

	}

	

	if(array_key_exists("approved_on",$purchase_order_items_update_data))

	{	

		$approved_on = $purchase_order_items_update_data["approved_on"];

	}

	else

	{

		$approved_on = "";

	}

	

	if(array_key_exists("added_by",$purchase_order_items_update_data))

	{	

		$added_by = $purchase_order_items_update_data["added_by"];

	}

	else

	{

		$added_by = "";

	}

	if(array_key_exists("added_on",$purchase_order_items_update_data))

	{	

		$added_on = $purchase_order_items_update_data["added_on"];

	}

	else

	{

		$added_on = "";

	}

	

	// Query

    $purchase_order_items_update_uquery_base = "update stock_purchase_order_items set";  

	

	$purchase_order_items_update_uquery_set = "";

	

	$purchase_order_items_update_uquery_where = " where stock_purchase_order_id=:order_id";

	

	$purchase_order_items_update_udata = array("order_id"=>$order_id);

	

	$filter_count = 0;
	$where_filter_count = 1;



	if($item != "")

	{

		$purchase_order_items_update_uquery_set = $purchase_order_items_update_uquery_set." stock_purchase_order_item = :item,";

		$purchase_order_items_update_udata[":item"] = $item;

		$filter_count++;

	}
	
	if($item_id!= "")
		
	{
		
		if($where_filter_count == 0)
			
		{
			// Query
			$purchase_order_items_update_uquery_where = $purchase_order_items_update_uquery_where." where stock_purchase_order_item_id = :item_id";	
			
		}
		
		else
			
		{
			
			// Query
			$purchase_order_items_update_uquery_where = $purchase_order_items_update_uquery_where." and stock_purchase_order_item_id = :item_id";
			
		}
		
		//Data
		$purchase_order_items_update_udata[':item_id']  = $item_id;
		
		$where_filter_count++;
	}

	

	if($quantity != "")

	{

		$purchase_order_items_update_uquery_set = $purchase_order_items_update_uquery_set." stock_purchase_order_item_quantity = :quantity,";

		$purchase_order_items_update_udata[":quantity"] = $quantity;

		$filter_count++;

	}

	

	if($uom != "")

	{

		$purchase_order_items_update_uquery_set = $purchase_order_items_update_uquery_set." stock_purchase_order_item_uom=:uom,";

		$purchase_order_items_update_udata[":uom"] = $uom;

		$filter_count++;

	}

	
	if($excise_duty != "")
	{
		$purchase_order_items_update_uquery_set = $purchase_order_items_update_uquery_set." stock_purchase_order_item_excise_duty=:excise_duty,";
		$purchase_order_items_update_udata[":excise_duty"] = $excise_duty;
		$filter_count++;
	}
	
	
	if($tax_type != "")

	{

		$purchase_order_items_update_uquery_set = $purchase_order_items_update_uquery_set." stock_purchase_order_item_tax_type=:tax_type,";

		$purchase_order_items_update_udata[":tax_type"] = $tax_type;

		$filter_count++;

	}

	

	if($cost != "")

	{

		$purchase_order_items_update_uquery_set = $purchase_order_items_update_uquery_set." stock_purchase_order_item_cost=:cost,";

		$purchase_order_items_update_udata[":cost"] = $cost;

		$filter_count++;

	}

	

	if($transport != "")

	{

		$purchase_order_items_update_uquery_set = $purchase_order_items_update_uquery_set." stock_purchase_order_item_transport=:transport,";

		$purchase_order_items_update_udata[":transport"] = $transport;

		$filter_count++;

	}

	

	if($delivery_schedule != "")

	{

		$purchase_order_items_update_uquery_set = $purchase_order_items_update_uquery_set." stock_purchase_order_item_delivery_schedule=:delivery_schedule,";

		$purchase_order_items_update_udata[":delivery_schedule"] = $delivery_schedule;

		$filter_count++;

	}

	

	if($active != "")

	{

		$purchase_order_items_update_uquery_set = $purchase_order_items_update_uquery_set." stock_purchase_order_item_active=:active,";

		$purchase_order_items_update_udata[":active"] = $active;

		$filter_count++;

	}

	

	if($status != "")

	{

		$purchase_order_items_update_uquery_set = $purchase_order_items_update_uquery_set." stock_purchase_order_item_status=:status,";

		$purchase_order_items_update_udata[":status"] = $status;

		$filter_count++;

	}

	

	if($approved_by != "")

	{

		$purchase_order_items_update_uquery_set = $purchase_order_items_update_uquery_set." stock_purchase_order_item_approved_by=:approved_by,";

		$purchase_order_items_update_udata[":approved_by"] = $approved_by;		

		$filter_count++;

	}

	if($approved_on != "")

	{

		$purchase_order_items_update_uquery_set = $purchase_order_items_update_uquery_set." stock_purchase_order_item_approved_by=:approved_on,";

		$purchase_order_items_update_udata[":approved_on"] = $approved_on;		

		$filter_count++;

	}

	

	if($added_by != "")

	{

		$purchase_order_items_update_uquery_set = $purchase_order_items_update_uquery_set." stock_purchase_order_item_added_by=:added_by,";

		$purchase_order_items_update_udata[":added_by"] = $added_by;		

		$filter_count++;

	}

	if($added_on != "")

	{

		$purchase_order_items_update_uquery_set = $purchase_order_items_update_uquery_set." stock_purchase_order_item_added_on=:added_on,";

		$purchase_order_items_update_udata[":added_on"] = $added_on;		

		$filter_count++;

	}

	

	if($filter_count > 0)

	{

		$purchase_order_items_update_uquery_set = trim($purchase_order_items_update_uquery_set,',');

	}

	

	$purchase_order_items_update_uquery = $purchase_order_items_update_uquery_base.$purchase_order_items_update_uquery_set.$purchase_order_items_update_uquery_where;
	



    try

    {

        $dbConnection = get_conn_handle();

        

        $purchase_order_items_update_ustatement = $dbConnection->prepare($purchase_order_items_update_uquery);		

        

        $purchase_order_items_update_ustatement -> execute($purchase_order_items_update_udata);

        

        $return["status"] = SUCCESS;

		$return["data"]   = $order_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

	

	return $return;

}



?>