<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 09 May 2017
// LAST UPDATED BY: Ashwini
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
/* INCLUDES - END */


if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET["payment_id"]))
	{		
		$payment_id = $_GET["payment_id"];				
	}
	else
	{
		$payment_id = "";
	}

	// Capture the form data
	if(isset($_POST["edit_project_contract_issue_payment_submit"]))
	{
		$payment_id           = $_POST["hd_payment_id"];
		$amount               = $_POST["amount"];
		$deduction            = $_POST["deduction"];
		$instrument_details   = $_POST["txt_details"];
		$payment_mode         = $_POST["ddl_mode"];
		$remarks 	          = $_POST["txt_remarks"];
	
		// Check for mandatory fields
		if(($amount != "") && ($payment_mode != ""))
		{
			$project_contract_issue_payment_update_data = array("amount"=>$amount,"deduction"=>$deduction,"payment_mode"=>$payment_mode,"instrument_details"=>$instrument_details,"remarks"=>$remarks);
			$project_contract_issue_payment_update_iresult = i_update_project_contract_issue_payment($payment_id,$project_contract_issue_payment_update_data);
			
			
			if($project_contract_issue_payment_update_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				//header("location:project_contract_issue_payment_list.php");
			}
			else
			{
				$alert_type = 0;
			}
			
			$alert = $project_contract_issue_payment_update_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	// Get project_contract_issue_payment modes already added
	$project_contract_issue_payment_search_data = array("payment_id"=>$payment_id);
	$project_contract_issue_payment_list = i_get_project_contract_issue_payment($project_contract_issue_payment_search_data);
	//var_dump($project_contract_issue_payment_list); 
	if($project_contract_issue_payment_list['status'] == SUCCESS)
	{
		$project_contract_issue_payment_list_data = $project_contract_issue_payment_list['data'];

	}
	
	//Get Payment Mode 
	$payment_mode_list =  i_get_payment_mode_list('','1');
	if($payment_mode_list['status'] == SUCCESS)
	{
		$payment_mode_list_data = $payment_mode_list['data'];		
	}	
	else
	{
		$alert = $payment_mode_list["data"];
		$alert_type = 0;		
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Edit Project Contract Issue Payment</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Edit Project Contract Issue Payment</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Edit Project Contract Issue Payment Condition</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_project_contract_issue_payment_form" class="form-horizontal" method="post" action="project_edit_contract_issue_payment.php">
								<input type="hidden" name="hd_payment_id" value="<?php echo $payment_id; ?>" />
									<fieldset>			
										
										<div class="control-group">											
											<label class="control-label" for="amount">Amount</label>
											<div class="controls">
											<input type="float" class="span6" value="<?php echo $project_contract_issue_payment_list_data[0]["project_contract_issue_payment_amount"] ;?>" 
											name="amount">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="deduction">Deduction</label>
											<div class="controls">
												<input type="number" class="span6" name="deduction" onkeyup="return payable_amount('<?php echo $amount ;?>');"  id="deduction"value="<?php echo $project_contract_issue_payment_list_data[0]["project_contract_issue_payment_deduction"] ;?>"  placeholder="Deduction">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="vendor_id">Vendor</label>
											<div class="controls">
											<input type="text" class="span6"  disabled value="<?php echo $project_contract_issue_payment_list_data[0]["project_manpower_agency_name"] ;?>" 
											name="vendor_id">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_mode">Payment Mode*</label>
											<div class="controls">
												<select name="ddl_mode" required>
												<option>- - -Select Mode- - -</option>
												<?php
												for($count = 0; $count < count($payment_mode_list_data); $count++)
												{
													?>
												<option value="<?php echo $payment_mode_list_data[$count]["payment_mode_id"]; ?>"<?php if($payment_mode_list_data[$count]["payment_mode_id"] == $project_contract_issue_payment_list_data[0]["project_contract_issue_payment_mode"]){ ?> selected="selected" <?php } ?>><?php echo $payment_mode_list_data[$count]["payment_mode_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_details">Instrument Details</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_details" value="<?php echo $project_contract_issue_payment_list_data[0]["project_contract_issue_payment_instrument_details"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
											<input type="text" class="span6" value="<?php echo $project_contract_issue_payment_list_data[0]["project_contract_issue_payment_remarks"] ;?>" 
											name="txt_remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                        <br />
																					
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_project_contract_issue_payment_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
						</div>					
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->  
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function check_validity(total_amount,issued_amount,deduction)
{
	var amount = parseInt(document.getElementById('amount').value);
	
	var cur_amount = parseInt(amount);
	var total_amount  = parseInt(total_amount);
	var deduction  = parseInt(deduction);
	var issued_amount      = parseInt(issued_amount);
	if((cur_amount + issued_amount + deduction) > total_amount)
	{
		document.getElementById('amount').value = 0;
		alert('Cannot release greater than total value');
	}
}
function payable_amount()
{
	var deduction = parseInt(document.getElementById('deduction').value);
	var cur_amount = parseInt(document.getElementById('amount').value);
	
	var deduction = parseInt(deduction);
	var cur_amount  = parseInt(cur_amount);
	
	if(deduction > cur_amount)
	{
		document.getElementById('deduction').value = 0;
		alert('Cannot release greater than Amount');
	}
	
	total_payable_amount = cur_amount - deduction;
	document.getElementById('total_payable_amount').value = total_payable_amount;
	document.getElementById('hd_payable_amount').value = total_payable_amount;
		
}
</script>

  </body>

</html>
