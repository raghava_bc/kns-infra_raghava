<?php

/**

 * @author Nitin kashyap

 * @copyright 2015

 */



$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_bd_projects.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_config.php');



/*

PURPOSE : To add BD project

INPUT 	: Project, Location, Start Date, GFS Month, Launch Month, Development Time, Sale Time, File Path, Fund Source, Added By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_bd_project($project_name,$location,$start_date,$gfs_month,$launch_month,$dev_time,$sale_time,$file_path,$fund_source,$added_by)

{

	$bd_project_sresult = db_get_bd_project_list('',$project_name,$location,'','','','');

	if($bd_project_sresult["status"] == DB_NO_RECORD)

	{

		$bd_project_iresult = db_add_bd_project($project_name,$location,$start_date,$gfs_month,$launch_month,$dev_time,$sale_time,$file_path,$fund_source,$added_by);

			

		if($bd_project_iresult['status'] == SUCCESS)

		{

			$return["data"]   = "BD Project Successfully added";

			$return["status"] = SUCCESS;							

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}

	}

	else

	{

		$return["data"]   = "";

		$return["status"] = FAILURE;

	}

		

	return $return;

}



/*

PURPOSE : To get BD project list

INPUT 	: Project ID, Project Name, Location, Funding Source,Status of Project

OUTPUT 	: BD Project List or Error Details, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_bd_project_list($project_id,$project_name,$location,$fund_source,$status="")

{

	$bd_project_sresult = db_get_bd_project_list($project_id,$project_name,$location,$fund_source,'','','',$status);

	

	if($bd_project_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $bd_project_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No projects added yet!"; 

    }

	

	return $return;

}



/*

PURPOSE : To add file to BD project

INPUT 	: Project, Survey No, Owner, Owner Address, Owner Phone No, Village, Extent Owner Status, Cost, Process Status, JD Share Percent, Own Account, Remarks

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_file_to_bd_project($project_id,$survey_no,$sale_deed,$sale_deed_date,$owner,$owner_address,$owner_phone,$village,$extent,$owner_status,$cost,$broker_name,$broker_address,$process_status,$jd_share_percent,$own_account,$remarks,$added_by,$brokerage_amount="")

{

	$bd_project_sresult = db_get_mapped_survey_list('',$project_id,$survey_no,$owner,'','','','','','','');

	

	if($bd_project_sresult["status"] == DB_NO_RECORD)

	{

		$bd_project_iresult = db_add_survey_to_bd_project($project_id,$survey_no,$sale_deed,$sale_deed_date,$owner,$owner_address,$owner_phone,$village,$extent,$owner_status,$cost,$brokerage_amount,$broker_name,$broker_address,$process_status,$jd_share_percent,$own_account,$remarks,$added_by);

			

		if($bd_project_iresult['status'] == SUCCESS)

		{

			$return["data"]   = "File Successfully added to project";

			$return["status"] = SUCCESS;							

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}

	}

	else

	{

		$return["data"]   = "This survey number already exists for this project and owner";

		$return["status"] = FAILURE;

	}

		

	return $return;

}



/*

PURPOSE : To get BD files list

INPUT 	: File ID, Project ID, Survey No, Owner, Village, Owner Status, Process Status, Active

OUTPUT 	: BD File List or Error Details, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_bd_files_list($file_id,$project_id,$survey_no,$owner,$village,$owner_status,$process_status,$active,$sale_deed="")

{

	$bd_file_sresult = db_get_mapped_survey_list($file_id,$project_id,$survey_no,$owner,$village,$owner_status,$process_status,$active,'','','',$sale_deed="");

	

	if($bd_file_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $bd_file_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No file added yet!"; 

    }

	

	return $return;

}



/*

PURPOSE : To add BD project profitability calculations

INPUT 	: Project, Expected Rate, Stamp Duty, Dev Cost, Admin Charges, Finance Rate, Added By

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_bd_prof_calc_settings($project_id,$expected_rate,$stamp_duty,$dev_cost,$admin_charges,$finance_rate,$added_by)

{

	$bd_prof_calc_sresult = db_get_prof_calc_settings('',$project_id,'','','');

	if($bd_prof_calc_sresult["status"] == DB_NO_RECORD)

	{

		$bd_prof_calc_iresult = db_add_prof_calc_settings($project_id,$expected_rate,$stamp_duty,$dev_cost,$admin_charges,$finance_rate,$added_by);

			

		if($bd_prof_calc_iresult['status'] == SUCCESS)

		{

			$return["data"]   = "BD Profitability Calculation Settings Successfully added";

			$return["status"] = SUCCESS;							

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}

	}

	else

	{

		$return["data"]   = "Financial Workings already entered for this project";

		$return["status"] = FAILURE;

	}

		

	return $return;

}



/*

PURPOSE : To get BD project profitability calculation settings

INPUT 	: Calculation ID, Project ID

OUTPUT 	: BD Profitability Settings List or Error Details, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_bd_prof_calc_settings($calc_id,$project_id)

{

	$bd_prof_calc_sresult = db_get_prof_calc_settings($calc_id,$project_id,'','','');

	

	if($bd_prof_calc_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $bd_prof_calc_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No profitability calculation settings added yet!"; 

    }

	

	return $return;

}



/*

PURPOSE : To get BD files list

INPUT 	: File ID, Project ID, Survey No, Owner, Owner Address, Owner Phone No, Village, Extent, Owner Status, Cost, Process Status, JD Share Percent, Own Account, Remarks

OUTPUT 	: BD File ID or Error Details, success or failure message

BY 		: Nitin Kashyap

*/

function i_edit_bd_file($file_id,$project_id,$survey_no,$owner,$owner_address,$owner_phone,$village,$extent,$owner_status,$cost,$process_status,$broker_name,$broker_address,$jd_share_percent,$own_account,$remarks,$active,$brokerage_amount="",$sale_deed="",$sale_dead_date="")
{

	$bd_file_uresult = db_update_bd_file($file_id,$project_id,$survey_no,$owner,$owner_address,$owner_phone,$village,$extent,$owner_status,$cost,$broker_name,$broker_address,$process_status,$jd_share_percent,$own_account,$remarks,$active,$brokerage_amount,$sale_deed,$sale_dead_date);

	

	if($bd_file_uresult['status'] == SUCCESS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $bd_file_uresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "There was an internal error. Please try again later!"; 

    }

	

	return $return;

}



/*

PURPOSE : To remove file from a BD project

INPUT 	: File ID, Removed By

OUTPUT 	: BD File ID or Error Details, success or failure message

BY 		: Nitin Kashyap

*/

function i_remove_file_from_project($file_id,$removed_by)

{

	$bd_file_uresult = db_update_bd_file($file_id,'','','','','','','','','','','','','','','','0');

	

	if($bd_file_uresult['status'] == SUCCESS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $bd_file_uresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "There was an internal error. Please try again later!"; 

    }

	

	return $return;

}



/*

PURPOSE : To add file to a BD project

INPUT 	: File ID, Added By

OUTPUT 	: BD File ID or Error Details, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_file_to_project($file_id,$added_by)

{

	$bd_file_uresult = db_update_bd_file($file_id,'','','','','','','','','','','','','','','','1','','','');

	

	if($bd_file_uresult['status'] == SUCCESS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $bd_file_uresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "There was an internal error. Please try again later!"; 

    }

	

	return $return;

}



/*

PURPOSE : To edit BD project details

INPUT 	: Project ID, Name, Location, GFS Month, Launch Month, Development Time, Sale Time, File Path, Source of Funding, Added By

OUTPUT 	: BD File ID or Error Details, success or failure message

BY 		: Nitin Kashyap

*/

function i_edit_bd_project($project_id,$name,$location,$start_date,$gfs_month,$launch_month,$dev_time,$sale_time,$file_path,$source,$added_by)

{

	$bd_project_uresult = db_update_bd_project($project_id,'',$name,$location,$start_date,$gfs_month,$launch_month,$dev_time,$sale_time,$file_path,$source);

	

	if($bd_project_uresult['status'] == SUCCESS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $bd_project_uresult["data"];



		$edit_history_iresult = db_add_bd_project_history($project_id,$name,$location,$start_date,$gfs_month,$launch_month,$dev_time,$sale_time,$source,$added_by);

		

		if($edit_history_iresult["status"] == SUCCESS)

		{

			$return["status"] = SUCCESS;

			$return["data"]   = "Project Details successfully edited";

		}

		else

		{

			$return["status"] = FAILURE;

			$return["data"]   = "There was an internal error. Please try again later!";

		}

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "There was an internal error. Please try again later!"; 

    }

	

	return $return;

}



/*

PURPOSE : To add document to a BD file

INPUT 	: File ID, Document Title, Document path, Remarks, Added by

OUTPUT 	: Message, success or failure message

BY 		: Nitin Kashyap

*/

function i_add_bd_file_doc($file_id,$title,$path,$remarks,$added_by)

{

	$bd_file_doc_iresult = db_add_bd_file_doc($file_id,$title,$path,$remarks,$added_by);

		

	if($bd_file_doc_iresult['status'] == SUCCESS)

	{

		$return["data"]   = "File Successfully uploaded";

		$return["status"] = SUCCESS;							

	}

	else

	{

		$return["data"]   = "Internal Error. Please try again later";

		$return["status"] = FAILURE;

	}

		

	return $return;

}



/*

PURPOSE : To get BD docs for a file

INPUT 	: Doc ID, File ID, Added By

OUTPUT 	: BD Doc List or Error Details, success or failure message

BY 		: Nitin Kashyap

*/

function i_get_bd_file_docs($doc_id,$file_id,$added_by)

{

	$bd_file_doc_sresult = db_get_file_docs($doc_id,$file_id,$added_by,'','');

	

	if($bd_file_doc_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $bd_file_doc_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No document added yet!"; 

    }

	

	return $return;

}



/*

PURPOSE : To delete a BD file

INPUT 	: BD File ID, Deleted By

OUTPUT 	: BD File ID or Error Details, success or failure message

BY 		: Nitin Kashyap

*/

function i_delete_bd_file($file_id,$removed_by)

{

	$bd_file_uresult = db_update_bd_file($file_id,'','','','','','','','','','','','','','','','2');

	

	if($bd_file_uresult['status'] == SUCCESS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $bd_file_uresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "There was an internal error. Please try again later!"; 

    }

	

	return $return;

}



/*

PURPOSE : To add Delay Details

INPUT 	: File ID, Reason ID, Is Sub Task, Remarks, Added By

OUTPUT 	: Message, success or failure message

BY 		: Sonakshi D

*/

function i_add_delay_details($delay_reasons_file_id,$delay_reason_id,$delay_is_task,$delay_remarks,$delay_added_by)

{	

	$reasons_iresult = db_add_bd_delay_reasons($delay_reasons_file_id,$delay_reason_id,$delay_is_task,$delay_remarks,$delay_added_by);

	

	if($reasons_iresult["status"] == SUCCESS)

	{

		$return["status"] = SUCCESS;

		$return["data"]   = "Delay Details updated successfully";

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "There was an Internal Error";

	}

		

	return $return;		

}



/*

PURPOSE : To get Delay Details

INPUT 	: File ID

OUTPUT 	: Delay List, success or failure message

BY 		: Sonakshi D

*/

function i_get_delay_details($delay_reasons_file_id)

{

	$reasons_search_data   = array('delay_reason_file_id'=>$delay_reasons_file_id);

	$delay_details_sresult = db_get_bd_delay_reasons($reasons_search_data);

	

	if($delay_details_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $delay_details_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No delay details added for this file"; 

    }

	

	return $return;

}



/*

PURPOSE : To delete BD project details

INPUT 	: Project ID, Status, Updated By

OUTPUT 	: BD File ID or Error Details, success or failure message

BY 		: Soankshi D

*/

function i_delete_bd_project($project_id,$status,$updated_by)

{

	// Get files under this project

	$project_files_list = i_get_bd_files_list('',$project_id,'','','','','','');

	if($project_files_list['status'] == SUCCESS)

	{

		$remove_status = 1;

		$remove_result = '';

		for($count = 0; $count < count($project_files_list['data']); $count++)

		{

			$bd_file_remove_result = i_remove_file_from_project($project_files_list['data'][$count]['bd_project_file_id'],$updated_by);

			

			if($bd_file_remove_result['status'] != SUCCESS)

			{

				$remove_status = $remove_status & 0;

				$remove_result = $remove_result.$project_files_list['data'][$count]['bd_project_file_id'];

			}

		}

	}

	else

	{

		// No files under this project.

		$remove_status = 1;

	}

	

	if($remove_status == 1)

	{

		$bd_project_uresult = db_update_bd_project($project_id,$status,'','','','','','','','','');

		if($bd_project_uresult['status'] == SUCCESS)

		{

			$return["status"] = SUCCESS;

			$return["data"]   = $bd_project_uresult["data"];



			$edit_history_iresult = db_add_bd_project_history($project_id,'','','','','','','','',$updated_by);

			

			if($edit_history_iresult["status"] == SUCCESS)

			{

				$return["status"] = SUCCESS;

				$return["data"]   = "Project Details successfully Deleted";

			}

			else

			{

				$return["status"] = FAILURE;

				$return["data"]   = "There was an internal error. Please try again later!";

			}

		}

		else

		{

			$return["status"] = FAILURE;

			$return["data"]   = "There was an internal error. Please try again later!"; 

		}

	}

	else

	{

		$return["status"] = FAILURE;

		$return["data"]   = "Project not deleted because the following files were not removed from the project: ".$remove_result; 

	}

	

	return $return;

}



/*

PURPOSE : To update financial workings of a project

INPUT 	: Project ID, Finance Working Data

OUTPUT 	: Case Follow Up ID or Error Details, success or failure message

BY 		: Nitin Kashyap

*/

function i_update_bd_prof_calc_settings($project_id,$finance_working_data)

{

	$prof_calc_uresult = db_update_prof_calc_settings($project_id,$finance_working_data);

	

	if($prof_calc_uresult['status'] == SUCCESS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = 'Financial Working Data updated successfully';

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "There was an internal error. Please try again later!"; 

    }

	

	return $return;

}



/*

PURPOSE : To add land Status

INPUT 	: File ID,Land Status,Date,Added By

OUTPUT 	: Message, success or failure message

BY 		: Punith

*/

function i_add_bd_land_status($file_id,$land_status,$date,$added_by)

{

	$bd_land_land_sresult = db_get_land_status($file_id,$land_status,'','','','');

	if($bd_land_land_sresult["status"] == DB_NO_RECORD)

	{

		$bd_land_iresult = db_add_land_status($file_id,$land_status,$date,$added_by);

		

		if($bd_land_iresult['status'] == SUCCESS)

		{

			$return["data"]   = "BD Land Status Successfully added";

			$return["status"] = SUCCESS;							

		}

		else

		{

			$return["data"]   = "Internal Error. Please try again later";

			$return["status"] = FAILURE;

		}

	}

	else

	{

		$return["data"]   = "The file is already in the status mentioned by you";

		$return["status"] = FAILURE;

	}

		

	return $return;

}



/*

PURPOSE : To get BD land status List

INPUT 	: File Id, Land Status, Date, Added By,Start Date, End Date

OUTPUT 	: BD Land Status List or Error Details, success or failure message

BY 		: Punith

*/

function i_get_bd_land_status_list($file_id,$land_status,$date,$added_by)

{

	$bd_land_sresult = db_get_land_status($file_id,$land_status,$date,$added_by,'','');

	

	if($bd_land_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $bd_land_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "Land Status not updated yet"; 

    }

	

	return $return;

}



/*

PURPOSE : To update BD Delay reason

INPUT 	: Delay Reason ID, Delay Reason Data

OUTPUT 	: Delay Reason ID or Error Details, success or failure message

BY 		: Nitin Kashyap

*/

function i_update_bd_delay_reason($delay_reason_id,$delay_reason_data)

{

	$delay_reason_uresult = db_update_bd_delay_reason($delay_reason_id,$delay_reason_data);

	

	if($delay_reason_uresult['status'] == SUCCESS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $delay_reason_uresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "There was an internal error. Please try again later!"; 

    }

	

	return $return;

}



/*

PURPOSE : To add BD borrow Details

INPUT 	: Custody, File ID, Bank, Mortgage, Loan Value, Released Date, Active, Added By

OUTPUT 	: Message, success or failure message

BY 		: Sonakshi

*/

function i_add_bd_borrow($custody,$file_id,$bank,$mortgage,$recieved_loan,$name,$released_date,$remarks,$added_by)

{

	$bd_borrow_iresult = db_add_bd_file_borrow_details($custody,$file_id,$bank,$mortgage,$recieved_loan,$name,$released_date,$remarks,$added_by);

		

	if($bd_borrow_iresult['status'] == SUCCESS)

	{

		$return["data"]   = "BD  borrow Details Successfully added";

		$return["status"] = SUCCESS;							

	}

	else

	{

		$return["data"]   = "Internal Error. Please try again later";

		$return["status"] = FAILURE;

	}

		

	return $return;

}



/*

PURPOSE : To get BD borrow details list

INPUT 	: Custody, File ID, Bank, Mortgage, Loan Value, Released Date, Active, Added By, Village, Added On, Borrow ID, Mortgage Start Date, Mortgage End Date, Village

OUTPUT 	: BD borrow details List or Error Details, success or failure message

BY 		: Sonakshi

*/

function i_get_bd_borrow_list($custody,$file_id,$bank,$mortgage,$loan_value,$released_date,$active,$added_by,$village,$added_on,$borrow_id='',$mort_start_date='',$mort_end_date='')

{

	$bd_borrow_sresult = db_get_bd_file_borrow_list($custody,$file_id,$bank,$mortgage,$loan_value,$released_date,$active,$added_by,$village,$added_on,$borrow_id,$mort_start_date,$mort_end_date,$village);

	

	if($bd_borrow_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   = $bd_borrow_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No borrow details added yet!"; 

    }

	

	return $return;

}



/*

PURPOSE : To update BD borrow details list

INPUT 	: Custody, File ID, Bank, Mortgage, Loan Value, Released Date, Active, Added By

OUTPUT 	: success or failure message

BY 		: Sonakshi

*/

function i_update_bd_file_borrow($borrow_id,$custody,$file_id,$bank,$mortgage,$loan_value,$borrower,$released_date,$active,$remarks)

{

	$bd_borrow_uresult = db_update_bd_file_borrow($borrow_id,$custody,$file_id,$bank,$mortgage,$loan_value,$borrower,$released_date,$active,$remarks);

	

	if($bd_borrow_uresult['status'] == SUCCESS)

	{

		$return["data"]   = "BD Borrow details successfully updated";

		$return["status"] = SUCCESS;							

	}

	else

	{

		$return["data"]   = "Internal Error. Please try again later";

		$return["status"] = FAILURE;

	}

		

	return $return;

}



/*

PURPOSE : To add BD Land Bank

INPUT 	: File ID, Remarks, Added By

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/

function i_add_bd_land_bank($file_id,$remarks,$added_by)

{

	$bd_land_bank_iresult = db_add_bd_land_bank($file_id,$remarks,$added_by);

	

	if($bd_land_bank_iresult['status'] == SUCCESS)

	{

		$return["data"]   = "bd land bank Successfully Added";

		$return["status"] = SUCCESS;	

	}

	else

	{

		$return["data"]   = "Internal Error. Please try again later";

		$return["status"] = FAILURE;

	}

		

	return $return;

}

	

/*

PURPOSE : To get BD Land Bank list

INPUT 	: BD Land Bank ID, File ID, Added By, Start Date(for added on), End Date(for added on)

OUTPUT 	: BD Land Bank List or Error Details, success or failure message

BY 		: Lakshmi

*/

function i_get_bd_land_bank_list($bd_land_bank_search_data)

{

	$bd_land_bank_sresult = db_get_bd_land_bank_list($bd_land_bank_search_data);

	

	if($bd_land_bank_sresult['status'] == DB_RECORD_ALREADY_EXISTS)

    {

		$return["status"] = SUCCESS;

        $return["data"]   =$bd_land_bank_sresult["data"]; 

    }

    else

    {

	    $return["status"] = FAILURE;

        $return["data"]   = "No bd land bank added. Please contact the system admin"; 

    }

	

	return $return;

}



/*

PURPOSE : To update BD Land Bank

INPUT 	: BD Land Bank ID, BD Land Bank Update Array

OUTPUT 	: Message, success or failure message

BY 		: Lakshmi

*/

function i_update_bd_land_bank($bank_id,$bd_land_bank_update_data)

{

	$bd_land_bank_sresult = db_update_bd_land_bank($bank_id,$bd_land_bank_update_data);

	

	if($bd_land_bank_sresult['status'] == SUCCESS)

	{

		$return["data"]   = "bd land bank Successfully added";

		$return["status"] = SUCCESS;							

	}

	else

	{

		$return["data"]   = "Internal Error. Please try again later";

		$return["status"] = FAILURE;

	}

		

	return $return;

}

?>