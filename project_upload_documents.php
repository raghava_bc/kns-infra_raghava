<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 4th oct 2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* DEFINES - START */
define('METHOD_PLAN_FUNC_ID','282');
/* DEFINES - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_quotation_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',METHOD_PLAN_FUNC_ID,'2','1');
	$add_perms_list    = i_get_user_perms($user,'',METHOD_PLAN_FUNC_ID,'1','1');
	$edit_perms_list   = i_get_user_perms($user,'',METHOD_PLAN_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',METHOD_PLAN_FUNC_ID,'4','1');

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	// Query String Data
	if(isset($_REQUEST['task_id']))
	{
		$task_id = $_REQUEST['task_id'];
	}
	else
	{
		$task_id = '-1';
	}
	if(isset($_REQUEST['plan_id']))
	{
		$plan_id = $_REQUEST['plan_id'];
	}
	else
	{
		$plan_id = '';
	}
	// Nothing
	if(isset($_REQUEST['project_plan_process_id']))
	{
		$process_id = $_REQUEST['project_plan_process_id'];
	}
	else
	{
		$process_id = "";
	}

	// Capture the form data
	if(isset($_POST["add_method_plan_submit"]))
	{
		$process_id    = $_POST["hd_process_id"];
		$task_id       = $_POST["hd_task_id"];
		$doc 		   = upload("file_remarks_doc",$user);
		$plan_type     = $_POST["rb_plan_type"];
		$remarks       = $_POST["stxt_remarks"];
		$task_iresult  = i_add_project_task_method_planning($task_id,$doc,$plan_type,$remarks,$user);
	}

	// Get Project Management Master modes already added
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list['status'] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list['data'];
	}

	else
	{
		$alert = $project_management_master_list["data"];
		$alert_type = 0;
	}

	//Get Task List
	$project_task_method_planning_search_data = array("active"=>'1',"task_id"=>$task_id);
	$project_task_method_planning_list = i_get_project_task_method_planning($project_task_method_planning_search_data);
	if($project_task_method_planning_list["status"] == SUCCESS)
	{
		$project_task_method_planning_list_data = $project_task_method_planning_list["data"];
		$project_process_name     = $project_task_method_planning_list_data[0]["project_process_master_name"];
		$project_task_name        = $project_task_method_planning_list_data[0]["project_task_master_name"];
		$project_name             = $project_task_method_planning_list_data[0]["project_master_name"];
	}

}
else
{
	header("location:login.php");
}

// Functions
function upload($file_id,$user_id)
{
	if($_FILES[$file_id]["name"]!="")
	{
		if ($_FILES[$file_id]["error"] > 0)
		{
			// echo "Return Code: " . $_FILES[$file_id]["error"] . "<br />";
		}
		else
		{
			$_FILES[$file_id]["name"]=$user_id."_".$_FILES[$file_id]["name"];
			move_uploaded_file($_FILES[$file_id]["tmp_name"], "documents/".$_FILES[$file_id]["name"]);
		}
	}

	return $_FILES[$file_id]["name"];
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Add Method Plan</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">

	<div class="main-inner">

	    <div class="container">

	      <div class="row">

	      	<div class="span12">

	      		<div class="widget ">

	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Add Method Plan &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Project:  &nbsp;<?php echo $project_name; ?>&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbspProcess:  &nbsp;<?php echo $project_process_name; ?>&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Task:  &nbsp;<?php echo $project_task_name; ?></h3><span style="float:right; padding-right:20px;"><a href="project_task_method_planning_list.php">Project Task Method Planning List</a></span>
	  				</div> <!-- /widget-header -->

					<div class="widget-content">



						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Method Plan</a>
						  </li>
						</ul>
						<br>
							<div class="control-group">
								<div class="controls">
								<?php
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>

								<?php
								if($alert_type == 1) // Success
								{
								?>
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
								<?php
								}
								?>
								</div> <!-- /controls -->
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_method_plan_form" class="form-horizontal" method="post" action="project_upload_documents.php" enctype="multipart/form-data">
								<input type="hidden" name="hd_task_id" value="<?php echo $task_id; ?>" />
								<input type="hidden" name="hd_process_id" value="<?php echo $process_id; ?>" />
									<fieldset>

										<div class="control-group">
											<label class="control-label" for="file_remarks_doc">Document</label>
											<div class="controls">
											<input type="file" name="file_remarks_doc" id="file_remarks_doc" />
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

										<div class="control-group">
										<label class="control-label" for="rb_plan_type" >Document Type</label>
										<div class="controls">
											<input type="radio" name="rb_plan_type" id="rb_plan_type" value="1"/> Study Document&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="radio" name="rb_plan_type" id="rb_plan_type" value="2"/> Check List&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="radio" name="rb_plan_type" id="rb_plan_type" value="3"/> Cad Drawings
										</div> <!-- /controls -->
									    </div> <!-- /control-group -->

										<div class="control-group">
											<label class="control-label" for="stxt_remarks">Remarks</label>
											<div class="controls">
												<textarea name="stxt_remarks"  class="span6" rows="4" cols="50" placeholder="Remarks"></textarea>
											</div> <!-- /controls -->
										</div> <!-- /control-group -->

                                                                                                                                                               										 <br />

										<?php
										if($add_perms_list['status'] == SUCCESS)
										{
										?>
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_method_plan_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
										<?php
										}
										else
										{
											?>
											<div class="form-actions">
											<?php echo 'You are not authorized to add a Method Plan'; ?>
											</div> <!-- /form-actions -->
											<?php
										}
										?>
									</fieldset>
								</form>
								</div>
							</div>

							<div class="widget-content">

              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Document</th>
					<th>Document Type</th>
					<th>Remarks</th>
					<th>Added By</th>
					<th>Added On</th>
					<th colspan="4" style="text-align:center;">Actions</th>
				</tr>
				</thead>
				<tbody>
				<?php
					if($project_task_method_planning_list["status"] == SUCCESS)
					{
					$sl_no = 0;
					for($count = 0; $count < count($project_task_method_planning_list_data); $count++)
					{

						$sl_no++;
						$doc = $project_task_method_planning_list_data[$count]["project_task_method_planning_document"];
						$plan_type = $project_task_method_planning_list_data[$count]["project_task_method_planning_type"];
						if($plan_type == 1)
						{
							$plan_type = "Study Document";
						}
						elseif($plan_type == 2)
						{
							$plan_type = "Check List";
						}
						else
						{
							$plan_type = "Cad Drawings";
						}


					if($doc != 0)
					{
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><a href="documents/<?php echo $project_task_method_planning_list_data[$count]["project_task_method_planning_document"]; ?>" target="_blank"><?php echo $project_task_method_planning_list_data[$count]["project_task_method_planning_document"]; ?></a></td>
					<td><?php echo $plan_type; ?></td>
					<td><?php echo $project_task_method_planning_list_data[$count]["project_task_method_planning_remarks"]; ?></td>
					<td><?php echo $project_task_method_planning_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_task_method_planning_list_data[$count][
					"project_task_method_planning_added_on"])); ?></td>
					<td><?php if(($project_task_method_planning_list_data[$count]["project_task_method_planning_active"] == "1")){?><a href="#" onclick="return project_delete_method_plan('<?php echo $project_task_method_planning_list_data[$count]["project_task_method_planning_id"]; ?>','<?php echo $task_id ;?>');">Delete</a><?php } ?></td>

					</tr>
					<?php
					}
					}

				}
				else
				{
				?>
				<td colspan="6">No Project Master condition added yet!</td>

				<?php
				}
				 ?>

                </tbody>
              </table>
            </div>

					</div> <!-- /widget-content -->

				</div> <!-- /widget -->

		    </div> <!-- /span8 -->




	      </div> <!-- /row -->

	    </div> <!-- /container -->

	</div> <!-- /main-inner -->

</div> <!-- /main -->




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>

function project_delete_method_plan(planning_id,task_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_upload_documents.php?task_id=" +task_id;
					}
				}
			}

			xmlhttp.open("POST", "ajax/project_delete_method_plan.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("planning_id=" + planning_id + "&action=0");
		}
	}
}
</script>

  </body>

</html>
