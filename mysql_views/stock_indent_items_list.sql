-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 22, 2018 at 01:38 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kns_erp_live`
--

-- --------------------------------------------------------

--
-- Structure for view `stock_indent_items_list`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `stock_indent_items_list` AS select `SII`.`stock_indent_item_id` AS `indent_item_id`,`SI`.`stock_indent_id` AS `indent_id`,`SI`.`stock_indent_no` AS `indent_no`,`SI`.`stock_indent_project` AS `project_id`,`SP`.`stock_project_name` AS `project_name`,`SII`.`stock_indent_item_material_id` AS `material_id`,`SMM`.`stock_material_code` AS `material_code`,`SMM`.`stock_material_name` AS `material_name`,`SMM`.`stock_material_price` AS `material_price`,`SUMM`.`stock_unit_name` AS `uom`,`SII`.`stock_indent_item_quantity` AS `material_qty`,`U`.`user_name` AS `indent_by`,`SI`.`stock_indent_added_on` AS `added_on`,`SII`.`stock_indent_item_status` AS `indent_item_status`,`SII`.`stock_indent_item_active` AS `indent_item_active` from ((((((`stock_indent_items` `SII` join `users` `U` on((`SII`.`stock_indent_item_added_by` = `U`.`user_id`))) join `stock_material_master` `SMM` on((`SII`.`stock_indent_item_material_id` = `SMM`.`stock_material_id`))) join `stock_indent` `SI` on((`SI`.`stock_indent_id` = `SII`.`stock_indent_id`))) join `stock_unit_measure_master` `SUMM` on((`SUMM`.`stock_unit_id` = `SMM`.`stock_material_unit_of_measure`))) join `stock_project` `SP` on((`SP`.`stock_project_id` = `SI`.`stock_indent_project`))) left join `stock_location_master` `SLM` on((`SLM`.`stock_location_id` = `SI`.`stock_indent_location`)));

--
-- VIEW  `stock_indent_items_list`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
