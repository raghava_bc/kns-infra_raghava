<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';

/* DEFINES - START */
define('PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID', '253');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '2', '1');
$edit_perms_list   	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '3', '1');
$delete_perms_list 	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '4', '1');
$ok_perms_list   	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '5', '1');
$approve_perms_list = i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '6', '1');

    // Query String Data
    // Nothing

    $alert_type = -1;
    $alert = "";

    $search_project = "";

    if (isset($_POST["search_project"])) {
        $search_project   = $_POST["search_project"];
    // set here
    } else {
        $search_project = "";
    }

    $search_process = "kill D";
    if (isset($_POST["search_process"])) {
        $search_process = $_POST["search_process"];
    }
    else if (isset($_REQUEST["hd_process_id"])) {
      $search_process = $_REQUEST["hd_process_id"];

    }
    else {
      $search_process = "kill me";
    }

    $search_task = "";
    if (isset($_POST["search_task"])) {
        $search_task = $_POST["search_task"];
    }
    else if (isset($_REQUEST["hd_task_id"])) {
      $search_task =  $_REQUEST["hd_task_id"];
    }
    else {
      $search_task = "";
    }
    $search_status = "";
    if (isset($_POST["search_status"])) {
        $search_status = $_POST["search_status"];
    }

    if (isset($_POST["complete_remarks_submit"])) {

  				$remarks_id 			 = $_POST["hd_remarks_id"];
  				$project_id		     = $_POST["hd_project_id"];
  				$completed_remarks = $_POST["completed_remarks"];
  				$process_remarks_update_data = array("status"=>"Completed","completed_remarks"=>$completed_remarks);
  				$payment_manpower_mapping_iresult = db_update_process_remarks_task($remarks_id,$process_remarks_update_data);
  				header("location:project_process_remarks_list.php?search_project=$project_id");
  	}

    // Get Project Management Master modes already added
    $project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
    $project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
    if ($project_management_master_list['status'] == SUCCESS) {
        $project_management_master_list_data = $project_management_master_list['data'];
    } else {
        $alert = $project_management_master_list["data"];
        $alert_type = 0;
    }
} else {
    header("location:login.php");
}
?>


<html>
  <head>
    <meta charset="utf-8">
    <title>Project Task Actual Man Power List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
    <script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/bootstrap.js"></script>
		<script data-jsfiddle="common" src="js/handsontable-master/demo/js/moment/moment.js"></script>
    <link rel="stylesheet" type="text/css" href="tools/datatables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="tools/datatables/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="datatable.css">

     <script src="tools/datatables/js/jquery.dataTables.min.js"></script>
		 <script src="datatable/project_completed_remarks.js"></script>

  </head>
  <body>

    <?php
    include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
    ?>

  <div class="main">
    <div class="main-inner">
      <div class="container">
        <div class="row">

            <div class="span6" style="width:100%;">

            <div class="widget widget-table action-table">
              <div class="widget-header"> <i class="icon-th-list"></i>
                <h3>To-Do List</h3>
              </div>
              <!-- /widget-header -->

              <div class="widget-content">

								<?php
                                if ($view_perms_list['status'] == SUCCESS) {
                                    ?>
								<div class="widget-header" style="height:80px; padding-top:10px;">
								  <form method="post" id="file_search_form" action="project_process_remarks_list.php">
                    <input type="hidden" id="hd_process_id" name="hd_process_id" value="<?php echo $search_process ;?>">
                    <input type="hidden" id="hd_task_id" name="hd_task_id" value="<?php echo $search_task ;?>">
                    <span style="padding-left:20px; padding-right:20px;">
            			  <select id= "search_project" name="search_project">
            			  <option value="">- - Select Project - -</option>
            			  <?php
                          for ($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++) {
                              ?>
            			  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if ($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) {
                                  ?> selected="selected" <?php
                              } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
            			  <?php
                          }
                          ?>
            			  </select>
            			  </span>

                    <span style="padding-left:20px; padding-right:20px;">
                   <select id="search_process" name="search_process">
                   <option value="">- - Select Process - -</option>
                   </select>
                   </span>

                    <span style="padding-left:20px; padding-right:20px;">
                   <select id="search_task" name="search_task">
                   <option value="">- - Select Task - -</option>
                   </select>
                   </span>

								  <input type="submit" name="manpower_search_submit" />

									<script>
									var formElements = [];
									formElements['search_project'] = <?=  json_encode($search_project); ?>;
									formElements['search_process'] = <?=  json_encode($search_process) ; ?>;
									formElements['search_task'] = <?=  json_encode($search_task) ; ?>;
									formElements['search_status'] = <?=  json_encode($search_status) ; ?>;

									setVars(formElements);
									</script>

								  </form>
					            </div>
								 <?php
                                } else {
                                    echo 'You are not authorized to view this page';
                                }
                                ?>
                                <?php
                                      if ($view_perms_list['status'] == SUCCESS) {
                                          ?>
                <table class="table table-bordered" cellpadding="0" cellspacing="0" border="0" class="display dataTable" id="example" aria-describedby="example_info">
                  <tbody>
                  <thead>
                <tr style="background-color: #f2f2f2">
                <th style="word-wrap:break-word;">#</th>
                <th style="word-wrap:break-word;">Process</th>
                <th style="word-wrap:break-word;" >Task</th>
                <th style="word-wrap:break-word;" >Manpower</th>
                <th style="word-wrap:break-word;" >Machine</th>
                <th style="word-wrap:break-word;" >Contrct</th>
                <th style="word-wrap:break-word;" >Material</th>
                <th style="word-wrap:break-word;" >Remarks</th>
                <th style="word-wrap:break-word;" >Added By</th>
                <th style="word-wrap:break-word;" >Added On</th>
                <th style="word-wrap:break-word;">Action</th>
                </tr>
                </thead>
                  </tbody>
                </table>
                <?php
                   } else {
                       echo 'You are not authorized to view this page';
                   }
                   ?>
              </div>


              <!-- /widget-content -->
            </div>
            <!-- /widget -->

            </div>
            <!-- /widget -->
          </div>
          <!-- /span6 -->
        </div>
        <!-- /row -->
      </div>
      <!-- /container -->
    </div>

</body>

  <div class="extra">

  	<div class="extra-inner">

  		<div class="container">

  			<div class="row">

                  </div> <!-- /row -->

  		</div> <!-- /container -->

  	</div> <!-- /extra-inner -->

  </div> <!-- /extra -->




  <div class="footer">

  	<div class="footer-inner">

  		<div class="container">

  			<div class="row">

      			<div class="span12">
      				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
      			</div> <!-- /span12 -->

      		</div> <!-- /row -->

  		</div> <!-- /container -->

  	</div> <!-- /footer-inner -->

  </div> <!-- /footer -->
  <script>
  $(document).ready(function() {
    project_id = document.getElementById("search_project").value;
    $.ajax({
      url: 'ajax/project_get_process.php',
      data: {project_id: project_id},
      dataType: 'json',
      success: function(response) {
        console.log('hope i got all users ', response);

        $("#search_process").empty();
        $("#search_task").empty();
        $("#search_process").append("<option value=0>Select Process</option>");
        for(var i=0; i< response.length; i++) {
          var id = response[i]['process_master_id'];
          var name = response[i]['process_name'];
           var sel_process_id = document.getElementById("search_process");
           $("#search_process").append("<option value='"+id+"'>"+name+"</option>");
        }
      }
    })
})

  $(document).ready(function() {
  $("#search_project").change(function() {
    var project_id = $(this).val();
    console.log('project_id ', project_id);
    if(project_id == 0) {
      $("#search_process").empty();
      return false;
    }
    $.ajax({
      url: 'ajax/project_get_process.php',
      data: {project_id: project_id},
      dataType: 'json',
      success: function(response) {
        console.log('hope i got all users ', response);

        $("#search_process").empty();
        $("#search_task").empty();
        $("#search_process").append("<option value=0>Select Process</option>");
        for(var i=0; i< response.length; i++) {
          var id = response[i]['process_master_id'];
          var name = response[i]['process_name'];
           var sel_process_id = document.getElementById("search_process");
           $("#search_process").append("<option value='"+id+"'>"+name+"</option>");
        }
      }
    })
  })

  $("#search_process").change(function() {
    var process_id = $(this).val();
    console.log('process_id ', process_id);
    if(process_id == 0) {
      $("#search_task").empty();
      return false;
    }
    $.ajax({
      url: 'ajax/project_get_task_master.php',
      data: {process_id: process_id},
      dataType: 'json',
      success: function(response) {
        console.log('hope i got all users ', response);

        $("#search_task").empty();
         $("#search_task").append("<option value=0>Select Task</option>");
        for(var i=0; i< response.length; i++) {
          var id = response[i]['project_task_master_id'];
          var name = response[i]['project_task_master_name'];
          // var class = 'list-group-item';

           $("#search_task").append("<option value='"+id+"'>"+name+"</option>");
        }
      }
    })
  })


  })
  </script>

</html>
