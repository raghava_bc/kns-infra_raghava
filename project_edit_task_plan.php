<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 08-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_mail.php');
require("utilities/sendgrid/sendgrid-php.php");

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    /* DATA INITIALIZATION - START */
    $alert_type = -1;
    $alert = "";
    /* DATA INITIALIZATION - END */
    $task_data = $_POST['task_data'];
    $task_data_array = json_decode($task_data, true);
    $data_value = $task_data_array["data"];
    $task_planning = $task_data_array["planning_ids"];
    $project_id = $task_data_array["project_id"];
    $table_header = "<table  style=border-collapse:collapse;width:100%>
		<tr>
		<th style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;background-color: #2C4099;color: white;'>Process</th>
		<th style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;background-color: #2C4099;color: white;'>Task</th>
		<th style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;background-color: #2C4099;color: white;'>Road</th>
		<th style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;background-color: #2C4099;color: white;'>UOM</th>
		<th style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;background-color: #2C4099;color: white;'>Old<br> Msrmnt</th>
		<th style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;background-color: #2C4099;color: white;'>New<br> Msrmnt</th>
		<th style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;background-color: #2C4099;color: white;'>Diff  <br/> Msrmnt</th>
		<th style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;background-color: #2C4099;color: white;'>Old <br/> End date</th>
		<th style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;background-color: #2C4099;color: white;'>New <br/> End date</th>
		<th style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;background-color: #2C4099;color: white;'>Diff  <br/> Days</th>
		</tr>";

    //Get Email list
    $project_email_search_data = array("project_id"=>$project_id);
    $email_list = db_get_project_email_list($project_email_search_data);
    if ($email_list["status"] == DB_RECORD_ALREADY_EXISTS) {
        $email_list_data = $email_list["data"];
        $to = $email_list_data[0]["project_email_details_to"];
        $cc = $email_list_data[0]["project_email_details_cc"];
        $bcc = $email_list_data[0]["project_email_details_bcc"];
    }
    for ($task_count = 0 ; $task_count < (count($data_value)) ; $task_count++) {
        $process_name   = $data_value[$task_count][0];
        $task_name    	= $data_value[$task_count][1];
        $task_id    	= $data_value[$task_count][2];
        $uom	    	= $data_value[$task_count][3];
        $measurment 	= $data_value[$task_count][4];
        $road_name	 	= $data_value[$task_count][5];
        $object_type 	= $data_value[$task_count][6];
        $object_name 	= $data_value[$task_count][7];
        $no_of_objects 	= $data_value[$task_count][8];
        $per_day_output = $data_value[$task_count][9];
        $total_days	    = $data_value[$task_count][10];
        $rate			= $data_value[$task_count][11];
        $start_date 	= $data_value[$task_count][12];
        $end_date	 	= $data_value[$task_count][13];
        $planning_id 	= $data_value[$task_count][14];
        if (in_array($planning_id, $task_planning)) {
            $task_planning_id = $planning_id ;
            // Get list of task plans for this process plan
            $project_task_planning_search_data = array("active"=>'1',"planning_id"=>$task_planning_id);
            $project_task_planning_list = i_get_project_task_planning($project_task_planning_search_data);
            if ($project_task_planning_list["status"] == SUCCESS) {
                $project_task_planning_list_data = $project_task_planning_list["data"];
                $old_measurment = $project_task_planning_list_data[0]["project_task_planning_measurment"];
                $old_end_date  = $project_task_planning_list_data[0]["project_task_planning_end_date"];
                $old_start_date  = $project_task_planning_list_data[0]["project_task_planning_start_date"];
                $project_name  = $project_task_planning_list_data[0]["project_master_name"];
                $process_name  = $project_task_planning_list_data[0]["project_process_master_name"];
                $task_name  = $project_task_planning_list_data[0]["project_task_master_name"];
                $road_name  = $project_task_planning_list_data[0]["project_site_location_mapping_master_name"];
            }
        } else {
            $task_planning_id = "";
        }
        // Machine Type List
        $project_machine_type_master_search_data = array("active"=>'1',"name"=>$object_name);
        $project_machine_type_master_list = i_get_project_machine_type_master($project_machine_type_master_search_data);
        if ($project_machine_type_master_list["status"] == SUCCESS) {
            $project_machine_type_master_list_data = $project_machine_type_master_list["data"];
            $object_name_id = $project_machine_type_master_list_data[0]["project_machine_type_master_id"];
        } else {
            // Get Project CW Master List
            $project_cw_master_search_data = array("active"=>'1',"name"=>$object_name);
            $project_cw_master_list = i_get_project_cw_master($project_cw_master_search_data);
            if ($project_cw_master_list['status'] == SUCCESS) {
                $project_cw_master_list_data = $project_cw_master_list["data"];
                $object_name_id = $project_cw_master_list_data[0]["project_cw_master_id"];
            }
        }

        $start_date_formatted = get_formatted_date($start_date, "Y-m-d");
        $end_date_formatted = get_formatted_date($end_date, "Y-m-d");
        $diff = abs($old_measurment - $measurment);
				$date_diff = get_date_diff($old_end_date,$end_date_formatted);
        
        //Data Insertion
        // if ($measurment != 0 && $total_days>=1 ) {
        if ($measurment > $old_measurment ) {
        ?>
        <script>
          console.log('measurement ', <?= $measurment; ?>, <?= $old_measurment; ?>);
        </script>
        <?php

            $project_task_planning_update_data= array("measurment"=>$measurment,"object_type"=>$object_type,
            "machine_type"=>$object_name_id,"no_of_object"=>$no_of_objects,"per_day_out"=>$per_day_output,"total_days"=>$total_days,"plan_start_date"=>$start_date_formatted,"plan_end_date"=>$end_date_formatted);
            $project_task_planning_iresult = i_update_project_task_planning($planning_id, '', $project_task_planning_update_data);
            if (($project_task_planning_iresult["status"] == SUCCESS) && ($task_planning_id != "")) {
                $planning_id1 = $project_task_planning_iresult["data"];

                $task_plan_history_iresults = db_add_project_task_planning_shadow($planning_id1, $old_measurment, $measurment, $old_end_date, $end_date_formatted, $old_start_date, $start_date_formatted, $user);
                // Compose
								$table_content .= "<tr>
								<td  style='border: 1px solid #ddd;padding: 8px; '>".$process_name."</td>
								<td  style='border: 1px solid #ddd;padding: 8px; '>".$task_name."</td>
								<td  style='border: 1px solid #ddd;padding: 8px; '>".$road_name."</td>
								<td  style='border: 1px solid #ddd;padding: 8px; '>".$uom."</td>
								<td  style='border: 1px solid #ddd;padding: 8px; '>".$old_measurment."</td>
								<td  style='border: 1px solid #ddd;padding: 8px; '>".$measurment."</td>
								<td  style='border: 1px solid #ddd;padding: 8px; color: red;'>".$diff."</td>
								<td  style='border: 1px solid #ddd;padding: 8px; '>".date("d-M-y",strtotime($old_end_date))."</td>
								<td  style='border: 1px solid #ddd;padding: 8px; '>".date("d-M-y",strtotime($end_date_formatted))."</td>
								<td  style='border: 1px solid #ddd;padding: 8px; color:red;'>".abs($date_diff["data"])."</td>
								</tr>
				";

                	$subject = '[Notice] Project '.$project_name.' Measurment and End dates have been changed';
            }
        }
    }

		$heading = "<h3>
								Project: <span style='color:#00ba8b;'>".$project_name ."</span>
								</h3>
								<h3>
								Changed By: <span style='color:#00ba8b;margin-right:30px;'>". $loggedin_name ."</span>

								Changed On: <span style='color:#00ba8b;'>". date('d-m-y h:m A') ."</span>
								</h3>
								<br/>";

		$table_end .= "</table>";
		$message = $heading;
		$message .= $table_header;
		$message .= $table_content;
		$message .= $table_end;

      $cc = explode(',',$cc);

      $cc_string = '[';
    	for($count = 0; $count < count($cc); $count++)
    	{
    		$cc_string = $cc_string.'{"email":"'.$cc[$count].'"},';
    	}
    	$cc_string = trim($cc_string,',');
    	$cc_string = $cc_string.']';

      $bcc = explode(',',$bcc);
				$bcc_string = '[';
	    	for($count = 0; $count < count($bcc); $count++)
	    	{
	    		$bcc_string = $bcc_string.'{"email":"'.$bcc[$count].'"},';
	    	}
	    	$bcc_string = trim($bcc_string,',');
	    	$bcc_string = $bcc_string.']';

    	$request_body = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/','','{
    		"personalizations": [
    			{
    				"to": [
    					{
    					  "email": "'.$to.'"
    					}
    				],
    				"cc": '.$cc_string.',
    				"bcc": '.$bcc_string.',
    				"subject": "'.$subject.'"
    			}
    		],
    		"from": {
    			"email": "venkataramanaiah@knsgroup.in"
    		},
    		"content": [
    			{
    				"type": "text/html",
    				"value": "'.$message.'"
    			}
    		]
    	}'));

    	$apiKey = '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';
    	$sg = new \SendGrid($apiKey);


    	$response = $sg->client->mail()->send()->post($request_body);

} else {
    header("location:login.php");
}
