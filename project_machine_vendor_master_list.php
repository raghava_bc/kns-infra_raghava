<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 07-April-2017
// LAST UPDATED BY: Ashwini
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */
$_SESSION['module'] = 'PM Masters';

/* DEFINES - START */
define('PROJECT_MASTER_PROJECT_MACHINE_VENDOR_FUNC_ID','249');
/* DEFINES - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',PROJECT_MASTER_PROJECT_MACHINE_VENDOR_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',PROJECT_MASTER_PROJECT_MACHINE_VENDOR_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',PROJECT_MASTER_PROJECT_MACHINE_VENDOR_FUNC_ID,'4','1');
	$add_perms_list    = i_get_user_perms($user,'',PROJECT_MASTER_PROJECT_MACHINE_VENDOR_FUNC_ID,'1','1');

	//Get Project Machine Vendor master List
	$project_machine_vendor_master_search_data = array("active"=>'1');
	$project_machine_vendor_master_list = i_get_project_machine_vendor_master_list($project_machine_vendor_master_search_data);
	if($project_machine_vendor_master_list["status"] == SUCCESS)
	{
		$project_machine_vendor_master_list_data = $project_machine_vendor_master_list["data"];
	}
	else
	{
		$alert = $project_machine_vendor_master_list["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Machine Vendor Master List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Machine Vendor Master List</h3><?php if($add_perms_list['status'] == SUCCESS){ ?><span style="float:right; padding-right:20px;"><a href="project_add_machine_vendor_master.php">Add Machine Vendor</a></span><?php } ?>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered" style="table-layout: fixed;">
								<thead>
								  <tr>
									<th>Name</th>
									<th>Remarks</th>		
									<th>Added By</th>		
									<th>Added On</th>
                                    <th colspan="2" style="text-align:center;">Action</th>									
								</tr>
								</thead>
								<tbody>							
								<?php
								if($project_machine_vendor_master_list["status"] == SUCCESS)
								{									
									for($count = 0; $count < count($project_machine_vendor_master_list_data); $count++)
									{																	
									?>
									<tr>
									<td style="word-wrap:break-word;"><?php echo $project_machine_vendor_master_list_data[$count]["project_machine_vendor_master_name"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $project_machine_vendor_master_list_data[$count]["project_machine_vendor_master_remarks"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $project_machine_vendor_master_list_data[$count]["user_name"]; ?></td>	
									<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_machine_vendor_master_list_data[$count]
									["project_machine_vendor_master_added_on"])); ?></td>	
                                    <td><?php if($edit_perms_list['status'] == SUCCESS){ ?><a href="project_edit_machine_vendor_master.php?master_id=<?php echo $project_machine_vendor_master_list_data[$count]["project_machine_vendor_master_id"]; ?>">Edit</a><?php } ?></td>
                                    <td><?php if($delete_perms_list['status'] == SUCCESS){ ?><?php if(($project_machine_vendor_master_list_data[$count]["project_machine_vendor_master_active"] == "1")){?><a href="#" onclick="return delete_project_machine_vendor_master(<?php echo $project_machine_vendor_master_list_data[$count]["project_machine_vendor_master_id"]; ?>);">Delete</a><?php } ?><?php } ?></td>									
									</tr>
									<?php									
									}
								}
								else
								{
								?>
								<td colspan="5">No Project Machine Vendor Master data added yet!</td>
								<?php
								}	
								?>	
								</tbody>
							  </table>
		  <?php 
		    } 
			?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function delete_project_machine_vendor_master(master_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "project_machine_vendor_master_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_delete_machine_vendor_master.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("master_id=" + master_id + "&action=0");
		}
	}	
}

</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>

</html>