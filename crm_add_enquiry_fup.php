<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 7th Sep 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Query string data
	if(isset($_GET["enquiry"]))
	{
		$enquiry_id = $_GET["enquiry"];
	}
	else
	{
		$enquiry_id = "";
	}
	
	if(isset($_REQUEST["assigned_to"]))
	{
		$assigned_to = $_REQUEST["assigned_to"];
	}
	else
	{
		$assigned_to = "";
	}
	// Capture the form data
	if(isset($_POST["add_enquiry_fup_submit"]))
	{
		$enquiry_id      = $_POST["hd_enquiry"];
		$assigned_to     = $_POST["hd_assigned_to"];
		$remarks         = $_POST["txt_remarks"];
		$interest_status = $_POST["ddl_interest_status"];
		$follow_up_date  = $_POST["dt_follow_up"];
		
		// Check for mandatory fields
		if(($enquiry_id !="") && ($remarks !="") && ($interest_status !="") && ($follow_up_date !="") && ($user !=""))
		{
			if(strtotime($follow_up_date) < strtotime(date("Y-m-d H:i:s")))
			{
				$alert_type = 0;
				$alert      = "Follow Up date cannot be lesser than current date time";
			}
			else
			{
				$enquiry_fup_iresult = i_add_enquiry_fup($enquiry_id,$remarks,$interest_status,$follow_up_date,$user);
				
				if($enquiry_fup_iresult["status"] == SUCCESS)
				{
					$alert_type = 1;
					/*if(($role == 1) || ($role == 5))
					{
						header("location:crm_enquiry_list_unq.php");
					}*/
					//else					
					//{
						header("location:crm_enquiry_fup_latest.php?gassignee=$assigned_to");
					//}
				}
				else
				{
					$alert_type = 0;
				}
				
				$alert = $enquiry_fup_iresult["data"];
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Interest Status
	$interest_status_list = i_get_interest_status_list('','1');
	if($interest_status_list["status"] == SUCCESS)
	{
		$interest_status_list_data = $interest_status_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$interest_status_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Enquiry List
	$enquiry_list = i_get_enquiry_list($enquiry_id,'','','','','','','','','','','','','','','','','','','');
	if($enquiry_list["status"] == SUCCESS)
	{
		$enquiry_list_data = $enquiry_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$enquiry_list["data"];
	}
	
	// Enquiry Follow Up List
	$enquiry_fup_list = i_get_enquiry_fup_list($enquiry_id,'','','added_date_desc','','');
	if($enquiry_fup_list["status"] == SUCCESS)
	{
		$enquiry_fup_list_data = $enquiry_fup_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$enquiry_fup_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Enquiry Follow Up</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header" style="height:70px;">
	      				<i class="icon-user"></i>
	      				<h3>Enquiry ID: <?php echo $enquiry_list_data[0]["enquiry_number"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Name: <?php echo $enquiry_list_data[0]["name"]; ?>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mobile: <?php echo $enquiry_list_data[0]["cell"]; ?>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Source: <?php echo $enquiry_list_data[0]["enquiry_source_master_name"]; ?>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email: <?php echo $enquiry_list_data[0]["email"]; ?>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Project: <?php echo $enquiry_list_data[0]["project_name"];?></h3>
						<a href="crm_add_site_visit_plan.php?enquiry=<?php echo $enquiry_id; ?>"><br /><span style="padding-left:35px;"><strong>Add Site Visit Plan</strong></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="crm_assign_enquiry.php?enquiry=<?php echo $enquiry_id; ?>"><strong>Assign</strong></a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    Add Follow Up							
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_enquiry_fup" class="form-horizontal" method="post" action="crm_add_enquiry_fup.php">
								<input type="hidden" name="hd_enquiry" value="<?php echo $enquiry_id; ?>" />
								<input type="hidden" name="hd_assigned_to" value="<?php echo $assigned_to; ?>" />
									<fieldset>																				
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<textarea name="txt_remarks" class="span6"></textarea>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_interest_status">Interest Status*</label>
											<div class="controls">
												<select name="ddl_interest_status" required>
												<?php
												for($count = 0; $count < count($interest_status_list_data); $count++)
												{
												?>
												<option value="<?php echo $interest_status_list_data[$count]["crm_cust_interest_status_id"]; ?>"><?php echo $interest_status_list_data[$count]["crm_cust_interest_status_name"]; ?></option>								
												<?php
												}
												?>														
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="dt_follow_up">Follow Up Date*</label>
											<div class="controls">
												<input type="datetime-local" class="span6" name="dt_follow_up" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_enquiry_fup_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								<table class="table table-bordered" style="table-layout: fixed;">
								<thead>
								  <tr>
									<th>Date</th>					
									<th>Remarks</th>					
									<th>Added By</th>														
								</tr>
								</thead>
								<tbody>							
								<?php
								if($enquiry_fup_list["status"] == SUCCESS)
								{									
									for($count = 0; $count < count($enquiry_fup_list_data); $count++)
									{																	
									?>
									<tr>
									<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($enquiry_fup_list_data[$count]["enquiry_follow_up_added_on"])); ?></td>
									<td style="word-wrap:break-word;"><?php echo $enquiry_fup_list_data[$count]["enquiry_follow_up_remarks"]; ?></td>					
									<td style="word-wrap:break-word;"><?php echo $enquiry_fup_list_data[$count]["user_name"]; ?></td>					
									</tr>
									<?php									
									}
								}
								else
								{
								?>
								<td colspan="3">No follow ups yet!</td>
								<?php
								}	
								?>	
								</tbody>
							  </table>
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
