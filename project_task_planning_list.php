<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_process_master_list.php
CREATED ON	: 0*-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';

/* DEFINES - START */
define('PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID', '359');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '2', '1');
    $edit_perms_list   = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '3', '1');
    $delete_perms_list = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '4', '1');
    $add_perms_list    = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '1', '1');

    // Query String Data
    // Nothing

    // Temp data
    $alert_type = -1;
    $alert 	    = "";

    if (isset($_REQUEST['source'])) {
        $source = $_REQUEST['source'];
    } else {
        $source = "-1";
    }

    if (isset($_REQUEST['project_id'])) {
        $project_id = $_REQUEST['project_id'];
    } else {
        $project_id = "-1";
    }

    if (isset($_POST["file_search_submit"])) {
        $project_id   = $_POST["search_project"];
        $start_date	  	  = $_POST["start_date"];
        $end_date	      = $_POST["end_date"];
    } else {
        $project_id   = "-1";
        if ($source != "all") {
            $start_date = "";
            $end_date = "";
        } else {
            $start_date = "";
            $end_date = "";
        }
    }
    if (isset($_POST["search_process"]) && $_POST["search_process"] != '') {
        $search_process = $_POST["search_process"];
    } else {
        $search_process   = "-1";
    }
    $task_planned = 0;
    // Get Already added Object Output
    $project_task_planning_search_data = array("active"=>'1',"project_id"=>$project_id,"planned_start_date"=>$start_date,"planned_end_date"=>$end_date,"process_id"=>$search_process);

    $project_task_planning_list = i_get_project_task_planning($project_task_planning_search_data);
    if ($project_task_planning_list["status"] == SUCCESS) {
        $project_task_planning_list_data = $project_task_planning_list["data"];

        for ($task_count = 0 ; $task_count < count($project_task_planning_list_data) ; $task_count ++) {
            if ($project_task_planning_list_data[$task_count]["project_task_planning_measurment"] != 0) {
                $task_planned++;
            }
            // else {
            //     $task_planned = 1;
            // }
        }
    } else {
        $alert = $alert."Alert: ".$project_task_planning_list["data"];
    }
    // Get Already added process
    $project_process_master_search_data = array("active"=>'1');
    $project_process_master_list = i_get_project_process_master($project_process_master_search_data);
    if ($project_process_master_list["status"] == SUCCESS) {
        $project_process_master_list_data = $project_process_master_list["data"];
    } else {
        $alert = $project_process_master_list["data"];
        $alert_type = 0;
    }

    // Get Project Management Master modes already added
    $project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
    $project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
    if ($project_management_master_list['status'] == SUCCESS) {
        $project_management_master_list_data = $project_management_master_list['data'];
    } else {
        $alert = $project_management_master_list["data"];
        $alert_type = 0;
    }

    // Get Already added Object Output
    $project_task_planning_search_data = array("status"=>"min","min_date"=>"0000-00-00","project_id"=>$project_id);
    $project_task_planning_min_date_list = db_get_project_task_planning_date($project_task_planning_search_data);
    if ($project_task_planning_min_date_list["status"] == DB_RECORD_ALREADY_EXISTS) {
        $project_task_planning_min_date_list_data = $project_task_planning_min_date_list["data"];
        $planned_start_date= $project_task_planning_min_date_list_data[0]["min_date"];
    } else {
        $planned_start_date = "";
    }

    // Get Already added Object Output
    $project_task_planning_search_data = array("status"=>"max","max_date"=>"0000-00-00","project_id"=>$project_id);
    $project_task_planning_max_date_list =db_get_project_task_planning_date($project_task_planning_search_data);
    if ($project_task_planning_max_date_list["status"] == DB_RECORD_ALREADY_EXISTS) {
        $project_task_planning_max_date_list_data = $project_task_planning_max_date_list["data"];
        $planned_end_date= $project_task_planning_max_date_list_data[0]["max_date"];
    } else {
        $planned_end_date = "";
    }
    $total_days = get_date_diff($planned_start_date, $planned_end_date);
} else {
    header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Management - Project Task Planning List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">

	<style>
div.hover {

    padding: 20px;
    display: none;
}

span:hover + div {
    display: block;
}
</style>



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>


<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header" style="height:10%;"> <i class="icon-th-list"></i>
              <h3>Project Management - Project Task Planning List &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Start Date: <?php echo $planned_start_date;?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;End
			  Date:&nbsp;<?php echo $planned_end_date ;?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total No of Objects&nbsp;&nbsp;&nbsp;<span id="total_objects"></span>&nbsp;&nbsp;(MC:&nbsp; <span id="total_mc_object"></span>&nbsp; CW:&nbsp;&nbsp;<span id="total_cw_objects"></span> )&nbsp;&nbsp; Total days:&nbsp;&nbsp;	<?php echo $total_days["data"] ;?></h3><?php if ($add_perms_list['status'] == SUCCESS) {
    ?><?php
} ?>
            </div>
			<div class="widget-header" style="height:80px; padding-top:10px;">
			  <form method="post" id="file_search_form" action="project_task_planning_list.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
              for ($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++) {
                  ?>
			  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if ($project_id == $project_management_master_list_data[$project_count]["project_management_master_id"]) {
                      ?> selected="selected" <?php
                  } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
			  <?php
              }
              ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_process">
			  <option value="">- - Select Process - -</option>
			  <?php
              for ($process_count = 0; $process_count < count($project_process_master_list_data); $process_count++) {
                  ?>
			  <option value="<?php echo $project_process_master_list_data[$process_count]["project_process_master_id"]; ?>" <?php if ($search_process == $project_process_master_list_data[$process_count]["project_process_master_id"]) {
                      ?> selected="selected" <?php
                  } ?>><?php echo $project_process_master_list_data[$process_count]["project_process_master_name"]; ?></option>
			  <?php
              }
              ?>
			  </select>
			  </span>
			  <span>
			  <input type="date" name="start_date" value="<?php echo $start_date ;?>" >
			  </span>
			 <span>
			  <input type="date" name="end_date" value="<?php echo $end_date ;?>" >
			  </span>
			  <input type="submit" name="file_search_submit" />
			  </form>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php if ($view_perms_list['status'] == SUCCESS) {
                  ?>
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th style="width:1%">#</th>
				    <th style="width:4%">Project</th>
					<th style="width:5%">Process</th>
					<th style="width:5%">Task</th>
					<th style="width:3%">UOM</th>
					<th style="width:3%">Measurment</th>
					<th style="width:2%">Road Number</th>
					<th style="width:2%">Object Type</th>
					<th style="width:3%">Object Name</th>
					<th style="width:2%">No Of Objects</th>
					<th style="width:3%">Per Day output</th>
					<th style="width:3%">P Start Date</th>
					<th style="width:3%">P End Date</th>
					<th style="width:2%">Total Days</th>
					<th style="width:6%">Planned By</th>
					<th style="width:3%">Planned On</th>
					<th style="width:3%">Plan</th>
					<th style="width:3%">Actuals</th>
					<th style="width:3%">Action</th>

				</tr>
				</thead>
				<tbody>
				<?php
                $total_machine_objects = 0;
                  $total_cw_objects = 0;
                  if ($task_planned > 0) {
                      $sl_no = 0;

                      $total_days = 0;
                      $no_of_workers = 0;

                      for ($count = 0; $count < count($project_task_planning_list_data); $count++) {

                        // Get list of task plans for this process plan
                          $project_process_task_search_data = array("active"=>'1',"task_id"=>$project_task_planning_list_data[$count]["project_task_planning_task_id"],"location"=>$project_task_planning_list_data[$count]["project_task_planning_no_of_roads"]);
                          $project_process_task_list = i_get_project_process_task($project_process_task_search_data);
                          if ($project_process_task_list["status"] == SUCCESS) {
                              $project_process_task_list_data = $project_process_task_list["data"];
                              $pause_status = $project_process_task_list_data[0]["project_process_task_status"];
                              $road_id = $project_process_task_list_data[0]["project_process_task_location_id"];
                          } else {
                              $road_id = "-1";
                              $pause_status = "";
                          }

                          if ((float)($project_task_planning_list_data[$count]["project_task_planning_measurment"]) > 0) {
                              if ($project_task_planning_list_data[$count]["project_task_planning_no_of_roads"] == "No Roads") {
                                  $road_id = "'No Roads'";
                              } else {
                                  $road_id = $project_task_planning_list_data[$count]["project_task_planning_no_of_roads"];
                              }
                              $total_days = $total_days +  $project_task_planning_list_data[$count]["project_task_planning_total_days"] ;
                              $sl_no ++ ;
                              $object_type = $project_task_planning_list_data[$count]["project_task_planning_object_type"];
                              if (($project_task_planning_list_data[$count]["project_task_planning_object_type"] == "CW")) {
                                  $machine_name = $project_task_planning_list_data[$count]["project_cw_master_name"];
                                  // Get Project CW Master List
                                  $project_cw_master_search_data = array("active"=>'1',"cw_master_id"=>$project_task_planning_list_data[$count]["project_task_planning_machine_type"]);
                                  $project_cw_master_list = i_get_project_cw_master($project_cw_master_search_data);
                                  if ($project_cw_master_list['status'] == SUCCESS) {
                                      $project_cw_master_list_data = $project_cw_master_list["data"];
                                      $no_of_workers = $project_cw_master_list_data[0]["project_cw_master_set_no"];
                                  } else {
                                      $cw_id = "";
                                      $no_of_workers = "";
                                  }
                                  $total_cw_objects = $total_cw_objects +  ($project_task_planning_list_data[$count]["project_task_planning_no_of_object"] * $no_of_workers);
                              } elseif (($project_task_planning_list_data[$count]["project_task_planning_object_type"] == "MC")) {
                                  $machine_name = $project_task_planning_list_data[$count]["project_machine_type_master_name"];
                                  $total_machine_objects = $total_machine_objects +  $project_task_planning_list_data[$count]["project_task_planning_no_of_object"] ;
                              } else {
                                  $machine_name = "";
                              }
                              if (($project_task_planning_list_data[$count]["project_task_planning_no_of_roads"] != "No Roads")) {
                                  $road_name = $project_task_planning_list_data[$count]["project_site_location_mapping_master_name"];
                              } else {
                                  $road_name = "No Roads";
                              } ?>
					<!-- <input type="hidden" id="hd_task_id" name= "hd_task_id" value="<?php echo $project_task_planning_list_data[$count]["project_task_planning_task_id"]  ; ?>"> -->
					<input type="hidden" id="hd_project_id" name= "hd_project_id" value="<?php echo  $project_task_planning_list_data[$count]["project_management_master_id"]  ; ?>">
					<!-- <input type="hidden" id="hd_location_id" name= "hd_location_id" value="<?php echo $project_task_planning_list_data[$count]["project_task_planning_no_of_roads"]  ; ?>"> -->

          <tr>

					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_task_planning_list_data[$count]["project_master_name"]; ?></td>
					<!-- <input type="hidden" id="hd_task_id" name= "hd_task_id" value="<?php echo $project_task_planning_list_data[$count]["project_task_planning_task_id"]  ; ?>"> -->
					<td><?php echo $project_task_planning_list_data[$count]["project_process_master_name"]; ?></td>
					<td><?php echo $project_task_planning_list_data[$count]["project_task_master_name"]; ?></td>
					<td><?php echo $project_task_planning_list_data[$count]["project_uom_name"]; ?></td>
					<td><?php echo $project_task_planning_list_data[$count]["project_task_planning_measurment"]; ?></td>
					<td><?php echo $road_name; ?></td>
					<td><?php echo $project_task_planning_list_data[$count]["project_task_planning_object_type"] ; ?></td>
					<td><?php echo $machine_name ; ?></td>
					<td><?php echo $project_task_planning_list_data[$count]["project_task_planning_no_of_object"] ; ?></td>
					<td><?php echo $project_task_planning_list_data[$count]["project_task_planning_per_day_out"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo get_formatted_date($project_task_planning_list_data[$count][
                    "project_task_planning_start_date"], "d-M-Y"); ?></td>
					<td style="word-wrap:break-word;"><?php echo get_formatted_date($project_task_planning_list_data[$count][
                    "project_task_planning_end_date"], "d-M-Y"); ?></td>
					<td><?php echo $project_task_planning_list_data[$count]["project_task_planning_total_days"]; ?></td>

					<td><?php echo $project_task_planning_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y", strtotime($project_task_planning_list_data[$count][
                    "project_process_master_added_on"])); ?></td>

					<td><?php if ($add_perms_list['status'] == SUCCESS) {
                        ?><a href="#" data-toggle="modal" data-id="<?php echo $project_task_planning_list_data[$count]["project_process_task_id"]; ?>" data-pause-status="<?php echo $project_process_task_list_data[$count]["project_process_task_status"]; ?>" data-target="#myModal1" class="open_task" id="task_id_link_<?php echo $project_task_planning_list_data[$count]["project_process_task_id"]; ?>"><strong>Plan</strong></a><?php
                    } ?></td>

            <td>*<?php if (($add_perms_list['status'] == SUCCESS) && (strtotime($project_task_planning_list_data[$count][
                      "project_task_planning_end_date"]) > (strtotime(date("d-m-Y"))))) {
                        ?><a href="#" data-toggle="modal" onclick="setData(<?php echo($project_task_planning_list_data[$count]["project_process_task_id"].','.$road_id); ?>)"  data-pause-status="<?php echo $pause_status ; ?>" data-target="#myModal" class="open_task">Actuals </a>
                          <?php
                    } else {
                        echo "Update plan end date";
                    } ?></td>
                    <?php
                                if ($pause_status== "Pause") {
                                    $status = "Release";
                                } else {
                                    $status = "Pause";
                                } ?>

                    <td style="word-wrap:break-word;"><?php if (($add_perms_list['status'] == SUCCESS) && ($road_id != -1) && (strtotime($project_task_planning_list_data[$count][
                              "project_task_planning_end_date"]) > (strtotime(date("d-m-Y"))))) {
                                    ?><a style="padding-right:10px" onclick="return go_to_delay_reason_master('<?php echo $project_task_planning_list_data[$count]["project_task_planning_task_id"]; ?>','<?php echo $status ; ?>','<?php echo $project_task_planning_list_data[$count]["project_task_planning_no_of_roads"] ; ?>') ; "><?php echo $status ; ?></a><?php
                                } else {
                                    echo "*Update plan end date " ;
                                } ?></div></td>

					</tr>
					<?php
                          }
                      }
                  } else {
                      ?>
				<td colspan="8" style="margin:10  ">Plan all tasks' to add Actuals data!</td>

				<?php
                  }
                  $total_objects = $total_cw_objects + $total_machine_objects ; ?>

                </tbody>
              </table>

			   <script>
				  document.getElementById('total_mc_object').innerHTML = '<?php echo round($total_machine_objects); ?>';
				  document.getElementById('total_cw_objects').innerHTML = '<?php echo round($total_cw_objects); ?>';
				  document.getElementById('total_objects').innerHTML = '<?php echo round($total_objects); ?>';
		    </script>
			    <?php
              }
                ?>
				 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document"  style="padding:20px;">
                     <div class="modal-content">
					 <table class="table table-bordered" id="actuals_table_released">
					 <tr>

						<td><?php if ($add_perms_list['status'] == SUCCESS) {
                    ?><a style="padding-right:10px" href="#" onclick="return go_to_project_task_actual();">Add Actual Man Power</a><?php
                } ?></td>
						<td><?php if ($add_perms_list['status'] == SUCCESS) {
                    ?><a style="padding-right:10px" href="#" onclick="return go_to_project_task_actual_machine_planning();">Add Actual Machine</a><?php
                } ?></td>
						<td><?php if ($add_perms_list['status'] == SUCCESS) {
                    ?><a style="padding-right:10px" href="#" onclick="return go_to_project_task_boq_actuals();">Add Contract Work</a><?php
                } ?> </td>
						</tr>
						</table>
						<table class="table table-bordered" id="actuals_table_paused" style="display:none;">
						<tr><td>This task is paused. Please release before adding actuals</td></tr>
						</table>
						</div>
                           </form>
                     </div>
                  </div>

				  <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document"  style="padding:20px;">
                     <div class="modal-content">
					   <table class="table table-bordered" id="allow_plan_table">
						<tr>
						<td><?php if ($add_perms_list['status'] == SUCCESS) {
                    ?><a style="padding-right:10px" href="#" onclick="return go_to_add_method_planning();">Method Plan</a><?php
                } ?> </td>
                <?php echo $road_id ;?>
						<td><?php if ($add_perms_list['status'] == SUCCESS && ($road_id != "")) {
                    ?><a style="padding-right:10px" href="#" onclick="return go_to_add_material();">Material Plan</a><?php
                } ?> </td>
						</tr>
						</table>


						</div>
			  <br />
            </div>
            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>

$(document).ready(function(){
   $(".open_task").click(function(){
     $("#hd_task_id").val($(this).data('id'));
	 var estimate_id = ($(this).data('estimate'));
	 if($(this).data('pause-status'))
	 {
		 if($(this).data('pause-status') == 'Pause')
		 {
			 $("#actuals_table_paused").show();
			 $("#actuals_table_released").hide();
		 }
		 else
		 {
			 $("#actuals_table_paused").hide();
			 $("#actuals_table_released").show();
		 }
	 }
   });
});

function go_to_delay_reason_master(project_process_task_id,status,road_id)
{
  project_id = document.getElementById("hd_project_id").value;
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_update_delay_reason_master.php");
form.setAttribute("target","blank");
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);

	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","status");
	hiddenField3.setAttribute("value",status);

	var hiddenField4 = document.createElement("input");
	hiddenField4.setAttribute("type","hidden");
	hiddenField4.setAttribute("name","road_id");
	hiddenField4.setAttribute("value",road_id);

  var hiddenField5 = document.createElement("input");
	hiddenField5.setAttribute("type","hidden");
	hiddenField5.setAttribute("name","project_id");
	hiddenField5.setAttribute("value",project_id);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField3);
	form.appendChild(hiddenField4);
	form.appendChild(hiddenField5);

	document.body.appendChild(form);
    form.submit();
}


function setData(task_id, location_id){

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("id","project_process_task_id");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",task_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("id","location_id");
	hiddenField2.setAttribute("value",location_id);

	document.body.appendChild(hiddenField1);
	document.body.appendChild(hiddenField2);
}

function go_to_project_task_actual()
{

	project_id = document.getElementById("hd_project_id").value;
	project_process_task_id = document.getElementById("project_process_task_id").value;
	location_id = document.getElementById("location_id").value;

	var form = document.createElement("form");
  form.setAttribute("method", "GET");
	form.setAttribute("action", "project_task_actual_manpower.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","project_id");
	hiddenField2.setAttribute("value",project_id);

	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","location_id");
	hiddenField3.setAttribute("value",location_id);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	form.appendChild(hiddenField3);

	document.body.appendChild(form);
    form.submit();
}
function go_to_project_task_actual_machine_planning()
{

  project_id = document.getElementById("hd_project_id").value;
	project_process_task_id = document.getElementById("project_process_task_id").value;
	location_id = document.getElementById("location_id").value;

	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_task_actual_machine_plan.php");

	//project_process_task_id=document.getElementById("hd_task_id").value;

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","project_id");
	hiddenField2.setAttribute("value",project_id);

	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","location_id");
	hiddenField3.setAttribute("value",location_id);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	form.appendChild(hiddenField3);

	document.body.appendChild(form);
    form.submit();
}

function go_to_project_machine_planning_list()
{
  project_id = document.getElementById("hd_project_id").value;
	project_process_task_id = document.getElementById("project_process_task_id").value;
	location_id = document.getElementById("location_id").value;

	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_add_machine_planning.php");

project_process_task_id=document.getElementById("hd_task_id").value;

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","project_id");
	hiddenField2.setAttribute("value",project_id);

	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","location_id");
	hiddenField3.setAttribute("value",location_id);


	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	form.appendChild(hiddenField3);

	document.body.appendChild(form);
    form.submit();
}

function go_to_project_task_boq_actuals()
{

	project_process_task_id = document.getElementById("project_process_task_id").value;
	project_id = document.getElementById("hd_project_id").value;
	location_id = document.getElementById("location_id").value;

	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_add_task_boq_actuals.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_process_task_id");
	hiddenField1.setAttribute("value",project_process_task_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","project_id");
	hiddenField2.setAttribute("value",project_id);

	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","location_id");
	hiddenField3.setAttribute("value",location_id);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	form.appendChild(hiddenField3);

	document.body.appendChild(form);
    form.submit();
}

function go_to_add_method_planning()
{
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_upload_documents.php");

	task_id=document.getElementById("hd_task_id").value;
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","task_id");
	hiddenField1.setAttribute("value",task_id);

	form.appendChild(hiddenField1);
	document.body.appendChild(form);
    form.submit();
}

function go_to_add_material()
{
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_add_task_required_item.php");

	task_id=document.getElementById("hd_task_id").value;

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","task_id");
	hiddenField1.setAttribute("value",task_id);

	form.appendChild(hiddenField1);
	document.body.appendChild(form);
    form.submit();
}
</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>
<script src="js/handson.js"></script>
  </body>

</html>
