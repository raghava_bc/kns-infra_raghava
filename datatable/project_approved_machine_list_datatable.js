var table;

function toggleSelection(ele) {
  if (!enableGenerateBilling()) {
    $(ele).prop("checked", false);
  }
  $("input.input-checkbox").prop("checked", ele.checked);
}

function get_machine_list() {
  var searchstring = document.getElementById('stxt_machine').value;
  if (searchstring.length >= 3) {
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        if (xmlhttp.responseText != 'FAILURE') {
          document.getElementById('search_results').style.display = 'block';
          document.getElementById('search_results').innerHTML = xmlhttp.responseText;
        }
      }
    }
    xmlhttp.open("POST", "ajax/project_get_machine.php"); // file name where delete code is written
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("search=" + searchstring);
  } else {
    document.getElementById('search_results').style.display = 'none';
  }
}

function select_machine(machine_id, search_machine, number) {
  document.getElementById('hd_machine_id').value = machine_id;
  document.getElementById('stxt_machine').value = number;
  //var name_code = search_machine.concat('-',number);
  //document.getElementById('stxt_machine').value = name_code;
  document.getElementById('search_results').style.display = 'none';
}

var tmpMachineData = [];

function generateBill() {

  var machine = [];
  var tableData = table.data();

  // get all the 'checked' input:checkbox(s)
  $("table.DTFC_Cloned input.input-checkbox:checked").each(function(i, ele) {
    machine.push(tableData[parseInt(ele.value)]);
  });

  if ($('input#selectAllCheckbox').is(':checked')) {
    machine = tableData;
  } else if (!machine.length) {
    machine = [];
  }

  if (!machine.length) {
    alert('Error: Select atleast one row to Bill');
    return false;
  }

  $.each(machine, function(i, value) {
    tmpMachineData.push(value);
  });
  $('#exampleModal').modal('show');

  // flush the previously loaded modal content
  $('#exampleModal').on('hidden.bs.modal', function(e) {
    $('form#add-bill-form')[0].reset();
  });
}

function generateMachineId() {
  if (($('#billing_address option:selected').val().trim() == '') ||
    ($('#remarks').val().trim() == '')) {
    alert('Please input required field');
    return false;
  }

  var bill_substr = $("#billing_address option:selected").text().trim().replace(/\./g, '').replace('KNS', '').trim().substr(0, 5);
  bill_substr = (bill_substr === "KN SU") ? "SUREN" : bill_substr;

  $.ajax({
    url: 'ajax/project_select_machine_payment.php',
    type: 'POST',
    data: {
      val: tmpMachineData,
      bill_substr: bill_substr,
      vendor_id: $('#search_vendor').val(),
    },
    dataType: 'json',
    success: function(response) {
      tmpMachineData = [];
      submitBilling(response.id)
    }
  });
}

function submitBilling(machine_id) {
  $.ajax({
    url: 'project_add_machine_bill_no.php',
    type: 'POST',
    data: {
      add_bill_no_submit: 'yes',
      machine_id: machine_id,
      billing_address: $('#billing_address').val(),
      remarks: $('#remarks').val()
    },
    success: function(bill_number) {
      $('#exampleModal').modal('hide');
      tableDraw();
      setTimeout(function() {
        alert('Bill generated: ' + bill_number);
      }, 750);
    }
  });
}

function removeModal() {
  $(this).find('.modal-content').empty();
  $(this).data('bs.modal', null);
}

function hasValue(name) {
  var value = $(name).val();
  return (value !== '' && value.length);
}

function enableGenerateBilling() {
  return hasValue('#search_project') &&
    hasValue('#search_vendor') &&
    hasValue('#start_date') &&
    hasValue('#end_date') &&
    hasValue('#stxt_machine') &&
    table.data().length;
}

function tableDraw() {
  table.draw();
}
$(document).ready(function() {
  var columnsMapping = {
    '13': 'total_amount',
  };

  table = $('#example').DataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_approved_machine_lists.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    drawCallback: function() {
      machine = [];
      $("input#selectAllCheckbox").prop("checked", false);
      $("input.input-checkbox").prop("checked", false);
    },
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "project_machine_list";
      aoData.search_project = $('#search_project').val();
      aoData.search_vendor = $('#search_vendor').val();
      aoData.search_machine = $('#stxt_machine').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    buttons: [{
      extend: 'colvis',
      columns: ':not(.noVis)'
    }],
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 3
    },
    scrollX: true,
    "columns": [{
        className: 'noVis',
        "orderable": false,
        "data": function() {
          return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
        }
      },
      {
        "orderable": false,
        "data": "project_machine_vendor_master_name"
      },
      {
        "orderable": false,
        "data": "project_master_name"
      },
      {
        "orderable": false,
        "data": "project_process_master_name"
      },
      {
        "orderable": false,
        "data": "project_task_master_name"
      },
      {
        "orderable": false,
        "data": "project_uom_name"
      },
      {
        "orderable": false,
        "data": "project_task_actual_machine_work_type"
      },
      {
        "orderable": false,
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          if (data.project_task_actual_machine_plan_road_id == "No Roads" ||
            data.project_task_actual_machine_plan_road_id == "") {
            return 'No Roads';
          }
          return data.project_site_location_mapping_master_name;
        }
      },
      {
        "orderable": false,
        "data": "project_machine_type"
      },
      {
        "orderable": false,
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return data.project_machine_master_name + " " + data.project_machine_master_id_number;
        }
      },
      {
        "orderable": false,
        "data": function(data, type, full) {
          var end_date;
          if (type !== 'display') {
            return '';
          }
          if (data.project_task_actual_machine_plan_end_date_time == "0000-00-00 00:00:00") {
            end_date = moment().format('x');
          } else {
            end_date = moment(data.project_task_actual_machine_plan_end_date_time).format('x');;
          }
          var diff_time = end_date - moment(data.project_task_actual_machine_plan_start_date_time).format('x');
          return (diff_time / (3600 * 1000)).toFixed(2);
        }
      },
      {
        "orderable": false,
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return Math.round(data.project_task_actual_machine_plan_off_time / 60);
        }
      },
      {
        "orderable": false,
        "data": "project_task_actual_machine_fuel_charges"
      },
      {
        "orderable": false,
        "data": "project_task_actual_machine_msmrt"
      }, {
        "orderable": false,
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          if (data.project_task_actual_machine_work_type === 'Regular') {
            return data.project_task_actual_machine_completion;
          } else {
            return data.project_task_actual_machine_rework_completion;
          }
        }
      }, {
        "orderable": false,
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return Math.round(data.project_task_actual_machine_plan_additional_cost);
        }
      }, {
        "orderable": false,
        "data": "project_task_actual_machine_bata"
      }, {
        "orderable": false,
        "data": "project_task_actual_machine_issued_fuel"
      }, {
        "orderable": false,
        "data": "project_task_actual_machine_plan_remarks"
      },
      {
        "orderable": false,
        data: function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return moment(data.project_task_actual_machine_plan_approved_on).
          format('DD-MM-YYYY hh:mm:ss');
        }
      },
      {
        "orderable": false,
        "data": "project_task_actual_machine_plan_approved_by"
      },
      {
        "orderable": true,
        "orderData": [21, 22],
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return moment(data.project_task_actual_machine_plan_start_date_time).
          format('DD-MM-YYYY hh:mm:ss');
        }
      },
      {
        "orderable": true,
        "orderData": [22, 21],
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          if (data.project_task_actual_machine_plan_end_date_time != "0000-00-00 00:00:00") {
            return moment(data.project_task_actual_machine_plan_end_date_time)
              .format('DD-MM-YYYY hh:mm:ss');
          } else {
            return '';
          }
        }
      },
      {
        "orderable": false,
        data: function(data, type, full, meta) {
          var end_date;
          if (data.project_task_actual_machine_plan_end_date_time == "0000-00-00 00:00:00") {
            end_date = moment().format('x');
          } else {
            end_date = moment(data.project_task_actual_machine_plan_end_date_time).format('x');;
          }
          var diff_time = Math.round(end_date - moment(data.project_task_actual_machine_plan_start_date_time).format('x'));
          var eff_hrs = parseFloat((diff_time / (3600 * 1000))) - (parseFloat(data.project_task_actual_machine_plan_off_time / 60));
          var total = (parseFloat(data.project_task_actual_machine_fuel_charges * eff_hrs) + parseFloat(data.project_task_actual_machine_bata) +
            parseFloat(data.project_task_actual_machine_plan_additional_cost) - parseFloat(data.project_task_actual_machine_issued_fuel)).toFixed(2);

          // set the 'total_amount' to each row in the table data
          // and that can be used it later operations
          meta.settings.aoData[meta.row]._aData.total_amount = total;
          return total;
        }
      },
      {
        "orderable": false,
        "className": 'noVis',
        "data": function() {
          if ((arguments[1] !== 'display') || (!enableGenerateBilling())) {
            return '';
          }
          return '<input type="checkbox" class="input-checkbox" value="' + arguments[3].row + '" >';
        }
      }
    ],
  });

});