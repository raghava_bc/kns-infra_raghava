<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 08-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* DEFINES - START */
define('PROJECT_TASK_PLANNING_LIST_ID','359');
/* DEFINES - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];



include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	$_SESSION["task_planning_id"] = '';

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */

	// Get permission settings for this user for this page
	$add_perms_list     = i_get_user_perms($user,'',PROJECT_TASK_PLANNING_LIST_ID,'1','1');
	$view_perms_list    = i_get_user_perms($user,'',PROJECT_TASK_PLANNING_LIST_ID,'2','1');
	$edit_perms_list    = i_get_user_perms($user,'',PROJECT_TASK_PLANNING_LIST_ID,'3','1');
	$delete_perms_list  = i_get_user_perms($user,'',PROJECT_TASK_PLANNING_LIST_ID,'4','1');
	$approve_perms_list = i_get_user_perms($user,'',PROJECT_TASK_PLANNING_LIST_ID,'6','1');

	if(isset($_GET['process_id']))
	{
		$process_id = $_GET['process_id'];
	}
	else
	{
		$process_id = "";
	}
	if(isset($_GET['project_id']))
	{
		$project_id = $_GET['project_id'];
	}
	else
	{
		$project_id = "";
	}
	if(isset($_GET['process_master_id']))
	{
		$process_master_id = $_GET['process_master_id'];
	}
	else
	{
		$process_master_id = "";
	}

	$project_task_planning_search_data = array("active"=>'1',"project_id"=>$project_id);

$project_task_planning_list = i_get_project_task_planning($project_task_planning_search_data);



if ($project_task_planning_list["status"] == SUCCESS) {

	$total_records = count($project_task_planning_list["data"]);

} else {

	$total_records = '0';

}

	// Project data
	$project_management_master_search_data = array("active"=>'1',"project_id"=>$project_id, "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list["status"] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list["data"];
		$project_name = $project_management_master_list_data[0]["project_master_name"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_management_master_list["data"];
		$project_name = "";
	}

	// Project data
	$project_plan_wish_search_data = array("active"=>'1',"project_id"=>$project_id);
	$project_plan_wish_list = i_get_project_plan_wish($project_plan_wish_search_data);
	if($project_plan_wish_list["status"] == SUCCESS)
	{
		$project_plan_wish_list_data = $project_plan_wish_list["data"];
		$wish_days = $project_plan_wish_list_data[0]["project_plan_wish_days"];
		$start_date = $project_plan_wish_list_data[0]["project_plan_wish_start_date"];
		$status = $project_plan_wish_list_data[0]["project_plan_wish_lock"];
	}
	else
	{
		$wish_days = "";
		$start_date = "";
		$status = "Plan";
	}


}
else
{
	header("location:login.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="js/handsontable-master/dist/handsontable.full.css" media="screen" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
		<div id="ajax_loading"></div>
    <div class="container">
      <div class="row">

	  <div class="widget-header" style="width:100%"> <i class="icon-th-list"></i>
              <!-- <h3>Project Task Planning&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Project :<?php echo $project_name ;?>;&nbsp;&nbsp;&nbsp;&nbsp Wish to start :<?php echo $start_date ;?>&nbsp;&nbsp;&nbsp;&nbsp Wish to complete :<?php echo $wish_days ;?>&nbsp;&nbsp;&nbsp;&nbsp Start Date: <span id="p_start_date"></span>&nbsp;&nbsp;&nbsp;&nbsp End Date: <span id="p_end_date"> </span> &nbsp;&nbsp;&nbsp;&nbsp Duration: <span id="duration_value"> </span></h3> -->
							<h3>

								<span class="header-label">Project Task Planning</span>

								<span class="header-label">Total Records:</span> <?= $total_records; ?>

							</h3>

							<br/>

							<h3 style="margin-left:25px">

								<span class="header-label">Project:</span>  <?= $project_name; ?>

								<span class="header-label">Wish to start:</span> <?= $start_date; ?>

								<span class="header-label">Wish to complete</span> <?= $wish_days; ?>

							</h3>

							<h3>

								<span class="header-label" >Start Date: </span> <span id="p_start_date"></span>

								<span class="header-label" >End Date: </span> <span id="p_end_date"></span>

								<span class="header-label" >Duration: </span> <span id="duration_value"></span>

							</h3>
				<div class="pull-right" style="padding-right:10px;"><?php if($status == "Update") { ?><a href="#" onclick="return project_lock('<?php echo $project_id; ?>');"><button disabled>Lock Task Plan </button></a><?php } ?><?php if($status == "Plan") { ?><a href="#" onclick="return project_update('<?php echo $project_id; ?>');"><button disabled>Update</button></a><?php } ?></div>
            </div>
          <div class="span6" style="width:95%;">
						<div class="search-filter" style="margin-top:30px;">
							<input type="search" id="search_field" placeholder="Search...">

							<div id="search_results" style="display:inline-block; margin-left:10px" class="header-label"></div>
						</div>
            <!-- /widget-header -->
			<div id="spreadsheet" style="height:60vh;margin-top:30px;overflow:hidden;margin:bottom:20px"></div>

          <!-- /widget -->

        <!-- /span6 -->
      </div>
	  <div class="form-actions">
			<input type="hidden" id="hd_status" name= "hd_status" value="<?php echo $status ;?>">
			<input type="hidden" id="hd_start_date" value="">
			<input type="hidden" id="hd_end_date" value="">
				<input type="submit" class="btn btn-primary" name="add_task_plan_submit" id="add_task_plan_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>

</div>





<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->

<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/handsontable-master/dist/handsontable.full.js"></script>
<script src="js/task_plan.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script src="js/date.js"></script>
<script>
function project_lock(project_id)
{
	var ok = confirm("Are you sure you want to Lock this Plan?")

	{

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{
					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else

					{

						var url = "project_task_planning.php?project_id=" + project_id;

						window.location = url;

					}

				}

			}



			xmlhttp.open("POST", "project_lock_project_plan.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("project_id=" + project_id + "&action=Locked");

		}

	}
}
function project_update(project_id)
{
	var ok = confirm("Are you sure you want to Update this Plan?")

	{

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{
					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else

					{

						var url = "project_task_planning.php?project_id=" + project_id;

						window.location = url;

					}

				}

			}



			xmlhttp.open("POST", "project_update_task_plan.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("project_id=" + project_id + "&action=Update");

		}

	}
}
</script>

  </body>

</html>
