-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 22, 2018 at 01:38 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kns_erp_live`
--

-- --------------------------------------------------------

--
-- Structure for view `project_process_task_list`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `project_process_task_list` AS select `PPPT`.`project_process_task_id` AS `project_process_task_id`,`PPPT`.`project_process_id` AS `project_process_id`,`PPPT`.`project_process_task_type` AS `project_process_task_type`,`PPP`.`project_plan_process_id` AS `project_plan_process_id`,`PPP`.`project_plan_process_name` AS `project_plan_process_name`,`PTM`.`project_task_master_id` AS `project_task_master_id`,`PTM`.`project_task_master_name` AS `project_task_master_name`,`PTM`.`project_task_master_process` AS `project_task_master_process`,`PTM`.`project_task_master_order` AS `project_task_master_order`,`PMPM`.`project_management_master_id` AS `project_management_master_id`,`PMPM`.`project_master_name` AS `project_master_name` from ((((((`project_plan_process_task` `PPPT` join `project_plan_process` `PPP` on((`PPP`.`project_plan_process_id` = `PPPT`.`project_process_id`))) join `project_task_master` `PTM` on((`PTM`.`project_task_master_id` = `PPPT`.`project_process_task_type`))) join `users` `U` on((`U`.`user_id` = `PPP`.`project_plan_process_assigned_user`))) join `project_process_master` `PPM` on((`PPM`.`project_process_master_id` = `PPP`.`project_plan_process_name`))) join `project_plan` `PP` on((`PP`.`project_plan_id` = `PPP`.`project_plan_process_plan_id`))) join `project_management_project_master` `PMPM` on((`PMPM`.`project_management_master_id` = `PP`.`project_plan_project_id`)));

--
-- VIEW  `project_process_task_list`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
