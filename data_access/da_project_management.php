<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');
/*
LIST OF TBDs
1. Wherever there is name search, there should be a wildcard
*/
/*
PURPOSE : To add new Project Plan Process Task
INPUT 	: Process ID, Task Type, Actual Start Date, Actual End Date, Remarks, Added By
OUTPUT 	: Task ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_process_task($process_id,$task_type,$location,$task_doc,$actual_start_date,$actual_end_date,$remarks,$added_by)
{
	// Query
   $project_process_task_iquery = "insert into project_plan_process_task
   (project_process_id,project_process_task_type,project_process_task_location_id,project_process_task_doc,project_process_actual_start_date,project_process_actual_end_date,
	project_process_task_active,project_process_task_remarks,project_process_task_added_by,project_process_task_added_on)values(:process_id,:task_type,
	:location,:task_doc,:actual_start_date,:actual_end_date,:active,:remarks,:added_by,:added_on)";
    try
    {
        $dbConnection = get_conn_handle();
        $project_process_task_istatement = $dbConnection->prepare($project_process_task_iquery);

        // Data
        $project_process_task_idata = array(':process_id'=>$process_id,':task_doc'=>$task_doc,':task_type'=>$task_type,':location'=>$location,':actual_start_date'=>$actual_start_date,':actual_end_date'=>$actual_end_date,
		':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_process_task_istatement->execute($project_process_task_idata);
		$project_process_task_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_process_task_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project Plan Process Task list
INPUT 	: Task ID, Process ID, Task Type, Actual Start Date, Actual End Date, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of project Process Task
BY 		: Lakshmi
*/
function db_get_project_process_task($project_process_task_search_data)
{
	// var_dump($project_process_task_search_data);
	if(array_key_exists("task_id",$project_process_task_search_data))
	{
		$task_id = $project_process_task_search_data["task_id"];
	}
	else
	{
		$task_id= "";
	}

	if(array_key_exists("process_id",$project_process_task_search_data))
	{
		$process_id = $project_process_task_search_data["process_id"];
	}
	else
	{
		$process_id = "";
	}

	if(array_key_exists("process_master_id",$project_process_task_search_data))
	{
		$process_master_id = $project_process_task_search_data["process_master_id"];
	}
	else
	{
		$process_master_id = "";
	}

	if(array_key_exists("project_id",$project_process_task_search_data))
	{
		$project_id = $project_process_task_search_data["project_id"];
	}
	else
	{
		$project_id = "";
	}
	if(array_key_exists("assigned_to",$project_process_task_search_data))
	{
		$assigned_to = $project_process_task_search_data["assigned_to"];
	}
	else
	{
		$assigned_to = "";
	}

	if(array_key_exists("plan_id",$project_process_task_search_data))
	{
		$plan_id = $project_process_task_search_data["plan_id"];
	}
	else
	{
		$plan_id = "";
	}


	if(array_key_exists("task_type",$project_process_task_search_data))
	{
		$task_type = $project_process_task_search_data["task_type"];
	}
	else
	{
		$task_type = "";
	}

	if(array_key_exists("location",$project_process_task_search_data))
	{
		$location = $project_process_task_search_data["location"];
	}
	else
	{
		$location = "";
	}

	if(array_key_exists("status",$project_process_task_search_data))
	{
		$status = $project_process_task_search_data["status"];
	}
	else
	{
		$status = "";
	}

	if(array_key_exists("actual_start_date",$project_process_task_search_data))
	{
		$actual_start_date = $project_process_task_search_data["actual_start_date"];
	}
	else
	{
		$actual_start_date = "";
	}

	if(array_key_exists("actual_end_date",$project_process_task_search_data))
	{
		$actual_end_date = $project_process_task_search_data["actual_end_date"];
	}
	else
	{
		$actual_end_date = "";
	}

	if(array_key_exists("active",$project_process_task_search_data))
	{
		$active = $project_process_task_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_process_task_search_data))
	{
		$added_by = $project_process_task_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_process_task_search_data))
	{
		$start_date= $project_process_task_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_process_task_search_data))
	{
		$end_date= $project_process_task_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	if(array_key_exists("order",$project_process_task_search_data))
	{
		$order = $project_process_task_search_data["order"];
	}
	else
	{
		$order = "";
	}

	if(array_key_exists("ignore_not_started",$project_process_task_search_data))
	{
		$ignore_not_started = $project_process_task_search_data["ignore_not_started"];
	}
	else
	{
		$ignore_not_started = "";
	}

	$get_project_process_task_list_squery_base = " select * from project_plan_process_task PPPT inner join project_plan_process PPP on PPP.project_plan_process_id=PPPT.project_process_id inner join project_task_master PTM on PTM.project_task_master_id=PPPT.project_process_task_type inner join users U on U.user_id=PPP.project_plan_process_assigned_user inner join project_process_master PPM on PPM.project_process_master_id=PPP.project_plan_process_name inner join project_plan PP on PP.project_plan_id=PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id = PP.project_plan_project_id";

	/*$get_project_process_task_list_squery_base = " select * from project_plan_process_task PPPT inner join project_plan_process PPP on PPP.project_plan_process_id=PPPT.project_process_id inner join project_task_master PTM on PTM.project_task_master_id=PPPT.project_process_task_type inner join users U on U.user_id=PPP.project_plan_process_assigned_user inner join project_process_master PPM on PPM.project_process_master_id=PPP.project_plan_process_name inner join project_plan PP on PP.project_plan_id=PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id = PP.project_plan_project_id left outer join project_site_location_mapping_master PSLMM on PSLMM.project_site_location_mapping_master_id=PPPT.project_process_task_location_id";*/

	$get_project_process_task_list_squery_where = "";

	$get_project_plan_process_list_squery_order = "";

	$filter_count = 0;

	// Data
	$get_project_process_task_list_sdata = array();

	if($task_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." where project_process_task_id = :task_id";
		}
		else
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." and project_process_task_id = :task_id";
		}

		// Data
		$get_project_process_task_list_sdata[':task_id'] = $task_id;

		$filter_count++;
	}

	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." where PPPT.project_process_id = :process_id";
		}
		else
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." and PPPT.project_process_id = :process_id";
		}

		// Data
		$get_project_process_task_list_sdata[':process_id'] = $process_id;

		$filter_count++;
	}

	if($process_master_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." where PPM.project_process_master_id = :process_master_id";
		}
		else
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." and PPM.project_process_master_id = :process_master_id";
		}

		// Data
		$get_project_process_task_list_sdata[':process_master_id'] = $process_master_id;

		$filter_count++;
	}

	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." where PP.project_plan_project_id = :project_id";
		}
		else
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." and PP.project_plan_project_id = :project_id";
		}

		// Data
		$get_project_process_task_list_sdata[':project_id'] = $project_id;

		$filter_count++;
	}
	if($assigned_to != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." where PPP.project_plan_process_assigned_user = :assigned_to";
		}
		else
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." and PPP.project_plan_process_assigned_user = :assigned_to";
		}

		// Data
		$get_project_process_task_list_sdata[':assigned_to'] = $assigned_to;

		$filter_count++;
	}
	if($plan_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." where PPP.project_plan_process_plan_id = :plan_id";
		}
		else
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." and PPP.project_plan_process_plan_id = :plan_id";
		}

		// Data
		$get_project_process_task_list_sdata[':plan_id'] = $plan_id;

		$filter_count++;
	}

	if($task_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." where project_process_task_type = :task_type";
		}
		else
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." and project_process_task_type = :task_type";
		}

		// Data
		$get_project_process_task_list_sdata[':task_type'] = $task_type;

		$filter_count++;
	}

	if($location != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." where project_process_task_location_id = :location";
		}
		else
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." and project_process_task_location_id = :location";
		}

		// Data
		$get_project_process_task_list_sdata[':location'] = $location;

		$filter_count++;
	}

	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." where project_process_task_status = :status";
		}
		else
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." and project_process_task_status = :status";
		}

		// Data
		$get_project_process_task_list_sdata[':status'] = $status;

		$filter_count++;
	}

	if($actual_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." where project_process_actual_start_date = :actual_start_date";
		}
		else
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." and project_process_actual_start_date = :actual_start_date";
		}

		// Data
		$get_project_process_task_list_sdata[':actual_start_date'] = $actual_start_date;

		$filter_count++;
	}

	if($actual_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." where project_process_actual_end_date = :actual_end_date";
		}
		else
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." and project_process_actual_end_date = :actual_end_date";
		}

		// Data
		$get_project_process_task_list_sdata[':actual_end_date'] = $actual_end_date;

		$filter_count++;
	}

		if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." where project_process_task_active = :active";
		}
		else
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." and project_process_task_active = :active";
		}

		// Data
		$get_project_process_task_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($ignore_not_started != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." where project_process_actual_start_date != :actual_nstart_date";
		}
		else
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." and project_process_actual_start_date != :actual_nstart_date";
		}

		// Data
		$get_project_process_task_list_sdata[':actual_nstart_date'] = '0000-00-00';

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." where project_process_task_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." and project_process_task_added_by = :added_by";
		}

		//Data
		$get_project_process_task_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." where project_process_task_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." and project_process_task_added_on >= :start_date";
		}

		//Data
		$get_project_process_task_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." where project_process_task_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." and project_process_task_added_on <= :end_date";
		}

		//Data
		$get_project_process_task_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	if($order != "")
	{
		switch($order)
		{
			case 'start_date_asc':
			$get_project_plan_process_list_squery_order = ' order by project_process_actual_start_date asc ,project_process_master_order ASC';
			if($filter_count == 0)
			{
				// Query
				$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." where project_process_actual_start_date != '0000-00-00'";
			}
			else
			{
				// Query
				$get_project_process_task_list_squery_where = $get_project_process_task_list_squery_where." and project_process_actual_start_date != '0000-00-00'";
			}

			$filter_count++;
			break;

			case 'end_date_desc':
			$get_project_plan_process_list_squery_order = ' order by project_process_actual_end_date desc ,project_process_master_order ASC';
			break;
		}
	}
	else
	{
		$get_project_plan_process_list_squery_order = ' order by project_process_master_order ASC,project_task_master_order ASC';
	}


	$get_project_process_task_list_squery = $get_project_process_task_list_squery_base.$get_project_process_task_list_squery_where.$get_project_plan_process_list_squery_order;

	try
	{
		$dbConnection = get_conn_handle();

		$get_project_process_task_list_sstatement = $dbConnection->prepare($get_project_process_task_list_squery);

		$get_project_process_task_list_sstatement -> execute($get_project_process_task_list_sdata);

		$get_project_process_task_list_sdetails = $get_project_process_task_list_sstatement -> fetchAll();

		if(FALSE === $get_project_process_task_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_process_task_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_process_task_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

 /*
PURPOSE : To update Project Plan Process Task
INPUT 	: Task ID, Project Process Task Update Array
OUTPUT 	: Task ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_process_task($task_id,$project_process_task_update_data)
{
	if(array_key_exists("process_id",$project_process_task_update_data))
	{
		$process_id = $project_process_task_update_data["process_id"];
	}
	else
	{
		$process_id = "";
	}

  if(array_key_exists("road_id",$project_process_task_update_data))
	{
		$road_id = $project_process_task_update_data["road_id"];
	}
	else
	{
		$road_id = "";
	}

	if(array_key_exists("task_type",$project_process_task_update_data))
	{
		$task_type = $project_process_task_update_data["task_type"];
	}
	else
	{
		$task_type = "";
	}

	if(array_key_exists("task_doc",$project_process_task_update_data))
	{
		$task_doc = $project_process_task_update_data["task_doc"];
	}
	else
	{
		$task_doc = "";
	}


	if(array_key_exists("actual_start_date",$project_process_task_update_data))
	{
		$actual_start_date = $project_process_task_update_data["actual_start_date"];
	}
	else
	{
		$actual_start_date = "";
	}

	if(array_key_exists("actual_end_date",$project_process_task_update_data))
	{
		$actual_end_date = $project_process_task_update_data["actual_end_date"];
	}
	else
	{
		$actual_end_date = "";
	}

	if(array_key_exists("completion",$project_process_task_update_data))
	{
		$completion = $project_process_task_update_data["completion"];
	}
	else
	{
		$completion = "";
	}

	if(array_key_exists("contract_completion",$project_process_task_update_data))
	{
		$contract_completion = $project_process_task_update_data["contract_completion"];
	}
	else
	{
		$contract_completion = "";
	}

	if(array_key_exists("machine_completion",$project_process_task_update_data))
	{
		$machine_completion = $project_process_task_update_data["machine_completion"];
	}
	else
	{
		$machine_completion = "";
	}



	if(array_key_exists("status",$project_process_task_update_data))
	{
		$status = $project_process_task_update_data["status"];
	}
	else
	{
		$status = "";
	}

	if(array_key_exists("active",$project_process_task_update_data))
	{
		$active = $project_process_task_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_process_task_update_data))
	{
		$remarks = $project_process_task_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_process_task_update_data))
	{
		$added_by = $project_process_task_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_process_task_update_data))
	{
		$added_on = $project_process_task_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_process_task_update_uquery_base = "update project_plan_process_task set";

	$project_process_task_update_uquery_set = "";

	$project_process_task_update_uquery_where = " where project_process_task_id = :task_id";

	$project_process_task_update_udata = array(":task_id"=>$task_id);

	$filter_count = 0;

	if($process_id != "")
	{
		$project_process_task_update_uquery_set = $project_process_task_update_uquery_set." project_process_id = :process_id,";
		$project_process_task_update_udata[":process_id"] = $process_id;
		$filter_count++;
	}

  if($road_id != "")
	{
		$project_process_task_update_uquery_set = $project_process_task_update_uquery_set." project_process_task_location_id = :road_id,";
		$project_process_task_update_udata[":road_id"] = $road_id;
		$filter_count++;
	}

	if($task_type != "")
	{
		$project_process_task_update_uquery_set = $project_process_task_update_uquery_set." project_process_task_type = :task_type,";
		$project_process_task_update_udata[":task_type"] = $task_type;
		$filter_count++;
	}

	if($task_doc != "")
	{
		$project_process_task_update_uquery_set = $project_process_task_update_uquery_set." project_process_task_doc = :task_doc,";
		$project_process_task_update_udata[":task_doc"] = $task_doc;
		$filter_count++;
	}

	if($actual_start_date != "")
	{
		$project_process_task_update_uquery_set = $project_process_task_update_uquery_set." project_process_actual_start_date = :actual_start_date,";
		$project_process_task_update_udata[":actual_start_date"] = $actual_start_date;
		$filter_count++;
	}

	if($actual_end_date != "")
	{
		$project_process_task_update_uquery_set = $project_process_task_update_uquery_set." project_process_actual_end_date = :actual_end_date,";
		$project_process_task_update_udata[":actual_end_date"] = $actual_end_date;
		$filter_count++;
	}

	if($completion	!= "")
	{
		$project_process_task_update_uquery_set = $project_process_task_update_uquery_set." project_process_task_completion = :completion,";
		$project_process_task_update_udata[":completion"] = $completion;
		$filter_count++;
	}

	if($contract_completion != "")
	{
		$project_process_task_update_uquery_set = $project_process_task_update_uquery_set." project_process_contract_task_completion = :contract_completion,";
		$project_process_task_update_udata[":contract_completion"] = $contract_completion;
		$filter_count++;
	}

	if($machine_completion	!= "")
	{
		$project_process_task_update_uquery_set = $project_process_task_update_uquery_set." project_process_machine_task_completion = :machine_completion,";
		$project_process_task_update_udata[":machine_completion"] = $machine_completion;
		$filter_count++;
	}

	if($status	!= "")
	{
		$project_process_task_update_uquery_set = $project_process_task_update_uquery_set." project_process_task_status = :status,";
		$project_process_task_update_udata[":status"] = $status;
		$filter_count++;
	}

	if($active != "")
	{
		$project_process_task_update_uquery_set = $project_process_task_update_uquery_set." project_process_task_active = :active,";
		$project_process_task_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_process_task_update_uquery_set = $project_process_task_update_uquery_set." project_process_task_remarks = :remarks,";
		$project_process_task_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_process_task_update_uquery_set = $project_process_task_update_uquery_set." project_process_task_added_by = :added_by,";
		$project_process_task_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_process_task_update_uquery_set = $project_process_task_update_uquery_set." project_process_task_added_on = :added_on,";
		$project_process_task_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_process_task_update_uquery_set = trim($project_process_task_update_uquery_set,',');
	}

	$project_process_task_update_uquery = $project_process_task_update_uquery_base.$project_process_task_update_uquery_set.$project_process_task_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $project_process_task_update_ustatement = $dbConnection->prepare($project_process_task_update_uquery);

        $project_process_task_update_ustatement -> execute($project_process_task_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $task_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Plan
INPUT 	: Project ID,  User ID, Plan Start Date, Plan End Date, Remarks, Added By
OUTPUT 	: Plan ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_plan($project_id,$user_id,$plan_start_date,$plan_end_date,$remarks,$added_by)
{
	// Query
   $project_plan_iquery = "insert into project_plan
   (project_plan_project_id,project_plan_user_id,project_plan_start_date,project_plan_end_date,project_plan_active,project_plan_remarks,project_plan_added_by,project_plan_added_on)
   values(:project_id,:user_id,:plan_start_date,:plan_end_date,:active,:remarks,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_plan_istatement = $dbConnection->prepare($project_plan_iquery);

        // Data
        $project_plan_idata = array(':project_id'=>$project_id,':user_id'=>$user_id,':plan_start_date'=>$plan_start_date,':plan_end_date'=>$plan_end_date,':active'=>'1',
		':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_plan_istatement->execute($project_plan_idata);
		$project_plan_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_plan_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project Plan list
INPUT 	: Plan ID, Project ID, User ID, Start Date, End date, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Plan
BY 		: Lakshmi
*/
function db_get_project_plan($project_plan_search_data)
{

	if(array_key_exists("plan_id",$project_plan_search_data))
	{
		$plan_id = $project_plan_search_data["plan_id"];
	}
	else
	{
		$plan_id= "";
	}

	if(array_key_exists("project_id",$project_plan_search_data))
	{
		$project_id = $project_plan_search_data["project_id"];
	}
	else
	{
		$project_id = "";
	}

	if(array_key_exists("user_id",$project_plan_search_data))
	{
		$user_id = $project_plan_search_data["user_id"];
	}
	else
	{
		$user_id = "";
	}

	if(array_key_exists("plan_start_date",$project_plan_search_data))
	{
		$plan_start_date = $project_plan_search_data["plan_start_date"];
	}
	else
	{
		$plan_start_date = "";
	}

	if(array_key_exists("plan_end_date",$project_plan_search_data))
	{
		$plan_end_date = $project_plan_search_data["plan_end_date"];
	}
	else
	{
		$plan_end_date = "";
	}

	if(array_key_exists("active",$project_plan_search_data))
	{
		$active = $project_plan_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_plan_search_data))
	{
		$added_by = $project_plan_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_plan_search_data))
	{
		$start_date= $project_plan_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_plan_search_data))
	{
		$end_date= $project_plan_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_project_plan_list_squery_base = "select *,U.user_name as added_by,AU.user_name as assigned_user from project_plan PP inner join users U on U.user_id = PP.project_plan_added_by inner join users AU on AU.user_id = PP.project_plan_user_id inner join project_management_project_master PMPM on PMPM.project_management_master_id = PP.project_plan_project_id";

	$get_project_plan_list_squery_where = "";
	$get_project_plan_list_squery_order_by = " order by PMPM.project_master_name ASC";

	$filter_count = 0;
	// Data
	$get_project_plan_list_sdata = array();

	if($plan_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_list_squery_where = $get_project_plan_list_squery_where." where project_plan_id = :plan_id";
		}
		else
		{
			// Query
			$get_project_plan_list_squery_where = $get_project_plan_list_squery_where." and project_plan_id = :plan_id";
		}

		// Data
		$get_project_plan_list_sdata[':plan_id'] = $plan_id;

		$filter_count++;
	}

	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_list_squery_where = $get_project_plan_list_squery_where." where project_plan_project_id = :project_id";
		}
		else
		{
			// Query
			$get_project_plan_list_squery_where = $get_project_plan_list_squery_where." and project_plan_project_id = :project_id";
		}

		// Data
		$get_project_plan_list_sdata[':project_id'] = $project_id;

		$filter_count++;
	}

	if($user_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_list_squery_where = $get_project_plan_list_squery_where." where project_plan_user_id = :user_id";
		}
		else
		{
			// Query
			$get_project_plan_list_squery_where = $get_project_plan_list_squery_where." and project_plan_user_id = :user_id";
		}

		// Data
		$get_project_plan_list_sdata[':user_id']  = $user_id;

		$filter_count++;
	}

	if($plan_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_list_squery_where = $get_project_plan_list_squery_where." where project_plan_start_date = :plan_start_date";
		}
		else
		{
			// Query
			$get_project_plan_list_squery_where = $get_project_plan_list_squery_where." and project_plan_start_date = :plan_start_date";
		}

		// Data
		$get_project_plan_list_sdata[':plan_start_date']  = $plan_start_date;

		$filter_count++;
	}

	if($plan_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_list_squery_where = $get_project_plan_list_squery_where." where project_plan_end_date = :plan_end_date";
		}
		else
		{
			// Query
			$get_project_plan_list_squery_where = $get_project_plan_list_squery_where." and project_plan_end_date = :plan_end_date";
		}

		// Data
		$get_project_plan_list_sdata[':plan_end_date']  = $plan_end_date;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_list_squery_where = $get_project_plan_list_squery_where." where project_plan_active = :active";
		}
		else
		{
			// Query
			$get_project_plan_list_squery_where = $get_project_plan_list_squery_where." and project_plan_active = :active";
		}

		// Data
		$get_project_plan_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_list_squery_where = $get_project_plan_list_squery_where." where project_plan_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_plan_list_squery_where = $get_project_plan_list_squery_where." and project_plan_added_by = :added_by";
		}

		//Data
		$get_project_plan_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_list_squery_where = $get_project_plan_list_squery_where." where project_plan_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_plan_list_squery_where = $get_project_plan_list_squery_where." and project_plan_added_on >= :start_date";
		}

		//Data
		$get_project_plan_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_list_squery_where = $get_project_plan_list_squery_where." where project_plan_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_plan_list_squery_where = $get_project_plan_list_squery_where." and project_plan_added_on <= :end_date";
		}

		//Data
		$get_project_plan_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}


	$get_project_plan_list_squery = $get_project_plan_list_squery_base.$get_project_plan_list_squery_where.$get_project_plan_list_squery_order_by;

	try
	{
		$dbConnection = get_conn_handle();

		$get_project_plan_list_sstatement = $dbConnection->prepare($get_project_plan_list_squery);

		$get_project_plan_list_sstatement -> execute($get_project_plan_list_sdata);

		$get_project_plan_list_sdetails = $get_project_plan_list_sstatement -> fetchAll();

		if(FALSE === $get_project_plan_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_plan_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_plan_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

 /*
PURPOSE : To update Project Plan
INPUT 	: Plan ID, Project Plan Update Array
OUTPUT 	: Plan ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_plan($plan_id,$project_plan_update_data)
{

	if(array_key_exists("project_id",$project_plan_update_data))
	{
		$project_id = $project_plan_update_data["project_id"];
	}
	else
	{
		$project_id = "";
	}

	if(array_key_exists("user_id",$project_plan_update_data))
	{
		$user_id = $project_plan_update_data["user_id"];
	}
	else
	{
		$user_id = "";
	}

	if(array_key_exists("plan_start_date",$project_plan_update_data))
	{
		$plan_start_date = $project_plan_update_data["plan_start_date"];
	}
	else
	{
		$plan_start_date = "";
	}

	if(array_key_exists("plan_end_date",$project_plan_update_data))
	{
		$plan_end_date = $project_plan_update_data["plan_end_date"];
	}
	else
	{
		$plan_end_date = "";
	}

	if(array_key_exists("active",$project_plan_update_data))
	{
		$active = $project_plan_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_plan_update_data))
	{
		$remarks = $project_plan_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_plan_update_data))
	{
		$added_by = $project_plan_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_plan_update_data))
	{
		$added_on = $project_plan_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_plan_update_uquery_base = "update project_plan set";

	$project_plan_update_uquery_set = "";

	$project_plan_update_uquery_where = " where project_plan_id = :plan_id";

	$project_plan_update_udata = array(":plan_id"=>$plan_id);

	$filter_count = 0;

	if($project_id != "")
	{
		$project_plan_update_uquery_set = $project_plan_update_uquery_set." project_plan_project_id = :project_id,";
		$project_plan_update_udata[":project_id"] = $project_id;
		$filter_count++;
	}

	if($user_id != "")
	{
		$project_plan_update_uquery_set = $project_plan_update_uquery_set." project_plan_user_id = :user_id,";
		$project_plan_update_udata[":user_id"] = $user_id;
		$filter_count++;
	}

	if($plan_start_date != "")
	{
		$project_plan_update_uquery_set = $project_plan_update_uquery_set." project_plan_start_date = :plan_start_date,";
		$project_plan_update_udata[":plan_start_date"] = $plan_start_date;
		$filter_count++;
	}

	if($plan_end_date != "")
	{
		$project_plan_update_uquery_set = $project_plan_update_uquery_set." project_plan_end_date = :plan_end_date,";
		$project_plan_update_udata[":plan_end_date"] = $plan_end_date;
		$filter_count++;
	}

	if($active != "")
	{
		$project_plan_update_uquery_set = $project_plan_update_uquery_set." project_plan_active = :active,";
		$project_plan_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_plan_update_uquery_set = $project_plan_update_uquery_set." project_plan_remarks = :remarks,";
		$project_plan_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_plan_update_uquery_set = $project_plan_update_uquery_set." project_plan_added_by = :added_by,";
		$project_plan_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_plan_update_uquery_set = $project_plan_update_uquery_set." project_plan_added_on = :added_on,";
		$project_plan_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_plan_update_uquery_set = trim($project_plan_update_uquery_set,',');
	}

	$project_plan_update_uquery = $project_plan_update_uquery_base.$project_plan_update_uquery_set.$project_plan_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $project_plan_update_ustatement = $dbConnection->prepare($project_plan_update_uquery);

        $project_plan_update_ustatement -> execute($project_plan_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $plan_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Plan Process
INPUT 	: Plan ID, Name, Method, Process Start Date, Process End Date, Remarks, Added By, Approved BY, Assigned User
OUTPUT 	: Process ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_plan_process($plan_id,$name,$method,$process_start_date,$process_end_date,$remarks,$added_by,$approved_by,$assigned_user)
{
	// Query
   $project_plan_process_iquery = "insert into project_plan_process
   (project_plan_process_plan_id,project_plan_process_name,project_plan_process_method,project_plan_process_active,project_plan_process_status,project_plan_process_start_date,
   project_plan_process_end_date,project_plan_process_remarks,project_plan_process_added_by,project_plan_process_approved_by,project_plan_process_assigned_user,
   project_plan_process_added_on)
   values(:plan_id,:name,:method,:active,:status,:process_start_date,:process_end_date,:remarks,:added_by,:approved_by,:assigned_user,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_plan_process_istatement = $dbConnection->prepare($project_plan_process_iquery);

        // Data
        $project_plan_process_idata = array(':plan_id'=>$plan_id,':name'=>$name,':method'=>$method,':active'=>'1',':status'=>'Waiting',':process_start_date'=>$process_start_date,
		':process_end_date'=>$process_end_date,':remarks'=>$remarks,':added_by'=>$added_by,':approved_by'=>$approved_by,':assigned_user'=>$assigned_user,
		':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_plan_process_istatement->execute($project_plan_process_idata);
		$project_plan_process_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_plan_process_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project Plan Process list
INPUT 	: Process ID, Plan ID, Name, Method, Active, Status, Process Start Date, Process End Date, Added By, Approved By, Approved On, Assigned User, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Plan Process
BY 		: Lakshmi
*/
function db_get_project_plan_process($project_plan_process_search_data)
{

	if(array_key_exists("process_id",$project_plan_process_search_data))
	{
		$process_id = $project_plan_process_search_data["process_id"];
	}
	else
	{
		$process_id= "";
	}

	if(array_key_exists("project_id",$project_plan_process_search_data))
	{
		$project_id = $project_plan_process_search_data["project_id"];
	}
	else
	{
		$project_id = "";
	}

	if(array_key_exists("assigned_to",$project_plan_process_search_data))
	{
		$assigned_to = $project_plan_process_search_data["assigned_to"];
	}
	else
	{
		$assigned_to = "";
	}

	if(array_key_exists("plan_id",$project_plan_process_search_data))
	{
		$plan_id = $project_plan_process_search_data["plan_id"];
	}
	else
	{
		$plan_id = "";
	}

	if(array_key_exists("name",$project_plan_process_search_data))
	{
		$name = $project_plan_process_search_data["name"];
	}
	else
	{
		$name = "";
	}

	if(array_key_exists("method",$project_plan_process_search_data))
	{
		$method = $project_plan_process_search_data["method"];
	}
	else
	{
		$method = "";
	}

	if(array_key_exists("active",$project_plan_process_search_data))
	{
		$active = $project_plan_process_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("status",$project_plan_process_search_data))
	{
		$status = $project_plan_process_search_data["status"];
	}
	else
	{
		$status = "";
	}

	if(array_key_exists("process_start_date",$project_plan_process_search_data))
	{
		$process_start_date = $project_plan_process_search_data["process_start_date"];
	}
	else
	{
		$process_start_date = "";
	}

	if(array_key_exists("process_end_date",$project_plan_process_search_data))
	{
		$process_end_date = $project_plan_process_search_data["process_end_date"];
	}
	else
	{
		$process_end_date = "";
	}

	if(array_key_exists("added_by",$project_plan_process_search_data))
	{
		$added_by = $project_plan_process_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("approved_by",$project_plan_process_search_data))
	{
		$approved_by = $project_plan_process_search_data["approved_by"];
	}
	else
	{
		$approved_by = "";
	}

	if(array_key_exists("approved_on",$project_plan_process_search_data))
	{
		$approved_on = $project_plan_process_search_data["approved_on"];
	}
	else
	{
		$approved_on = "";
	}

	if(array_key_exists("assigned_user",$project_plan_process_search_data))
	{
		$assigned_user = $project_plan_process_search_data["assigned_user"];
	}
	else
	{
		$assigned_user = "";
	}

	if(array_key_exists("start_date",$project_plan_process_search_data))
	{
		$start_date= $project_plan_process_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_plan_process_search_data))
	{
		$end_date= $project_plan_process_search_data["end_date"];
	}

	{
		$end_date= "";
	}

	if(array_key_exists("order",$project_plan_process_search_data))
	{
		$order = $project_plan_process_search_data["order"];
	}
	else
	{
		$order = "";
	}

	if(array_key_exists("ignore_not_started",$project_plan_process_search_data))
	{
		$ignore_not_started = $project_plan_process_search_data["ignore_not_started"];
	}
	else
	{
		$ignore_not_started = "";
	}

	$get_project_plan_process_list_squery_base = "select *,U.user_name as added_by,AU.user_name as assigned_user,AP.user_name as approved_by from project_plan_process PPP inner join users U on U.user_id = PPP.project_plan_process_assigned_user inner join users AU on AU.user_id = PPP.project_plan_process_assigned_user inner join project_process_master PPM on PPM.project_process_master_id = PPP.project_plan_process_name left outer join users AP on AP.user_id=PPP.project_plan_process_approved_by inner join project_plan PP on PP.project_plan_id=PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id = PP.project_plan_project_id";

	$get_project_plan_process_list_squery_where = "";

	$get_project_plan_process_list_squery_order = "";

	$filter_count = 0;

	// Data
	$get_project_plan_process_list_sdata = array();

	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." where project_plan_process_id = :process_id";
		}
		else
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." and project_plan_process_id = :process_id";
		}

		// Data
		$get_project_plan_process_list_sdata[':process_id'] = $process_id;

		$filter_count++;
	}

	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." where PP.project_plan_project_id = :project_id";
		}
		else
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." and PP.project_plan_project_id = :project_id";
		}

		// Data
		$get_project_plan_process_list_sdata[':project_id'] = $project_id;

		$filter_count++;
	}

	if($plan_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." where project_plan_process_plan_id = :plan_id";
		}
		else
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." and project_plan_process_plan_id = :plan_id";
		}

		// Data
		$get_project_plan_process_list_sdata[':plan_id'] = $plan_id;

		$filter_count++;
	}

	if($assigned_to != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." where PPP.project_plan_process_assigned_user = :assigned_to";
		}
		else
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." and PPP.project_plan_process_assigned_user = :assigned_to";
		}

		// Data
		$get_project_plan_process_list_sdata[':assigned_to'] = $assigned_to;

		$filter_count++;
	}

	if($name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." where project_plan_process_name = :name";
		}
		else
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." and project_plan_process_name = :name";
		}

		// Data
		$get_project_plan_process_list_sdata[':name']  = $name;

		$filter_count++;
	}

	if($method != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." where project_plan_process_method = :method";
		}
		else
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." and project_plan_process_method = :method";
		}

		// Data
		$get_project_plan_process_list_sdata[':method']  = $method;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." where project_plan_process_active = :active";
		}
		else
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." and project_plan_process_active = :active";
		}

		// Data
		$get_project_plan_process_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." where project_plan_process_status = :status";
		}
		else
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." and project_plan_process_status = :status";
		}

		// Data
		$get_project_plan_process_list_sdata[':status']  = $status;

		$filter_count++;
	}

	if($process_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." where project_plan_process_start_date = :process_start_date";
		}
		else
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." and project_plan_process_start_date = :process_start_date";
		}

		// Data
		$get_project_plan_process_list_sdata[':process_start_date']  = $process_start_date;

		$filter_count++;
	}

	if($ignore_not_started != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." where project_plan_process_start_date != :process_nstart_date";
		}
		else
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." and project_plan_process_start_date != :process_nstart_date";
		}

		// Data
		$get_project_plan_process_list_sdata[':process_nstart_date']  = '0000-00-00';

		$filter_count++;
	}

	if($process_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." where project_plan_process_end_date = :process_end_date";
		}
		else
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." and project_plan_process_end_date = :process_end_date";
		}

		// Data
		$get_project_plan_process_list_sdata[':process_end_date']  = $process_end_date;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." where project_plan_process_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." and project_plan_process_added_by = :added_by";
		}

		//Data
		$get_project_plan_process_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($approved_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." where project_plan_process_approved_by = :approved_by";
		}
		else
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." and project_plan_process_approved_by = :approved_by";
		}

		//Data
		$get_project_plan_process_list_sdata[':approved_by']  = $approved_by;

		$filter_count++;
	}

	if($approved_on!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." where project_plan_process_approved_on = :approved_on";
		}
		else
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." and project_plan_process_approved_on = :approved_on";
		}

		//Data
		$get_project_plan_process_list_sdata[':approved_on']  = $approved_on;

		$filter_count++;
	}

	if($assigned_user!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." where project_plan_process_assigned_user = :assigned_user";
		}
		else
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." and project_plan_process_assigned_user = :assigned_user";
		}

		//Data
		$get_project_plan_process_list_sdata[':assigned_user']  = $assigned_user;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." where project_plan_process_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." and project_plan_process_added_on >= :start_date";
		}

		//Data
		$get_project_plan_process_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." where project_plan_process_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_plan_process_list_squery_where = $get_project_plan_process_list_squery_where." and project_plan_process_added_on <= :end_date";
		}

		//Data
		$get_project_plan_process_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	if($order != "")
	{
		switch($order)
		{
			case 'start_date_asc':
			$get_project_plan_process_list_squery_order = ' order by project_plan_process_start_date asc project_process_master_order ASC';
			break;
			case 'end_date_desc':
			$get_project_plan_process_list_squery_order = ' order by project_plan_process_end_date descproject_process_master_order ASC';
			break;
		}
	}
	else
	{
		$get_project_plan_process_list_squery_order = ' order by project_process_master_order ASC';
	}

	$get_project_plan_process_list_squery = $get_project_plan_process_list_squery_base.$get_project_plan_process_list_squery_where.$get_project_plan_process_list_squery_order;

	try
	{
		$dbConnection = get_conn_handle();

		$get_project_plan_process_list_sstatement = $dbConnection->prepare($get_project_plan_process_list_squery);

		$get_project_plan_process_list_sstatement -> execute($get_project_plan_process_list_sdata);

		$get_project_plan_process_list_sdetails = $get_project_plan_process_list_sstatement -> fetchAll();

		if(FALSE === $get_project_plan_process_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_plan_process_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_plan_process_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

 /*
PURPOSE : To update Project Plan Process
INPUT 	: Process ID, Project Plan Process Update Array
OUTPUT 	: Process ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_plan_process($process_id,$project_plan_process_update_data)
{

	if(array_key_exists("plan_id",$project_plan_process_update_data))
	{
		$plan_id = $project_plan_process_update_data["plan_id"];
	}
	else
	{
		$plan_id = "";
	}

	if(array_key_exists("name",$project_plan_process_update_data))
	{
		$name = $project_plan_process_update_data["name"];
	}
	else
	{
		$name = "";
	}

	if(array_key_exists("method",$project_plan_process_update_data))
	{
		$method = $project_plan_process_update_data["method"];
	}
	else
	{
		$method = "";
	}

	if(array_key_exists("active",$project_plan_process_update_data))
	{
		$active = $project_plan_process_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("status",$project_plan_process_update_data))
	{
		$status = $project_plan_process_update_data["status"];
	}
	else
	{
		$status = "";
	}

	if(array_key_exists("process_start_date",$project_plan_process_update_data))
	{
		$process_start_date = $project_plan_process_update_data["process_start_date"];
	}
	else
	{
		$process_start_date = "";
	}

	if(array_key_exists("process_end_date",$project_plan_process_update_data))
	{
		$process_end_date = $project_plan_process_update_data["process_end_date"];
	}
	else
	{
		$process_end_date = "";
	}
	if(array_key_exists("remarks",$project_plan_process_update_data))
	{
		$remarks = $project_plan_process_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_plan_process_update_data))
	{
		$added_by = $project_plan_process_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("approved_by",$project_plan_process_update_data))
	{
		$approved_by = $project_plan_process_update_data["approved_by"];
	}
	else
	{
		$approved_by = "";
	}

	if(array_key_exists("approved_on",$project_plan_process_update_data))
	{
		$approved_on = $project_plan_process_update_data["approved_on"];
	}
	else
	{
		$approved_on = "";
	}

	if(array_key_exists("assigned_user",$project_plan_process_update_data))
	{
		$assigned_user = $project_plan_process_update_data["assigned_user"];
	}
	else
	{
		$assigned_user = "";
	}

	if(array_key_exists("added_on",$project_plan_process_update_data))
	{
		$added_on = $project_plan_process_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_plan_process_update_uquery_base = "update project_plan_process set";

	$project_plan_process_update_uquery_set = "";

	$project_plan_process_update_uquery_where = " where project_plan_process_id = :process_id";

	$project_plan_process_update_udata = array(":process_id"=>$process_id);

	$filter_count = 0;

	if($plan_id != "")
	{
		$project_plan_process_update_uquery_set = $project_plan_process_update_uquery_set." project_plan_process_plan_id = :plan_id,";
		$project_plan_process_update_udata[":plan_id"] = $plan_id;
		$filter_count++;
	}

	if($name != "")
	{
		$project_plan_process_update_uquery_set = $project_plan_process_update_uquery_set." project_plan_process_name = :name,";
		$project_plan_process_update_udata[":name"] = $name;
		$filter_count++;
	}

	if($method != "")
	{
		$project_plan_process_update_uquery_set = $project_plan_process_update_uquery_set." project_plan_process_method = :method,";
		$project_plan_process_update_udata[":method"] = $method;
		$filter_count++;
	}

	if($active != "")
	{
		$project_plan_process_update_uquery_set = $project_plan_process_update_uquery_set." project_plan_process_active = :active,";
		$project_plan_process_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($status != "")
	{
		$project_plan_process_update_uquery_set = $project_plan_process_update_uquery_set." project_plan_process_status = :status,";
		$project_plan_process_update_udata[":status"] = $status;
		$filter_count++;
	}

	if($process_start_date != "")
	{
		$project_plan_process_update_uquery_set = $project_plan_process_update_uquery_set." project_plan_process_start_date = :process_start_date,";
		$project_plan_process_update_udata[":process_start_date"] = $process_start_date;
		$filter_count++;
	}

	if($process_end_date != "")
	{
		$project_plan_process_update_uquery_set = $project_plan_process_update_uquery_set." project_plan_process_end_date = :process_end_date,";
		$project_plan_process_update_udata[":process_end_date"] = $process_end_date;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_plan_process_update_uquery_set = $project_plan_process_update_uquery_set." project_plan_process_remarks = :remarks,";
		$project_plan_process_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($approved_by != "")
	{
		$project_plan_process_update_uquery_set = $project_plan_process_update_uquery_set." project_plan_process_approved_by = :approved_by,";
		$project_plan_process_update_udata[":approved_by"] = $approved_by;
		$filter_count++;
	}

	if($approved_on != "")
	{
		$project_plan_process_update_uquery_set = $project_plan_process_update_uquery_set." project_plan_process_approved_on = :approved_on,";
		$project_plan_process_update_udata[":approved_on"] = $approved_on;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_plan_process_update_uquery_set = $project_plan_process_update_uquery_set." project_plan_process_added_by = :added_by,";
		$project_plan_process_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($assigned_user != "")
	{
		$project_plan_process_update_uquery_set = $project_plan_process_update_uquery_set." project_plan_process_assigned_user = :assigned_user,";
		$project_plan_process_update_udata[":assigned_user"] = $assigned_user;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_plan_process_update_uquery_set = $project_plan_process_update_uquery_set." project_plan_process_added_on = :added_on,";
		$project_plan_process_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_plan_process_update_uquery_set = trim($project_plan_process_update_uquery_set,',');
	}

	$project_plan_process_update_uquery = $project_plan_process_update_uquery_base.$project_plan_process_update_uquery_set.$project_plan_process_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $project_plan_process_update_ustatement = $dbConnection->prepare($project_plan_process_update_uquery);

        $project_plan_process_update_ustatement -> execute($project_plan_process_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $process_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Task Required Item
INPUT 	: Material ID, Task ID, Quantity, Rate, Remarks, Added By
OUTPUT 	: Item ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_task_required_item($material_id,$task_id,$quantity,$rate,$remarks,$added_by)
{
	// Query
   $project_task_required_item_iquery = "insert into project_task_required_item
   (project_task_required_material_id,project_task_required_item_task_id,project_task_required_item_quantity,project_task_required_item_rate,project_task_required_item_status,
   project_task_required_item_active,project_task_required_item_remarks,project_task_required_item_added_by,project_task_required_item_added_on)
   values(:material_id,:task_id,:quantity,:rate,:status,:active,:remarks,:added_by,:added_on)";
    try
    {
        $dbConnection = get_conn_handle();
        $project_task_required_item_istatement = $dbConnection->prepare($project_task_required_item_iquery);

        // Data
        $project_task_required_item_idata = array(':material_id'=>$material_id,':task_id'=>$task_id,':quantity'=>$quantity,':rate'=>$rate,':status'=>'Approved',
		':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $project_task_required_item_istatement->execute($project_task_required_item_idata);
		$project_task_required_item_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_task_required_item_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project Task Required Item list
INPUT 	: Item ID, Material ID, Task ID, Quantity, Rate, Status, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Task Required Item
BY 		: Lakshmi
*/
function db_get_project_task_required_item($project_task_required_item_search_data)
{

	if(array_key_exists("item_id",$project_task_required_item_search_data))
	{
		$item_id = $project_task_required_item_search_data["item_id"];
	}
	else
	{
		$item_id= "";
	}

	if(array_key_exists("material_id",$project_task_required_item_search_data))
	{
		$material_id = $project_task_required_item_search_data["material_id"];
	}
	else
	{
		$material_id = "";
	}

	if(array_key_exists("task_id",$project_task_required_item_search_data))
	{
		$task_id = $project_task_required_item_search_data["task_id"];
	}
	else
	{
		$task_id = "";
	}

	if(array_key_exists("process_id",$project_task_required_item_search_data))
	{
		$process_id = $project_task_required_item_search_data["process_id"];
	}
	else
	{
		$process_id = "";
	}

	if(array_key_exists("project_id",$project_task_required_item_search_data))
	{
		$project_id = $project_task_required_item_search_data["project_id"];
	}
	else
	{
		$project_id = "";
	}

	if(array_key_exists("user_id",$project_task_required_item_search_data))
	{
		$user_id = $project_task_required_item_search_data["user_id"];
	}
	else
	{
		$user_id = "";
	}

	if(array_key_exists("quantity",$project_task_required_item_search_data))
	{
		$quantity = $project_task_required_item_search_data["quantity"];
	}
	else
	{
		$quantity = "";
	}

	if(array_key_exists("rate",$project_task_required_item_search_data))
	{
		$rate = $project_task_required_item_search_data["rate"];
	}
	else
	{
		$rate = "";
	}

	if(array_key_exists("status",$project_task_required_item_search_data))
	{
		$status = $project_task_required_item_search_data["status"];
	}
	else
	{
		$status = "";
	}

	if(array_key_exists("active",$project_task_required_item_search_data))
	{
		$active = $project_task_required_item_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_task_required_item_search_data))
	{
		$added_by = $project_task_required_item_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_task_required_item_search_data))
	{
		$start_date= $project_task_required_item_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_task_required_item_search_data))
	{
		$end_date= $project_task_required_item_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_project_task_required_item_list_squery_base = " select * from project_task_required_item PTRI inner join stock_material_master SMM on SMM.stock_material_id = PTRI.project_task_required_material_id inner join stock_unit_measure_master SUMM on SUMM.stock_unit_id = SMM.stock_material_unit_of_measure inner join project_plan_process_task PPPT on PPPT.project_process_task_id = PTRI.project_task_required_item_task_id inner join project_task_master PTM on PTM.project_task_master_id = PPPT.project_process_task_type inner join project_plan_process PPP on PPP.project_plan_process_id = PPPT.project_process_id inner join project_process_master PPM on PPM.project_process_master_id = PPP.project_plan_process_name inner join project_plan PP on PP.project_plan_id = PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id = PP.project_plan_project_id inner join users U on U.user_id = PTRI.project_task_required_item_added_by";

	$get_project_task_required_item_list_squery_where = "";

	$filter_count = 0;

	// Data
	$get_project_task_required_item_list_sdata = array();

	if($item_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." where project_task_required_item_id = :item_id";
		}
		else
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." and project_task_required_item_id = :item_id";
		}

		// Data
		$get_project_task_required_item_list_sdata[':item_id'] = $item_id;

		$filter_count++;
	}

	if($material_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." where project_task_required_material_id = :material_id";
		}
		else
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." and project_task_required_material_id = :material_id";
		}

		// Data
		$get_project_task_required_item_list_sdata[':material_id'] = $material_id;

		$filter_count++;
	}

	if($task_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." where project_task_required_item_task_id = :task_id";
		}
		else
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." and project_task_required_item_task_id = :task_id";
		}

		// Data
		$get_project_task_required_item_list_sdata[':task_id'] = $task_id;

		$filter_count++;
	}

	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." where PPM.project_process_master_id = :process_id";
		}
		else
		{
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." and PPM.project_process_master_id = :process_id";
			// Query
		}

		// Data
		$get_project_task_required_item_list_sdata[':process_id'] = $process_id;

		$filter_count++;
	}

	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." where PMPM.project_management_master_id = :project_id";
		}
		else
		{
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." and PMPM.project_management_master_id = :project_id";
			// Query
		}

		// Data
		$get_project_task_required_item_list_sdata[':project_id'] = $project_id;

		$filter_count++;
	}

	if($user_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." where U.user_id = :user_id";
		}
		else
		{
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." and U.user_id = :user_id";
			// Query
		}

		// Data
		$get_project_task_required_item_list_sdata[':user_id'] = $user_id;

		$filter_count++;
	}

	if($quantity != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." where project_task_required_item_quantity = :quantity";
		}
		else
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." and project_task_required_item_quantity = :quantity";
		}

		// Data
		$get_project_task_required_item_list_sdata[':quantity'] = $quantity;

		$filter_count++;
	}

	if($rate != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." where project_task_required_item_rate = :rate";
		}
		else
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." and project_task_required_item_rate = :rate";
		}

		// Data
		$get_project_task_required_item_list_sdata[':rate'] = $rate;

		$filter_count++;
	}

	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." where project_task_required_item_status = :status";
		}
		else
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." and project_task_required_item_status = :status";
		}

		// Data
		$get_project_task_required_item_list_sdata[':status'] = $status;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." where project_task_required_item_active = :active";
		}
		else
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." and project_task_required_item_active = :active";
		}

		// Data
		$get_project_task_required_item_list_sdata[':active'] = $active;

		$filter_count++;
	}

	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." where project_task_required_item_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." and project_task_required_item_added_by = :added_by";
		}

		// Data
		$get_project_task_required_item_list_sdata[':added_by'] = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." where project_task_required_item_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." and project_task_required_item_added_on >= :start_date";
		}

		//Data
		$get_project_task_required_item_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." where project_task_required_item_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." and project_task_required_item_added_on <= :end_date";
		}

		//Data
		$get_project_task_required_item_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}


	$get_project_task_required_item_list_squery = $get_project_task_required_item_list_squery_base.$get_project_task_required_item_list_squery_where;

	try
	{
		$dbConnection = get_conn_handle();

		$get_project_task_required_item_list_sstatement = $dbConnection->prepare($get_project_task_required_item_list_squery);

		$get_project_task_required_item_list_sstatement -> execute($get_project_task_required_item_list_sdata);

		$get_project_task_required_item_list_sdetails = $get_project_task_required_item_list_sstatement -> fetchAll();

		if(FALSE === $get_project_task_required_item_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_task_required_item_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_task_required_item_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

 /*
PURPOSE : To update Project Task Required Item
INPUT 	: Item ID, Project Task Required Item Update Array
OUTPUT 	: Item ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_task_required_item($item_id,$project_task_required_item_update_data)
{

	if(array_key_exists("material_id",$project_task_required_item_update_data))
	{
		$material_id = $project_task_required_item_update_data["material_id"];
	}
	else
	{
		$material_id = "";
	}

	if(array_key_exists("task_id",$project_task_required_item_update_data))
	{
		$task_id = $project_task_required_item_update_data["task_id"];
	}
	else
	{
		$task_id = "";
	}

	if(array_key_exists("quantity",$project_task_required_item_update_data))
	{
		$quantity = $project_task_required_item_update_data["quantity"];
	}
	else
	{
		$quantity = "";
	}

	if(array_key_exists("rate",$project_task_required_item_update_data))
	{
		$rate = $project_task_required_item_update_data["rate"];
	}
	else
	{
		$rate = "";
	}

	if(array_key_exists("status",$project_task_required_item_update_data))
	{
		$status = $project_task_required_item_update_data["status"];
	}
	else
	{
		$status = "";
	}

	if(array_key_exists("active",$project_task_required_item_update_data))
	{
		$active = $project_task_required_item_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_task_required_item_update_data))
	{
		$remarks = $project_task_required_item_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_task_required_item_update_data))
	{
		$added_by = $project_task_required_item_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_task_required_item_update_data))
	{
		$added_on = $project_task_required_item_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_task_required_item_update_uquery_base = "update project_task_required_item set ";

	$project_task_required_item_update_uquery_set = "";

	$project_task_required_item_update_uquery_where = " where project_task_required_item_id = :item_id";

	$project_task_required_item_update_udata = array(":item_id"=>$item_id);

	$filter_count = 0;

	if($material_id != "")
	{
		$project_task_required_item_update_uquery_set = $project_task_required_item_update_uquery_set." project_task_required_material_id = :material_id,";
		$project_task_required_item_update_udata[":material_id"] = $material_id;
		$filter_count++;
	}

	if($task_id != "")
	{
		$project_task_required_item_update_uquery_set = $project_task_required_item_update_uquery_set." project_task_required_item_task_id = :task_id,";
		$project_task_required_item_update_udata[":task_id"] = $task_id;
		$filter_count++;
	}

	if($quantity != "")
	{
		$project_task_required_item_update_uquery_set = $project_task_required_item_update_uquery_set." project_task_required_item_quantity = :quantity,";
		$project_task_required_item_update_udata[":quantity"] = $quantity;
		$filter_count++;
	}

	if($rate != "")
	{
		$project_task_required_item_update_uquery_set = $project_task_required_item_update_uquery_set." project_task_required_item_rate = :rate,";
		$project_task_required_item_update_udata[":rate"] = $rate;
		$filter_count++;
	}

	if($status != "")
	{
		$project_task_required_item_update_uquery_set = $project_task_required_item_update_uquery_set." project_task_required_item_status = :status,";
		$project_task_required_item_update_udata[":status"] = $status;
		$filter_count++;
	}

	if($active != "")
	{
		$project_task_required_item_update_uquery_set = $project_task_required_item_update_uquery_set." project_task_required_item_active = :active,";
		$project_task_required_item_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_task_required_item_update_uquery_set = $project_task_required_item_update_uquery_set." project_task_required_item_remarks = :remarks,";
		$project_task_required_item_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_task_required_item_update_uquery_set = $project_task_required_item_update_uquery_set." project_task_required_item_added_by = :added_by,";
		$project_task_required_item_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_task_required_item_update_uquery_set = $project_task_required_item_update_uquery_set." project_task_required_item_added_on = :added_on,";
		$project_task_required_item_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_task_required_item_update_uquery_set = trim($project_task_required_item_update_uquery_set,',');
	}

	$project_task_required_item_update_uquery = $project_task_required_item_update_uquery_base.$project_task_required_item_update_uquery_set.$project_task_required_item_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();

        $project_task_required_item_update_ustatement = $dbConnection->prepare($project_task_required_item_update_uquery);

        $project_task_required_item_update_ustatement -> execute($project_task_required_item_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $item_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Task Man Power
INPUT 	: Task ID, Power Type ID, Hours, Remarks, Added By
OUTPUT 	: Man Power ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_task_man_power($task_id,$power_type_id,$hours,$remarks,$added_by)
{
	// Query
   $project_task_man_power_iquery = "insert into project_task_man_power
   (project_task_man_power_task_id,project_task_man_power_type_id,project_task_man_power_hours,project_task_man_power_status,project_task_man_power_active,
   project_task_man_power_remarks,project_task_man_power_added_by,project_task_man_power_added_on)
   values(:task_id,:power_type_id,:hours,:status,:active,:remarks,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_task_man_power_istatement = $dbConnection->prepare($project_task_man_power_iquery);

        // Data
        $project_task_man_power_idata = array(':task_id'=>$task_id,':power_type_id'=>$power_type_id,':hours'=>$hours,':status'=>'0',':active'=>'1',':remarks'=>$remarks,
		':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_task_man_power_istatement->execute($project_task_man_power_idata);
		$project_task_man_power_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_task_man_power_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project Task Man Power list
INPUT 	: Man Power ID, Task ID, Power Type ID, Hours, Status, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Task Man Power
BY 		: Lakshmi
*/
function db_get_project_task_man_power($project_task_man_power_search_data)
{

	if(array_key_exists("man_power_id",$project_task_man_power_search_data))
	{
		$man_power_id = $project_task_man_power_search_data["man_power_id"];
	}
	else
	{
		$man_power_id= "";
	}

	if(array_key_exists("task_id",$project_task_man_power_search_data))
	{
		$task_id = $project_task_man_power_search_data["task_id"];
	}
	else
	{
		$task_id = "";
	}

	if(array_key_exists("process",$project_task_man_power_search_data))
	{
		$process = $project_task_man_power_search_data["process"];
	}
	else
	{
		$process = "";
	}

	if(array_key_exists("project_id",$project_task_man_power_search_data))
	{
		$project_id = $project_task_man_power_search_data["project_id"];
	}
	else
	{
		$project_id = "";
	}

	if(array_key_exists("user_id",$project_task_man_power_search_data))
	{
		$user_id = $project_task_man_power_search_data["user_id"];
	}
	else
	{
		$user_id = "";
	}

	if(array_key_exists("power_type_id",$project_task_man_power_search_data))
	{
		$power_type_id = $project_task_man_power_search_data["power_type_id"];
	}
	else
	{
		$power_type_id = "";
	}

	if(array_key_exists("hours",$project_task_man_power_search_data))
	{
		$hours = $project_task_man_power_search_data["hours"];
	}
	else
	{
		$hours = "";
	}

	if(array_key_exists("status",$project_task_man_power_search_data))
	{
		$status = $project_task_man_power_search_data["status"];
	}
	else
	{
		$status = "";
	}

	if(array_key_exists("active",$project_task_man_power_search_data))
	{
		$active = $project_task_man_power_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_task_man_power_search_data))
	{
		$added_by = $project_task_man_power_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_task_man_power_search_data))
	{
		$start_date= $project_task_man_power_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_task_man_power_search_data))
	{
		$end_date= $project_task_man_power_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_project_task_man_power_list_squery_base = "select * from project_task_man_power PTMP inner join project_task_master PTM on PTM.project_task_master_id = PTMP.project_task_man_power_task_id inner join project_man_power_type_master PMPTM on PMPTM.project_man_power_master_id = PTMP.project_task_man_power_type_id inner join project_process_master PPM on PPM.project_process_master_id = PTM.project_task_master_process inner join project_plan_process PPP on PPP.project_plan_process_id = PPM.project_process_master_id inner join project_plan PP on PP.project_plan_id = PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id = PP.project_plan_project_id  inner join project_task_user_mapping PTUM on PTUM.project_task_id=PTM.project_task_master_id inner join users U on U.user_id = PTUM.project_task_user_id";

	$get_project_task_man_power_list_squery_where = "";

	$filter_count = 0;

	// Data
	$get_project_task_man_power_list_sdata = array();

	if($man_power_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." where project_task_man_power_id = :man_power_id";
		}
		else
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." and project_task_man_power_id = :man_power_id";
		}

		// Data
		$get_project_task_man_power_list_sdata[':man_power_id'] = $man_power_id;

		$filter_count++;
	}

	if($task_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." where project_task_man_power_task_id = :task_id";
		}
		else
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." and project_task_man_power_task_id = :task_id";
		}

		// Data
		$get_project_task_man_power_list_sdata[':task_id'] = $task_id;

		$filter_count++;
	}

	if($process != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." where PPM.project_process_master_id = :process";
		}
		else
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." and PPM.project_process_master_id = :process";
		}

		// Data
		$get_project_task_man_power_list_sdata[':process'] = $process;

		$filter_count++;
	}

	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." where PMPM.project_management_master_id = :project_id";
		}
		else
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." and PMPM.project_management_master_id = :project_id";
		}

		// Data
		$get_project_task_man_power_list_sdata[':project_id'] = $project_id;

		$filter_count++;
	}

	if($user_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." where U.user_id = :user_id";
		}
		else
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." and U.user_id = :user_id";
		}

		// Data
		$get_project_task_man_power_list_sdata[':user_id'] = $user_id;

		$filter_count++;
	}

	if($power_type_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." where project_task_man_power_type_id = :power_type_id";
		}
		else
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." and project_task_man_power_type_id = :power_type_id";
		}

		// Data
		$get_project_task_man_power_list_sdata[':power_type_id'] = $power_type_id;

		$filter_count++;
	}

	if($hours != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." where project_task_man_power_hours = :hours";
		}
		else
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." and project_task_man_power_hours = :hours";
		}

		// Data
		$get_project_task_man_power_list_sdata[':hours'] = $hours;

		$filter_count++;
	}

	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." where project_task_man_power_status = :status";
		}
		else
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." and project_task_man_power_status = :status";
		}

		// Data
		$get_project_task_man_power_list_sdata[':status'] = $status;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." where project_task_man_power_active = :active";
		}
		else
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." and project_task_man_power_active = :active";
		}

		// Data
		$get_project_task_man_power_list_sdata[':active'] = $active;

		$filter_count++;
	}

	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." where project_task_man_power_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." and project_task_man_power_added_by = :added_by";
		}

		// Data
		$get_project_task_man_power_list_sdata[':added_by'] = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." where project_task_man_power_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." and project_task_man_power_added_on >= :start_date";
		}

		//Data
		$get_project_task_man_power_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." where project_task_man_power_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_task_man_power_list_squery_where = $get_project_task_man_power_list_squery_where." and project_task_man_power_added_on <= :end_date";
		}

		//Data
		$get_project_task_man_power_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	$get_project_task_man_power_list_order_by = " order by project_task_actual_manpower_added_on DESC";
	$get_project_task_man_power_list_squery = $get_project_task_man_power_list_squery_base.$get_project_task_man_power_list_squery_where.$get_project_task_man_power_list_order_by;

	try
	{
		$dbConnection = get_conn_handle();

		$get_project_task_man_power_list_sstatement = $dbConnection->prepare($get_project_task_man_power_list_squery);

		$get_project_task_man_power_list_sstatement -> execute($get_project_task_man_power_list_sdata);

		$get_project_task_man_power_list_sdetails = $get_project_task_man_power_list_sstatement -> fetchAll();

		if(FALSE === $get_project_task_man_power_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_task_man_power_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_task_man_power_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

 /*
PURPOSE : To update Project Task Man Power
INPUT 	: Man Power ID, Project Task Required Item Update Array
OUTPUT 	: Man Power ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_task_man_power($man_power_id,$project_task_man_power_update_data)
{

	if(array_key_exists("task_id",$project_task_man_power_update_data))
	{
		$task_id = $project_task_man_power_update_data["task_id"];
	}
	else
	{
		$task_id = "";
	}

	if(array_key_exists("power_type_id",$project_task_man_power_update_data))
	{
		$power_type_id = $project_task_man_power_update_data["power_type_id"];
	}
	else
	{
		$power_type_id = "";
	}

	if(array_key_exists("hours",$project_task_man_power_update_data))
	{
		$hours = $project_task_man_power_update_data["hours"];
	}
	else
	{
		$hours = "";
	}

	if(array_key_exists("status",$project_task_man_power_update_data))
	{
		$status = $project_task_man_power_update_data["status"];
	}
	else
	{
		$status = "";
	}

	if(array_key_exists("active",$project_task_man_power_update_data))
	{
		$active = $project_task_man_power_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_task_man_power_update_data))
	{
		$remarks = $project_task_man_power_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_task_man_power_update_data))
	{
		$added_by = $project_task_man_power_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_task_man_power_update_data))
	{
		$added_on = $project_task_man_power_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_task_man_power_update_uquery_base = "update project_task_man_power set";

	$project_task_man_power_update_uquery_set = "";

	$project_task_man_power_update_uquery_where = " where project_task_man_power_id = :man_power_id";

	$project_task_man_power_update_udata = array(":man_power_id"=>$man_power_id);

	$filter_count = 0;

	if($task_id != "")
	{
		$project_task_man_power_update_uquery_set = $project_task_man_power_update_uquery_set." project_task_man_power_task_id = :task_id,";
		$project_task_man_power_update_udata[":task_id"] = $task_id;
		$filter_count++;
	}

	if($power_type_id != "")
	{
		$project_task_man_power_update_uquery_set = $project_task_man_power_update_uquery_set." project_task_man_power_type_id = :power_type_id,";
		$project_task_man_power_update_udata[":power_type_id"] = $power_type_id;
		$filter_count++;
	}

	if($hours != "")
	{
		$project_task_man_power_update_uquery_set = $project_task_man_power_update_uquery_set." project_task_man_power_hours = :hours,";
		$project_task_man_power_update_udata[":hours"] = $hours;
		$filter_count++;
	}

	if($status != "")
	{
		$project_task_man_power_update_uquery_set = $project_task_man_power_update_uquery_set." project_task_man_power_status = :status,";
		$project_task_man_power_update_udata[":status"] = $status;
		$filter_count++;
	}

	if($active != "")
	{
		$project_task_man_power_update_uquery_set = $project_task_man_power_update_uquery_set." project_task_man_power_active = :active,";
		$project_task_man_power_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_task_man_power_update_uquery_set = $project_task_man_power_update_uquery_set." project_task_man_power_remarks = :remarks,";
		$project_task_man_power_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_task_man_power_update_uquery_set = $project_task_man_power_update_uquery_set." project_task_man_power_added_by = :added_by,";
		$project_task_man_power_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_task_man_power_update_uquery_set = $project_task_man_power_update_uquery_set." project_task_man_power_added_on = :added_on,";
		$project_task_man_power_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_task_man_power_update_uquery_set = trim($project_task_man_power_update_uquery_set,',');
	}

	$project_task_man_power_update_uquery = $project_task_man_power_update_uquery_base.$project_task_man_power_update_uquery_set.$project_task_man_power_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $project_task_man_power_update_ustatement = $dbConnection->prepare($project_task_man_power_update_uquery);

        $project_task_man_power_update_ustatement -> execute($project_task_man_power_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $man_power_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Machine Vendor Mapping
INPUT 	: Machine ID, Vendor ID, Remarks, Added By
OUTPUT 	: Mapping ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_machine_vendor_mapping($machine_id,$vendor_id,$remarks,$added_by)
{
	// Query
   $project_machine_vendor_mapping_iquery = "insert into project_machine_vendor_mapping
   (project_machine_vendor_mapping_machine_id,project_machine_vendor_mapping_vendor_id,project_machine_vendor_mapping_active,project_machine_vendor_mapping_remarks,
   project_machine_vendor_mapping_added_by,project_machine_vendor_mapping_added_on)
   values(:machine_id,:vendor_id,:active,:remarks,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_machine_vendor_mapping_istatement = $dbConnection->prepare($project_machine_vendor_mapping_iquery);

        // Data
        $project_machine_vendor_mapping_idata = array(':machine_id'=>$machine_id,':vendor_id'=>$vendor_id,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,
		':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_machine_vendor_mapping_istatement->execute($project_machine_vendor_mapping_idata);
		$project_machine_vendor_mapping_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_machine_vendor_mapping_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get project Machine Vendor Mapping list
INPUT 	: Mapping ID, Machine ID, Vendor ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of project Machine Vendor Mapping
BY 		: Lakshmi
*/
function db_get_project_machine_vendor_mapping($project_machine_vendor_mapping_search_data)
{

	if(array_key_exists("mapping_id",$project_machine_vendor_mapping_search_data))
	{
		$mapping_id = $project_machine_vendor_mapping_search_data["mapping_id"];
	}
	else
	{
		$mapping_id= "";
	}

	if(array_key_exists("machine_id",$project_machine_vendor_mapping_search_data))
	{
		$machine_id = $project_machine_vendor_mapping_search_data["machine_id"];
	}
	else
	{
		$machine_id = "";
	}

	if(array_key_exists("vendor_id",$project_machine_vendor_mapping_search_data))
	{
		$vendor_id = $project_machine_vendor_mapping_search_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}

	if(array_key_exists("active",$project_machine_vendor_mapping_search_data))
	{
		$active = $project_machine_vendor_mapping_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_machine_vendor_mapping_search_data))
	{
		$added_by = $project_machine_vendor_mapping_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_machine_vendor_mapping_search_data))
	{
		$start_date= $project_machine_vendor_mapping_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_machine_vendor_mapping_search_data))
	{
		$end_date= $project_machine_vendor_mapping_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_project_machine_vendor_mapping_list_squery_base = " select * from project_machine_vendor_mapping PMVM inner join users U on U.user_id = PMVM.project_machine_vendor_mapping_added_by inner join project_machine_master PMM on PMM.project_machine_master_id = PMVM.project_machine_vendor_mapping_machine_id inner join stock_vendor_master SVM on SVM.stock_vendor_id = PMVM.project_machine_vendor_mapping_vendor_id";

	$get_project_machine_vendor_mapping_list_squery_where = "";

	$filter_count = 0;

	// Data
	$get_project_machine_vendor_mapping_list_sdata = array();

	if($mapping_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_vendor_mapping_list_squery_where = $get_project_machine_vendor_mapping_list_squery_where." where project_machine_vendor_mapping_id = :mapping_id";
		}
		else
		{
			// Query
			$get_project_machine_vendor_mapping_list_squery_where = $get_project_machine_vendor_mapping_list_squery_where." and project_machine_vendor_mapping_id = :mapping_id";
		}

		// Data
		$get_project_machine_vendor_mapping_list_sdata[':mapping_id'] = $mapping_id;

		$filter_count++;
	}

	if($machine_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_vendor_mapping_list_squery_where = $get_project_machine_vendor_mapping_list_squery_where." where project_machine_vendor_mapping_machine_id = :machine_id";
		}
		else
		{
			// Query
			$get_project_machine_vendor_mapping_list_squery_where = $get_project_machine_vendor_mapping_list_squery_where." and project_machine_vendor_mapping_machine_id = :machine_id";
		}

		// Data
		$get_project_machine_vendor_mapping_list_sdata[':machine_id'] = $machine_id;

		$filter_count++;
	}

	if($vendor_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_vendor_mapping_list_squery_where = $get_project_machine_vendor_mapping_list_squery_where." where project_machine_vendor_mapping_vendor_id = :vendor_id";
		}
		else
		{
			// Query
			$get_project_machine_vendor_mapping_list_squery_where = $get_project_machine_vendor_mapping_list_squery_where." and project_machine_vendor_mapping_vendor_id = :vendor_id";
		}

		// Data
		$get_project_machine_vendor_mapping_list_sdata[':vendor_id'] = $vendor_id;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_vendor_mapping_list_squery_where = $get_project_machine_vendor_mapping_list_squery_where." where project_machine_vendor_mapping_active = :active";
		}
		else
		{
			// Query
			$get_project_machine_vendor_mapping_list_squery_where = $get_project_machine_vendor_mapping_list_squery_where." and project_machine_vendor_mapping_active = :active";
		}

		// Data
		$get_project_machine_vendor_mapping_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_vendor_mapping_list_squery_where = $get_project_machine_vendor_mapping_list_squery_where." where project_machine_vendor_mapping_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_machine_vendor_mapping_list_squery_where = $get_project_machine_vendor_mapping_list_squery_where." and project_machine_vendor_mapping_added_by = :added_by";
		}

		//Data
		$get_project_machine_vendor_mapping_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_vendor_mapping_list_squery_where = $get_project_machine_vendor_mapping_list_squery_where." where project_machine_vendor_mapping_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_machine_vendor_mapping_list_squery_where = $get_project_machine_vendor_mapping_list_squery_where." and project_machine_vendor_mapping_added_on >= :start_date";
		}

		//Data
		$get_project_machine_vendor_mapping_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_vendor_mapping_list_squery_where = $get_project_machine_vendor_mapping_list_squery_where." where project_machine_vendor_mapping_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_machine_vendor_mapping_list_squery_where = $get_project_machine_vendor_mapping_list_squery_where." and project_machine_vendor_mapping_added_on <= :end_date";
		}

		//Data
		$get_project_machine_vendor_mapping_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}


	$get_project_machine_vendor_mapping_list_squery = $get_project_machine_vendor_mapping_list_squery_base.$get_project_machine_vendor_mapping_list_squery_where;

	try
	{
		$dbConnection = get_conn_handle();

		$get_project_machine_vendor_mapping_list_sstatement = $dbConnection->prepare($get_project_machine_vendor_mapping_list_squery);

		$get_project_machine_vendor_mapping_list_sstatement -> execute($get_project_machine_vendor_mapping_list_sdata);

		$get_project_machine_vendor_mapping_list_sdetails = $get_project_machine_vendor_mapping_list_sstatement -> fetchAll();

		if(FALSE === $get_project_machine_vendor_mapping_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_machine_vendor_mapping_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_machine_vendor_mapping_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

 /*
PURPOSE : To update Project Machine Vendor Mapping
INPUT 	: Mapping ID, Project Machine Vendor Mapping Update Array
OUTPUT 	: Mapping ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_machine_vendor_mapping($mapping_id,$project_machine_vendor_mapping_update_data)
{
	if(array_key_exists("machine_id",$project_machine_vendor_mapping_update_data))
	{
		$machine_id = $project_machine_vendor_mapping_update_data["machine_id"];
	}
	else
	{
		$machine_id = "";
	}

	if(array_key_exists("vendor_id",$project_machine_vendor_mapping_update_data))
	{
		$vendor_id = $project_machine_vendor_mapping_update_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}

	if(array_key_exists("active",$project_machine_vendor_mapping_update_data))
	{
		$active = $project_machine_vendor_mapping_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_machine_vendor_mapping_update_data))
	{
		$remarks = $project_machine_vendor_mapping_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_machine_vendor_mapping_update_data))
	{
		$added_by = $project_machine_vendor_mapping_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_machine_vendor_mapping_update_data))
	{
		$added_on = $project_machine_vendor_mapping_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_machine_vendor_mapping_update_uquery_base = "update project_machine_vendor_mapping set";

	$project_machine_vendor_mapping_update_uquery_set = "";

	$project_machine_vendor_mapping_update_uquery_where = " where project_machine_vendor_mapping_id = :mapping_id";

	$project_machine_vendor_mapping_update_udata = array(":mapping_id"=>$mapping_id);

	$filter_count = 0;

	if($machine_id != "")
	{
		$project_machine_vendor_mapping_update_uquery_set = $project_machine_vendor_mapping_update_uquery_set." project_machine_vendor_mapping_machine_id = :machine_id,";
		$project_machine_vendor_mapping_update_udata[":machine_id"] = $machine_id;
		$filter_count++;
	}

	if($vendor_id != "")
	{
		$project_machine_vendor_mapping_update_uquery_set = $project_machine_vendor_mapping_update_uquery_set." project_machine_vendor_mapping_vendor_id = :vendor_id,";
		$project_machine_vendor_mapping_update_udata[":vendor_id"] = $vendor_id;
		$filter_count++;
	}

	if($active != "")
	{
		$project_machine_vendor_mapping_update_uquery_set = $project_machine_vendor_mapping_update_uquery_set." project_machine_vendor_mapping_active = :active,";
		$project_machine_vendor_mapping_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_machine_vendor_mapping_update_uquery_set = $project_machine_vendor_mapping_update_uquery_set." project_machine_vendor_mapping_remarks = :remarks,";
		$project_machine_vendor_mapping_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_machine_vendor_mapping_update_uquery_set = $project_machine_vendor_mapping_update_uquery_set." project_machine_vendor_mapping_added_by = :added_by,";
		$project_machine_vendor_mapping_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_machine_vendor_mapping_update_uquery_set = $project_machine_vendor_mapping_update_uquery_set." project_machine_vendor_mapping_added_on = :added_on,";
		$project_machine_vendor_mapping_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_machine_vendor_mapping_update_uquery_set = trim($project_machine_vendor_mapping_update_uquery_set,',');
	}

	$project_machine_vendor_mapping_update_uquery = $project_machine_vendor_mapping_update_uquery_base.$project_machine_vendor_mapping_update_uquery_set.$project_machine_vendor_mapping_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();

        $project_machine_vendor_mapping_update_ustatement = $dbConnection->prepare($project_machine_vendor_mapping_update_uquery);

        $project_machine_vendor_mapping_update_ustatement -> execute($project_machine_vendor_mapping_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $mapping_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Task Actual Man Power
INPUT 	: Task ID, Date, Road, Men, Women, Mason, Others, Agency, No Of Man Hours, Completion Percent, Remarks, Added By
OUTPUT 	: Man Power ID, success or failure message
BY 		: Lakshmi
*/
function db_add_man_power($task_id,$road_id,$date,$men,$women,$mason,$others,$agency,$men_rate,$women_rate,$mason_rate,$no_of_man_hours,$completion_percent,$work_type,$rework,$remarks,$added_by)
{
	// Query
   $man_power_iquery = "insert into project_task_actual_manpower
   (project_task_actual_manpower_task_id,project_task_actual_manpower_road_id,project_task_actual_manpower_date,project_task_actual_manpower_no_of_men,project_task_actual_manpower_no_of_women,
   project_task_actual_manpower_no_of_mason,project_task_actual_manpower_no_of_others,project_task_actual_manpower_agency,project_task_actual_manpower_men_rate,project_task_actual_manpower_women_rate,project_task_actual_manpower_mason_rate,project_task_actual_manpower_no_of_man_hours,
   project_task_actual_manpower_completion_percentage,project_task_actual_manpower_work_type,project_task_actual_manpower_rework_completion,project_task_actual_manpower_display_status,project_task_actual_manpower_active,project_task_actual_manpower_remarks,project_task_actual_manpower_added_by,
   project_task_actual_manpower_added_on)values(:task_id,:road_id,:date,:men,:women,:mason,:others,:agency,:men_rate,:women_rate,:mason_rate,:no_of_man_hours,:completion_percent,:work_type,:rework,:display_status,:active,:remarks,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $man_power_istatement = $dbConnection->prepare($man_power_iquery);

        // Data
        $man_power_idata = array(':task_id'=>$task_id,':road_id'=>$road_id,':date'=>$date,':men'=>$men,':women'=>$women,':mason'=>$mason,':others'=>$others,':agency'=>$agency,
		':men_rate'=>$men_rate,':women_rate'=>$women_rate,':mason_rate'=>$mason_rate,':no_of_man_hours'=>$no_of_man_hours,':completion_percent'=>$completion_percent,':work_type'=>$work_type,':rework'=>$rework,':display_status'=>'not approved',':active'=>'1',
		':remarks'=>$remarks,':added_by'=>$added_by,
		':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $man_power_istatement->execute($man_power_idata);
		$man_power_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $man_power_id;
    }
    catch(PDOException $e)
	{
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project Task Actual Man Power list
INPUT 	: man Power ID, Task ID, Date, Men, Women, Mason, Others, Agency, No Of Man Hours, Completion Percent, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Task Actual Man Power
BY 		: Lakshmi
*/
function db_get_man_power_list($man_power_search_data)
{
	if(array_key_exists("man_power_id",$man_power_search_data))
	{
		$man_power_id = $man_power_search_data["man_power_id"];
	}
	else
	{
		$man_power_id= "";
	}

	if(array_key_exists("task_id",$man_power_search_data))
	{
		$task_id = $man_power_search_data["task_id"];
	}
	else
	{
		$task_id= "";
	}

  if(array_key_exists("road_id",$man_power_search_data))
	{
		$road_id = $man_power_search_data["road_id"];
	}
	else
	{
		$road_id = "";
	}

	if(array_key_exists("date",$man_power_search_data))
	{
		$date = $man_power_search_data["date"];
	}
	else
	{
		$date= "";
	}

	if(array_key_exists("work_start_date",$man_power_search_data))
	{
		$work_start_date = $man_power_search_data["work_start_date"];
	}
	else
	{
		$work_start_date = "";
	}

	if(array_key_exists("work_end_date",$man_power_search_data))
	{
		$work_end_date = $man_power_search_data["work_end_date"];
	}
	else
	{
		$work_end_date = "";
	}

	if(array_key_exists("men",$man_power_search_data))
	{
		$men= $man_power_search_data["men"];
	}
	else
	{
		$men = "";
	}

	if(array_key_exists("women",$man_power_search_data))
	{
		$women= $man_power_search_data["women"];
	}
	else
	{
		$women = "";
	}

	if(array_key_exists("mason",$man_power_search_data))
	{
		$mason= $man_power_search_data["mason"];
	}
	else
	{
		$mason = "";
	}

	if(array_key_exists("others",$man_power_search_data))
	{
		$others= $man_power_search_data["others"];
	}
	else
	{
		$others = "";
	}

	if(array_key_exists("agency",$man_power_search_data))
	{
		$agency= $man_power_search_data["agency"];
	}
	else
	{
		$agency = "";
	}

	if(array_key_exists("no_of_man_hours",$man_power_search_data))
	{
		$no_of_man_hours= $man_power_search_data["no_of_man_hours"];
	}
	else
	{
		$no_of_man_hours = "";
	}

	if(array_key_exists("completion_percent",$man_power_search_data))
	{
		$completion_percent= $man_power_search_data["completion_percent"];
	}
	else
	{
		$completion_percent= "";
	}

	if(array_key_exists("display_status",$man_power_search_data))
	{
		$display_status= $man_power_search_data["display_status"];
	}
	else
	{
		$display_status= "";
	}
	if(array_key_exists("active",$man_power_search_data))
	{
		$active= $man_power_search_data["active"];
	}
	else
	{
		$active= "";
	}

	if(array_key_exists("added_by",$man_power_search_data))
	{
		$added_by= $man_power_search_data["added_by"];
	}
	else
	{
		$added_by= "";
	}

	if(array_key_exists("start_date",$man_power_search_data))
	{
		$start_date= $man_power_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	if(array_key_exists("end_date",$man_power_search_data))
	{
		$end_date= $man_power_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	if(array_key_exists("project",$man_power_search_data))
	{
		$project= $man_power_search_data["project"];
	}
	else
	{
		$project= "";
	}

	if(array_key_exists("process_id",$man_power_search_data))
	{
		$process_id= $man_power_search_data["process_id"];
	}
	else
	{
		$process_id= "";
	}

	if(array_key_exists("percentage_completion",$man_power_search_data))
	{
		$percentage_completion= $man_power_search_data["percentage_completion"];
	}
	else
	{
		$percentage_completion= "";
	}

	if(array_key_exists("work_type",$man_power_search_data))
	{
		$work_type= $man_power_search_data["work_type"];
	}
	else
	{
		$work_type= "";
	}

	if(array_key_exists("percentage_pending",$man_power_search_data))
	{
		$percentage_pending= $man_power_search_data["percentage_pending"];
	}
	else
	{
		$percentage_pending= "";
	}

	if(array_key_exists("max_percentage",$man_power_search_data))
	{
		$max_percentage= $man_power_search_data["max_percentage"];
	}
	else
	{
		$max_percentage= "";
	}


	$get_man_power_list_squery_base = "select *,U.user_name as user_name,AU.user_name as approved_by from project_task_actual_manpower PTMP inner join users U on U.user_id= PTMP.project_task_actual_manpower_added_by inner join project_plan_process_task PPPT on PPPT.project_process_task_id = PTMP.project_task_actual_manpower_task_id inner join project_manpower_agency PMA on PMA.project_manpower_agency_id=PTMP.project_task_actual_manpower_agency inner join project_task_master PTM on PTM.project_task_master_id=PPPT.project_process_task_type inner join project_plan_process PPP on PPP.project_plan_process_id=PPPT.project_process_id inner join project_plan PP on PP.project_plan_id=PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id=PP.project_plan_project_id inner join project_process_master PPM on PPM.project_process_master_id = PPP.project_plan_process_name left outer join users AU on AU.user_id = PTMP.project_task_actual_manpower_approved_by left outer join project_site_location_mapping_master PSLMM on PSLMM.project_site_location_mapping_master_id=PTMP.project_task_actual_manpower_road_id";

	$get_man_power_list_squery_where = "";


	$filter_count = 0;

	// Data
	$get_man_power_list_sdata = array();

	if($man_power_id != "")
	{
		if($filter_count == 0)
		{
			// Quer
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where project_task_actual_manpower_id = :man_power_id";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and project_task_actual_manpower_id = :man_power_id";
		}

		// Data
		$get_man_power_list_sdata[':man_power_id'] = $man_power_id;

		$filter_count++;
	}

	if($task_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where project_task_actual_manpower_task_id = :task_id";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and project_task_actual_manpower_task_id = :task_id";
		}

		// Data
		$get_man_power_list_sdata[':task_id'] = $task_id;

		$filter_count++;
	}

  if($road_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where project_task_actual_manpower_road_id = :road_id";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and project_task_actual_manpower_road_id = :road_id";
		}

		// Data
		$get_man_power_list_sdata[':road_id'] = $road_id;

		$filter_count++;
	}

	if($date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where project_task_actual_manpower_date = :date";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and project_task_actual_manpower_date = :date";
		}

		// Data
		$get_man_power_list_sdata[':date'] = $date;

		$filter_count++;
	}

	if($work_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where project_task_actual_manpower_date >= :work_start_date";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and project_task_actual_manpower_date >= :work_start_date";
		}

		//Data
		$get_man_power_list_sdata[':work_start_date']  = $work_start_date;

		$filter_count++;
	}

	if($work_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where project_task_actual_manpower_date <= :work_end_date";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and project_task_actual_manpower_date <= :work_end_date";
		}

		//Data
		$get_man_power_list_sdata[':work_end_date']  = $work_end_date;

		$filter_count++;
	}

	if($men != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where project_task_actual_manpower_no_of_men = :men";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and project_task_actual_manpower_no_of_men = :men";
		}

		// Data
		$get_man_power_list_sdata[':men']  = $men;

		$filter_count++;
	}

	if($women != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where project_task_actual_manpower_no_of_women = :women";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and project_task_actual_manpower_no_of_women = :women";
		}

		// Data
		$get_man_power_list_sdata[':women']  = $women;

		$filter_count++;
	}

	if($mason != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where project_task_actual_manpower_no_of_mason = :mason";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and project_task_actual_manpower_no_of_mason = :mason";
		}

		// Data
		$get_man_power_list_sdata[':mason']  = $mason;

		$filter_count++;
	}

	if($others != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where project_task_actual_manpower_no_of_others = :others";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and project_task_actual_manpower_no_of_others = :others";
		}

		// Data
		$get_man_power_list_sdata[':others']  = $others;

		$filter_count++;
	}

	if($agency != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where project_task_actual_manpower_agency = :agency";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and project_task_actual_manpower_agency = :agency";
		}

		// Data
		$get_man_power_list_sdata[':agency']  = $agency;

		$filter_count++;
	}

	if($no_of_man_hours != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where project_task_actual_manpower_no_of_man_hours = :no_of_man_hours";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and project_task_actual_manpower_no_of_man_hours = :no_of_man_hours";
		}

		// Data
		$get_man_power_list_sdata[':no_of_man_hours']  = $no_of_man_hours;

		$filter_count++;
	}
	if($completion_percent != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where project_task_actual_manpower_completion_percent = :completion_percent";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and project_task_actual_manpower_completion_percent = :completion_percent";
		}

		// Data
		$get_man_power_list_sdata[':completion_percent']  = $completion_percent;

		$filter_count++;
	}

	if($display_status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where project_task_actual_manpower_display_status = :display_status";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and project_task_actual_manpower_display_status = :display_status";
		}

		// Data
		$get_man_power_list_sdata[':display_status']  = $display_status;

		$filter_count++;
	}
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where project_task_actual_manpower_active = :active";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and project_task_actual_manpower_active = :active";
		}

		// Data
		$get_man_power_list_sdata[':active']  = $active;

		$filter_count++;
	}


	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where man_power_added_by = :added_by";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and man_power_added_by = :added_by";
		}

		//Data
		$get_man_power_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}


	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where project_task_actual_manpower_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and project_task_actual_manpower_added_on >= :start_date";
		}

		//Data
		$get_man_power_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where project_task_actual_manpower_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and project_task_actual_manpower_added_on <= :end_date";
		}

		//Data
		$get_man_power_list_sdata[':end_date']  = $end_date;

		$filter_count++;
	}

	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where PP.project_plan_project_id = :project";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and PP.project_plan_project_id = :project";
		}

		//Data
		$get_man_power_list_sdata['project']  = $project;

		$filter_count++;
	}

	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where PPPT.project_process_id = :process_id";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and PPPT.project_process_id = :process_id";
		}

		//Data
		$get_man_power_list_sdata['process_id']  = $process_id;

		$filter_count++;
	}

	if($percentage_completion != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where project_task_actual_manpower_completion_percentage = '100'";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and project_task_actual_manpower_completion_percentage = '100'";
		}

		$filter_count++;
	}

	if($work_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where project_task_actual_manpower_work_type = :work_type";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and project_task_actual_manpower_work_type = :work_type";
		}

		//Data
		$get_man_power_list_sdata['work_type']  = $work_type;

		$filter_count++;
	}

	if($percentage_pending != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where project_task_actual_manpower_completion_percentage < '100'";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and project_task_actual_manpower_completion_percentage < '100'";
		}

		$filter_count++;
	}
	if($max_percentage != "")
	{
		// Query
		$get_man_power_list_squery_base = "select max(project_task_actual_manpower_completion_percentage)as max_percentage from project_task_actual_manpower";
	}
	$get_man_power_list_order_by = " order by project_task_actual_manpower_added_on DESC";
	$get_man_power_list_squery = $get_man_power_list_squery_base.$get_man_power_list_squery_where.$get_man_power_list_order_by;

	try
	{
		$dbConnection = get_conn_handle();

		$get_man_power_list_sstatement = $dbConnection->prepare($get_man_power_list_squery);

		$get_man_power_list_sstatement -> execute($get_man_power_list_sdata);
    // ini_set('memory_limit', '-1');
		$get_man_power_list_sdetails = $get_man_power_list_sstatement -> fetchAll();

		if(FALSE === $get_man_power_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_man_power_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_man_power_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

/*
PURPOSE : To update Project Task Actual Man Power
INPUT 	: Man power ID, Man Power Update Array
OUTPUT 	: Man Power ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_man_power($man_power_id,$man_power_update_data)
{
	if(array_key_exists("task_id",$man_power_update_data))
	{
		$task_id = $man_power_update_data["task_id"];
	}
	else
	{
		$task_id = "";
	}

	if(array_key_exists("date",$man_power_update_data))
	{
		$date = $man_power_update_data["date"];
	}
	else
	{
		$date = "";
	}

	if(array_key_exists("men",$man_power_update_data))
	{
		$men = $man_power_update_data["men"];
	}
	else
	{
		$men = "";
	}

	if(array_key_exists("women",$man_power_update_data))
	{
		$women = $man_power_update_data["women"];
	}
	else
	{
		$women = "";
	}

	if(array_key_exists("mason",$man_power_update_data))
	{
		$mason = $man_power_update_data["mason"];
	}
	else
	{
		$mason = "";
	}

	if(array_key_exists("others",$man_power_update_data))
	{
		$others = $man_power_update_data["others"];
	}
	else
	{
		$others = "";
	}

	if(array_key_exists("agency",$man_power_update_data))
	{
		$agency = $man_power_update_data["agency"];
	}
	else
	{
		$agency = "";
	}

	if(array_key_exists("men_rate",$man_power_update_data))
	{
		$men_rate = $man_power_update_data["men_rate"];
	}
	else
	{
		$men_rate = "";
	}

	if(array_key_exists("women_rate",$man_power_update_data))
	{
		$women_rate = $man_power_update_data["women_rate"];
	}
	else
	{
		$women_rate = "";
	}

	if(array_key_exists("mason_rate",$man_power_update_data))
	{
		$mason_rate = $man_power_update_data["mason_rate"];
	}
	else
	{
		$mason_rate = "";
	}

	if(array_key_exists("no_of_man_hours",$man_power_update_data))
	{
		$no_of_man_hours = $man_power_update_data["no_of_man_hours"];
	}
	else
	{
		$no_of_man_hours = "";
	}

  if(array_key_exists("msmrt",$man_power_update_data))
  {
    $msmrt = $man_power_update_data["msmrt"];
  }
  else
  {
    $msmrt = "";
  }


	if(array_key_exists("completion_percent",$man_power_update_data))
	{
		$completion_percent = $man_power_update_data["completion_percent"];
	}
	else
	{
		$completion_percent = "";
	}

	if(array_key_exists("display_status",$man_power_update_data))
	{
		$display_status = $man_power_update_data["display_status"];
	}
	else
	{
		$display_status = "";
	}

	if(array_key_exists("check_status",$man_power_update_data))
	{
		$check_status = $man_power_update_data["check_status"];
	}
	else
	{
		$check_status = "";
	}

  if(array_key_exists("status1",$man_power_update_data))
  {
    $status1 = $man_power_update_data["status1"];
  }
  else
  {
    $status1 = "";
  }




	if(array_key_exists("completion",$man_power_update_data))
	{
		$completion = $man_power_update_data["completion"];
	}
	else
	{
		$completion = "";
	}

	if(array_key_exists("work_type",$man_power_update_data))
	{
		$work_type = $man_power_update_data["work_type"];
	}
	else
	{
		$work_type = "";
	}

	if(array_key_exists("rework_completion",$man_power_update_data))
	{
		$rework_completion = $man_power_update_data["rework_completion"];
	}
	else
	{
		$rework_completion = "";
	}

	if(array_key_exists("active",$man_power_update_data))
	{
		$active = $man_power_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$man_power_update_data))
	{
		$remarks = $man_power_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("checked_by",$man_power_update_data))
	{
		$checked_by = $man_power_update_data["checked_by"];
	}
	else
	{
		$checked_by = "";
	}

	if(array_key_exists("checked_on",$man_power_update_data))
	{
		$checked_on = $man_power_update_data["checked_on"];
	}
	else
	{
		$checked_on = "";
	}

	if(array_key_exists("approved_by",$man_power_update_data))
	{
		$approved_by = $man_power_update_data["approved_by"];
	}
	else
	{
		$approved_by = "";
	}

	if(array_key_exists("approved_on",$man_power_update_data))
	{
		$approved_on = $man_power_update_data["approved_on"];
	}
	else
	{
		$approved_on = "";
	}

	if(array_key_exists("added_by",$man_power_update_data))
	{
		$added_by = $man_power_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$man_power_update_data))
	{
		$added_on = $man_power_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $man_power_update_uquery_base = "update project_task_actual_manpower set";

	$man_power_update_uquery_set = "";

	$man_power_update_uquery_where = " where project_task_actual_manpower_id = :man_power_id";

	$man_power_update_udata = array(":man_power_id"=>$man_power_id);

	$filter_count = 0;

	if($task_id != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_task_id = :task_id,";
		$man_power_update_udata[":task_id"] = $task_id;
		$filter_count++;
	}

	if($date != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_date = :date,";
		$man_power_update_udata[":date"] = $date;
		$filter_count++;
	}

	if($men != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_no_of_men = :men,";
		$man_power_update_udata[":men"] = $men;
		$filter_count++;
	}

	if($women != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_no_of_women = :women,";
		$man_power_update_udata[":women"] = $women;
		$filter_count++;
	}

	if($mason != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_no_of_mason = :mason,";
		$man_power_update_udata[":mason"] = $mason;
		$filter_count++;
	}

	if($others != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_no_of_others = :others,";
		$man_power_update_udata[":others"] = $others;
		$filter_count++;
	}

	if($agency != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_agency = :agency,";
		$man_power_update_udata[":agency"] = $agency;
		$filter_count++;
	}

	if($men_rate != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_men_rate = :men_rate,";
		$man_power_update_udata[":men_rate"] = $men_rate;
		$filter_count++;
	}

	if($women_rate != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_women_rate = :women_rate,";
		$man_power_update_udata[":women_rate"] = $women_rate;
		$filter_count++;
	}

	if($mason_rate != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_mason_rate = :mason_rate,";
		$man_power_update_udata[":mason_rate"] = $mason_rate;
		$filter_count++;
	}

	if($no_of_man_hours != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_no_of_man_hours = :no_of_man_hours,";
		$man_power_update_udata[":no_of_man_hours"] = $no_of_man_hours;
		$filter_count++;
	}

  if($msmrt != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_completed_msmrt = :msmrt,";
		$man_power_update_udata[":msmrt"] = $msmrt;
		$filter_count++;
	}

	if($completion != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_completion = :completion,";
		$man_power_update_udata[":completion"] = $completion;
		$filter_count++;
	}

	if($completion_percent != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_completion_percentage = :completion_percent,";
		$man_power_update_udata[":completion_percent"] = $completion_percent;
		$filter_count++;
	}

	if($work_type != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_work_type = :work_type,";
		$man_power_update_udata[":work_type"] = $work_type;
		$filter_count++;
	}

	if($rework_completion != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_rework_completion = :rework_completion,";
		$man_power_update_udata[":rework_completion"] = $rework_completion;
		$filter_count++;
	}

	if($display_status != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_display_status = :display_status,";
		$man_power_update_udata[":display_status"] = $display_status;
		$filter_count++;
	}

	if($check_status != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_check_status = :check_status,";
		$man_power_update_udata[":check_status"] = $check_status;
		$filter_count++;
	}

  if($status1 != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_status = :status1,";
		$man_power_update_udata[":status1"] = $status1;
		$filter_count++;
	}

	if($active != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_active = :active,";
		$man_power_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_remarks = :remarks,";
		$man_power_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($checked_by != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_checked_by = :checked_by,";
		$man_power_update_udata[":checked_by"] = $checked_by;
		$filter_count++;
	}

	if($checked_on != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_checked_on = :checked_on,";
		$man_power_update_udata[":checked_on"] = $checked_on;
		$filter_count++;
	}

	if($approved_by != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_approved_by = :approved_by,";
		$man_power_update_udata[":approved_by"] = $approved_by;
		$filter_count++;
	}

	if($approved_on != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_approved_on = :approved_on,";
		$man_power_update_udata[":approved_on"] = $approved_on;
		$filter_count++;
	}

	if($added_by != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_added_by = :added_by,";
		$man_power_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_added_on = :added_on,";
		$man_power_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$man_power_update_uquery_set = trim($man_power_update_uquery_set,',');
	}

	$man_power_update_uquery = $man_power_update_uquery_base.$man_power_update_uquery_set.$man_power_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $man_power_update_ustatement = $dbConnection->prepare($man_power_update_uquery);

        $man_power_update_ustatement -> execute($man_power_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $man_power_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	return $return;
}/*
PURPOSE : To add new Project Man Power Estimate
INPUT 	:  Task ID, Date, No of Men, No of Women, No of Mason, No of Others, Agency, Display Status, Remarks, Added By
OUTPUT 	: Estimate ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_man_power_estimate($task_id,$date,$no_of_men,$no_of_women,$no_of_mason,$no_of_others,$agency,$remarks,$added_by)
{
	// Query
   $project_man_power_estimate_iquery = "insert into project_man_power_estimate
(project_man_power_estimate_task_id,project_man_power_estimate_date,project_man_power_estimate_no_of_men,project_man_power_estimate_no_of_women,project_man_power_estimate_no_of_mason,project_man_power_estimate_no_of_others,project_man_power_estimate_agency,project_man_power_estimate_display_status,project_man_power_estimate_active,project_man_power_estimate_remarks,project_man_power_estimate_added_by,project_man_power_estimate_added_on)
values(:task_id,:date,:no_of_men,:no_of_women,:no_of_mason,:no_of_others,:agency,:display_status,:active,:remarks,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_man_power_estimate_istatement = $dbConnection->prepare($project_man_power_estimate_iquery);

        // Data
        $project_man_power_estimate_idata = array(':task_id'=>$task_id,':date'=>$date,':no_of_men'=>$no_of_men,':no_of_women'=>$no_of_women,':no_of_mason'=>$no_of_mason,':no_of_others'=>$no_of_others,':agency'=>$agency,':display_status'=>'not approved',':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_man_power_estimate_istatement->execute($project_man_power_estimate_idata);
		$project_man_power_estimate_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_man_power_estimate_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get project Man Power Estimate list
INPUT 	: Estimate ID, Task ID, Date, No of Men, No of Women, No of Mason, No of Others, Agency, Display Status, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of project Man Power Estimate
BY 		: Lakshmi
*/
function db_get_project_man_power_estimate($project_man_power_estimate_search_data)
{

	if(array_key_exists("estimate_id",$project_man_power_estimate_search_data))
	{
		$estimate_id = $project_man_power_estimate_search_data["estimate_id"];
	}
	else
	{
		$estimate_id= "";
	}

	if(array_key_exists("task_id",$project_man_power_estimate_search_data))
	{
		$task_id = $project_man_power_estimate_search_data["task_id"];
	}
	else
	{
		$task_id= "";
	}

	if(array_key_exists("date",$project_man_power_estimate_search_data))
	{
		$date = $project_man_power_estimate_search_data["date"];
	}
	else
	{
		$date= "";
	}

	if(array_key_exists("no_of_men",$project_man_power_estimate_search_data))
	{
		$no_of_men = $project_man_power_estimate_search_data["no_of_men"];
	}
	else
	{
		$no_of_men= "";
	}

	if(array_key_exists("no_of_women",$project_man_power_estimate_search_data))
	{
		$no_of_women = $project_man_power_estimate_search_data["no_of_women"];
	}
	else
	{
		$no_of_women= "";
	}

	if(array_key_exists("no_of_mason",$project_man_power_estimate_search_data))
	{
		$no_of_mason = $project_man_power_estimate_search_data["no_of_mason"];
	}
	else
	{
		$no_of_mason= "";
	}

	if(array_key_exists("no_of_others",$project_man_power_estimate_search_data))
	{
		$no_of_others = $project_man_power_estimate_search_data["no_of_others"];
	}
	else
	{
		$no_of_others= "";
	}

	if(array_key_exists("agency",$project_man_power_estimate_search_data))
	{
		$agency = $project_man_power_estimate_search_data["agency"];
	}
	else
	{
		$agency= "";
	}

	if(array_key_exists("process_id",$project_man_power_estimate_search_data))
	{
		$process_id = $project_man_power_estimate_search_data["process_id"];
	}
	else
	{
		$process_id= "";
	}
	if(array_key_exists("display_status",$project_man_power_estimate_search_data))
	{
		$display_status = $project_man_power_estimate_search_data["display_status"];
	}
	else
	{
		$display_status= "";
	}

	if(array_key_exists("active",$project_man_power_estimate_search_data))
	{
		$active = $project_man_power_estimate_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_man_power_estimate_search_data))
	{
		$added_by = $project_man_power_estimate_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_man_power_estimate_search_data))
	{
		$start_date= $project_man_power_estimate_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_man_power_estimate_search_data))
	{
		$end_date= $project_man_power_estimate_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	if(array_key_exists("project",$project_man_power_estimate_search_data))
	{
		$project= $project_man_power_estimate_search_data["project"];
	}
	else
	{
		$project= "";
	}

	if(array_key_exists("process",$project_man_power_estimate_search_data))
	{
		$process= $project_man_power_estimate_search_data["process"];
	}
	else
	{
		$process= "";
	}

	if(array_key_exists("task",$project_man_power_estimate_search_data))
	{
		$task= $project_man_power_estimate_search_data["task"];
	}
	else
	{
		$task= "";
	}

	$get_project_man_power_estimate_list_squery_base = " select * from project_man_power_estimate PMPE inner join users U on U.user_id= PMPE.project_man_power_estimate_added_by inner join project_plan_process_task PPPT on PPPT.project_process_task_id = PMPE.project_man_power_estimate_task_id inner join project_task_master PTM on PTM.project_task_master_id=PPPT.project_process_task_type inner join project_plan_process PPP on PPP.project_plan_process_id=PPPT.project_process_id inner join project_plan PP on PP.project_plan_id=PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id=PP.project_plan_project_id inner join project_process_master PPM on PPM.project_process_master_id = PPP.project_plan_process_name";

	$get_project_man_power_estimate_list_squery_where = "";

	$filter_count = 0;

	// Data
	$get_project_man_power_estimate_list_sdata = array();

	if($estimate_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." where project_man_power_estimate_id = :estimate_id";
		}
		else
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." and project_man_power_estimate_id = :estimate_id";
		}

		// Data
		$get_project_man_power_estimate_list_sdata[':estimate_id'] = $estimate_id;

		$filter_count++;
	}

	if($task_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." where project_man_power_estimate_task_id = :task_id";
		}
		else
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." and project_man_power_estimate_task_id = :task_id";
		}

		// Data
		$get_project_man_power_estimate_list_sdata[':task_id'] = $task_id;

		$filter_count++;
	}

	if($date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." where project_man_power_estimate_date = :date";
		}
		else
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." and project_man_power_estimate_date = :date";
		}

		// Data
		$get_project_man_power_estimate_list_sdata[':date'] = $date;

		$filter_count++;
	}

	if($no_of_men != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." where project_man_power_estimate_no_of_men = :no_of_men";
		}
		else
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." and project_man_power_estimate_no_of_men = :no_of_men";
		}

		// Data
		$get_project_man_power_estimate_list_sdata[':no_of_men'] = $no_of_men;

		$filter_count++;
	}

	if($no_of_women != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." where project_man_power_estimate_no_of_women = :no_of_women";
		}
		else
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." and project_man_power_estimate_no_of_women = :no_of_women";
		}

		// Data
		$get_project_man_power_estimate_list_sdata[':no_of_women'] = $no_of_women;

		$filter_count++;
	}

	if($no_of_mason != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." where project_man_power_estimate_no_of_mason = :no_of_mason";
		}
		else
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." and project_man_power_estimate_no_of_mason = :no_of_mason";
		}

		// Data
		$get_project_man_power_estimate_list_sdata[':no_of_mason'] = $no_of_mason;

		$filter_count++;
	}

	if($no_of_others != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." where project_man_power_estimate_no_of_others = :no_of_others";
		}
		else
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." and project_man_power_estimate_no_of_others = :no_of_others";
		}

		// Data
		$get_project_man_power_estimate_list_sdata[':no_of_others'] = $no_of_others;

		$filter_count++;
	}

	if($agency != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." where project_man_power_estimate_agency = :agency";
		}
		else
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." and project_man_power_estimate_agency = :agency";
		}

		// Data
		$get_project_man_power_estimate_list_sdata[':agency'] = $agency;

		$filter_count++;
	}

	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." where PPPT.project_process_id = :process_id";
		}
		else
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." and PPPT.project_process_id  = :process_id";
		}

		// Data
		$get_project_man_power_estimate_list_sdata[':process_id'] = $process_id;

		$filter_count++;
	}

	if($process != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." where PPM.project_process_master_id = :process";
		}
		else
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." and PPM.project_process_master_id  = :process";
		}

		// Data
		$get_project_man_power_estimate_list_sdata[':process'] = $process;

		$filter_count++;
	}

	if($task != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." where PTM.project_task_master_id = :task";
		}
		else
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." and PTM.project_task_master_id  = :task";
		}

		// Data
		$get_project_man_power_estimate_list_sdata[':task'] = $task;

		$filter_count++;
	}

	if($display_status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." where project_man_power_estimate_display_status = :display_status";
		}
		else
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." and project_man_power_estimate_display_status = :display_status";
		}

		// Data
		$get_project_man_power_estimate_list_sdata[':display_status'] = $display_status;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." where project_man_power_estimate_active = :active";
		}
		else
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." and project_man_power_estimate_active = :active";
		}

		// Data
		$get_project_man_power_estimate_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." where project_man_power_estimate_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." and project_man_power_estimate_added_by = :added_by";
		}

		//Data
		$get_project_man_power_estimate_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." where project_man_power_estimate_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." and project_man_power_estimate_added_on >= :start_date";
		}

		//Data
		$get_project_man_power_estimate_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." where project_man_power_estimate_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." and project_man_power_estimate_added_on <= :end_date";
		}

		//Data
		$get_project_man_power_estimate_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." where PP.project_plan_project_id = :project";
		}
		else
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." and PP.project_plan_project_id = :project";
		}

		//Data
		$get_project_man_power_estimate_list_sdata['project']  = $project;

		$filter_count++;
	}


	$get_project_man_power_estimate_list_squery = $get_project_man_power_estimate_list_squery_base.$get_project_man_power_estimate_list_squery_where;


	try
	{
		$dbConnection = get_conn_handle();

		$get_project_man_power_estimate_list_sstatement = $dbConnection->prepare($get_project_man_power_estimate_list_squery);

		$get_project_man_power_estimate_list_sstatement -> execute($get_project_man_power_estimate_list_sdata);

		$get_project_man_power_estimate_list_sdetails = $get_project_man_power_estimate_list_sstatement -> fetchAll();

		if(FALSE === $get_project_man_power_estimate_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_man_power_estimate_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_man_power_estimate_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

  /*
PURPOSE : To update Project Man Power Estimate
INPUT 	: Estimate ID, Project Man Power Estimate Update Array
OUTPUT 	: Estimate ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_man_power_estimate($estimate_id,$task_id,$project_man_power_estimate_update_data)
{

	if(array_key_exists("date",$project_man_power_estimate_update_data))
	{
		$date = $project_man_power_estimate_update_data["date"];
	}
	else
	{
		$date = "";
	}

	if(array_key_exists("no_of_men",$project_man_power_estimate_update_data))
	{
		$no_of_men = $project_man_power_estimate_update_data["no_of_men"];
	}
	else
	{
		$no_of_men = "";
	}

	if(array_key_exists("no_of_women",$project_man_power_estimate_update_data))
	{
		$no_of_women = $project_man_power_estimate_update_data["no_of_women"];
	}
	else
	{
		$no_of_women = "";
	}

	if(array_key_exists("no_of_mason",$project_man_power_estimate_update_data))
	{
		$no_of_mason = $project_man_power_estimate_update_data["no_of_mason"];
	}
	else
	{
		$no_of_mason = "";
	}

	if(array_key_exists("no_of_others",$project_man_power_estimate_update_data))
	{
		$no_of_others = $project_man_power_estimate_update_data["no_of_others"];
	}
	else
	{
		$no_of_others = "";
	}

	if(array_key_exists("agency",$project_man_power_estimate_update_data))
	{
		$agency = $project_man_power_estimate_update_data["agency"];
	}
	else
	{
		$agency = "";
	}
	if(array_key_exists("display_status",$project_man_power_estimate_update_data))
	{
		$display_status = $project_man_power_estimate_update_data["display_status"];
	}
	else
	{
		$display_status = "";
	}

	if(array_key_exists("active",$project_man_power_estimate_update_data))
	{
		$active = $project_man_power_estimate_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_man_power_estimate_update_data))
	{
		$remarks = $project_man_power_estimate_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_man_power_estimate_update_data))
	{
		$added_by = $project_man_power_estimate_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_man_power_estimate_update_data))
	{
		$added_on = $project_man_power_estimate_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_man_power_estimate_update_uquery_base = "update project_man_power_estimate set";

	$project_man_power_estimate_update_uquery_set = "";

	$project_man_power_estimate_update_uquery_where = " where project_man_power_estimate_id = :estimate_id or project_man_power_estimate_task_id = :task_id";

	$project_man_power_estimate_update_udata = array(":estimate_id"=>$estimate_id,":task_id"=>$task_id);

	$filter_count = 0;
	if($date != "")
	{
		$project_man_power_estimate_update_uquery_set = $project_man_power_estimate_update_uquery_set." project_man_power_estimate_date = :date,";
		$project_man_power_estimate_update_udata[":date"] = $date;
		$filter_count++;
	}
	if($no_of_men != "")
	{
		$project_man_power_estimate_update_uquery_set = $project_man_power_estimate_update_uquery_set." project_man_power_estimate_no_of_men= :no_of_men,";
		$project_man_power_estimate_update_udata[":no_of_men"] = $no_of_men;
		$filter_count++;
	}
	if($no_of_women != "")
	{
		$project_man_power_estimate_update_uquery_set = $project_man_power_estimate_update_uquery_set." project_man_power_estimate_no_of_women = :no_of_women,";
		$project_man_power_estimate_update_udata[":no_of_women"] = $no_of_women;
		$filter_count++;
	}
	if($no_of_mason != "")
	{
		$project_man_power_estimate_update_uquery_set = $project_man_power_estimate_update_uquery_set." project_man_power_estimate_no_of_mason = :no_of_mason,";
		$project_man_power_estimate_update_udata[":no_of_mason"] = $no_of_mason;
		$filter_count++;
	}
	if($no_of_others != "")
	{
		$project_man_power_estimate_update_uquery_set = $project_man_power_estimate_update_uquery_set." project_man_power_estimate_no_of_others = :no_of_others,";
		$project_man_power_estimate_update_udata[":no_of_others"] = $no_of_others;
		$filter_count++;
	}
	if($agency != "")
	{
		$project_man_power_estimate_update_uquery_set = $project_man_power_estimate_update_uquery_set." project_man_power_estimate_agency = :agency,";
		$project_man_power_estimate_update_udata[":agency"] = $agency;
		$filter_count++;
	}
	if($display_status != "")
	{
		$project_man_power_estimate_update_uquery_set = $project_man_power_estimate_update_uquery_set." project_man_power_estimate_display_status = :display_status,";
		$project_man_power_estimate_update_udata[":display_status"] = $display_status;
		$filter_count++;
	}

	if($active != "")
	{
		$project_man_power_estimate_update_uquery_set = $project_man_power_estimate_update_uquery_set." project_man_power_estimate_active = :active,";
		$project_man_power_estimate_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_man_power_estimate_update_uquery_set = $project_man_power_estimate_update_uquery_set." project_man_power_estimate_remarks = :remarks,";
		$project_man_power_estimate_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_man_power_estimate_update_uquery_set = $project_man_power_estimate_update_uquery_set." project_man_power_estimate_added_by = :added_by,";
		$project_man_power_estimate_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_man_power_estimate_update_uquery_set = $project_man_power_estimate_update_uquery_set." project_man_power_estimate_added_on = :added_on,";
		$project_man_power_estimate_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_man_power_estimate_update_uquery_set = trim($project_man_power_estimate_update_uquery_set,',');
	}

	$project_man_power_estimate_update_uquery = $project_man_power_estimate_update_uquery_base.$project_man_power_estimate_update_uquery_set.$project_man_power_estimate_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();

        $project_man_power_estimate_update_ustatement = $dbConnection->prepare($project_man_power_estimate_update_uquery);

        $project_man_power_estimate_update_ustatement -> execute($project_man_power_estimate_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $estimate_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project User Mapping
INPUT 	: Project ID, User ID, Remarks, Added By
OUTPUT 	: Mapping ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_user_mapping($project_id,$user_id,$remarks,$added_by)
{
	// Query
   $project_user_mapping_iquery = "insert into project_user_mapping
   (project_user_mapping_project_id,project_user_mapping_user_id,project_user_mapping_status,project_user_mapping_active,project_user_mapping_remarks,project_user_mapping_added_by,
   project_user_mapping_added_on)values(:project_id,:user_id,:status,:active,:remarks,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_user_mapping_istatement = $dbConnection->prepare($project_user_mapping_iquery);

        // Data
        $project_user_mapping_idata = array(':project_id'=>$project_id,':user_id'=>$user_id,':status'=>'0',':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,
		':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_user_mapping_istatement->execute($project_user_mapping_idata);
		$project_user_mapping_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_user_mapping_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project  User Mapping list
INPUT 	: Mapping ID, Project ID, User ID, Status, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project User Mapping
BY 		: Lakshmi
*/
function db_get_project_user_mapping($project_user_mapping_search_data)
{

	if(array_key_exists("mapping_id",$project_user_mapping_search_data))
	{
		$mapping_id = $project_user_mapping_search_data["mapping_id"];
	}
	else
	{
		$mapping_id= "";
	}

	if(array_key_exists("project_id",$project_user_mapping_search_data))
	{
		$project_id = $project_user_mapping_search_data["project_id"];
	}
	else
	{
		$project_id = "";
	}

	if(array_key_exists("user_id",$project_user_mapping_search_data))
	{
		$user_id = $project_user_mapping_search_data["user_id"];
	}
	else
	{
		$user_id = "";
	}

	if(array_key_exists("status",$project_user_mapping_search_data))
	{
		$status = $project_user_mapping_search_data["status"];
	}
	else
	{
		$status = "";
	}

	if(array_key_exists("active",$project_user_mapping_search_data))
	{
		$active = $project_user_mapping_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_user_mapping_search_data))
	{
		$added_by = $project_user_mapping_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_user_mapping_search_data))
	{
		$start_date= $project_user_mapping_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_user_mapping_search_data))
	{
		$end_date= $project_user_mapping_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_project_user_mapping_list_squery_base = " select *,U.user_name as added_by,AU.user_name as project_user from project_user_mapping PUMM inner join users U on U.user_id = PUMM.project_user_mapping_added_by inner join users AU on AU.user_id = PUMM.project_user_mapping_user_id inner join project_management_project_master PMPM on PMPM.project_management_master_id = PUMM.project_user_mapping_project_id";

	$get_project_user_mapping_list_squery_where = "";
	$get_project_user_mapping_list_squery_order_by = " order by project_user_mapping_project_id ASC";

	$filter_count = 0;

	// Data
	$get_project_user_mapping_list_sdata = array();

	if($mapping_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_user_mapping_list_squery_where = $get_project_user_mapping_list_squery_where." where project_user_mapping_id = :mapping_id";
		}
		else
		{
			// Query
			$get_project_user_mapping_list_squery_where = $get_project_user_mapping_list_squery_where." and project_user_mapping_id = :mapping_id";
		}

		// Data
		$get_project_user_mapping_list_sdata[':mapping_id'] = $mapping_id;

		$filter_count++;
	}

	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_user_mapping_list_squery_where = $get_project_user_mapping_list_squery_where." where project_user_mapping_project_id = :project_id";
		}
		else
		{
			// Query
			$get_project_user_mapping_list_squery_where = $get_project_user_mapping_list_squery_where." and project_user_mapping_project_id = :project_id";
		}

		// Data
		$get_project_user_mapping_list_sdata[':project_id'] = $project_id;

		$filter_count++;
	}

	if($user_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_user_mapping_list_squery_where = $get_project_user_mapping_list_squery_where." where project_user_mapping_user_id = :user_id";
		}
		else
		{
			// Query
			$get_project_user_mapping_list_squery_where = $get_project_user_mapping_list_squery_where." and project_user_mapping_user_id = :user_id";
		}

		// Data
		$get_project_user_mapping_list_sdata[':user_id'] = $user_id;

		$filter_count++;
	}

	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_user_mapping_list_squery_where = $get_project_user_mapping_list_squery_where." where project_user_mapping_status = :status";
		}
		else
		{
			// Query
			$get_project_user_mapping_list_squery_where = $get_project_user_mapping_list_squery_where." and project_user_mapping_status = :status";
		}

		// Data
		$get_project_user_mapping_list_sdata[':status'] = $status;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_user_mapping_list_squery_where = $get_project_user_mapping_list_squery_where." where project_user_mapping_active = :active";
		}
		else
		{
			// Query
			$get_project_user_mapping_list_squery_where = $get_project_user_mapping_list_squery_where." and project_user_mapping_active = :active";
		}

		// Data
		$get_project_user_mapping_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_user_mapping_list_squery_where = $get_project_user_mapping_list_squery_where." where project_user_mapping_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_user_mapping_list_squery_where = $get_project_user_mapping_list_squery_where." and project_user_mapping_added_by = :added_by";
		}

		//Data
		$get_project_user_mapping_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_user_mapping_list_squery_where = $get_project_user_mapping_list_squery_where." where project_user_mapping_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_user_mapping_list_squery_where = $get_project_user_mapping_list_squery_where." and project_user_mapping_added_on >= :start_date";
		}

		//Data
		$get_project_user_mapping_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_user_mapping_list_squery_where = $get_project_user_mapping_list_squery_where." where project_user_mapping_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_user_mapping_list_squery_where = $get_project_user_mapping_list_squery_where." and project_user_mapping_added_on <= :end_date";
		}

		//Data
		$get_project_user_mapping_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	$get_project_user_mapping_list_squery = $get_project_user_mapping_list_squery_base.$get_project_user_mapping_list_squery_where.$get_project_user_mapping_list_squery_order_by;
	try
	{
		$dbConnection = get_conn_handle();

		$get_project_user_mapping_list_sstatement = $dbConnection->prepare($get_project_user_mapping_list_squery);

		$get_project_user_mapping_list_sstatement -> execute($get_project_user_mapping_list_sdata);

		$get_project_user_mapping_list_sdetails = $get_project_user_mapping_list_sstatement -> fetchAll();

		if(FALSE === $get_project_user_mapping_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_user_mapping_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_user_mapping_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

  /*
PURPOSE : To update Project User Mapping
INPUT 	: Mapping ID, Project User Mapping Update Array
OUTPUT 	: Mapping ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_user_mapping($mapping_id,$project_user_mapping_update_data)
{
	if(array_key_exists("project_id",$project_user_mapping_update_data))
	{
		$project_id = $project_user_mapping_update_data["project_id"];
	}
	else
	{
		$project_id = "";
	}

	if(array_key_exists("user_id",$project_user_mapping_update_data))
	{
		$user_id = $project_user_mapping_update_data["user_id"];
	}
	else
	{
		$user_id = "";
	}

	if(array_key_exists("status",$project_user_mapping_update_data))
	{
		$status = $project_user_mapping_update_data["status"];
	}
	else
	{
		$status = "";
	}

	if(array_key_exists("active",$project_user_mapping_update_data))
	{
		$active = $project_user_mapping_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_user_mapping_update_data))
	{
		$remarks = $project_user_mapping_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_user_mapping_update_data))
	{
		$added_by = $project_user_mapping_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_user_mapping_update_data))
	{
		$added_on = $project_user_mapping_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_user_mapping_update_uquery_base = "update project_user_mapping set";

	$project_user_mapping_update_uquery_set = "";

	$project_user_mapping_update_uquery_where = " where project_user_mapping_id = :mapping_id";

	$project_user_mapping_update_udata = array(":mapping_id"=>$mapping_id);

	$filter_count = 0;

	if($project_id != "")
	{
		$project_user_mapping_update_uquery_set = $project_user_mapping_update_uquery_set." project_user_mapping_project_id = :project_id,";
		$project_user_mapping_update_udata[":project_id"] = $project_id;
		$filter_count++;
	}

	if($user_id != "")
	{
		$project_user_mapping_update_uquery_set = $project_user_mapping_update_uquery_set." project_user_mapping_user_id = :user_id,";
		$project_user_mapping_update_udata[":user_id"] = $user_id;
		$filter_count++;
	}

	if($status != "")
	{
		$project_user_mapping_update_uquery_set = $project_user_mapping_update_uquery_set." project_user_mapping_status = :status,";
		$project_user_mapping_update_udata[":status"] = $status;
		$filter_count++;
	}

	if($active != "")
	{
		$project_user_mapping_update_uquery_set = $project_user_mapping_update_uquery_set." project_user_mapping_active = :active,";
		$project_user_mapping_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_user_mapping_update_uquery_set = $project_user_mapping_update_uquery_set." project_user_mapping_remarks = :remarks,";
		$project_user_mapping_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_user_mapping_update_uquery_set = $project_user_mapping_update_uquery_set." project_user_mapping_added_by = :added_by,";
		$project_user_mapping_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_user_mapping_update_uquery_set = $project_user_mapping_update_uquery_set." project_user_mapping_added_on = :added_on,";
		$project_user_mapping_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_user_mapping_update_uquery_set = trim($project_user_mapping_update_uquery_set,',');
	}

	$project_user_mapping_update_uquery = $project_user_mapping_update_uquery_base.$project_user_mapping_update_uquery_set.$project_user_mapping_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $project_user_mapping_update_ustatement = $dbConnection->prepare($project_user_mapping_update_uquery);

        $project_user_mapping_update_ustatement -> execute($project_user_mapping_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $mapping_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Machine Planning
INPUT 	: Task ID, Machine ID, Machine Rate, No of Hours, Additional Cost, Remarks, Added By
OUTPUT 	: Planning ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_machine_planning($task_id,$machine_id,$machine_rate,$no_of_hours,$additional_cost,$remarks,$added_by)
{
	// Query
   $project_machine_planning_iquery = "insert into project_machine_planning
   (project_machine_planning_task_id,project_machine_planning_machine_id,project_machine_planning_machine_rate,project_machine_planning_no_of_hours,
   project_machine_planning_additional_cost,project_machine_planning_active,project_machine_planning_remarks,project_machine_planning_added_by,project_machine_planning_added_on)
   values(:task_id,:machine_id,:machine_rate,:no_of_hours,:additional_cost,:active,:remarks,:added_by,:added_on)";
    try
    {
        $dbConnection = get_conn_handle();
        $project_machine_planning_istatement = $dbConnection->prepare($project_machine_planning_iquery);

        // Data
        $project_machine_planning_idata = array(':task_id'=>$task_id,':machine_id'=>$machine_id,':machine_rate'=>$machine_rate,':no_of_hours'=>$no_of_hours,
		':additional_cost'=>$additional_cost,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_machine_planning_istatement->execute($project_machine_planning_idata);
		$project_machine_planning_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_machine_planning_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project Machine Planning list
INPUT 	: Planning ID, Task ID, Machine ID, Machine Rate, No of Hours, Additional Cost, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Machine Planning
BY 		: Lakshmi
*/
function db_get_project_machine_planning($project_machine_planning_search_data)
{
	if(array_key_exists("planning_id",$project_machine_planning_search_data))
	{
		$planning_id = $project_machine_planning_search_data["planning_id"];
	}
	else
	{
		$planning_id= "";
	}

	if(array_key_exists("task_id",$project_machine_planning_search_data))
	{
		$task_id = $project_machine_planning_search_data["task_id"];
	}
	else
	{
		$task_id = "";
	}

	if(array_key_exists("process_id",$project_machine_planning_search_data))
	{
		$process_id = $project_machine_planning_search_data["process_id"];
	}
	else
	{
		$process_id = "";
	}

	if(array_key_exists("project_id",$project_machine_planning_search_data))
	{
		$project_id = $project_machine_planning_search_data["project_id"];
	}
	else
	{
		$project_id = "";
	}


	if(array_key_exists("machine_id",$project_machine_planning_search_data))
	{
		$machine_id = $project_machine_planning_search_data["machine_id"];
	}
	else
	{
		$machine_id = "";
	}

	if(array_key_exists("machine_rate",$project_machine_planning_search_data))
	{
		$machine_rate = $project_machine_planning_search_data["machine_rate"];
	}
	else
	{
		$machine_rate = "";
	}

	if(array_key_exists("no_of_hours",$project_machine_planning_search_data))
	{
		$no_of_hours = $project_machine_planning_search_data["no_of_hours"];
	}
	else
	{
		$no_of_hours = "";
	}

	if(array_key_exists("additional_cost",$project_machine_planning_search_data))
	{
		$additional_cost = $project_machine_planning_search_data["additional_cost"];
	}
	else
	{
		$additional_cost = "";
	}

	if(array_key_exists("active",$project_machine_planning_search_data))
	{
		$active = $project_machine_planning_search_data["active"];
	}
	else
	{
		$active = "";
	}


	if(array_key_exists("added_by",$project_machine_planning_search_data))
	{
		$added_by = $project_machine_planning_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_machine_planning_search_data))
	{
		$start_date= $project_machine_planning_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_machine_planning_search_data))
	{
		$end_date= $project_machine_planning_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	if(array_key_exists("process",$project_machine_planning_search_data))
	{
		$process = $project_machine_planning_search_data["process"];
	}
	else
	{
		$process = "";
	}

	if(array_key_exists("task",$project_machine_planning_search_data))
	{
		$task = $project_machine_planning_search_data["task"];
	}
	else
	{
		$task = "";
	}

	$get_project_machine_planning_list_squery_base = " select * from project_machine_planning PMP inner join users U on U.user_id=PMP.project_machine_planning_added_by inner join project_machine_type_master PMTM on PMTM.project_machine_type_master_id=PMP.project_machine_planning_machine_id inner join project_plan_process_task PPPT on PPPT.project_process_task_id = PMP.project_machine_planning_task_id inner join project_task_master PTM on PTM.project_task_master_id=PPPT.project_process_task_type inner join project_plan_process PPP on PPP.project_plan_process_id=PPPT.project_process_id inner join project_plan PP on PP.project_plan_id=PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id=PP.project_plan_project_id inner join project_process_master PPM on PPM.project_process_master_id = PPP.project_plan_process_name";

	$get_project_machine_planning_list_squery_where = "";

	$filter_count = 0;

	// Data
	$get_project_machine_planning_list_sdata = array();

	if($planning_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." where project_machine_planning_id = :planning_id";
		}
		else
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." and project_machine_planning_id = :planning_id";
		}


		// Data
		$get_project_machine_planning_list_sdata[':planning_id'] = $planning_id;

		$filter_count++;
	}

	if($task_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." where project_machine_planning_task_id = :task_id";
		}
		else
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." and project_machine_planning_task_id = :task_id";
		}

		// Data
		$get_project_machine_planning_list_sdata[':task_id'] = $task_id;

		$filter_count++;
	}

	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." where PPPT.project_process_id= :process_id";
		}
		else
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." and PPPT.project_process_id = :process_id";
		}

		// Data
		$get_project_machine_planning_list_sdata[':process_id'] = $process_id;

		$filter_count++;
	}

	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." where PP.project_plan_project_id = :project_id";
		}
		else
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." and PP.project_plan_project_id = :project_id";
		}

		// Data
		$get_project_machine_planning_list_sdata[':project_id'] = $project_id;

		$filter_count++;
	}

	if($machine_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." where project_machine_planning_machine_id = :machine_id";
		}
		else
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." and project_machine_planning_machine_id = :machine_id";
		}

		// Data
		$get_project_machine_planning_list_sdata[':machine_id'] = $machine_id;

		$filter_count++;
	}

	if($machine_rate != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." where project_machine_planning_machine_rate = :machine_rate";
		}
		else
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." and project_machine_planning_machine_rate = :machine_rate";
		}

		// Data
		$get_project_machine_planning_list_sdata[':machine_rate'] = $machine_rate;

		$filter_count++;
	}


	if($no_of_hours != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." where project_machine_planning_no_of_hours = :no_of_hours";
		}
		else
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." and project_machine_planning_no_of_hours = :no_of_hours";
		}

		// Data
		$get_project_machine_planning_list_sdata[':no_of_hours'] = $no_of_hours;

		$filter_count++;
	}

	if($additional_cost != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." where project_machine_planning_additional_cost = :additional_cost";
		}
		else
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." and project_machine_planning_additional_cost = :additional_cost";
		}

		// Data
		$get_project_machine_planning_list_sdata[':additional_cost']  = $additional_cost;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." where project_machine_planning_active = :active";
		}
		else
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." and project_machine_planning_active = :active";
		}

		// Data
		$get_project_machine_planning_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." where project_machine_planning_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." and project_machine_planning_added_by = :added_by";
		}

		//Data
		$get_project_machine_planning_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." where project_machine_planning_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." and project_machine_planning_added_on >= :start_date";
		}

		//Data
		$get_project_machine_planning_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." where project_machine_planning_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." and project_machine_planning_added_on <= :end_date";
		}

		//Data
		$get_project_machine_planning_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	if($task != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." where PTM.project_task_master_id = :task";
		}
		else
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." and PTM.project_task_master_id = :task";
		}

		// Data
		$get_project_machine_planning_list_sdata[':task'] = $task;

		$filter_count++;
	}

	if($process != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." where PPM.project_process_master_id = :process";
		}
		else
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." and PPM.project_process_master_id = :process";
		}

		// Data
		$get_project_machine_planning_list_sdata[':process'] = $process;

		$filter_count++;
	}

	$get_project_machine_planning_list_squery = $get_project_machine_planning_list_squery_base.$get_project_machine_planning_list_squery_where;
	try
	{
		$dbConnection = get_conn_handle();

		$get_project_machine_planning_list_sstatement = $dbConnection->prepare($get_project_machine_planning_list_squery);

		$get_project_machine_planning_list_sstatement -> execute($get_project_machine_planning_list_sdata);

		$get_project_machine_planning_list_sdetails = $get_project_machine_planning_list_sstatement -> fetchAll();

		if(FALSE === $get_project_machine_planning_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_machine_planning_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_machine_planning_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

  /*
PURPOSE : To update Project Machine Planning
INPUT 	: Planning ID, Project Machine Planning Update Array
OUTPUT 	: Planning ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_machine_planning($planning_id,$task_id,$project_machine_planning_update_data)
{
	if(array_key_exists("machine_id",$project_machine_planning_update_data))
	{
		$machine_id = $project_machine_planning_update_data["machine_id"];
	}
	else
	{
		$machine_id = "";
	}

	if(array_key_exists("machine_rate",$project_machine_planning_update_data))
	{
		$machine_rate = $project_machine_planning_update_data["machine_rate"];
	}
	else
	{
		$machine_rate = "";
	}

	if(array_key_exists("no_of_hours",$project_machine_planning_update_data))
	{
		$no_of_hours = $project_machine_planning_update_data["no_of_hours"];
	}
	else
	{
		$no_of_hours = "";
	}

	if(array_key_exists("additional_cost",$project_machine_planning_update_data))
	{
		$additional_cost = $project_machine_planning_update_data["additional_cost"];
	}
	else
	{
		$additional_cost = "";
	}

	if(array_key_exists("active",$project_machine_planning_update_data))
	{
		$active = $project_machine_planning_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_machine_planning_update_data))
	{
		$remarks = $project_machine_planning_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_machine_planning_update_data))
	{
		$added_by = $project_machine_planning_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_machine_planning_update_data))
	{
		$added_on = $project_machine_planning_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_machine_planning_update_uquery_base = "update project_machine_planning set ";

	$project_machine_planning_update_uquery_set = "";

	$project_machine_planning_update_uquery_where = " where project_machine_planning_id = :planning_id or project_machine_planning_task_id = :task_id";

	$project_machine_planning_update_udata = array(":planning_id"=>$planning_id,":task_id"=>$task_id);

	$filter_count = 0;

	if($machine_id != "")
	{
		$project_machine_planning_update_uquery_set = $project_machine_planning_update_uquery_set." project_machine_planning_machine_id = :machine_id,";
		$project_machine_planning_update_udata[":machine_id"] = $machine_id;
		$filter_count++;
	}

	if($machine_rate != "")
	{
		$project_machine_planning_update_uquery_set = $project_machine_planning_update_uquery_set." project_machine_planning_machine_rate = :machine_rate,";
		$project_machine_planning_update_udata[":machine_rate"] = $machine_rate;
		$filter_count++;
	}

	if($no_of_hours != "")
	{
		$project_machine_planning_update_uquery_set = $project_machine_planning_update_uquery_set." project_machine_planning_no_of_hours = :no_of_hours,";
		$project_machine_planning_update_udata[":no_of_hours"] = $no_of_hours;
		$filter_count++;
	}

	if($additional_cost != "")
	{
		$project_machine_planning_update_uquery_set = $project_machine_planning_update_uquery_set." project_machine_planning_additional_cost = :additional_cost,";
		$project_machine_planning_update_udata[":additional_cost"] = $additional_cost;
		$filter_count++;
	}

	if($active != "")
	{
		$project_machine_planning_update_uquery_set = $project_machine_planning_update_uquery_set." project_machine_planning_active = :active,";
		$project_machine_planning_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_machine_planning_update_uquery_set = $project_machine_planning_update_uquery_set." project_machine_planning_remarks = :remarks,";
		$project_machine_planning_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_machine_planning_update_uquery_set = $project_machine_planning_update_uquery_set." project_machine_planning_added_by = :added_by,";
		$project_machine_planning_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_user_mapping_update_uquery_set = $project_user_mapping_update_uquery_set." project_machine_planning_added_on = :added_on,";
		$project_machine_planning_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_machine_planning_update_uquery_set = trim($project_machine_planning_update_uquery_set,',');
	}

	$project_machine_planning_update_uquery = $project_machine_planning_update_uquery_base.$project_machine_planning_update_uquery_set.$project_machine_planning_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $project_machine_planning_update_ustatement = $dbConnection->prepare($project_machine_planning_update_uquery);

        $project_machine_planning_update_ustatement -> execute($project_machine_planning_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $planning_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Task Indent
INPUT 	: Task Id, Indent Id, Remarks, Added by
OUTPUT 	: Task Indent Id, success or failure message
BY 		: Sonakshi
*/
function db_add_project_task_indent($task_id,$indent_id,$remarks,$added_by)
{
	// Query
    $project_task_indent_iquery = "insert into project_task_indent (project_task_indent_task_id,project_indent_id,project_task_indent_active,project_task_indent_remarks,project_task_indent_added_by,project_task_indent_added_on) values (:task_id,:indent_id,:active,:remarks,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_task_indent_istatement = $dbConnection->prepare($project_task_indent_iquery);

        // Data
        $project_management_master_idata = array(':task_id'=>$task_id,':indent_id'=>$indent_id,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_task_indent_istatement->execute($project_management_master_idata);
		$project_task_indent_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_task_indent_id	;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project Task Indent
INPUT 	: Project Task Indent Array
OUTPUT 	: List of Project Task Indent
BY 		: Sonakshi
*/
function db_get_project_task_indent_list($project_task_indent_search_data)
{
	// Extract all input parameters
	if(array_key_exists("task_indent_id",$project_task_indent_search_data))
	{
		$task_indent_id = $project_task_indent_search_data["task_indent_id"];
	}
	else
	{
		$task_indent_id = "";
	}

	if(array_key_exists("task_id",$project_task_indent_search_data))
	{
		$task_id = $project_task_indent_search_data["task_id"];
	}
	else
	{
		$task_id = "";
	}

	if(array_key_exists("indent_id",$project_task_indent_search_data))
	{
		$indent_id = $project_task_indent_search_data["indent_id"];
	}
	else
	{
		$indent_id = "";
	}

	if(array_key_exists("active",$project_task_indent_search_data))
	{
		$active = $project_task_indent_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_task_indent_search_data))
	{
		$added_by = $project_task_indent_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_task_indent_search_data))
	{
		$start_date = $project_task_indent_search_data["start_date"];
	}
	else
	{
		$start_date = "";
	}

	if(array_key_exists("end_date",$project_task_indent_search_data))
	{
		$end_date = $project_task_indent_search_data["end_date"];
	}
	else
	{
		$end_date = "";
	}
	$get_task_indent_list_squery_base = "select * from project_task_indent PTI inner join stock_indent_items SII on SII.stock_indent_id=PTI.project_indent_id";

	$get_project_task_indent_list_squery_where = "";
	$filter_count = 0;

	// Data
	$get_project_task_indentr_list_sdata = array();

	if($task_indent_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_indent_list_squery_where = $get_project_task_indent_list_squery_where." where project_task_indent_id = :task_indent_id";
		}
		else
		{
			// Query
			$get_project_task_indent_list_squery_where = $get_project_task_indent_list_squery_where." and project_task_indent_id = :task_indent_id";
		}

		// Data
		$get_project_task_indentr_list_sdata[':task_indent_id'] = $task_indent_id;

		$filter_count++;
	}

	if($task_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_indent_list_squery_where = $get_project_task_indent_list_squery_where." where project_task_indent_task_id = :task_id";
		}
		else
		{
			// Query
			$get_project_task_indent_list_squery_where = $get_project_task_indent_list_squery_where." and project_task_indent_task_id = :task_id";
		}

		// Data
		$get_project_task_indentr_list_sdata[':task_id'] = $task_id;

		$filter_count++;
	}

	if($indent_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_indent_list_squery_where = $get_project_task_indent_list_squery_where." where project_indent_id = :indent_id";
		}
		else
		{
			// Query
			$get_project_task_indent_list_squery_where = $get_project_task_indent_list_squery_where." and project_indent_id = :indent_id";
		}

		// Data
		$get_project_task_indentr_list_sdata[':indent_id'] = $indent_id;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_indent_list_squery_where = $get_project_task_indent_list_squery_where." where project_task_indent_active = :active";
		}
		else
		{
			// Query
			$get_project_task_indent_list_squery_where = $get_project_task_indent_list_squery_where." and project_task_indent_active = :active";
		}

		// Data
		$get_project_task_indentr_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_indent_list_squery_where = $get_project_task_indent_list_squery_where." where project_task_indent_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_task_indent_list_squery_where = $get_project_task_indent_list_squery_where." and project_task_indent_added_by = :added_by";
		}

		//Data
		$get_project_task_indentr_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_indent_list_squery_where = $get_project_task_indent_list_squery_where." where project_task_indent_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_task_indent_list_squery_where = $get_project_task_indent_list_squery_where." and project_task_indent_added_on >= :start_date";
		}

		//Data
		$get_project_task_indentr_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_indent_list_squery_where = $get_project_task_indent_list_squery_where." where project_task_indent_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_task_indent_list_squery_where = $get_project_task_indent_list_squery_where." and project_task_indent_added_on <= :end_date";
		}

		//Data
		$get_project_task_indentr_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	$get_project_task_indent_list_squery = $get_task_indent_list_squery_base.$get_project_task_indent_list_squery_where;

	try
	{
		$dbConnection = get_conn_handle();

		$get_project_task_indent_list_sstatement = $dbConnection->prepare($get_project_task_indent_list_squery);

		$get_project_task_indent_list_sstatement -> execute($get_project_task_indentr_list_sdata);

		$get_project_task_indent_list_sdetails = $get_project_task_indent_list_sstatement -> fetchAll();

		if(FALSE === $get_project_task_indent_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_task_indent_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_task_indent_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }
 /*
PURPOSE : To add new project_actual_machine_plan
INPUT 	: machine_id,Start Date Time, End Date Time, Additional Cost, Machine Vendor, Machine Number, Machine Fuel Charges, Machine With Fuel Charges, Machine Bata,
          Machine Issued Fuel, Machine Type, remarks,added_by
OUTPUT 	: task_id, success or failure message
BY 		: Ashwini
*/
function db_add_project_actual_machine_plan($task_id,$road_id,$machine_id,$start_date_time,$end_date_time,$additional_cost,$machine_vendor,$machine_number,$machine_fuel_charges,$machine_with_fuel_charges,$machine_bata,$machine_issued_fuel,$work_type,$rework_completion,$machine_type,$remarks,$added_by)
{
	// Query
	$actual_machine_plan_iquery = "insert into project_task_actual_machine_plan (project_task_actual_machine_plan_task_id,project_task_actual_machine_plan_road_id,project_task_machine_id,project_task_actual_machine_plan_start_date_time,project_task_actual_machine_plan_end_date_time,project_task_actual_machine_plan_off_time,project_task_actual_machine_plan_additional_cost,project_task_actual_machine_vendor,project_task_actual_machine_number,project_task_actual_machine_fuel_charges,project_task_actual_machine_with_fuel_charges,project_task_actual_machine_bata,project_task_actual_machine_issued_fuel,project_task_actual_machine_completion,project_task_actual_machine_work_type,project_task_actual_machine_rework_completion,project_task_actual_machine_type,project_task_actual_machine_plan_active,project_task_actual_machine_plan_check_status,project_task_actual_machine_plan_checked_by,project_task_actual_machine_plan_checked_on,project_task_actual_machine_plan_approved_by,project_task_actual_machine_plan_approved_on,project_task_actual_machine_plan_remarks,project_task_actual_machine_plan_added_by,project_task_actual_machine_plan_added_on) values (:task_id,:road_id,:machine_id,:start_date_time,:end_date_time,:off_duration,:additional_cost,:machine_vendor,:machine_number,:machine_fuel_charges,:machine_with_fuel_charges,:machine_bata,:machine_issued_fuel,:completion,:work_type,:rework_completion,:machine_type,:active,:check_status,:checked_by,:checked_on,:approved_by,:approved_on,:remarks,:added_by,:added_on)";

   try
    {
        $dbConnection = get_conn_handle();
        $actual_machine_plan_istatement = $dbConnection->prepare($actual_machine_plan_iquery);

        // Data
        $actual_machine_plan_idata = array(':task_id'=>$task_id,':road_id'=>$road_id,':machine_id'=>$machine_id,':start_date_time'=>$start_date_time,':end_date_time'=>$end_date_time,':off_duration'=>'0',':additional_cost'=>$additional_cost,':machine_vendor'=>$machine_vendor,':machine_number'=>$machine_number,':machine_fuel_charges'=>$machine_fuel_charges,':machine_with_fuel_charges'=>$machine_with_fuel_charges,':machine_bata'=>$machine_bata,':machine_issued_fuel'=>$machine_issued_fuel,':completion'=>'0',':work_type'=>$work_type,':rework_completion'=>$rework_completion,':machine_type'=>$machine_type,':active'=>'1',':check_status'=>'0',':checked_by'=>'',':checked_on'=>'',':approved_by'=>'',':approved_on'=>'',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $actual_machine_plan_istatement->execute($actual_machine_plan_idata);
		$actual_machine_plan_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $actual_machine_plan_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get project_actual_machine_plan list
INPUT 	: task_id, machine_id, start_date_time, end_date_time, additional_cost,Machine Vendor, Machine Number, Machine Fuel Charges, Machine With Fuel Charges,
          Machine Bata, Machine Issued Fuel, Machine Type, active, added_by, added_on
OUTPUT 	: List of project_actual_machine_plan
BY 		: Ashwini
*/
function db_get_actual_machine_plan($actual_machine_plan_search_data)
{
	if(array_key_exists("plan_id",$actual_machine_plan_search_data))
	{
		$plan_id = $actual_machine_plan_search_data["plan_id"];
	}
	else
	{
		$plan_id = "";
	}

	if(array_key_exists("task_id",$actual_machine_plan_search_data))
	{
		$task_id = $actual_machine_plan_search_data["task_id"];
	}
	else
	{
		$task_id = "";
	}

  if(array_key_exists("road_id",$actual_machine_plan_search_data))
	{
		$road_id = $actual_machine_plan_search_data["road_id"];
	}
	else
	{
		$road_id = "";
	}


	if(array_key_exists("task",$actual_machine_plan_search_data))
	{
		$task = $actual_machine_plan_search_data["task"];
	}
	else
	{
		$task = "";
	}

	if(array_key_exists("process",$actual_machine_plan_search_data))
	{
		$process = $actual_machine_plan_search_data["process"];
	}
	else
	{
		$process = "";
	}

	if(array_key_exists("machine_id",$actual_machine_plan_search_data))
	{
		$machine_id = $actual_machine_plan_search_data["machine_id"];
	}
	else
	{
		$machine_id = "";
	}

	if(array_key_exists("start_date_time",$actual_machine_plan_search_data))
	{
		$start_date_time= $actual_machine_plan_search_data["start_date_time"];
	}
	else
	{
		$start_date_time = "";
	}

	if(array_key_exists("end_date_time",$actual_machine_plan_search_data))
	{
		$end_date_time = $actual_machine_plan_search_data["end_date_time"];
	}
	else
	{
		$end_date_time = "";
	}

	if(array_key_exists("incomplete_check",$actual_machine_plan_search_data))
	{
		$incomplete_check = $actual_machine_plan_search_data["incomplete_check"];
	}
	else
	{
		$incomplete_check = "";
	}

	if(array_key_exists("is_overlap",$actual_machine_plan_search_data))
	{
		$is_overlap = $actual_machine_plan_search_data["is_overlap"];
	}
	else
	{
		$is_overlap = "";
	}

	if(array_key_exists("additional_cost",$actual_machine_plan_search_data))
	{
		$additional_cost = $actual_machine_plan_search_data["additional_cost"];
	}
	else
	{
		$additional_cost = "";
	}
	if(array_key_exists("machine_vendor",$actual_machine_plan_search_data))
	{
		$machine_vendor = $actual_machine_plan_search_data["machine_vendor"];
	}
	else
	{
		$machine_vendor = "";
	}

	if(array_key_exists("machine_number",$actual_machine_plan_search_data))
	{
		$machine_number = $actual_machine_plan_search_data["machine_number"];
	}
	else
	{
		$machine_number = "";
	}
	if(array_key_exists("machine_fuel_charges",$actual_machine_plan_search_data))
	{
		$machine_fuel_charges = $actual_machine_plan_search_data["machine_fuel_charges"];
	}
	else
	{
		$machine_fuel_charges = "";
	}
	if(array_key_exists("machine_with_fuel_charges",$actual_machine_plan_search_data))
	{
		$machine_with_fuel_charges = $actual_machine_plan_search_data["machine_with_fuel_charges"];
	}
	else
	{
		$machine_with_fuel_charges = "";
	}

	if(array_key_exists("machine_bata",$actual_machine_plan_search_data))
	{
		$machine_bata = $actual_machine_plan_search_data["machine_bata"];
	}
	else
	{
		$machine_bata = "";
	}

	if(array_key_exists("machine_issued_fuel",$actual_machine_plan_search_data))
	{
		$machine_issued_fuel = $actual_machine_plan_search_data["machine_issued_fuel"];
	}
	else
	{
		$machine_issued_fuel = "";
	}

	if(array_key_exists("machine_type",$actual_machine_plan_search_data))
	{
		$machine_type = $actual_machine_plan_search_data["machine_type"];
	}
	else
	{
		$machine_type = "";
	}

	if(array_key_exists("work_type",$actual_machine_plan_search_data))
	{
		$work_type = $actual_machine_plan_search_data["work_type"];
	}
	else
	{
		$work_type = "";
	}

	if(array_key_exists("rework_completion",$actual_machine_plan_search_data))
	{
		$rework_completion = $actual_machine_plan_search_data["rework_completion"];
	}
	else
	{
		$rework_completion = "";
	}

	if(array_key_exists("machine_type_id",$actual_machine_plan_search_data))
	{
		$machine_type_id = $actual_machine_plan_search_data["machine_type_id"];
	}
	else
	{
		$machine_type_id = "";
	}

    if(array_key_exists("active",$actual_machine_plan_search_data))
	{
		$active = $actual_machine_plan_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("display_status",$actual_machine_plan_search_data))
	{
		$display_status = $actual_machine_plan_search_data["display_status"];
	}
	else
	{
		$display_status = "";
	}

	if(array_key_exists("remarks",$actual_machine_plan_search_data))
	{
		$remarks = $actual_machine_plan_search_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$actual_machine_plan_search_data))
	{
		$added_by = $actual_machine_plan_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$actual_machine_plan_search_data))
	{
		$added_on = $actual_machine_plan_search_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	if(array_key_exists("project",$actual_machine_plan_search_data))
	{
		$project= $actual_machine_plan_search_data["project"];
	}
	else
	{
		$project= "";
	}

	if(array_key_exists("process_id",$actual_machine_plan_search_data))
	{
		$process_id= $actual_machine_plan_search_data["process_id"];
	}
	else
	{
		$process_id= "";
	}

	if(array_key_exists("percentage_pending",$actual_machine_plan_search_data))
	{
		$percentage_pending= $actual_machine_plan_search_data["percentage_pending"];
	}
	else
	{
		$percentage_pending= "";
	}

	if(array_key_exists("percentage_completion",$actual_machine_plan_search_data))
	{
		$percentage_completion= $actual_machine_plan_search_data["percentage_completion"];
	}
	else
	{
		$percentage_completion= "";
	}

	if(array_key_exists("start",$actual_machine_plan_search_data))
	{
		$start= $actual_machine_plan_search_data["start"];
	}
	else
	{
		$start= "-1";
	}

	if(array_key_exists("limit",$actual_machine_plan_search_data))
	{
		$limit= $actual_machine_plan_search_data["limit"];
	}
	else
	{
		$limit= "";
	}

	if(array_key_exists("max_machine_percentage",$actual_machine_plan_search_data))
	{
		$max_machine_percentage= $actual_machine_plan_search_data["max_machine_percentage"];
	}
	else
	{
		$max_machine_percentage= "";
	}



	$get_actual_machine_plan_list_squery_base = "select *,U.user_name as user_name,AU.user_name as approver from project_task_actual_machine_plan PTAMP inner join project_machine_master PMM on PMM.project_machine_master_id=PTAMP.project_task_machine_id inner join users U on U.user_id = PTAMP.project_task_actual_machine_plan_added_by inner join project_plan_process_task PPPT on PPPT.project_process_task_id = PTAMP.project_task_actual_machine_plan_task_id inner join project_task_master PTM on PTM.project_task_master_id=PPPT.project_process_task_type inner join project_plan_process PPP on PPP.project_plan_process_id=PPPT.project_process_id inner join project_plan PP on PP.project_plan_id=PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id=PP.project_plan_project_id inner join project_process_master PPM on PPM.project_process_master_id = PPP.project_plan_process_name inner join project_machine_vendor_master PMVM on PMVM.project_machine_vendor_master_id = PTAMP.project_task_actual_machine_vendor left outer join users AU on AU.user_id = PTAMP.project_task_actual_machine_plan_approved_by inner join project_machine_type_master PMTM on PMTM.project_machine_type_master_id = PMM.project_machine_master_machine_type left outer join project_site_location_mapping_master PSLMM on PSLMM.project_site_location_mapping_master_id=PTAMP.project_task_actual_machine_plan_road_id";

	$get_actual_machine_plan_list_squery_where = "";
	$get_actual_machine_plan_list_squery_limit  = "";
	$filter_count = 0;

	// Data
	$get_actual_machine_plan_list_sdata = array();

	if($plan_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_plan_id = :plan_id";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_plan_id = :plan_id";
		}

		// Data
		$get_actual_machine_plan_list_sdata[':plan_id'] = $plan_id;

		$filter_count++;
	}

	if($task_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_plan_task_id = :task_id";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_plan_task_id = :task_id";
		}

		// Data
		$get_actual_machine_plan_list_sdata[':task_id'] = $task_id;

		$filter_count++;
	}

  if($road_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_plan_road_id = :road_id";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_plan_road_id = :road_id";
		}

		// Data
		$get_actual_machine_plan_list_sdata[':road_id'] = $road_id;

		$filter_count++;
	}

	if($machine_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_machine_id = :machine_id";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_machine_id = :machine_id";
		}

		// Data
		$get_actual_machine_plan_list_sdata[':machine_id'] = $machine_id;

		$filter_count++;
	}

	if($start_date_time != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_plan_start_date_time >= :start_date_time";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_plan_start_date_time >= :start_date_time";
		}

		//Data
		$get_actual_machine_plan_list_sdata[':start_date_time']  = $start_date_time;

		$filter_count++;
	}
	if($end_date_time != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_plan_start_date_time <= :end_date_time";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_plan_start_date_time <= :end_date_time";
		}

		//Data
		$get_actual_machine_plan_list_sdata['end_date_time']  = $end_date_time;

		$filter_count++;
	}

	if($incomplete_check != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_plan_end_date_time = :not_complete_date_time";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_plan_end_date_time = :not_complete_date_time";
		}

		//Data
		$get_actual_machine_plan_list_sdata['not_complete_date_time']  = '0000-00-00 00:00:00';

		$filter_count++;
	}

	if($is_overlap != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where (project_task_actual_machine_plan_start_date_time <= :cur_start_date_time and project_task_actual_machine_plan_end_date_time >= :cur_start_date_time)";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and (project_task_actual_machine_plan_start_date_time <= :cur_start_date_time and project_task_actual_machine_plan_end_date_time >= :cur_start_date_time)";
		}

		//Data
		$get_actual_machine_plan_list_sdata['cur_start_date_time'] = $is_overlap;

		$filter_count++;
	}

	if($additional_cost != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_plan_additional_cost<= :additional_cost";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_plan_additional_cost <= :additional_cost";
		}

		//Data
		$get_actual_machine_plan_list_sdata['additional_cost']  = $additional_cost;

		$filter_count++;
	}

	if($display_status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_display_status = :display_status";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_display_status = :display_status";
		}

		//Data
		$get_actual_machine_plan_list_sdata['display_status']  = $display_status;

		$filter_count++;
	}

	if($machine_vendor != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_vendor = :machine_vendor";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_vendor = :machine_vendor";
		}

		//Data
		$get_actual_machine_plan_list_sdata['machine_vendor']  = $machine_vendor;

		$filter_count++;
	}

	if($machine_number != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_number like :machine_number";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_number like :machine_number";
		}

		//Data
		$get_actual_machine_plan_list_sdata[':machine_number'] = '%'.$machine_number;

		$filter_count++;
	}

	if($machine_fuel_charges != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_fuel_charges = :machine_fuel_charges";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_fuel_charges = :machine_fuel_charges";
		}

		//Data
		$get_actual_machine_plan_list_sdata['machine_fuel_charges']  = $machine_fuel_charges;

		$filter_count++;
	}
	if($machine_with_fuel_charges != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_with_fuel_charges = :machine_with_fuel_charges";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_with_fuel_charges = :machine_with_fuel_charges";
		}

		//Data
		$get_actual_machine_plan_list_sdata['machine_with_fuel_charges']  = $machine_with_fuel_charges;

		$filter_count++;
	}

	if($machine_bata != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_bata = :machine_bata";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_bata = :machine_bata";
		}

		//Data
		$get_actual_machine_plan_list_sdata['machine_bata']  = $machine_bata;

		$filter_count++;
	}

	if($machine_issued_fuel != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_issued_fuel = :machine_issued_fuel";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_issued_fuel = :machine_issued_fuel";
		}

		//Data
		$get_actual_machine_plan_list_sdata['machine_issued_fuel']  = $machine_issued_fuel;

		$filter_count++;
	}

	if($machine_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where PMM.project_machine_type = :machine_type";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and PMM.project_machine_type = :machine_type";
		}

		//Data
		$get_actual_machine_plan_list_sdata['machine_type']  = $machine_type;

		$filter_count++;
	}

	if($work_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_work_type = :work_type";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_work_type = :work_type";
		}

		//Data
		$get_actual_machine_plan_list_sdata['work_type']  = $work_type;

		$filter_count++;
	}

	if($rework_completion != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_rework_completion = :rework_completion";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_rework_completion = :rework_completion";
		}

		//Data
		$get_actual_machine_plan_list_sdata['rework_completion']  = $rework_completion;

		$filter_count++;
	}

	if($machine_type_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where PMM.project_machine_master_machine_type = :machine_type_id";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and PMM.project_machine_master_machine_type = :machine_type_id";
		}

		//Data
		$get_actual_machine_plan_list_sdata['machine_type_id']  = $machine_type_id;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_plan_active = :active";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_plan_active = :active";
		}

		// Data
		$get_actual_machine_plan_list_sdata[':active']  = $active;

		$filter_count++;
	}
	if($remarks != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_plan_remarks = :remarks";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_plan_remarks = :remarks";
		}

		// Data
		$get_actual_machine_plan_list_sdata[':remarks']  = $remarks;

		$filter_count++;
	}

	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_plan_added_by = :added_by";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_plan_added_by = :added_by";
		}

		//Data
		$get_actual_machine_plan_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($added_on != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_plan_added_on = :added_on";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_plan_added_on = :added_on";
		}

		//Data
		$get_actual_machine_plan_list_sdata[':added_on']  = $added_on;

		$filter_count++;
	}

	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where PP.project_plan_project_id = :project";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and PP.project_plan_project_id = :project";
		}

		//Data
		$get_actual_machine_plan_list_sdata['project']  = $project;

		$filter_count++;
	}

	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where PPPT.project_process_id = :process_id";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and PPPT.project_process_id = :process_id";
		}

		//Data
		$get_actual_machine_plan_list_sdata['process_id']  = $process_id;

		$filter_count++;
	}

	if($task != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where PTM.project_task_master_id = :task";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and PTM.project_task_master_id = :task";
		}

		//Data
		$get_actual_machine_plan_list_sdata['task']  = $task;

		$filter_count++;
	}

	if($process != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where PPM.project_process_master_id = :process";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and PPM.project_process_master_id = :process";
		}

		//Data
		$get_actual_machine_plan_list_sdata['process']  = $process;

		$filter_count++;
	}

	if($percentage_completion != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_completion = '100'";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_completion = '100'";
		}

		$filter_count++;
	}

	if($percentage_pending != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_completion < '100'";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_completion < '100'";
		}

		$filter_count++;
	}

	if($start >= 0)
	{
			// Query
			$get_actual_machine_plan_list_squery_limit = $get_actual_machine_plan_list_squery_limit." limit $start,$limit";
	}
	else
	{
		// Query
		$get_actual_machine_plan_list_squery_limit = "";
	}

	if($max_machine_percentage != "")
	{
		// Query
		$get_actual_machine_plan_list_squery_base = "select max(project_task_actual_machine_completion)as max_mpercentage from project_task_actual_machine_plan";
	}


	$get_actual_machine_plan_list_order_by = " order by project_task_actual_machine_plan_start_date_time DESC";
	$get_actual_machine_plan_list_squery = $get_actual_machine_plan_list_squery_base.$get_actual_machine_plan_list_squery_where.$get_actual_machine_plan_list_order_by.$get_actual_machine_plan_list_squery_limit;

	try
	{
		$dbConnection = get_conn_handle();

		$get_actual_machine_plan_list_sstatement = $dbConnection->prepare($get_actual_machine_plan_list_squery);

		$get_actual_machine_plan_list_sstatement -> execute($get_actual_machine_plan_list_sdata);
		ini_set('memory_limit', '-1');
		$get_actual_machine_plan_list_sdetails = $get_actual_machine_plan_list_sstatement -> fetchAll();

		if(FALSE === $get_actual_machine_plan_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_actual_machine_plan_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_actual_machine_plan_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

  /*
PURPOSE : To update project_actual_machine_plan list
INPUT 	: task_id,actual_machine_plan Update Array
OUTPUT 	: task ID; Message of success or failure
BY 		: Ashwini
*/
function db_update_actual_machine_plan($plan_id,$actual_machine_plan_update_data)
{
	if(array_key_exists("task_id",$actual_machine_plan_update_data))
	{
		$task_id = $actual_machine_plan_update_data["task_id"];
	}
	else
	{
		$task_id = "";
	}
	if(array_key_exists("machine_id",$actual_machine_plan_update_data))
	{
		$machine_id = $actual_machine_plan_update_data["machine_id"];
	}
	else
	{
		$machine_id = "";
	}
	if(array_key_exists("start_date_time",$actual_machine_plan_update_data))
	{
		$start_date_time = $actual_machine_plan_update_data["start_date_time"];
	}
	else
	{
		$start_date_time = "";
	}
	if(array_key_exists("end_date_time",$actual_machine_plan_update_data))
	{
		$end_date_time = $actual_machine_plan_update_data["end_date_time"];
	}
	else
	{
		$end_date_time = "";
	}

	if(array_key_exists("off_time",$actual_machine_plan_update_data))
	{
		$off_time = $actual_machine_plan_update_data["off_time"];
	}
	else
	{
		$off_time = "";
	}

	if(array_key_exists("additional_cost",$actual_machine_plan_update_data))
	{
		$additional_cost = $actual_machine_plan_update_data["additional_cost"];
	}
	else
	{
		$additional_cost = "";
	}
	if(array_key_exists("machine_vendor",$actual_machine_plan_update_data))
	{
		$machine_vendor = $actual_machine_plan_update_data["machine_vendor"];
	}
	else
	{
		$machine_vendor = "";
	}
	if(array_key_exists("machine_number",$actual_machine_plan_update_data))
	{
		$machine_number = $actual_machine_plan_update_data["machine_number"];
	}
	else
	{
		$machine_number = "";
	}
	if(array_key_exists("machine_fuel_charges",$actual_machine_plan_update_data))
	{
		$machine_fuel_charges = $actual_machine_plan_update_data["machine_fuel_charges"];
	}
	else
	{
		$machine_fuel_charges = "";
	}

	if(array_key_exists("machine_with_fuel_charges",$actual_machine_plan_update_data))
	{
		$machine_with_fuel_charges = $actual_machine_plan_update_data["machine_with_fuel_charges"];
	}
	else
	{
		$machine_with_fuel_charges = "";
	}

	if(array_key_exists("machine_bata",$actual_machine_plan_update_data))
	{
		$machine_bata = $actual_machine_plan_update_data["machine_bata"];
	}
	else
	{
		$machine_bata = "";
	}
	if(array_key_exists("machine_issued_fuel",$actual_machine_plan_update_data))
	{
		$machine_issued_fuel = $actual_machine_plan_update_data["machine_issued_fuel"];
	}
	else
	{
		$machine_issued_fuel = "";
	}

	if(array_key_exists("display_status",$actual_machine_plan_update_data))
	{
		$display_status = $actual_machine_plan_update_data["display_status"];
	}
	else
	{
		$display_status = "";
	}
	if(array_key_exists("status1",$actual_machine_plan_update_data))
	{
		$status1 = $actual_machine_plan_update_data["status1"];
	}
	else
	{
		$status1 = "";
	}

	if(array_key_exists("machine_type",$actual_machine_plan_update_data))
	{
		$machine_type = $actual_machine_plan_update_data["machine_type"];
	}
	else
	{
		$machine_type = "";
	}

	if(array_key_exists("work_type",$actual_machine_plan_update_data))
	{
		$work_type = $actual_machine_plan_update_data["work_type"];
	}
	else
	{
		$work_type = "";
	}

	if(array_key_exists("rework_completion",$actual_machine_plan_update_data))
	{
		$rework_completion = $actual_machine_plan_update_data["rework_completion"];
	}
	else
	{
		$rework_completion = "";
	}

	if(array_key_exists("active",$actual_machine_plan_update_data))
	{
		$active = $actual_machine_plan_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("check_status",$actual_machine_plan_update_data))
	{
		$check_status = $actual_machine_plan_update_data["check_status"];
	}
	else
	{
		$check_status = "";
	}

  if(array_key_exists("msmrt",$actual_machine_plan_update_data))
	{
		$msmrt = $actual_machine_plan_update_data["msmrt"];
	}
	else
	{
		$msmrt = "";
	}

	if(array_key_exists("completion",$actual_machine_plan_update_data))
	{
		$completion = $actual_machine_plan_update_data["completion"];
	}
	else
	{
		$completion = "";
	}

	if(array_key_exists("checked_by",$actual_machine_plan_update_data))
	{
		$checked_by = $actual_machine_plan_update_data["checked_by"];
	}
	else
	{
		$checked_by = "";
	}

	if(array_key_exists("checked_on",$actual_machine_plan_update_data))
	{
		$checked_on = $actual_machine_plan_update_data["checked_on"];
	}
	else
	{
		$checked_on = "";
	}

	if(array_key_exists("approved_by",$actual_machine_plan_update_data))
	{
		$approved_by = $actual_machine_plan_update_data["approved_by"];
	}
	else
	{
		$approved_by = "";
	}

	if(array_key_exists("approved_on",$actual_machine_plan_update_data))
	{
		$approved_on = $actual_machine_plan_update_data["approved_on"];
	}
	else
	{
		$approved_on = "";
	}
	if(array_key_exists("remarks",$actual_machine_plan_update_data))
	{
		$remarks = $actual_machine_plan_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$actual_machine_plan_update_data))
	{
		$added_by = $actual_machine_plan_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$actual_machine_plan_update_data))
	{
		$added_on = $actual_machine_plan_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $actual_machine_plan_update_uquery_base = "update project_task_actual_machine_plan set ";

	$actual_machine_plan_update_uquery_set = "";

	$actual_machine_plan_update_uquery_where = " where project_task_actual_machine_plan_id = :plan_id";

	$actual_machine_plan_update_udata = array(":plan_id"=>$plan_id);

	$filter_count = 0;

	if($task_id != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set."project_task_actual_machine_plan_task_id  = :task_id,";
		$actual_machine_plan_update_udata[":task_id"] = $task_id;
		$filter_count++;
	}
	if($machine_id != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set."project_task_machine_id  = :machine_id,";
		$actual_machine_plan_update_udata[":machine_id"] = $machine_id;
		$filter_count++;
	}
	if($start_date_time != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set."project_task_actual_machine_plan_start_date_time = :start_date_time,";
		$actual_machine_plan_update_udata[":start_date_time"] = $start_date_time;
		$filter_count++;
	}
	if($end_date_time != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set."project_task_actual_machine_plan_end_date_time = :end_date_time,";
		$actual_machine_plan_update_udata[":end_date_time"] = $end_date_time;
		$filter_count++;
	}
	if($off_time != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set."project_task_actual_machine_plan_off_time = :off_time,";
		$actual_machine_plan_update_udata[":off_time"] = $off_time;
		$filter_count++;
	}
	if($additional_cost != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set."project_task_actual_machine_plan_additional_cost = :additional_cost,";
		$actual_machine_plan_update_udata[":additional_cost"] = $additional_cost;
		$filter_count++;
	}
	if($machine_vendor != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set."project_task_actual_machine_vendor = :machine_vendor,";
		$actual_machine_plan_update_udata[":machine_vendor"] = $machine_vendor;
		$filter_count++;
	}
	if($machine_number != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set."project_task_actual_machine_number = :machine_number,";
		$actual_machine_plan_update_udata[":machine_number"] = $machine_number;
		$filter_count++;
	}
	if($machine_fuel_charges != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set."project_task_actual_machine_fuel_charges = :machine_fuel_charges,";
		$actual_machine_plan_update_udata[":machine_fuel_charges"] = $machine_fuel_charges;
		$filter_count++;
	}
	if($machine_with_fuel_charges != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set."project_task_actual_machine_with_fuel_charges = :machine_with_fuel_charges,";
		$actual_machine_plan_update_udata[":machine_with_fuel_charges"] = $machine_with_fuel_charges;
		$filter_count++;
	}
	if($display_status != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set."project_task_actual_machine_display_status = :display_status,";
		$actual_machine_plan_update_udata[":display_status"] = $display_status;
		$filter_count++;
	}
	if($status1 != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set."project_task_actual_machine_bill_status = :status1,";
		$actual_machine_plan_update_udata[":status1"] = $status1;
		$filter_count++;
	}

	if($machine_bata != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set."project_task_actual_machine_bata = :machine_bata,";
		$actual_machine_plan_update_udata[":machine_bata"] = $machine_bata;
		$filter_count++;
	}
	if($machine_issued_fuel != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set."project_task_actual_machine_issued_fuel = :machine_issued_fuel,";
		$actual_machine_plan_update_udata[":machine_issued_fuel"] = $machine_issued_fuel;
		$filter_count++;
	}

	if($machine_type != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set."project_task_actual_machine_type = :machine_type,";
		$actual_machine_plan_update_udata[":machine_type"] = $machine_type;
		$filter_count++;
	}

	if($work_type != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set."project_task_actual_machine_work_type = :work_type,";
		$actual_machine_plan_update_udata[":work_type"] = $work_type;
		$filter_count++;
	}

	if($rework_completion != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set."project_task_actual_machine_rework_completion = :rework_completion,";
		$actual_machine_plan_update_udata[":rework_completion"] = $rework_completion;
		$filter_count++;
	}

	if($active != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set." project_task_actual_machine_plan_active = :active,";
		$actual_machine_plan_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($check_status != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set." project_task_actual_machine_plan_check_status = :check_status,";
		$actual_machine_plan_update_udata[":check_status"] = $check_status;
		$filter_count++;
	}

  if($msmrt != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set." project_task_actual_machine_msmrt	 = :msmrt,";
		$actual_machine_plan_update_udata[":msmrt"] = $msmrt;
		$filter_count++;
	}

	if($completion != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set." project_task_actual_machine_completion = :completion,";
		$actual_machine_plan_update_udata[":completion"] = $completion;
		$filter_count++;
	}

	if($checked_by != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set." project_task_actual_machine_plan_checked_by = :checked_by,";
		$actual_machine_plan_update_udata[":checked_by"] = $checked_by;
		$filter_count++;
	}

	if($checked_on != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set." project_task_actual_machine_plan_checked_on = :checked_on,";
		$actual_machine_plan_update_udata[":checked_on"] = $checked_on;
		$filter_count++;
	}

	if($approved_by != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set." project_task_actual_machine_plan_approved_by = :approved_by,";
		$actual_machine_plan_update_udata[":approved_by"] = $approved_by;
		$filter_count++;
	}

	if($approved_on != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set." project_task_actual_machine_plan_approved_on = :approved_on,";
		$actual_machine_plan_update_udata[":approved_on"] = $approved_on;
		$filter_count++;
	}

	if($remarks != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set." project_task_actual_machine_plan_remarks = :remarks,";
		$actual_machine_plan_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set." project_task_actual_machine_plan_added_by = :added_by,";
		$actual_machine_plan_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$actual_machine_plan_update_uquery_set = $actual_machine_plan_update_uquery_set." project_task_actual_machine_plan_added_on = :added_on,";
		$actual_machine_plan_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$actual_machine_plan_update_uquery_set = trim($actual_machine_plan_update_uquery_set,',');
	}

	$actual_machine_plan_update_uquery = $actual_machine_plan_update_uquery_base.$actual_machine_plan_update_uquery_set.$actual_machine_plan_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();

        $actual_machine_plan_update_ustatement = $dbConnection->prepare($actual_machine_plan_update_uquery);

        $actual_machine_plan_update_ustatement -> execute($actual_machine_plan_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $plan_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Process delay Reason
INPUT 	: Start Date,End Date,Name, Remarks, Added By
OUTPUT 	: Reason ID, success or failure message
BY 		: Sonakshi
*/
function db_add_project_process_delay_reason($start_date,$end_date,$task_id,$name,$remarks,$added_by)
{
	// Query
   $process_delay_reason_iquery = "insert into project_process_delay_reason (project_process_delay_reason_start_date,project_process_delay_reason_end_date,project_process_delay_reason_process_id,project_process_delay_reason_name,
   project_process_delay_reason_active,project_process_delay_reason_remarks,project_process_delay_reason_added_by,
   project_process_delay_reason_added_on) values(:start_date,:end_date,:task_id,:name,:active,:remarks,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $process_delay_reason_istatement = $dbConnection->prepare($process_delay_reason_iquery);

        // Data
        $delay_reason_idata = array(':start_date'=>$start_date,'end_date'=>$end_date,':task_id'=>$task_id,':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $process_delay_reason_istatement->execute($delay_reason_idata);
		$project_process_delay_reason_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_process_delay_reason_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Process delay Reason List
INPUT 	: Reason ID, Start Date,End Date, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Process Delay Reasons
BY 		:Sonakshi D
*/
function db_get_project_process_delay_reason($process_delay_reason_search_data)
{
	if(array_key_exists("reason_id",$process_delay_reason_search_data))
	{
		$reason_id = $process_delay_reason_search_data["reason_id"];
	}
	else
	{
		$reason_id= "";
	}

	if(array_key_exists("start_date",$process_delay_reason_search_data))
	{
		$start_date = $process_delay_reason_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$process_delay_reason_search_data))
	{
		$end_date = $process_delay_reason_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	if(array_key_exists("process_id",$process_delay_reason_search_data))
	{
		$process_id = $process_delay_reason_search_data["process_id"];
	}
	else
	{
		$process_id= "";
	}

	if(array_key_exists("name",$process_delay_reason_search_data))
	{
		$name = $process_delay_reason_search_data["name"];
	}
	else
	{
		$name = "";
	}

	if(array_key_exists("active",$process_delay_reason_search_data))
	{
		$active = $process_delay_reason_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$process_delay_reason_search_data))
	{
		$added_by = $process_delay_reason_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$process_delay_reason_search_data))
	{
		$start_date= $process_delay_reason_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$process_delay_reason_search_data))
	{
		$end_date= $process_delay_reason_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_process_delay_reason_list_squery_base = "select * from project_process_delay_reason  PPDR inner join users U on U.user_id=PPDR.project_process_delay_reason_added_by inner join delay_reason_master DRM on DRM.delay_reason_master_id=PPDR.project_process_delay_reason_name inner join project_process_master PPM on PPM.project_process_master_id= PPDR.project_process_delay_reason_process_id ";
	$get_process_delay_reason_list_squery_where = "";
	$filter_count = 0;

	// Data
	$get_process_delay_reason_list_sdata = array();

	if($reason_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_delay_reason_list_squery_where = $get_process_delay_reason_list_squery_where." where project_process_delay_reason_id = :reason_id";
		}
		else
		{
			// Query
			$get_process_delay_reason_list_squery_where = $get_process_delay_reason_list_squery_where." and project_process_delay_reason_id = :reason_id";
		}

		// Data
		$get_process_delay_reason_list_sdata[':reason_id'] = $reason_id;

		$filter_count++;
	}

	if($name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_delay_reason_list_squery_where = $get_process_delay_reason_list_squery_where." where project_process_delay_reason_name = :name";
		}
		else
		{
			// Query
			$get_process_delay_reason_list_squery_where = $get_process_delay_reason_list_squery_where." and project_process_delay_reason_name = :name";
		}

		// Data
		$get_process_delay_reason_list_sdata[':name'] = $name;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_delay_reason_list_squery_where = $get_process_delay_reason_list_squery_where." where project_process_delay_reason_start_date = :start_date";
		}
		else
		{
			// Query
			$get_process_delay_reason_list_squery_where = $get_process_delay_reason_list_squery_where." and project_process_delay_reason_start_date = :start_date";
		}

		// Data
		$get_process_delay_reason_list_sdata[':start_date'] = $start_date;

		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_delay_reason_list_squery_where = $get_process_delay_reason_list_squery_where." where project_process_delay_reason_end_date = :end_date";
		}
		else
		{
			// Query
			$get_process_delay_reason_list_squery_where = $get_process_delay_reason_list_squery_where." and project_process_delay_reason_end_date = :end_date";
		}

		// Data
		$get_process_delay_reason_list_sdata[':end_date'] = $end_date;

		$filter_count++;
	}

	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_delay_reason_list_squery_where = $get_process_delay_reason_list_squery_where." where project_process_delay_reason_process_id = :process_id";
		}
		else
		{
			// Query
			$get_process_delay_reason_list_squery_where = $get_process_delay_reason_list_squery_where." and project_process_delay_reason_process_id = :process_id";
		}

		// Data
		$get_process_delay_reason_list_sdata[':process_id'] = $process_id;

		$filter_count++;
	}


	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_delay_reason_list_squery_where = $get_process_delay_reason_list_squery_where." where project_process_delay_reason_active = :active";
		}
		else
		{
			// Query
			$get_process_delay_reason_list_squery_where = $get_process_delay_reason_list_squery_where." and project_process_delay_reason_active = :active";
		}

		// Data
		$get_process_delay_reason_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_delay_reason_list_squery_where = $get_process_delay_reason_list_squery_where." where project_process_delay_reason_added_by = :added_by";
		}
		else
		{
			// Query
			$get_process_delay_reason_list_squery_where = $get_process_delay_reason_list_squery_where." and project_process_delay_reason_added_by = :added_by";
		}

		//Data
		$get_process_delay_reason_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_delay_reason_list_squery_where = $get_process_delay_reason_list_squery_where." where project_process_delay_reason_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_process_delay_reason_list_squery_where = $get_process_delay_reason_list_squery_where." and project_process_delay_reason_added_on >= :start_date";
		}

		//Data
		$get_process_delay_reason_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_process_delay_reason_list_squery_where = $get_process_delay_reason_list_squery_where." where project_process_delay_reason_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_process_delay_reason_list_squery_where = $get_process_delay_reason_list_squery_where." and project_process_delay_reason_added_on <= :end_date";
		}

		//Data
		$get_process_delay_reason_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	$get_process_delay_reason_list_squery = $get_process_delay_reason_list_squery_base.$get_process_delay_reason_list_squery_where;
	try
	{
		$dbConnection = get_conn_handle();

		$get_process_delay_reason_list_sstatement = $dbConnection->prepare($get_process_delay_reason_list_squery);

		$get_process_delay_reason_list_sstatement -> execute($get_process_delay_reason_list_sdata);

		$get_process_delay_reason_list_sdetails = $get_process_delay_reason_list_sstatement -> fetchAll();

		if(FALSE === $get_process_delay_reason_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_process_delay_reason_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_process_delay_reason_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

 /*
PURPOSE : To update Process delay Reason
INPUT 	: Reason ID, Process delay Reason Master Update Array
OUTPUT 	: Reason ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_process_delay_reason($reason_id,$process_delay_reason_update_data)
{
	if(array_key_exists("process_id",$process_delay_reason_update_data))
	{
		$process_id = $process_delay_reason_update_data["process_id"];
	}
	else
	{
		$process_id = "";
	}

	if(array_key_exists("name",$process_delay_reason_update_data))
	{
		$name = $process_delay_reason_update_data["name"];
	}
	else
	{
		$name = "";
	}

	if(array_key_exists("start_date",$process_delay_reason_search_data))
	{
		$start_date = $process_delay_reason_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$process_delay_reason_search_data))
	{
		$end_date = $process_delay_reason_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	if(array_key_exists("active",$process_delay_reason_update_data))
	{
		$active = $process_delay_reason_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$process_delay_reason_update_data))
	{
		$remarks = $process_delay_reason_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$process_delay_reason_update_data))
	{
		$added_by = $process_delay_reason_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$process_delay_reason_update_data))
	{
		$added_on = $process_delay_reason_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $process_delay_reason_update_uquery_base = "update project_process_delay_reason set";

	$process_delay_reason_update_uquery_set = "";

	$process_delay_reason_update_uquery_where = " where project_process_delay_reason_id = :reason_id";

	$process_delay_reason_update_udata = array(":reason_id"=>$reason_id);

	$filter_count = 0;

	if($process_id != "")
	{
		$process_delay_reason_update_uquery_set = $process_delay_reason_update_uquery_set." project_process_delay_reason_process_id = :process_id,";
		$process_delay_reason_update_udata[":process_id"] = $process_id;
		$filter_count++;
	}

	if($name != "")
	{
		$process_delay_reason_update_uquery_set = $process_delay_reason_update_uquery_set." project_process_delay_reason_name = :name,";
		$process_delay_reason_update_udata[":name"] = $name;
		$filter_count++;
	}

	if($start_date != "")
	{
		$process_delay_reason_update_uquery_set = $process_delay_reason_update_uquery_set." project_process_delay_reason_start_date = :start_date,";
		$process_delay_reason_update_udata[":start_date"] = $start_date;
		$filter_count++;
	}

	if($end_date != "")
	{
		$process_delay_reason_update_uquery_set = $process_delay_reason_update_uquery_set." project_process_delay_reason_end_date = :end_date,";
		$process_delay_reason_update_udata[":end_date"] = $end_date;
		$filter_count++;
	}

	if($active != "")
	{
		$process_delay_reason_update_uquery_set = $process_delay_reason_update_uquery_set." project_process_delay_reason_active = :active,";
		$process_delay_reason_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$process_delay_reason_update_uquery_set = $process_delay_reason_update_uquery_set." project_process_delay_reason_remarks = :remarks,";
		$process_delay_reason_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$process_delay_reason_update_uquery_set = $process_delay_reason_update_uquery_set." project_process_delay_reason_added_by = :added_by,";
		$process_delay_reason_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$process_delay_reason_update_uquery_set = $process_delay_reason_update_uquery_set." project_process_delay_reason_added_on = :added_on,";
		$process_delay_reason_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$process_delay_reason_update_uquery_set = trim($process_delay_reason_update_uquery_set,',');
	}

	$process_delay_reason_update_uquery = $process_delay_reason_update_uquery_base.$process_delay_reason_update_uquery_set.$process_delay_reason_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $process_delay_reason_update_ustatement = $dbConnection->prepare($process_delay_reason_update_uquery);

        $process_delay_reason_update_ustatement -> execute($process_delay_reason_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $reason_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Task Method Planning
INPUT 	: Task ID , Doc, Plan Type, Remarks, Added By
OUTPUT 	: Planning ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_task_method_planning($task_id,$document,$plan_type,$remarks,$added_by)
{
	// Query
   $project_task_method_planning_iquery = "insert into project_task_method_planning
   (project_task_method_planning_task_id,project_task_method_planning_document,project_task_method_planning_type,project_task_method_planning_active,
   project_task_method_planning_remarks,project_task_method_planning_added_by,project_task_method_planning_added_on) values (:task_id,:document,:plan_type,:active,:remarks,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_task_method_planning_istatement = $dbConnection->prepare($project_task_method_planning_iquery);

        // Data
        $project_task_method_planning_idata = array(':task_id'=>$task_id,':document'=>$document,':plan_type'=>$plan_type,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,
		':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_task_method_planning_istatement->execute($project_task_method_planning_idata);
		$project_task_method_planning_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_task_method_planning_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project Task Method Planning List
INPUT 	: Planning ID, Task ID, Document, Plan Type, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Method Planning
BY 		: Lakshmi
*/
function db_get_project_task_method_planning($project_task_method_planning_search_data)
{
	if(array_key_exists("planning_id",$project_task_method_planning_search_data))
	{
		$planning_id = $project_task_method_planning_search_data["planning_id"];
	}
	else
	{
		$planning_id = "";
	}

	if(array_key_exists("task_id",$project_task_method_planning_search_data))
	{
		$task_id = $project_task_method_planning_search_data["task_id"];
	}
	else
	{
		$task_id = "";
	}

	if(array_key_exists("process_id",$project_task_method_planning_search_data))
	{
		$process_id = $project_task_method_planning_search_data["process_id"];
	}
	else
	{
		$process_id = "";
	}

	if(array_key_exists("project_id",$project_task_method_planning_search_data))
	{
		$project_id = $project_task_method_planning_search_data["project_id"];
	}
	else
	{
		$project_id = "";
	}

	if(array_key_exists("document",$project_task_method_planning_search_data))
	{
		$document = $project_task_method_planning_search_data["document"];
	}
	else
	{
		$document = "";
	}

	if(array_key_exists("plan_type",$project_task_method_planning_search_data))
	{
		$plan_type = $project_task_method_planning_search_data["plan_type"];
	}
	else
	{
		$plan_type = "";
	}

	if(array_key_exists("active",$project_task_method_planning_search_data))
	{
		$active = $project_task_method_planning_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_task_method_planning_search_data))
	{
		$added_by = $project_task_method_planning_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_task_method_planning_search_data))
	{
		$start_date = $project_task_method_planning_search_data["start_date"];
	}
	else
	{
		$start_date = "";
	}

	if(array_key_exists("end_date",$project_task_method_planning_search_data))
	{
		$end_date = $project_task_method_planning_search_data["end_date"];
	}
	else
	{
		$end_date = "";
	}

	$get_project_task_method_planning_list_squery_base = "select * from project_task_method_planning PTMP inner join users U on U.user_id=PTMP.project_task_method_planning_added_by inner join project_plan_process_task PPPT on PPPT.project_process_task_id = PTMP.project_task_method_planning_task_id inner join project_task_master PTM on PTM.project_task_master_id = PPPT.project_process_task_type inner join project_plan_process PPP on PPP.project_plan_process_id = PPPT.project_process_id inner join project_process_master PPM on PPM.project_process_master_id = PPP.project_plan_process_name inner join project_plan PP on PP.project_plan_id = PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id = PP.project_plan_project_id";

	$get_project_task_method_planning_list_squery_where = "";
	$filter_count = 0;

	// Data
	$get_project_task_method_planning_list_sdata = array();

	if($planning_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_method_planning_list_squery_where = $get_project_task_method_planning_list_squery_where." where project_task_method_planning_id = :planning_id";
		}
		else
		{
			// Query
			$get_project_task_method_planning_list_squery_where = $get_project_task_method_planning_list_squery_where." and project_task_method_planning_id = :planning_id";
		}

		// Data
		$get_project_task_method_planning_list_sdata[':planning_id'] = $planning_id;

		$filter_count++;
	}

	if($task_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_method_planning_list_squery_where = $get_project_task_method_planning_list_squery_where." where project_task_method_planning_task_id = :task_id";
		}
		else
		{
			// Query
			$get_project_task_method_planning_list_squery_where = $get_project_task_method_planning_list_squery_where." and project_task_method_planning_task_id = :task_id";
		}

		// Data
		$get_project_task_method_planning_list_sdata[':task_id'] = $task_id;

		$filter_count++;
	}

	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_method_planning_list_squery_where = $get_project_task_method_planning_list_squery_where." where PPM.project_process_master_id = :process_id";
		}
		else
		{
			// Query
			$get_project_task_method_planning_list_squery_where = $get_project_task_method_planning_list_squery_where." and PPM.project_process_master_id = :process_id";
		}

		// Data
		$get_project_task_method_planning_list_sdata[':process_id'] = $process_id;

		$filter_count++;
	}

	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_method_planning_list_squery_where = $get_project_task_method_planning_list_squery_where." where PMPM.project_management_master_id = :project_id";
		}
		else
		{
			// Query
			$get_project_task_method_planning_list_squery_where = $get_project_task_method_planning_list_squery_where." and PMPM.project_management_master_id = :project_id";
		}

		// Data
		$get_project_task_method_planning_list_sdata[':project_id'] = $project_id;

		$filter_count++;
	}

	if($document != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_method_planning_list_squery_where = $get_project_task_method_planning_list_squery_where." where project_task_method_planning_document = :document";
		}
		else
		{
			// Query
			$get_project_task_method_planning_list_squery_where = $get_project_task_method_planning_list_squery_where." and project_task_method_planning_document = :document";
		}

		// Data
		$get_project_task_method_planning_list_sdata[':document'] = $document;

		$filter_count++;
	}

	if($plan_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_method_planning_list_squery_where = $get_project_task_method_planning_list_squery_where." where project_task_method_planning_type = :plan_type";
		}
		else
		{
			// Query
			$get_project_task_method_planning_list_squery_where = $get_project_task_method_planning_list_squery_where." and project_task_method_planning_type = :plan_type";
		}

		// Data
		$get_project_task_method_planning_list_sdata[':plan_type'] = $plan_type;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_method_planning_list_squery_where = $get_project_task_method_planning_list_squery_where." where project_task_method_planning_active = :active";
		}
		else
		{
			// Query
			$get_project_task_method_planning_list_squery_where = $get_project_task_method_planning_list_squery_where." and project_task_method_planning_active = :active";
		}

		// Data
		$get_project_task_method_planning_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_method_planning_list_squery_where = $get_project_task_method_planning_list_squery_where." where project_task_method_planning_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_task_method_planning_list_squery_where = $get_project_task_method_planning_list_squery_where." and project_task_method_planning_added_by = :added_by";
		}

		//Data
		$get_project_task_method_planning_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_method_planning_list_squery_where = $get_project_task_method_planning_list_squery_where." where project_task_method_planning_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_task_method_planning_list_squery_where = $get_project_task_method_planning_list_squery_where." and project_task_method_planning_added_on >= :start_date";
		}

		//Data
		$get_project_task_method_planning_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_method_planning_list_squery_where = $get_project_task_method_planning_list_squery_where." where project_task_method_planning_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_task_method_planning_list_squery_where = $get_project_task_method_planning_list_squery_where." and project_task_method_planning_added_on <= :end_date";
		}

		//Data
		$get_project_task_method_planning_list_sdata[':end_date']  = $end_date;

		$filter_count++;
	}

	$get_project_task_method_planning_list_squery = $get_project_task_method_planning_list_squery_base.$get_project_task_method_planning_list_squery_where;

	try
	{
		$dbConnection = get_conn_handle();

		$get_project_task_method_planning_list_sstatement = $dbConnection->prepare($get_project_task_method_planning_list_squery);

		$get_project_task_method_planning_list_sstatement -> execute($get_project_task_method_planning_list_sdata);

		$get_project_task_method_planning_list_sdetails = $get_project_task_method_planning_list_sstatement -> fetchAll();

		if(FALSE === $get_project_task_method_planning_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_task_method_planning_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_task_method_planning_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

 /*
PURPOSE : To update Method Planning list
INPUT 	: Planning ID, Method Planning Update Array
OUTPUT 	: Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_task_method_planning($planning_id,$project_task_method_planning_update_data)
{

	if(array_key_exists("task_id",$project_task_method_planning_update_data))
	{
		$task_id = $project_task_method_planning_update_data["task_id"];
	}
	else
	{
		$task_id = "";
	}

	if(array_key_exists("document",$project_task_method_planning_update_data))
	{
		$document = $project_task_method_planning_update_data["document"];
	}
	else
	{
		$document = "";
	}

	if(array_key_exists("plan_type",$project_task_method_planning_update_data))
	{
		$plan_type = $project_task_method_planning_update_data["plan_type"];
	}
	else
	{
		$plan_type = "";
	}

	if(array_key_exists("active",$project_task_method_planning_update_data))
	{
		$active = $project_task_method_planning_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_task_method_planning_update_data))
	{
		$remarks = $project_task_method_planning_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_task_method_planning_update_data))
	{
		$added_by = $project_task_method_planning_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_task_method_planning_update_data))
	{
		$added_on = $project_task_method_planning_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_task_method_planning_update_uquery_base = "update project_task_method_planning set ";

	$project_task_method_planning_update_uquery_set = "";

	$project_task_method_planning_update_uquery_where = " where project_task_method_planning_id = :planning_id";

	$project_task_method_planning_update_udata = array(":planning_id"=>$planning_id);

	$filter_count = 0;

	if($task_id != "")
	{
		$project_task_method_planning_update_uquery_set = $project_task_method_planning_update_uquery_set."project_task_method_planning_task_id  = :task_id,";
		$project_task_method_planning_update_udata[":task_id"] = $task_id;
		$filter_count++;
	}

	if($document != "")
	{
		$project_task_method_planning_update_uquery_set = $project_task_method_planning_update_uquery_set."project_task_method_planning_document  = :document,";
		$project_task_method_planning_update_udata[":document"] = $document;
		$filter_count++;
	}

	if($plan_type != "")
	{
		$project_task_method_planning_update_uquery_set = $project_task_method_planning_update_uquery_set."project_task_method_planning_type  = :plan_type,";
		$project_task_method_planning_update_udata[":plan_type"] = $plan_type;
		$filter_count++;
	}

	if($active != "")
	{
		$project_task_method_planning_update_uquery_set = $project_task_method_planning_update_uquery_set."project_task_method_planning_active = :active,";
		$project_task_method_planning_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_task_method_planning_update_uquery_set = $project_task_method_planning_update_uquery_set." project_task_method_planning_remarks = :remarks,";
		$project_task_method_planning_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_task_method_planning_update_uquery_set = $project_task_method_planning_update_uquery_set."project_task_method_planning_added_by = :added_by,";
		$project_task_method_planning_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_task_method_planning_update_uquery_set = $project_task_method_planning_update_uquery_set."project_task_method_planning_added_on = :added_on,";
		$project_task_method_planning_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_task_method_planning_update_uquery_set = trim($project_task_method_planning_update_uquery_set,',');
	}

	$project_task_method_planning_update_uquery = $project_task_method_planning_update_uquery_base.$project_task_method_planning_update_uquery_set.
	$project_task_method_planning_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();

        $project_task_method_planning_update_ustatement = $dbConnection->prepare($project_task_method_planning_update_uquery);

        $project_task_method_planning_update_ustatement -> execute($project_task_method_planning_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $planning_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Task BOQ
INPUT 	: UOM, Number, Remarks, Added By
OUTPUT 	: BOQ ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_task_boq($task_id,$process,$contract_task,$uom,$location,$number,$length,$breadth,$depth,$total_sqft,$amount,$remarks,$added_by)
{
	// Query
   $project_task_boq_iquery = "insert into project_task_boq (project_task_id,project_task_contract_process,project_task_contract_task,project_task_boq_uom,project_task_boq_location,project_task_boq_number,project_task_boq_length,project_task_boq_breadth,project_task_boq_depth,project_task_boq_total_measurement,project_task_boq_amount,project_task_boq_active,project_task_boq_remarks,project_task_boq_added_by,project_task_boq_added_on) values (:task_id,:process,:contract_task,:uom,:location,:number,:length,:breadth,:depth,:total_sqft,:amount,:active,:remarks,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_task_boq_istatement = $dbConnection->prepare($project_task_boq_iquery);

        // Data
        $project_task_boq_idata = array(':task_id'=>$task_id,':process'=>$process,':contract_task'=>$contract_task,':uom'=>$uom,':location'=>$location,':number'=>$number,':length'=>$length,':breadth'=>$breadth,':depth'=>$depth,':total_sqft'=>$total_sqft,':amount'=>$amount,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_task_boq_istatement->execute($project_task_boq_idata);
		$project_task_boq_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_task_boq_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project Task BOQ List
INPUT 	: BOQ ID, UOM, Number, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Task BOQ
BY 		: Lakshmi
*/
function db_get_project_task_boq($project_task_boq_search_data)
{
	if(array_key_exists("boq_id",$project_task_boq_search_data))
	{
		$boq_id = $project_task_boq_search_data["boq_id"];
	}
	else
	{
		$boq_id= "";
	}
	if(array_key_exists("task_id",$project_task_boq_search_data))
	{
		$task_id = $project_task_boq_search_data["task_id"];
	}
	else
	{
		$task_id= "";
	}

	if(array_key_exists("process",$project_task_boq_search_data))
	{
		$process = $project_task_boq_search_data["process"];
	}
	else
	{
		$process= "";
	}

	if(array_key_exists("task_master",$project_task_boq_search_data))
	{
		$task_master = $project_task_boq_search_data["task_master"];
	}
	else
	{
		$task_master= "";
	}

	if(array_key_exists("process_master",$project_task_boq_search_data))
	{
		$process_master = $project_task_boq_search_data["process_master"];
	}
	else
	{
		$process_master= "";
	}

	if(array_key_exists("contract_task",$project_task_boq_search_data))
	{
		$contract_task = $project_task_boq_search_data["contract_task"];
	}
	else
	{
		$contract_task= "";
	}


	if(array_key_exists("uom",$project_task_boq_search_data))
	{
		$uom = $project_task_boq_search_data["uom"];
	}
	else
	{
		$uom = "";
	}

	if(array_key_exists("location",$project_task_boq_search_data))
	{
		$location = $project_task_boq_search_data["location"];
	}
	else
	{
		$location = "";
	}

	if(array_key_exists("number",$project_task_boq_search_data))
	{
		$number = $project_task_boq_search_data["number"];
	}
	else
	{
		$number = "";
	}

	if(array_key_exists("length",$project_task_boq_search_data))
	{
		$length = $project_task_boq_search_data["length"];
	}
	else
	{
		$length = "";
	}

	if(array_key_exists("breadth",$project_task_boq_search_data))
	{
		$breadth = $project_task_boq_search_data["breadth"];
	}
	else
	{
		$breadth = "";
	}

	if(array_key_exists("depth",$project_task_boq_search_data))
	{
		$depth = $project_task_boq_search_data["depth"];
	}
	else
	{
		$depth = "";
	}

	if(array_key_exists("total_sqft",$project_task_boq_search_data))
	{
		$total_sqft = $project_task_boq_search_data["total_sqft"];
	}
	else
	{
		$total_sqft = "";
	}

	if(array_key_exists("amount",$project_task_boq_search_data))
	{
		$amount = $project_task_boq_search_data["amount"];
	}
	else
	{
		$amount = "";
	}

	if(array_key_exists("active",$project_task_boq_search_data))
	{
		$active = $project_task_boq_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_task_boq_search_data))
	{
		$added_by = $project_task_boq_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_task_boq_search_data))
	{
		$start_date= $project_task_boq_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_task_boq_search_data))
	{
		$end_date= $project_task_boq_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	if(array_key_exists("project",$project_task_boq_search_data))
	{
		$project = $project_task_boq_search_data["project"];
	}
	else
	{
		$project = "";
	}

	$get_project_task_boq_list_squery_base = "select * from project_task_boq PTQ inner join users U on U.user_id = PTQ.project_task_boq_added_by inner join stock_unit_measure_master SUM on SUM.stock_unit_id = PTQ.project_task_boq_uom inner join project_plan_process_task PPPT on PPPT.project_process_task_id = PTQ.project_task_id inner join project_task_master PTM on PTM.project_task_master_id=PPPT.project_process_task_type inner join project_contract_process PCP on PCP.project_contract_process_id = PTQ.project_task_contract_process inner join project_contract_rate_master PCRM on PCRM.project_contract_rate_master_id=PTQ.project_task_contract_task inner join project_plan_process PPP on PPP.project_plan_process_id=PPPT.project_process_id inner join project_plan PP on PP.project_plan_id=PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id=PP.project_plan_project_id inner join project_process_master PPM on PPM.project_process_master_id = PPP.project_plan_process_name inner join project_site_location_mapping_master PSLMM on PSLMM.project_site_location_mapping_master_id = PTQ.project_task_boq_location";

	$get_project_task_boq_list_squery_where = "";
	$get_project_task_boq_list_squery_order_by = " order by PCRM.project_contract_rate_master_work_task ASC";
	$filter_count = 0;

	// Data
	$get_project_task_boq_list_sdata = array();

	if($boq_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." where project_task_boq_id = :boq_id";
		}
		else
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." and project_task_boq_id = :boq_id";
		}

		// Data
		$get_project_task_boq_list_sdata[':boq_id'] = $boq_id;

		$filter_count++;
	}

	if($task_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." where project_task_id = :task_id";
		}
		else
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." and project_task_id = :task_id";
		}

		// Data
		$get_project_task_boq_list_sdata[':task_id'] = $task_id;

		$filter_count++;
	}

	if($process != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." where project_task_contract_process = :process";
		}
		else
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." and project_task_contract_process = :process";
		}

		// Data
		$get_project_task_boq_list_sdata[':process'] = $process;

		$filter_count++;
	}

	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." where PP.project_plan_project_id = :project";
		}
		else
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." and PP.project_plan_project_id = :project";
		}

		// Data
		$get_project_task_boq_list_sdata[':project'] = $project;

		$filter_count++;
	}

	if($task_master != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." where PPPT.project_process_task_type = :task_master";
		}
		else
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." and PPPT.project_process_task_type = :task_master";
		}

		// Data
		$get_project_task_boq_list_sdata[':task_master'] = $task_master;

		$filter_count++;
	}

	if($process_master != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." where PPP.project_plan_process_name = :process_master";
		}
		else
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." and PPP.project_plan_process_name = :process_master";
		}

		// Data
		$get_project_task_boq_list_sdata[':process_master'] = $process_master;

		$filter_count++;
	}

	if($contract_task != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." where project_task_contract_task = :contract_task";
		}
		else
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." and project_task_contract_task = :contract_task";
		}

		// Data
		$get_project_task_boq_list_sdata[':contract_task'] = $contract_task;

		$filter_count++;
	}

	if($location != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." where project_task_boq_location = :location";
		}
		else
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." and project_task_boq_location = :location";
		}

		// Data
		$get_project_task_boq_list_sdata[':location'] = $location;

		$filter_count++;
	}

	if($uom != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." where project_task_boq_uom = :uom";
		}
		else
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." and project_task_boq_uom = :uom";
		}

		// Data
		$get_project_task_boq_list_sdata[':uom'] = $uom;

		$filter_count++;
	}

	if($number != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." where project_task_boq_number = :number";
		}
		else
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." and project_task_boq_number = :number";
		}

		// Data
		$get_project_task_boq_list_sdata[':number'] = $number;

		$filter_count++;
	}

	if($length != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." where project_task_boq_length = :length";
		}
		else
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." and project_task_boq_length = :length";
		}

		// Data
		$get_project_task_boq_list_sdata[':length'] = $length;

		$filter_count++;
	}

	if($breadth != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." where project_task_boq_breadth = :breadth";
		}
		else
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." and project_task_boq_breadth = :breadth";
		}

		// Data
		$get_project_task_boq_list_sdata[':breadth'] = $breadth;

		$filter_count++;
	}

	if($depth != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." where project_task_boq_depth = :depth";
		}
		else
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." and project_task_boq_depth = :depth";
		}

		// Data
		$get_project_task_boq_list_sdata[':depth'] = $depth;

		$filter_count++;
	}

	if($total_sqft != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." where project_task_boq_total_measurement = :total_sqft";
		}
		else
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." and project_task_boq_total_measurement = :total_sqft";
		}

		// Data
		$get_project_task_boq_list_sdata[':total_sqft'] = $total_sqft;

		$filter_count++;
	}

	if($amount != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." where project_task_boq_amount = :amount";
		}
		else
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." and project_task_boq_amount = :amount";
		}

		// Data
		$get_project_task_boq_list_sdata[':amount'] = $amount;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." where project_task_boq_active = :active";
		}
		else
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." and project_task_boq_active = :active";
		}

		// Data
		$get_project_task_boq_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." where project_task_boq_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." and project_task_boq_added_by = :added_by";
		}

		//Data
		$get_project_task_boq_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." where project_task_boq_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." and project_task_boq_added_on >= :start_date";
		}

		//Data
		$get_project_task_boq_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." where project_task_boq_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." and project_task_boq_added_on <= :end_date";
		}

		//Data
		$get_project_task_boq_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	$get_project_task_boq_list_squery = $get_project_task_boq_list_squery_base.$get_project_task_boq_list_squery_where;

	try
	{
		$dbConnection = get_conn_handle();

		$get_project_task_boq_list_sstatement = $dbConnection->prepare($get_project_task_boq_list_squery);

		$get_project_task_boq_list_sstatement -> execute($get_project_task_boq_list_sdata);

		$get_project_task_boq_list_sdetails = $get_project_task_boq_list_sstatement -> fetchAll();

		if(FALSE === $get_project_task_boq_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_task_boq_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_task_boq_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

   /*
PURPOSE : To update Project Task BOQ
INPUT 	: BOQ ID, Project Task BOQ Update Array
OUTPUT 	: BOQ ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_task_boq($boq_id,$task_id,$project_task_boq_update_data)
{
	if(array_key_exists("process",$project_task_boq_update_data))
	{
		$process = $project_task_boq_update_data["process"];
	}
	else
	{
		$process = "";
	}

	if(array_key_exists("contract_task",$project_task_boq_update_data))
	{
		$contract_task = $project_task_boq_update_data["contract_task"];
	}
	else
	{
		$contract_task = "";
	}

	if(array_key_exists("uom",$project_task_boq_update_data))
	{
		$uom = $project_task_boq_update_data["uom"];
	}
	else
	{
		$uom = "";
	}

	if(array_key_exists("location",$project_task_boq_update_data))
	{
		$location = $project_task_boq_update_data["location"];
	}
	else
	{
		$location = "";
	}

	if(array_key_exists("number",$project_task_boq_update_data))
	{
		$number = $project_task_boq_update_data["number"];
	}
	else
	{
		$number = "";
	}

	if(array_key_exists("length",$project_task_boq_update_data))
	{
		$length = $project_task_boq_update_data["length"];
	}
	else
	{
		$length = "";
	}


	if(array_key_exists("breadth",$project_task_boq_update_data))
	{
		$breadth = $project_task_boq_update_data["breadth"];
	}
	else
	{
		$breadth = "";
	}


	if(array_key_exists("depth",$project_task_boq_update_data))
	{
		$depth = $project_task_boq_update_data["depth"];
	}
	else
	{
		$depth = "";
	}

	if(array_key_exists("total_sqft",$project_task_boq_update_data))
	{
		$total_sqft = $project_task_boq_update_data["total_sqft"];
	}
	else
	{
		$total_sqft = "";
	}

	if(array_key_exists("amount",$project_task_boq_update_data))
	{
		$amount = $project_task_boq_update_data["amount"];
	}
	else
	{
		$amount = "";
	}


	if(array_key_exists("active",$project_task_boq_update_data))
	{
		$active = $project_task_boq_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_task_boq_update_data))
	{
		$remarks = $project_task_boq_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_task_boq_update_data))
	{
		$added_by = $project_task_boq_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_task_boq_update_data))
	{
		$added_on = $project_task_boq_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_task_boq_update_uquery_base = "update project_task_boq set";

	$project_task_boq_update_uquery_set = "";

	$project_task_boq_update_uquery_where = " where project_task_boq_id = :boq_id or project_task_id = :task_id";

	$project_task_boq_update_udata = array(":boq_id"=>$boq_id,":task_id"=>$task_id);

	$filter_count = 0;

	if($process != "")
	{
		$project_task_boq_update_uquery_set = $project_task_boq_update_uquery_set." project_task_contract_process = :process,";
		$project_task_boq_update_udata[":process"] = $process;
		$filter_count++;
	}

	if($contract_task != "")
	{
		$project_task_boq_update_uquery_set = $project_task_boq_update_uquery_set." project_task_contract_task = :contract_task,";
		$project_task_boq_update_udata[":contract_task"] = $contract_task;
		$filter_count++;
	}

	if($uom != "")
	{
		$project_task_boq_update_uquery_set = $project_task_boq_update_uquery_set." project_task_boq_uom = :uom,";
		$project_task_boq_update_udata[":uom"] = $uom;
		$filter_count++;
	}

	if($location != "")
	{
		$project_task_boq_update_uquery_set = $project_task_boq_update_uquery_set." project_task_boq_location = :location,";
		$project_task_boq_update_udata[":location"] = $location;
		$filter_count++;
	}

	if($number != "")
	{
		$project_task_boq_update_uquery_set = $project_task_boq_update_uquery_set." project_task_boq_number = :number,";
		$project_task_boq_update_udata[":number"] = $number;
		$filter_count++;
	}

	if($length != "")
	{
		$project_task_boq_update_uquery_set = $project_task_boq_update_uquery_set." project_task_boq_length = :length,";
		$project_task_boq_update_udata[":length"] = $length;
		$filter_count++;
	}

	if($breadth != "")
	{
		$project_task_boq_update_uquery_set = $project_task_boq_update_uquery_set." project_task_boq_breadth = :breadth,";
		$project_task_boq_update_udata[":breadth"] = $breadth;
		$filter_count++;
	}

	if($depth != "")
	{
		$project_task_boq_update_uquery_set = $project_task_boq_update_uquery_set." project_task_boq_depth = :depth,";
		$project_task_boq_update_udata[":depth"] = $depth;
		$filter_count++;
	}

	if($total_sqft != "")
	{
		$project_task_boq_update_uquery_set = $project_task_boq_update_uquery_set." project_task_boq_total_measurement = :total_sqft,";
		$project_task_boq_update_udata[":total_sqft"] = $total_sqft;
		$filter_count++;
	}

	if($amount != "")
	{
		$project_task_boq_update_uquery_set = $project_task_boq_update_uquery_set." project_task_boq_amount = :amount,";
		$project_task_boq_update_udata[":amount"] = $amount;
		$filter_count++;
	}

	if($active != "")
	{
		$project_task_boq_update_uquery_set = $project_task_boq_update_uquery_set." project_task_boq_active = :active,";
		$project_task_boq_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_task_boq_update_uquery_set = $project_task_boq_update_uquery_set." project_task_boq_remarks = :remarks,";
		$project_task_boq_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_task_boq_update_uquery_set = $project_task_boq_update_uquery_set." project_task_boq_added_by = :added_by,";
		$project_task_boq_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_task_boq_update_uquery_set = $project_task_boq_update_uquery_set." project_task_boq_added_on = :added_on,";
		$project_task_boq_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_task_boq_update_uquery_set = trim($project_task_boq_update_uquery_set,',');
	}

	$project_task_boq_update_uquery = $project_task_boq_update_uquery_base.$project_task_boq_update_uquery_set.$project_task_boq_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();

        $project_task_boq_update_ustatement = $dbConnection->prepare($project_task_boq_update_uquery);

        $project_task_boq_update_ustatement -> execute($project_task_boq_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $boq_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Task BOQ Actuals
INPUT 	: UOM, Number, Remarks, Added By
OUTPUT 	: BOQ ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_task_actual_boq($task_id, $road_id, $vendor_id, $date, $process, $contract_task, $uom, $location, $number, $length, $breadth, $depth, $total_sqft, $amount, $total_amount, $work_type, $rework_completion, $remarks, $added_by)
{
	// Query
	$project_task_actual_boq_iquery = "insert into project_task_boq_actuals
	(project_boq_actual_task_id,project_task_actual_boq_road_id,project_task_actual_boq_vendor_id,project_task_actual_boq_date,project_task_boq_actual_contract_process,project_task_actual_contract_task,
	project_task_actual_boq_uom,project_task_actual_boq_location,project_task_boq_actual_number,project_task_actual_boq_length,project_task_actual_boq_breadth,
	project_task_actual_boq_depth,project_task_actual_boq_total_measurement,project_task_actual_boq_amount,project_task_actual_boq_lumpsum,project_task_actual_boq_completion,
	project_task_actual_boq_work_type,project_task_actual_boq_rework_completion,project_task_actual_boq_status,project_task_actual_boq_active,project_task_actual_boq_remarks,
	project_task_actual_boq_added_by,project_task_actual_boq_added_on)
	values (:task_id,:road_id,:vendor_id,:date,:process,:contract_task,:uom,:location,:number,:length,:breadth,:depth,:total_sqft,:amount,:lumpsum,:completion,:work_type,:rework_completion,
	:status,:active,:remarks,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_task_actual_boq_istatement = $dbConnection->prepare($project_task_actual_boq_iquery);

        // Data
        $project_task_actual_boq_idata = array(':task_id'=>$task_id,
                                               ':road_id'=>$road_id,
                                               ':vendor_id'=>$vendor_id,
                                               ':date'=>$date,
                                               ':process'=>$process,
                                               ':contract_task'=>$contract_task,
                                               ':uom'=>$uom,
                                               ':location'=>$location,
                                               ':number'=>$number,
                                               ':length'=>$length,
                                               ':breadth'=>$breadth,
                                               ':depth'=>$depth,
                                               ':total_sqft'=>$total_sqft,
                                               ':amount'=>$amount,
                                               ':lumpsum'=>$total_amount,
                                               ':completion'=>'0',
                                               ':work_type'=>$work_type,
                                               ':rework_completion'=>$rework_completion,
                                               ':status'=>'Pending',
                                               ':active'=>'1',
                                               ':remarks'=>$remarks,
                                               ':added_by'=>$added_by,
		                                           ':added_on'=>date("Y-m-d H:i:s")
                                             );
		$dbConnection->beginTransaction();
        $project_task_actual_boq_istatement->execute($project_task_actual_boq_idata);
		$project_task_boq_actual_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_task_boq_actual_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project Task BOQ List
INPUT 	: BOQ ID, UOM, Number, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Task BOQ
BY 		: Lakshmi
*/
function db_get_project_task_actual_boq($project_task_actual_boq_search_data)
{
	if(array_key_exists("boq_id",$project_task_actual_boq_search_data))
	{
		$boq_id = $project_task_actual_boq_search_data["boq_id"];
	}
	else
	{
		$boq_id = "";
	}
	if(array_key_exists("task_id",$project_task_actual_boq_search_data))
	{
		$task_id = $project_task_actual_boq_search_data["task_id"];
	}
	else
	{
		$task_id = "";
	}

	if(array_key_exists("work_type",$project_task_actual_boq_search_data))
	{
		$work_type = $project_task_actual_boq_search_data["work_type"];
	}
	else
	{
		$work_type = "";
	}

	if(array_key_exists("rework_completion",$project_task_actual_boq_search_data))
	{
		$rework_completion = $project_task_actual_boq_search_data["rework_completion"];
	}
	else
	{
		$rework_completion = "";
	}

	if(array_key_exists("vendor_id",$project_task_actual_boq_search_data))
	{
		$vendor_id = $project_task_actual_boq_search_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}

	if(array_key_exists("date",$project_task_actual_boq_search_data))
	{
		$date = $project_task_actual_boq_search_data["date"];
	}
	else
	{
		$date = "";
	}

	if(array_key_exists("boq_start_date",$project_task_actual_boq_search_data))
	{
		$boq_start_date = $project_task_actual_boq_search_data["boq_start_date"];
	}
	else
	{
		$boq_start_date = "";
	}

	if(array_key_exists("boq_end_date",$project_task_actual_boq_search_data))
	{
		$boq_end_date = $project_task_actual_boq_search_data["boq_end_date"];
	}
	else
	{
		$boq_end_date = "";
	}

	if(array_key_exists("process",$project_task_actual_boq_search_data))
	{
		$process = $project_task_actual_boq_search_data["process"];
	}
	else
	{
		$process = "";
	}

	if(array_key_exists("contract_task",$project_task_actual_boq_search_data))
	{
		$contract_task = $project_task_actual_boq_search_data["contract_task"];
	}
	else
	{
		$contract_task = "";
	}

	if(array_key_exists("location",$project_task_actual_boq_search_data))
	{
		$location = $project_task_actual_boq_search_data["location"];
	}
	else
	{
		$location = "";
	}

	if(array_key_exists("uom",$project_task_actual_boq_search_data))
	{
		$uom = $project_task_actual_boq_search_data["uom"];
	}
	else
	{
		$uom = "";
	}

	if(array_key_exists("number",$project_task_actual_boq_search_data))
	{
		$number = $project_task_actual_boq_search_data["number"];
	}
	else
	{
		$number = "";
	}

	if(array_key_exists("length",$project_task_actual_boq_search_data))
	{
		$length = $project_task_actual_boq_search_data["length"];
	}
	else
	{
		$length = "";
	}

	if(array_key_exists("breadth",$project_task_actual_boq_search_data))
	{
		$breadth = $project_task_actual_boq_search_data["breadth"];
	}
	else
	{
		$breadth = "";
	}

	if(array_key_exists("depth",$project_task_actual_boq_search_data))
	{
		$depth = $project_task_actual_boq_search_data["depth"];
	}
	else
	{
		$depth = "";
	}

	if(array_key_exists("total_sqft",$project_task_actual_boq_search_data))
	{
		$total_sqft = $project_task_actual_boq_search_data["total_sqft"];
	}
	else
	{
		$total_sqft = "";
	}

	if(array_key_exists("amount",$project_task_actual_boq_search_data))
	{
		$amount = $project_task_actual_boq_search_data["amount"];
	}
	else
	{
		$amount = "";
	}

	if(array_key_exists("status",$project_task_actual_boq_search_data))
	{
		$status = $project_task_actual_boq_search_data["status"];
	}
	else
	{
		$status = "";
	}

	if(array_key_exists("secondary_contract_status",$project_task_actual_boq_search_data))
	{
		$secondary_contract_status = $project_task_actual_boq_search_data["secondary_contract_status"];
	}
	else
	{
		$secondary_contract_status = "";
	}

	if(array_key_exists("active",$project_task_actual_boq_search_data))
	{
		$active = $project_task_actual_boq_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_task_actual_boq_search_data))
	{
		$added_by = $project_task_actual_boq_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_task_actual_boq_search_data))
	{
		$start_date= $project_task_actual_boq_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_task_actual_boq_search_data))
	{
		$end_date= $project_task_actual_boq_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	if(array_key_exists("project",$project_task_actual_boq_search_data))
	{
		$project= $project_task_actual_boq_search_data["project"];
	}
	else
	{
		$project= "";
	}

	if(array_key_exists("process_id",$project_task_actual_boq_search_data))
	{
		$process_id= $project_task_actual_boq_search_data["process_id"];
	}
	else
	{
		$process_id= "";
	}
	if(array_key_exists("percentage_pending",$project_task_actual_boq_search_data))
	{
		$percentage_pending= $project_task_actual_boq_search_data["percentage_pending"];
	}
	else
	{
		$percentage_pending= "";
	}

	if(array_key_exists("percentage_completion",$project_task_actual_boq_search_data))
	{
		$percentage_completion= $project_task_actual_boq_search_data["percentage_completion"];
	}
	else
	{
		$percentage_completion= "";
	}

	if(array_key_exists("max_contract_percentage",$project_task_actual_boq_search_data))
	{
		$max_contract_percentage= $project_task_actual_boq_search_data["max_contract_percentage"];
	}
	else
	{
		$max_contract_percentage= "";
	}

	$get_project_task_actual_boq_list_squery_base = "select *,U.user_name as added_by,AU.user_name as approved_by,PSLMM.project_site_location_mapping_master_name as location_name,APSL.project_site_location_mapping_master_name as road_name  from project_task_boq_actuals PTBA inner join users U on U.user_id = PTBA.project_task_actual_boq_added_by inner join stock_unit_measure_master SUM on SUM.stock_unit_id = PTBA.project_task_actual_boq_uom inner join project_plan_process_task PPPT on PPPT.project_process_task_id = PTBA.project_boq_actual_task_id inner join project_task_master PTM on PTM.project_task_master_id=PPPT.project_process_task_type inner join project_contract_process PCP on PCP.project_contract_process_id = PTBA.project_task_boq_actual_contract_process inner join project_contract_rate_master PCRM on PCRM.project_contract_rate_master_id=PTBA.project_task_actual_contract_task inner join project_plan_process PPP on PPP.project_plan_process_id=PPPT.project_process_id inner join project_plan PP on PP.project_plan_id=PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id=PP.project_plan_project_id inner join project_process_master PPM on PPM.project_process_master_id = PPP.project_plan_process_name inner join project_manpower_agency PMA on PMA.project_manpower_agency_id=PTBA.project_task_actual_boq_vendor_id left outer join users AU on AU.user_id=PTBA.project_task_actual_boq_approved_by left outer join project_site_location_mapping_master PSLMM on PSLMM.project_site_location_mapping_master_id = PTBA.project_task_actual_boq_location left outer join project_site_location_mapping_master APSL on APSL.project_site_location_mapping_master_id=PTBA.project_task_actual_boq_road_id";

	$get_project_task_actual_boq_list_squery_where = "";
	$filter_count = 0;

	// Data
	$get_project_task_actual_boq_list_sdata = array();

	if($boq_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_boq_actual_id = :boq_id";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_boq_actual_id = :boq_id";
		}

		// Data
		$get_project_task_actual_boq_list_sdata[':boq_id'] = $boq_id;

		$filter_count++;
	}

	if($task_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_boq_actual_task_id = :task_id";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_boq_actual_task_id = :task_id";
		}

		// Data
		$get_project_task_actual_boq_list_sdata[':task_id'] = $task_id;

		$filter_count++;
	}

	if($work_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_work_type = :work_type";

		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_work_type = :work_type";
		}

		// Data
		$get_project_task_actual_boq_list_sdata[':work_type'] = $work_type;

		$filter_count++;
	}

	if($rework_completion != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_rework_completion = :rework_completion";

		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_rework_completion = :rework_completion";
		}

		// Data
		$get_project_task_actual_boq_list_sdata[':rework_completion'] = $rework_completion;

		$filter_count++;
	}

	if($vendor_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_vendor_id = :vendor_id";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_vendor_id = :vendor_id";
		}

		// Data
		$get_project_task_actual_boq_list_sdata[':vendor_id'] = $vendor_id;

		$filter_count++;
	}

	if($date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_date = :date";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_date = :date";
		}

		// Data
		$get_project_task_actual_boq_list_sdata[':date'] = $date;

		$filter_count++;
	}

	if($boq_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_date >= :boq_start_date";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_date >= :boq_start_date";
		}

		//Data
		$get_project_task_actual_boq_list_sdata[':boq_start_date']  = $boq_start_date;

		$filter_count++;
	}
	if($boq_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_date <= :boq_end_date";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_date <= :boq_end_date";
		}

		//Data
		$get_project_task_actual_boq_list_sdata['boq_end_date']  = $boq_end_date;

		$filter_count++;
	}

	if($location != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_location = :location";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_location = :location";
		}

		// Data
		$get_project_task_actual_boq_list_sdata[':location'] = $location;

		$filter_count++;
	}

	if($process != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_boq_actual_contract_process = :process";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_boq_actual_contract_process = :process";
		}

		// Data
		$get_project_task_actual_boq_list_sdata[':process'] = $process;

		$filter_count++;
	}

	if($contract_task != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_contract_task = :contract_task";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_contract_task = :contract_task";
		}

		// Data
		$get_project_task_actual_boq_list_sdata[':contract_task'] = $contract_task;

		$filter_count++;
	}

	if($uom != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_uom = :uom";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_uom = :uom";
		}

		// Data
		$get_project_task_actual_boq_list_sdata[':uom'] = $uom;

		$filter_count++;
	}

	if($number != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_number = :number";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_number = :number";
		}

		// Data
		$get_project_task_actual_boq_list_sdata[':number'] = $number;

		$filter_count++;
	}

	if($length != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_length = :length";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_length = :length";
		}

		// Data
		$get_project_task_actual_boq_list_sdata[':length'] = $length;

		$filter_count++;
	}

	if($breadth != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_breadth = :breadth";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_breadth = :breadth";
		}

		// Data
		$get_project_task_actual_boq_list_sdata[':breadth'] = $breadth;

		$filter_count++;
	}

	if($depth != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_depth = :depth";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_depth = :depth";
		}

		// Data
		$get_project_task_actual_boq_list_sdata[':depth'] = $depth;

		$filter_count++;
	}

	if($total_sqft != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_total_measurement = :total_sqft";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_total_measurement = :total_sqft";
		}

		// Data
		$get_project_task_actual_boq_list_sdata[':total_sqft'] = $total_sqft;

		$filter_count++;
	}

	if($amount != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_amount = :amount";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_amount = :amount";
		}

		// Data
		$get_project_task_actual_boq_list_sdata[':amount'] = $amount;

		$filter_count++;
	}

	if($status != "")
	{
		if($secondary_contract_status != "")
		{
			if($filter_count == 0)
			{
				// Query
				$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where (project_task_actual_boq_status = :status or project_task_actual_boq_status = :secondary_contract_status)";
			}
			else
			{
				// Query
				$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and (project_task_actual_boq_status = :status or project_task_actual_boq_status = :secondary_contract_status)";
			}

			$get_project_task_actual_boq_list_sdata[':status'] = $status;
			$get_project_task_actual_boq_list_sdata[':secondary_contract_status'] = $secondary_contract_status;

			$filter_count++;

		}
		else
		{
			if($filter_count == 0)
			{
				// Query
				$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_status = :status";
			}
			else
			{
				// Query
				$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_status = :status";
			}
			// Data
			$get_project_task_actual_boq_list_sdata[':status'] = $status;

			$filter_count++;
		}
		// Data
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_active = :active";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_active = :active";
		}

		// Data
		$get_project_task_actual_boq_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_added_by = :added_by";
		}

		//Data
		$get_project_task_actual_boq_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_added_on >= :start_date";
		}

		//Data
		$get_project_task_actual_boq_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_added_on <= :end_date";
		}

		//Data
		$get_project_task_actual_boq_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where PP.project_plan_project_id = :project";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and PP.project_plan_project_id = :project";
		}

		//Data
		$get_project_task_actual_boq_list_sdata['project']  = $project;

		$filter_count++;
	}

	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where PPPT.project_process_id = :process_id";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and PPPT.project_process_id = :process_id";
		}

		//Data
		$get_project_task_actual_boq_list_sdata['process_id']  = $process_id;

		$filter_count++;
	}
	if($percentage_completion != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_completion = '100'";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_completion = '100'";
		}

		$filter_count++;
	}

	if($percentage_pending != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_completion < '100'";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_completion < '100'";
		}

		$filter_count++;
	}
	if($max_contract_percentage != "")
	{
		// Query
		$get_project_task_actual_boq_list_squery_base = "select max(project_task_actual_boq_completion)as max_cpercentage from project_task_boq_actuals";
	}
	$get_project_task_actual_boq_list_order_by = " order by project_task_actual_boq_added_on DESC";
	$get_project_task_actual_boq_list_squery = $get_project_task_actual_boq_list_squery_base.$get_project_task_actual_boq_list_squery_where.$get_project_task_actual_boq_list_order_by;

	try
	{
		$dbConnection = get_conn_handle();

		$get_project_task_actual_boq_list_sstatement = $dbConnection->prepare($get_project_task_actual_boq_list_squery);

		$get_project_task_actual_boq_list_sstatement -> execute($get_project_task_actual_boq_list_sdata);

		$get_project_task_actual_boq_list_sdetails = $get_project_task_actual_boq_list_sstatement -> fetchAll();

		if(FALSE === $get_project_task_actual_boq_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_task_actual_boq_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_task_actual_boq_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

  /*
PURPOSE : To update Project Task BOQ
INPUT 	: BOQ ID, Project Task BOQ Update Array
OUTPUT 	: BOQ ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_task_actual_boq($boq_id,$project_task_actual_boq_update_data)
{
	if(array_key_exists("task_id",$project_task_actual_boq_update_data))
	{
		$task_id = $project_task_actual_boq_update_data["task_id"];
	}
	else
	{
		$task_id = "";
	}

	if(array_key_exists("vendor_id",$project_task_actual_boq_update_data))
	{
		$vendor_id = $project_task_actual_boq_update_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}

	if(array_key_exists("date",$project_task_actual_boq_update_data))
	{
		$date = $project_task_actual_boq_update_data["date"];
	}
	else
	{
		$date = "";
	}

	if(array_key_exists("process",$project_task_actual_boq_update_data))
	{
		$process = $project_task_actual_boq_update_data["process"];
	}
	else
	{
		$process = "";
	}

	if(array_key_exists("contract_task",$project_task_actual_boq_update_data))
	{
		$contract_task = $project_task_actual_boq_update_data["contract_task"];
	}
	else
	{
		$contract_task = "";
	}

	if(array_key_exists("uom",$project_task_actual_boq_update_data))
	{
		$uom = $project_task_actual_boq_update_data["uom"];
	}
	else
	{
		$uom = "";
	}

	if(array_key_exists("number",$project_task_actual_boq_update_data))
	{
		$number = $project_task_actual_boq_update_data["number"];
	}
	else
	{
		$number = "";
	}

	if(array_key_exists("completion",$project_task_actual_boq_update_data))
	{
		$completion = $project_task_actual_boq_update_data["completion"];
	}
	else
	{
		$completion = "";
	}

	if(array_key_exists("length",$project_task_actual_boq_update_data))
	{
		$length = $project_task_actual_boq_update_data["length"];
	}
	else
	{
		$length = "";
	}


	if(array_key_exists("breadth",$project_task_actual_boq_update_data))
	{
		$breadth = $project_task_actual_boq_update_data["breadth"];
	}
	else
	{
		$breadth = "";
	}


	if(array_key_exists("depth",$project_task_actual_boq_update_data))
	{
		$depth = $project_task_actual_boq_update_data["depth"];
	}
	else
	{
		$depth = "";
	}

	if(array_key_exists("total_sqft",$project_task_actual_boq_update_data))
	{
		$total_sqft = $project_task_actual_boq_update_data["total_sqft"];
	}
	else
	{
		$total_sqft = "";
	}

	if(array_key_exists("amount",$project_task_actual_boq_update_data))
	{
		$amount = $project_task_actual_boq_update_data["amount"];
	}
	else
	{
		$amount = "";
	}

	if(array_key_exists("total_amount",$project_task_actual_boq_update_data))
	{
		$total_amount = $project_task_actual_boq_update_data["total_amount"];
	}
	else
	{
		$total_amount = "";
	}

	if(array_key_exists("status",$project_task_actual_boq_update_data))
	{
		$status = $project_task_actual_boq_update_data["status"];
	}
	else
	{
		$status = "";
	}


	if(array_key_exists("active",$project_task_actual_boq_update_data))
	{
		$active = $project_task_actual_boq_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_task_actual_boq_update_data))
	{
		$remarks = $project_task_actual_boq_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_task_actual_boq_update_data))
	{
		$added_by = $project_task_actual_boq_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_task_actual_boq_update_data))
	{
		$added_on = $project_task_actual_boq_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	if(array_key_exists("checked_by",$project_task_actual_boq_update_data))
	{
		$checked_by = $project_task_actual_boq_update_data["checked_by"];
	}
	else
	{
		$checked_by = "";
	}

	if(array_key_exists("checked_on",$project_task_actual_boq_update_data))
	{
		$checked_on = $project_task_actual_boq_update_data["checked_on"];
	}
	else
	{
		$checked_on = "";
	}

  if(array_key_exists("status1",$project_task_actual_boq_update_data))
  {
    $status1 = $project_task_actual_boq_update_data["status1"];
  }
  else
  {
    $status1 = "";
  }


	if(array_key_exists("approved_by",$project_task_actual_boq_update_data))
	{
		$approved_by = $project_task_actual_boq_update_data["approved_by"];
	}
	else
	{
		$approved_by = "";
	}

	if(array_key_exists("approved_on",$project_task_actual_boq_update_data))
	{
		$approved_on = $project_task_actual_boq_update_data["approved_on"];
	}
	else
	{
		$approved_on = "";
	}

	if(array_key_exists("work_type",$project_task_actual_boq_update_data))
	{
		$work_type = $project_task_actual_boq_update_data["work_type"];
	}
	else
	{
		$work_type = "";
	}

	if(array_key_exists("rework_completion",$project_task_actual_boq_update_data))
	{
		$rework_completion = $project_task_actual_boq_update_data["rework_completion"];
	}
	else
	{
		$rework_completion = "";
	}

	// Query
    $project_task_actual_boq_update_uquery_base = "update project_task_boq_actuals set ";

	$project_task_actual_boq_update_uquery_set = "";

	$project_task_actual_boq_update_uquery_where = " where project_task_boq_actual_id = :boq_id";

	$project_task_actual_boq_update_udata = array(":boq_id"=>$boq_id);

	$filter_count = 0;


	if($task_id != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_boq_actual_task_id = :task_id,";
		$project_task_actual_boq_update_udata[":task_id"] = $task_id;
		$filter_count++;
	}

	if($vendor_id != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_boq_vendor_id = :vendor_id,";
		$project_task_actual_boq_update_udata[":vendor_id"] = $vendor_id;
		$filter_count++;
	}

	if($date != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_boq_date = :date,";
		$project_task_actual_boq_update_udata[":date"] = $date;
		$filter_count++;
	}

	if($process != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_boq_actual_contract_process = :process,";
		$project_task_actual_boq_update_udata[":process"] = $process;
		$filter_count++;
	}

	if($contract_task != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_contract_task = :contract_task,";
		$project_task_actual_boq_update_udata[":contract_task"] = $contract_task;
		$filter_count++;
	}

	if($uom != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_boq_uom = :uom,";
		$project_task_actual_boq_update_udata[":uom"] = $uom;
		$filter_count++;
	}

	if($number != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_boq_actual_number = :number,";
		$project_task_actual_boq_update_udata[":number"] = $number;
		$filter_count++;
	}

	if($completion != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_boq_completion = :completion,";
		$project_task_actual_boq_update_udata[":completion"] = $completion;
		$filter_count++;
	}

	if($length != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_boq_length = :length,";
		$project_task_actual_boq_update_udata[":length"] = $length;
		$filter_count++;
	}

	if($breadth != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_boq_breadth = :breadth,";
		$project_task_actual_boq_update_udata[":breadth"] = $breadth;
		$filter_count++;
	}

	if($depth != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_boq_depth = :depth,";
		$project_task_actual_boq_update_udata[":depth"] = $depth;
		$filter_count++;
	}

	if($total_sqft != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_boq_total_measurement = :total_sqft,";
		$project_task_actual_boq_update_udata[":total_sqft"] = $total_sqft;
		$filter_count++;
	}

	if($amount != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_boq_amount = :amount,";
		$project_task_actual_boq_update_udata[":amount"] = $amount;
		$filter_count++;
	}

	if($total_amount != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_boq_lumpsum = :tot_amount,";
		$project_task_actual_boq_update_udata[":tot_amount"] = $total_amount;
		$filter_count++;
	}

	if($status != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_boq_status = :status,";
		$project_task_actual_boq_update_udata[":status"] = $status;
		$filter_count++;
	}
	if($active != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_boq_active = :active,";
		$project_task_actual_boq_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_boq_remarks = :remarks,";
		$project_task_actual_boq_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_boq_added_by = :added_by,";
		$project_task_actual_boq_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_boq_added_on = :added_on,";
		$project_task_actual_boq_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_boq_checked_on = :added_by,";
		$project_task_actual_boq_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($checked_by != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_boq_checked_by = :checked_by,";
		$project_task_actual_boq_update_udata[":checked_by"] = $checked_by;
		$filter_count++;
	}

  if($status1 != "")
  {
    $project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_boq_bill_status = :status1,";
    $project_task_actual_boq_update_udata[":status1"] = $status1;
    $filter_count++;
  }

	if($approved_by != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_boq_approved_by = :approved_by,";
		$project_task_actual_boq_update_udata[":approved_by"] = $approved_by;
		$filter_count++;
	}

	if($approved_on != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_boq_approved_on = :approved_on,";
		$project_task_actual_boq_update_udata[":approved_on"] = $approved_on;
		$filter_count++;
	}

	if($work_type != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_boq_work_type = :work_type,";
		$project_task_actual_boq_update_udata[":work_type"] = $work_type;
		$filter_count++;
	}

	if($rework_completion != "")
	{
		$project_task_actual_boq_update_uquery_set = $project_task_actual_boq_update_uquery_set." project_task_actual_boq_rework_completion = :rework_completion,";
		$project_task_actual_boq_update_udata[":rework_completion"] = $rework_completion;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_task_actual_boq_update_uquery_set = trim($project_task_actual_boq_update_uquery_set,',');
	}

	$project_task_boq_update_uquery = $project_task_actual_boq_update_uquery_base.$project_task_actual_boq_update_uquery_set.$project_task_actual_boq_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();

        $project_task_actual_boq_update_ustatement = $dbConnection->prepare($project_task_boq_update_uquery);

        $project_task_actual_boq_update_ustatement -> execute($project_task_actual_boq_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $boq_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Actual Payment ManPower
INPUT 	: Task  Manpower ID, Vendor ID, Amount, From Date, To Date, Remarks, Added By
OUTPUT 	: ManPower ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_actual_payment_manpower($task_manpower_id,$vendor_id,$amount,$man_hrs,$bill_no,$from_date,$to_date,$remarks,$added_by)
{
	// Query
   $project_actual_payment_manpower_iquery = "insert into project_actual_payment_manpower(project_actual_payment_task_manpower_id,project_actual_payment_manpower_vendor_id,project_actual_payment_manpower_amount,
  project_actual_payment_manpower_hrs,project_actual_payment_manpower_status,project_actual_payment_manpower_bill_no,project_actual_payment_manpower_from_date,
  project_actual_payment_manpower_to_date,project_actual_payment_manpower_active,project_actual_payment_manpower_remarks,
  project_actual_payment_manpower_added_by,project_actual_payment_manpower_added_on)
	values(:task_manpower_id,:vendor_id,:amount,:man_hrs,:status,:bill_no,:from_date,:to_date,:active,:remarks,:added_by,:added_on)";
    try
    {
        $dbConnection = get_conn_handle();
        $project_actual_payment_manpower_istatement = $dbConnection->prepare($project_actual_payment_manpower_iquery);

        // Data
        $project_actual_payment_manpower_idata = array(':task_manpower_id'=>$task_manpower_id,':vendor_id'=>$vendor_id,':amount'=>$amount,':man_hrs'=>$man_hrs,':status'=>'Pending',':bill_no'=>$bill_no,
		':from_date'=>$from_date,':to_date'=>$to_date,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_actual_payment_manpower_istatement->execute($project_actual_payment_manpower_idata);
		$project_actual_payment_manpower_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_actual_payment_manpower_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project Actual Payment ManPower list
INPUT 	: ManPower ID, Vendor ID, Amount, Status, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of project Process Task
BY 		: Lakshmi
*/
function db_get_project_actual_payment_manpower($project_actual_payment_manpower_search_data)
{
	if(array_key_exists("manpower_id",$project_actual_payment_manpower_search_data))
	{
		$manpower_id = $project_actual_payment_manpower_search_data["manpower_id"];
	}
	else
	{
		$manpower_id= "";
	}

	if(array_key_exists("task_manpower_id",$project_actual_payment_manpower_search_data))
	{
		$task_manpower_id = $project_actual_payment_manpower_search_data["task_manpower_id"];
	}
	else
	{
		$task_manpower_id= "";
	}

	if(array_key_exists("vendor_id",$project_actual_payment_manpower_search_data))
	{
		$vendor_id = $project_actual_payment_manpower_search_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}
	if(array_key_exists("amount",$project_actual_payment_manpower_search_data))
	{
		$amount = $project_actual_payment_manpower_search_data["amount"];
	}
	else
	{
		$amount = "";
	}

	if(array_key_exists("status",$project_actual_payment_manpower_search_data))
	{
		$status = $project_actual_payment_manpower_search_data["status"];
	}
	else
	{
		$status = "";
	}

	if(array_key_exists("secondary_status",$project_actual_payment_manpower_search_data))
	{
		$secondary_status = $project_actual_payment_manpower_search_data["secondary_status"];
	}
	else
	{
		$secondary_status = "";
	}

	if(array_key_exists("from_date",$project_actual_payment_manpower_search_data))
	{
		$from_date = $project_actual_payment_manpower_search_data["from_date"];
	}
	else
	{
		$from_date = "";
	}

	if(array_key_exists("to_date",$project_actual_payment_manpower_search_data))
	{
		$to_date = $project_actual_payment_manpower_search_data["to_date"];
	}
	else
	{
		$to_date = "";
	}

	if(array_key_exists("active",$project_actual_payment_manpower_search_data))
	{
		$active = $project_actual_payment_manpower_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_actual_payment_manpower_search_data))
	{
		$added_by = $project_actual_payment_manpower_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_actual_payment_manpower_search_data))
	{
		$start_date= $project_actual_payment_manpower_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_actual_payment_manpower_search_data))
	{
		$end_date= $project_actual_payment_manpower_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	if(array_key_exists("start",$project_actual_payment_manpower_search_data))
	{
		$start= $project_actual_payment_manpower_search_data["start"];
	}
	else
	{
		$start= "";
	}

	if(array_key_exists("limit",$project_actual_payment_manpower_search_data))
	{
		$limit= $project_actual_payment_manpower_search_data["limit"];
	}
	else
	{
		$limit= "";
	}

	// $get_project_actual_payment_manpower_list_squery_base = " select *,U.user_name as added_by,AU.user_name as approved_by from project_actual_payment_manpower PAPMP inner join users U on U.user_id = PAPMP.project_actual_payment_manpower_added_by inner join project_manpower_agency PMA on PMA.project_manpower_agency_id = PAPMP.project_actual_payment_manpower_vendor_id left outer join stock_company_master SCM on SCM.stock_company_master_id=PAPMP.project_actual_payment_manpower_billing_address left outer join users AU on AU.user_id=PAPMP.project_actual_payment_manpower_approved_by inner join project_payment_manpower_mapping PPMM on PPMM.project_payment_manpower_mapping_payment_id=PAPMP.project_actual_payment_manpower_id";

  $get_project_actual_payment_manpower_list_squery_base = " select *,U.user_name as added_by,AU.user_name as approved_by from project_actual_payment_manpower PAPMP inner join users U on U.user_id = PAPMP.project_actual_payment_manpower_added_by inner join project_manpower_agency PMA on PMA.project_manpower_agency_id = PAPMP.project_actual_payment_manpower_vendor_id left outer join stock_company_master SCM on SCM.stock_company_master_id=PAPMP.project_actual_payment_manpower_billing_address left outer join users AU on AU.user_id=PAPMP.project_actual_payment_manpower_approved_by";


	$get_project_actual_payment_manpower_list_squery_where = "";
	$get_project_actual_payment_manpower_list_squery_limit = "";

	$filter_count = 0;

	// Data
	$get_project_actual_payment_manpower_list_sdata = array();

	if($manpower_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." where project_actual_payment_manpower_id = :manpower_id";
		}
		else
		{
			// Query
			$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." and project_actual_payment_manpower_id = :manpower_id";
		}

		// Data
		$get_project_actual_payment_manpower_list_sdata[':manpower_id'] = $manpower_id;

		$filter_count++;
	}

	if($task_manpower_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." where project_actual_payment_task_manpower_id = :task_manpower_id";
		}
		else
		{
			// Query
			$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." and project_actual_payment_task_manpower_id = :task_manpower_id";
		}

		// Data
		$get_project_actual_payment_manpower_list_sdata[':task_manpower_id'] = $task_manpower_id;

		$filter_count++;
	}

	if($vendor_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." where project_actual_payment_manpower_vendor_id = :vendor_id";
		}
		else
		{
			// Query
			$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." and project_actual_payment_manpower_vendor_id = :vendor_id";
		}

		// Data
		$get_project_actual_payment_manpower_list_sdata[':vendor_id'] = $vendor_id;

		$filter_count++;
	}

	if($amount != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." where project_actual_payment_manpower_amount = :amount";
		}
		else
		{
			// Query
			$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." and project_actual_payment_manpower_amount = :amount";
		}

		// Data
		$get_project_actual_payment_manpower_list_sdata[':amount'] = $amount;

		$filter_count++;
	}

	if($status != "")
	{
		if($secondary_status != "")
		{
			if($filter_count == 0)
			{
				// Query
				$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." where (project_actual_payment_manpower_status = :status or project_actual_payment_manpower_status = :secondary_status)";
			}
			else
			{
				// Query
				$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." and (project_actual_payment_manpower_status = :status or project_actual_payment_manpower_status = :secondary_status)";
			}

			$get_project_actual_payment_manpower_list_sdata[':status'] = $status;
			$get_project_actual_payment_manpower_list_sdata[':secondary_status'] = $secondary_status;

			$filter_count++;

		}
		else
		{
			if($filter_count == 0)
			{
				// Query
				$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." where project_actual_payment_manpower_status = :status";
			}
			else
			{
				// Query
				$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." and project_actual_payment_manpower_status = :status";
			}
			// Data
			$get_project_actual_payment_manpower_list_sdata[':status'] = $status;

			$filter_count++;
		}
		// Data
	}

	if($from_date != "")
	{
		if($filter_count == 0)
		{
			// Query
		$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." where project_actual_payment_manpower_from_date = :from_date";
		}
		else
		{
			// Query
		$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." and project_actual_payment_manpower_from_date = :from_date";
		}

		// Data
		$get_project_actual_payment_manpower_list_sdata[':from_date']  = $from_date;

		$filter_count++;
	}

		if($to_date != "")
	{
		if($filter_count == 0)
		{
			// Query
		$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." where project_actual_payment_manpower_to_date = :to_date";
		}
		else
		{
			// Query
		$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." and project_actual_payment_manpower_to_date = :to_date";
		}

		// Data
		$get_project_actual_payment_manpower_list_sdata[':to_date']  = $to_date;

		$filter_count++;
	}

		if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
		$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." where project_actual_payment_manpower_active = :active";
		}
		else
		{
			// Query
		$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." and project_actual_payment_manpower_active = :active";
		}

		// Data
		$get_project_actual_payment_manpower_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." where project_actual_payment_manpower_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." and project_actual_payment_manpower_added_by = :added_by";
		}

		//Data
		$get_project_actual_payment_manpower_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." where project_actual_payment_manpower_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." and project_actual_payment_manpower_added_on >= :start_date";
		}

		//Data
		$get_project_actual_payment_manpower_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." where project_actual_payment_manpower_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." and project_actual_payment_manpower_added_on <= :end_date";
		}

		//Data
		$get_project_actual_payment_manpower_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	if(array_key_exists("sort",$project_actual_payment_manpower_search_data))
	{
		if($project_actual_payment_manpower_search_data['sort'] == '1')
		{
			$get_manpower_list_squery_order = " order by project_actual_payment_manpower_id desc limit 0,1";
		}
		else
		{
			$get_manpower_list_squery_order = " order by project_actual_payment_manpower_added_on DESC";
		}
	}
  else{
    $get_manpower_list_squery_order = " order by project_actual_payment_manpower_added_on DESC";
  }

	if(array_key_exists("empty_check",$project_actual_payment_manpower_search_data))
	{
		if($filter_count == 0)
		{
			$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." where project_actual_payment_manpower_bill_no != ''";
		}
		else
		{
			$get_project_actual_payment_manpower_list_squery_where = $get_project_actual_payment_manpower_list_squery_where." and project_actual_payment_manpower_bill_no != ''";
		}
		$filter_count++;
	}

	if($start >= 0)
	{
			// Query
			$get_project_actual_payment_manpower_list_squery_limit = $get_project_actual_payment_manpower_list_squery_limit." limit $start,$limit";
	}
	else
	{
		// Query
		$get_project_actual_payment_manpower_list_squery_limit = "";
	}

	$get_project_actual_payment_manpower_list_squery = $get_project_actual_payment_manpower_list_squery_base.$get_project_actual_payment_manpower_list_squery_where.$get_manpower_list_squery_order.$get_project_actual_payment_manpower_list_squery_limit;
	try
	{
		$dbConnection = get_conn_handle();

		$get_project_actual_payment_manpower_list_sstatement = $dbConnection->prepare($get_project_actual_payment_manpower_list_squery);

		$get_project_actual_payment_manpower_list_sstatement -> execute($get_project_actual_payment_manpower_list_sdata);

		$get_project_actual_payment_manpower_list_sdetails = $get_project_actual_payment_manpower_list_sstatement -> fetchAll();

		if(FALSE === $get_project_actual_payment_manpower_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_actual_payment_manpower_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_actual_payment_manpower_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

 /*
PURPOSE : To update Project Actual Payment ManPower
INPUT 	: ManPower ID, Project Actual Payment ManPower Update Array
OUTPUT 	: ManPower ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_actual_payment_manpower($manpower_id,$project_actual_payment_manpower_update_data)
{

	if(array_key_exists("task_manpower_id",$project_actual_payment_manpower_update_data))
	{
		$task_manpower_id = $project_actual_payment_manpower_update_data["task_manpower_id"];
	}
	else
	{
		$task_manpower_id = "";
	}

	if(array_key_exists("vendor_id",$project_actual_payment_manpower_update_data))
	{
		$vendor_id = $project_actual_payment_manpower_update_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}

	if(array_key_exists("amount",$project_actual_payment_manpower_update_data))
	{
		$amount = $project_actual_payment_manpower_update_data["amount"];
	}
	else
	{
		$amount = "";
	}

	if(array_key_exists("tds",$project_actual_payment_manpower_update_data))
	{
		$tds = $project_actual_payment_manpower_update_data["tds"];
	}
	else
	{
		$tds = "";
	}

	if(array_key_exists("no_of_hours",$project_actual_payment_manpower_update_data))
	{
		$no_of_hours = $project_actual_payment_manpower_update_data["no_of_hours"];
	}
	else
	{
		$no_of_hours = "";
	}

	if(array_key_exists("men_hrs",$project_actual_payment_manpower_update_data))
	{
		$men_hrs = $project_actual_payment_manpower_update_data["men_hrs"];
	}
	else
	{
		$men_hrs = "";
	}

	if(array_key_exists("women_hrs",$project_actual_payment_manpower_update_data))
	{
		$women_hrs = $project_actual_payment_manpower_update_data["women_hrs"];
	}
	else
	{
		$women_hrs = "";
	}

	if(array_key_exists("mason_hrs",$project_actual_payment_manpower_update_data))
	{
		$mason_hrs = $project_actual_payment_manpower_update_data["mason_hrs"];
	}
	else
	{
		$mason_hrs = "";
	}

	if(array_key_exists("others_hrs",$project_actual_payment_manpower_update_data))
	{
		$others_hrs = $project_actual_payment_manpower_update_data["others_hrs"];
	}
	else
	{
		$others_hrs = "";
	}


	if(array_key_exists("status",$project_actual_payment_manpower_update_data))
	{
		$status = $project_actual_payment_manpower_update_data["status"];
	}
	else
	{
		$status = "";
	}

	if(array_key_exists("bill_no",$project_actual_payment_manpower_update_data))
	{
		$bill_no = $project_actual_payment_manpower_update_data["bill_no"];
	}
	else
	{
		$bill_no = "";
	}

	if(array_key_exists("billing_address",$project_actual_payment_manpower_update_data))
	{
		$billing_address = $project_actual_payment_manpower_update_data["billing_address"];
	}
	else
	{
		$billing_address = "";
	}

	if(array_key_exists("from_date",$project_actual_payment_manpower_update_data))
	{
		$from_date = $project_actual_payment_manpower_update_data["from_date"];
	}
	else
	{
		$from_date = "";
	}

	if(array_key_exists("to_date",$project_actual_payment_manpower_update_data))
	{
		$to_date = $project_actual_payment_manpower_update_data["to_date"];
	}
	else
	{
		$to_date = "";
	}

	if(array_key_exists("active",$project_actual_payment_manpower_update_data))
	{
		$active = $project_actual_payment_manpower_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_actual_payment_manpower_update_data))
	{
		$remarks = $project_actual_payment_manpower_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("accepted_by",$project_actual_payment_manpower_update_data))
	{
		$accepted_by = $project_actual_payment_manpower_update_data["accepted_by"];
	}
	else
	{
		$accepted_by = "";
	}

	if(array_key_exists("accepted_on",$project_actual_payment_manpower_update_data))
	{
		$accepted_on = $project_actual_payment_manpower_update_data["accepted_on"];
	}
	else
	{
		$accepted_on = "";
	}

	if(array_key_exists("approved_on",$project_actual_payment_manpower_update_data))
	{
		$approved_on = $project_actual_payment_manpower_update_data["approved_on"];
	}
	else
	{
		$approved_on = "";
	}

	if(array_key_exists("approved_by",$project_actual_payment_manpower_update_data))
	{
		$approved_by = $project_actual_payment_manpower_update_data["approved_by"];
	}
	else
	{
		$approved_by = "";
	}

	if(array_key_exists("approved_on",$project_actual_payment_manpower_update_data))
	{
		$approved_on = $project_actual_payment_manpower_update_data["approved_on"];
	}
	else
	{
		$approved_on = "";
	}

	if(array_key_exists("added_by",$project_actual_payment_manpower_update_data))
	{
		$added_by = $project_actual_payment_manpower_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_actual_payment_manpower_update_data))
	{
		$added_on = $project_actual_payment_manpower_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_actual_payment_manpower_update_uquery_base = "update project_actual_payment_manpower set";

	$project_actual_payment_manpower_update_uquery_set = "";

	$project_actual_payment_manpower_update_uquery_where = " where project_actual_payment_manpower_id = :manpower_id";

	$project_actual_payment_manpower_update_udata = array(":manpower_id"=>$manpower_id);

	$filter_count = 0;

	if($task_manpower_id != "")
	{
		$project_actual_payment_manpower_update_uquery_set = $project_actual_payment_manpower_update_uquery_set." project_actual_payment_task_manpower_id = :task_manpower_id,";
		$project_actual_payment_manpower_update_udata[":task_manpower_id"] = $task_manpower_id;
		$filter_count++;
	}

	if($vendor_id != "")
	{
		$project_actual_payment_manpower_update_uquery_set = $project_actual_payment_manpower_update_uquery_set." project_actual_payment_manpower_vendor_id = :vendor_id,";
		$project_actual_payment_manpower_update_udata[":vendor_id"] = $vendor_id;
		$filter_count++;
	}
	if($amount != "")
	{
		$project_actual_payment_manpower_update_uquery_set = $project_actual_payment_manpower_update_uquery_set." project_actual_payment_manpower_amount = :amount,";
		$project_actual_payment_manpower_update_udata[":amount"] = $amount;
		$filter_count++;
	}

	if($tds != "")
	{
		$project_actual_payment_manpower_update_uquery_set = $project_actual_payment_manpower_update_uquery_set." project_actual_payment_manpower_tds = :tds,";
		$project_actual_payment_manpower_update_udata[":tds"] = $tds;
		$filter_count++;
	}

	if($no_of_hours != "")
	{
		$project_actual_payment_manpower_update_uquery_set = $project_actual_payment_manpower_update_uquery_set." project_actual_payment_manpower_hrs = :no_of_hours,";
		$project_actual_payment_manpower_update_udata[":no_of_hours"] = $no_of_hours;
		$filter_count++;
	}

	if($men_hrs != "")
	{
		$project_actual_payment_manpower_update_uquery_set = $project_actual_payment_manpower_update_uquery_set." project_actual_payment_men_hrs = :men_hrs,";
		$project_actual_payment_manpower_update_udata[":men_hrs"] = $men_hrs;
		$filter_count++;
	}

	if($women_hrs != "")
	{
		$project_actual_payment_manpower_update_uquery_set = $project_actual_payment_manpower_update_uquery_set." project_actual_payment_women_hrs = :women_hrs,";
		$project_actual_payment_manpower_update_udata[":women_hrs"] = $women_hrs;
		$filter_count++;
	}

	if($mason_hrs != "")
	{
		$project_actual_payment_manpower_update_uquery_set = $project_actual_payment_manpower_update_uquery_set." project_actual_payment_mason_hrs = :mason_hrs,";
		$project_actual_payment_manpower_update_udata[":mason_hrs"] = $mason_hrs;
		$filter_count++;
	}

	if($others_hrs != "")
	{
		$project_actual_payment_manpower_update_uquery_set = $project_actual_payment_manpower_update_uquery_set." project_actual_payment_others_hrs = :others_hrs,";
		$project_actual_payment_manpower_update_udata[":others_hrs"] = $others_hrs;
		$filter_count++;
	}

	if($status	!= "")
	{
		$project_actual_payment_manpower_update_uquery_set = $project_actual_payment_manpower_update_uquery_set." project_actual_payment_manpower_status = :status,";
		$project_actual_payment_manpower_update_udata[":status"] = $status;
		$filter_count++;
	}

	if($bill_no	!= "")
	{
		$project_actual_payment_manpower_update_uquery_set = $project_actual_payment_manpower_update_uquery_set." project_actual_payment_manpower_bill_no = :bill_no,";
		$project_actual_payment_manpower_update_udata[":bill_no"] = $bill_no;
		$filter_count++;
	}

	if($billing_address	!= "")
	{
		$project_actual_payment_manpower_update_uquery_set = $project_actual_payment_manpower_update_uquery_set." project_actual_payment_manpower_billing_address = :billing_address,";
		$project_actual_payment_manpower_update_udata[":billing_address"] = $billing_address;
		$filter_count++;
	}


	if($from_date != "")
	{
		$project_actual_payment_manpower_update_uquery_set = $project_actual_payment_manpower_update_uquery_set." project_actual_payment_manpower_from_date = :from_date,";
		$project_actual_payment_manpower_update_udata[":from_date"] = $from_date;
		$filter_count++;
	}

	if($to_date != "")
	{
		$project_actual_payment_manpower_update_uquery_set = $project_actual_payment_manpower_update_uquery_set." project_actual_payment_manpower_to_date = :to_date,";
		$project_actual_payment_manpower_update_udata[":to_date"] = $to_date;
		$filter_count++;
	}

	if($active != "")
	{
		$project_actual_payment_manpower_update_uquery_set = $project_actual_payment_manpower_update_uquery_set." project_actual_payment_manpower_active = :active,";
		$project_actual_payment_manpower_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_actual_payment_manpower_update_uquery_set = $project_actual_payment_manpower_update_uquery_set." project_actual_payment_manpower_remarks = :remarks,";
		$project_actual_payment_manpower_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($accepted_by != "")
	{
		$project_actual_payment_manpower_update_uquery_set = $project_actual_payment_manpower_update_uquery_set." project_actual_payment_manpower_accepted_by = :accepted_by,";
		$project_actual_payment_manpower_update_udata[":accepted_by"] = $accepted_by;
		$filter_count++;
	}

	if($accepted_on != "")
	{
		$project_actual_payment_manpower_update_uquery_set = $project_actual_payment_manpower_update_uquery_set." project_actual_payment_manpower_accepted_on = :accepted_on,";
		$project_actual_payment_manpower_update_udata[":accepted_on"] = $accepted_on;
		$filter_count++;
	}

	if($approved_by != "")
	{
		$project_actual_payment_manpower_update_uquery_set = $project_actual_payment_manpower_update_uquery_set." project_actual_payment_manpower_approved_by = :approved_by,";
		$project_actual_payment_manpower_update_udata[":approved_by"] = $approved_by;
		$filter_count++;
	}

	if($approved_on != "")
	{
		$project_actual_payment_manpower_update_uquery_set = $project_actual_payment_manpower_update_uquery_set." project_actual_payment_manpower_approved_on = :approved_on,";
		$project_actual_payment_manpower_update_udata[":approved_on"] = $approved_on;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_actual_payment_manpower_update_uquery_set = $project_actual_payment_manpower_update_uquery_set." project_actual_payment_manpower_added_by = :added_by,";
		$project_actual_payment_manpower_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_actual_payment_manpower_update_uquery_set = $project_actual_payment_manpower_update_uquery_set." project_actual_payment_manpower_added_on = :added_on,";
		$project_actual_payment_manpower_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_actual_payment_manpower_update_uquery_set = trim($project_actual_payment_manpower_update_uquery_set,',');
	}

	$project_actual_payment_manpower_update_uquery = $project_actual_payment_manpower_update_uquery_base.$project_actual_payment_manpower_update_uquery_set.$project_actual_payment_manpower_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();

        $project_actual_payment_manpower_update_ustatement = $dbConnection->prepare($project_actual_payment_manpower_update_uquery);

        $project_actual_payment_manpower_update_ustatement -> execute($project_actual_payment_manpower_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $manpower_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Manpower Issue Payment
INPUT 	: Payment ID ,Man Power ID, Amount, Vendor ID, Payment Mode, Active, Remarks, Added By, Added on
OUTPUT 	: Payment ID, success or failure message
BY 		: Ashwini
*/
function db_add_project_man_power_issue_payment($man_power_id,$amount,$deduction,$vendor_id,$payment_mode,$instrument_details,$remarks,$added_by)
{
	// Query
   $project_man_power_issue_payment_iquery = "insert into project_man_power_issue_payment(		      			project_man_power_issue_payment_man_power_id,project_man_power_issue_payment_amount,project_man_power_issue_payment_deduction,project_man_power_issue_payment_vendor_id,
	project_man_power_issue_payment_mode,project_man_power_issue_payment_instrument_details,project_man_power_issue_payment_active,
	project_man_power_issue_payment_remarks,project_man_power_issue_payment_added_by,project_man_power_issue_payment_added_on) values(:man_power_id,:amount,:deduction,:vendor_id,:payment_mode,:instrument_details,:active,:remarks,:added_by,:added_on)";
    try
    {
        $dbConnection = get_conn_handle();
        $project_man_power_issue_payment_istatement = $dbConnection->prepare($project_man_power_issue_payment_iquery);

        // Data
        $project_man_power_issue_payment_idata = array(':man_power_id'=>$man_power_id,':amount'=>$amount,':deduction'=>$deduction,':vendor_id'=>$vendor_id,':payment_mode'=>$payment_mode,':instrument_details'=>$instrument_details,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_man_power_issue_payment_istatement->execute($project_man_power_issue_payment_idata);
		$project_man_power_issue_payment_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_man_power_issue_payment_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project Manpower Issue Payment list
INPUT 	: Project Manpower Issue Payment array data
OUTPUT 	: List of Project Manpower Issue Payment
BY 		: Ashwini
*/
function db_get_project_man_power_issue_payment($project_man_power_issue_payment_search_data)
{
	if(array_key_exists("payment_id",$project_man_power_issue_payment_search_data))
	{
		$payment_id = $project_man_power_issue_payment_search_data["payment_id"];
	}
	else
	{
		$payment_id = "";
	}
	if(array_key_exists("man_power_id",$project_man_power_issue_payment_search_data))
	{
		$man_power_id = $project_man_power_issue_payment_search_data["man_power_id"];
	}
	else
	{
		$man_power_id = "";
	}
	if(array_key_exists("amount",$project_man_power_issue_payment_search_data))
	{
		$amount = $project_man_power_issue_payment_search_data["amount"];
	}
	else
	{
		$amount = "";
	}

	if(array_key_exists("vendor_id",$project_man_power_issue_payment_search_data))
	{
		$vendor_id = $project_man_power_issue_payment_search_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}

	if(array_key_exists("payment_mode",$project_man_power_issue_payment_search_data))
	{
		$payment_mode = $project_man_power_issue_payment_search_data["payment_mode"];
	}
	else
	{
		$payment_mode = "";
	}

	if(array_key_exists("active",$project_man_power_issue_payment_search_data))
	{
		$active = $project_man_power_issue_payment_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_man_power_issue_payment_search_data))
	{
		$added_by = $project_man_power_issue_payment_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_man_power_issue_payment_search_data))
	{
		$start_date = $project_man_power_issue_payment_search_data["start_date"];
	}
	else
	{
		$start_date = "";
	}

	if(array_key_exists("end_date",$project_man_power_issue_payment_search_data))
	{
		$end_date = $project_man_power_issue_payment_search_data["end_date"];
	}
	else
	{
		$end_date = "";
	}

	$get_project_man_power_issue_payment_list_squery_base = "select * from project_man_power_issue_payment PMIP inner join  users U on U.user_id=PMIP.project_man_power_issue_payment_added_by inner join project_manpower_agency PMA on PMA.project_manpower_agency_id= PMIP.project_man_power_issue_payment_vendor_id inner join payment_mode_master PMM on PMM.payment_mode_id= PMIP.project_man_power_issue_payment_mode inner join project_actual_payment_manpower PAPM on PAPM.project_actual_payment_manpower_id= PMIP.project_man_power_issue_payment_man_power_id";

	$get_project_man_power_issue_payment_list_squery_where = "";
	$filter_count = 0;

	// Data
	$get_project_man_power_issue_payment_list_sdata = array();

	if($payment_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_issue_payment_list_squery_where = $get_project_man_power_issue_payment_list_squery_where." where project_man_power_issue_payment_id = :payment_id";
		}
		else
		{
			// Query
			$get_project_man_power_issue_payment_list_squery_where = $get_project_man_power_issue_payment_list_squery_where." and project_man_power_issue_payment_id = :payment_id";
		}

		// Data
		$get_project_man_power_issue_payment_list_sdata[':payment_id'] = $payment_id;

		$filter_count++;
	}
	if($man_power_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_issue_payment_list_squery_where = $get_project_man_power_issue_payment_list_squery_where." where project_man_power_issue_payment_man_power_id = :man_power_id";
		}
		else
		{
			// Query
			$get_project_man_power_issue_payment_list_squery_where = $get_project_man_power_issue_payment_list_squery_where." and project_man_power_issue_payment_man_power_id = :man_power_id";
		}

		// Data
		$get_project_man_power_issue_payment_list_sdata[':man_power_id'] = $man_power_id;

		$filter_count++;
	}

	if($amount != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_issue_payment_list_squery_where = $get_project_man_power_issue_payment_list_squery_where." where project_man_power_issue_payment_amount = :amount";
		}
		else
		{
			// Query
			$get_project_man_power_issue_payment_list_squery_where = $get_project_man_power_issue_payment_list_squery_where." and project_man_power_issue_payment_amount = :amount";
		}

		// Data
		$get_project_man_power_issue_payment_list_sdata[':amount'] = $amount;

		$filter_count++;
	}

	if($vendor_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_issue_payment_list_squery_where = $get_project_man_power_issue_payment_list_squery_where." where project_man_power_issue_payment_vendor_id = :vendor_id";
		}
		else
		{
			// Query
			$get_project_man_power_issue_payment_list_squery_where = $get_project_man_power_issue_payment_list_squery_where." and project_man_power_issue_payment_vendor_id = :vendor_id";
		}

		// Data
		$get_project_man_power_issue_payment_list_sdata[':vendor_id'] = $vendor_id;

		$filter_count++;
	}

	if($payment_mode != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_issue_payment_list_squery_where = $get_project_man_power_issue_payment_list_squery_where." where project_man_power_issue_payment_mode = :payment_mode";
		}
		else
		{
			// Query
			$get_project_man_power_issue_payment_list_squery_where = $get_project_man_power_issue_payment_list_squery_where." and project_man_power_issue_payment_mode = :payment_mode";
		}

		// Data
		$get_project_man_power_issue_payment_list_sdata[':payment_mode'] = $payment_mode;

		$filter_count++;
	}


	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_issue_payment_list_squery_where = $get_project_man_power_issue_payment_list_squery_where." where project_man_power_issue_payment_active = :active";
		}
		else
		{
			// Query
			$get_project_man_power_issue_payment_list_squery_where = $get_project_man_power_issue_payment_list_squery_where." and project_man_power_issue_payment_active = :active";
		}

		// Data
		$get_project_man_power_issue_payment_list_sdata[':active']  = $active;

		$filter_count++;
	}


	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_issue_payment_list_squery_where = $get_project_man_power_issue_payment_list_squery_where." where project_man_power_issue_payment_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_man_power_issue_payment_list_squery_where = $get_project_man_power_issue_payment_list_squery_where." and project_man_power_issue_payment_added_by = :added_by";
		}

		//Data
		$get_project_man_power_issue_payment_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_issue_payment_list_squery_where = $get_project_man_power_issue_payment_list_squery_where." where project_man_power_issue_payment_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_man_power_issue_payment_list_squery_where = $get_project_man_power_issue_payment_list_squery_where." and project_man_power_issue_payment_added_on >= :start_date";
		}

		//Data
		$get_project_man_power_issue_payment_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_issue_payment_list_squery_where = $get_project_man_power_issue_payment_list_squery_where." where project_man_power_issue_payment_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_man_power_issue_payment_list_squery_where = $get_project_man_power_issue_payment_list_squery_where." and project_man_power_issue_payment_added_on <= :end_date";
		}

		//Data
		$get_project_man_power_issue_payment_list_sdata[':end_date']  = $end_date;

		$filter_count++;
	}

	$get_project_man_power_issue_payment_list_squery = $get_project_man_power_issue_payment_list_squery_base.$get_project_man_power_issue_payment_list_squery_where;

	try
	{
		$dbConnection = get_conn_handle();

		$get_project_man_power_issue_payment_list_sstatement = $dbConnection->prepare($get_project_man_power_issue_payment_list_squery);

		$get_project_man_power_issue_payment_list_sstatement -> execute($get_project_man_power_issue_payment_list_sdata);

		$get_project_man_power_issue_payment_list_sdetails = $get_project_man_power_issue_payment_list_sstatement -> fetchAll();

		if(FALSE === $get_project_man_power_issue_payment_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_man_power_issue_payment_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_man_power_issue_payment_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

  /*
PURPOSE : To update Project Man Power Issue Payment list
INPUT 	: Payment ID,Project Man Power Issue Payment Update Array
OUTPUT 	: Message of success or failure
BY 		: Ashwini
*/
function db_update_project_man_power_issue_payment($payment_id,$project_man_power_issue_payment_update_data)
{

	if(array_key_exists("man_power_id",$project_man_power_issue_payment_update_data))
	{
		$man_power_id = $project_man_power_issue_payment_update_data["man_power_id"];
	}
	else
	{
		$man_power_id = "";
	}
	if(array_key_exists("amount",$project_man_power_issue_payment_update_data))
	{
		$amount = $project_man_power_issue_payment_update_data["amount"];
	}
	else
	{
		$amount = "";
	}

	if(array_key_exists("vendor_id",$project_man_power_issue_payment_update_data))
	{
		$vendor_id = $project_man_power_issue_payment_update_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}

	if(array_key_exists("payment_mode",$project_man_power_issue_payment_update_data))
	{
		$payment_mode = $project_man_power_issue_payment_update_data["payment_mode"];
	}
	else
	{
		$payment_mode = "";
	}

	if(array_key_exists("instrument_details",$project_man_power_issue_payment_update_data))
	{
		$instrument_details = $project_man_power_issue_payment_update_data["instrument_details"];
	}
	else
	{
		$instrument_details = "";
	}

	if(array_key_exists("active",$project_man_power_issue_payment_update_data))
	{
		$active = $project_man_power_issue_payment_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_man_power_issue_payment_update_data))
	{
		$remarks = $project_man_power_issue_payment_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_man_power_issue_payment_update_data))
	{
		$added_by = $project_man_power_issue_payment_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_man_power_issue_payment_update_data))
	{
		$added_on = $project_man_power_issue_payment_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_man_power_issue_payment_update_uquery_base = "update project_man_power_issue_payment set ";

	$project_man_power_issue_payment_update_uquery_set = "";

	$project_man_power_issue_payment_update_uquery_where = " where project_man_power_issue_payment_id = :payment_id";

	$project_man_power_issue_payment_update_udata = array(":payment_id"=>$payment_id);

	$filter_count = 0;

	if($man_power_id != "")
	{
		$project_man_power_issue_payment_update_uquery_set = $project_man_power_issue_payment_update_uquery_set."project_man_power_issue_payment_man_power_id  = :man_power_id,";
		$project_man_power_issue_payment_update_udata[":man_power_id"] = $man_power_id;
		$filter_count++;
	}
	if($amount != "")
	{
		$project_man_power_issue_payment_update_uquery_set = $project_man_power_issue_payment_update_uquery_set."project_man_power_issue_payment_amount  = :amount,";
		$project_man_power_issue_payment_update_udata[":amount"] = $amount;
		$filter_count++;
	}

	if($vendor_id != "")
	{
		$project_man_power_issue_payment_update_uquery_set = $project_man_power_issue_payment_update_uquery_set."project_man_power_issue_payment_vendor_id  = :vendor_id,";
		$project_man_power_issue_payment_update_udata[":vendor_id"] = $vendor_id;
		$filter_count++;
	}

	if($payment_mode != "")
	{
		$project_man_power_issue_payment_update_uquery_set = $project_man_power_issue_payment_update_uquery_set."project_man_power_issue_payment_mode  = :payment_mode,";
		$project_man_power_issue_payment_update_udata[":payment_mode"] = $payment_mode;
		$filter_count++;
	}
	if($instrument_details != "")
	{
		$project_man_power_issue_payment_update_uquery_set = $project_man_power_issue_payment_update_uquery_set."project_man_power_issue_payment_instrument_details  = :instrument_details,";
		$project_man_power_issue_payment_update_udata[":instrument_details"] = $instrument_details;
		$filter_count++;
	}
	if($active != "")
	{
		$project_man_power_issue_payment_update_uquery_set = $project_man_power_issue_payment_update_uquery_set."project_man_power_issue_payment_active = :active,";
		$project_man_power_issue_payment_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_man_power_issue_payment_update_uquery_set = $project_man_power_issue_payment_update_uquery_set." project_man_power_issue_payment_remarks = :remarks,";
		$project_man_power_issue_payment_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_man_power_issue_payment_update_uquery_set = $project_man_power_issue_payment_update_uquery_set."project_man_power_issue_payment_added_by = :added_by,";
		$project_man_power_issue_payment_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_man_power_issue_payment_update_uquery_set = $project_man_power_issue_payment_update_uquery_set."project_man_power_issue_payment_added_on = :added_on,";
		$project_man_power_issue_payment_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_man_power_issue_payment_update_uquery_set = trim($project_man_power_issue_payment_update_uquery_set,',');
	}

	$project_man_power_issue_payment_update_uquery = $project_man_power_issue_payment_update_uquery_base.$project_man_power_issue_payment_update_uquery_set.
	$project_man_power_issue_payment_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $project_man_power_issue_payment_update_ustatement = $dbConnection->prepare($project_man_power_issue_payment_update_uquery);

        $project_man_power_issue_payment_update_ustatement -> execute($project_man_power_issue_payment_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $payment_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Payment Manpower Mapping
INPUT 	: Manpower ID, Payment ID, Remarks, Added By
OUTPUT 	: Manpower Mapping ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_payment_manpower_mapping($manpower_id,$payment_id,$remarks,$added_by)
{
	// Query
   $project_payment_manpower_mapping_iquery = "insert into project_payment_manpower_mapping
   (project_payment_manpower_mapping_manpower_id,project_payment_manpower_mapping_payment_id,project_payment_manpower_mapping_active,
   project_payment_manpower_mapping_remarks,project_payment_manpower_mapping_added_by,project_payment_manpower_mapping_added_on)
   values(:manpower_id,:payment_id,:active,:remarks,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_payment_manpower_mapping_istatement = $dbConnection->prepare($project_payment_manpower_mapping_iquery);

        // Data
        $project_payment_manpower_mapping_idata = array(':manpower_id'=>$manpower_id,':payment_id'=>$payment_id,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,
		':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_payment_manpower_mapping_istatement->execute($project_payment_manpower_mapping_idata);
		$project_payment_manpower_mapping_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_payment_manpower_mapping_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project Payment Manpower  Mapping list
INPUT 	: Manpower Mapping ID, Manpower ID, Payment ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Payment Manpower  Mapping
BY 		: Lakshmi
*/
function db_get_project_payment_manpower_mapping($project_payment_manpower_mapping_search_data)
{

	if(array_key_exists("manpower_mapping_id",$project_payment_manpower_mapping_search_data))
	{
		$manpower_mapping_id = $project_payment_manpower_mapping_search_data["manpower_mapping_id"];
	}
	else
	{
		$manpower_mapping_id= "";
	}

	if(array_key_exists("manpower_id",$project_payment_manpower_mapping_search_data))
	{
		$manpower_id = $project_payment_manpower_mapping_search_data["manpower_id"];
	}
	else
	{
		$manpower_id = "";
	}

	if(array_key_exists("payment_id",$project_payment_manpower_mapping_search_data))
	{
		$payment_id = $project_payment_manpower_mapping_search_data["payment_id"];
	}
	else
	{
		$payment_id = "";
	}

	if(array_key_exists("project",$project_payment_manpower_mapping_search_data))
	{
		$project = $project_payment_manpower_mapping_search_data["project"];
	}
	else
	{
		$project = "";
	}

	if(array_key_exists("active",$project_payment_manpower_mapping_search_data))
	{
		$active = $project_payment_manpower_mapping_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_payment_manpower_mapping_search_data))
	{
		$added_by = $project_payment_manpower_mapping_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_payment_manpower_mapping_search_data))
	{
		$start_date= $project_payment_manpower_mapping_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_payment_manpower_mapping_search_data))
	{
		$end_date= $project_payment_manpower_mapping_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_project_payment_manpower_mapping_list_squery_base = " select *,U.user_name as adder,AU.user_name as approver,OU.user_name as oker from project_payment_manpower_mapping PPMM inner join project_task_actual_manpower PTMP on PTMP.project_task_actual_manpower_id=PPMM.project_payment_manpower_mapping_manpower_id  inner join project_plan_process_task PPPT on PPPT.project_process_task_id = PTMP.project_task_actual_manpower_task_id inner join project_manpower_agency PMA on PMA.project_manpower_agency_id=PTMP.project_task_actual_manpower_agency inner join project_task_master PTM on PTM.project_task_master_id=PPPT.project_process_task_type inner join project_plan_process PPP on PPP.project_plan_process_id=PPPT.project_process_id inner join project_plan PP on PP.project_plan_id=PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id=PP.project_plan_project_id inner join project_process_master PPM on PPM.project_process_master_id = PPP.project_plan_process_name left outer join users AU on AU.user_id = PTMP.project_task_actual_manpower_approved_by left outer join users OU on OU.user_id = PTMP.project_task_actual_manpower_checked_by inner join users U on U.user_id = PTMP.project_task_actual_manpower_added_by left outer join project_site_location_mapping_master PSLMM on PSLMM.project_site_location_mapping_master_id=PTMP.project_task_actual_manpower_road_id";

	$get_project_payment_manpower_mapping_list_squery_where = "";

	$filter_count = 0;

	// Data
	$get_project_payment_manpower_mapping_list_sdata = array();

	if($manpower_mapping_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_manpower_mapping_list_squery_where = $get_project_payment_manpower_mapping_list_squery_where." where project_payment_manpower_mapping_id = :manpower_mapping_id";
		}
		else
		{
			// Query
			$get_project_payment_manpower_mapping_list_squery_where = $get_project_payment_manpower_mapping_list_squery_where." and project_payment_manpower_mapping_id = :manpower_mapping_id";
		}

		// Data
		$get_project_payment_manpower_mapping_list_sdata[':manpower_mapping_id'] = $manpower_mapping_id;

		$filter_count++;
	}

	if($manpower_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_manpower_mapping_list_squery_where = $get_project_payment_manpower_mapping_list_squery_where." where project_payment_manpower_mapping_manpower_id = :manpower_id";
		}
		else
		{
			// Query
			$get_project_payment_manpower_mapping_list_squery_where = $get_project_payment_manpower_mapping_list_squery_where." and project_payment_manpower_mapping_manpower_id = :manpower_id";
		}

		// Data
		$get_project_payment_manpower_mapping_list_sdata[':manpower_id'] = $manpower_id;

		$filter_count++;
	}

	if($payment_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_manpower_mapping_list_squery_where = $get_project_payment_manpower_mapping_list_squery_where." where project_payment_manpower_mapping_payment_id = :payment_id";
		}
		else
		{
			// Query
			$get_project_payment_manpower_mapping_list_squery_where = $get_project_payment_manpower_mapping_list_squery_where." and project_payment_manpower_mapping_payment_id = :payment_id";
		}

		// Data
		$get_project_payment_manpower_mapping_list_sdata[':payment_id'] = $payment_id;

		$filter_count++;
	}

	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_manpower_mapping_list_squery_where = $get_project_payment_manpower_mapping_list_squery_where." where PTMP.project_plan_project_id = :project";
		}
		else
		{
			// Query
			$get_project_payment_manpower_mapping_list_squery_where = $get_project_payment_manpower_mapping_list_squery_where." and PTMP.project_plan_project_id = :project";
		}

		// Data
		$get_project_payment_manpower_mapping_list_sdata[':project'] = $project;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_manpower_mapping_list_squery_where = $get_project_payment_manpower_mapping_list_squery_where." where project_payment_manpower_mapping_active = :active";
		}
		else
		{
			// Query
			$get_project_payment_manpower_mapping_list_squery_where = $get_project_payment_manpower_mapping_list_squery_where." and project_payment_manpower_mapping_active = :active";
		}

		// Data
		$get_project_payment_manpower_mapping_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_manpower_mapping_list_squery_where = $get_project_payment_manpower_mapping_list_squery_where." where project_payment_manpower_mapping_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_payment_manpower_mapping_list_squery_where = $get_project_payment_manpower_mapping_list_squery_where." and project_payment_manpower_mapping_added_by = :added_by";
		}

		//Data
		$get_project_payment_manpower_mapping_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_manpower_mapping_list_squery_where = $get_project_payment_manpower_mapping_list_squery_where." where project_payment_manpower_mapping_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_payment_manpower_mapping_list_squery_where = $get_project_payment_manpower_mapping_list_squery_where." and project_payment_manpower_mapping_added_on >= :start_date";
		}

		//Data
		$get_project_payment_manpower_mapping_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_manpower_mapping_list_squery_where = $get_project_payment_manpower_mapping_list_squery_where." where project_payment_manpower_mapping_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_payment_manpower_mapping_list_squery_where = $get_project_payment_manpower_mapping_list_squery_where." and project_payment_manpower_mapping_added_on <= :end_date";
		}

		//Data
		$get_project_payment_manpower_mapping_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	$get_project_payment_manpower_mapping_list_squery = $get_project_payment_manpower_mapping_list_squery_base.$get_project_payment_manpower_mapping_list_squery_where;
	try
	{
		$dbConnection = get_conn_handle();

		$get_project_payment_manpower_mapping_list_sstatement = $dbConnection->prepare($get_project_payment_manpower_mapping_list_squery);

		$get_project_payment_manpower_mapping_list_sstatement -> execute($get_project_payment_manpower_mapping_list_sdata);
    ini_set('memory_limit', '-1');
		$get_project_payment_manpower_mapping_list_sdetails = $get_project_payment_manpower_mapping_list_sstatement -> fetchAll();

		if(FALSE === $get_project_payment_manpower_mapping_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_payment_manpower_mapping_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_payment_manpower_mapping_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

  /*
PURPOSE : To update Project Payment Manpower Mapping
INPUT 	: Manpower Mapping ID, Project Payment Manpower Mapping Update Array
OUTPUT 	: Manpower Mapping ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_payment_manpower_mapping($manpower_mapping_id,$project_payment_manpower_mapping_update_data)
{
	if(array_key_exists("manpower_id",$project_payment_manpower_mapping_update_data))
	{
		$manpower_id = $project_payment_manpower_mapping_update_data["manpower_id"];
	}
	else
	{
		$manpower_id = "";
	}

	if(array_key_exists("payment_id",$project_payment_manpower_mapping_update_data))
	{
		$payment_id = $project_payment_manpower_mapping_update_data["payment_id"];
	}
	else
	{
		$payment_id = "";
	}

	if(array_key_exists("active",$project_payment_manpower_mapping_update_data))
	{
		$active = $project_payment_manpower_mapping_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_payment_manpower_mapping_update_data))
	{
		$remarks = $project_payment_manpower_mapping_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_payment_manpower_mapping_update_data))
	{
		$added_by = $project_payment_manpower_mapping_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_payment_manpower_mapping_update_data))
	{
		$added_on = $project_payment_manpower_mapping_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_payment_manpower_mapping_update_uquery_base = "update project_payment_manpower_mapping set ";

	$project_payment_manpower_mapping_update_uquery_set = "";

	$project_payment_manpower_mapping_update_uquery_where = " where project_payment_manpower_mapping_id = :manpower_mapping_id";

	$project_payment_manpower_mapping_update_udata = array(":manpower_mapping_id"=>$manpower_mapping_id);

	$filter_count = 0;

	if($manpower_id != "")
	{
		$project_payment_manpower_mapping_update_uquery_set = $project_payment_manpower_mapping_update_uquery_set." project_payment_manpower_mapping_manpower_id = :manpower_id,";
		$project_payment_manpower_mapping_update_udata[":manpower_id"] = $manpower_id;
		$filter_count++;
	}

	if($payment_id != "")
	{
		$project_payment_manpower_mapping_update_uquery_set = $project_payment_manpower_mapping_update_uquery_set." project_payment_manpower_mapping_payment_id = :payment_id,";
		$project_payment_manpower_mapping_update_udata[":payment_id"] = $payment_id;
		$filter_count++;
	}

	if($active != "")
	{
		$project_payment_manpower_mapping_update_uquery_set = $project_payment_manpower_mapping_update_uquery_set." project_payment_manpower_mapping_active = :active,";
		$project_payment_manpower_mapping_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_payment_manpower_mapping_update_uquery_set = $project_payment_manpower_mapping_update_uquery_set." project_payment_manpower_mapping_remarks = :remarks,";
		$project_payment_manpower_mapping_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_payment_manpower_mapping_update_uquery_set = $project_payment_manpower_mapping_update_uquery_set." project_payment_manpower_mapping_added_by = :added_by,";
		$project_payment_manpower_mapping_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_payment_manpower_mapping_update_uquery_set = $project_payment_manpower_mapping_update_uquery_set." project_payment_manpower_mapping_added_on = :added_on,";
		$project_payment_manpower_mapping_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_payment_manpower_mapping_update_uquery_set = trim($project_payment_manpower_mapping_update_uquery_set,',');
	}

	$project_payment_manpower_mapping_update_uquery = $project_payment_manpower_mapping_update_uquery_base.$project_payment_manpower_mapping_update_uquery_set.$project_payment_manpower_mapping_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $project_payment_manpower_mapping_update_ustatement = $dbConnection->prepare($project_payment_manpower_mapping_update_uquery);

        $project_payment_manpower_mapping_update_ustatement -> execute($project_payment_manpower_mapping_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $manpower_mapping_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Actual Contract Payment
INPUT 	: Contract Payment ID, Contract ID, From Date, To Date, Vendor ID, Amount, Deposit Amount, Number, Length, Breadth, Depth, Total Measurement, UOM, Rate, Status, Remarks, Accepted By, Accepted On, Approved By, Approved On, Added By
OUTPUT 	: Contract Payment ID, success or failure message
BY 		: Ashwini
*/
function db_add_project_actual_contract_payment($contract_id,$from_date,$to_date,$vendor_id,$amount,$deposit_amount,$number,$length,$breadth,$depth,$total_measurement,$uom,$rate,$bill_no,$remarks,$added_by)
{
	// Query
   $project_actual_contract_payment_iquery = "insert into project_actual_contract_payment
(project_actual_contract_id,project_actual_contract_payment_from_date,project_actual_contract_payment_to_date,project_actual_contract_payment_vendor_id,project_actual_contract_payment_amount,project_actual_contract_deposit_amount,project_actual_contract_payment_number,project_actual_contract_payment_length,project_actual_contract_payment_breadth,project_actual_contract_payment_depth,project_actual_contract_payment_total_measurement,project_actual_contract_payment_uom,project_actual_contract_payment_rate,project_actual_contract_payment_bill_no,project_actual_contract_payment_status,project_actual_contract_payment_active,project_actual_contract_payment_remarks,project_actual_contract_payment_added_by,project_actual_contract_payment_added_on)
	values(:contract_id,:from_date,:to_date,:vendor_id,:amount,:deposit_amount,:number,:length,:breadth,:depth,:total_measurement,:uom,:rate,:bill_no,:status,:active,:remarks,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_actual_contract_payment_istatement = $dbConnection->prepare($project_actual_contract_payment_iquery);

        // Data
        $project_actual_contract_payment_idata = array(':contract_id'=>$contract_id,':from_date'=>$from_date,':to_date'=>$to_date,':vendor_id'=>$vendor_id,':amount'=>$amount,':deposit_amount'=>$deposit_amount,':number'=>$number,':length'=>$length,':breadth'=>$breadth,':depth'=>$depth,':total_measurement'=>$total_measurement,':uom'=>$uom,':rate'=>$rate,':bill_no'=>$bill_no,':status'=>'Pending',':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_actual_contract_payment_istatement->execute($project_actual_contract_payment_idata);
		$project_actual_contract_payment_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_actual_contract_payment_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project Actual Contract Payment list
INPUT 	: Contract Payment ID, Contract ID, From Date, To Date, Vendor ID,Amount , Number, Length, Breadth, Depth, Total Measurement, UOM, Rate, Status, Active, Accepted By, Accepted On, Approved By, Approved On, Added By, Start Date(for Added on), End Date(for Added on)
OUTPUT 	: List of Project Actual Contract Payment
BY 		: Ashwini
*/
function db_get_project_actual_contract_payment($project_actual_contract_payment_search_data)
{

	if(array_key_exists("contract_payment_id",$project_actual_contract_payment_search_data))
	{
		$contract_payment_id = $project_actual_contract_payment_search_data["contract_payment_id"];
	}
	else
	{
		$contract_payment_id= "";
	}

	if(array_key_exists("contract_id",$project_actual_contract_payment_search_data))
	{
		$contract_id = $project_actual_contract_payment_search_data["contract_id"];
	}
	else
	{
		$contract_id= "";
	}

	if(array_key_exists("from_date",$project_actual_contract_payment_search_data))
	{
		$from_date = $project_actual_contract_payment_search_data["from_date"];
	}
	else
	{
		$from_date= "";
	}

	if(array_key_exists("to_date",$project_actual_contract_payment_search_data))
	{
		$to_date = $project_actual_contract_payment_search_data["to_date"];
	}
	else
	{
		$to_date= "";
	}

	if(array_key_exists("vendor_id",$project_actual_contract_payment_search_data))
	{
		$vendor_id = $project_actual_contract_payment_search_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}

	if(array_key_exists("amount",$project_actual_contract_payment_search_data))
	{
		$amount = $project_actual_contract_payment_search_data["amount"];
	}
	else
	{
		$amount= "";
	}

	if(array_key_exists("number",$project_actual_contract_payment_search_data))
	{
		$number = $project_actual_contract_payment_search_data["number"];
	}
	else
	{
		$number= "";
	}
	if(array_key_exists("length",$project_actual_contract_payment_search_data))
	{
		$length = $project_actual_contract_payment_search_data["length"];
	}
	else
	{
		$length = "";
	}

	if(array_key_exists("breadth",$project_actual_contract_payment_search_data))
	{
		$breadth = $project_actual_contract_payment_search_data["breadth"];
	}
	else
	{
		$breadth = "";
	}

	if(array_key_exists("depth",$project_actual_contract_payment_search_data))
	{
		$depth = $project_actual_contract_payment_search_data["depth"];
	}
	else
	{
		$depth = "";
	}

	if(array_key_exists("total_measurement",$project_actual_contract_payment_search_data))
	{
		$total_measurement = $project_actual_contract_payment_search_data["total_measurement"];
	}
	else
	{
		$total_measurement = "";
	}

	if(array_key_exists("uom",$project_actual_contract_payment_search_data))
	{
		$uom = $project_actual_contract_payment_search_data["uom"];
	}
	else
	{
		$uom = "";
	}

	if(array_key_exists("rate",$project_actual_contract_payment_search_data))
	{
		$rate = $project_actual_contract_payment_search_data["rate"];
	}
	else
	{
		$rate = "";
	}

	if(array_key_exists("status",$project_actual_contract_payment_search_data))
	{
		$status = $project_actual_contract_payment_search_data["status"];
	}
	else
	{
		$status = "";
	}

	if(array_key_exists("secondary_status",$project_actual_contract_payment_search_data))
	{
		$secondary_status = $project_actual_contract_payment_search_data["secondary_status"];
	}
	else
	{
		$secondary_status = "";
	}

	if(array_key_exists("active",$project_actual_contract_payment_search_data))
	{
		$active = $project_actual_contract_payment_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("accepted_by",$project_actual_contract_payment_search_data))
	{
		$accepted_by = $project_actual_contract_payment_search_data["accepted_by"];
	}
	else
	{
		$accepted_by = "";
	}

	if(array_key_exists("accepted_on",$project_actual_contract_payment_search_data))
	{
		$accepted_on = $project_actual_contract_payment_search_data["accepted_on"];
	}
	else
	{
		$accepted_on = "";
	}

	if(array_key_exists("approved_by",$project_actual_contract_payment_search_data))
	{
		$approved_by = $project_actual_contract_payment_search_data["approved_by"];
	}
	else
	{
		$approved_by = "";
	}

	if(array_key_exists("approved_on",$project_actual_contract_payment_search_data))
	{
		$approved_on = $project_actual_contract_payment_search_data["approved_on"];
	}
	else
	{
		$approved_on = "";
	}

	if(array_key_exists("added_by",$project_actual_contract_payment_search_data))
	{
		$added_by = $project_actual_contract_payment_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_actual_contract_payment_search_data))
	{
		$start_date= $project_actual_contract_payment_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_actual_contract_payment_search_data))
	{
		$end_date= $project_actual_contract_payment_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_project_actual_contract_payment_list_squery_base = " select *,U.user_name as added_by, AU.user_name as approved_by from project_actual_contract_payment PACP inner join users U on U.user_id = PACP.project_actual_contract_payment_added_by inner join project_manpower_agency PMA on PMA.project_manpower_agency_id= PACP.project_actual_contract_payment_vendor_id inner join stock_unit_measure_master SUMM on SUMM.stock_unit_id = PACP.project_actual_contract_payment_uom left outer join users AU on AU.user_id=PACP.project_actual_contract_payment_approved_by left outer join stock_company_master SCM on SCM.stock_company_master_id= PACP.project_actual_contract_payment_billing_address";

	$get_project_actual_contract_payment_list_squery_where = "";
	$get_contract_list_squery_order = "";

	$filter_count = 0;

	// Data
	$get_project_actual_contract_payment_list_sdata = array();

	if($contract_payment_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_payment_id = :contract_payment_id";
		}
		else
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_payment_id = :contract_payment_id";
		}

		// Data
		$get_project_actual_contract_payment_list_sdata[':contract_payment_id'] = $contract_payment_id;

		$filter_count++;
	}

	if($contract_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_id = :contract_id";
		}
		else
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_id = :contract_id";
		}

		// Data
		$get_project_actual_contract_payment_list_sdata[':contract_id'] = $contract_id;

		$filter_count++;
	}

	if($from_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_payment_from_date = :from_date";
		}
		else
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_payment_from_date = :from_date";
		}

		// Data
		$get_project_actual_contract_payment_list_sdata[':from_date'] = $from_date;

		$filter_count++;
	}

	if($to_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_payment_to_date = :to_date";
		}
		else
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_payment_to_date = :to_date";
		}

		// Data
		$get_project_actual_contract_payment_list_sdata[':to_date'] = $to_date;

		$filter_count++;
	}

	if($vendor_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_payment_vendor_id = :vendor_id";
		}
		else
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_payment_vendor_id = :vendor_id";
		}

		// Data
		$get_project_actual_contract_payment_list_sdata[':vendor_id'] = $vendor_id;

		$filter_count++;
	}

	if($amount != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_payment_amount = :amount";
		}
		else
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_payment_amount = :amount";
		}

		// Data
		$get_project_actual_contract_payment_list_sdata[':amount'] = $amount;

		$filter_count++;
	}

	if($number != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_payment_number = :number";
		}
		else
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_payment_number = :number";
		}

		// Data
		$get_project_actual_contract_payment_list_sdata[':number'] = $number;

		$filter_count++;
	}
	if($length != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_payment_length = :length";
		}
		else
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_payment_length = :length";
		}

		// Data
		$get_project_actual_contract_payment_list_sdata[':length'] = $length;

		$filter_count++;
	}

	if($breadth != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_payment_breadth = :breadth";
		}
		else
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_payment_breadth = :breadth";
		}

		// Data
		$get_project_actual_contract_payment_list_sdata[':breadth'] = $breadth;

		$filter_count++;
	}

	if($depth != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_payment_depth = :depth";
		}
		else
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_payment_depth = :depth";
		}

		// Data
		$get_project_actual_contract_payment_list_sdata[':depth'] = $depth;

		$filter_count++;
	}

	if($total_measurement != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_payment_total_measurement = :total_measurement";
		}
		else
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_payment_total_measurement = :total_measurement";
		}

		// Data
		$get_project_actual_contract_payment_list_sdata[':total_measurement'] = $total_measurement;

		$filter_count++;
	}

	if($uom != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_payment_uom = :uom";
		}
		else
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_payment_uom = :uom";
		}

		// Data
		$get_project_actual_contract_payment_list_sdata[':uom'] = $uom;

		$filter_count++;
	}

	if($rate != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_payment_rate = :rate";
		}
		else
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_payment_rate = :rate";
		}

		// Data
		$get_project_actual_contract_payment_list_sdata[':rate'] = $rate;

		$filter_count++;
	}

	if($status != "")
	{
		if($secondary_status != "")
		{
			if($filter_count == 0)
			{
				// Query
				$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where (project_actual_contract_payment_status = :status or project_actual_contract_payment_status = :secondary_status)";
			}
			else
			{
				// Query
				$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and (project_actual_contract_payment_status = :status or project_actual_contract_payment_status = :secondary_status)";
			}

			$get_project_actual_contract_payment_list_sdata[':status'] = $status;
			$get_project_actual_contract_payment_list_sdata[':secondary_status'] = $secondary_status;

			$filter_count++;

		}
		else
		{
			if($filter_count == 0)
			{
				// Query
				$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_payment_status = :status";
			}
			else
			{
				// Query
				$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_payment_status = :status";
			}
			// Data
			$get_project_actual_contract_payment_list_sdata[':status'] = $status;

			$filter_count++;
		}
		// Data
	}


	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_payment_active = :active";
		}
		else
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_payment_active = :active";
		}

		// Data
		$get_project_actual_contract_payment_list_sdata[':active'] = $active;

		$filter_count++;
	}

	if($accepted_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_payment_accepted_by = :accepted_by";
		}
		else
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_payment_accepted_by = :accepted_by";
		}

		// Data
		$get_project_actual_contract_payment_list_sdata[':accepted_by'] = $accepted_by;

		$filter_count++;
	}

	if($accepted_on != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_payment_accepted_on = :accepted_on";
		}
		else
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_payment_accepted_on = :accepted_on";
		}

		// Data
		$get_project_actual_contract_payment_list_sdata[':accepted_on'] = $accepted_on;

		$filter_count++;
	}

	if($approved_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_payment_approved_by = :approved_by";
		}
		else
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_payment_approved_by = :approved_by";
		}

		// Data
		$get_project_actual_contract_payment_list_sdata[':approved_by'] = $approved_by;

		$filter_count++;
	}

	if($approved_on != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_payment_approved_on = :approved_on";
		}
		else
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_payment_approved_on = :approved_on";
		}

		// Data
		$get_project_actual_contract_payment_list_sdata[':approved_on'] = $approved_on;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_payment_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_payment_added_by = :added_by";
		}

		//Data
		$get_project_actual_contract_payment_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_payment_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_payment_added_on >= :start_date";
		}

		//Data
		$get_project_actual_contract_payment_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_payment_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_payment_added_on <= :end_date";
		}

		//Data
		$get_project_actual_contract_payment_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	if(array_key_exists("sort",$project_actual_contract_payment_search_data))
	{
		if($project_actual_contract_payment_search_data['sort'] == '1')
		{
			$get_contract_list_squery_order = " order by project_actual_contract_payment_id desc limit 0,1";
		}
		else
		{
			$get_contract_list_squery_order = " order by project_actual_contract_payment_added_on ASC";
		}
	}

	if(array_key_exists("empty_check",$project_actual_contract_payment_search_data))
	{
		if($filter_count == 0)
		{
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." where project_actual_contract_payment_bill_no != ''";
		}
		else
		{
			$get_project_actual_contract_payment_list_squery_where = $get_project_actual_contract_payment_list_squery_where." and project_actual_contract_payment_bill_no != ''";
		}
		$filter_count++;
	}


	$get_project_actual_contract_payment_list_squery = $get_project_actual_contract_payment_list_squery_base.$get_project_actual_contract_payment_list_squery_where.$get_contract_list_squery_order;
  try
	{
		$dbConnection = get_conn_handle();
		$get_project_actual_contract_payment_list_sstatement = $dbConnection->prepare($get_project_actual_contract_payment_list_squery);
		$get_project_actual_contract_payment_list_sstatement -> execute($get_project_actual_contract_payment_list_sdata);

		$get_project_actual_contract_payment_list_sdetails = $get_project_actual_contract_payment_list_sstatement -> fetchAll();

		if(FALSE === $get_project_actual_contract_payment_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_actual_contract_payment_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_actual_contract_payment_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

 /*
PURPOSE : To update Project Actual Contract Payment
INPUT 	: Contract Payment ID, Project Actual Contract Payment Update Array
OUTPUT 	: Contract Payment ID; Message of success or failure
BY 		: Ashwini
*/
function db_update_project_actual_contract_payment($contract_payment_id,$project_actual_contract_payment_update_data)
{

	if(array_key_exists("contract_id",$project_actual_contract_payment_update_data))
	{
		$contract_id = $project_actual_contract_payment_update_data["contract_id"];
	}
	else
	{
		$contract_id = "";
	}

	if(array_key_exists("from_date",$project_actual_contract_payment_update_data))
	{
		$from_date = $project_actual_contract_payment_update_data["from_date"];
	}
	else
	{
		$from_date = "";
	}

	if(array_key_exists("to_date",$project_actual_contract_payment_update_data))
	{
		$to_date = $project_actual_contract_payment_update_data["to_date"];
	}
	else
	{
		$to_date = "";
	}

	if(array_key_exists("vendor_id",$project_actual_contract_payment_update_data))
	{
		$vendor_id = $project_actual_contract_payment_update_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}

	if(array_key_exists("amount",$project_actual_contract_payment_update_data))
	{
		$amount = $project_actual_contract_payment_update_data["amount"];
	}
	else
	{
		$amount = "";
	}

	if(array_key_exists("tds",$project_actual_contract_payment_update_data))
	{
		$tds = $project_actual_contract_payment_update_data["tds"];
	}
	else
	{
		$tds = "";
	}

	if(array_key_exists("sec_dep_amount",$project_actual_contract_payment_update_data))
	{
		$sec_dep_amount = $project_actual_contract_payment_update_data["sec_dep_amount"];
	}
	else
	{
		$sec_dep_amount = "";
	}

	if(array_key_exists("number",$project_actual_contract_payment_update_data))
	{
		$number = $project_actual_contract_payment_update_data["number"];
	}
	else
	{
		$number = "";
	}

	if(array_key_exists("length",$project_actual_contract_payment_update_data))
	{
		$length = $project_actual_contract_payment_update_data["length"];
	}
	else
	{
		$length = "";
	}

	if(array_key_exists("breadth",$project_actual_contract_payment_update_data))
	{
		$breadth = $project_actual_contract_payment_update_data["breadth"];
	}
	else
	{
		$breadth = "";
	}


	if(array_key_exists("depth",$project_actual_contract_payment_update_data))
	{
		$depth = $project_actual_contract_payment_update_data["depth"];
	}
	else
	{
		$depth = "";
	}

	if(array_key_exists("total_measurement",$project_actual_contract_payment_update_data))
	{
		$total_measurement = $project_actual_contract_payment_update_data["total_measurement"];
	}
	else
	{
		$total_measurement = "";
	}

	if(array_key_exists("uom",$project_actual_contract_payment_update_data))
	{
		$uom = $project_actual_contract_payment_update_data["uom"];
	}
	else
	{
		$uom = "";
	}

	if(array_key_exists("rate",$project_actual_contract_payment_update_data))
	{
		$rate = $project_actual_contract_payment_update_data["rate"];
	}
	else
	{
		$rate = "";
	}

	if(array_key_exists("status",$project_actual_contract_payment_update_data))
	{
		$status = $project_actual_contract_payment_update_data["status"];
	}
	else
	{
		$status = "";
	}

	if(array_key_exists("bill_no",$project_actual_contract_payment_update_data))
	{
		$bill_no = $project_actual_contract_payment_update_data["bill_no"];
	}
	else
	{
		$bill_no = "";
	}

	if(array_key_exists("billing_address",$project_actual_contract_payment_update_data))
	{
		$billing_address = $project_actual_contract_payment_update_data["billing_address"];
	}
	else
	{
		$billing_address = "";
	}

	if(array_key_exists("active",$project_actual_contract_payment_update_data))
	{
		$active = $project_actual_contract_payment_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_actual_contract_payment_update_data))
	{
		$remarks = $project_actual_contract_payment_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("accepted_by",$project_actual_contract_payment_update_data))
	{
		$accepted_by = $project_actual_contract_payment_update_data["accepted_by"];
	}
	else
	{
		$accepted_by = "";
	}

	if(array_key_exists("accepted_on",$project_actual_contract_payment_update_data))
	{
		$accepted_on = $project_actual_contract_payment_update_data["accepted_on"];
	}
	else
	{
		$accepted_on = "";
	}

	if(array_key_exists("approved_by",$project_actual_contract_payment_update_data))
	{
		$approved_by = $project_actual_contract_payment_update_data["approved_by"];
	}
	else
	{
		$approved_by = "";
	}

	if(array_key_exists("approved_on",$project_actual_contract_payment_update_data))
	{
		$approved_on = $project_actual_contract_payment_update_data["approved_on"];
	}
	else
	{
		$approved_on = "";
	}

	if(array_key_exists("added_by",$project_actual_contract_payment_update_data))
	{
		$added_by = $project_actual_contract_payment_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_actual_contract_payment_update_data))
	{
		$added_on = $project_actual_contract_payment_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_actual_contract_payment_update_uquery_base = "update project_actual_contract_payment set ";

	$project_actual_contract_payment_update_uquery_set = "";

	$project_actual_contract_payment_update_uquery_where = " where project_actual_contract_payment_id = :contract_payment_id";

	$project_actual_contract_payment_update_udata = array(":contract_payment_id"=>$contract_payment_id);

	$filter_count = 0;

	if($contract_id != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_id = :contract_id,";
		$project_actual_contract_payment_update_udata[":contract_id"] = $contract_id;
		$filter_count++;
	}

	if($from_date != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_from_date = :from_date,";
		$project_actual_contract_payment_update_udata[":from_date"] = $from_date;
		$filter_count++;
	}

	if($to_date != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_to_date = :to_date,";
		$project_actual_contract_payment_update_udata[":to_date"] = $to_date;
		$filter_count++;
	}

	if($vendor_id != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_vendor_id = :vendor_id,";
		$project_actual_contract_payment_update_udata[":vendor_id"] = $vendor_id;
		$filter_count++;
	}

	if($amount != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_amount = :amount,";
		$project_actual_contract_payment_update_udata[":amount"] = $amount;
		$filter_count++;
	}

	if($tds != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_tds = :tds,";
		$project_actual_contract_payment_update_udata[":tds"] = $tds;
		$filter_count++;
	}

	if($sec_dep_amount != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_deposit_amount = :sec_dep_amount,";
		$project_actual_contract_payment_update_udata[":sec_dep_amount"] = $sec_dep_amount;
		$filter_count++;
	}

	if($number != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_number = :number,";
		$project_actual_contract_payment_update_udata[":number"] = $number;
		$filter_count++;
	}
	if($length != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_length = :length,";
		$project_actual_contract_payment_update_udata[":length"] = $length;
		$filter_count++;
	}

	if($breadth	!= "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_breadth = :breadth,";
		$project_actual_contract_payment_update_udata[":breadth"] = $breadth;
		$filter_count++;
	}

	if($depth != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_depth = :depth,";
		$project_actual_contract_payment_update_udata[":depth"] = $depth;
		$filter_count++;
	}
	if($total_measurement != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_total_measurement = :total_measurement,";
		$project_actual_contract_payment_update_udata[":total_measurement"] = $total_measurement;
		$filter_count++;
	}

	if($uom != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_uom = :uom,";
		$project_actual_contract_payment_update_udata[":uom"] = $uom;
		$filter_count++;
	}

	if($rate != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_rate = :rate,";
		$project_actual_contract_payment_update_udata[":rate"] = $rate;
		$filter_count++;
	}

	if($status != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_status = :status,";
		$project_actual_contract_payment_update_udata[":status"] = $status;
		$filter_count++;
	}
	if($bill_no != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_bill_no = :bill_no,";
		$project_actual_contract_payment_update_udata[":bill_no"] = $bill_no;
		$filter_count++;
	}

	if($billing_address != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_billing_address = :billing_address,";
		$project_actual_contract_payment_update_udata[":billing_address"] = $billing_address;
		$filter_count++;
	}

	if($active != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_active = :active,";
		$project_actual_contract_payment_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_remarks = :remarks,";
		$project_actual_contract_payment_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($accepted_by != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_accepted_by = :accepted_by,";
		$project_actual_contract_payment_update_udata[":accepted_by"] = $accepted_by;
		$filter_count++;
	}

	if($accepted_on != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_accepted_on = :accepted_on,";
		$project_actual_contract_payment_update_udata[":accepted_on"] = $accepted_on;
		$filter_count++;
	}

	if($approved_by != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_approved_by = :approved_by,";
		$project_actual_contract_payment_update_udata[":approved_by"] = $approved_by;
		$filter_count++;
	}

	if($approved_on != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_approved_on = :approved_on,";
		$project_actual_contract_payment_update_udata[":approved_on"] = $approved_on;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_added_by = :added_by,";
		$project_actual_contract_payment_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_actual_contract_payment_update_uquery_set = $project_actual_contract_payment_update_uquery_set." project_actual_contract_payment_added_on = :added_on,";
		$project_actual_contract_payment_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_actual_contract_payment_update_uquery_set = trim($project_actual_contract_payment_update_uquery_set,',');
	}

	$project_actual_contract_payment_update_uquery = $project_actual_contract_payment_update_uquery_base.$project_actual_contract_payment_update_uquery_set.$project_actual_contract_payment_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();

        $project_actual_contract_payment_update_ustatement = $dbConnection->prepare($project_actual_contract_payment_update_uquery);
        $project_actual_contract_payment_update_ustatement -> execute($project_actual_contract_payment_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $contract_payment_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Payment contract Mapping
INPUT 	: Contract ID, Payment ID, Remarks, Added By
OUTPUT 	: Contract Mapping ID, success or failure message
BY 		: Ashwini
*/
function db_add_project_payment_contract_mapping($contract_id,$payment_id,$remarks,$added_by)
{
	// Query
   $project_payment_contract_mapping_iquery = "insert into project_payment_contract_mapping
   (project_payment_contract_mapping_contract_id,project_payment_contract_mapping_payment_id,project_payment_contract_mapping_active,
   project_payment_contract_mapping_remarks,project_payment_contract_mapping_added_by,project_payment_contract_mapping_added_on)
   values(:contract_id,:payment_id,:active,:remarks,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_payment_contract_mapping_istatement = $dbConnection->prepare($project_payment_contract_mapping_iquery);

        // Data
        $project_payment_contract_mapping_idata = array(':contract_id'=>$contract_id,':payment_id'=>$payment_id,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_payment_contract_mapping_istatement->execute($project_payment_contract_mapping_idata);
		$project_payment_contract_mapping_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_payment_contract_mapping_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project Payment Contract  Mapping list
INPUT 	: Contract Mapping ID, Contract ID, Payment ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Payment Contract  Mapping
BY 		: Ashwini
*/
function db_get_project_payment_contract_mapping($project_payment_contract_mapping_search_data)
{

	if(array_key_exists("contract_mapping_id",$project_payment_contract_mapping_search_data))
	{
		$contract_mapping_id = $project_payment_contract_mapping_search_data["contract_mapping_id"];
	}
	else
	{
		$contract_mapping_id= "";
	}

	if(array_key_exists("contract_id",$project_payment_contract_mapping_search_data))
	{
		$contract_id = $project_payment_contract_mapping_search_data["contract_id"];
	}
	else
	{
		$contract_id = "";
	}

	if(array_key_exists("payment_id",$project_payment_contract_mapping_search_data))
	{
		$payment_id = $project_payment_contract_mapping_search_data["payment_id"];
	}
	else
	{
		$payment_id = "";
	}

	if(array_key_exists("active",$project_payment_contract_mapping_search_data))
	{
		$active = $project_payment_contract_mapping_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_payment_contract_mapping_search_data))
	{
		$added_by = $project_payment_contract_mapping_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_payment_contract_mapping_search_data))
	{
		$start_date= $project_payment_contract_mapping_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_payment_contract_mapping_search_data))
	{
		$end_date= $project_payment_contract_mapping_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_project_payment_contract_mapping_list_squery_base = " select *,U.user_name as adder,AU.user_name as approver,OU.user_name as oker from project_payment_contract_mapping PPCM inner join project_task_boq_actuals PTBA on PTBA.project_task_boq_actual_id=PPCM.project_payment_contract_mapping_contract_id inner join stock_unit_measure_master SUM on SUM.stock_unit_id = PTBA.project_task_actual_boq_uom inner join project_plan_process_task PPPT on PPPT.project_process_task_id = PTBA.project_boq_actual_task_id inner join project_task_master PTM on PTM.project_task_master_id=PPPT.project_process_task_type inner join project_contract_process PCP on PCP.project_contract_process_id = PTBA.project_task_boq_actual_contract_process inner join project_contract_rate_master PCRM on PCRM.project_contract_rate_master_id=PTBA.project_task_actual_contract_task inner join project_plan_process PPP on PPP.project_plan_process_id=PPPT.project_process_id inner join project_plan PP on PP.project_plan_id=PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id=PP.project_plan_project_id inner join project_process_master PPM on PPM.project_process_master_id = PPP.project_plan_process_name inner join project_manpower_agency PMA on PMA.project_manpower_agency_id=PTBA.project_task_actual_boq_vendor_id inner join users U on U.user_id= PTBA.project_task_actual_boq_added_by left outer join project_actual_contract_payment PACP on PACP.project_actual_contract_payment_id = PPCM.project_payment_contract_mapping_payment_id inner join stock_company_master SCM on SCM.stock_company_master_id = PACP.project_actual_contract_payment_billing_address inner join project_site_location_mapping_master PSLMM on PSLMM.project_site_location_mapping_master_id = PTBA.project_task_actual_boq_location left outer join users OU on OU.user_id= PTBA.project_task_actual_boq_checked_by left outer join users AU on AU.user_id= PTBA.project_task_actual_boq_approved_by";

	$get_project_payment_contract_mapping_list_squery_where = "";

	$filter_count = 0;

	// Data
	$get_project_payment_contract_mapping_list_sdata = array();

	if($contract_mapping_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_contract_mapping_list_squery_where = $get_project_payment_contract_mapping_list_squery_where." where project_payment_contract_mapping_id = :contract_mapping_id";
		}
		else
		{
			// Query
			$get_project_payment_contract_mapping_list_squery_where = $get_project_payment_contract_mapping_list_squery_where." and project_payment_contract_mapping_id = :contract_mapping_id";
		}

		// Data
		$get_project_payment_contract_mapping_list_sdata[':contract_mapping_id'] = $contract_mapping_id;

		$filter_count++;
	}

	if($contract_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_contract_mapping_list_squery_where = $get_project_payment_contract_mapping_list_squery_where." where project_payment_contract_mapping_contract_id = :contract_id";
		}
		else
		{
			// Query
			$get_project_payment_contract_mapping_list_squery_where = $get_project_payment_contract_mapping_list_squery_where." and project_payment_contract_mapping_contract_id = :contract_id";
		}

		// Data
		$get_project_payment_contract_mapping_list_sdata[':contract_id'] = $contract_id;

		$filter_count++;
	}

	if($payment_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_contract_mapping_list_squery_where = $get_project_payment_contract_mapping_list_squery_where." where project_payment_contract_mapping_payment_id = :payment_id";
		}
		else
		{
			// Query
			$get_project_payment_contract_mapping_list_squery_where = $get_project_payment_contract_mapping_list_squery_where." and project_payment_contract_mapping_payment_id = :payment_id";
		}

		// Data
		$get_project_payment_contract_mapping_list_sdata[':payment_id'] = $payment_id;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_contract_mapping_list_squery_where = $get_project_payment_contract_mapping_list_squery_where." where project_payment_contract_mapping_active = :active";
		}
		else
		{
			// Query
			$get_project_payment_contract_mapping_list_squery_where = $get_project_payment_contract_mapping_list_squery_where." and project_payment_contract_mapping_active = :active";
		}

		// Data
		$get_project_payment_contract_mapping_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_contract_mapping_list_squery_where = $get_project_payment_contract_mapping_list_squery_where." where project_payment_contract_mapping_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_payment_contract_mapping_list_squery_where = $get_project_payment_contract_mapping_list_squery_where." and project_payment_contract_mapping_added_by = :added_by";
		}

		//Data
		$get_project_payment_contract_mapping_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_contract_mapping_list_squery_where = $get_project_payment_contract_mapping_list_squery_where." where project_payment_contract_mapping_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_payment_contract_mapping_list_squery_where = $get_project_payment_contract_mapping_list_squery_where." and project_payment_contract_mapping_added_on >= :start_date";
		}

		//Data
		$get_project_payment_contract_mapping_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_contract_mapping_list_squery_where = $get_project_payment_contract_mapping_list_squery_where." where project_payment_contract_mapping_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_payment_contract_mapping_list_squery_where = $get_project_payment_contract_mapping_list_squery_where." and project_payment_contract_mapping_added_on <= :end_date";
		}

		//Data
		$get_project_payment_contract_mapping_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	$get_project_payment_contract_mapping_list_squery = $get_project_payment_contract_mapping_list_squery_base.$get_project_payment_contract_mapping_list_squery_where;
	try
	{
		$dbConnection = get_conn_handle();

		$get_project_payment_contract_mapping_list_sstatement = $dbConnection->prepare($get_project_payment_contract_mapping_list_squery);
		$get_project_payment_contract_mapping_list_sstatement -> execute($get_project_payment_contract_mapping_list_sdata);

		$get_project_payment_contract_mapping_list_sdetails = $get_project_payment_contract_mapping_list_sstatement -> fetchAll();

		if(FALSE === $get_project_payment_contract_mapping_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_payment_contract_mapping_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_payment_contract_mapping_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

  /*
PURPOSE : To update Project Payment Contract Mapping
INPUT 	: Contract Mapping ID, Project Payment Contract Mapping Update Array
OUTPUT 	: Contract Mapping ID; Message of success or failure
BY 		: Ashwini
*/
function db_update_project_payment_contract_mapping($contract_mapping_id,$project_payment_contract_mapping_update_data)
{
	if(array_key_exists("contract_id",$project_payment_contract_mapping_update_data))
	{
		$contract_id = $project_payment_contract_mapping_update_data["contract_id"];
	}
	else
	{
		$contract_id = "";
	}

	if(array_key_exists("payment_id",$project_payment_contract_mapping_update_data))
	{
		$payment_id = $project_payment_contract_mapping_update_data["payment_id"];
	}
	else
	{
		$payment_id = "";
	}

	if(array_key_exists("active",$project_payment_contract_mapping_update_data))
	{
		$active = $project_payment_contract_mapping_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_payment_contract_mapping_update_data))
	{
		$remarks = $project_payment_contract_mapping_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_payment_contract_mapping_update_data))
	{
		$added_by = $project_payment_contract_mapping_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_payment_contract_mapping_update_data))
	{
		$added_on = $project_payment_contract_mapping_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_payment_contract_mapping_update_uquery_base = "update project_payment_contract_mapping set ";

	$project_payment_contract_mapping_update_uquery_set = "";

	$project_payment_contract_mapping_update_uquery_where = " where project_payment_contract_mapping_id = :contract_mapping_id";

	$project_payment_contract_mapping_update_udata = array(":contract_mapping_id"=>$contract_mapping_id);

	$filter_count = 0;

	if($contract_id != "")
	{
		$project_payment_contract_mapping_update_uquery_set = $project_payment_contract_mapping_update_uquery_set." project_payment_contract_mapping_contract_id = :contract_id,";
		$project_payment_contract_mapping_update_udata[":contract_id"] = $contract_id;
		$filter_count++;
	}

	if($payment_id != "")
	{
		$project_payment_contract_mapping_update_uquery_set = $project_payment_contract_mapping_update_uquery_set." project_payment_contract_mapping_payment_id = :payment_id,";
		$project_payment_contract_mapping_update_udata[":payment_id"] = $payment_id;
		$filter_count++;
	}

	if($active != "")
	{
		$project_payment_contract_mapping_update_uquery_set = $project_payment_contract_mapping_update_uquery_set." project_payment_contract_mapping_active = :active,";
		$project_payment_contract_mapping_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_payment_contract_mapping_update_uquery_set = $project_payment_contract_mapping_update_uquery_set." project_payment_contract_mapping_remarks = :remarks,";
		$project_payment_contract_mapping_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_payment_contract_mapping_update_uquery_set = $project_payment_contract_mapping_update_uquery_set." project_payment_contract_mapping_added_by = :added_by,";
		$project_payment_contract_mapping_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_payment_contract_mapping_update_uquery_set = $project_payment_contract_mapping_update_uquery_set." project_payment_contract_mapping_added_on = :added_on,";
		$project_payment_contract_mapping_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_payment_contract_mapping_update_uquery_set = trim($project_payment_contract_mapping_update_uquery_set,',');
	}

	$project_payment_contract_mapping_update_uquery = $project_payment_contract_mapping_update_uquery_base.$project_payment_contract_mapping_update_uquery_set.$project_payment_contract_mapping_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $project_payment_contract_mapping_update_ustatement = $dbConnection->prepare($project_payment_contract_mapping_update_uquery);

        $project_payment_contract_mapping_update_ustatement -> execute($project_payment_contract_mapping_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $contract_mapping_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Contract Issue Payment
INPUT 	: Payment ID , Contract ID, Amount, Deduction, Vendor ID, Payment Mode, Instrument Details, Active, Remarks, Added By
OUTPUT 	: Payment ID, success or failure message
BY 		: Ashwini
*/
function db_add_project_contract_issue_payment($contract_id,$amount,$deduction,$vendor_id,$payment_mode,$instrument_details,$remarks,$added_by)
{
	// Query
   $project_contract_issue_payment_iquery = "insert into project_contract_issue_payment(		      			project_contract_issue_payment_contract_id,project_contract_issue_payment_amount,project_contract_issue_payment_deduction,project_contract_issue_payment_vendor_id,project_contract_issue_payment_mode,project_contract_issue_payment_instrument_details,project_contract_issue_payment_active,
	project_contract_issue_payment_remarks,project_contract_issue_payment_added_by,project_contract_issue_payment_added_on) values(:contract_id,:amount,:deduction,:vendor_id,:payment_mode,:instrument_details,:active,:remarks,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_contract_issue_payment_istatement = $dbConnection->prepare($project_contract_issue_payment_iquery);

        // Data
        $project_contract_issue_payment_idata = array(':contract_id'=>$contract_id,':amount'=>$amount,':deduction'=>$deduction,':vendor_id'=>$vendor_id,':payment_mode'=>$payment_mode,':instrument_details'=>$instrument_details,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_contract_issue_payment_istatement->execute($project_contract_issue_payment_idata);
		$project_contract_issue_payment_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_contract_issue_payment_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project Contract Issue Payment list
INPUT 	: Project Contract Issue Payment array data
OUTPUT 	: List of Project Contract Issue Payment
BY 		: Ashwini
*/
function db_get_project_contract_issue_payment($project_contract_issue_payment_search_data)
{
	if(array_key_exists("payment_id",$project_contract_issue_payment_search_data))
	{
		$payment_id = $project_contract_issue_payment_search_data["payment_id"];
	}
	else
	{
		$payment_id = "";
	}
	if(array_key_exists("contract_id",$project_contract_issue_payment_search_data))
	{
		$contract_id = $project_contract_issue_payment_search_data["contract_id"];
	}
	else
	{
		$contract_id = "";
	}

	if(array_key_exists("amount",$project_contract_issue_payment_search_data))
	{
		$amount = $project_contract_issue_payment_search_data["amount"];
	}
	else
	{
		$amount = "";
	}

	if(array_key_exists("deduction",$project_contract_issue_payment_search_data))
	{
		$deduction = $project_contract_issue_payment_search_data["deduction"];
	}
	else
	{
		$deduction = "";
	}

	if(array_key_exists("vendor_id",$project_contract_issue_payment_search_data))
	{
		$vendor_id = $project_contract_issue_payment_search_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}

	if(array_key_exists("payment_mode",$project_contract_issue_payment_search_data))
	{
		$payment_mode = $project_contract_issue_payment_search_data["payment_mode"];
	}
	else
	{
		$payment_mode = "";
	}

	if(array_key_exists("active",$project_contract_issue_payment_search_data))
	{
		$active = $project_contract_issue_payment_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_contract_issue_payment_search_data))
	{
		$added_by = $project_contract_issue_payment_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_contract_issue_payment_search_data))
	{
		$start_date = $project_contract_issue_payment_search_data["start_date"];
	}
	else
	{
		$start_date = "";
	}

	if(array_key_exists("end_date",$project_contract_issue_payment_search_data))
	{
		$end_date = $project_contract_issue_payment_search_data["end_date"];
	}
	else
	{
		$end_date = "";
	}

	$get_project_contract_issue_payment_list_squery_base = "select * from project_contract_issue_payment PCIP inner join users U on U.user_id=PCIP.project_contract_issue_payment_added_by inner join project_manpower_agency PMA on PMA.project_manpower_agency_id=PCIP.project_contract_issue_payment_vendor_id inner join payment_mode_master PMM on PMM.payment_mode_id=PCIP.project_contract_issue_payment_mode";

	$get_project_contract_issue_payment_list_squery_where = "";
	$filter_count = 0;

	// Data
	$get_project_contract_issue_payment_list_sdata = array();

	if($payment_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_issue_payment_list_squery_where = $get_project_contract_issue_payment_list_squery_where." where project_contract_issue_payment_id = :payment_id";
		}
		else
		{
			// Query
			$get_project_contract_issue_payment_list_squery_where = $get_project_contract_issue_payment_list_squery_where." and project_contract_issue_payment_id = :payment_id";
		}

		// Data
		$get_project_contract_issue_payment_list_sdata[':payment_id'] = $payment_id;

		$filter_count++;
	}
	if($contract_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_issue_payment_list_squery_where = $get_project_contract_issue_payment_list_squery_where." where project_contract_issue_payment_contract_id = :contract_id";
		}
		else
		{
			// Query
			$get_project_contract_issue_payment_list_squery_where = $get_project_contract_issue_payment_list_squery_where." and project_contract_issue_payment_contract_id = :contract_id";
		}

		// Data
		$get_project_contract_issue_payment_list_sdata[':contract_id'] = $contract_id;

		$filter_count++;
	}

	if($amount != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_issue_payment_list_squery_where = $get_project_contract_issue_payment_list_squery_where." where project_contract_issue_payment_amount = :amount";
		}
		else
		{
			// Query
			$get_project_contract_issue_payment_list_squery_where = $get_project_contract_issue_payment_list_squery_where." and project_contract_issue_payment_amount = :amount";
		}

		// Data
		$get_project_contract_issue_payment_list_sdata[':amount'] = $amount;

		$filter_count++;
	}

	if($deduction != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_issue_payment_list_squery_where = $get_project_contract_issue_payment_list_squery_where." where project_contract_issue_payment_deduction = :deduction";
		}
		else
		{
			// Query
			$get_project_contract_issue_payment_list_squery_where = $get_project_contract_issue_payment_list_squery_where." and project_contract_issue_payment_deduction = :deduction";
		}

		// Data
		$get_project_contract_issue_payment_list_sdata[':deduction'] = $deduction;

		$filter_count++;
	}

	if($vendor_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_issue_payment_list_squery_where = $get_project_contract_issue_payment_list_squery_where." where project_contract_issue_payment_vendor_id = :vendor_id";
		}
		else
		{
			// Query
			$get_project_contract_issue_payment_list_squery_where = $get_project_contract_issue_payment_list_squery_where." and project_contract_issue_payment_vendor_id = :vendor_id";
		}

		// Data
		$get_project_contract_issue_payment_list_sdata[':vendor_id'] = $vendor_id;

		$filter_count++;
	}

	if($payment_mode != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_issue_payment_list_squery_where = $get_project_contract_issue_payment_list_squery_where." where project_contract_issue_payment_mode = :payment_mode";
		}
		else
		{
			// Query
			$get_project_contract_issue_payment_list_squery_where = $get_project_contract_issue_payment_list_squery_where." and project_contract_issue_payment_mode = :payment_mode";
		}

		// Data
		$get_project_contract_issue_payment_list_sdata[':payment_mode'] = $payment_mode;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_issue_payment_list_squery_where = $get_project_contract_issue_payment_list_squery_where." where project_contract_issue_payment_active = :active";
		}
		else
		{
			// Query
			$get_project_contract_issue_payment_list_squery_where = $get_project_contract_issue_payment_list_squery_where." and project_contract_issue_payment_active = :active";
		}

		// Data
		$get_project_contract_issue_payment_list_sdata[':active']  = $active;

		$filter_count++;
	}


	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_issue_payment_list_squery_where = $get_project_contract_issue_payment_list_squery_where." where project_contract_issue_payment_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_contract_issue_payment_list_squery_where = $get_project_contract_issue_payment_list_squery_where." and project_contract_issue_payment_added_by = :added_by";
		}

		//Data
		$get_project_contract_issue_payment_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_issue_payment_list_squery_where = $get_project_contract_issue_payment_list_squery_where." where project_contract_issue_payment_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_contract_issue_payment_list_squery_where = $get_project_contract_issue_payment_list_squery_where." and project_contract_issue_payment_added_on >= :start_date";
		}

		//Data
		$get_project_contract_issue_payment_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_issue_payment_list_squery_where = $get_project_contract_issue_payment_list_squery_where." where project_contract_issue_payment_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_contract_issue_payment_list_squery_where = $get_project_contract_issue_payment_list_squery_where." and project_contract_issue_payment_added_on <= :end_date";
		}

		//Data
		$get_project_contract_issue_payment_list_sdata[':end_date']  = $end_date;

		$filter_count++;
	}

	$get_project_contract_issue_payment_list_squery = $get_project_contract_issue_payment_list_squery_base.$get_project_contract_issue_payment_list_squery_where;
	try
	{
		$dbConnection = get_conn_handle();

		$get_project_contract_issue_payment_list_sstatement = $dbConnection->prepare($get_project_contract_issue_payment_list_squery);

		$get_project_contract_issue_payment_list_sstatement -> execute($get_project_contract_issue_payment_list_sdata);

		$get_project_contract_issue_payment_list_sdetails = $get_project_contract_issue_payment_list_sstatement -> fetchAll();

		if(FALSE === $get_project_contract_issue_payment_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_contract_issue_payment_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_contract_issue_payment_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

  /*
PURPOSE : To update Project Contract Issue Payment list
INPUT 	: Payment ID,Project Contract Issue Payment Update Array
OUTPUT 	: Message of success or failure
BY 		: Ashwini
*/
function db_update_project_contract_issue_payment($payment_id,$project_contract_issue_payment_update_data)
{

	if(array_key_exists("contract_id",$project_contract_issue_payment_update_data))
	{
		$contract_id = $project_contract_issue_payment_update_data["contract_id"];
	}
	else
	{
		$contract_id = "";
	}

	if(array_key_exists("amount",$project_contract_issue_payment_update_data))
	{
		$amount = $project_contract_issue_payment_update_data["amount"];
	}
	else
	{
		$amount = "";
	}

	if(array_key_exists("deduction",$project_contract_issue_payment_update_data))
	{
		$deduction = $project_contract_issue_payment_update_data["deduction"];
	}
	else
	{
		$deduction = "";
	}

	if(array_key_exists("vendor_id",$project_contract_issue_payment_update_data))
	{
		$vendor_id = $project_contract_issue_payment_update_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}

	if(array_key_exists("payment_mode",$project_contract_issue_payment_update_data))
	{
		$payment_mode = $project_contract_issue_payment_update_data["payment_mode"];
	}
	else
	{
		$payment_mode = "";
	}

	if(array_key_exists("instrument_details",$project_contract_issue_payment_update_data))
	{
		$instrument_details = $project_contract_issue_payment_update_data["instrument_details"];
	}
	else
	{
		$instrument_details = "";
	}

	if(array_key_exists("active",$project_contract_issue_payment_update_data))
	{
		$active = $project_contract_issue_payment_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_contract_issue_payment_update_data))
	{
		$remarks = $project_contract_issue_payment_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_contract_issue_payment_update_data))
	{
		$added_by = $project_contract_issue_payment_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_contract_issue_payment_update_data))
	{
		$added_on = $project_contract_issue_payment_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_contract_issue_payment_update_uquery_base = "update project_contract_issue_payment set ";

	$project_contract_issue_payment_update_uquery_set = "";

	$project_contract_issue_payment_update_uquery_where = " where project_contract_issue_payment_id = :payment_id";

	$project_contract_issue_payment_update_udata = array(":payment_id"=>$payment_id);

	$filter_count = 0;

	if($contract_id != "")
	{
		$project_contract_issue_payment_update_uquery_set = $project_contract_issue_payment_update_uquery_set."project_contract_issue_payment_contract_id  = :contract_id,";
		$project_contract_issue_payment_update_udata[":contract_id"] = $contract_id;
		$filter_count++;
	}

	if($amount != "")
	{
		$project_contract_issue_payment_update_uquery_set = $project_contract_issue_payment_update_uquery_set."project_contract_issue_payment_amount  = :amount,";
		$project_contract_issue_payment_update_udata[":amount"] = $amount;
		$filter_count++;
	}

	if($deduction != "")
	{
		$project_contract_issue_payment_update_uquery_set = $project_contract_issue_payment_update_uquery_set."project_contract_issue_payment_deduction  = :deduction,";
		$project_contract_issue_payment_update_udata[":deduction"] = $deduction;
		$filter_count++;
	}

	if($vendor_id != "")
	{
		$project_contract_issue_payment_update_uquery_set = $project_contract_issue_payment_update_uquery_set."project_contract_issue_payment_vendor_id  = :vendor_id,";
		$project_contract_issue_payment_update_udata[":vendor_id"] = $vendor_id;
		$filter_count++;
	}

	if($payment_mode != "")
	{
		$project_contract_issue_payment_update_uquery_set = $project_contract_issue_payment_update_uquery_set."project_contract_issue_payment_mode  = :payment_mode,";
		$project_contract_issue_payment_update_udata[":payment_mode"] = $payment_mode;
		$filter_count++;
	}
	if($instrument_details != "")
	{
		$project_contract_issue_payment_update_uquery_set = $project_contract_issue_payment_update_uquery_set."project_contract_issue_payment_instrument_details  = :instrument_details,";
		$project_contract_issue_payment_update_udata[":instrument_details"] = $instrument_details;
		$filter_count++;
	}
	if($active != "")
	{
		$project_contract_issue_payment_update_uquery_set = $project_contract_issue_payment_update_uquery_set."project_contract_issue_payment_active = :active,";
		$project_contract_issue_payment_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_contract_issue_payment_update_uquery_set = $project_contract_issue_payment_update_uquery_set." project_contract_issue_payment_remarks = :remarks,";
		$project_contract_issue_payment_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_contract_issue_payment_update_uquery_set = $project_contract_issue_payment_update_uquery_set."project_contract_issue_payment_added_by = :added_by,";
		$project_contract_issue_payment_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_contract_issue_payment_update_uquery_set = $project_contract_issue_payment_update_uquery_set."project_contract_issue_payment_added_on = :added_on,";
		$project_contract_issue_payment_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_contract_issue_payment_update_uquery_set = trim($project_contract_issue_payment_update_uquery_set,',');
	}

	$project_contract_issue_payment_update_uquery = $project_contract_issue_payment_update_uquery_base.$project_contract_issue_payment_update_uquery_set.
	$project_contract_issue_payment_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $project_contract_issue_payment_update_ustatement = $dbConnection->prepare($project_contract_issue_payment_update_uquery);

        $project_contract_issue_payment_update_ustatement -> execute($project_contract_issue_payment_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $payment_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Actual Payment Machine
INPUT 	: Machine ID, Vendor ID, Amount, No Of Hours, From Date, To Date, Remarks, Added By
OUTPUT 	: Payment Machine ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_actual_payment_machine($machine_id,$vendor_id,$amount,$no_of_hrs,$from_date,$to_date,$remarks,$added_by)
{
	// Query
	$project_actual_payment_machine_iquery = "insert into project_actual_payment_machine (project_actual_machine_id,project_actual_payment_machine_vendor_id,project_actual_payment_machine_amount,project_actual_payment_machine_status,project_actual_payment_machine_no_of_hrs,project_actual_payment_machine_from_date,project_actual_payment_machine_to_date,project_actual_payment_machine_bill_no,project_actual_payment_machine_billing_address,project_actual_payment_machine_active,project_actual_payment_machine_remarks,project_actual_payment_machine_approved_by,project_actual_payment_machine_approved_on,project_actual_payment_machine_added_by,project_actual_payment_machine_added_on) values (:machine_id,:vendor_id,:amount,:status,:no_of_hrs,:from_date,:to_date,:bill_no,:billing_address,:active,:remarks,:approved_by,;approved_on,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_actual_payment_machine_istatement = $dbConnection->prepare($project_actual_payment_machine_iquery);

        // Data
        $project_actual_payment_machine_idata = array(':machine_id'=>$machine_id,':vendor_id'=>$vendor_id,':amount'=>$amount,':status'=>'Approved',
		':no_of_hrs'=>$no_of_hrs,':from_date'=>$from_date,':to_date'=>$to_date,':bill_no'=>'',':billing_address'=>'',':approved_by'=>'',':approved_on'=>'',':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_actual_payment_machine_istatement->execute($project_actual_payment_machine_idata);
		$project_actual_payment_machine_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_actual_payment_machine_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project Actual Payment Machine list
INPUT 	: Payment Machine ID, Machine ID, Vendor ID, Amount, Status, No of Hrs, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of project Actual Payment Machine
BY 		: Lakshmi
*/
function db_get_project_actual_payment_machine($project_actual_payment_machine_search_data)
{
	if(array_key_exists("payment_machine_id",$project_actual_payment_machine_search_data))
	{
		$payment_machine_id = $project_actual_payment_machine_search_data["payment_machine_id"];
	}
	else
	{
		$payment_machine_id= "";
	}

	if(array_key_exists("machine_id",$project_actual_payment_machine_search_data))
	{
		$machine_id = $project_actual_payment_machine_search_data["machine_id"];
	}
	else
	{
		$machine_id= "";
	}

	if(array_key_exists("vendor_id",$project_actual_payment_machine_search_data))
	{
		$vendor_id = $project_actual_payment_machine_search_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}
	if(array_key_exists("amount",$project_actual_payment_machine_search_data))
	{
		$amount = $project_actual_payment_machine_search_data["amount"];
	}
	else
	{
		$amount = "";
	}

	if(array_key_exists("status",$project_actual_payment_machine_search_data))
	{
		$status = $project_actual_payment_machine_search_data["status"];
	}
	else
	{
		$status = "";
	}

	if(array_key_exists("no_of_hrs",$project_actual_payment_machine_search_data))
	{
		$no_of_hrs = $project_actual_payment_machine_search_data["no_of_hrs"];
	}
	else
	{
		$no_of_hrs = "";
	}

	if(array_key_exists("from_date",$project_actual_payment_machine_search_data))
	{
		$from_date = $project_actual_payment_machine_search_data["from_date"];
	}
	else
	{
		$from_date = "";
	}

	if(array_key_exists("to_date",$project_actual_payment_machine_search_data))
	{
		$to_date = $project_actual_payment_machine_search_data["to_date"];
	}
	else
	{
		$to_date = "";
	}

	if(array_key_exists("active",$project_actual_payment_machine_search_data))
	{
		$active = $project_actual_payment_machine_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_actual_payment_machine_search_data))
	{
		$added_by = $project_actual_payment_machine_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_actual_payment_machine_search_data))
	{
		$start_date= $project_actual_payment_machine_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_actual_payment_machine_search_data))
	{
		$end_date= $project_actual_payment_machine_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_project_actual_payment_machine_list_squery_base = " select * from project_actual_payment_machine PAPM users U on U.user_id = PAPM.project_actual_payment_machine_added_by inner join project_machine_vendor_master PMVM on PMVM.project_machine_vendor_master_id = PAPM.project_actual_payment_machine_vendor_id inner join project_machine_master PMM on PMM.project_machine_master_id = PAPM.project_actual_machine_id";

	$get_project_actual_payment_machine_list_squery_where = "";

	$filter_count = 0;

	// Data
	$get_project_actual_payment_machine_list_sdata = array();

	if($payment_machine_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." where project_actual_payment_machine_id = :payment_machine_id";
		}
		else
		{
			// Query
			$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." and project_actual_payment_machine_id = :payment_machine_id";
		}

		// Data
		$get_project_actual_payment_machine_list_sdata[':payment_machine_id'] = $payment_machine_id;

		$filter_count++;
	}

	if($machine_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." where project_actual_machine_id = :machine_id";
		}
		else
		{
			// Query
			$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." and project_actual_machine_id = :machine_id";
		}

		// Data
		$get_project_actual_payment_machine_list_sdata[':machine_id'] = $machine_id;

		$filter_count++;
	}

	if($vendor_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." where project_actual_payment_machine_vendor_id = :vendor_id";
		}
		else
		{
			// Query
			$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." and project_actual_payment_machine_vendor_id = :vendor_id";
		}

		// Data
		$get_project_actual_payment_machine_list_sdata[':vendor_id'] = $vendor_id;

		$filter_count++;
	}

	if($amount != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." where project_actual_payment_machine_amount = :amount";
		}
		else
		{
			// Query
			$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." and project_actual_payment_machine_amount = :amount";
		}

		// Data
		$get_project_actual_payment_machine_list_sdata[':amount'] = $amount;

		$filter_count++;
	}

	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." where project_actual_payment_machine_status = :status";
		}
		else
		{
			// Query
			$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." and project_actual_payment_machine_status = :status";
		}

		// Data
		$get_project_actual_payment_machine_list_sdata[':status'] = $status;

		$filter_count++;
	}

	if($no_of_hrs != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." where project_actual_payment_machine_no_of_hrs = :no_of_hrs";
		}
		else
		{
			// Query
			$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." and project_actual_payment_machine_no_of_hrs = :no_of_hrs";
		}

		// Data
		$get_project_actual_payment_machine_list_sdata[':no_of_hrs'] = $no_of_hrs;

		$filter_count++;
	}

		if($from_date != "")
	{
		if($filter_count == 0)
		{
			// Query
		$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." where project_actual_payment_machine_from_date = :from_date";
		}
		else
		{
			// Query
		$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." and project_actual_payment_machine_from_date = :from_date";
		}

		// Data
		$get_project_actual_payment_machine_list_sdata[':from_date']  = $from_date;

		$filter_count++;
	}

		if($to_date != "")
	{
		if($filter_count == 0)
		{
			// Query
		$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." where project_actual_payment_machine_to_date = :to_date";
		}
		else
		{
			// Query
		$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." and project_actual_payment_machine_to_date = :to_date";
		}

		// Data
		$get_project_actual_payment_machine_list_sdata[':to_date']  = $to_date;

		$filter_count++;
	}

		if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
		$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." where project_actual_payment_machine_active = :active";
		}
		else
		{
			// Query
		$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." and project_actual_payment_machine_active = :active";
		}

		// Data
		$get_project_actual_payment_machine_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." where project_actual_payment_machine_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." and project_actual_payment_machine_added_by = :added_by";
		}

		//Data
		$get_project_actual_payment_machine_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." where project_actual_payment_machine_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." and project_actual_payment_machine_added_on >= :start_date";
		}

		//Data
		$get_project_actual_payment_machine_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." where project_actual_payment_machine_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_actual_payment_machine_list_squery_where = $get_project_actual_payment_machine_list_squery_where." and project_actual_payment_machine_added_on <= :end_date";
		}

		//Data
		$get_project_actual_payment_machine_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}


	$get_project_actual_payment_machine_list_squery = $get_project_actual_payment_machine_list_squery_base.$get_project_actual_payment_machine_list_squery_where;
	try
	{
		$dbConnection = get_conn_handle();

		$get_project_actual_payment_machine_list_sstatement = $dbConnection->prepare($get_project_actual_payment_machine_list_squery);

		$get_project_actual_payment_machine_list_sstatement -> execute($get_project_actual_payment_machine_list_sdata);

		$get_project_actual_payment_machine_list_sdetails = $get_project_actual_payment_machine_list_sstatement -> fetchAll();

		if(FALSE === $get_project_actual_payment_machine_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_actual_payment_machine_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_actual_payment_machine_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

 /*
PURPOSE : To update Project Actual Payment Machine
INPUT 	: Payment Machine ID, Project Actual Payment Machine Update Array
OUTPUT 	: Payment Machine ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_actual_payment_machine($payment_machine_id,$project_actual_payment_machine_update_data)
{
	if(array_key_exists("machine_id",$project_actual_payment_machine_update_data))
	{
		$machine_id = $project_actual_payment_machine_update_data["machine_id"];
	}
	else
	{
		$machine_id = "";
	}

	if(array_key_exists("vendor_id",$project_actual_payment_machine_update_data))
	{
		$vendor_id = $project_actual_payment_machine_update_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}

	if(array_key_exists("amount",$project_actual_payment_machine_update_data))
	{
		$amount = $project_actual_payment_machine_update_data["amount"];
	}
	else
	{
		$amount = "";
	}

	if(array_key_exists("tds",$project_actual_payment_machine_update_data))
	{
		$tds = $project_actual_payment_machine_update_data["tds"];
	}
	else
	{
		$tds = "";
	}

	if(array_key_exists("status",$project_actual_payment_machine_update_data))
	{
		$status = $project_actual_payment_machine_update_data["status"];
	}
	else
	{
		$status = "";
	}

	if(array_key_exists("no_of_hrs",$project_actual_payment_machine_update_data))
	{
		$no_of_hrs = $project_actual_payment_machine_update_data["no_of_hrs"];
	}
	else
	{
		$no_of_hrs = "";
	}

	if(array_key_exists("from_date",$project_actual_payment_machine_update_data))
	{
		$from_date = $project_actual_payment_machine_update_data["from_date"];
	}
	else
	{
		$from_date = "";
	}

	if(array_key_exists("to_date",$project_actual_payment_machine_update_data))
	{
		$to_date = $project_actual_payment_machine_update_data["to_date"];
	}
	else
	{
		$to_date = "";
	}

	if(array_key_exists("bill_no",$project_actual_payment_machine_update_data))
	{
		$bill_no = $project_actual_payment_machine_update_data["bill_no"];
	}
	else
	{
		$bill_no = "";
	}

	if(array_key_exists("billing_address",$project_actual_payment_machine_update_data))
	{
		$billing_address = $project_actual_payment_machine_update_data["billing_address"];
	}
	else
	{
		$billing_address = "";
	}

	if(array_key_exists("active",$project_actual_payment_machine_update_data))
	{
		$active = $project_actual_payment_machine_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_actual_payment_machine_update_data))
	{
		$remarks = $project_actual_payment_machine_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("approved_by",$project_actual_payment_machine_update_data))
	{
		$approved_by = $project_actual_payment_machine_update_data["approved_by"];
	}
	else
	{
		$approved_by = "";
	}

	if(array_key_exists("approved_on",$project_actual_payment_machine_update_data))
	{
		$approved_on = $project_actual_payment_machine_update_data["approved_on"];
	}
	else
	{
		$approved_on = "";
	}

	if(array_key_exists("added_by",$project_actual_payment_machine_update_data))
	{
		$added_by = $project_actual_payment_machine_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_actual_payment_machine_update_data))
	{
		$added_on = $project_actual_payment_machine_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_actual_payment_machine_update_uquery_base = "update project_actual_payment_machine set ";

	$project_actual_payment_machine_update_uquery_set = "";

	$project_actual_payment_machine_update_uquery_where = " where project_actual_payment_machine_id = :payment_machine_id";

	$project_actual_payment_machine_update_udata = array(":payment_machine_id"=>$payment_machine_id);

	$filter_count = 0;

	if($machine_id != "")
	{
		$project_actual_payment_machine_update_uquery_set = $project_actual_payment_machine_update_uquery_set." project_actual_machine_id = :machine_id,";
		$project_actual_payment_machine_update_udata[":machine_id"] = $machine_id;
		$filter_count++;
	}

	if($vendor_id != "")
	{
		$project_actual_payment_machine_update_uquery_set = $project_actual_payment_machine_update_uquery_set." project_actual_payment_machine_vendor_id = :vendor_id,";
		$project_actual_payment_machine_update_udata[":vendor_id"] = $vendor_id;
		$filter_count++;
	}
	if($amount != "")
	{
		$project_actual_payment_machine_update_uquery_set = $project_actual_payment_machine_update_uquery_set." project_actual_payment_machine_amount = :amount,";
		$project_actual_payment_machine_update_udata[":amount"] = $amount;
		$filter_count++;
	}

	if($tds != "")
	{
		$project_actual_payment_machine_update_uquery_set = $project_actual_payment_machine_update_uquery_set." project_payment_machine_tds = :tds,";
		$project_actual_payment_machine_update_udata[":tds"] = $tds;
		$filter_count++;
	}

	if($status	!= "")
	{
		$project_actual_payment_machine_update_uquery_set = $project_actual_payment_machine_update_uquery_set." project_actual_payment_machine_status = :status,";
		$project_actual_payment_machine_update_udata[":status"] = $status;
		$filter_count++;
	}

	if($no_of_hrs	!= "")
	{
		$project_actual_payment_machine_update_uquery_set = $project_actual_payment_machine_update_uquery_set." project_actual_payment_machine_no_of_hrs = :no_of_hrs,";
		$project_actual_payment_machine_update_udata[":no_of_hrs"] = $no_of_hrs;
		$filter_count++;
	}

	if($from_date != "")
	{
		$project_actual_payment_machine_update_uquery_set = $project_actual_payment_machine_update_uquery_set." project_actual_payment_machine_from_date = :from_date,";
		$project_actual_payment_machine_update_udata[":from_date"] = $from_date;
		$filter_count++;
	}

	if($to_date != "")
	{
		$project_actual_payment_machine_update_uquery_set = $project_actual_payment_machine_update_uquery_set." project_actual_payment_machine_to_date = :to_date,";
		$project_actual_payment_machine_update_udata[":to_date"] = $to_date;
		$filter_count++;
	}

	if($bill_no != "")
	{
		$project_actual_payment_machine_update_uquery_set = $project_actual_payment_machine_update_uquery_set." project_actual_payment_machine_bill_no = :bill_no,";
		$project_actual_payment_machine_update_udata[":bill_no"] = $bill_no;
		$filter_count++;
	}

	if($billing_address != "")
	{
		$project_actual_payment_machine_update_uquery_set = $project_actual_payment_machine_update_uquery_set." project_actual_payment_machine_billing_address = :billing_address,";
		$project_actual_payment_machine_update_udata[":billing_address"] = $billing_address;
		$filter_count++;
	}

	if($active != "")
	{
		$project_actual_payment_machine_update_uquery_set = $project_actual_payment_machine_update_uquery_set." project_actual_payment_machine_active = :active,";
		$project_actual_payment_machine_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_actual_payment_machine_update_uquery_set = $project_actual_payment_machine_update_uquery_set." project_actual_payment_machine_remarks = :remarks,";
		$project_actual_payment_machine_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($approved_by != "")
	{
		$project_actual_payment_machine_update_uquery_set = $project_actual_payment_machine_update_uquery_set." project_actual_payment_machine_approved_by = :approved_by,";
		$project_actual_payment_machine_update_udata[":approved_by"] = $approved_by;
		$filter_count++;
	}

	if($approved_on != "")
	{
		$project_actual_payment_machine_update_uquery_set = $project_actual_payment_machine_update_uquery_set." project_actual_payment_machine_approved_on = :approved_on,";
		$project_actual_payment_machine_update_udata[":approved_on"] = $approved_on;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_actual_payment_machine_update_uquery_set = $project_actual_payment_machine_update_uquery_set." project_actual_payment_machine_added_by = :added_by,";
		$project_actual_payment_machine_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_actual_payment_machine_update_uquery_set = $project_actual_payment_machine_update_uquery_set." project_actual_payment_machine_added_on = :added_on,";
		$project_actual_payment_machine_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_actual_payment_machine_update_uquery_set = trim($project_actual_payment_machine_update_uquery_set,',');
	}

	$project_actual_payment_machine_update_uquery = $project_actual_payment_machine_update_uquery_base.$project_actual_payment_machine_update_uquery_set.$project_actual_payment_machine_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();

        $project_actual_payment_machine_update_ustatement = $dbConnection->prepare($project_actual_payment_machine_update_uquery);

        $project_actual_payment_machine_update_ustatement -> execute($project_actual_payment_machine_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $payment_machine_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Payment Machine Mapping
INPUT 	: Machine Actuals ID, Payment ID, Remarks, Added By
OUTPUT 	: Machine Mapping ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_payment_machine_mapping($machine_actuals_id,$payment_id,$remarks,$added_by)
{
	// Query
	$project_payment_machine_mapping_iquery = "insert into project_payment_machine_mapping (project_payment_machine_mapping_machine_actuals_id,project_payment_machine_mapping_payment_id,project_payment_machine_mapping_active,project_payment_machine_mapping_remarks,project_payment_machine_mapping_added_by,project_payment_machine_mapping_added_on) values (:machine_actuals_id,:payment_id,:active,:remarks,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_payment_machine_mapping_istatement = $dbConnection->prepare($project_payment_machine_mapping_iquery);

        // Data
        $project_payment_machine_mapping_idata = array(':machine_actuals_id'=>$machine_actuals_id,':payment_id'=>$payment_id,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_payment_machine_mapping_istatement->execute($project_payment_machine_mapping_idata);
		$project_payment_machine_mapping_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_payment_machine_mapping_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project Payment Machine Mapping list
INPUT 	: Machine Mapping ID, Machine ID, Payment ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Payment Machine Mapping
BY 		: Lakshmi
*/
function db_get_project_payment_machine_mapping($project_payment_machine_mapping_search_data)
{
	if(array_key_exists("machine_mapping_id",$project_payment_machine_mapping_search_data))
	{
		$machine_mapping_id = $project_payment_machine_mapping_search_data["machine_mapping_id"];
	}
	else
	{
		$machine_mapping_id= "";
	}

	if(array_key_exists("machine_actuals_id",$project_payment_machine_mapping_search_data))
	{
		$machine_actuals_id = $project_payment_machine_mapping_search_data["machine_actuals_id"];
	}
	else
	{
		$machine_actuals_id = "";
	}

	if(array_key_exists("payment_id",$project_payment_machine_mapping_search_data))
	{
		$payment_id = $project_payment_machine_mapping_search_data["payment_id"];
	}
	else
	{
		$payment_id = "";
	}

	if(array_key_exists("active",$project_payment_machine_mapping_search_data))
	{
		$active = $project_payment_machine_mapping_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_payment_machine_mapping_search_data))
	{
		$added_by = $project_payment_machine_mapping_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_payment_machine_mapping_search_data))
	{
		$start_date= $project_payment_machine_mapping_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_payment_machine_mapping_search_data))
	{
		$end_date= $project_payment_machine_mapping_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_project_payment_machine_mapping_list_squery_base = "select * from project_payment_machine_mapping PPMP inner join project_task_actual_machine_plan PTAMP on PTAMP.project_task_actual_machine_plan_id = PPMP.project_payment_machine_mapping_machine_actuals_id inner join project_machine_master PMM on PMM.project_machine_master_id = PTAMP.project_task_machine_id";

	$get_project_payment_machine_mapping_list_squery_where = "";

	$filter_count = 0;

	// Data
	$get_project_payment_machine_mapping_list_sdata = array();

	if($machine_mapping_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_mapping_list_squery_where = $get_project_payment_machine_mapping_list_squery_where." where project_payment_machine_mapping_id = :machine_mapping_id";
		}
		else
		{
			// Query
			$get_project_payment_machine_mapping_list_squery_where = $get_project_payment_machine_mapping_list_squery_where." and project_payment_machine_mapping_id = :machine_mapping_id";
		}

		// Data
		$get_project_payment_machine_mapping_list_sdata[':machine_mapping_id'] = $machine_mapping_id;

		$filter_count++;
	}

	if($machine_actuals_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_mapping_list_squery_where = $get_project_payment_machine_mapping_list_squery_where." where project_payment_machine_mapping_machine_actuals_id = :machine_actuals_id";
		}
		else
		{
			// Query
			$get_project_payment_machine_mapping_list_squery_where = $get_project_payment_machine_mapping_list_squery_where." and project_payment_machine_mapping_machine_actuals_id = :machine_actuals_id";
		}

		// Data
		$get_project_payment_machine_mapping_list_sdata[':machine_actuals_id'] = $machine_actuals_id;

		$filter_count++;
	}

	if($payment_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_mapping_list_squery_where = $get_project_payment_machine_mapping_list_squery_where." where project_payment_machine_mapping_payment_id = :payment_id";
		}
		else
		{
			// Query
			$get_project_payment_machine_mapping_list_squery_where = $get_project_payment_machine_mapping_list_squery_where." and project_payment_machine_mapping_payment_id = :payment_id";
		}

		// Data
		$get_project_payment_machine_mapping_list_sdata[':payment_id'] = $payment_id;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_mapping_list_squery_where = $get_project_payment_machine_mapping_list_squery_where." where project_payment_machine_mapping_active = :active";
		}
		else
		{
			// Query
			$get_project_payment_machine_mapping_list_squery_where = $get_project_payment_machine_mapping_list_squery_where." and project_payment_machine_mapping_active = :active";
		}

		// Data
		$get_project_payment_machine_mapping_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_mapping_list_squery_where = $get_project_payment_machine_mapping_list_squery_where." where project_payment_machine_mapping_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_payment_machine_mapping_list_squery_where = $get_project_payment_machine_mapping_list_squery_where." and project_payment_machine_mapping_added_by = :added_by";
		}

		//Data
		$get_project_payment_machine_mapping_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_mapping_list_squery_where = $get_project_payment_machine_mapping_list_squery_where." where project_payment_machine_mapping_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_payment_machine_mapping_list_squery_where = $get_project_payment_machine_mapping_list_squery_where." and project_payment_machine_mapping_added_on >= :start_date";
		}

		//Data
		$get_project_payment_machine_mapping_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_mapping_list_squery_where = $get_project_payment_machine_mapping_list_squery_where." where project_payment_machine_mapping_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_payment_machine_mapping_list_squery_where = $get_project_payment_machine_mapping_list_squery_where." and project_payment_machine_mapping_added_on <= :end_date";
		}

		//Data
		$get_project_payment_machine_mapping_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	$get_project_payment_machine_mapping_list_squery = $get_project_payment_machine_mapping_list_squery_base.$get_project_payment_machine_mapping_list_squery_where;

	try
	{
		$dbConnection = get_conn_handle();

		$get_project_payment_machine_mapping_list_sstatement = $dbConnection->prepare($get_project_payment_machine_mapping_list_squery);

		$get_project_payment_machine_mapping_list_sstatement -> execute($get_project_payment_machine_mapping_list_sdata);

		$get_project_payment_machine_mapping_list_sdetails = $get_project_payment_machine_mapping_list_sstatement -> fetchAll();

		if(FALSE === $get_project_payment_machine_mapping_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_payment_machine_mapping_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_payment_machine_mapping_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

/*
PURPOSE : To update Project Payment Machine Mapping
INPUT 	: Machine Mapping ID, Project Payment Machine Mapping Update Array
OUTPUT 	: Machine Mapping ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_payment_machine_mapping($machine_mapping_id,$project_payment_machine_mapping_update_data)
{
	if(array_key_exists("machine_actuals_id",$project_payment_machine_mapping_update_data))
	{
		$machine_actuals_id = $project_payment_machine_mapping_update_data["machine_actuals_id"];
	}
	else
	{
		$machine_actuals_id = "";
	}

	if(array_key_exists("payment_id",$project_payment_machine_mapping_update_data))
	{
		$payment_id = $project_payment_machine_mapping_update_data["payment_id"];
	}
	else
	{
		$payment_id = "";
	}

	if(array_key_exists("active",$project_payment_machine_mapping_update_data))
	{
		$active = $project_payment_machine_mapping_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_payment_machine_mapping_update_data))
	{
		$remarks = $project_payment_machine_mapping_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_payment_machine_mapping_update_data))
	{
		$added_by = $project_payment_machine_mapping_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_payment_machine_mapping_update_data))
	{
		$added_on = $project_payment_machine_mapping_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_payment_machine_mapping_update_uquery_base = "update project_payment_machine_mapping set ";

	$project_payment_machine_mapping_update_uquery_set = "";

	$project_payment_machine_mapping_update_uquery_where = " where project_payment_machine_mapping_id = :machine_mapping_id";

	$project_payment_machine_mapping_update_udata = array(":machine_mapping_id"=>$machine_mapping_id);

	$filter_count = 0;

	if($machine_actuals_id != "")
	{
		$project_payment_machine_mapping_update_uquery_set = $project_payment_machine_mapping_update_uquery_set." project_payment_machine_mapping_machine_actuals_id = :machine_actuals_id,";
		$project_payment_machine_mapping_update_udata[":machine_actuals_id"] = $machine_actuals_id;
		$filter_count++;
	}

	if($payment_id != "")
	{
		$project_payment_machine_mapping_update_uquery_set = $project_payment_machine_mapping_update_uquery_set." project_payment_machine_mapping_payment_id = :payment_id,";
		$project_payment_machine_mapping_update_udata[":payment_id"] = $payment_id;
		$filter_count++;
	}

	if($active != "")
	{
		$project_payment_machine_mapping_update_uquery_set = $project_payment_machine_mapping_update_uquery_set." project_payment_machine_mapping_active = :active,";
		$project_payment_machine_mapping_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_payment_machine_mapping_update_uquery_set = $project_payment_machine_mapping_update_uquery_set." project_payment_machine_mapping_remarks = :remarks,";
		$project_payment_machine_mapping_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_payment_machine_mapping_update_uquery_set = $project_payment_machine_mapping_update_uquery_set." project_payment_machine_mapping_added_by = :added_by,";
		$project_payment_machine_mapping_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_payment_machine_mapping_update_uquery_set = $project_payment_machine_mapping_update_uquery_set." project_payment_machine_mapping_added_on = :added_on,";
		$project_payment_machine_mapping_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_payment_machine_mapping_update_uquery_set = trim($project_payment_machine_mapping_update_uquery_set,',');
	}

	$project_payment_machine_mapping_update_uquery = $project_payment_machine_mapping_update_uquery_base.$project_payment_machine_mapping_update_uquery_set.$project_payment_machine_mapping_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $project_payment_machine_mapping_update_ustatement = $dbConnection->prepare($project_payment_machine_mapping_update_uquery);

        $project_payment_machine_mapping_update_ustatement -> execute($project_payment_machine_mapping_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $machine_mapping_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Payment Machine
INPUT 	: Vendor ID, Amount, Status, Bill No, Billing Address, From Date, To Date, Remarks, Accepted By, Accepted On, Approved By, Approved On, Added By
OUTPUT 	: Payment Machine ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_payment_machine($vendor_id,$amount,$bill_no,$billing_address,$from_date,$to_date,$remarks,$accepted_by,$accepted_on,$approved_by,$approved_on,$added_by)
{
	// Query
	$project_payment_machine_iquery = "insert into project_payment_machine  (project_payment_machine_vendor_id,project_payment_machine_amount,project_payment_machine_status,project_payment_machine_bill_no,project_payment_machine_billing_address,project_payment_machine_from_date,project_payment_machine_to_date,project_payment_machine_active,project_payment_machine_remarks,project_payment_machine_accepted_by,project_payment_machine_accepted_on,project_payment_machine_approved_by,project_payment_machine_approved_on,project_payment_machine_added_by,project_payment_machine_added_on) values (:vendor_id,:amount,:status,:bill_no,:billing_address,:from_date,:to_date,:active,:remarks,:accepted_by,:accepted_on,:approved_by,:approved_on,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_payment_machine_istatement = $dbConnection->prepare($project_payment_machine_iquery);

        // Data
        $project_payment_machine_idata = array(':vendor_id'=>$vendor_id,':amount'=>$amount,':status'=>'Approved',':bill_no'=>$bill_no,':billing_address'=>$billing_address,':from_date'=>$from_date,':to_date'=>$to_date,':active'=>'1',':remarks'=>$remarks,':accepted_by'=>$accepted_by,':accepted_on'=>$accepted_on,':approved_by'=>$approved_by,':approved_on'=>$approved_on,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $project_payment_machine_istatement->execute($project_payment_machine_idata);
		$project_payment_machine_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_payment_machine_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project Payment Machine List
INPUT 	: Payment Machine ID, Vendor ID, Amount, Status, Bill No, Billing Address, From Date, To Date, Active, Accepted By, Accepted On, Approved By, Approved On, Added By , Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Payment Machine
BY 		: Lakshmi
*/
function db_get_project_payment_machine($project_payment_machine_search_data)
{
	if(array_key_exists("payment_machine_id",$project_payment_machine_search_data))
	{
		$payment_machine_id = $project_payment_machine_search_data["payment_machine_id"];
	}
	else
	{
		$payment_machine_id= "";
	}

	if(array_key_exists("vendor_id",$project_payment_machine_search_data))
	{
		$vendor_id = $project_payment_machine_search_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}

	if(array_key_exists("amount",$project_payment_machine_search_data))
	{
		$amount = $project_payment_machine_search_data["amount"];
	}
	else
	{
		$amount = "";
	}

	if(array_key_exists("status",$project_payment_machine_search_data))
	{
		$status = $project_payment_machine_search_data["status"];
	}
	else
	{
		$status = "";
	}

	if(array_key_exists("bill_no",$project_payment_machine_search_data))
	{
		$bill_no = $project_payment_machine_search_data["bill_no"];
	}
	else
	{
		$bill_no = "";
	}

	if(array_key_exists("billing_address",$project_payment_machine_search_data))
	{
		$billing_address = $project_payment_machine_search_data["billing_address"];
	}
	else
	{
		$billing_address = "";
	}

	if(array_key_exists("from_date",$project_payment_machine_search_data))
	{
		$from_date = $project_payment_machine_search_data["from_date"];
	}
	else
	{
		$from_date = "";
	}

	if(array_key_exists("to_date",$project_payment_machine_search_data))
	{
		$to_date = $project_payment_machine_search_data["to_date"];
	}
	else
	{
		$to_date = "";
	}

	if(array_key_exists("active",$project_payment_machine_search_data))
	{
		$active = $project_payment_machine_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("accepted_by",$project_payment_machine_search_data))
	{
		$accepted_by = $project_payment_machine_search_data["accepted_by"];
	}
	else
	{
		$accepted_by = "";
	}

	if(array_key_exists("accepted_on",$project_payment_machine_search_data))
	{
		$accepted_on = $project_payment_machine_search_data["accepted_on"];
	}
	else
	{
		$accepted_on = "";
	}

	if(array_key_exists("approved_by",$project_payment_machine_search_data))
	{
		$approved_by = $project_payment_machine_search_data["approved_by"];
	}
	else
	{
		$approved_by = "";
	}

	if(array_key_exists("approved_on",$project_payment_machine_search_data))
	{
		$approved_on = $project_payment_machine_search_data["approved_on"];
	}
	else
	{
		$approved_on = "";
	}

	if(array_key_exists("added_by",$project_payment_machine_search_data))
	{
		$added_by = $project_payment_machine_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_payment_machine_search_data))
	{
		$start_date= $project_payment_machine_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_payment_machine_search_data))
	{
		$end_date= $project_payment_machine_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_project_payment_machine_list_squery_base = "select *,AU.user_name as accepted_by,U.user_name as added_by from project_payment_machine PM inner join users U on U.user_id = PM.project_payment_machine_added_by inner join project_machine_vendor_master PMVM on PMVM.project_machine_vendor_master_id = PM.project_payment_machine_vendor_id left outer join stock_company_master SCM on SCM.stock_company_master_id = PM.project_payment_machine_billing_address left outer join users AU on AU.user_id = PM.project_payment_machine_accepted_by";

	$get_project_payment_machine_list_squery_where = "";
	$filter_count = 0;

	// Data
	$get_project_payment_machine_list_sdata = array();

	if($payment_machine_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." where project_payment_machine_id = :payment_machine_id";
		}
		else
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." and project_payment_machine_id = :payment_machine_id";
		}

		// Data
		$get_project_payment_machine_list_sdata[':payment_machine_id'] = $payment_machine_id;

		$filter_count++;
	}

	if($vendor_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." where project_payment_machine_vendor_id = :vendor_id";
		}
		else
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." and project_payment_machine_vendor_id = :vendor_id";
		}

		// Data
		$get_project_payment_machine_list_sdata[':vendor_id'] = $vendor_id;

		$filter_count++;
	}

	if($amount != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." where project_payment_machine_amount = :amount";
		}
		else
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." and project_payment_machine_amount = :amount";
		}

		// Data
		$get_project_payment_machine_list_sdata[':amount'] = $amount;

		$filter_count++;
	}

	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." where project_payment_machine_status = :status";
		}
		else
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." and project_payment_machine_status = :status";
		}

		// Data
		$get_project_payment_machine_list_sdata[':status'] = $status;

		$filter_count++;
	}

	if($bill_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." where project_payment_machine_bill_no = :bill_no";
		}
		else
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." and project_payment_machine_bill_no = :bill_no";
		}

		// Data
		$get_project_payment_machine_list_sdata[':bill_no'] = $bill_no;

		$filter_count++;
	}

	if($billing_address != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." where project_payment_machine_billing_address = :billing_address";
		}
		else
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." and project_payment_machine_billing_address = :billing_address";
		}

		// Data
		$get_project_payment_machine_list_sdata[':billing_address'] = $billing_address;

		$filter_count++;
	}

	if($from_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." where project_payment_machine_from_date = :from_date";
		}
		else
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." and project_payment_machine_from_date = :from_date";
		}

		// Data
		$get_project_payment_machine_list_sdata[':from_date'] = $from_date;

		$filter_count++;
	}

	if($to_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." where project_payment_machine_to_date = :to_date";
		}
		else
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." and project_payment_machine_to_date = :to_date";
		}

		// Data
		$get_project_payment_machine_list_sdata[':to_date'] = $to_date;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." where project_payment_machine_active = :active";
		}
		else
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." and project_payment_machine_active = :active";
		}

		// Data
		$get_project_payment_machine_list_sdata[':active'] = $active;

		$filter_count++;
	}

	if($accepted_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." where project_payment_machine_accepted_by = :accepted_by";
		}
		else
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." and project_payment_machine_accepted_by = :accepted_by";
		}

		// Data
		$get_project_payment_machine_list_sdata[':accepted_by'] = $accepted_by;

		$filter_count++;
	}

	if($accepted_on != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." where project_payment_machine_accepted_on = :accepted_on";
		}
		else
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." and project_payment_machine_accepted_on = :accepted_on";
		}

		// Data
		$get_project_payment_machine_list_sdata[':accepted_on'] = $accepted_on;

		$filter_count++;
	}

	if($approved_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." where project_payment_machine_approved_by = :approved_by";
		}
		else
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." and project_payment_machine_approved_by = :approved_by";
		}

		// Data
		$get_project_payment_machine_list_sdata[':approved_by'] = $approved_by;

		$filter_count++;
	}

	if($approved_on != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." where project_payment_machine_approved_on = :approved_on";
		}
		else
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." and project_payment_machine_approved_on = :approved_on";
		}

		// Data
		$get_project_payment_machine_list_sdata[':approved_on']  = $approved_on;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." where project_payment_machine_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." and project_payment_machine_added_by = :added_by";
		}

		//Data
		$get_project_payment_machine_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." where project_payment_machine_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." and project_payment_machine_added_on >= :start_date";
		}

		//Data
		$get_project_payment_machine_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." where project_payment_machine_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." and project_payment_machine_added_on <= :end_date";
		}

		//Data
		$get_project_payment_machine_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	if(array_key_exists("sort",$project_payment_machine_search_data))
	{
		if($project_payment_machine_search_data['sort'] == '1')
		{
			$get_machine_list_squery_order = " order by project_payment_machine_id desc limit 0,1";
		}
		else
		{
			$get_machine_list_squery_order = " order by project_payment_machine_added_on DESC";
		}
	}
  else {
    $get_machine_list_squery_order = " order by project_payment_machine_added_on DESC";
  }

	if(array_key_exists("empty_check",$project_payment_machine_search_data))
	{
		if($filter_count == 0)
		{
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." where project_payment_machine_bill_no != ''";
		}
		else
		{
			$get_project_payment_machine_list_squery_where = $get_project_payment_machine_list_squery_where." and project_payment_machine_bill_no != ''";
		}
		$filter_count++;
	}

	$get_project_payment_machine_list_squery = $get_project_payment_machine_list_squery_base.$get_project_payment_machine_list_squery_where.$get_machine_list_squery_order;

	try
	{
		$dbConnection = get_conn_handle();

		$get_project_payment_machine_list_sstatement = $dbConnection->prepare($get_project_payment_machine_list_squery);

		$get_project_payment_machine_list_sstatement -> execute($get_project_payment_machine_list_sdata);

		$get_project_payment_machine_list_sdetails = $get_project_payment_machine_list_sstatement -> fetchAll();

		if(FALSE === $get_project_payment_machine_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_payment_machine_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_payment_machine_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

/*
PURPOSE : To update Project Payment Machine
INPUT 	: Payment Machine ID, Project Payment Machine Update Array
OUTPUT 	: Payment Machine ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_payment_machine($payment_machine_id,$project_payment_machine_update_data)
{
	if(array_key_exists("vendor_id",$project_payment_machine_update_data))
	{
		$vendor_id = $project_payment_machine_update_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}

	if(array_key_exists("amount",$project_payment_machine_update_data))
	{
		$amount = $project_payment_machine_update_data["amount"];
	}
	else
	{
		$amount = "";
	}

	if(array_key_exists("tds",$project_payment_machine_update_data))
	{
		$tds = $project_payment_machine_update_data["tds"];
	}
	else
	{
		$tds = "";
	}

	if(array_key_exists("status",$project_payment_machine_update_data))
	{
		$status = $project_payment_machine_update_data["status"];
	}
	else
	{
		$status = "";
	}

	if(array_key_exists("bill_no",$project_payment_machine_update_data))
	{
		$bill_no = $project_payment_machine_update_data["bill_no"];
	}
	else
	{
		$bill_no = "";
	}

	if(array_key_exists("billing_address",$project_payment_machine_update_data))
	{
		$billing_address = $project_payment_machine_update_data["billing_address"];
	}
	else
	{
		$billing_address = "";
	}

	if(array_key_exists("from_date",$project_payment_machine_update_data))
	{
		$from_date = $project_payment_machine_update_data["from_date"];
	}
	else
	{
		$from_date = "";
	}

	if(array_key_exists("to_date",$project_payment_machine_update_data))
	{
		$to_date = $project_payment_machine_update_data["to_date"];
	}
	else
	{
		$to_date = "";
	}

	if(array_key_exists("active",$project_payment_machine_update_data))
	{
		$active = $project_payment_machine_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_payment_machine_update_data))
	{
		$remarks = $project_payment_machine_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("accepted_by",$project_payment_machine_update_data))
	{
		$accepted_by = $project_payment_machine_update_data["accepted_by"];
	}
	else
	{
		$accepted_by = "";
	}

	if(array_key_exists("accepted_on",$project_payment_machine_update_data))
	{
		$accepted_on = $project_payment_machine_update_data["accepted_on"];
	}
	else
	{
		$accepted_on = "";
	}

	if(array_key_exists("approved_by",$project_payment_machine_update_data))
	{
		$approved_by = $project_payment_machine_update_data["approved_by"];
	}
	else
	{
		$approved_by = "";
	}

	if(array_key_exists("approved_on",$project_payment_machine_update_data))
	{
		$approved_on = $project_payment_machine_update_data["approved_on"];
	}
	else
	{
		$approved_on = "";
	}

	if(array_key_exists("added_by",$project_payment_machine_update_data))
	{
		$added_by = $project_payment_machine_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_payment_machine_update_data))
	{
		$added_on = $project_payment_machine_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_payment_machine_update_uquery_base = "update project_payment_machine set ";

	$project_payment_machine_update_uquery_set = "";

	$project_payment_machine_update_uquery_where = " where project_payment_machine_id = :payment_machine_id";

	$project_payment_machine_update_udata = array(":payment_machine_id"=>$payment_machine_id);

	$filter_count = 0;

	if($vendor_id != "")
	{
		$project_payment_machine_update_uquery_set = $project_payment_machine_update_uquery_set." project_payment_machine_vendor_id = :vendor_id,";
		$project_payment_machine_update_udata[":vendor_id"] = $vendor_id;
		$filter_count++;
	}

	if($amount != "")
	{
		$project_payment_machine_update_uquery_set = $project_payment_machine_update_uquery_set." project_payment_machine_amount = :amount,";
		$project_payment_machine_update_udata[":amount"] = $amount;
		$filter_count++;
	}

	if($tds != "")
	{
		$project_payment_machine_update_uquery_set = $project_payment_machine_update_uquery_set." project_payment_machine_tds = :tds,";
		$project_payment_machine_update_udata[":tds"] = $tds;
		$filter_count++;
	}


	if($status != "")
	{
		$project_payment_machine_update_uquery_set = $project_payment_machine_update_uquery_set." project_payment_machine_status = :status,";
		$project_payment_machine_update_udata[":status"] = $status;
		$filter_count++;
	}

	if($bill_no != "")
	{
		$project_payment_machine_update_uquery_set = $project_payment_machine_update_uquery_set." project_payment_machine_bill_no = :bill_no,";
		$project_payment_machine_update_udata[":bill_no"] = $bill_no;
		$filter_count++;
	}

	if($billing_address != "")
	{
		$project_payment_machine_update_uquery_set = $project_payment_machine_update_uquery_set." project_payment_machine_billing_address = :billing_address,";
		$project_payment_machine_update_udata[":billing_address"] = $billing_address;
		$filter_count++;
	}

	if($from_date != "")
	{
		$project_payment_machine_update_uquery_set = $project_payment_machine_update_uquery_set." project_payment_machine_from_date = :from_date,";
		$project_payment_machine_update_udata[":from_date"] = $from_date;
		$filter_count++;
	}

	if($to_date != "")
	{
		$project_payment_machine_update_uquery_set = $project_payment_machine_update_uquery_set." project_payment_machine_to_date = :to_date,";
		$project_payment_machine_update_udata[":to_date"] = $to_date;
		$filter_count++;
	}

	if($active != "")
	{
		$project_payment_machine_update_uquery_set = $project_payment_machine_update_uquery_set." project_payment_machine_active = :active,";
		$project_payment_machine_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_payment_machine_update_uquery_set = $project_payment_machine_update_uquery_set." project_payment_machine_remarks = :remarks,";
		$project_payment_machine_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($accepted_by != "")
	{
		$project_payment_machine_update_uquery_set = $project_payment_machine_update_uquery_set." project_payment_machine_accepted_by = :accepted_by,";
		$project_payment_machine_update_udata[":accepted_by"] = $accepted_by;
		$filter_count++;
	}

	if($accepted_on != "")
	{
		$project_payment_machine_update_uquery_set = $project_payment_machine_update_uquery_set." project_payment_machine_accepted_on = :accepted_on,";
		$project_payment_machine_update_udata[":accepted_on"] = $accepted_on;
		$filter_count++;
	}

	if($approved_by != "")
	{
		$project_payment_machine_update_uquery_set = $project_payment_machine_update_uquery_set." project_payment_machine_approved_by = :approved_by,";
		$project_payment_machine_update_udata[":approved_by"] = $approved_by;
		$filter_count++;
	}

	if($approved_on != "")
	{
		$project_payment_machine_update_uquery_set = $project_payment_machine_update_uquery_set." project_payment_machine_approved_on = :approved_on,";
		$project_payment_machine_update_udata[":approved_on"] = $approved_on;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_payment_machine_update_uquery_set = $project_payment_machine_update_uquery_set." project_payment_machine_added_by = :added_by,";
		$project_payment_machine_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_payment_machine_update_uquery_set = $project_payment_machine_update_uquery_set." project_payment_machine_added_on = :added_on,";
		$project_payment_machine_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_payment_machine_update_uquery_set = trim($project_payment_machine_update_uquery_set,',');
	}

	$project_payment_machine_update_uquery = $project_payment_machine_update_uquery_base.$project_payment_machine_update_uquery_set.$project_payment_machine_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $project_payment_machine_update_ustatement = $dbConnection->prepare($project_payment_machine_update_uquery);

        $project_payment_machine_update_ustatement -> execute($project_payment_machine_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $payment_machine_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Machine Issue Payment
INPUT 	: Payment ID ,Machine ID, Amount, Vendor ID, Payment Mode, Instrument Details, Active, Remarks, Added By, Added on
OUTPUT 	: Payment ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_machine_issue_payment($machine_id,$amount,$vendor_id,$payment_mode,$instrument_details,$remarks,$added_by)
{
	// Query
	$project_machine_issue_payment_iquery = "insert into project_machine_issue_payment (project_machine_issue_payment_machine_id,project_machine_issue_payment_amount,project_machine_issue_payment_vendor_id,project_machine_issue_payment_mode,project_machine_issue_payment_instrument_details,project_machine_issue_payment_active,project_machine_issue_payment_remarks,project_machine_issue_payment_added_by,project_machine_issue_payment_added_on)values(:machine_id,:amount,:vendor_id,:payment_mode,:instrument_details,:active,:remarks,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_machine_issue_payment_istatement = $dbConnection->prepare($project_machine_issue_payment_iquery);

        // Data
        $project_machine_issue_payment_idata = array(':machine_id'=>$machine_id,':amount'=>$amount,':vendor_id'=>$vendor_id,':payment_mode'=>$payment_mode,':instrument_details'=>$instrument_details,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_machine_issue_payment_istatement->execute($project_machine_issue_payment_idata);
		$project_machine_issue_payment_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_machine_issue_payment_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project Machine Issue Payment list
INPUT 	: Project Machine Issue Payment array data
OUTPUT 	: List of Project Machine Issue Payment
BY 		: Lakshmi
*/
function db_get_project_machine_issue_payment($project_machine_issue_payment_search_data)
{
	if(array_key_exists("payment_id",$project_machine_issue_payment_search_data))
	{
		$payment_id = $project_machine_issue_payment_search_data["payment_id"];
	}
	else
	{
		$payment_id = "";
	}

	if(array_key_exists("machine_id",$project_machine_issue_payment_search_data))
	{
		$machine_id = $project_machine_issue_payment_search_data["machine_id"];
	}
	else
	{
		$machine_id = "";
	}

	if(array_key_exists("amount",$project_machine_issue_payment_search_data))
	{
		$amount = $project_machine_issue_payment_search_data["amount"];
	}
	else
	{
		$amount = "";
	}

	if(array_key_exists("vendor_id",$project_machine_issue_payment_search_data))
	{
		$vendor_id = $project_machine_issue_payment_search_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}

	if(array_key_exists("payment_mode",$project_machine_issue_payment_search_data))
	{
		$payment_mode = $project_machine_issue_payment_search_data["payment_mode"];
	}
	else
	{
		$payment_mode = "";
	}

	if(array_key_exists("instrument_details",$project_machine_issue_payment_search_data))
	{
		$instrument_details = $project_machine_issue_payment_search_data["instrument_details"];
	}
	else
	{
		$instrument_details = "";
	}

	if(array_key_exists("active",$project_machine_issue_payment_search_data))
	{
		$active = $project_machine_issue_payment_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_machine_issue_payment_search_data))
	{
		$added_by = $project_machine_issue_payment_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_machine_issue_payment_search_data))
	{
		$start_date = $project_machine_issue_payment_search_data["start_date"];
	}
	else
	{
		$start_date = "";
	}

	if(array_key_exists("end_date",$project_machine_issue_payment_search_data))
	{
		$end_date = $project_machine_issue_payment_search_data["end_date"];
	}
	else
	{
		$end_date = "";
	}

	$get_project_machine_issue_payment_list_squery_base = "select * from project_machine_issue_payment PMMIP inner join  users U on U.user_id = PMMIP.project_machine_issue_payment_added_by inner join project_machine_vendor_master PMVM on PMVM.project_machine_vendor_master_id =
	PMMIP.project_machine_issue_payment_vendor_id inner join payment_mode_master PM on PM.payment_mode_id = PMMIP.project_machine_issue_payment_mode";

	$get_project_machine_issue_payment_list_squery_where = "";
	$filter_count = 0;

	// Data
	$get_project_machine_issue_payment_list_sdata = array();

	if($payment_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_issue_payment_list_squery_where = $get_project_machine_issue_payment_list_squery_where." where project_machine_issue_payment_id = :payment_id";
		}
		else
		{
			// Query
			$get_project_machine_issue_payment_list_squery_where = $get_project_machine_issue_payment_list_squery_where." and project_machine_issue_payment_id = :payment_id";
		}

		// Data
		$get_project_machine_issue_payment_list_sdata[':payment_id'] = $payment_id;

		$filter_count++;
	}
	if($machine_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_issue_payment_list_squery_where = $get_project_machine_issue_payment_list_squery_where." where project_machine_issue_payment_machine_id = :machine_id";
		}
		else
		{
			// Query
			$get_project_machine_issue_payment_list_squery_where = $get_project_machine_issue_payment_list_squery_where." and project_machine_issue_payment_machine_id = :machine_id";
		}

		// Data
		$get_project_machine_issue_payment_list_sdata[':machine_id'] = $machine_id;

		$filter_count++;
	}

	if($amount != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_issue_payment_list_squery_where = $get_project_machine_issue_payment_list_squery_where." where project_machine_issue_payment_amount = :amount";
		}
		else
		{
			// Query
			$get_project_machine_issue_payment_list_squery_where = $get_project_machine_issue_payment_list_squery_where." and project_machine_issue_payment_amount = :amount";
		}

		// Data
		$get_project_machine_issue_payment_list_sdata[':amount'] = $amount;

		$filter_count++;
	}

	if($vendor_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_issue_payment_list_squery_where = $get_project_machine_issue_payment_list_squery_where." where project_machine_issue_payment_vendor_id = :vendor_id";
		}
		else
		{
			// Query
			$get_project_machine_issue_payment_list_squery_where = $get_project_machine_issue_payment_list_squery_where." and project_machine_issue_payment_vendor_id = :vendor_id";
		}

		// Data
		$get_project_machine_issue_payment_list_sdata[':vendor_id'] = $vendor_id;

		$filter_count++;
	}

	if($payment_mode != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_issue_payment_list_squery_where = $get_project_machine_issue_payment_list_squery_where." where project_machine_issue_payment_mode = :payment_mode";
		}
		else
		{
			// Query
			$get_project_machine_issue_payment_list_squery_where = $get_project_machine_issue_payment_list_squery_where." and project_machine_issue_payment_mode = :payment_mode";
		}

		// Data
		$get_project_machine_issue_payment_list_sdata[':payment_mode'] = $payment_mode;

		$filter_count++;
	}

	if($instrument_details != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_issue_payment_list_squery_where = $get_project_machine_issue_payment_list_squery_where." where project_machine_issue_payment_instrument_details = :instrument_details";
		}
		else
		{
			// Query
			$get_project_machine_issue_payment_list_squery_where = $get_project_machine_issue_payment_list_squery_where." and project_machine_issue_payment_instrument_details = :instrument_details";
		}

		// Data
		$get_project_machine_issue_payment_list_sdata[':instrument_details'] = $instrument_details;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_issue_payment_list_squery_where = $get_project_machine_issue_payment_list_squery_where." where project_machine_issue_payment_active = :active";
		}
		else
		{
			// Query
			$get_project_machine_issue_payment_list_squery_where = $get_project_machine_issue_payment_list_squery_where." and project_machine_issue_payment_active = :active";
		}

		// Data
		$get_project_machine_issue_payment_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_issue_payment_list_squery_where = $get_project_machine_issue_payment_list_squery_where." where project_machine_issue_payment_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_machine_issue_payment_list_squery_where = $get_project_machine_issue_payment_list_squery_where." and project_machine_issue_payment_added_by = :added_by";
		}

		//Data
		$get_project_machine_issue_payment_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_issue_payment_list_squery_where = $get_project_machine_issue_payment_list_squery_where." where project_machine_issue_payment_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_machine_issue_payment_list_squery_where = $get_project_machine_issue_payment_list_squery_where." and project_machine_issue_payment_added_on >= :start_date";
		}

		//Data
		$get_project_machine_issue_payment_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_issue_payment_list_squery_where = $get_project_machine_issue_payment_list_squery_where." where project_machine_issue_payment_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_machine_issue_payment_list_squery_where = $get_project_machine_issue_payment_list_squery_where." and project_machine_issue_payment_added_on <= :end_date";
		}

		//Data
		$get_project_machine_issue_payment_list_sdata[':end_date']  = $end_date;

		$filter_count++;
	}

	$get_project_machine_issue_payment_list_squery = $get_project_machine_issue_payment_list_squery_base.$get_project_machine_issue_payment_list_squery_where;

	try
	{
		$dbConnection = get_conn_handle();

		$get_project_machine_issue_payment_list_sstatement = $dbConnection->prepare($get_project_machine_issue_payment_list_squery);

		$get_project_machine_issue_payment_list_sstatement -> execute($get_project_machine_issue_payment_list_sdata);

		$get_project_machine_issue_payment_list_sdetails = $get_project_machine_issue_payment_list_sstatement -> fetchAll();

		if(FALSE === $get_project_machine_issue_payment_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_machine_issue_payment_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_machine_issue_payment_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

/*
PURPOSE : To update Project Machine Issue Payment list
INPUT 	: Payment ID,Project Machine Issue Payment Update Array
OUTPUT 	: Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_machine_issue_payment($payment_id,$project_machine_issue_payment_update_data)
{
	if(array_key_exists("machine_id",$project_machine_issue_payment_update_data))
	{
		$machine_id = $project_machine_issue_payment_update_data["machine_id"];
	}
	else
	{
		$machine_id = "";
	}
	if(array_key_exists("amount",$project_machine_issue_payment_update_data))
	{
		$amount = $project_machine_issue_payment_update_data["amount"];
	}
	else
	{
		$amount = "";
	}

	if(array_key_exists("vendor_id",$project_machine_issue_payment_update_data))
	{
		$vendor_id = $project_machine_issue_payment_update_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}

	if(array_key_exists("payment_mode",$project_machine_issue_payment_update_data))
	{
		$payment_mode = $project_machine_issue_payment_update_data["payment_mode"];
	}
	else
	{
		$payment_mode = "";
	}

	if(array_key_exists("instrument_details",$project_machine_issue_payment_update_data))
	{
		$instrument_details = $project_machine_issue_payment_update_data["instrument_details"];
	}
	else
	{
		$instrument_details = "";
	}

	if(array_key_exists("active",$project_machine_issue_payment_update_data))
	{
		$active = $project_machine_issue_payment_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_machine_issue_payment_update_data))
	{
		$remarks = $project_machine_issue_payment_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_machine_issue_payment_update_data))
	{
		$added_by = $project_machine_issue_payment_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_machine_issue_payment_update_data))
	{
		$added_on = $project_machine_issue_payment_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_machine_issue_payment_update_uquery_base = "update project_machine_issue_payment set ";

	$project_machine_issue_payment_update_uquery_set = "";

	$project_machine_issue_payment_update_uquery_where = " where project_machine_issue_payment_id = :payment_id";

	$project_machine_issue_payment_update_udata = array(":payment_id"=>$payment_id);

	$filter_count = 0;

	if($machine_id != "")
	{
		$project_machine_issue_payment_update_uquery_set = $project_machine_issue_payment_update_uquery_set."project_machine_issue_payment_machine_id  = :machine_id,";
		$project_machine_issue_payment_update_udata[":machine_id"] = $machine_id;
		$filter_count++;
	}
	if($amount != "")
	{
		$project_machine_issue_payment_update_uquery_set = $project_machine_issue_payment_update_uquery_set."project_machine_issue_payment_amount  = :amount,";
		$project_machine_issue_payment_update_udata[":amount"] = $amount;
		$filter_count++;
	}

	if($vendor_id != "")
	{
		$project_machine_issue_payment_update_uquery_set = $project_machine_issue_payment_update_uquery_set."project_machine_issue_payment_vendor_id  = :vendor_id,";
		$project_machine_issue_payment_update_udata[":vendor_id"] = $vendor_id;
		$filter_count++;
	}

	if($payment_mode != "")
	{
		$project_machine_issue_payment_update_uquery_set = $project_machine_issue_payment_update_uquery_set."project_machine_issue_payment_mode  = :payment_mode,";
		$project_machine_issue_payment_update_udata[":payment_mode"] = $payment_mode;
		$filter_count++;
	}

	if($instrument_details != "")
	{
		$project_machine_issue_payment_update_uquery_set = $project_machine_issue_payment_update_uquery_set."project_machine_issue_payment_instrument_details  = :instrument_details,";
		$project_machine_issue_payment_update_udata[":instrument_details"] = $instrument_details;
		$filter_count++;
	}
	if($active != "")
	{
		$project_machine_issue_payment_update_uquery_set = $project_machine_issue_payment_update_uquery_set."project_machine_issue_payment_active = :active,";
		$project_machine_issue_payment_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_machine_issue_payment_update_uquery_set = $project_machine_issue_payment_update_uquery_set." project_machine_issue_payment_remarks = :remarks,";
		$project_machine_issue_payment_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_machine_issue_payment_update_uquery_set = $project_machine_issue_payment_update_uquery_set."project_machine_issue_payment_added_by = :added_by,";
		$project_machine_issue_payment_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_machine_issue_payment_update_uquery_set = $project_machine_issue_payment_update_uquery_set."project_machine_issue_payment_added_on = :added_on,";
		$project_machine_issue_payment_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_machine_issue_payment_update_uquery_set = trim($project_machine_issue_payment_update_uquery_set,',');
	}

	$project_machine_issue_payment_update_uquery = $project_machine_issue_payment_update_uquery_base.$project_machine_issue_payment_update_uquery_set.
	$project_machine_issue_payment_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $project_machine_issue_payment_update_ustatement = $dbConnection->prepare($project_machine_issue_payment_update_uquery);

        $project_machine_issue_payment_update_ustatement -> execute($project_machine_issue_payment_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $payment_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To get project Man Power Estimate list
INPUT 	: Project, Active
OUTPUT 	: List of project Man Power Estimate
BY 		: Lakshmi
*/
function db_get_man_power_estimate($project,$active)
{
	$get_project_man_power_estimate_list_squery_base = " select * from project_man_power_estimate PMPE inner join users U on U.user_id= PMPE.project_man_power_estimate_added_by inner join project_plan_process_task PPPT on PPPT.project_process_task_id = PMPE.project_man_power_estimate_task_id inner join project_task_master PTM on PTM.project_task_master_id=PPPT.project_process_task_type inner join project_plan_process PPP on PPP.project_plan_process_id=PPPT.project_process_id inner join project_plan PP on PP.project_plan_id=PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id=PP.project_plan_project_id inner join project_process_master PPM on PPM.project_process_master_id = PPP.project_plan_process_name";

	$get_project_man_power_estimate_list_squery_where = "";

	$filter_count = 0;

	// Data
	$get_project_man_power_estimate_list_sdata = array();

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." where project_man_power_estimate_active = :active";
		}
		else
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." and project_man_power_estimate_active = :active";
		}

		// Data
		$get_project_man_power_estimate_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." where PP.project_plan_project_id = :project";
		}
		else
		{
			// Query
			$get_project_man_power_estimate_list_squery_where = $get_project_man_power_estimate_list_squery_where." and PP.project_plan_project_id = :project";
		}

		//Data
		$get_project_man_power_estimate_list_sdata['project']  = $project;

		$filter_count++;
	}

	$get_project_man_power_estimate_list_squery = $get_project_man_power_estimate_list_squery_base.$get_project_man_power_estimate_list_squery_where;

	try
	{
		$dbConnection = get_conn_handle();

		$get_project_man_power_estimate_list_sstatement = $dbConnection->prepare($get_project_man_power_estimate_list_squery);

		$get_project_man_power_estimate_list_sstatement -> execute($get_project_man_power_estimate_list_sdata);

		$get_project_man_power_estimate_list_sdetails = $get_project_man_power_estimate_list_sstatement -> fetchAll();

		if(FALSE === $get_project_man_power_estimate_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_man_power_estimate_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_man_power_estimate_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
}
/*
PURPOSE : To get Project Task Actual Man Power list
INPUT 	: Project, Active
OUTPUT 	: List of Project Task Actual Man Power
BY 		: Lakshmi
*/
function db_get_actual_manpower_list($project,$active)
{
	$get_man_power_list_squery_base = "select *,U.user_name as user_name,AU.user_name as approved_by from project_task_actual_manpower PTMP inner join users U on U.user_id= PTMP.project_task_actual_manpower_added_by inner join project_plan_process_task PPPT on PPPT.project_process_task_id = PTMP.project_task_actual_manpower_task_id inner join project_manpower_agency PMA on PMA.project_manpower_agency_id=PTMP.project_task_actual_manpower_agency inner join project_task_master PTM on PTM.project_task_master_id=PPPT.project_process_task_type inner join project_plan_process PPP on PPP.project_plan_process_id=PPPT.project_process_id inner join project_plan PP on PP.project_plan_id=PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id=PP.project_plan_project_id inner join project_process_master PPM on PPM.project_process_master_id = PPP.project_plan_process_name left outer join users AU on AU.user_id = PTMP.project_task_actual_manpower_approved_by";

	$get_man_power_list_squery_where = "";

	$filter_count = 0;

	// Data
	$get_man_power_list_sdata = array();

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where project_task_actual_manpower_active = :active";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and project_task_actual_manpower_active = :active";
		}

		// Data
		$get_man_power_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." where PP.project_plan_project_id = :project";
		}
		else
		{
			// Query
			$get_man_power_list_squery_where = $get_man_power_list_squery_where." and PP.project_plan_project_id = :project";
		}

		//Data
		$get_man_power_list_sdata['project']  = $project;

		$filter_count++;
	}

	$get_man_power_list_order_by = " order by project_task_actual_manpower_added_on ASC";
	$get_man_power_list_squery = $get_man_power_list_squery_base.$get_man_power_list_squery_where.$get_man_power_list_order_by;
	try
	{
		$dbConnection = get_conn_handle();

		$get_man_power_list_sstatement = $dbConnection->prepare($get_man_power_list_squery);

		$get_man_power_list_sstatement -> execute($get_man_power_list_sdata);

		$get_man_power_list_sdetails = $get_man_power_list_sstatement -> fetchAll();

		if(FALSE === $get_man_power_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_man_power_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_man_power_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
}

/*
PURPOSE : To get Project Task BOQ Plan List
INPUT 	: Project, Active
OUTPUT 	: List of Project Task BOQ Plan
BY 		: Soankshi
*/
function db_get_project_task_boq_plan($project,$active)
{
	$get_project_task_boq_list_squery_base = "select * from project_task_boq PTQ inner join users U on U.user_id = PTQ.project_task_boq_added_by inner join stock_unit_measure_master SUM on SUM.stock_unit_id = PTQ.project_task_boq_uom inner join project_plan_process_task PPPT on PPPT.project_process_task_id = PTQ.project_task_id inner join project_task_master PTM on PTM.project_task_master_id=PPPT.project_process_task_type inner join project_contract_process PCP on PCP.project_contract_process_id = PTQ.project_task_contract_process inner join project_contract_rate_master PCRM on PCRM.project_contract_rate_master_id=PTQ.project_task_contract_task inner join project_plan_process PPP on PPP.project_plan_process_id=PPPT.project_process_id inner join project_plan PP on PP.project_plan_id=PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id=PP.project_plan_project_id inner join project_process_master PPM on PPM.project_process_master_id = PPP.project_plan_process_name inner join project_site_location_mapping_master PSLMM on PSLMM.project_site_location_mapping_master_id = PTQ.project_task_boq_location";

	$get_project_task_boq_list_squery_where = "";
	$get_project_task_boq_list_squery_order_by = " order by PCRM.project_contract_rate_master_work_task ASC";
	$filter_count = 0;

	// Data
	$get_project_task_boq_list_sdata = array();

	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." where PP.project_plan_project_id = :project";
		}
		else
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." and PP.project_plan_project_id = :project";
		}

		// Data
		$get_project_task_boq_list_sdata[':project'] = $project;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." where project_task_boq_active = :active";
		}
		else
		{
			// Query
			$get_project_task_boq_list_squery_where = $get_project_task_boq_list_squery_where." and project_task_boq_active = :active";
		}

		// Data
		$get_project_task_boq_list_sdata[':active']  = $active;

		$filter_count++;
	}

	$get_project_task_boq_list_squery = $get_project_task_boq_list_squery_base.$get_project_task_boq_list_squery_where;
	try
	{
		$dbConnection = get_conn_handle();

		$get_project_task_boq_list_sstatement = $dbConnection->prepare($get_project_task_boq_list_squery);

		$get_project_task_boq_list_sstatement -> execute($get_project_task_boq_list_sdata);

		$get_project_task_boq_list_sdetails = $get_project_task_boq_list_sstatement -> fetchAll();

		if(FALSE === $get_project_task_boq_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_task_boq_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_task_boq_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
}
/*
PURPOSE : To get Project Task BOQ Actuals List
INPUT 	: Project,Active
OUTPUT 	: List of Project Task BOQ Actuals
BY 		: Sonakshi
*/
function db_get_task_boq_actual($project,$active)
{
	$get_project_task_actual_boq_list_squery_base = "select *,U.user_name as added_by,AU.user_name as approved_by from project_task_boq_actuals PTBA inner join users U on U.user_id = PTBA.project_task_actual_boq_added_by inner join stock_unit_measure_master SUM on SUM.stock_unit_id = PTBA.project_task_actual_boq_uom inner join project_plan_process_task PPPT on PPPT.project_process_task_id = PTBA.project_boq_actual_task_id inner join project_task_master PTM on PTM.project_task_master_id=PPPT.project_process_task_type inner join project_contract_process PCP on PCP.project_contract_process_id = PTBA.project_task_boq_actual_contract_process inner join project_contract_rate_master PCRM on PCRm.project_contract_rate_master_id=PTBA.project_task_actual_contract_task inner join project_plan_process PPP on PPP.project_plan_process_id=PPPT.project_process_id inner join project_plan PP on PP.project_plan_id=PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id=PP.project_plan_project_id inner join project_process_master PPM on PPM.project_process_master_id = PPP.project_plan_process_name inner join project_manpower_agency PMA on PMA.project_manpower_agency_id=PTBA.project_task_actual_boq_vendor_id left outer join users AU on AU.user_id=PTBA.project_task_actual_boq_approved_by inner join project_site_location_mapping_master PSLMM on PSLMM.project_site_location_mapping_master_id = PTBA.project_task_actual_boq_location";

	$get_project_task_actual_boq_list_squery_where = "";
	$filter_count = 0;

	// Data
	$get_project_task_actual_boq_list_sdata = array();

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_active = :active";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_active = :active";
		}

		// Data
		$get_project_task_actual_boq_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where PP.project_plan_project_id = :project";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and PP.project_plan_project_id = :project";
		}

		//Data
		$get_project_task_actual_boq_list_sdata['project']  = $project;

		$filter_count++;
	}

	$get_project_task_actual_boq_list_squery = $get_project_task_actual_boq_list_squery_base.$get_project_task_actual_boq_list_squery_where;
	try
	{
		$dbConnection = get_conn_handle();

		$get_project_task_actual_boq_list_sstatement = $dbConnection->prepare($get_project_task_actual_boq_list_squery);

		$get_project_task_actual_boq_list_sstatement -> execute($get_project_task_actual_boq_list_sdata);

		$get_project_task_actual_boq_list_sdetails = $get_project_task_actual_boq_list_sstatement -> fetchAll();

		if(FALSE === $get_project_task_actual_boq_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_task_actual_boq_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_task_actual_boq_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
}
/*
PURPOSE : To get Project Machine Planning list
INPUT 	: Project , Active
OUTPUT 	: List of Project Machine Planning
BY 		: Sonakshi
*/
function db_get_project_machine_plan($project,$active)
{
	$get_project_machine_planning_list_squery_base = " select * from project_machine_planning PMP inner join users U on U.user_id=PMP.project_machine_planning_added_by inner join project_machine_type_master PMTM on PMTM.project_machine_type_master_id=PMP.project_machine_planning_machine_id inner join project_plan_process_task PPPT on PPPT.project_process_task_id = PMP.project_machine_planning_task_id inner join project_task_master PTM on PTM.project_task_master_id=PPPT.project_process_task_type inner join project_plan_process PPP on PPP.project_plan_process_id=PPPT.project_process_id inner join project_plan PP on PP.project_plan_id=PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id=PP.project_plan_project_id inner join project_process_master PPM on PPM.project_process_master_id = PPP.project_plan_process_name";

	$get_project_machine_planning_list_squery_where = "";

	$filter_count = 0;

	// Data
	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." where PP.project_plan_project_id = :project_id";
		}
		else
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." and PP.project_plan_project_id = :project_id";
		}

		// Data
		$get_project_machine_planning_list_sdata[':project_id'] = $project_id;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." where project_machine_planning_active = :active";
		}
		else
		{
			// Query
			$get_project_machine_planning_list_squery_where = $get_project_machine_planning_list_squery_where." and project_machine_planning_active = :active";
		}

		// Data
		$get_project_machine_planning_list_sdata[':active']  = $active;

		$filter_count++;
	}
	$get_project_machine_planning_list_squery = $get_project_machine_planning_list_squery_base.$get_project_machine_planning_list_squery_where;
	try
	{
		$dbConnection = get_conn_handle();

		$get_project_machine_planning_list_sstatement = $dbConnection->prepare($get_project_machine_planning_list_squery);

		$get_project_machine_planning_list_sstatement -> execute($get_project_machine_planning_list_sdata);

		$get_project_machine_planning_list_sdetails = $get_project_machine_planning_list_sstatement -> fetchAll();

		if(FALSE === $get_project_machine_planning_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_machine_planning_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_machine_planning_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
}

/*
PURPOSE : To get project machine spending actuals
INPUT 	: Project, Active
OUTPUT 	: List of project macine actuals
BY 		: Soankshi
*/
function db_get_actual_machine_list($project,$active)
{
	$get_actual_machine_plan_list_squery_base = "select *,U.user_name as user_name,AU.user_name as approver from project_task_actual_machine_plan PTAMP inner join project_machine_master PMM on PMM.project_machine_master_id=PTAMP.project_task_machine_id inner join users U on U.user_id = PTAMP.project_task_actual_machine_plan_added_by inner join project_plan_process_task PPPT on PPPT.project_process_task_id = PTAMP.project_task_actual_machine_plan_task_id inner join project_task_master PTM on PTM.project_task_master_id=PPPT.project_process_task_type inner join project_plan_process PPP on PPP.project_plan_process_id=PPPT.project_process_id inner join project_plan PP on PP.project_plan_id=PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id=PP.project_plan_project_id inner join project_process_master PPM on PPM.project_process_master_id = PPP.project_plan_process_name inner join project_machine_vendor_master PMVM on PMVM.project_machine_vendor_master_id = PTAMP.project_task_actual_machine_vendor left outer join users AU on AU.user_id = PTAMP.project_task_actual_machine_plan_approved_by";

	$get_actual_machine_plan_list_squery_where = "";
	$filter_count = 0;

	// Data
	$get_actual_machine_plan_list_sdata = array();
	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where PP.project_plan_project_id = :project";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and PP.project_plan_project_id = :project";
		}

		//Data
		$get_actual_machine_plan_list_sdata['project']  = $project;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." where project_task_actual_machine_plan_active = :active";
		}
		else
		{
			// Query
			$get_actual_machine_plan_list_squery_where = $get_actual_machine_plan_list_squery_where." and project_task_actual_machine_plan_active = :active";
		}

		//Data
		$get_actual_machine_plan_list_sdata['active']  = $active;

		$filter_count++;
	}


	$get_actual_machine_plan_list_order_by = " order by project_task_actual_machine_plan_added_on DESC";
	$get_actual_machine_plan_list_squery = $get_actual_machine_plan_list_squery_base.$get_actual_machine_plan_list_squery_where.$get_actual_machine_plan_list_order_by;

	try
	{
		$dbConnection = get_conn_handle();

		$get_actual_machine_plan_list_sstatement = $dbConnection->prepare($get_actual_machine_plan_list_squery);

		$get_actual_machine_plan_list_sstatement -> execute($get_actual_machine_plan_list_sdata);

		$get_actual_machine_plan_list_sdetails = $get_actual_machine_plan_list_sstatement -> fetchAll();

		if(FALSE === $get_actual_machine_plan_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_actual_machine_plan_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_actual_machine_plan_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
}
/*
PURPOSE : To add new Project Budget
INPUT 	: Value, project ID, Remarks, Added By
OUTPUT 	: Budget ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_budget($value,$project_id,$remarks,$added_by)
{
	// Query
   $project_budget_iquery = "insert into project_budget
   (project_budget_value,project_budget_project_id,project_budget_active,project_budget_remarks,project_budget_added_by,project_budget_added_on) values (:value,:project_id,:active,:remarks,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_budget_istatement = $dbConnection->prepare($project_budget_iquery);

        // Data
        $project_budget_idata = array(':value'=>$value,':project_id'=>$project_id,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_budget_istatement->execute($project_budget_idata);
		$project_budget_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_budget_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}
/*
PURPOSE : To get Project Budget List
INPUT 	: Budget ID, Value, Project ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Budget
BY 		: Lakshmi
*/
function db_get_project_budget($project_budget_search_data)
{
	if(array_key_exists("budget_id",$project_budget_search_data))
	{
		$budget_id = $project_budget_search_data["budget_id"];
	}
	else
	{
		$budget_id= "";
	}

	if(array_key_exists("value",$project_budget_search_data))
	{
		$value = $project_budget_search_data["value"];
	}
	else
	{
		$value = "";
	}

	if(array_key_exists("project_id",$project_budget_search_data))
	{
		$project_id = $project_budget_search_data["project_id"];
	}
	else
	{
		$project_id = "";
	}

	if(array_key_exists("active",$project_budget_search_data))
	{
		$active = $project_budget_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_budget_search_data))
	{
		$added_by = $project_budget_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_budget_search_data))
	{
		$start_date= $project_budget_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_budget_search_data))
	{
		$end_date= $project_budget_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_project_budget_list_squery_base = "select * from project_budget PB inner join users U on U.user_id = PB.project_budget_added_by inner join project_management_project_master PMPM on PMPM.project_management_master_id = PB.project_budget_project_id";

	$get_project_budget_list_squery_where = "";
	$filter_count = 0;

	// Data
	$get_project_budget_list_sdata = array();

	if($budget_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_budget_list_squery_where = $get_project_budget_list_squery_where." where project_budget_id = :budget_id";
		}
		else
		{
			// Query
			$get_project_budget_list_squery_where = $get_project_budget_list_squery_where." and project_budget_id = :budget_id";
		}

		// Data
		$get_project_budget_list_sdata[':budget_id'] = $budget_id;

		$filter_count++;
	}

	if($value != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_budget_list_squery_where = $get_project_budget_list_squery_where." where project_budget_value = :value";
		}
		else
		{
			// Query
			$get_project_budget_list_squery_where = $get_project_budget_list_squery_where." and project_budget_value = :value";
		}

		// Data
		$get_project_budget_list_sdata[':value'] = $value;

		$filter_count++;
	}

	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_budget_list_squery_where = $get_project_budget_list_squery_where." where project_budget_project_id = :project_id";
		}
		else
		{
			// Query
			$get_project_budget_list_squery_where = $get_project_budget_list_squery_where." and project_budget_project_id = :project_id";
		}

		// Data
		$get_project_budget_list_sdata[':project_id'] = $project_id;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_budget_list_squery_where = $get_project_budget_list_squery_where." where project_budget_active = :active";
		}
		else
		{
			// Query
			$get_project_budget_list_squery_where = $get_project_budget_list_squery_where." and project_budget_active = :active";
		}

		// Data
		$get_project_budget_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_budget_list_squery_where = $get_project_budget_list_squery_where." where project_budget_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_budget_list_squery_where = $get_project_budget_list_squery_where." and project_budget_added_by = :added_by";
		}

		//Data
		$get_project_budget_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_budget_list_squery_where = $get_project_budget_list_squery_where." where project_budget_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_budget_list_squery_where = $get_project_budget_list_squery_where." and project_budget_added_on >= :start_date";
		}

		//Data
		$get_project_budget_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}
	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_budget_list_squery_where = $get_project_budget_list_squery_where." where project_budget_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_budget_list_squery_where = $get_project_budget_list_squery_where." and project_budget_added_on <= :end_date";
		}

		//Data
		$get_project_budget_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	$get_project_budget_list_squery = $get_project_budget_list_squery_base.$get_project_budget_list_squery_where;

	try
	{
		$dbConnection = get_conn_handle();

		$get_project_budget_list_sstatement = $dbConnection->prepare($get_project_budget_list_squery);

		$get_project_budget_list_sstatement -> execute($get_project_budget_list_sdata);

		$get_project_budget_list_sdetails = $get_project_budget_list_sstatement -> fetchAll();

		if(FALSE === $get_project_budget_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_budget_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_budget_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

/*
PURPOSE : To update Project Budget
INPUT 	: Budget ID, Project Budget Update Array
OUTPUT 	: Budget ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_budget($budget_id,$project_budget_update_data)
{
	if(array_key_exists("value",$project_budget_update_data))
	{
		$value = $project_budget_update_data["value"];
	}
	else
	{
		$value = "";
	}

	if(array_key_exists("project_id",$project_budget_update_data))
	{
		$project_id = $project_budget_update_data["project_id"];
	}
	else
	{
		$project_id = "";
	}

	if(array_key_exists("active",$project_budget_update_data))
	{
		$active = $project_budget_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_budget_update_data))
	{
		$remarks = $project_budget_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_budget_update_data))
	{
		$added_by = $project_budget_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_budget_update_data))
	{
		$added_on = $project_budget_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_budget_update_uquery_base = "update project_budget set ";

	$project_budget_update_uquery_set = "";

	$project_budget_update_uquery_where = " where project_budget_id = :budget_id";

	$project_budget_update_udata = array(":budget_id"=>$budget_id);

	$filter_count = 0;

	if($value != "")
	{
		$project_budget_update_uquery_set = $project_budget_update_uquery_set." project_budget_value = :value,";
		$project_budget_update_udata[":value"] = $value;
		$filter_count++;
	}

	if($project_id != "")
	{
		$project_budget_update_uquery_set = $project_budget_update_uquery_set." project_budget_project_id = :project_id,";
		$project_budget_update_udata[":project_id"] = $project_id;
		$filter_count++;
	}

	if($active != "")
	{
		$project_budget_update_uquery_set = $project_budget_update_uquery_set." project_budget_active = :active,";
		$project_budget_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_budget_update_uquery_set = $project_budget_update_uquery_set." project_budget_remarks = :remarks,";
		$project_budget_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_budget_update_uquery_set = $project_budget_update_uquery_set." project_budget_added_by = :added_by,";
		$project_budget_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_budget_update_uquery_set = $project_budget_update_uquery_set." project_budget_added_on = :added_on,";
		$project_budget_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_budget_update_uquery_set = trim($project_budget_update_uquery_set,',');
	}

	$project_budget_update_uquery = $project_budget_update_uquery_base.$project_budget_update_uquery_set.$project_budget_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $project_budget_update_ustatement = $dbConnection->prepare($project_budget_update_uquery);

        $project_budget_update_ustatement -> execute($project_budget_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $budget_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To get Project Task Required Item list
INPUT 	: Project, Active
OUTPUT 	: List of Project Task Required Item
BY 		: Sonakshi
*/
function db_get_project_task_material_list($project,$active)
{
	$get_project_task_required_item_list_squery_base = "select * from project_task_required_item PTRI inner join stock_material_master SMM on SMM.stock_material_id = PTRI.project_task_required_material_id inner join stock_unit_measure_master SUMM on SUMM.stock_unit_id = SMM.stock_material_unit_of_measure inner join project_plan_process_task PPPT on PPPT.project_process_task_id = PTRI.project_task_required_item_task_id inner join project_task_master PTM on PTM.project_task_master_id = PPPT.project_process_task_type inner join project_plan_process PPP on PPP.project_plan_process_id = PPPT.project_process_id inner join project_process_master PPM on PPM.project_process_master_id = PPP.project_plan_process_name inner join project_plan PP on PP.project_plan_id = PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id = PP.project_plan_project_id inner join users U on U.user_id = PTRI.project_task_required_item_added_by";

	$get_project_task_required_item_list_squery_where = "";

	$filter_count = 0;

	// Data
	$get_project_task_required_item_list_sdata = array();

	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." where PP.project_plan_project_id  = :project";
		}
		else
		{
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." and PP.project_plan_project_id  = :project";
			// Query
		}

		// Data
		$get_project_task_required_item_list_sdata[':project'] = $project;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." where project_task_required_item_active = :active";
		}
		else
		{
			// Query
			$get_project_task_required_item_list_squery_where = $get_project_task_required_item_list_squery_where." and project_task_required_item_active = :active";
		}

		// Data
		$get_project_task_required_item_list_sdata[':active'] = $active;

		$filter_count++;
	}

	$get_project_task_required_item_list_squery = $get_project_task_required_item_list_squery_base.$get_project_task_required_item_list_squery_where;
	try
	{
		$dbConnection = get_conn_handle();

		$get_project_task_required_item_list_sstatement = $dbConnection->prepare($get_project_task_required_item_list_squery);

		$get_project_task_required_item_list_sstatement -> execute($get_project_task_required_item_list_sdata);

		$get_project_task_required_item_list_sdetails = $get_project_task_required_item_list_sstatement -> fetchAll();

		if(FALSE === $get_project_task_required_item_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_task_required_item_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_task_required_item_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
}
/*
PURPOSE : To get Stock Issue Item
INPUT 	: Project,Active
OUTPUT 	: List of Stock Issue Item
BY 		: Sonakshi
*/
function db_get_stock_material_issued_list($project,$active)
{
	$get_stock_issue_item_list_squery_base = "select *,U.user_name as added_by,AU.user_name as issued_to from stock_issue_item SII inner join stock_issue SI on SI.stock_issue_id= SII.stock_issue_item_issue_id inner join users U on U.user_id=SII.stock_issue_item_issued_by inner join stock_indent SIO on SIO.stock_indent_id=SI.stock_issue_indent_id inner join stock_material_master SMM on SMM.stock_material_id=SII.stock_issue_item_material_id inner join users AU on AU.user_id = SIO.stock_indent_added_by";
	$get_stock_issue_item_list_squery_where = "";
	$filter_count = 0;
	// Data
	$get_stock_issue_item_list_sdata = array();

	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where stock_issue_item_project = :project";
		}
		else
		{
			// Query
			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and stock_issue_item_project = :project";
		}

		// Data
		$get_stock_issue_item_list_sdata[':project'] = $project;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where stock_issue_item_active = :active";
		}
		else
		{
			// Query
			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and stock_issue_item_active = :active";
		}

		// Data
		$get_stock_issue_item_list_sdata[':active']  = $active;

		$filter_count++;
	}


	$get_stock_issue_item_list_order = " order by stock_issue_item_issued_on  DESC";
	$get_stock_issue_item_list_squery = $get_stock_issue_item_list_squery_base.$get_stock_issue_item_list_squery_where.$get_stock_issue_item_list_order;

	try
	{
		$dbConnection = get_conn_handle();

		$get_stock_issue_item_list_sstatement = $dbConnection->prepare($get_stock_issue_item_list_squery);

		$get_stock_issue_item_list_sstatement -> execute($get_stock_issue_item_list_sdata);

		$get_stock_issue_item_list_sdetails = $get_stock_issue_item_list_sstatement -> fetchAll();

		if(FALSE === $get_stock_issue_item_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_stock_issue_item_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_stock_issue_item_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
}

/*
PURPOSE : To add new Project Stock Module Mapping
INPUT 	: Mgmnt Project ID, Stock Project ID, Added By
OUTPUT 	: Mapping ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_stock_module_mapping($mgmnt_project_id,$stock_project_id,$added_by)
{
	// Query
   $project_stock_module_mapping_iquery = "insert into project_stock_module_mapping
   (project_stock_module_mapping_mgmnt_project_id,project_stock_module_mapping_stock_project_id,project_stock_module_mapping_added_by,project_stock_module_mapping_added_on)
    values (:mgmnt_project_id,:stock_project_id,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_stock_module_mapping_istatement = $dbConnection->prepare($project_stock_module_mapping_iquery);

        // Data
        $project_stock_module_mapping_idata = array(':mgmnt_project_id'=>$mgmnt_project_id,':stock_project_id'=>$stock_project_id,':added_by'=>$added_by,
		':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_stock_module_mapping_istatement->execute($project_stock_module_mapping_idata);
		$project_stock_module_mapping_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_stock_module_mapping_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project Stock Module Mapping list
INPUT 	: Mapping ID, Mgmnt Project ID, Stock Project ID, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Stock Module Mapping
BY 		: Lakshmi
*/
function db_get_project_stock_module_mapping($project_stock_module_mapping_search_data)
{

	if(array_key_exists("mapping_id",$project_stock_module_mapping_search_data))
	{
		$mapping_id = $project_stock_module_mapping_search_data["mapping_id"];
	}
	else
	{
		$mapping_id= "";
	}

	if(array_key_exists("mgmnt_project_id",$project_stock_module_mapping_search_data))
	{
		$mgmnt_project_id = $project_stock_module_mapping_search_data["mgmnt_project_id"];
	}
	else
	{
		$mgmnt_project_id = "";
	}

	if(array_key_exists("stock_project_id",$project_stock_module_mapping_search_data))
	{
		$stock_project_id = $project_stock_module_mapping_search_data["stock_project_id"];
	}
	else
	{
		$stock_project_id = "";
	}

	if(array_key_exists("added_by",$project_stock_module_mapping_search_data))
	{
		$added_by = $project_stock_module_mapping_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_stock_module_mapping_search_data))
	{
		$start_date= $project_stock_module_mapping_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_stock_module_mapping_search_data))
	{
		$end_date= $project_stock_module_mapping_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_project_stock_module_mapping_list_squery_base = " select * from project_stock_module_mapping PSMM inner join users U on U.user_id = PSMM.project_stock_module_mapping_added_by inner join project_management_project_master PMPM on PMPM.project_management_master_id = PSMM.project_stock_module_mapping_mgmnt_project_id inner join stock_project SP on SP.stock_project_id = PSMM.project_stock_module_mapping_stock_project_id";

	$get_project_stock_module_mapping_list_squery_where = "";

	$filter_count = 0;

	// Data
	$get_project_stock_module_mapping_list_sdata = array();

	if($mapping_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_stock_module_mapping_list_squery_where = $get_project_stock_module_mapping_list_squery_where." where project_stock_module_mapping_id = :mapping_id";
		}
		else
		{
			// Query
			$get_project_stock_module_mapping_list_squery_where = $get_project_stock_module_mapping_list_squery_where." and project_stock_module_mapping_id = :mapping_id";
		}

		// Data
		$get_project_stock_module_mapping_list_sdata[':mapping_id'] = $mapping_id;

		$filter_count++;
	}

	if($mgmnt_project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_stock_module_mapping_list_squery_where = $get_project_stock_module_mapping_list_squery_where." where project_stock_module_mapping_mgmnt_project_id = :mgmnt_project_id";
		}
		else
		{
			// Query
			$get_project_stock_module_mapping_list_squery_where = $get_project_stock_module_mapping_list_squery_where." and project_stock_module_mapping_mgmnt_project_id = :mgmnt_project_id";
		}

		// Data
		$get_project_stock_module_mapping_list_sdata[':mgmnt_project_id'] = $mgmnt_project_id;

		$filter_count++;
	}

	if($stock_project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_stock_module_mapping_list_squery_where = $get_project_stock_module_mapping_list_squery_where." where project_stock_module_mapping_stock_project_id = :stock_project_id";
		}
		else
		{
			// Query
			$get_project_stock_module_mapping_list_squery_where = $get_project_stock_module_mapping_list_squery_where." and project_stock_module_mapping_stock_project_id = :stock_project_id";
		}

		// Data
		$get_project_stock_module_mapping_list_sdata[':stock_project_id'] = $stock_project_id;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_stock_module_mapping_list_squery_where = $get_project_stock_module_mapping_list_squery_where." where project_stock_module_mapping_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_stock_module_mapping_list_squery_where = $get_project_stock_module_mapping_list_squery_where." and project_stock_module_mapping_added_by = :added_by";
		}

		//Data
		$get_project_stock_module_mapping_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_stock_module_mapping_list_squery_where = $get_project_stock_module_mapping_list_squery_where." where project_stock_module_mapping_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_stock_module_mapping_list_squery_where = $get_project_stock_module_mapping_list_squery_where." and project_stock_module_mapping_added_on >= :start_date";
		}

		//Data
		$get_project_stock_module_mapping_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_stock_module_mapping_list_squery_where = $get_project_stock_module_mapping_list_squery_where." where project_stock_module_mapping_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_stock_module_mapping_list_squery_where = $get_project_stock_module_mapping_list_squery_where." and project_stock_module_mapping_added_on <= :end_date";
		}

		//Data
		$get_project_stock_module_mapping_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}


	$get_project_stock_module_mapping_list_squery = $get_project_stock_module_mapping_list_squery_base.$get_project_stock_module_mapping_list_squery_where;
	try
	{
		$dbConnection = get_conn_handle();

		$get_project_stock_module_mapping_list_sstatement = $dbConnection->prepare($get_project_stock_module_mapping_list_squery);

		$get_project_stock_module_mapping_list_sstatement -> execute($get_project_stock_module_mapping_list_sdata);

		$get_project_stock_module_mapping_list_sdetails = $get_project_stock_module_mapping_list_sstatement -> fetchAll();

		if(FALSE === $get_project_stock_module_mapping_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_stock_module_mapping_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_stock_module_mapping_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

/*
PURPOSE : To add new Project Finance Dashboard
INPUT 	: Project ID, Material Planned Value, Material Actual Value, Material Variance Value, Manpower Planned Value, Manpower Actual Value, Manpower Variance Value,
          Machine Planned Value, Machine Actual Value, Machine Variance Value, Contract Planned Value, Contract Actual Value, Contract Variance Value, Remarks, Added By
OUTPUT 	: Dashboard ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_finance_dashboard($project_id,$material_planned_value,$material_actual_value,$material_variance_value,$manpower_planned_value,$manpower_actual_value,$manpower_variance_value,$machine_planned_value,$machine_actual_value,$machine_variance_value,$contract_planned_value,$contract_actual_value,$contract_variance_value,$remarks,$added_by)
{
	// Query
   $project_finance_dashboard_iquery = "insert into project_finance_dashboard
   (project_finance_dashboard_project_id,project_finance_material_planned_value,project_finance_material_actual_value,project_finance_material_variance_value,
   project_finance_manpower_planned_value,project_finance_manpower_actual_value,project_finance_manpower_variance_value
   ,project_finance_machine_planned_value,project_finance_machine_actual_value,project_finance_machine_variance_value,project_finance_contract_planned_value,
   project_finance_contract_actual_value,project_finance_contract_variance_value,project_finance_dashboard_active,project_finance_dashboard_remarks,
   project_finance_dashboard_added_by,project_finance_dashboard_added_on) values (:project_id,:material_planned_value,:material_actual_value,:material_variance_value,:manpower_planned_value,:manpower_actual_value,:manpower_variance_value,
   :machine_planned_value,:machine_actual_value,:machine_variance_value,:contract_planned_value,:contract_actual_value,:contract_variance_value,:active,:remarks,
   :added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_finance_dashboard_istatement = $dbConnection->prepare($project_finance_dashboard_iquery);

        // Data
        $project_finance_dashboard_idata = array(':project_id'=>$project_id,':material_planned_value'=>$material_planned_value,':material_actual_value'=>$material_actual_value,':material_variance_value'=>$material_variance_value,':manpower_planned_value'=>$manpower_planned_value,':manpower_actual_value'=>$manpower_actual_value,':manpower_variance_value'=>$manpower_variance_value,':machine_planned_value'=>$machine_planned_value,':machine_actual_value'=>$machine_actual_value,':machine_variance_value'=>$machine_variance_value,':contract_planned_value'=>$contract_planned_value,':contract_actual_value'=>$contract_actual_value,':contract_variance_value'=>$contract_variance_value,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_finance_dashboard_istatement->execute($project_finance_dashboard_idata);
		$project_finance_dashboard_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_finance_dashboard_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project Finance Dashboard List
INPUT 	: Dashboard ID, Project ID, Manpower Planned Value, Manpower Actual Value, Manpower Variance Value, Machine Planned Value, Machine Actual Value, Machine Variance Value,
          Contract Planned Value, Contract Actual Value, Contract Variance Value, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Finance Dashboard
BY 		: Lakshmi
*/
function db_get_project_finance_dashboard($project_finance_dashboard_search_data)
{
	if(array_key_exists("dashboard_id",$project_finance_dashboard_search_data))
	{
		$dashboard_id = $project_finance_dashboard_search_data["dashboard_id"];
	}
	else
	{
		$dashboard_id= "";
	}

	if(array_key_exists("project_id",$project_finance_dashboard_search_data))
	{
		$project_id = $project_finance_dashboard_search_data["project_id"];
	}
	else
	{
		$project_id = "";
	}

	if(array_key_exists("material_planned_value",$project_finance_dashboard_search_data))
	{
		$material_planned_value = $project_finance_dashboard_search_data["material_planned_value"];
	}
	else
	{
		$material_planned_value = "";
	}

	if(array_key_exists("material_actual_value",$project_finance_dashboard_search_data))
	{
		$material_actual_value = $project_finance_dashboard_search_data["material_actual_value"];
	}
	else
	{
		$material_actual_value = "";
	}

	if(array_key_exists("material_variance_value",$project_finance_dashboard_search_data))
	{
		$material_variance_value = $project_finance_dashboard_search_data["material_variance_value"];
	}
	else
	{
		$material_variance_value = "";
	}

	if(array_key_exists("manpower_planned_value",$project_finance_dashboard_search_data))
	{
		$manpower_planned_value = $project_finance_dashboard_search_data["manpower_planned_value"];
	}
	else
	{
		$manpower_planned_value = "";
	}

	if(array_key_exists("manpower_actual_value",$project_finance_dashboard_search_data))
	{
		$manpower_actual_value = $project_finance_dashboard_search_data["manpower_actual_value"];
	}
	else
	{
		$manpower_actual_value = "";
	}

	if(array_key_exists("manpower_variance_value",$project_finance_dashboard_search_data))
	{
		$manpower_variance_value = $project_finance_dashboard_search_data["manpower_variance_value"];
	}
	else
	{
		$manpower_variance_value = "";
	}

	if(array_key_exists("machine_planned_value",$project_finance_dashboard_search_data))
	{
		$machine_planned_value = $project_finance_dashboard_search_data["machine_planned_value"];
	}
	else
	{
		$machine_planned_value = "";
	}

	if(array_key_exists("machine_actual_value",$project_finance_dashboard_search_data))
	{
		$machine_actual_value = $project_finance_dashboard_search_data["machine_actual_value"];
	}
	else
	{
		$machine_actual_value = "";
	}

	if(array_key_exists("machine_variance_value",$project_finance_dashboard_search_data))
	{
		$machine_variance_value = $project_finance_dashboard_search_data["machine_variance_value"];
	}
	else
	{
		$machine_variance_value = "";
	}

	if(array_key_exists("contract_planned_value",$project_finance_dashboard_search_data))
	{
		$contract_planned_value = $project_finance_dashboard_search_data["contract_planned_value"];
	}
	else
	{
		$contract_planned_value = "";
	}

	if(array_key_exists("contract_actual_value",$project_finance_dashboard_search_data))
	{
		$contract_actual_value = $project_finance_dashboard_search_data["contract_actual_value"];
	}
	else
	{
		$contract_actual_value = "";
	}

	if(array_key_exists("contract_variance_value",$project_finance_dashboard_search_data))
	{
		$contract_variance_value = $project_finance_dashboard_search_data["contract_variance_value"];
	}
	else
	{
		$contract_variance_value = "";
	}

	if(array_key_exists("active",$project_finance_dashboard_search_data))
	{
		$active = $project_finance_dashboard_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_finance_dashboard_search_data))
	{
		$added_by = $project_finance_dashboard_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_finance_dashboard_search_data))
	{
		$start_date= $project_finance_dashboard_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_finance_dashboard_search_data))
	{
		$end_date= $project_finance_dashboard_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_project_finance_dashboard_list_squery_base = "select * from project_finance_dashboard PD";

	$get_project_finance_dashboard_list_squery_where = "";
	$filter_count = 0;

	// Data
	$get_project_finance_dashboard_list_sdata = array();

	if($dashboard_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." where project_finance_dashboard_id = :dashboard_id";
		}
		else
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." and project_finance_dashboard_id = :dashboard_id";
		}

		// Data
		$get_project_finance_dashboard_list_sdata[':dashboard_id'] = $dashboard_id;

		$filter_count++;
	}

	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." where project_finance_dashboard_project_id = :project_id";
		}
		else
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." and project_finance_dashboard_project_id = :project_id";
		}

		// Data
		$get_project_finance_dashboard_list_sdata[':project_id'] = $project_id;

		$filter_count++;
	}

	if($manpower_planned_value != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." where project_finance_manpower_planned_value = :manpower_planned_value";
		}
		else
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." and project_finance_manpower_planned_value = :manpower_planned_value";
		}

		// Data
		$get_project_finance_dashboard_list_sdata[':manpower_planned_value'] = $manpower_planned_value;

		$filter_count++;
	}

	if($manpower_actual_value != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." where project_finance_manpower_actual_value = :manpower_actual_value";
		}
		else
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." and project_finance_manpower_actual_value = :manpower_actual_value";
		}

		// Data
		$get_project_finance_dashboard_list_sdata[':manpower_actual_value'] = $manpower_actual_value;

		$filter_count++;
	}

	if($manpower_variance_value != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." where project_finance_manpower_variance_value = :manpower_variance_value";
		}
		else
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." and project_finance_manpower_variance_value = :manpower_variance_value";
		}

		// Data
		$get_project_finance_dashboard_list_sdata[':manpower_variance_value'] = $manpower_variance_value;

		$filter_count++;
	}

	if($material_planned_value != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." where project_finance_material_planned_value = :material_planned_value";
		}
		else
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." and project_finance_material_planned_value = :material_planned_value";
		}

		// Data
		$get_project_finance_dashboard_list_sdata[':material_planned_value'] = $material_planned_value;

		$filter_count++;
	}

	if($material_actual_value != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." where project_finance_material_actual_value = :material_actual_value";
		}
		else
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." and project_finance_material_actual_value = :material_actual_value";
		}

		// Data
		$get_project_finance_dashboard_list_sdata[':material_actual_value'] = $material_actual_value;

		$filter_count++;
	}

	if($material_variance_value != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." where project_finance_material_variance_value = :material_variance_value";
		}
		else
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." and project_finance_material_variance_value = :material_variance_value";
		}

		// Data
		$get_project_finance_dashboard_list_sdata[':material_variance_value'] = $material_variance_value;

		$filter_count++;
	}

	if($machine_planned_value != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." where project_finance_machine_planned_value = :machine_planned_value";
		}
		else
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." and project_finance_machine_planned_value = :machine_planned_value";
		}

		// Data
		$get_project_finance_dashboard_list_sdata[':machine_planned_value'] = $machine_planned_value;

		$filter_count++;
	}

	if($machine_actual_value != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." where project_finance_machine_actual_value = :machine_actual_value";
		}
		else
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." and project_finance_machine_actual_value = :machine_actual_value";
		}

		// Data
		$get_project_finance_dashboard_list_sdata[':machine_actual_value'] = $machine_actual_value;

		$filter_count++;
	}

	if($machine_variance_value != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." where project_finance_machine_variance_value = :machine_variance_value";
		}
		else
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." and project_finance_machine_variance_value = :machine_variance_value";
		}

		// Data
		$get_project_finance_dashboard_list_sdata[':machine_variance_value'] = $machine_variance_value;

		$filter_count++;
	}

	if($contract_planned_value != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." where project_finance_contract_planned_value = :contract_planned_value";
		}
		else
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." and project_finance_contract_planned_value = :contract_planned_value";
		}

		// Data
		$get_project_finance_dashboard_list_sdata[':contract_planned_value'] = $contract_planned_value;

		$filter_count++;
	}

	if($contract_actual_value != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." where project_finance_contract_actual_value = :contract_actual_value";
		}
		else
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." and project_finance_contract_actual_value = :contract_actual_value";
		}

		// Data
		$get_project_finance_dashboard_list_sdata[':contract_actual_value'] = $contract_actual_value;

		$filter_count++;
	}

	if($contract_variance_value != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." where project_finance_contract_variance_value = :contract_variance_value";
		}
		else
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." and project_finance_contract_variance_value = :contract_variance_value";
		}

		// Data
		$get_project_finance_dashboard_list_sdata[':contract_variance_value'] = $contract_variance_value;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." where project_finance_dashboard_active = :active";
		}
		else
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." and project_finance_dashboard_active = :active";
		}

		// Data
		$get_project_finance_dashboard_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." where project_finance_dashboard_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." and project_finance_dashboard_added_by = :added_by";
		}

		//Data
		$get_project_finance_dashboard_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." where project_finance_dashboard_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." and project_finance_dashboard_added_on >= :start_date";
		}

		//Data
		$get_project_finance_dashboard_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." where project_finance_dashboard_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_finance_dashboard_list_squery_where = $get_project_finance_dashboard_list_squery_where." and project_finance_dashboard_added_on <= :end_date";
		}

		//Data
		$get_project_finance_dashboard_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	$get_project_finance_dashboard_list_squery = $get_project_finance_dashboard_list_squery_base.$get_project_finance_dashboard_list_squery_where;

	try
	{
		$dbConnection = get_conn_handle();

		$get_project_finance_dashboard_list_sstatement = $dbConnection->prepare($get_project_finance_dashboard_list_squery);

		$get_project_finance_dashboard_list_sstatement -> execute($get_project_finance_dashboard_list_sdata);

		$get_project_finance_dashboard_list_sdetails = $get_project_finance_dashboard_list_sstatement -> fetchAll();

		if(FALSE === $get_project_finance_dashboard_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_finance_dashboard_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_finance_dashboard_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

/*
PURPOSE : To update Project Finance Dashboard
INPUT 	: Dashboard ID, Project Finance Dashboard Update Array
OUTPUT 	: Dashboard ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_finance_dashboard($dashboard_id,$project_finance_dashboard_update_data)
{
	if(array_key_exists("project_id",$project_finance_dashboard_update_data))
	{
		$project_id = $project_finance_dashboard_update_data["project_id"];
	}
	else
	{
		$project_id = "";
	}

	if(array_key_exists("material_planned_value",$project_finance_dashboard_update_data))
	{
		$material_planned_value = $project_finance_dashboard_update_data["material_planned_value"];
	}
	else
	{
		$material_planned_value = "";
	}

	if(array_key_exists("material_actual_value",$project_finance_dashboard_update_data))
	{
		$material_actual_value = $project_finance_dashboard_update_data["material_actual_value"];
	}
	else
	{
		$material_actual_value = "";
	}

	if(array_key_exists("material_variance_value",$project_finance_dashboard_update_data))
	{
		$material_variance_value = $project_finance_dashboard_update_data["material_variance_value"];
	}
	else
	{
		$material_variance_value = "";
	}

	if(array_key_exists("manpower_planned_value",$project_finance_dashboard_update_data))
	{
		$manpower_planned_value = $project_finance_dashboard_update_data["manpower_planned_value"];
	}
	else
	{
		$manpower_planned_value = "";
	}

	if(array_key_exists("manpower_actual_value",$project_finance_dashboard_update_data))
	{
		$manpower_actual_value = $project_finance_dashboard_update_data["manpower_actual_value"];
	}
	else
	{
		$manpower_actual_value = "";
	}

	if(array_key_exists("manpower_variance_value",$project_finance_dashboard_update_data))
	{
		$manpower_variance_value = $project_finance_dashboard_update_data["manpower_variance_value"];
	}
	else
	{
		$manpower_variance_value = "";
	}

	if(array_key_exists("machine_planned_value",$project_finance_dashboard_update_data))
	{
		$machine_planned_value = $project_finance_dashboard_update_data["machine_planned_value"];
	}
	else
	{
		$machine_planned_value = "";
	}

	if(array_key_exists("machine_actual_value",$project_finance_dashboard_update_data))
	{
		$machine_actual_value = $project_finance_dashboard_update_data["machine_actual_value"];
	}
	else
	{
		$machine_actual_value = "";
	}

	if(array_key_exists("machine_variance_value",$project_finance_dashboard_update_data))
	{
		$machine_variance_value = $project_finance_dashboard_update_data["machine_variance_value"];
	}
	else
	{
		$machine_variance_value = "";
	}

	if(array_key_exists("contract_planned_value",$project_finance_dashboard_update_data))
	{
		$contract_planned_value = $project_finance_dashboard_update_data["contract_planned_value"];
	}
	else
	{
		$contract_planned_value = "";
	}

	if(array_key_exists("contract_actual_value",$project_finance_dashboard_update_data))
	{
		$contract_actual_value = $project_finance_dashboard_update_data["contract_actual_value"];
	}
	else
	{
		$contract_actual_value = "";
	}

	if(array_key_exists("contract_variance_value",$project_finance_dashboard_update_data))
	{
		$contract_variance_value = $project_finance_dashboard_update_data["contract_variance_value"];
	}
	else
	{
		$contract_variance_value = "";
	}

	if(array_key_exists("active",$project_finance_dashboard_update_data))
	{
		$active = $project_finance_dashboard_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$project_finance_dashboard_update_data))
	{
		$remarks = $project_finance_dashboard_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_finance_dashboard_update_data))
	{
		$added_by = $project_finance_dashboard_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_finance_dashboard_update_data))
	{
		$added_on = $project_finance_dashboard_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_finance_dashboard_update_uquery_base = "update project_finance_dashboard set ";

	$project_finance_dashboard_update_uquery_set = "";

	$project_finance_dashboard_update_uquery_where = " where project_finance_dashboard_id = :dashboard_id";

	$project_finance_dashboard_update_udata = array(":dashboard_id"=>$dashboard_id);

	$filter_count = 0;

	if($project_id != "")
	{
		$project_finance_dashboard_update_uquery_set = $project_finance_dashboard_update_uquery_set." project_finance_dashboard_project_id = :project_id,";
		$project_finance_dashboard_update_udata[":project_id"] = $project_id;
		$filter_count++;
	}

	if($material_planned_value != "")
	{
		$project_finance_dashboard_update_uquery_set = $project_finance_dashboard_update_uquery_set." project_finance_material_planned_value = :material_planned_value,";
		$project_finance_dashboard_update_udata[":material_planned_value"] = $material_planned_value;
		$filter_count++;
	}

	if($material_actual_value != "")
	{
		$project_finance_dashboard_update_uquery_set = $project_finance_dashboard_update_uquery_set." project_finance_material_actual_value = :material_actual_value,";
		$project_finance_dashboard_update_udata[":material_actual_value"] = $material_actual_value;
		$filter_count++;
	}

	if($material_variance_value != "")
	{
		$project_finance_dashboard_update_uquery_set = $project_finance_dashboard_update_uquery_set." project_finance_material_variance_value = :material_variance_value,";
		$project_finance_dashboard_update_udata[":material_variance_value"] = $material_variance_value;
		$filter_count++;
	}

	if($manpower_planned_value != "")
	{
		$project_finance_dashboard_update_uquery_set = $project_finance_dashboard_update_uquery_set." project_finance_manpower_planned_value = :manpower_planned_value,";
		$project_finance_dashboard_update_udata[":manpower_planned_value"] = $manpower_planned_value;
		$filter_count++;
	}

	if($manpower_actual_value != "")
	{
		$project_finance_dashboard_update_uquery_set = $project_finance_dashboard_update_uquery_set." project_finance_manpower_actual_value = :manpower_actual_value,";
		$project_finance_dashboard_update_udata[":manpower_actual_value"] = $manpower_actual_value;
		$filter_count++;
	}

	if($manpower_variance_value != "")
	{
		$project_finance_dashboard_update_uquery_set = $project_finance_dashboard_update_uquery_set." project_finance_manpower_variance_value = :manpower_variance_value,";
		$project_finance_dashboard_update_udata[":manpower_variance_value"] = $manpower_variance_value;
		$filter_count++;
	}

	if($machine_planned_value != "")
	{
		$project_finance_dashboard_update_uquery_set = $project_finance_dashboard_update_uquery_set." project_finance_machine_planned_value = :machine_planned_value,";
		$project_finance_dashboard_update_udata[":machine_planned_value"] = $machine_planned_value;
		$filter_count++;
	}

	if($machine_actual_value != "")
	{
		$project_finance_dashboard_update_uquery_set = $project_finance_dashboard_update_uquery_set." project_finance_machine_actual_value = :machine_actual_value,";
		$project_finance_dashboard_update_udata[":machine_actual_value"] = $machine_actual_value;
		$filter_count++;
	}

	if($machine_variance_value != "")
	{
		$project_finance_dashboard_update_uquery_set = $project_finance_dashboard_update_uquery_set." project_finance_machine_variance_value = :machine_variance_value,";
		$project_finance_dashboard_update_udata[":machine_variance_value"] = $machine_variance_value;
		$filter_count++;
	}

	if($contract_planned_value != "")
	{
		$project_finance_dashboard_update_uquery_set = $project_finance_dashboard_update_uquery_set." project_finance_contract_planned_value = :contract_planned_value,";
		$project_finance_dashboard_update_udata[":contract_planned_value"] = $contract_planned_value;
		$filter_count++;
	}

	if($contract_actual_value != "")
	{
		$project_finance_dashboard_update_uquery_set = $project_finance_dashboard_update_uquery_set." project_finance_contract_actual_value = :contract_actual_value,";
		$project_finance_dashboard_update_udata[":contract_actual_value"] = $contract_actual_value;
		$filter_count++;
	}

	if($contract_variance_value != "")
	{
		$project_finance_dashboard_update_uquery_set = $project_finance_dashboard_update_uquery_set." project_finance_contract_variance_value = :contract_variance_value,";
		$project_finance_dashboard_update_udata[":contract_variance_value"] = $contract_variance_value;
		$filter_count++;
	}

	if($active != "")
	{
		$project_finance_dashboard_update_uquery_set = $project_finance_dashboard_update_uquery_set." project_finance_dashboard_active = :active,";
		$project_finance_dashboard_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_finance_dashboard_update_uquery_set = $project_finance_dashboard_update_uquery_set." project_finance_dashboard_remarks = :remarks,";
		$project_finance_dashboard_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_finance_dashboard_update_uquery_set = $project_finance_dashboard_update_uquery_set." project_finance_dashboard_added_by = :added_by,";
		$project_finance_dashboard_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_finance_dashboard_update_uquery_set = $project_finance_dashboard_update_uquery_set." project_finance_dashboard_added_on = :added_on,";
		$project_finance_dashboard_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_finance_dashboard_update_uquery_set = trim($project_finance_dashboard_update_uquery_set,',');
	}

	$project_finance_dashboard_update_uquery = $project_finance_dashboard_update_uquery_base.$project_finance_dashboard_update_uquery_set.$project_finance_dashboard_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();

        $project_finance_dashboard_update_ustatement = $dbConnection->prepare($project_finance_dashboard_update_uquery);

        $project_finance_dashboard_update_ustatement -> execute($project_finance_dashboard_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $dashboard_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Plan Cancelation
INPUT 	: Task ID, Plan Type, Active, Remarks, Added By
OUTPUT 	: Cancelation ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_plan_cancelation($task_id,$plan_type,$remarks,$added_by)
{
	// Query
   $project_plan_cancelation_iquery = "insert into project_plan_cancelation
   (project_plan_cancelation_task_id,project_plan_cancelation_plan_type,project_plan_cancelation_active,project_plan_cancelation_remarks,project_plan_cancelation_added_by,
   project_plan_cancelation_added_on) values (:task_id,:plan_type,:active,:remarks,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_plan_cancelation_istatement = $dbConnection->prepare($project_plan_cancelation_iquery);

        // Data
        $project_plan_cancelation_idata = array(':task_id'=>$task_id,':plan_type'=>$plan_type,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,
		':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_plan_cancelation_istatement->execute($project_plan_cancelation_idata);
		$project_plan_cancelation_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_plan_cancelation_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project Plan Cancelation List
INPUT 	: Cancelation ID, Task ID, Plan type, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Plan Cancelation
BY 		: Lakshmi
*/
function db_get_project_plan_cancelation($project_plan_cancelation_search_data)
{
	if(array_key_exists("cancelation_id",$project_plan_cancelation_search_data))
	{
		$cancelation_id = $project_plan_cancelation_search_data["cancelation_id"];
	}
	else
	{
		$cancelation_id= "";
	}

	if(array_key_exists("task_id",$project_plan_cancelation_search_data))
	{
		$task_id = $project_plan_cancelation_search_data["task_id"];
	}
	else
	{
		$task_id = "";
	}

	if(array_key_exists("project",$project_plan_cancelation_search_data))
	{
		$project = $project_plan_cancelation_search_data["project"];
	}
	else
	{
		$project = "";
	}

	if(array_key_exists("process",$project_plan_cancelation_search_data))
	{
		$process = $project_plan_cancelation_search_data["process"];
	}
	else
	{
		$process = "";
	}

	if(array_key_exists("task",$project_plan_cancelation_search_data))
	{
		$task = $project_plan_cancelation_search_data["task"];
	}
	else
	{
		$task = "";
	}

	if(array_key_exists("plan_type",$project_plan_cancelation_search_data))
	{
		$plan_type = $project_plan_cancelation_search_data["plan_type"];
	}
	else
	{
		$plan_type = "";
	}

	if(array_key_exists("active",$project_plan_cancelation_search_data))
	{
		$active = $project_plan_cancelation_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_plan_cancelation_search_data))
	{
		$added_by = $project_plan_cancelation_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_plan_cancelation_search_data))
	{
		$start_date= $project_plan_cancelation_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_plan_cancelation_search_data))
	{
		$end_date= $project_plan_cancelation_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_project_plan_cancelation_list_squery_base = "select * from project_plan_cancelation PC inner join users U on U.user_id = PC.project_plan_cancelation_added_by inner join project_plan_process_task PPPT on PPPT.project_process_task_id = PC.project_plan_cancelation_task_id inner join project_task_master PTM on PTM.project_task_master_id = PPPT.project_process_task_type inner join project_plan_process PPP on PPP.project_plan_process_id = PPPT.project_process_id inner join project_process_master PPM on PPM.project_process_master_id = PPP.project_plan_process_name inner join project_plan PP on PP.project_plan_id = PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id = PP.project_plan_project_id";

	$get_project_plan_cancelation_list_squery_where = "";
	$filter_count = 0;

	// Data
	$get_project_plan_cancelation_list_sdata = array();

	if($cancelation_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_cancelation_list_squery_where = $get_project_plan_cancelation_list_squery_where." where project_plan_cancelation_id = :cancelation_id";
		}
		else
		{
			// Query
			$get_project_plan_cancelation_list_squery_where = $get_project_plan_cancelation_list_squery_where." and project_plan_cancelation_id = :cancelation_id";
		}

		// Data
		$get_project_plan_cancelation_list_sdata[':cancelation_id'] = $cancelation_id;

		$filter_count++;
	}

	if($task_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_cancelation_list_squery_where = $get_project_plan_cancelation_list_squery_where." where project_plan_cancelation_task_id = :task_id";
		}
		else
		{
			// Query
			$get_project_plan_cancelation_list_squery_where = $get_project_plan_cancelation_list_squery_where." and project_plan_cancelation_task_id = :task_id";
		}

		// Data
		$get_project_plan_cancelation_list_sdata[':task_id'] = $task_id;

		$filter_count++;
	}

	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_cancelation_list_squery_where = $get_project_plan_cancelation_list_squery_where." where PMPM.project_management_master_id = :project";
		}
		else
		{
			// Query
			$get_project_plan_cancelation_list_squery_where = $get_project_plan_cancelation_list_squery_where." and PMPM.project_management_master_id = :project";
		}

		// Data
		$get_project_plan_cancelation_list_sdata[':project'] = $project;

		$filter_count++;
	}

	if($process != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_cancelation_list_squery_where = $get_project_plan_cancelation_list_squery_where." where PPM.project_process_master_id = :process";
		}
		else
		{
			// Query
			$get_project_plan_cancelation_list_squery_where = $get_project_plan_cancelation_list_squery_where." and PPM.project_process_master_id = :process";
		}

		// Data
		$get_project_plan_cancelation_list_sdata[':process'] = $process;

		$filter_count++;
	}

	if($task != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_cancelation_list_squery_where = $get_project_plan_cancelation_list_squery_where." where PTM.project_task_master_id = :task";
		}
		else
		{
			// Query
			$get_project_plan_cancelation_list_squery_where = $get_project_plan_cancelation_list_squery_where." and PTM.project_task_master_id = :task";
		}

		// Data
		$get_project_plan_cancelation_list_sdata[':task'] = $task;

		$filter_count++;
	}

	if($plan_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_cancelation_list_squery_where = $get_project_plan_cancelation_list_squery_where." where project_plan_cancelation_plan_type = :plan_type";
		}
		else
		{
			// Query
			$get_project_plan_cancelation_list_squery_where = $get_project_plan_cancelation_list_squery_where." and project_plan_cancelation_plan_type = :plan_type";
		}

		// Data
		$get_project_plan_cancelation_list_sdata[':plan_type'] = $plan_type;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_cancelation_list_squery_where = $get_project_plan_cancelation_list_squery_where." where project_plan_cancelation_active = :active";
		}
		else
		{
			// Query
			$get_project_plan_cancelation_list_squery_where = $get_project_plan_cancelation_list_squery_where." and project_plan_cancelation_active = :active";
		}

		// Data
		$get_project_plan_cancelation_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_cancelation_list_squery_where = $get_project_plan_cancelation_list_squery_where." where project_plan_cancelation_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_plan_cancelation_list_squery_where = $get_project_plan_cancelation_list_squery_where." and project_plan_cancelation_added_by = :added_by";
		}

		//Data
		$get_project_plan_cancelation_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_cancelation_list_squery_where = $get_project_plan_cancelation_list_squery_where." where project_plan_cancelation_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_plan_cancelation_list_squery_where = $get_project_plan_cancelation_list_squery_where." and project_plan_cancelation_added_on >= :start_date";
		}

		//Data
		$get_project_plan_cancelation_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_cancelation_list_squery_where = $get_project_plan_cancelation_list_squery_where." where project_plan_cancelation_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_plan_cancelation_list_squery_where = $get_project_plan_cancelation_list_squery_where." and project_plan_cancelation_added_on <= :end_date";
		}

		//Data
		$get_project_plan_cancelation_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	$get_project_plan_cancelation_list_squery = $get_project_plan_cancelation_list_squery_base.$get_project_plan_cancelation_list_squery_where;

	try
	{
		$dbConnection = get_conn_handle();

		$get_project_plan_cancelation_list_sstatement = $dbConnection->prepare($get_project_plan_cancelation_list_squery);

		$get_project_plan_cancelation_list_sstatement -> execute($get_project_plan_cancelation_list_sdata);

		$get_project_plan_cancelation_list_sdetails = $get_project_plan_cancelation_list_sstatement -> fetchAll();

		if(FALSE === $get_project_plan_cancelation_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_plan_cancelation_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_plan_cancelation_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

/*
PURPOSE : To update Project Plan Cancelation
INPUT 	: Cancelation ID, Project Plan Cancelation Update Array
OUTPUT 	: Cancelation ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_plan_cancelation($cancelation_id,$project_plan_cancelation_update_data)
{
	if(array_key_exists("task_id",$project_plan_cancelation_update_data))
	{
		$task_id = $project_plan_cancelation_update_data["task_id"];
	}
	else
	{
		$task_id = "";
	}

	if(array_key_exists("plan_type",$project_plan_cancelation_update_data))
	{
		$plan_type = $project_plan_cancelation_update_data["plan_type"];
	}
	else
	{
		$plan_type = "";
	}

	if(array_key_exists("active",$project_plan_cancelation_update_data))
	{
		$active = $project_plan_cancelation_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$project_plan_cancelation_update_data))
	{
		$remarks = $project_plan_cancelation_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_plan_cancelation_update_data))
	{
		$added_by = $project_plan_cancelation_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_plan_cancelation_update_data))
	{
		$added_on = $project_plan_cancelation_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_plan_cancelation_update_uquery_base = "update project_plan_cancelation set ";

	$project_plan_cancelation_update_uquery_set = "";

	$project_plan_cancelation_update_uquery_where = " where project_plan_cancelation_id = :cancelation_id";

	$project_plan_cancelation_update_udata = array(":cancelation_id"=>$cancelation_id);

	$filter_count = 0;

	if($task_id != "")
	{
		$project_plan_cancelation_update_uquery_set = $project_plan_cancelation_update_uquery_set." project_plan_cancelation_task_id = :task_id,";
		$project_plan_cancelation_update_udata[":task_id"] = $task_id;
		$filter_count++;
	}

	if($plan_type != "")
	{
		$project_plan_cancelation_update_uquery_set = $project_plan_cancelation_update_uquery_set." project_plan_cancelation_plan_type = :plan_type,";
		$project_plan_cancelation_update_udata[":plan_type"] = $plan_type;
		$filter_count++;
	}

	if($active != "")
	{
		$project_plan_cancelation_update_uquery_set = $project_plan_cancelation_update_uquery_set." project_plan_cancelation_active = :active,";
		$project_plan_cancelation_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_plan_cancelation_update_uquery_set = $project_plan_cancelation_update_uquery_set." project_plan_cancelation_remarks = :remarks,";
		$project_plan_cancelation_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_plan_cancelation_update_uquery_set = $project_plan_cancelation_update_uquery_set." project_plan_cancelation_added_by = :added_by,";
		$project_plan_cancelation_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_plan_cancelation_update_uquery_set = $project_plan_cancelation_update_uquery_set." project_plan_cancelation_added_on = :added_on,";
		$project_plan_cancelation_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_plan_cancelation_update_uquery_set = trim($project_plan_cancelation_update_uquery_set,',');
	}

	$project_plan_cancelation_update_uquery = $project_plan_cancelation_update_uquery_base.$project_plan_cancelation_update_uquery_set.$project_plan_cancelation_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();

        $project_plan_cancelation_update_ustatement = $dbConnection->prepare($project_plan_cancelation_update_uquery);

        $project_plan_cancelation_update_ustatement -> execute($project_plan_cancelation_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $cancelation_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}

/*
PURPOSE : To get Project Task BOQ  Sum List
INPUT 	: BOQ ID, UOM, Number, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Task BOQ
BY 		: Lakshmi
*/
function db_get_actual_boq_sum($project_task_actual_boq_search_data)
{

	if(array_key_exists("task_id",$project_task_actual_boq_search_data))
	{
		$task_id = $project_task_actual_boq_search_data["task_id"];
	}
	else
	{
		$task_id = "";
	}


	if(array_key_exists("active",$project_task_actual_boq_search_data))
	{
		$active = $project_task_actual_boq_search_data["active"];
	}
	else
	{
		$active = "";
	}


	$get_project_task_actual_boq_list_squery_base = "select * ,sum(project_task_actual_boq_total_measurement) as total_msmt,U.user_name as added_by,AU.user_name as approved_by from project_task_boq_actuals PTBA inner join users U on U.user_id = PTBA.project_task_actual_boq_added_by inner join stock_unit_measure_master SUM on SUM.stock_unit_id = PTBA.project_task_actual_boq_uom inner join project_plan_process_task PPPT on PPPT.project_process_task_id = PTBA.project_boq_actual_task_id inner join project_task_master PTM on PTM.project_task_master_id=PPPT.project_process_task_type inner join project_contract_process PCP on PCP.project_contract_process_id = PTBA.project_task_boq_actual_contract_process inner join project_contract_rate_master PCRM on PCRM.project_contract_rate_master_id=PTBA.project_task_actual_contract_task inner join project_plan_process PPP on PPP.project_plan_process_id=PPPT.project_process_id inner join project_plan PP on PP.project_plan_id=PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id=PP.project_plan_project_id inner join project_process_master PPM on PPM.project_process_master_id = PPP.project_plan_process_name inner join project_manpower_agency PMA on PMA.project_manpower_agency_id=PTBA.project_task_actual_boq_vendor_id left outer join users AU on AU.user_id=PTBA.project_task_actual_boq_approved_by inner join project_site_location_mapping_master PSLMM on PSLMM.project_site_location_mapping_master_id = PTBA.project_task_actual_boq_location";

	$get_project_task_actual_boq_list_squery_where = "";
	$filter_count = 0;

	// Data
	$get_project_task_actual_boq_list_sdata = array();



	if($task_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_boq_actual_task_id = :task_id";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_boq_actual_task_id = :task_id";
		}

		// Data
		$get_project_task_actual_boq_list_sdata[':task_id'] = $task_id;

		$filter_count++;
	}



	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." where project_task_actual_boq_active = :active";
		}
		else
		{
			// Query
			$get_project_task_actual_boq_list_squery_where = $get_project_task_actual_boq_list_squery_where." and project_task_actual_boq_active = :active";
		}

		// Data
		$get_project_task_actual_boq_list_sdata[':active']  = $active;

		$filter_count++;
	}

	$get_project_task_actual_boq_list_order_by = " order by project_task_actual_boq_added_on DESC";
	$get_project_task_actual_boq_list_squery = $get_project_task_actual_boq_list_squery_base.$get_project_task_actual_boq_list_squery_where.$get_project_task_actual_boq_list_order_by;

	try
	{
		$dbConnection = get_conn_handle();

		$get_project_task_actual_boq_list_sstatement = $dbConnection->prepare($get_project_task_actual_boq_list_squery);

		$get_project_task_actual_boq_list_sstatement -> execute($get_project_task_actual_boq_list_sdata);

		$get_project_task_actual_boq_list_sdetails = $get_project_task_actual_boq_list_sstatement -> fetchAll();

		if(FALSE === $get_project_task_actual_boq_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_task_actual_boq_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_task_actual_boq_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }
/*
PURPOSE : To add new Project Manpower Rework
INPUT 	: Task ID, Date, Men, Women, Completion Percent, Checked By, Checked On, Approved By, Approved On, Remarks, Added By
OUTPUT 	: Rework ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_manpower_rework($task_id,$date,$men,$women,$mason,$vendor_id,$completion_percent,$checked_by,$checked_on,$approved_by,$approved_on,$remarks,$added_by)
{
	// Query
   $project_manpower_rework_iquery = "insert into project_manpower_rework
   (project_manpower_rework_task_id,project_manpower_rework_date,project_manpower_rework_no_of_men,project_manpower_rework_no_of_women,project_manpower_rework_no_of_mason,project_manpower_rework_vendor_id,project_manpower_rework_completion_percentage,project_manpower_rework_active,project_manpower_rework_status,project_manpower_rework_remarks,project_manpower_rework_checked_by,
   project_manpower_rework_checked_on,project_manpower_rework_approved_by,project_manpower_rework_approved_on,project_manpower_rework_added_by,project_manpower_rework_added_on)
   values (:task_id,:date,:men,:women,:mason,:vendor_id,:completion_percent,:status,:active,:remarks,:checked_by,:checked_on,:approved_by,:approved_on,:added_by,:added_on)";
    try
    {
        $dbConnection = get_conn_handle();
        $project_manpower_rework_istatement = $dbConnection->prepare($project_manpower_rework_iquery);

        // Data
        $project_manpower_rework_idata = array(':task_id'=>$task_id,':date'=>$date,':men'=>$men,':women'=>$women,':mason'=>$mason,':vendor_id'=>$vendor_id,':completion_percent'=>$completion_percent,':status'=>'Pending',':active'=>'1',':remarks'=>$remarks,':checked_by'=>$checked_by,':checked_on'=>$checked_on,':approved_by'=>$approved_by,':approved_on'=>$approved_on,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_manpower_rework_istatement->execute($project_manpower_rework_idata);
		$project_manpower_rework_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_manpower_rework_id;
    }
    catch(PDOException $e)
	{
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project Manpower Rework list
INPUT 	: Rework ID, Task ID, Date, Men, Women, Mason, Completion Percent, Checked By, Checked On, Approved By, Approved On, Remarks, Added By, Start Date(for added on),
          End Date(for added on)
OUTPUT 	: List of Project Manpower Rework
BY 		: Lakshmi
*/
function db_get_project_manpower_rework($project_manpower_rework_search_data)
{
	if(array_key_exists("rework_id",$project_manpower_rework_search_data))
	{
		$rework_id = $project_manpower_rework_search_data["rework_id"];
	}
	else
	{
		$rework_id= "";
	}

	if(array_key_exists("task_id",$project_manpower_rework_search_data))
	{
		$task_id = $project_manpower_rework_search_data["task_id"];
	}
	else
	{
		$task_id= "";
	}

	if(array_key_exists("date",$project_manpower_rework_search_data))
	{
		$date = $project_manpower_rework_search_data["date"];
	}
	else
	{
		$date= "";
	}

	if(array_key_exists("men",$project_manpower_rework_search_data))
	{
		$men= $project_manpower_rework_search_data["men"];
	}
	else
	{
		$men = "";
	}

	if(array_key_exists("women",$project_manpower_rework_search_data))
	{
		$women= $project_manpower_rework_search_data["women"];
	}
	else
	{
		$women = "";
	}

	if(array_key_exists("vendor_id",$project_manpower_rework_search_data))
	{
		$vendor_id= $project_manpower_rework_search_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}

	if(array_key_exists("completion_percent",$project_manpower_rework_search_data))
	{
		$completion_percent= $project_manpower_rework_search_data["completion_percent"];
	}
	else
	{
		$completion_percent= "";
	}

	if(array_key_exists("status",$project_manpower_rework_search_data))
	{
		$status= $project_manpower_rework_search_data["status"];
	}
	else
	{
		$status= "";
	}

	if(array_key_exists("active",$project_manpower_rework_search_data))
	{
		$active= $project_manpower_rework_search_data["active"];
	}
	else
	{
		$active= "";
	}

	if(array_key_exists("checked_by",$project_manpower_rework_search_data))
	{
		$checked_by= $project_manpower_rework_search_data["checked_by"];
	}
	else
	{
		$checked_by = "";
	}

	if(array_key_exists("checked_on",$project_manpower_rework_search_data))
	{
		$checked_on= $project_manpower_rework_search_data["checked_on"];
	}
	else
	{
		$checked_on = "";
	}

	if(array_key_exists("approved_by",$project_manpower_rework_search_data))
	{
		$approved_by= $project_manpower_rework_search_data["approved_by"];
	}
	else
	{
		$approved_by = "";
	}

	if(array_key_exists("approved_on",$project_manpower_rework_search_data))
	{
		$approved_on= $project_manpower_rework_search_data["approved_on"];
	}
	else
	{
		$approved_on = "";
	}

	if(array_key_exists("added_by",$project_manpower_rework_search_data))
	{
		$added_by= $project_manpower_rework_search_data["added_by"];
	}
	else
	{
		$added_by= "";
	}

	if(array_key_exists("start_date",$project_manpower_rework_search_data))
	{
		$start_date= $project_manpower_rework_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	if(array_key_exists("end_date",$project_manpower_rework_search_data))
	{
		$end_date= $project_manpower_rework_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_project_manpower_rework_list_squery_base = "select * from project_manpower_rework ";

	$get_project_manpower_rework_list_squery_where = "";


	$filter_count = 0;

	// Data
	$get_project_manpower_rework_list_sdata = array();

	if($rework_id != "")
	{
		if($filter_count == 0)
		{
			// Quer
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." where project_manpower_rework_id = :rework_id";
		}
		else
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." and project_manpower_rework_id = :rework_id";
		}

		// Data
		$get_project_manpower_rework_list_sdata[':rework_id'] = $rework_id;

		$filter_count++;
	}

	if($task_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." where project_manpower_rework_task_id = :task_id";
		}
		else
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." and project_manpower_rework_task_id = :task_id";
		}

		// Data
		$get_project_manpower_rework_list_sdata[':task_id'] = $task_id;

		$filter_count++;
	}

	if($date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." where project_manpower_rework_date = :date";
		}
		else
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." and project_manpower_rework_date = :date";
		}

		// Data
		$get_project_manpower_rework_list_sdata[':date'] = $date;

		$filter_count++;
	}

	if($men != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." where project_manpower_rework_no_of_men = :men";
		}
		else
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." and project_manpower_rework_no_of_men = :men";
		}

		// Data
		$get_project_manpower_rework_list_sdata[':men']  = $men;

		$filter_count++;
	}

	if($women != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." where project_manpower_rework_no_of_women = :women";
		}
		else
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." and project_manpower_rework_no_of_women = :women";
		}

		// Data
		$get_project_manpower_rework_list_sdata[':women']  = $women;

		$filter_count++;
	}

	if($vendor_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." where project_manpower_rework_vendor_id = :vendor_id";
		}
		else
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." and project_manpower_rework_vendor_id = :vendor_id";
		}

		// Data
		$get_project_manpower_rework_list_sdata[':vendor_id']  = $vendor_id;

		$filter_count++;
	}

	if($completion_percent != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." where project_manpower_rework_completion_percentage = :completion_percent";
		}
		else
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." and project_manpower_rework_completion_percentage = :completion_percent";
		}

		// Data
		$get_project_manpower_rework_list_sdata[':completion_percent']  = $completion_percent;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." where project_manpower_rework_active = :active";
		}
		else
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." and project_manpower_rework_active = :active";
		}

		// Data
		$get_project_manpower_rework_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." where project_manpower_rework_status = :status";
		}
		else
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." and project_manpower_rework_status = :status";
		}

		// Data
		$get_project_manpower_rework_list_sdata[':status']  = $status;

		$filter_count++;
	}

	if($checked_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." where project_manpower_rework_checked_by = :checked_by";
		}
		else
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." and project_manpower_rework_checked_by = :checked_by";
		}

		//Data
		$get_project_manpower_rework_list_sdata[':checked_by']  = $checked_by;

		$filter_count++;
	}

	if($checked_on!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." where project_manpower_rework_checked_on = :checked_on";
		}
		else
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." and project_manpower_rework_checked_on = :checked_on";
		}

		//Data
		$get_project_manpower_rework_list_sdata[':checked_on']  = $checked_on;

		$filter_count++;
	}

	if($approved_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." where project_manpower_rework_approved_by = :approved_by";
		}
		else
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." and project_manpower_rework_approved_by = :approved_by";
		}

		//Data
		$get_project_manpower_rework_list_sdata[':approved_by']  = $approved_by;

		$filter_count++;
	}

	if($approved_on!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." where project_manpower_rework_approved_on = :approved_on";
		}
		else
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." and project_manpower_rework_approved_on = :approved_on";
		}

		//Data
		$get_project_manpower_rework_list_sdata[':approved_on']  = $approved_on;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." where project_manpower_rework_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." and project_manpower_rework_added_by = :added_by";
		}

		//Data
		$get_project_manpower_rework_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." where project_manpower_rework_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." and project_manpower_rework_added_on >= :start_date";
		}

		//Data
		$get_project_manpower_rework_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." where project_manpower_rework_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_manpower_rework_list_squery_where = $get_project_manpower_rework_list_squery_where." and project_manpower_rework_added_on <= :end_date";
		}

		//Data
		$get_project_manpower_rework_list_sdata[':end_date']  = $end_date;

		$filter_count++;
	}

	$get_project_manpower_rework_list_order_by = " order by project_manpower_rework_added_on DESC";
	$get_project_manpower_rework_list_squery = $get_project_manpower_rework_list_squery_base.$get_project_manpower_rework_list_squery_where.$get_project_manpower_rework_list_order_by;
	try
	{
		$dbConnection = get_conn_handle();

		$get_project_manpower_rework_list_sstatement = $dbConnection->prepare($get_project_manpower_rework_list_squery);

		$get_project_manpower_rework_list_sstatement -> execute($get_project_manpower_rework_list_sdata);

		$get_project_manpower_rework_list_sdetails = $get_project_manpower_rework_list_sstatement -> fetchAll();

		if(FALSE === $get_project_manpower_rework_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_manpower_rework_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_manpower_rework_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

/*
PURPOSE : To update Project Manpower Rework
INPUT 	: Rework ID, Project Manpower Rework Update Array
OUTPUT 	: Rework ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_manpower_rework($rework_id,$project_manpower_rework_update_data)
{
	if(array_key_exists("task_id",$project_manpower_rework_update_data))
	{
		$task_id = $project_manpower_rework_update_data["task_id"];
	}
	else
	{
		$task_id = "";
	}

	if(array_key_exists("date",$project_manpower_rework_update_data))
	{
		$date = $project_manpower_rework_update_data["date"];
	}
	else
	{
		$date = "";
	}

	if(array_key_exists("men",$project_manpower_rework_update_data))
	{
		$men = $project_manpower_rework_update_data["men"];
	}
	else
	{
		$men = "";
	}

	if(array_key_exists("women",$project_manpower_rework_update_data))
	{
		$women = $project_manpower_rework_update_data["women"];
	}
	else
	{
		$women = "";
	}

	if(array_key_exists("mason",$project_manpower_rework_update_data))
	{
		$mason = $project_manpower_rework_update_data["mason"];
	}
	else
	{
		$mason = "";
	}

	if(array_key_exists("vendor_id",$project_manpower_rework_update_data))
	{
		$vendor_id = $project_manpower_rework_update_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}

	if(array_key_exists("completion_percent",$project_manpower_rework_update_data))
	{
		$completion_percent = $project_manpower_rework_update_data["completion_percent"];
	}
	else
	{
		$completion_percent = "";
	}

	if(array_key_exists("status",$project_manpower_rework_update_data))
	{
		$status = $project_manpower_rework_update_data["status"];
	}
	else
	{
		$status = "";
	}

	if(array_key_exists("active",$project_manpower_rework_update_data))
	{
		$active = $project_manpower_rework_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$project_manpower_rework_update_data))
	{
		$remarks = $project_manpower_rework_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("checked_by",$project_manpower_rework_update_data))
	{
		$checked_by = $project_manpower_rework_update_data["checked_by"];
	}
	else
	{
		$checked_by = "";
	}

	if(array_key_exists("checked_on",$project_manpower_rework_update_data))
	{
		$checked_on = $project_manpower_rework_update_data["checked_on"];
	}
	else
	{
		$checked_on = "";
	}

	if(array_key_exists("approved_by",$project_manpower_rework_update_data))
	{
		$approved_by = $project_manpower_rework_update_data["approved_by"];
	}
	else
	{
		$approved_by = "";
	}

	if(array_key_exists("approved_on",$project_manpower_rework_update_data))
	{
		$approved_on = $project_manpower_rework_update_data["approved_on"];
	}
	else
	{
		$approved_on = "";
	}

	if(array_key_exists("added_by",$project_manpower_rework_update_data))
	{
		$added_by = $project_manpower_rework_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_manpower_rework_update_data))
	{
		$added_on = $project_manpower_rework_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_manpower_rework_update_uquery_base = "update project_manpower_rework set";

	$project_manpower_rework_update_uquery_set = "";

	$project_manpower_rework_update_uquery_where = " where project_manpower_rework_id = :rework_id";

	$project_manpower_rework_update_udata = array(":rework_id"=>$rework_id);

	$filter_count = 0;

	if($task_id != "")
	{
		$project_manpower_rework_update_uquery_set = $project_manpower_rework_update_uquery_set." project_manpower_rework_task_id = :task_id,";
		$project_manpower_rework_update_udata[":task_id"] = $task_id;
		$filter_count++;
	}

	if($date != "")
	{
		$project_manpower_rework_update_uquery_set = $project_manpower_rework_update_uquery_set." project_manpower_rework_date = :date,";
		$project_manpower_rework_update_udata[":date"] = $date;
		$filter_count++;
	}

	if($men != "")
	{
		$project_manpower_rework_update_uquery_set = $project_manpower_rework_update_uquery_set." project_manpower_rework_no_of_men = :men,";
		$project_manpower_rework_update_udata[":men"] = $men;
		$filter_count++;
	}

	if($women != "")
	{
		$project_manpower_rework_update_uquery_set = $project_manpower_rework_update_uquery_set." project_manpower_rework_no_of_women = :women,";
		$project_manpower_rework_update_udata[":women"] = $women;
		$filter_count++;
	}
	if($mason != "")
	{
		$project_manpower_rework_update_uquery_set = $project_manpower_rework_update_uquery_set." project_manpower_rework_no_of_mason = :mason,";
		$project_manpower_rework_update_udata[":mason"] = $mason;
		$filter_count++;
	}

	if($vendor_id != "")
	{
		$project_manpower_rework_update_uquery_set = $project_manpower_rework_update_uquery_set." project_manpower_rework_vendor_id = :vendor_id,";
		$project_manpower_rework_update_udata[":vendor_id"] = $vendor_id;
		$filter_count++;
	}

	if($completion_percent != "")
	{
		$project_manpower_rework_update_uquery_set = $project_manpower_rework_update_uquery_set." project_manpower_rework_completion_percentage = :completion_percent,";
		$project_manpower_rework_update_udata[":completion_percent"] = $completion_percent;
		$filter_count++;
	}

	if($status != "")
	{
		$project_manpower_rework_update_uquery_set = $project_manpower_rework_update_uquery_set." project_manpower_rework_status = :status,";
		$project_manpower_rework_update_udata[":status"] = $status;
		$filter_count++;
	}

	if($active != "")
	{
		$project_manpower_rework_update_uquery_set = $project_manpower_rework_update_uquery_set." project_manpower_rework_active = :active,";
		$project_manpower_rework_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_manpower_rework_update_uquery_set = $project_manpower_rework_update_uquery_set." project_manpower_rework_remarks = :remarks,";
		$project_manpower_rework_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($checked_by != "")
	{
		$project_manpower_rework_update_uquery_set = $project_manpower_rework_update_uquery_set." project_manpower_rework_checked_by = :checked_by,";
		$project_manpower_rework_update_udata[":checked_by"] = $checked_by;
		$filter_count++;
	}

	if($checked_on != "")
	{
		$project_manpower_rework_update_uquery_set = $project_manpower_rework_update_uquery_set." project_manpower_rework_checked_on = :checked_on,";
		$project_manpower_rework_update_udata[":checked_on"] = $checked_on;
		$filter_count++;
	}

	if($approved_by != "")
	{
		$project_manpower_rework_update_uquery_set = $project_manpower_rework_update_uquery_set." project_manpower_rework_approved_by = :approved_by,";
		$project_manpower_rework_update_udata[":approved_by"] = $approved_by;
		$filter_count++;
	}

	if($approved_on != "")
	{
		$project_manpower_rework_update_uquery_set = $project_manpower_rework_update_uquery_set." project_manpower_rework_approved_on = :approved_on,";
		$project_manpower_rework_update_udata[":approved_on"] = $approved_on;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_manpower_rework_update_uquery_set = $project_manpower_rework_update_uquery_set." project_manpower_rework_added_by = :added_by,";
		$project_manpower_rework_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_manpower_rework_update_uquery_set = $project_manpower_rework_update_uquery_set." project_manpower_rework_added_on = :added_on,";
		$project_manpower_rework_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_manpower_rework_update_uquery_set = trim($project_manpower_rework_update_uquery_set,',');
	}

	$project_manpower_rework_update_uquery = $project_manpower_rework_update_uquery_base.$project_manpower_rework_update_uquery_set.$project_manpower_rework_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $project_manpower_rework_update_ustatement = $dbConnection->prepare($project_manpower_rework_update_uquery);

        $project_manpower_rework_update_ustatement -> execute($project_manpower_rework_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $rework_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Contract Rework
INPUT 	: Task ID, Vendor ID, Date, Contract process, Contract Task, Uom, Location, Number, Length, Breadth, Depth, Total Measurment, Amount, Lumpsum, Completion, Status, Remarks,
          Checked By, Checked On, Approved By, Approved On, Added By
OUTPUT 	: Rework ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_contract_rework($task_id,$vendor_id,$date,$contract_process,$contract_task,$uom,$location,$number,$length,$breadth,$depth,$total_measurment,$amount,$lumpsum,$completion,$remarks,$checked_by,$checked_on,$approved_by,$approved_on,$added_by)
{
	// Query
   $project_contract_rework_iquery = "insert into project_contract_rework
   (project_contract_rework_task_id,project_contract_rework_vendor_id,project_contract_rework_date,project_contract_rework_contract_process,
   project_contract_rework_contract_task,project_contract_rework_uom,project_contract_rework_location,project_contract_rework_number,project_contract_rework_length,
   project_contract_rework_breadth,project_contract_rework_depth,project_contract_rework_total_measurement,project_contract_rework_amount,project_contract_rework_lumpsum,
   project_contract_rework_completion,project_contract_rework_status,project_contract_rework_active,project_contract_rework_remarks,project_contract_rework_checked_by,
   project_contract_rework_checked_on,project_contract_rework_approved_by,project_contract_rework_approved_on,project_contract_rework_added_by,project_contract_rework_added_on)
   values (:task_id,:vendor_id,:date,:contract_process,:contract_task,:uom,:location,:number,:length,:breadth,:depth,:total_measurment,:amount,:lumpsum,:completion,:status,:active,
   :remarks,:checked_by,:checked_on,:approved_by,:approved_on,:added_by,:added_on)";
    try
    {
        $dbConnection = get_conn_handle();
        $project_contract_rework_istatement = $dbConnection->prepare($project_contract_rework_iquery);

        // Data
        $project_contract_rework_idata = array(':task_id'=>$task_id,':vendor_id'=>$vendor_id,':date'=>$date,':contract_process'=>$contract_process,':contract_task'=>$contract_task,':uom'=>$uom,':location'=>$location,':number'=>$number,':length'=>$length,':breadth'=>$breadth,':depth'=>$depth,':total_measurment'=>$total_measurment,':amount'=>$amount,':lumpsum'=>$lumpsum,':completion'=>$completion,':status'=>'Approved',':active'=>'1',':remarks'=>$remarks,':checked_by'=>$checked_by,':checked_on'=>$checked_on,':approved_by'=>$approved_by,':approved_on'=>$approved_on,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $project_contract_rework_istatement->execute($project_contract_rework_idata);
		$project_contract_rework_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_contract_rework_id;
    }
    catch(PDOException $e)
	{
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project Contract Rework list
INPUT 	: Rework ID, Task ID, Vendor ID, Date, Contract process, Contract Task, Uom, Location, Number, Length, Breadth, Depth, Total Measurment, Amount, Lumpsum, Completion,
          Status, Remarks, Checked By, Checked On, Approved By, Approved On, Added By Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Contract Rework
BY 		: Lakshmi
*/
function db_get_project_contract_rework($project_contract_rework_search_data)
{
	if(array_key_exists("rework_id",$project_contract_rework_search_data))
	{
		$rework_id = $project_contract_rework_search_data["rework_id"];
	}
	else
	{
		$rework_id= "";
	}

	if(array_key_exists("task_id",$project_contract_rework_search_data))
	{
		$task_id = $project_contract_rework_search_data["task_id"];
	}
	else
	{
		$task_id= "";
	}

	if(array_key_exists("vendor_id",$project_contract_rework_search_data))
	{
		$vendor_id = $project_contract_rework_search_data["vendor_id"];
	}
	else
	{
		$vendor_id= "";
	}

	if(array_key_exists("date",$project_contract_rework_search_data))
	{
		$date = $project_contract_rework_search_data["date"];
	}
	else
	{
		$date= "";
	}

	if(array_key_exists("contract_process",$project_contract_rework_search_data))
	{
		$contract_process= $project_contract_rework_search_data["contract_process"];
	}
	else
	{
		$contract_process = "";
	}

	if(array_key_exists("contract_task",$project_contract_rework_search_data))
	{
		$contract_task= $project_contract_rework_search_data["contract_task"];
	}
	else
	{
		$contract_task = "";
	}

	if(array_key_exists("uom",$project_contract_rework_search_data))
	{
		$uom= $project_contract_rework_search_data["uom"];
	}
	else
	{
		$uom = "";
	}

	if(array_key_exists("location",$project_contract_rework_search_data))
	{
		$location= $project_contract_rework_search_data["location"];
	}
	else
	{
		$location = "";
	}

	if(array_key_exists("number",$project_contract_rework_search_data))
	{
		$number= $project_contract_rework_search_data["number"];
	}
	else
	{
		$number = "";
	}

	if(array_key_exists("length",$project_contract_rework_search_data))
	{
		$length= $project_contract_rework_search_data["length"];
	}
	else
	{
		$length = "";
	}

	if(array_key_exists("breadth",$project_contract_rework_search_data))
	{
		$breadth= $project_contract_rework_search_data["breadth"];
	}
	else
	{
		$breadth = "";
	}

	if(array_key_exists("depth",$project_contract_rework_search_data))
	{
		$depth= $project_contract_rework_search_data["depth"];
	}
	else
	{
		$depth = "";
	}

	if(array_key_exists("total_measurment",$project_contract_rework_search_data))
	{
		$total_measurment= $project_contract_rework_search_data["total_measurment"];
	}
	else
	{
		$total_measurment = "";
	}

	if(array_key_exists("amount",$project_contract_rework_search_data))
	{
		$amount= $project_contract_rework_search_data["amount"];
	}
	else
	{
		$amount = "";
	}

	if(array_key_exists("lumpsum",$project_contract_rework_search_data))
	{
		$lumpsum= $project_contract_rework_search_data["lumpsum"];
	}
	else
	{
		$lumpsum = "";
	}

	if(array_key_exists("completion",$project_contract_rework_search_data))
	{
		$completion= $project_contract_rework_search_data["completion"];
	}
	else
	{
		$completion= "";
	}

	if(array_key_exists("status",$project_contract_rework_search_data))
	{
		$status= $project_contract_rework_search_data["status"];
	}
	else
	{
		$status= "";
	}

	if(array_key_exists("active",$project_contract_rework_search_data))
	{
		$active= $project_contract_rework_search_data["active"];
	}
	else
	{
		$active= "";
	}

	if(array_key_exists("checked_by",$project_contract_rework_search_data))
	{
		$checked_by= $project_contract_rework_search_data["checked_by"];
	}
	else
	{
		$checked_by = "";
	}

	if(array_key_exists("checked_on",$project_contract_rework_search_data))
	{
		$checked_on= $project_contract_rework_search_data["checked_on"];
	}
	else
	{
		$checked_on = "";
	}

	if(array_key_exists("approved_by",$project_contract_rework_search_data))
	{
		$approved_by= $project_contract_rework_search_data["approved_by"];
	}
	else
	{
		$approved_by = "";
	}

	if(array_key_exists("approved_on",$project_contract_rework_search_data))
	{
		$approved_on= $project_contract_rework_search_data["approved_on"];
	}
	else
	{
		$approved_on = "";
	}

	if(array_key_exists("added_by",$project_contract_rework_search_data))
	{
		$added_by= $project_contract_rework_search_data["added_by"];
	}
	else
	{
		$added_by= "";
	}

	if(array_key_exists("start_date",$project_contract_rework_search_data))
	{
		$start_date= $project_contract_rework_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	if(array_key_exists("end_date",$project_contract_rework_search_data))
	{
		$end_date= $project_contract_rework_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_project_contract_rework_list_squery_base = "select * from project_contract_rework ";

	$get_project_contract_rework_list_squery_where = "";


	$filter_count = 0;

	// Data
	$get_project_contract_rework_list_sdata = array();

	if($rework_id != "")
	{
		if($filter_count == 0)
		{
			// Quer
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_id = :rework_id";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_id = :rework_id";
		}

		// Data
		$get_project_contract_rework_list_sdata[':rework_id'] = $rework_id;

		$filter_count++;
	}

	if($task_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_task_id = :task_id";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_task_id = :task_id";
		}

		// Data
		$get_project_contract_rework_list_sdata[':task_id'] = $task_id;

		$filter_count++;
	}

	if($vendor_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_vendor_id = :vendor_id";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_vendor_id = :vendor_id";
		}

		// Data
		$get_project_contract_rework_list_sdata[':vendor_id'] = $vendor_id;

		$filter_count++;
	}

	if($date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_date = :date";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_date = :date";
		}

		// Data
		$get_project_contract_rework_list_sdata[':date'] = $date;

		$filter_count++;
	}

	if($contract_process != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_contract_process = :contract_process";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_contract_process = :contract_process";
		}

		// Data
		$get_project_contract_rework_list_sdata[':contract_process'] = $contract_process;

		$filter_count++;
	}

	if($contract_task != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_contract_task = :contract_task";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_contract_task = :contract_task";
		}

		// Data
		$get_project_contract_rework_list_sdata[':contract_task'] = $contract_task;

		$filter_count++;
	}

	if($uom != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_uom = :uom";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_uom = :uom";
		}

		// Data
		$get_project_contract_rework_list_sdata[':uom'] = $uom;

		$filter_count++;
	}

	if($location != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_location = :location";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_location = :location";
		}

		// Data
		$get_project_contract_rework_list_sdata[':location'] = $location;

		$filter_count++;
	}

	if($number != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_number = :number";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_number = :number";
		}

		// Data
		$get_project_contract_rework_list_sdata[':number'] = $number;

		$filter_count++;
	}

	if($length != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_length = :length";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_length = :length";
		}

		// Data
		$get_project_contract_rework_list_sdata[':length'] = $length;

		$filter_count++;
	}

	if($breadth != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_breadth = :breadth";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_breadth = :breadth";
		}

		// Data
		$get_project_contract_rework_list_sdata[':breadth']  = $breadth;

		$filter_count++;
	}

	if($depth != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_depth = :depth";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_depth = :depth";
		}

		// Data
		$get_project_contract_rework_list_sdata[':depth']  = $depth;

		$filter_count++;
	}

	if($total_measurment != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_total_measurement = :total_measurment";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_total_measurement = :total_measurment";
		}

		// Data
		$get_project_contract_rework_list_sdata[':total_measurment']  = $total_measurment;

		$filter_count++;
	}

	if($amount != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_amount = :amount";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_amount = :amount";
		}

		// Data
		$get_project_contract_rework_list_sdata[':amount']  = $amount;

		$filter_count++;
	}

	if($lumpsum != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_lumpsum = :lumpsum";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_lumpsum = :lumpsum";
		}

		// Data
		$get_project_contract_rework_list_sdata[':lumpsum']  = $lumpsum;

		$filter_count++;
	}

	if($completion != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_completion = :completion";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_completion = :completion";
		}

		// Data
		$get_project_contract_rework_list_sdata[':completion']  = $completion;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_active = :active";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_active = :active";
		}

		// Data
		$get_project_contract_rework_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_status = :status";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_status = :status";
		}

		// Data
		$get_project_contract_rework_list_sdata[':status']  = $status;

		$filter_count++;
	}

	if($checked_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_checked_by = :checked_by";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_checked_by = :checked_by";
		}

		//Data
		$get_project_contract_rework_list_sdata[':checked_by']  = $checked_by;

		$filter_count++;
	}

	if($checked_on!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_checked_on = :checked_on";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_checked_on = :checked_on";
		}

		//Data
		$get_project_contract_rework_list_sdata[':checked_on']  = $checked_on;

		$filter_count++;
	}

	if($approved_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_approved_by = :approved_by";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_approved_by = :approved_by";
		}

		//Data
		$get_project_contract_rework_list_sdata[':approved_by']  = $approved_by;

		$filter_count++;
	}

	if($approved_on!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_approved_on = :approved_on";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_approved_on = :approved_on";
		}

		//Data
		$get_project_contract_rework_list_sdata[':approved_on']  = $approved_on;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_added_by = :added_by";
		}

		//Data
		$get_project_contract_rework_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_added_on >= :start_date";
		}

		//Data
		$get_project_contract_rework_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." where project_contract_rework_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_contract_rework_list_squery_where = $get_project_contract_rework_list_squery_where." and project_contract_rework_added_on <= :end_date";
		}

		//Data
		$get_project_contract_rework_list_sdata[':end_date']  = $end_date;

		$filter_count++;
	}

	$get_project_contract_rework_list_order_by = " order by project_contract_rework_added_on DESC";
	$get_project_contract_rework_list_squery = $get_project_contract_rework_list_squery_base.$get_project_contract_rework_list_squery_where.$get_project_contract_rework_list_order_by;
	try
	{
		$dbConnection = get_conn_handle();

		$get_project_contract_rework_list_sstatement = $dbConnection->prepare($get_project_contract_rework_list_squery);

		$get_project_contract_rework_list_sstatement -> execute($get_project_contract_rework_list_sdata);

		$get_project_contract_rework_list_sdetails = $get_project_contract_rework_list_sstatement -> fetchAll();

		if(FALSE === $get_project_contract_rework_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_contract_rework_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_contract_rework_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

/*
PURPOSE : To update Project Contract Rework
INPUT 	: Rework ID, Project Contract Rework Update Array
OUTPUT 	: Rework ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_contract_rework($rework_id,$project_contract_rework_update_data)
{
	if(array_key_exists("task_id",$project_contract_rework_update_data))
	{
		$task_id = $project_contract_rework_update_data["task_id"];
	}
	else
	{
		$task_id = "";
	}

	if(array_key_exists("vendor_id",$project_contract_rework_update_data))
	{
		$vendor_id = $project_contract_rework_update_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}

	if(array_key_exists("date",$project_contract_rework_update_data))
	{
		$date = $project_contract_rework_update_data["date"];
	}
	else
	{
		$date = "";
	}

	if(array_key_exists("contract_process",$project_contract_rework_update_data))
	{
		$contract_process = $project_contract_rework_update_data["contract_process"];
	}
	else
	{
		$contract_process = "";
	}

	if(array_key_exists("contract_task",$project_contract_rework_update_data))
	{
		$contract_task = $project_contract_rework_update_data["contract_task"];
	}
	else
	{
		$contract_task = "";
	}

	if(array_key_exists("uom",$project_contract_rework_update_data))
	{
		$uom = $project_contract_rework_update_data["uom"];
	}
	else
	{
		$uom = "";
	}

	if(array_key_exists("location",$project_contract_rework_update_data))
	{
		$location = $project_contract_rework_update_data["location"];
	}
	else
	{
		$location = "";
	}

	if(array_key_exists("number",$project_contract_rework_update_data))
	{
		$number = $project_contract_rework_update_data["number"];
	}
	else
	{
		$number = "";
	}

	if(array_key_exists("length",$project_contract_rework_update_data))
	{
		$length = $project_contract_rework_update_data["length"];
	}
	else
	{
		$length = "";
	}

	if(array_key_exists("breadth",$project_contract_rework_update_data))
	{
		$breadth = $project_contract_rework_update_data["breadth"];
	}
	else
	{
		$breadth = "";
	}

	if(array_key_exists("depth",$project_contract_rework_update_data))
	{
		$depth = $project_contract_rework_update_data["depth"];
	}
	else
	{
		$depth = "";
	}

	if(array_key_exists("total_measurment",$project_contract_rework_update_data))
	{
		$total_measurment = $project_contract_rework_update_data["total_measurment"];
	}
	else
	{
		$total_measurment = "";
	}

	if(array_key_exists("amount",$project_contract_rework_update_data))
	{
		$amount = $project_contract_rework_update_data["amount"];
	}
	else
	{
		$amount = "";
	}

	if(array_key_exists("lumpsum",$project_contract_rework_update_data))
	{
		$lumpsum = $project_contract_rework_update_data["lumpsum"];
	}
	else
	{
		$lumpsum = "";
	}

	if(array_key_exists("completion",$project_contract_rework_update_data))
	{
		$completion = $project_contract_rework_update_data["completion"];
	}
	else
	{
		$completion = "";
	}

	if(array_key_exists("status",$project_contract_rework_update_data))
	{
		$status = $project_contract_rework_update_data["status"];
	}
	else
	{
		$status = "";
	}

	if(array_key_exists("active",$project_contract_rework_update_data))
	{
		$active = $project_contract_rework_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$project_contract_rework_update_data))
	{
		$remarks = $project_contract_rework_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("checked_by",$project_contract_rework_update_data))
	{
		$checked_by = $project_contract_rework_update_data["checked_by"];
	}
	else
	{
		$checked_by = "";
	}

	if(array_key_exists("checked_on",$project_contract_rework_update_data))
	{
		$checked_on = $project_contract_rework_update_data["checked_on"];
	}
	else
	{
		$checked_on = "";
	}

	if(array_key_exists("approved_by",$project_contract_rework_update_data))
	{
		$approved_by = $project_contract_rework_update_data["approved_by"];
	}
	else
	{
		$approved_by = "";
	}

	if(array_key_exists("approved_on",$project_contract_rework_update_data))
	{
		$approved_on = $project_contract_rework_update_data["approved_on"];
	}
	else
	{
		$approved_on = "";
	}

	if(array_key_exists("added_by",$project_contract_rework_update_data))
	{
		$added_by = $project_contract_rework_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_contract_rework_update_data))
	{
		$added_on = $project_contract_rework_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_contract_rework_update_uquery_base = "update project_contract_rework set";

	$project_contract_rework_update_uquery_set = "";

	$project_contract_rework_update_uquery_where = " where project_contract_rework_id = :rework_id";

	$project_contract_rework_update_udata = array(":rework_id"=>$rework_id);

	$filter_count = 0;

	if($task_id != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_task_id = :task_id,";
		$project_contract_rework_update_udata[":task_id"] = $task_id;
		$filter_count++;
	}

	if($vendor_id != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_vendor_id = :vendor_id,";
		$project_contract_rework_update_udata[":vendor_id"] = $vendor_id;
		$filter_count++;
	}

	if($date != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_date = :date,";
		$project_contract_rework_update_udata[":date"] = $date;
		$filter_count++;
	}

	if($contract_process != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_contract_process = :contract_process,";
		$project_contract_rework_update_udata[":contract_process"] = $contract_process;
		$filter_count++;
	}

	if($contract_task != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_contract_task = :contract_task,";
		$project_contract_rework_update_udata[":contract_task"] = $contract_task;
		$filter_count++;
	}

	if($uom != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_uom = :uom,";
		$project_contract_rework_update_udata[":uom"] = $uom;
		$filter_count++;
	}

	if($location != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_location = :location,";
		$project_contract_rework_update_udata[":location"] = $location;
		$filter_count++;
	}

	if($number != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_number = :number,";
		$project_contract_rework_update_udata[":number"] = $number;
		$filter_count++;
	}

	if($length != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_length = :length,";
		$project_contract_rework_update_udata[":length"] = $length;
		$filter_count++;
	}

	if($breadth != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_breadth = :breadth,";
		$project_contract_rework_update_udata[":breadth"] = $breadth;
		$filter_count++;
	}

	if($depth != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_depth = :depth,";
		$project_contract_rework_update_udata[":depth"] = $depth;
		$filter_count++;
	}

	if($total_measurment != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_total_measurement = :total_measurment,";
		$project_contract_rework_update_udata[":total_measurment"] = $total_measurment;
		$filter_count++;
	}

	if($amount != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_amount = :amount,";
		$project_contract_rework_update_udata[":amount"] = $amount;
		$filter_count++;
	}

	if($lumpsum != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_lumpsum = :lumpsum,";
		$project_contract_rework_update_udata[":lumpsum"] = $lumpsum;
		$filter_count++;
	}

	if($completion != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_completion = :completion,";
		$project_contract_rework_update_udata[":completion"] = $completion;
		$filter_count++;
	}

	if($status != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_status = :status,";
		$project_contract_rework_update_udata[":status"] = $status;
		$filter_count++;
	}

	if($active != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_active = :active,";
		$project_contract_rework_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_remarks = :remarks,";
		$project_contract_rework_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($checked_by != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_checked_by = :checked_by,";
		$project_contract_rework_update_udata[":checked_by"] = $checked_by;
		$filter_count++;
	}

	if($checked_on != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_checked_on = :checked_on,";
		$project_contract_rework_update_udata[":checked_on"] = $checked_on;
		$filter_count++;
	}

	if($approved_by != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_approved_by = :approved_by,";
		$project_contract_rework_update_udata[":approved_by"] = $approved_by;
		$filter_count++;
	}

	if($approved_on != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_approved_on = :approved_on,";
		$project_contract_rework_update_udata[":approved_on"] = $approved_on;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_added_by = :added_by,";
		$project_contract_rework_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_contract_rework_update_uquery_set = $project_contract_rework_update_uquery_set." project_contract_rework_added_on = :added_on,";
		$project_contract_rework_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_contract_rework_update_uquery_set = trim($project_contract_rework_update_uquery_set,',');
	}

	$project_contract_rework_update_uquery = $project_contract_rework_update_uquery_base.$project_contract_rework_update_uquery_set.$project_contract_rework_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $project_contract_rework_update_ustatement = $dbConnection->prepare($project_contract_rework_update_uquery);

        $project_contract_rework_update_ustatement -> execute($project_contract_rework_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $rework_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Machine Rework
INPUT 	: Task ID, Vendor ID, Machine ID, Start Date Time, End Date Time, Plan Off Time, Aditional Cost, Number, Fuel Charges, with Fuel Charges, Bata, Issued Fuel, Display Status,
          Completion,  Machine Type, Check Status, Remarks, Checked By, Checked On, Approved By, Approved On, Added By
OUTPUT 	: Rework ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_machine_rework($task_id,$vendor_id,$machine_id,$start_date_time,$end_date_time,$plan_off_time,$additional_cost,$number,$fuel_charges,$with_fuel_charges,$bata,$issued_fuel,$display_status,$completion,$machine_type,$check_status,$remarks,$checked_by,$checked_on,$approved_by,$approved_on,$added_by)
{
	// Query
   $project_machine_rework_iquery = "insert into project_machine_rework
   (project_machine_rework_task_id,project_machine_rework_vendor_id,project_machine_rework_machine_id,project_machine_rework_start_date_time,project_machine_rework_end_date_time,
   project_machine_rework_plan_off_time,project_machine_rework_plan_additional_cost,project_machine_rework_number,project_machine_rework_fuel_charges,
   project_machine_rework_with_fuel_charges,project_machine_rework_bata,project_machine_rework_issued_fuel,project_machine_rework_display_status,
   project_machine_rework_machine_completion,project_machine_rework_machine_type,project_machine_rework_check_status,project_machine_rework_active,project_machine_rework_remarks,
   project_machine_rework_checked_by,project_machine_rework_checked_on,project_machine_rework_approved_by,project_machine_rework_approved_on,project_machine_rework_added_by,
   project_machine_rework_added_on)
   values (:task_id,:vendor_id,:machine_id,:start_date_time,:end_date_time,:plan_off_time,:additional_cost,:number,:fuel_charges,:with_fuel_charges,:bata,:issued_fuel,
   :display_status,:completion,:machine_type,:check_status,:active,:remarks,:checked_by,:checked_on,:approved_by,:approved_on,:added_by,:added_on)";
    try
    {
        $dbConnection = get_conn_handle();
        $project_machine_rework_istatement = $dbConnection->prepare($project_machine_rework_iquery);

        // Data
        $project_machine_rework_idata = array(':task_id'=>$task_id,':vendor_id'=>$vendor_id,':machine_id'=>$machine_id,':start_date_time'=>$start_date_time,
		':end_date_time'=>$end_date_time,':plan_off_time'=>$plan_off_time,':additional_cost'=>$additional_cost,':number'=>$number,':fuel_charges'=>$fuel_charges,
		':with_fuel_charges'=>$with_fuel_charges,':bata'=>$bata,':issued_fuel'=>$issued_fuel,':display_status'=>$display_status,':completion'=>$completion,
		':machine_type'=>$machine_type,':check_status'=>'check_status',':active'=>'1',':remarks'=>$remarks,':checked_by'=>$checked_by,':checked_on'=>$checked_on,
		':approved_by'=>$approved_by,':approved_on'=>$approved_on,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_machine_rework_istatement->execute($project_machine_rework_idata);
		$project_machine_rework_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_machine_rework_id;
    }
    catch(PDOException $e)
	{
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project Machine Rework list
INPUT 	: Rework ID, Task ID, Vendor ID, Machine ID, Start Date Time, End Date Time, Plan Off Time, Aditional Cost, Number, Fuel Charges, with Fuel Charges, Bata,
          Issued Fuel, Display Status, Completion,  Machine Type, Check Status,, Checked By, Checked On, Approved By, Approved On, Added By Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Machine Rework
BY 		: Lakshmi
*/
function db_get_project_machine_rework($project_machine_rework_search_data)
{
	if(array_key_exists("rework_id",$project_machine_rework_search_data))
	{
		$rework_id = $project_machine_rework_search_data["rework_id"];
	}
	else
	{
		$rework_id= "";
	}

	if(array_key_exists("task_id",$project_machine_rework_search_data))
	{
		$task_id = $project_machine_rework_search_data["task_id"];
	}
	else
	{
		$task_id= "";
	}

	if(array_key_exists("vendor_id",$project_machine_rework_search_data))
	{
		$vendor_id = $project_machine_rework_search_data["vendor_id"];
	}
	else
	{
		$vendor_id= "";
	}

	if(array_key_exists("machine_id",$project_machine_rework_search_data))
	{
		$machine_id = $project_machine_rework_search_data["machine_id"];
	}
	else
	{
		$machine_id= "";
	}

	if(array_key_exists("start_date_time",$project_machine_rework_search_data))
	{
		$start_date_time= $project_machine_rework_search_data["start_date_time"];
	}
	else
	{
		$start_date_time = "";
	}

	if(array_key_exists("end_date_time",$project_machine_rework_search_data))
	{
		$end_date_time= $project_machine_rework_search_data["end_date_time"];
	}
	else
	{
		$end_date_time = "";
	}

	if(array_key_exists("plan_off_time",$project_machine_rework_search_data))
	{
		$plan_off_time= $project_machine_rework_search_data["plan_off_time"];
	}
	else
	{
		$plan_off_time = "";
	}

	if(array_key_exists("additional_cost",$project_machine_rework_search_data))
	{
		$additional_cost= $project_machine_rework_search_data["additional_cost"];
	}
	else
	{
		$additional_cost = "";
	}

	if(array_key_exists("number",$project_machine_rework_search_data))
	{
		$number= $project_machine_rework_search_data["number"];
	}
	else
	{
		$number = "";
	}

	if(array_key_exists("fuel_charges",$project_machine_rework_search_data))
	{
		$fuel_charges= $project_machine_rework_search_data["fuel_charges"];
	}
	else
	{
		$fuel_charges = "";
	}

	if(array_key_exists("with_fuel_charges",$project_machine_rework_search_data))
	{
		$with_fuel_charges= $project_machine_rework_search_data["with_fuel_charges"];
	}
	else
	{
		$with_fuel_charges = "";
	}

	if(array_key_exists("bata",$project_machine_rework_search_data))
	{
		$bata= $project_machine_rework_search_data["bata"];
	}
	else
	{
		$bata = "";
	}

	if(array_key_exists("issued_fuel",$project_machine_rework_search_data))
	{
		$issued_fuel= $project_machine_rework_search_data["issued_fuel"];
	}
	else
	{
		$issued_fuel = "";
	}

	if(array_key_exists("display_status",$project_machine_rework_search_data))
	{
		$display_status= $project_machine_rework_search_data["display_status"];
	}
	else
	{
		$display_status = "";
	}

	if(array_key_exists("completion",$project_machine_rework_search_data))
	{
		$completion= $project_machine_rework_search_data["completion"];
	}
	else
	{
		$completion= "";
	}

	if(array_key_exists("machine_type",$project_machine_rework_search_data))
	{
		$machine_type= $project_machine_rework_search_data["machine_type"];
	}
	else
	{
		$machine_type = "";
	}

	if(array_key_exists("check_status",$project_machine_rework_search_data))
	{
		$check_status= $project_machine_rework_search_data["check_status"];
	}
	else
	{
		$check_status= "";
	}

	if(array_key_exists("active",$project_machine_rework_search_data))
	{
		$active= $project_machine_rework_search_data["active"];
	}
	else
	{
		$active= "";
	}

	if(array_key_exists("checked_by",$project_machine_rework_search_data))
	{
		$checked_by= $project_machine_rework_search_data["checked_by"];
	}
	else
	{
		$checked_by = "";
	}

	if(array_key_exists("checked_on",$project_machine_rework_search_data))
	{
		$checked_on= $project_machine_rework_search_data["checked_on"];
	}
	else
	{
		$checked_on = "";
	}

	if(array_key_exists("approved_by",$project_machine_rework_search_data))
	{
		$approved_by= $project_machine_rework_search_data["approved_by"];
	}
	else
	{
		$approved_by = "";
	}

	if(array_key_exists("approved_on",$project_machine_rework_search_data))
	{
		$approved_on= $project_machine_rework_search_data["approved_on"];
	}
	else
	{
		$approved_on = "";
	}

	if(array_key_exists("added_by",$project_machine_rework_search_data))
	{
		$added_by= $project_machine_rework_search_data["added_by"];
	}
	else
	{
		$added_by= "";
	}

	if(array_key_exists("start_date",$project_machine_rework_search_data))
	{
		$start_date= $project_machine_rework_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	if(array_key_exists("end_date",$project_machine_rework_search_data))
	{
		$end_date= $project_machine_rework_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_project_machine_rework_list_squery_base = "select * from project_machine_rework ";

	$get_project_machine_rework_list_squery_where = "";


	$filter_count = 0;

	// Data
	$get_project_machine_rework_list_sdata = array();

	if($rework_id != "")
	{
		if($filter_count == 0)
		{
			// Quer
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_id = :rework_id";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_id = :rework_id";
		}

		// Data
		$get_project_machine_rework_list_sdata[':rework_id'] = $rework_id;

		$filter_count++;
	}

	if($task_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_task_id = :task_id";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_task_id = :task_id";
		}

		// Data
		$get_project_machine_rework_list_sdata[':task_id'] = $task_id;

		$filter_count++;
	}

	if($vendor_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_vendor_id = :vendor_id";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_vendor_id = :vendor_id";
		}

		// Data
		$get_project_machine_rework_list_sdata[':vendor_id'] = $vendor_id;

		$filter_count++;
	}

	if($machine_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_machine_id = :machine_id";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_machine_id = :machine_id";
		}

		// Data
		$get_project_machine_rework_list_sdata[':machine_id'] = $machine_id;

		$filter_count++;
	}

	if($start_date_time != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_start_date_time = :start_date_time";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_start_date_time = :start_date_time";
		}

		// Data
		$get_project_machine_rework_list_sdata[':start_date_time'] = $start_date_time;

		$filter_count++;
	}

	if($end_date_time != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_end_date_time = :end_date_time";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_end_date_time = :end_date_time";
		}

		// Data
		$get_project_machine_rework_list_sdata[':end_date_time'] = $end_date_time;

		$filter_count++;
	}

	if($plan_off_time != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_plan_off_time = :plan_off_time";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_plan_off_time = :plan_off_time";
		}

		// Data
		$get_project_machine_rework_list_sdata[':plan_off_time'] = $plan_off_time;

		$filter_count++;
	}

	if($additional_cost != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_plan_additional_cost = :additional_cost";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_plan_additional_cost = :additional_cost";
		}

		// Data
		$get_project_machine_rework_list_sdata[':additional_cost'] = $additional_cost;

		$filter_count++;
	}

	if($number != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_number = :number";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_number = :number";
		}

		// Data
		$get_project_machine_rework_list_sdata[':number'] = $number;

		$filter_count++;
	}

	if($fuel_charges != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_fuel_charges = :fuel_charges";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_fuel_charges = :fuel_charges";
		}

		// Data
		$get_project_machine_rework_list_sdata[':fuel_charges'] = $fuel_charges;

		$filter_count++;
	}

	if($with_fuel_charges != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_with_fuel_charges = :with_fuel_charges";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_with_fuel_charges = :with_fuel_charges";
		}

		// Data
		$get_project_machine_rework_list_sdata[':with_fuel_charges']  = $with_fuel_charges;

		$filter_count++;
	}

	if($bata != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_bata = :bata";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_bata = :bata";
		}

		// Data
		$get_project_machine_rework_list_sdata[':bata']  = $bata;

		$filter_count++;
	}

	if($issued_fuel != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_issued_fuel = :issued_fuel";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_issued_fuel = :issued_fuel";
		}

		// Data
		$get_project_machine_rework_list_sdata[':issued_fuel']  = $issued_fuel;

		$filter_count++;
	}

	if($display_status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_display_status = :display_status";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_display_status = :display_status";
		}

		// Data
		$get_project_machine_rework_list_sdata[':display_status']  = $display_status;

		$filter_count++;
	}

	if($completion != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_machine_completion = :completion";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_machine_completion = :completion";
		}

		// Data
		$get_project_machine_rework_list_sdata[':completion']  = $completion;

		$filter_count++;
	}

	if($machine_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_machine_type = :machine_type";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_machine_type = :machine_type";
		}

		// Data
		$get_project_machine_rework_list_sdata[':machine_type']  = $machine_type;

		$filter_count++;
	}

	if($check_status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_check_status = :check_status";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_check_status = :check_status";
		}

		// Data
		$get_project_machine_rework_list_sdata[':check_status']  = $check_status;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_active = :active";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_active = :active";
		}

		// Data
		$get_project_machine_rework_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($checked_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_checked_by = :checked_by";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_checked_by = :checked_by";
		}

		//Data
		$get_project_machine_rework_list_sdata[':checked_by']  = $checked_by;

		$filter_count++;
	}

	if($checked_on!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_checked_on = :checked_on";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_checked_on = :checked_on";
		}

		//Data
		$get_project_machine_rework_list_sdata[':checked_on']  = $checked_on;

		$filter_count++;
	}

	if($approved_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_approved_by = :approved_by";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_approved_by = :approved_by";
		}

		//Data
		$get_project_machine_rework_list_sdata[':approved_by']  = $approved_by;

		$filter_count++;
	}

	if($approved_on!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_approved_on = :approved_on";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_approved_on = :approved_on";
		}

		//Data
		$get_project_machine_rework_list_sdata[':approved_on']  = $approved_on;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_added_by = :added_by";
		}

		//Data
		$get_project_machine_rework_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_added_on >= :start_date";
		}

		//Data
		$get_project_machine_rework_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." where project_machine_rework_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_machine_rework_list_squery_where = $get_project_machine_rework_list_squery_where." and project_machine_rework_added_on <= :end_date";
		}

		//Data
		$get_project_machine_rework_list_sdata[':end_date']  = $end_date;

		$filter_count++;
	}

	$get_project_machine_rework_list_order_by = " order by project_machine_rework_added_on DESC";
	$get_project_machine_rework_list_squery = $get_project_machine_rework_list_squery_base.$get_project_machine_rework_list_squery_where.$get_project_machine_rework_list_order_by;
	try
	{
		$dbConnection = get_conn_handle();

		$get_project_machine_rework_list_sstatement = $dbConnection->prepare($get_project_machine_rework_list_squery);

		$get_project_machine_rework_list_sstatement -> execute($get_project_machine_rework_list_sdata);

		$get_project_machine_rework_list_sdetails = $get_project_machine_rework_list_sstatement -> fetchAll();

		if(FALSE === $get_project_machine_rework_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_machine_rework_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_machine_rework_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

/*
PURPOSE : To update Project Machine Rework
INPUT 	: Rework ID, Project Machine Rework Update Array
OUTPUT 	: Rework ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_machine_rework($rework_id,$project_machine_rework_update_data)
{
	if(array_key_exists("task_id",$project_machine_rework_update_data))
	{
		$task_id = $project_machine_rework_update_data["task_id"];
	}
	else
	{
		$task_id = "";
	}

	if(array_key_exists("vendor_id",$project_machine_rework_update_data))
	{
		$vendor_id = $project_machine_rework_update_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}

	if(array_key_exists("machine_id",$project_machine_rework_update_data))
	{
		$machine_id = $project_machine_rework_update_data["machine_id"];
	}
	else
	{
		$machine_id = "";
	}

	if(array_key_exists("start_date_time",$project_machine_rework_update_data))
	{
		$start_date_time = $project_machine_rework_update_data["start_date_time"];
	}
	else
	{
		$start_date_time = "";
	}

	if(array_key_exists("end_date_time",$project_machine_rework_update_data))
	{
		$end_date_time = $project_machine_rework_update_data["end_date_time"];
	}
	else
	{
		$end_date_time = "";
	}

	if(array_key_exists("plan_off_time",$project_machine_rework_update_data))
	{
		$plan_off_time = $project_machine_rework_update_data["plan_off_time"];
	}
	else
	{
		$plan_off_time = "";
	}

	if(array_key_exists("additional_cost",$project_machine_rework_update_data))
	{
		$additional_cost = $project_machine_rework_update_data["additional_cost"];
	}
	else
	{
		$additional_cost = "";
	}

	if(array_key_exists("number",$project_machine_rework_update_data))
	{
		$number = $project_machine_rework_update_data["number"];
	}
	else
	{
		$number = "";
	}

	if(array_key_exists("fuel_charges",$project_machine_rework_update_data))
	{
		$fuel_charges = $project_machine_rework_update_data["fuel_charges"];
	}
	else
	{
		$fuel_charges = "";
	}

	if(array_key_exists("with_fuel_charges",$project_machine_rework_update_data))
	{
		$with_fuel_charges = $project_machine_rework_update_data["with_fuel_charges"];
	}
	else
	{
		$with_fuel_charges = "";
	}

	if(array_key_exists("bata",$project_machine_rework_update_data))
	{
		$bata = $project_machine_rework_update_data["bata"];
	}
	else
	{
		$bata = "";
	}

	if(array_key_exists("issued_fuel",$project_machine_rework_update_data))
	{
		$issued_fuel = $project_machine_rework_update_data["issued_fuel"];
	}
	else
	{
		$issued_fuel = "";
	}

	if(array_key_exists("display_status",$project_machine_rework_update_data))
	{
		$display_status = $project_machine_rework_update_data["display_status"];
	}
	else
	{
		$display_status = "";
	}

	if(array_key_exists("completion",$project_machine_rework_update_data))
	{
		$completion = $project_machine_rework_update_data["completion"];
	}
	else
	{
		$completion = "";
	}

	if(array_key_exists("machine_type",$project_machine_rework_update_data))
	{
		$machine_type = $project_machine_rework_update_data["machine_type"];
	}
	else
	{
		$machine_type = "";
	}

	if(array_key_exists("check_status",$project_machine_rework_update_data))
	{
		$check_status = $project_machine_rework_update_data["check_status"];
	}
	else
	{
		$check_status = "";
	}

	if(array_key_exists("active",$project_machine_rework_update_data))
	{
		$active = $project_machine_rework_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$project_machine_rework_update_data))
	{
		$remarks = $project_machine_rework_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("checked_by",$project_machine_rework_update_data))
	{
		$checked_by = $project_machine_rework_update_data["checked_by"];
	}
	else
	{
		$checked_by = "";
	}

	if(array_key_exists("checked_on",$project_machine_rework_update_data))
	{
		$checked_on = $project_machine_rework_update_data["checked_on"];
	}
	else
	{
		$checked_on = "";
	}

	if(array_key_exists("approved_by",$project_machine_rework_update_data))
	{
		$approved_by = $project_machine_rework_update_data["approved_by"];
	}
	else
	{
		$approved_by = "";
	}

	if(array_key_exists("approved_on",$project_machine_rework_update_data))
	{
		$approved_on = $project_machine_rework_update_data["approved_on"];
	}
	else
	{
		$approved_on = "";
	}

	if(array_key_exists("added_by",$project_machine_rework_update_data))
	{
		$added_by = $project_machine_rework_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_machine_rework_update_data))
	{
		$added_on = $project_machine_rework_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_machine_rework_update_uquery_base = "update project_machine_rework set";

	$project_machine_rework_update_uquery_set = "";

	$project_machine_rework_update_uquery_where = " where project_machine_rework_id = :rework_id";

	$project_machine_rework_update_udata = array(":rework_id"=>$rework_id);

	$filter_count = 0;

	if($task_id != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_task_id = :task_id,";
		$project_machine_rework_update_udata[":task_id"] = $task_id;
		$filter_count++;
	}

	if($vendor_id != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_vendor_id = :vendor_id,";
		$project_machine_rework_update_udata[":vendor_id"] = $vendor_id;
		$filter_count++;
	}

	if($machine_id != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_machine_id = :machine_id,";
		$project_machine_rework_update_udata[":machine_id"] = $machine_id;
		$filter_count++;
	}

	if($start_date_time != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_start_date_time = :start_date_time,";
		$project_machine_rework_update_udata[":start_date_time"] = $start_date_time;
		$filter_count++;
	}

	if($end_date_time != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_end_date_time = :end_date_time,";
		$project_machine_rework_update_udata[":end_date_time"] = $end_date_time;
		$filter_count++;
	}

	if($plan_off_time != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_plan_off_time = :plan_off_time,";
		$project_machine_rework_update_udata[":plan_off_time"] = $plan_off_time;
		$filter_count++;
	}

	if($additional_cost != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_plan_additional_cost = :additional_cost,";
		$project_machine_rework_update_udata[":additional_cost"] = $additional_cost;
		$filter_count++;
	}

	if($number != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_number = :number,";
		$project_machine_rework_update_udata[":number"] = $number;
		$filter_count++;
	}

	if($fuel_charges != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_fuel_charges = :fuel_charges,";
		$project_machine_rework_update_udata[":fuel_charges"] = $fuel_charges;
		$filter_count++;
	}

	if($with_fuel_charges != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_with_fuel_charges = :with_fuel_charges,";
		$project_machine_rework_update_udata[":with_fuel_charges"] = $with_fuel_charges;
		$filter_count++;
	}

	if($bata != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_bata = :bata,";
		$project_machine_rework_update_udata[":bata"] = $bata;
		$filter_count++;
	}

	if($issued_fuel != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_issued_fuel = :issued_fuel,";
		$project_machine_rework_update_udata[":issued_fuel"] = $issued_fuel;
		$filter_count++;
	}

	if($display_status != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_display_status = :display_status,";
		$project_machine_rework_update_udata[":display_status"] = $display_status;
		$filter_count++;
	}

	if($completion != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_machine_completion = :completion,";
		$project_machine_rework_update_udata[":completion"] = $completion;
		$filter_count++;
	}


	if($machine_type != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_machine_type = :machine_type,";
		$project_machine_rework_update_udata[":machine_type"] = $machine_type;
		$filter_count++;
	}

	if($check_status != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_check_status = :check_status,";
		$project_machine_rework_update_udata[":check_status"] = $check_status;
		$filter_count++;
	}

	if($active != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_active = :active,";
		$project_machine_rework_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_remarks = :remarks,";
		$project_machine_rework_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($checked_by != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_checked_by = :checked_by,";
		$project_machine_rework_update_udata[":checked_by"] = $checked_by;
		$filter_count++;
	}

	if($checked_on != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_checked_on = :checked_on,";
		$project_machine_rework_update_udata[":checked_on"] = $checked_on;
		$filter_count++;
	}

	if($approved_by != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_approved_by = :approved_by,";
		$project_machine_rework_update_udata[":approved_by"] = $approved_by;
		$filter_count++;
	}

	if($approved_on != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_approved_on = :approved_on,";
		$project_machine_rework_update_udata[":approved_on"] = $approved_on;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_added_by = :added_by,";
		$project_machine_rework_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_machine_rework_update_uquery_set = $project_machine_rework_update_uquery_set." project_machine_rework_added_on = :added_on,";
		$project_machine_rework_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_machine_rework_update_uquery_set = trim($project_machine_rework_update_uquery_set,',');
	}

	$project_machine_rework_update_uquery = $project_machine_rework_update_uquery_base.$project_machine_rework_update_uquery_set.$project_machine_rework_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $project_machine_rework_update_ustatement = $dbConnection->prepare($project_machine_rework_update_uquery);

        $project_machine_rework_update_ustatement -> execute($project_machine_rework_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $rework_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}

/*
PURPOSE : To update Project Task Actual Man Power
INPUT 	: Man power ID, Man Power Update Array
OUTPUT 	: Man Power ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_man_power_rate($vendor_id,$man_power_update_data)
{
	if(array_key_exists("men_rate",$man_power_update_data))
	{
		$men_rate = $man_power_update_data["men_rate"];
	}
	else
	{
		$men_rate = "";
	}

	if(array_key_exists("women_rate",$man_power_update_data))
	{
		$women_rate = $man_power_update_data["women_rate"];
	}
	else
	{
		$women_rate = "";
	}

	if(array_key_exists("mason_rate",$man_power_update_data))
	{
		$mason_rate = $man_power_update_data["mason_rate"];
	}
	else
	{
		$mason_rate = "";
	}

	// Query
    $man_power_update_uquery_base = "update project_task_actual_manpower set";

	$man_power_update_uquery_set = "";

	$man_power_update_uquery_where = " where project_task_actual_manpower_agency = :vendor_id";

	$man_power_update_udata = array(":vendor_id"=>$vendor_id);

	$filter_count = 0;

	if($men_rate != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_men_rate = :men_rate,";
		$man_power_update_udata[":men_rate"] = $men_rate;
		$filter_count++;
	}

	if($women_rate != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_women_rate = :women_rate,";
		$man_power_update_udata[":women_rate"] = $women_rate;
		$filter_count++;
	}

	if($mason_rate != "")
	{
		$man_power_update_uquery_set = $man_power_update_uquery_set." project_task_actual_manpower_mason_rate = :mason_rate,";
		$man_power_update_udata[":mason_rate"] = $mason_rate;
		$filter_count++;
	}



	if($filter_count > 0)
	{
		$man_power_update_uquery_set = trim($man_power_update_uquery_set,',');
	}

	$man_power_update_uquery = $man_power_update_uquery_base.$man_power_update_uquery_set.$man_power_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $man_power_update_ustatement = $dbConnection->prepare($man_power_update_uquery);

        $man_power_update_ustatement -> execute($man_power_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $vendor_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Task Planning
INPUT 	: Task ID, Measurment, No of Roads, No of Object, Total Days, Object Type, Parallel Road Work, Planning Start Date, Planning End Date, Remarks, Added By
OUTPUT 	: Planning ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_task_planning($project_id,$task_id,$measurment,$no_of_roads,$no_of_object,$total_days,$object_type,$machine_type,$uom,$rate,$per_day_out,$planning_start_date,$planning_end_date,$remarks,$added_by)
{
	// Query
   $project_task_planning_iquery = "insert into project_task_planning
   (project_task_planning_project_id,project_task_planning_task_id,project_task_planning_measurment,project_task_planning_no_of_roads,project_task_planning_no_of_object,
   project_task_planning_total_days,project_task_planning_object_type,
   project_task_planning_machine_type,project_task_planning_uom,project_task_planning_rate,project_task_planning_per_day_out,project_task_planning_start_date,project_task_planning_end_date
   ,project_task_planning_active, project_task_planning_remarks,project_task_planning_added_by,project_task_planning_added_on) values (:project_id,:task_id,:measurment,:no_of_roads,:no_of_object,:total_days,:object_type,:machine_type,:uom,:rate,:per_day_out,:planning_start_date,:planning_end_date,:active,
   :remarks,:added_by,:added_on)";

    try
    {
        $dbConnection = get_conn_handle();
        $project_task_planning_istatement = $dbConnection->prepare($project_task_planning_iquery);

        // Data
        $project_task_planning_idata = array(':project_id'=>$project_id,':task_id'=>$task_id,':measurment'=>$measurment,':no_of_roads'=>$no_of_roads,':no_of_object'=>$no_of_object,':total_days'=>$total_days,':object_type'=>$object_type,':machine_type'=>$machine_type,':uom'=>$uom,':rate'=>$rate,':per_day_out'=>$per_day_out,':planning_start_date'=>$planning_start_date,':planning_end_date'=>$planning_end_date,':active'=>'1',
		':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $project_task_planning_istatement->execute($project_task_planning_idata);
		$project_task_planning_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_task_planning_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project Task Planning list
INPUT 	: Planning ID, Task ID, Measurment, No of Roads, No of Object, Total Days, Object Type, Parallel Road Work, Planning Start Date, Planning End Date, Remarks, Added By,
          Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Task Planning
BY 		: Lakshmi
*/
function db_get_project_task_planning($project_task_planning_search_data)
{

	if(array_key_exists("planning_id",$project_task_planning_search_data))
	{
		$planning_id = $project_task_planning_search_data["planning_id"];
	}
	else
	{
		$planning_id= "";
	}

	if(array_key_exists("task_id",$project_task_planning_search_data))
	{
		$task_id = $project_task_planning_search_data["task_id"];
	}
	else
	{
		$task_id = "";
	}

	if(array_key_exists("measurment",$project_task_planning_search_data))
	{
		$measurment = $project_task_planning_search_data["measurment"];
	}
	else
	{
		$measurment = "";
	}

	if(array_key_exists("no_of_roads",$project_task_planning_search_data))
	{
		$no_of_roads = $project_task_planning_search_data["no_of_roads"];
	}
	else
	{
		$no_of_roads = "";
	}

	if(array_key_exists("no_of_object",$project_task_planning_search_data))
	{
		$no_of_object = $project_task_planning_search_data["no_of_object"];
	}
	else
	{
		$no_of_object = "";
	}

	if(array_key_exists("total_days",$project_task_planning_search_data))
	{
		$total_days = $project_task_planning_search_data["total_days"];
	}
	else
	{
		$total_days = "";
	}

	if(array_key_exists("object_type",$project_task_planning_search_data))
	{
		$object_type = $project_task_planning_search_data["object_type"];
	}
	else
	{
		$object_type = "";
	}

	if(array_key_exists("machine_type",$project_task_planning_search_data))
	{
		$machine_type = $project_task_planning_search_data["machine_type"];
	}
	else
	{
		$machine_type = "";
	}

	if(array_key_exists("planning_start_date",$project_task_planning_search_data))
	{
		$planning_start_date = $project_task_planning_search_data["planning_start_date"];
	}
	else
	{
		$planning_start_date = "";
	}

	if(array_key_exists("planning_end_date",$project_task_planning_search_data))
	{
		$planning_end_date = $project_task_planning_search_data["planning_end_date"];
	}
	else
	{
		$planning_end_date = "";
	}

	if(array_key_exists("active",$project_task_planning_search_data))
	{
		$active = $project_task_planning_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_task_planning_search_data))
	{
		$added_by = $project_task_planning_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_task_planning_search_data))
	{
		$start_date= $project_task_planning_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("planned_start_date",$project_task_planning_search_data))
	{
		$planned_start_date= $project_task_planning_search_data["planned_start_date"];
	}
	else
	{
		$planned_start_date= "";
	}

	if(array_key_exists("planned_end_date",$project_task_planning_search_data))
	{
		$planned_end_date= $project_task_planning_search_data["planned_end_date"];
	}
	else
	{
		$planned_end_date= "";
	}

	if(array_key_exists("end_date",$project_task_planning_search_data))
	{
		$end_date= $project_task_planning_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	if(array_key_exists("project_id",$project_task_planning_search_data))
	{
		$project_id= $project_task_planning_search_data["project_id"];
	}
	else
	{
		$project_id= "";
	}

	if(array_key_exists("process_id",$project_task_planning_search_data))
	{
		$process_id= $project_task_planning_search_data["process_id"];
	}
	else
	{
		$process_id= "";
	}

	if(array_key_exists("asc_sort",$project_task_planning_search_data))
	{
		$asc_sort = $project_task_planning_search_data["asc_sort"];
	}
	else
	{
		$asc_sort = "";
	}

	if(array_key_exists("desc_sort",$project_task_planning_search_data))
	{
		$desc_sort = $project_task_planning_search_data["desc_sort"];
	}
	else
	{
		$desc_sort = "";
	}

	$get_project_task_planning_list_squery_base = "select * from project_task_planning PTP inner join users U on U.user_id = PTP.project_task_planning_added_by inner join project_plan_process_task PPT on PPT.project_process_task_id= PTP.project_task_planning_task_id inner join project_plan_process PPP on PPP.project_plan_process_id= PPT.project_process_id inner join project_plan PP on PP.project_plan_id= PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id=PP.project_plan_project_id left outer join project_site_location_mapping_master PSLMM on PSLMM.project_site_location_mapping_master_id=PTP.project_task_planning_no_of_roads inner join project_process_master PPM on PPM.project_process_master_id=PPP.project_plan_process_name inner join project_task_master PTM on PTM.project_task_master_id=PPT.project_process_task_type left outer join project_object_output_master POOM on POOM.project_object_output_master_id=PTP.project_task_planning_object_type left outer join project_machine_type_master PMTM on PMTM.project_machine_type_master_id=PTP.project_task_planning_machine_type left outer join project_cw_master PCM on PCM.project_cw_master_id=PTP.project_task_planning_machine_type left outer join project_uom_master PUM on PUM.project_uom_id=PTP.project_task_planning_uom";

	$get_project_task_planning_list_squery_where = "";
	$get_project_task_planning_list_squery_order = "";

	$filter_count = 0;
	// Data
	$get_project_task_planning_list_sdata = array();

	if($planning_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." where project_task_planning_id = :planning_id";
		}
		else
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and project_task_planning_id = :planning_id";
		}

		// Data
		$get_project_task_planning_list_sdata[':planning_id'] = $planning_id;

		$filter_count++;
	}

	if($task_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." where project_task_planning_task_id = :task_id";
		}
		else
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and project_task_planning_task_id = :task_id";
		}

		// Data
		$get_project_task_planning_list_sdata[':task_id'] = $task_id;

		$filter_count++;
	}

	if($measurment != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." where project_task_planning_measurment = :measurment";
		}
		else
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and project_task_planning_measurment = :measurment";
		}

		// Data
		$get_project_task_planning_list_sdata[':measurment']  = $measurment;

		$filter_count++;
	}

	if($no_of_roads != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." where project_task_planning_no_of_roads = :no_of_roads";
		}
		else
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and project_task_planning_no_of_roads = :no_of_roads";
		}

		// Data
		$get_project_task_planning_list_sdata[':no_of_roads']  = $no_of_roads;

		$filter_count++;
	}

	if($no_of_object != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." where project_task_planning_no_of_object = :no_of_object";
		}
		else
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and project_task_planning_no_of_object = :no_of_object";
		}

		// Data
		$get_project_task_planning_list_sdata[':no_of_object']  = $no_of_object;

		$filter_count++;
	}

	if($total_days != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." where project_task_planning_total_days = :total_days";
		}
		else
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and project_task_planning_total_days = :total_days";
		}

		// Data
		$get_project_task_planning_list_sdata[':total_days']  = $total_days;

		$filter_count++;
	}

	if($object_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." where project_task_planning_object_type = :object_type";
		}
		else
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and project_task_planning_object_type = :object_type";
		}

		// Data
		$get_project_task_planning_list_sdata[':object_type']  = $object_type;

		$filter_count++;
	}

	if($machine_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." where project_task_planning_machine_type = :machine_type";
		}
		else
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and project_task_planning_machine_type = :machine_type";
		}

		// Data
		$get_project_task_planning_list_sdata[':machine_type']  = $machine_type;

		$filter_count++;
	}

	if($planned_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." where project_task_planning_end_date <= :planned_end_date";
		}
		else
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and project_task_planning_end_date <= :planned_end_date";
		}

		// Data
		$get_project_task_planning_list_sdata[':planned_end_date']  = $planned_end_date;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." where project_task_planning_active = :active";
		}
		else
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and project_task_planning_active = :active";
		}

		// Data
		$get_project_task_planning_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." where project_task_planning_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and project_task_planning_added_by = :added_by";
		}

		//Data
		$get_project_task_planning_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." where project_task_planning_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and project_task_planning_added_on >= :start_date";
		}

		//Data
		$get_project_task_planning_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}

	if($planned_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." where project_task_planning_start_date >= :planned_start_date";
		}
		else
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and project_task_planning_start_date >= :planned_start_date";
		}

		//Data
		$get_project_task_planning_list_sdata[':planned_start_date']  = $planned_start_date;

		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." where project_task_planning_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and project_task_planning_added_on <= :end_date";
		}

		//Data
		$get_project_task_planning_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." where PP.project_plan_project_id = :project_id";
		}
		else
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and PP.project_plan_project_id = :project_id";
		}

		//Data
		$get_project_task_planning_list_sdata['project_id']  = $project_id;

		$filter_count++;
	}

	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." where PPP.project_plan_process_name = :process_id";
		}
		else
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and PPP.project_plan_process_name = :process_id";
		}

		//Data
		$get_project_task_planning_list_sdata['process_id']  = $process_id;

		$filter_count++;
	}
	if($asc_sort != "")
	{
		$get_project_task_planning_list_squery_order = " order by project_task_planning_id ASC";
	}
	if($desc_sort != "")
	{
		$get_project_task_planning_list_squery_order = " order by project_task_planning_id DESC";
	}
  else {
    $get_project_task_planning_list_squery_order = " order by project_process_master_order ASC,project_task_master_order ASC,project_site_location_mapping_master_order ASC";
  }
	$get_project_task_planning_list_squery = $get_project_task_planning_list_squery_base.$get_project_task_planning_list_squery_where.$get_project_task_planning_list_squery_order;



	try
	{
		$dbConnection = get_conn_handle();

		$get_project_task_planning_list_sstatement = $dbConnection->prepare($get_project_task_planning_list_squery);

		$get_project_task_planning_list_sstatement -> execute($get_project_task_planning_list_sdata);

		$get_project_task_planning_list_sdetails = $get_project_task_planning_list_sstatement -> fetchAll();

		if(FALSE === $get_project_task_planning_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_task_planning_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_task_planning_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

 /*
PURPOSE : To update Project Task Planning
INPUT 	: Planning ID, Project Task Planning Update Array
OUTPUT 	: Planning ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_task_planning($planning_id,$project_id,$project_task_planning_update_data)
{

	if(array_key_exists("task_id",$project_task_planning_update_data))
	{
		$task_id = $project_task_planning_update_data["task_id"];
	}
	else
	{
		$task_id = "";
	}

	if(array_key_exists("measurment",$project_task_planning_update_data))
	{
		$measurment = $project_task_planning_update_data["measurment"];
	}
	else
	{
		$measurment = "";
	}

	if(array_key_exists("no_of_roads",$project_task_planning_update_data))
	{
		$no_of_roads = $project_task_planning_update_data["no_of_roads"];
	}
	else
	{
		$no_of_roads = "";
	}

	if(array_key_exists("no_of_object",$project_task_planning_update_data))
	{
		$no_of_object = $project_task_planning_update_data["no_of_object"];
	}
	else
	{
		$no_of_object = "";
	}

	if(array_key_exists("total_days",$project_task_planning_update_data))
	{
		$total_days = $project_task_planning_update_data["total_days"];
	}
	else
	{
		$total_days = "";
	}

	if(array_key_exists("object_type",$project_task_planning_update_data))
	{
		$object_type = $project_task_planning_update_data["object_type"];
	}
	else
	{
		$object_type = "";
	}

	if(array_key_exists("machine_type",$project_task_planning_update_data))
	{
		$machine_type = $project_task_planning_update_data["machine_type"];
	}
	else
	{
		$machine_type = "";
	}

	if(array_key_exists("plan_start_date",$project_task_planning_update_data))
	{
		$plan_start_date = $project_task_planning_update_data["plan_start_date"];
	}
	else
	{
		$plan_start_date = "";
	}

	if(array_key_exists("plan_end_date",$project_task_planning_update_data))
	{
		$plan_end_date = $project_task_planning_update_data["plan_end_date"];
	}
	else
	{
		$plan_end_date = "";
	}

	if(array_key_exists("active",$project_task_planning_update_data))
	{
		$active = $project_task_planning_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_task_planning_update_data))
	{
		$remarks = $project_task_planning_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_task_planning_update_data))
	{
		$added_by = $project_task_planning_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_task_planning_update_data))
	{
		$added_on = $project_task_planning_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	if(array_key_exists("uom",$project_task_planning_update_data))
	{
		$uom = $project_task_planning_update_data["uom"];
	}
	else
	{
		$uom = "";
	}

	if(array_key_exists("total_days",$project_task_planning_update_data))
	{
		$total_days = $project_task_planning_update_data["total_days"];
	}
	else
	{
		$total_days = "";
	}

	if(array_key_exists("per_day_out",$project_task_planning_update_data))
	{
		$per_day_out = $project_task_planning_update_data["per_day_out"];
	}
	else
	{
		$per_day_out = "";
	}


	// Query
    $project_task_planning_update_uquery_base = "update project_task_planning PTP inner join users U on U.user_id = PTP.project_task_planning_added_by inner join project_plan_process_task PPT on PPT.project_process_task_id= PTP.project_task_planning_task_id inner join project_plan_process PPP on PPP.project_plan_process_id= PPT.project_process_id inner join project_plan PP on PP.project_plan_id= PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id=PP.project_plan_project_id set";

	$project_task_planning_update_uquery_set = "";

	$project_task_planning_update_uquery_where = " where project_task_planning_id = :planning_id or PP.project_plan_project_id= :project_id";

	$project_task_planning_update_udata = array(":planning_id"=>$planning_id,"project_id"=>$project_id);

	$filter_count = 0;

	if($task_id != "")
	{
		$project_task_planning_update_uquery_set = $project_task_planning_update_uquery_set." project_task_planning_task_id = :task_id,";
		$project_task_planning_update_udata[":task_id"] = $task_id;
		$filter_count++;
	}

	if($measurment != "")
	{
		$project_task_planning_update_uquery_set = $project_task_planning_update_uquery_set." project_task_planning_measurment = :measurment,";
		$project_task_planning_update_udata[":measurment"] = $measurment;
		$filter_count++;
	}

	if($no_of_roads != "")
	{
		$project_task_planning_update_uquery_set = $project_task_planning_update_uquery_set." project_task_planning_no_of_roads = :no_of_roads,";
		$project_task_planning_update_udata[":no_of_roads"] = $no_of_roads;
		$filter_count++;
	}

	if($no_of_object != "")
	{
		$project_task_planning_update_uquery_set = $project_task_planning_update_uquery_set." project_task_planning_no_of_object = :no_of_object,";
		$project_task_planning_update_udata[":no_of_object"] = $no_of_object;
		$filter_count++;
	}

	if($object_type != "")
	{
		$project_task_planning_update_uquery_set = $project_task_planning_update_uquery_set." project_task_planning_object_type = :object_type,";
		$project_task_planning_update_udata[":object_type"] = $object_type;
		$filter_count++;
	}

	if($machine_type != "")
	{
		$project_task_planning_update_uquery_set = $project_task_planning_update_uquery_set." project_task_planning_machine_type = :machine_type,";
		$project_task_planning_update_udata[":machine_type"] = $machine_type;
		$filter_count++;
	}

	if($plan_start_date != "")
	{
		$project_task_planning_update_uquery_set = $project_task_planning_update_uquery_set." project_task_planning_start_date = :plan_start_date,";
		$project_task_planning_update_udata[":plan_start_date"] = $plan_start_date;
		$filter_count++;
	}

	if($plan_end_date != "")
	{
		$project_task_planning_update_uquery_set = $project_task_planning_update_uquery_set." project_task_planning_end_date = :plan_end_date,";
		$project_task_planning_update_udata[":plan_end_date"] = $plan_end_date;
		$filter_count++;
	}

	if($active != "")
	{
		$project_task_planning_update_uquery_set = $project_task_planning_update_uquery_set." project_task_planning_active = :active,";
		$project_task_planning_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_task_planning_update_uquery_set = $project_task_planning_update_uquery_set." project_task_planning_remarks = :remarks,";
		$project_task_planning_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_task_planning_update_uquery_set = $project_task_planning_update_uquery_set." project_task_planning_added_by = :added_by,";
		$project_task_planning_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_task_planning_update_uquery_set = $project_task_planning_update_uquery_set." project_task_planning_added_on = :added_on,";
		$project_task_planning_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($uom != "")
	{
		$project_task_planning_update_uquery_set = $project_task_planning_update_uquery_set." project_task_planning_uom = :uom,";
		$project_task_planning_update_udata[":uom"] = $uom;
		$filter_count++;
	}

	if($total_days != "")
	{
		$project_task_planning_update_uquery_set = $project_task_planning_update_uquery_set." project_task_planning_total_days = :total_days,";
		$project_task_planning_update_udata[":total_days"] = $total_days;
		$filter_count++;
	}

	if($per_day_out != "")
	{
		$project_task_planning_update_uquery_set = $project_task_planning_update_uquery_set." project_task_planning_per_day_out = :per_day_out,";
		$project_task_planning_update_udata[":per_day_out"] = $per_day_out;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_task_planning_update_uquery_set = trim($project_task_planning_update_uquery_set,',');
	}

	$project_task_planning_update_uquery = $project_task_planning_update_uquery_base.$project_task_planning_update_uquery_set.$project_task_planning_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $project_task_planning_update_ustatement = $dbConnection->prepare($project_task_planning_update_uquery);

        $project_task_planning_update_ustatement -> execute($project_task_planning_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $planning_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To add new Project Task planning_shadow
INPUT 	: Task ID, Measurment, No of Roads, No of Object, Total Days, Object Type, Parallel Road Work, planning_shadow Start Date, planning_shadow End Date, Remarks, Added By
OUTPUT 	: planning_shadow ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_task_planning_shadow($task_id,$old_msmrt,$new_msmrt,$old_end_date,$new_end_date,$old_start_date,$new_start_date,$changed_by)
{
	// Query
   $project_task_planning_shadow_iquery = "insert into project_task_planning_shadow
   (project_task_planning_shadow_planning_id,project_task_planning_shadow_old_msmrt,project_task_planning_shadow_new_msmrt,project_task_planning_shadow_old_end_date,project_task_planning_shadow_new_end_date	,
   project_task_planning_shadow_old_start_date,project_task_planning_shadow_new_start_date,project_task_planning_shadow_changed_by,project_task_planning_shadow_changed_on) values (:task_id,:old_msmrt,:new_msmrt,:old_end_date,:new_end_date,:old_start_date,:new_start_date,:changed_by,:changed_on)";
    try
    {
        $dbConnection = get_conn_handle();
        $project_task_planning_shadow_istatement = $dbConnection->prepare($project_task_planning_shadow_iquery);

        // Data
        $project_task_planning_shadow_idata = array(':task_id'=>$task_id,':old_msmrt'=>$old_msmrt,':new_msmrt'=>$new_msmrt,':old_end_date'=>$old_end_date,':new_end_date'=>$new_end_date,':old_start_date'=>$old_start_date,':new_start_date'=>$new_start_date,':changed_by'=>$changed_by,':changed_on'=>date("Y-m-d H:i:s"));


		$dbConnection->beginTransaction();
        $project_task_planning_shadow_istatement->execute($project_task_planning_shadow_idata);
		$project_task_planning_shadow_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_task_planning_shadow_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project Task planning_shadow list
INPUT 	: planning_shadow ID, Task ID, Measurment, No of Roads, No of Object, Total Days, Object Type, Parallel Road Work, planning_shadow Start Date, planning_shadow End Date, Remarks, Added By,
          Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Task planning_shadow
BY 		: Lakshmi
*/
function db_get_project_task_planning_shadow($project_task_planning_shadow_search_data)
{

	if(array_key_exists("planning_shadow_id",$project_task_planning_shadow_search_data))
	{
		$planning_shadow_id = $project_task_planning_shadow_search_data["planning_shadow_id"];
	}
	else
	{
		$planning_shadow_id= "";
	}

	if(array_key_exists("task_id",$project_task_planning_shadow_search_data))
	{
		$task_id = $project_task_planning_shadow_search_data["task_id"];
	}
	else
	{
		$task_id = "";
	}

	if(array_key_exists("measurment",$project_task_planning_shadow_search_data))
	{
		$measurment = $project_task_planning_shadow_search_data["measurment"];
	}
	else
	{
		$measurment = "";
	}

	if(array_key_exists("no_of_roads",$project_task_planning_shadow_search_data))
	{
		$no_of_roads = $project_task_planning_shadow_search_data["no_of_roads"];
	}
	else
	{
		$no_of_roads = "";
	}

	if(array_key_exists("no_of_object",$project_task_planning_shadow_search_data))
	{
		$no_of_object = $project_task_planning_shadow_search_data["no_of_object"];
	}
	else
	{
		$no_of_object = "";
	}

	if(array_key_exists("total_days",$project_task_planning_shadow_search_data))
	{
		$total_days = $project_task_planning_shadow_search_data["total_days"];
	}
	else
	{
		$total_days = "";
	}

	if(array_key_exists("object_type",$project_task_planning_shadow_search_data))
	{
		$object_type = $project_task_planning_shadow_search_data["object_type"];
	}
	else
	{
		$object_type = "";
	}

	if(array_key_exists("machine_type",$project_task_planning_shadow_search_data))
	{
		$machine_type = $project_task_planning_shadow_search_data["machine_type"];
	}
	else
	{
		$machine_type = "";
	}

	if(array_key_exists("planning_shadow_start_date",$project_task_planning_shadow_search_data))
	{
		$planning_shadow_start_date = $project_task_planning_shadow_search_data["planning_shadow_start_date"];
	}
	else
	{
		$planning_shadow_start_date = "";
	}

	if(array_key_exists("planning_shadow_end_date",$project_task_planning_shadow_search_data))
	{
		$planning_shadow_end_date = $project_task_planning_shadow_search_data["planning_shadow_end_date"];
	}
	else
	{
		$planning_shadow_end_date = "";
	}

	if(array_key_exists("active",$project_task_planning_shadow_search_data))
	{
		$active = $project_task_planning_shadow_search_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("added_by",$project_task_planning_shadow_search_data))
	{
		$added_by = $project_task_planning_shadow_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_task_planning_shadow_search_data))
	{
		$start_date= $project_task_planning_shadow_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_task_planning_shadow_search_data))
	{
		$end_date= $project_task_planning_shadow_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_project_task_planning_shadow_list_squery_base = "select * from project_task_planning_shadow PP inner join users U on U.user_id = PP.project_task_planning_shadow_added_by";

	$get_project_task_planning_shadow_list_squery_where = "";

	$filter_count = 0;
	// Data
	$get_project_task_planning_shadow_list_sdata = array();

	if($planning_shadow_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." where project_task_planning_shadow_id = :planning_shadow_id";
		}
		else
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." and project_task_planning_shadow_id = :planning_shadow_id";
		}

		// Data
		$get_project_task_planning_shadow_list_sdata[':planning_shadow_id'] = $planning_shadow_id;

		$filter_count++;
	}

	if($task_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." where project_task_planning_shadow_task_id = :task_id";
		}
		else
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." and project_task_planning_shadow_task_id = :task_id";
		}

		// Data
		$get_project_task_planning_shadow_list_sdata[':task_id'] = $task_id;

		$filter_count++;
	}

	if($measurment != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." where project_task_planning_shadow_measurment = :measurment";
		}
		else
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." and project_task_planning_shadow_measurment = :measurment";
		}

		// Data
		$get_project_task_planning_shadow_list_sdata[':measurment']  = $measurment;

		$filter_count++;
	}

	if($no_of_roads != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." where project_task_planning_shadow_no_of_roads = :no_of_roads";
		}
		else
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." and project_task_planning_shadow_no_of_roads = :no_of_roads";
		}

		// Data
		$get_project_task_planning_shadow_list_sdata[':no_of_roads']  = $no_of_roads;

		$filter_count++;
	}

	if($no_of_object != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." where project_task_planning_shadow_no_of_object = :no_of_object";
		}
		else
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." and project_task_planning_shadow_no_of_object = :no_of_object";
		}

		// Data
		$get_project_task_planning_shadow_list_sdata[':no_of_object']  = $no_of_object;

		$filter_count++;
	}

	if($total_days != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." where project_task_planning_shadow_total_days = :total_days";
		}
		else
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." and project_task_planning_shadow_total_days = :total_days";
		}

		// Data
		$get_project_task_planning_shadow_list_sdata[':total_days']  = $total_days;

		$filter_count++;
	}

	if($object_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." where project_task_planning_shadow_object_type = :object_type";
		}
		else
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." and project_task_planning_shadow_object_type = :object_type";
		}

		// Data
		$get_project_task_planning_shadow_list_sdata[':object_type']  = $object_type;

		$filter_count++;
	}

	if($machine_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." where project_task_planning_shadow_machine_type = :machine_type";
		}
		else
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." and project_task_planning_shadow_machine_type = :machine_type";
		}

		// Data
		$get_project_task_planning_shadow_list_sdata[':machine_type']  = $machine_type;

		$filter_count++;
	}

	if($planning_shadow_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." where project_task_planning_shadow_start_date = :planning_shadow_start_date";
		}
		else
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." and project_task_planning_shadow_start_date = :planning_shadow_start_date";
		}

		// Data
		$get_project_task_planning_shadow_list_sdata[':planning_shadow_start_date']  = $planning_shadow_start_date;

		$filter_count++;
	}

	if($planning_shadow_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." where project_task_planning_shadow_end_date = :planning_shadow_end_date";
		}
		else
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." and project_task_planning_shadow_end_date = :planning_shadow_end_date";
		}

		// Data
		$get_project_task_planning_shadow_list_sdata[':planning_shadow_end_date']  = $planning_shadow_end_date;

		$filter_count++;
	}

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." where project_task_planning_shadow_active = :active";
		}
		else
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." and project_task_planning_shadow_active = :active";
		}

		// Data
		$get_project_task_planning_shadow_list_sdata[':active']  = $active;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." where project_task_planning_shadow_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." and project_task_planning_shadow_added_by = :added_by";
		}

		//Data
		$get_project_task_planning_shadow_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." where project_task_planning_shadow_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." and project_task_planning_shadow_added_on >= :start_date";
		}

		//Data
		$get_project_task_planning_shadow_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." where project_task_planning_shadow_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_task_planning_shadow_list_squery_where = $get_project_task_planning_shadow_list_squery_where." and project_task_planning_shadow_added_on <= :end_date";
		}

		//Data
		$get_project_task_planning_shadow_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	$get_project_task_planning_shadow_list_squery = $get_project_task_planning_shadow_list_squery_base.$get_project_task_planning_shadow_list_squery_where;

	try
	{
		$dbConnection = get_conn_handle();

		$get_project_task_planning_shadow_list_sstatement = $dbConnection->prepare($get_project_task_planning_shadow_list_squery);

		$get_project_task_planning_shadow_list_sstatement -> execute($get_project_task_planning_shadow_list_sdata);

		$get_project_task_planning_shadow_list_sdetails = $get_project_task_planning_shadow_list_sstatement -> fetchAll();

		if(FALSE === $get_project_task_planning_shadow_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_task_planning_shadow_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_task_planning_shadow_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

 /*
PURPOSE : To update Project Task planning_shadow
INPUT 	: planning_shadow ID, Project Task planning_shadow Update Array
OUTPUT 	: planning_shadow ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_task_planning_shadow($planning_shadow_id,$project_task_planning_shadow_update_data)
{

	if(array_key_exists("task_id",$project_task_planning_shadow_update_data))
	{
		$task_id = $project_task_planning_shadow_update_data["task_id"];
	}
	else
	{
		$task_id = "";
	}

	if(array_key_exists("measurment",$project_task_planning_shadow_update_data))
	{
		$measurment = $project_task_planning_shadow_update_data["measurment"];
	}
	else
	{
		$measurment = "";
	}

	if(array_key_exists("no_of_roads",$project_task_planning_shadow_update_data))
	{
		$no_of_roads = $project_task_planning_shadow_update_data["no_of_roads"];
	}
	else
	{
		$no_of_roads = "";
	}

	if(array_key_exists("no_of_object",$project_task_planning_shadow_update_data))
	{
		$no_of_object = $project_task_planning_shadow_update_data["no_of_object"];
	}
	else
	{
		$no_of_object = "";
	}

	if(array_key_exists("total_days",$project_task_planning_shadow_update_data))
	{
		$total_days = $project_task_planning_shadow_update_data["total_days"];
	}
	else
	{
		$total_days = "";
	}

	if(array_key_exists("object_type",$project_task_planning_shadow_update_data))
	{
		$object_type = $project_task_planning_shadow_update_data["object_type"];
	}
	else
	{
		$object_type = "";
	}

	if(array_key_exists("machine_type",$project_task_planning_shadow_update_data))
	{
		$machine_type = $project_task_planning_shadow_update_data["machine_type"];
	}
	else
	{
		$machine_type = "";
	}

	if(array_key_exists("planning_shadow_start_date",$project_task_planning_shadow_update_data))
	{
		$planning_shadow_start_date = $project_task_planning_shadow_update_data["planning_shadow_start_date"];
	}
	else
	{
		$planning_shadow_start_date = "";
	}

	if(array_key_exists("planning_shadow_end_date",$project_task_planning_shadow_update_data))
	{
		$planning_shadow_end_date = $project_task_planning_shadow_update_data["planning_shadow_end_date"];
	}
	else
	{
		$planning_shadow_end_date = "";
	}

	if(array_key_exists("active",$project_task_planning_shadow_update_data))
	{
		$active = $project_task_planning_shadow_update_data["active"];
	}
	else
	{
		$active = "";
	}
	if(array_key_exists("remarks",$project_task_planning_shadow_update_data))
	{
		$remarks = $project_task_planning_shadow_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}

	if(array_key_exists("added_by",$project_task_planning_shadow_update_data))
	{
		$added_by = $project_task_planning_shadow_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_task_planning_shadow_update_data))
	{
		$added_on = $project_task_planning_shadow_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_task_planning_shadow_update_uquery_base = "update project_task_planning_shadow set";

	$project_task_planning_shadow_update_uquery_set = "";

	$project_task_planning_shadow_update_uquery_where = " where project_task_planning_shadow_id = :planning_shadow_id";

	$project_task_planning_shadow_update_udata = array(":planning_shadow_id"=>$planning_shadow_id);

	$filter_count = 0;

	if($task_id != "")
	{
		$project_task_planning_shadow_update_uquery_set = $project_task_planning_shadow_update_uquery_set." project_task_planning_shadow_task_id = :task_id,";
		$project_task_planning_shadow_update_udata[":task_id"] = $task_id;
		$filter_count++;
	}

	if($measurment != "")
	{
		$project_task_planning_shadow_update_uquery_set = $project_task_planning_shadow_update_uquery_set." project_task_planning_shadow_measurment = :measurment,";
		$project_task_planning_shadow_update_udata[":measurment"] = $measurment;
		$filter_count++;
	}

	if($no_of_roads != "")
	{
		$project_task_planning_shadow_update_uquery_set = $project_task_planning_shadow_update_uquery_set." project_task_planning_shadow_no_of_roads = :no_of_roads,";
		$project_task_planning_shadow_update_udata[":no_of_roads"] = $no_of_roads;
		$filter_count++;
	}

	if($no_of_object != "")
	{
		$project_task_planning_shadow_update_uquery_set = $project_task_planning_shadow_update_uquery_set." project_task_planning_shadow_no_of_object = :no_of_object,";
		$project_task_planning_shadow_update_udata[":no_of_object"] = $no_of_object;
		$filter_count++;
	}

	if($object_type != "")
	{
		$project_task_planning_shadow_update_uquery_set = $project_task_planning_shadow_update_uquery_set." project_task_planning_shadow_object_type = :object_type,";
		$project_task_planning_shadow_update_udata[":object_type"] = $object_type;
		$filter_count++;
	}

	if($machine_type != "")
	{
		$project_task_planning_shadow_update_uquery_set = $project_task_planning_shadow_update_uquery_set." project_task_planning_shadow_machine_type = :machine_type,";
		$project_task_planning_shadow_update_udata[":machine_type"] = $machine_type;
		$filter_count++;
	}

	if($plan_start_date != "")
	{
		$project_task_planning_shadow_update_uquery_set = $project_task_planning_shadow_update_uquery_set." project_task_planning_shadow_start_date = :plan_start_date,";
		$project_task_planning_shadow_update_udata[":plan_start_date"] = $plan_start_date;
		$filter_count++;
	}

	if($plan_end_date != "")
	{
		$project_task_planning_shadow_update_uquery_set = $project_task_planning_shadow_update_uquery_set." project_task_planning_shadow_end_date = :plan_end_date,";
		$project_task_planning_shadow_update_udata[":plan_end_date"] = $plan_end_date;
		$filter_count++;
	}

	if($active != "")
	{
		$project_task_planning_shadow_update_uquery_set = $project_task_planning_shadow_update_uquery_set." project_task_planning_shadow_active = :active,";
		$project_task_planning_shadow_update_udata[":active"] = $active;
		$filter_count++;
	}

	if($remarks != "")
	{
		$project_task_planning_shadow_update_uquery_set = $project_task_planning_shadow_update_uquery_set." project_task_planning_shadow_remarks = :remarks,";
		$project_task_planning_shadow_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_task_planning_shadow_update_uquery_set = $project_task_planning_shadow_update_uquery_set." project_task_planning_shadow_added_by = :added_by,";
		$project_task_planning_shadow_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_task_planning_shadow_update_uquery_set = $project_task_planning_shadow_update_uquery_set." project_task_planning_shadow_added_on = :added_on,";
		$project_task_planning_shadow_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_task_planning_shadow_update_uquery_set = trim($project_task_planning_shadow_update_uquery_set,',');
	}

	$project_task_planning_shadow_update_uquery = $project_task_planning_shadow_update_uquery_base.$project_task_planning_shadow_update_uquery_set.$project_task_planning_shadow_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();

        $project_task_planning_shadow_update_ustatement = $dbConnection->prepare($project_task_planning_shadow_update_uquery);

        $project_task_planning_shadow_update_ustatement -> execute($project_task_planning_shadow_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $planning_shadow_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	return $return;
}

/*
PURPOSE : To add new Project Plan Wish
INPUT 	: Project ID, Days, Plan Start Date, Lock, Locked By, Locked On, Added By
OUTPUT 	: Plan Wish ID, success or failure message
BY 		: Lakshmi
*/
function db_add_project_plan_wish($project_id,$days,$plan_start_date,$lock,$locked_by,$locked_on,$added_by)
{
	// Query
   $project_plan_wish_iquery = "insert into project_plan_wish
   (project_plan_wish_project_id,project_plan_wish_days,project_plan_wish_start_date,project_plan_wish_lock,project_plan_wish_locked_by,project_plan_wish_locked_on,
   project_plan_wish_added_by,project_plan_wish_added_on) values (:project_id,:days,:plan_start_date,:lock,:locked_by,:locked_on,:added_by,:added_on)";


    try
    {
        $dbConnection = get_conn_handle();
        $project_plan_wish_istatement = $dbConnection->prepare($project_plan_wish_iquery);

        // Data
       $project_plan_wish_idata = array(':project_id'=>$project_id,':days'=>$days,':plan_start_date'=>$plan_start_date,':lock'=>$lock,':locked_by'=>$locked_by,
	   ':locked_on'=>$locked_on,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));

		$dbConnection->beginTransaction();
        $project_plan_wish_istatement->execute($project_plan_wish_idata);
		$project_plan_wish_id = $dbConnection->lastInsertId();
		$dbConnection->commit();

        $return["status"] = SUCCESS;
		$return["data"]   = $project_plan_wish_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

    return $return;
}

/*
PURPOSE : To get Project Plan Wish List
INPUT 	: Plan Wish ID, Project ID, Days, Plan Start Date, Lock, Locked By, Locked On, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Plan Wish
BY 		: Lakshmi
*/
function db_get_project_plan_wish($project_plan_wish_search_data)
{
	if(array_key_exists("plan_wish_id",$project_plan_wish_search_data))
	{
		$plan_wish_id = $project_plan_wish_search_data["plan_wish_id"];
	}
	else
	{
		$plan_wish_id= "";
	}

	if(array_key_exists("project_id",$project_plan_wish_search_data))
	{
		$project_id = $project_plan_wish_search_data["project_id"];
	}
	else
	{
		$project_id = "";
	}

	if(array_key_exists("days",$project_plan_wish_search_data))
	{
		$days = $project_plan_wish_search_data["days"];
	}
	else
	{
		$days = "";
	}

	if(array_key_exists("plan_start_date",$project_plan_wish_search_data))
	{
		$plan_start_date = $project_plan_wish_search_data["plan_start_date"];
	}
	else
	{
		$plan_start_date = "";
	}

	if(array_key_exists("lock",$project_plan_wish_search_data))
	{
		$lock = $project_plan_wish_search_data["lock"];
	}
	else
	{
		$lock = "";
	}

	if(array_key_exists("locked_by",$project_plan_wish_search_data))
	{
		$locked_by = $project_plan_wish_search_data["locked_by"];
	}
	else
	{
		$locked_by = "";
	}

	if(array_key_exists("locked_on",$project_plan_wish_search_data))
	{
		$locked_on = $project_plan_wish_search_data["locked_on"];
	}
	else
	{
		$locked_on = "";
	}

	if(array_key_exists("added_by",$project_plan_wish_search_data))
	{
		$added_by = $project_plan_wish_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("start_date",$project_plan_wish_search_data))
	{
		$start_date= $project_plan_wish_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}

	if(array_key_exists("end_date",$project_plan_wish_search_data))
	{
		$end_date= $project_plan_wish_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}

	$get_project_plan_wish_list_squery_base = "select * from project_plan_wish";

	$get_project_plan_wish_list_squery_where = "";
	$filter_count = 0;

	// Data
	$get_project_plan_wish_list_sdata = array();

	if($plan_wish_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_wish_list_squery_where = $get_project_plan_wish_list_squery_where." where project_plan_wish_id = :plan_wish_id";
		}
		else
		{
			// Query
			$get_project_plan_wish_list_squery_where = $get_project_plan_wish_list_squery_where." and project_plan_wish_id = :plan_wish_id";
		}

		// Data
		$get_project_plan_wish_list_sdata[':plan_wish_id'] = $plan_wish_id;

		$filter_count++;
	}

	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_wish_list_squery_where = $get_project_plan_wish_list_squery_where." where project_plan_wish_project_id = :project_id";
		}
		else
		{
			// Query
			$get_project_plan_wish_list_squery_where = $get_project_plan_wish_list_squery_where." and project_plan_wish_project_id = :project_id";
		}

		// Data
		$get_project_plan_wish_list_sdata[':project_id'] = $project_id;

		$filter_count++;
	}

	if($days != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_wish_list_squery_where = $get_project_plan_wish_list_squery_where." where project_plan_wish_days = :days";
		}
		else
		{
			// Query
			$get_project_plan_wish_list_squery_where = $get_project_plan_wish_list_squery_where." and project_plan_wish_days = :days";
		}

		// Data
		$get_project_plan_wish_list_sdata[':days'] = $days;

		$filter_count++;
	}

	if($plan_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_wish_list_squery_where = $get_project_plan_wish_list_squery_where." where project_plan_wish_start_date = :plan_start_date";
		}
		else
		{
			// Query
			$get_project_plan_wish_list_squery_where = $get_project_plan_wish_list_squery_where." and project_plan_wish_start_date = :plan_start_date";
		}

		// Data
		$get_project_plan_wish_list_sdata[':plan_start_date'] = $plan_start_date;

		$filter_count++;
	}

	if($lock != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_wish_list_squery_where = $get_project_plan_wish_list_squery_where." where project_plan_wish_lock = :lock";
		}
		else
		{
			// Query
			$get_project_plan_wish_list_squery_where = $get_project_plan_wish_list_squery_where." and project_plan_wish_lock = :lock";
		}

		// Data
		$get_project_plan_wish_list_sdata[':lock'] = $lock;

		$filter_count++;
	}

	if($locked_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_wish_list_squery_where = $get_project_plan_wish_list_squery_where." where project_plan_wish_locked_by = :locked_by";
		}
		else
		{
			// Query
			$get_project_plan_wish_list_squery_where = $get_project_plan_wish_list_squery_where." and project_plan_wish_locked_by = :locked_by";
		}

		// Data
		$get_project_plan_wish_list_sdata[':locked_by'] = $locked_by;

		$filter_count++;
	}

	if($locked_on != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_wish_list_squery_where = $get_project_plan_wish_list_squery_where." where project_plan_wish_locked_on = :locked_on";
		}
		else
		{
			// Query
			$get_project_plan_wish_list_squery_where = $get_project_plan_wish_list_squery_where." and project_plan_wish_locked_on = :locked_on";
		}

		// Data
		$get_project_plan_wish_list_sdata[':locked_on'] = $locked_on;

		$filter_count++;
	}

	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_wish_list_squery_where = $get_project_plan_wish_list_squery_where." where project_plan_wish_added_by = :added_by";
		}
		else
		{
			// Query
			$get_project_plan_wish_list_squery_where = $get_project_plan_wish_list_squery_where." and project_plan_wish_added_by = :added_by";
		}

		//Data
		$get_project_plan_wish_list_sdata[':added_by']  = $added_by;

		$filter_count++;
	}

	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_wish_list_squery_where = $get_project_plan_wish_list_squery_where." where project_plan_wish_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_project_plan_wish_list_squery_where = $get_project_plan_wish_list_squery_where." and project_plan_wish_added_on >= :start_date";
		}

		//Data
		$get_project_plan_wish_list_sdata[':start_date']  = $start_date;

		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_plan_wish_list_squery_where = $get_project_plan_wish_list_squery_where." where project_plan_wish_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_project_plan_wish_list_squery_where = $get_project_plan_wish_list_squery_where." and project_plan_wish_added_on <= :end_date";
		}

		//Data
		$get_project_plan_wish_list_sdata['end_date']  = $end_date;

		$filter_count++;
	}

	$get_project_plan_wish_list_squery = $get_project_plan_wish_list_squery_base.$get_project_plan_wish_list_squery_where;

	try
	{
		$dbConnection = get_conn_handle();

		$get_project_plan_wish_list_sstatement = $dbConnection->prepare($get_project_plan_wish_list_squery);

		$get_project_plan_wish_list_sstatement -> execute($get_project_plan_wish_list_sdata);

		$get_project_plan_wish_list_sdetails = $get_project_plan_wish_list_sstatement -> fetchAll();

		if(FALSE === $get_project_plan_wish_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_plan_wish_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_plan_wish_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }

/*
PURPOSE : To update Project Plan Wish
INPUT 	: Plan Wish ID, Project Plan Wish Update Array
OUTPUT 	: Plan Wish ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_project_plan_wish($project_id,$project_plan_wish_update_data)
{
	if(array_key_exists("days",$project_plan_wish_update_data))
	{
		$days = $project_plan_wish_update_data["days"];
	}
	else
	{
		$days = "";
	}

	if(array_key_exists("plan_start_date",$project_plan_wish_update_data))
	{
		$plan_start_date = $project_plan_wish_update_data["plan_start_date"];
	}
	else
	{
		$plan_start_date = "";
	}

	if(array_key_exists("lock",$project_plan_wish_update_data))
	{
		$lock = $project_plan_wish_update_data["lock"];
	}
	else
	{
		$lock = "";
	}

	if(array_key_exists("locked_by",$project_plan_wish_update_data))
	{
		$locked_by = $project_plan_wish_update_data["locked_by"];
	}
	else
	{
		$locked_by = "";
	}

	if(array_key_exists("locked_on",$project_plan_wish_update_data))
	{
		$locked_on = $project_plan_wish_update_data["locked_on"];
	}
	else
	{
		$locked_on = "";
	}

	if(array_key_exists("added_by",$project_plan_wish_update_data))
	{
		$added_by = $project_plan_wish_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}

	if(array_key_exists("added_on",$project_plan_wish_update_data))
	{
		$added_on = $project_plan_wish_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}

	// Query
    $project_plan_wish_update_uquery_base = "update project_plan_wish set ";

	$project_plan_wish_update_uquery_set = "";

	$project_plan_wish_update_uquery_where = " where project_plan_wish_project_id = :project_id";

	$project_plan_wish_update_udata = array(":project_id"=>$project_id);

	$filter_count = 0;


	if($days != "")
	{
		$project_plan_wish_update_uquery_set = $project_plan_wish_update_uquery_set." project_plan_wish_days = :days,";
		$project_plan_wish_update_udata[":days"] = $days;
		$filter_count++;
	}

	if($plan_start_date != "")
	{
		$project_plan_wish_update_uquery_set = $project_plan_wish_update_uquery_set." project_plan_wish_start_date = :plan_start_date,";
		$project_plan_wish_update_udata[":plan_start_date"] = $plan_start_date;
		$filter_count++;
	}

	if($lock != "")
	{
		$project_plan_wish_update_uquery_set = $project_plan_wish_update_uquery_set." project_plan_wish_lock = :lock,";
		$project_plan_wish_update_udata[":lock"] = $lock;
		$filter_count++;
	}

	if($locked_by != "")
	{
		$project_plan_wish_update_uquery_set = $project_plan_wish_update_uquery_set." project_plan_wish_locked_by = :locked_by,";
		$project_plan_wish_update_udata[":locked_by"] = $locked_by;
		$filter_count++;
	}

	if($locked_on != "")
	{
		$project_plan_wish_update_uquery_set = $project_plan_wish_update_uquery_set." project_plan_wish_locked_on = :locked_on,";
		$project_plan_wish_update_udata[":locked_on"] = $locked_on;
		$filter_count++;
	}

	if($added_by != "")
	{
		$project_plan_wish_update_uquery_set = $project_plan_wish_update_uquery_set." project_uom_added_by = :added_by,";
		$project_plan_wish_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}

	if($added_on != "")
	{
		$project_plan_wish_update_uquery_set = $project_plan_wish_update_uquery_set." project_uom_added_on = :added_on,";
		$project_plan_wish_update_udata[":added_on"] = $added_on;
		$filter_count++;
	}

	if($filter_count > 0)
	{
		$project_plan_wish_update_uquery_set = trim($project_plan_wish_update_uquery_set,',');
	}

	$project_plan_wish_update_uquery = $project_plan_wish_update_uquery_base.$project_plan_wish_update_uquery_set.$project_plan_wish_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();

        $project_plan_wish_update_ustatement = $dbConnection->prepare($project_plan_wish_update_uquery);

        $project_plan_wish_update_ustatement -> execute($project_plan_wish_update_udata);

        $return["status"] = SUCCESS;
		$return["data"]   = $project_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }

	return $return;
}
/*
PURPOSE : To get Project Task Planning Min Start Date And Max End date
INPUT 	: Planning ID, Task ID, Measurment, No of Roads, No of Object, Total Days, Object Type, Parallel Road Work, Planning Start Date, Planning End Date, Remarks, Added By,
          Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Task Planning
BY 		: Sonakshi
*/
function db_get_project_task_planning_date($project_task_planning_search_data)
{

	if(array_key_exists("project_id",$project_task_planning_search_data))
	{
		$project_id = $project_task_planning_search_data["project_id"];
	}
	else
	{
		$project_id= "";
	}

	if(array_key_exists("min_date",$project_task_planning_search_data))
	{
		$min_date = $project_task_planning_search_data["min_date"];
	}
	else
	{
		$min_date = "";
	}

	if(array_key_exists("max_date",$project_task_planning_search_data))
	{
		$max_date = $project_task_planning_search_data["max_date"];
	}
	else
	{
		$max_date = "";
	}

	if(array_key_exists("status",$project_task_planning_search_data))
	{
		$status = $project_task_planning_search_data["status"];
	}
	else
	{
		$status = "";
	}

	switch($status)
	{
		case "min" :
		$get_project_task_planning_list_squery_base = "select  MIN(project_task_planning_start_date) as min_date from project_task_planning PTP inner join users U on U.user_id = PTP.project_task_planning_added_by inner join project_plan_process_task PPT on PPT.project_process_task_id= PTP.project_task_planning_task_id inner join project_plan_process PPP on PPP.project_plan_process_id= PPT.project_process_id inner join project_plan PP on PP.project_plan_id= PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id=PP.project_plan_project_id left outer join project_site_location_mapping_master PSLMM on PSLMM.project_site_location_mapping_master_id=PTP.project_task_planning_no_of_roads inner join project_process_master PPM on PPM.project_process_master_id=PPP.project_plan_process_name inner join project_task_master PTM on PTM.project_task_master_id=PPT.project_process_task_type left outer join project_object_output_master POOM on POOM.project_object_output_master_id=PTP.project_task_planning_object_type left outer join project_machine_type_master PMTM on PMTM.project_machine_type_master_id=PTP.project_task_planning_machine_type left outer join stock_unit_measure_master SUMM on SUMM.stock_unit_id=PTP.project_task_planning_uom left outer join project_cw_master PCM on PCM.project_cw_master_id=PTP.project_task_planning_machine_type";
		break;

		case "max" :
		$get_project_task_planning_list_squery_base = "select  MAX(project_task_planning_end_date) as max_date from project_task_planning PTP inner join users U on U.user_id = PTP.project_task_planning_added_by inner join project_plan_process_task PPT on PPT.project_process_task_id= PTP.project_task_planning_task_id inner join project_plan_process PPP on PPP.project_plan_process_id= PPT.project_process_id inner join project_plan PP on PP.project_plan_id= PPP.project_plan_process_plan_id inner join project_management_project_master PMPM on PMPM.project_management_master_id=PP.project_plan_project_id left outer join project_site_location_mapping_master PSLMM on PSLMM.project_site_location_mapping_master_id=PTP.project_task_planning_no_of_roads inner join project_process_master PPM on PPM.project_process_master_id=PPP.project_plan_process_name inner join project_task_master PTM on PTM.project_task_master_id=PPT.project_process_task_type left outer join project_object_output_master POOM on POOM.project_object_output_master_id=PTP.project_task_planning_object_type left outer join project_machine_type_master PMTM on PMTM.project_machine_type_master_id=PTP.project_task_planning_machine_type left outer join stock_unit_measure_master SUMM on SUMM.stock_unit_id=PTP.project_task_planning_uom left outer join project_cw_master PCM on PCM.project_cw_master_id=PTP.project_task_planning_machine_type";

		break;
	}




	$get_project_task_planning_list_squery_where = "";


	$filter_count = 0;
	// Data
	$get_project_task_planning_list_sdata = array();

	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." where PMPM.project_management_master_id = :project_id";
		}
		else
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and PMPM.project_management_master_id = :project_id";
		}

		//Data
		$get_project_task_planning_list_sdata['project_id']  = $project_id;

		$filter_count++;
	}

	if($min_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." where project_task_planning_start_date != :min_date";
		}
		else
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and project_task_planning_start_date != :min_date";
		}

		// Data
		$get_project_task_planning_list_sdata[':min_date'] = $min_date;

		$filter_count++;
	}

	if($max_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." where project_task_planning_end_date != :max_date";
		}
		else
		{
			// Query
			$get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and project_task_planning_end_date != :max_date";
		}

		// Data
		$get_project_task_planning_list_sdata[':max_date'] = $max_date;

		$filter_count++;
	}


	$get_project_task_planning_list_squery = $get_project_task_planning_list_squery_base.$get_project_task_planning_list_squery_where;

	try
	{
		$dbConnection = get_conn_handle();

		$get_project_task_planning_list_sstatement = $dbConnection->prepare($get_project_task_planning_list_squery);

		$get_project_task_planning_list_sstatement -> execute($get_project_task_planning_list_sdata);

		$get_project_task_planning_list_sdetails = $get_project_task_planning_list_sstatement -> fetchAll();

		if(FALSE === $get_project_task_planning_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_task_planning_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_task_planning_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}

	return $return;
 }
 /*
 PURPOSE : To get Project Email list
 INPUT 	: Prject Id
 OUTPUT 	: List of Email
 BY 		: Sonakshi
 */
 function db_get_project_email_list($project_email_search_data)
 {

 	if(array_key_exists("project_id",$project_email_search_data))
 	{
 		$project_id = $project_email_search_data["project_id"];
 	}
 	else
 	{
 		$project_id= "";
 	}

  $get_project_mail_list_squery_base = "select * from project_email_details PED inner join project_management_project_master PMPM on PMPM.project_management_master_id=PED.project_email_details_project_id";
 	$get_project_email_list_squery_where = "";


 	$filter_count = 0;
 	// Data
 	$project_email_list_data = array();

 	if($project_id != "")
 	{
 		if($filter_count == 0)
 		{
 			// Query
 			$get_project_email_list_squery_where = $get_project_email_list_squery_where." where PMPM.project_management_master_id = :project_id";
 		}
 		else
 		{
 			// Query
 			$get_project_email_list_squery_where = $get_project_email_list_squery_where." and PMPM.project_management_master_id = :project_id";
 		}

 		//Data
 		$project_email_list_data['project_id']  = $project_id;

 		$filter_count++;
 	}


 	$get_project_email_list_squery = $get_project_mail_list_squery_base.$get_project_email_list_squery_where;

 	try
 	{
 		$dbConnection = get_conn_handle();

 		$get_project_email_list_sstatement = $dbConnection->prepare($get_project_email_list_squery);

 		$get_project_email_list_sstatement -> execute($project_email_list_data);

 		$get_project_email_list_sdetails = $get_project_email_list_sstatement -> fetchAll();

 		if(FALSE === $get_project_email_list_sdetails)
 		{
 			$return["status"] = FAILURE;
 			$return["data"]   = "";
 		}
 		else if(count($get_project_email_list_sdetails) <= 0)
 		{
 			$return["status"] = DB_NO_RECORD;
 			$return["data"]   = "";
 		}
 		else
 		{
 			$return["status"] = DB_RECORD_ALREADY_EXISTS;
 			$return["data"]   = $get_project_email_list_sdetails;
 		}
 	}
 	catch(PDOException $e)
 	{
 		// Log the error
 		$return["status"] = FAILURE;
 		$return["data"] = "";
 	}

 	return $return;
  }


function db_get_project_overall_budget_manpower($key,$value) {
  $query = "select total_cost from actual_manpower where `$key` = ".$value;
    $get_project_budget_manpower_list_sdata = array();
  try
  {
    $dbConnection = get_conn_handle();

    $get_project_budget_manpower_list_sstatement = $dbConnection->prepare($query);

    $get_project_budget_manpower_list_sstatement -> execute($get_project_budget_manpower_list_sdata);

    $get_project_budget_manpower_list_sdetails = $get_project_budget_manpower_list_sstatement -> fetchAll();

    if(FALSE === $get_project_budget_manpower_list_sdetails)
    {
      $return["status"] = FAILURE;
      $return["data"]   = "";
    }
    else if(count($get_project_budget_manpower_list_sdetails) <= 0)
    {
      $return["status"] = DB_NO_RECORD;
      $return["data"]   = "";
    }
    else
    {
      $return["status"] = DB_RECORD_ALREADY_EXISTS;
      $return["data"]   = $get_project_budget_manpower_list_sdetails;
    }
  }
  catch(PDOException $e)
  {
    // Log the error
    $return["status"] = FAILURE;
    $return["data"] = "";
  }

  return $return;
}

function db_get_project_overall_budget_machine($key,$value) {
  $query = "select * from actual_machine where `$key` = ".$value;

    $get_project_budget_manpower_list_sdata = array();
  try
  {
    $dbConnection = get_conn_handle();

    $get_project_budget_manpower_list_sstatement = $dbConnection->prepare($query);

    $get_project_budget_manpower_list_sstatement -> execute($get_project_budget_manpower_list_sdata);

    $get_project_budget_manpower_list_sdetails = $get_project_budget_manpower_list_sstatement -> fetchAll();

    if(FALSE === $get_project_budget_manpower_list_sdetails)
    {
      $return["status"] = FAILURE;
      $return["data"]   = "";
    }
    else if(count($get_project_budget_manpower_list_sdetails) <= 0)
    {
      $return["status"] = DB_NO_RECORD;
      $return["data"]   = "";
    }
    else
    {
      $return["status"] = DB_RECORD_ALREADY_EXISTS;
      $return["data"]   = $get_project_budget_manpower_list_sdetails;
    }
  }
  catch(PDOException $e)
  {
    // Log the error
    $return["status"] = FAILURE;
    $return["data"] = "";
  }

  return $return;
}

function db_get_project__overall_budget_contract($key,$value) {
  $query = "select total_amount from actual_contract where `$key` = ".$value;
    $get_project_budget_manpower_list_sdata = array();
  try
  {
    $dbConnection = get_conn_handle();

    $get_project_budget_manpower_list_sstatement = $dbConnection->prepare($query);

    $get_project_budget_manpower_list_sstatement -> execute($get_project_budget_manpower_list_sdata);

    $get_project_budget_manpower_list_sdetails = $get_project_budget_manpower_list_sstatement -> fetchAll();

    if(FALSE === $get_project_budget_manpower_list_sdetails)
    {
      $return["status"] = FAILURE;
      $return["data"]   = "";
    }
    else if(count($get_project_budget_manpower_list_sdetails) <= 0)
    {
      $return["status"] = DB_NO_RECORD;
      $return["data"]   = "";
    }
    else
    {
      $return["status"] = DB_RECORD_ALREADY_EXISTS;
      $return["data"]   = $get_project_budget_manpower_list_sdetails;
    }
  }
  catch(PDOException $e)
  {
    // Log the error
    $return["status"] = FAILURE;
    $return["data"] = "";
  }

  return $return;
}


/*
PURPOSE : To get Project budget details
INPUT 	: Project Id
OUTPUT 	: List of data
BY 		: Soankshi
*/
function db_get_project_budget_manpower($project_budget_manpower_search_data)
{
  if(array_key_exists("task_id",$project_budget_manpower_search_data))
  {
    $task_id = $project_budget_manpower_search_data["task_id"];
  }
  else
  {
    $task_id= "";
  }

  if(array_key_exists("road_id",$project_budget_manpower_search_data))
  {
    $road_id = $project_budget_manpower_search_data["road_id"];
  }
  else
  {
    $road_id= "";
  }

  if(array_key_exists("process_id",$project_budget_manpower_search_data))
  {
    $process_id = $project_budget_manpower_search_data["process_id"];
  }
  else
  {
    $process_id= "";
  }

  if(array_key_exists("active",$project_budget_manpower_search_data))
  {
    $active = $project_budget_manpower_search_data["active"];
  }
  else
  {
    $active= "";
  }


  $get_project_budget_manpower_list_squery_base = "select *,min(date) as min_start_date , max(date) as max_end_date,sum(msmrt) as total_msmrt,max(cp) as max_cp  from actual_manpower";

  $get_project_budget_manpower_list_squery_where = "";
  $get_project_budget_manpower_list_squery_limit = " limit 0,30";
  $filter_count = 0;

  // Data
  $get_project_budget_manpower_list_sdata = array();

  if($task_id != "")
  {
    if($filter_count == 0)
    {
      // Query
      $get_project_budget_manpower_list_squery_where = $get_project_budget_manpower_list_squery_where." where task_id = :task_id";
    }
    else
    {
      // Query
      $get_project_budget_manpower_list_squery_where = $get_project_budget_manpower_list_squery_where." and task_id = :task_id";
    }

    // Data
    $get_project_budget_manpower_list_sdata[':task_id'] = $task_id;

    $filter_count++;
  }

  if($road_id != "")
  {
    if($filter_count == 0)
    {
      // Query
      $get_project_budget_manpower_list_squery_where = $get_project_budget_manpower_list_squery_where." where road_id = :road_id";
    }
    else
    {
      // Query
      $get_project_budget_manpower_list_squery_where = $get_project_budget_manpower_list_squery_where." and road_id = :road_id";
    }

    // Data
    $get_project_budget_manpower_list_sdata[':road_id'] = $road_id;

    $filter_count++;
  }

  if($process_id != "")
  {
    if($filter_count == 0)
    {
      // Query
      $get_project_budget_manpower_list_squery_where = $get_project_budget_manpower_list_squery_where." where process_id = :process_id";
    }
    else
    {
      // Query
      $get_project_budget_manpower_list_squery_where = $get_project_budget_manpower_list_squery_where." and process_id = :process_id";
    }

    // Data
    $get_project_budget_manpower_list_sdata[':process_id'] = $process_id;

    $filter_count++;
  }

  if($active != "")
  {
    if($filter_count == 0)
    {
      // Query
      $get_project_budget_manpower_list_squery_where = $get_project_budget_manpower_list_squery_where." where active = :active";
    }
    else
    {
      // Query
      $get_project_budget_manpower_list_squery_where = $get_project_budget_manpower_list_squery_where." and active = :active";
    }

    // Data
    $get_project_budget_manpower_list_sdata[':active'] = $active;

    $filter_count++;
  }

  $get_project_budget_manpower_list_squery = $get_project_budget_manpower_list_squery_base.$get_project_budget_manpower_list_squery_where.$get_project_budget_manpower_list_squery_limit;
  try
  {
    $dbConnection = get_conn_handle();

    $get_project_budget_manpower_list_sstatement = $dbConnection->prepare($get_project_budget_manpower_list_squery);

    $get_project_budget_manpower_list_sstatement -> execute($get_project_budget_manpower_list_sdata);

    $get_project_budget_manpower_list_sdetails = $get_project_budget_manpower_list_sstatement -> fetchAll();

    if(FALSE === $get_project_budget_manpower_list_sdetails)
    {
      $return["status"] = FAILURE;
      $return["data"]   = "";
    }
    else if(count($get_project_budget_manpower_list_sdetails) <= 0)
    {
      $return["status"] = DB_NO_RECORD;
      $return["data"]   = "";
    }
    else
    {
      $return["status"] = DB_RECORD_ALREADY_EXISTS;
      $return["data"]   = $get_project_budget_manpower_list_sdetails;
    }
  }
  catch(PDOException $e)
  {
    // Log the error
    $return["status"] = FAILURE;
    $return["data"] = "";
  }

  return $return;
 }

 /*
 PURPOSE : To get Project budget details
 INPUT 	: Project Id
 OUTPUT 	: List of data
 BY 		: Soankshi
 */
 function db_get_project_budget_machine($project_budget_machine_search_data)
 {
   if(array_key_exists("task_id",$project_budget_machine_search_data))
   {
     $task_id = $project_budget_machine_search_data["task_id"];
   }
   else
   {
     $task_id= "";
   }

   if(array_key_exists("road_id",$project_budget_machine_search_data))
   {
     $road_id = $project_budget_machine_search_data["road_id"];
   }
   else
   {
     $road_id= "";
   }

   if(array_key_exists("process_id",$project_budget_machine_search_data))
   {
     $process_id = $project_budget_machine_search_data["process_id"];
   }
   else
   {
     $process_id= "";
   }

   if(array_key_exists("active",$project_budget_machine_search_data))
   {
     $active = $project_budget_machine_search_data["active"];
   }
   else
   {
     $active= "";
   }


   $get_project_budget_machine_list_squery_base = "select *,min(machine_start_date) as min_start_date, max(machine_end_date) as max_end_date,sum(msmrt) as total_msmrt, max(cp) as max_cp from actual_machine";

   $get_project_budget_machine_list_squery_where = "";
   $get_project_budget_machine_list_squery_limit = " limit 0,30";
   $filter_count = 0;

   // Data
   $get_project_machine_machine_list_sdata = array();

   if($task_id != "")
   {
     if($filter_count == 0)
     {
       // Query  var_dump($mp_list);
       $get_project_budget_machine_list_squery_where = $get_project_budget_machine_list_squery_where." where task_id = :task_id";
     }
     else
     {
       // Query
       $get_project_budget_machine_list_squery_where = $get_project_budget_machine_list_squery_where." and task_id = :task_id";
     }

     // Data
     $get_project_machine_machine_list_sdata[':task_id'] = $task_id;

     $filter_count++;
   }

   if($road_id != "")
   {
     if($filter_count == 0)
     {
       // Query
       $get_project_budget_machine_list_squery_where = $get_project_budget_machine_list_squery_where." where road_id = :road_id";
     }
     else
     {
       // Query
       $get_project_budget_machine_list_squery_where = $get_project_budget_machine_list_squery_where." and road_id = :road_id";
     }

     // Data
     $get_project_machine_machine_list_sdata[':road_id'] = $road_id;

     $filter_count++;
   }

   if($process_id != "")
   {
     if($filter_count == 0)
     {
       // Query
       $get_project_budget_machine_list_squery_where = $get_project_budget_machine_list_squery_where." where process_id = :process_id";
     }
     else
     {
       // Query
       $get_project_budget_machine_list_squery_where = $get_project_budget_machine_list_squery_where." and process_id = :process_id";
     }

     // Data
     $get_project_machine_machine_list_sdata[':process_id'] = $process_id;

     $filter_count++;
   }


   if($active != "")
   {
     if($filter_count == 0)
     {
       // Query
       $get_project_budget_machine_list_squery_where = $get_project_budget_machine_list_squery_where." where active = :active";
     }
     else
     {
       // Query
       $get_project_budget_machine_list_squery_where = $get_project_budget_machine_list_squery_where." and active = :active";
     }

     // Data
     $get_project_machine_machine_list_sdata[':active'] = $active;

     $filter_count++;
   }


   $get_project_budget_machine_list_squery = $get_project_budget_machine_list_squery_base.$get_project_budget_machine_list_squery_where.$get_project_budget_machine_list_squery_limit;
   try
   {
     $dbConnection = get_conn_handle();

     $get_project_budget_machine_list_sstatement = $dbConnection->prepare($get_project_budget_machine_list_squery);

     $get_project_budget_machine_list_sstatement -> execute($get_project_machine_machine_list_sdata);

     $get_project_budget_machine_list_sdetails = $get_project_budget_machine_list_sstatement -> fetchAll();

     if(FALSE === $get_project_budget_machine_list_sdetails)
     {
       $return["status"] = FAILURE;
       $return["data"]   = "";
     }
     else if(count($get_project_budget_machine_list_sdetails) <= 0)
     {
       $return["status"] = DB_NO_RECORD;
       $return["data"]   = "";
     }
     else
     {
       $return["status"] = DB_RECORD_ALREADY_EXISTS;
       $return["data"]   = $get_project_budget_machine_list_sdetails;
     }
   }
   catch(PDOException $e)
   {
     // Log the error
     $return["status"] = FAILURE;
     $return["data"] = "";
   }

   return $return;
  }

  /*
  PURPOSE : To get Project budget details
  INPUT 	: Project Id
  OUTPUT 	: List of data
  BY 		: Soankshi
  */
  function db_get_project_budget_contract($project_budget_contract_search_data)
  {
    if(array_key_exists("task_id",$project_budget_contract_search_data))
    {
      $task_id = $project_budget_contract_search_data["task_id"];
    }
    else
    {
      $task_id= "";
    }

    if(array_key_exists("road_id",$project_budget_contract_search_data))
    {
      $road_id = $project_budget_contract_search_data["road_id"];
    }
    else
    {
      $road_id= "";
    }

    if(array_key_exists("process_id",$project_budget_contract_search_data))
    {
      $process_id = $project_budget_contract_search_data["process_id"];
    }
    else
    {
      $process_id= "";
    }

    if(array_key_exists("active",$project_budget_contract_search_data))
    {
      $active = $project_budget_contract_search_data["active"];
    }
    else
    {
      $active= "";
    }


    $get_project_budget_contract_list_squery_base = "select *, min(date) as min_start_date ,max(date) as max_end_date,sum(msmrt) as total_msmrt,max(cp) as max_cp from actual_contract";

    $get_project_budget_contract_list_squery_where = "";
    $get_project_budget_contract_list_squery_limit = " limit 0,30";
    $filter_count = 0;

    // Data
    $get_project_contract_list_sdata = array();

    if($task_id != "")
    {
      if($filter_count == 0)
      {
        // Query
        $get_project_budget_contract_list_squery_where = $get_project_budget_contract_list_squery_where." where task_id = :task_id";
      }
      else
      {
        // Query
        $get_project_budget_contract_list_squery_where = $get_project_budget_contract_list_squery_where." and task_id = :task_id";
      }

      // Data
      $get_project_contract_list_sdata[':task_id'] = $task_id;

      $filter_count++;
    }

    if($road_id != "")
    {
      if($filter_count == 0)
      {
        // Query
        $get_project_budget_contract_list_squery_where = $get_project_budget_contract_list_squery_where." where road_id = :road_id";
      }
      else
      {
        // Query
        $get_project_budget_contract_list_squery_where = $get_project_budget_contract_list_squery_where." and road_id = :road_id";
      }

      // Data
      $get_project_contract_list_sdata[':road_id'] = $road_id;

      $filter_count++;
    }

    if($process_id != "")
    {
      if($filter_count == 0)
      {
        // Query
        $get_project_budget_contract_list_squery_where = $get_project_budget_contract_list_squery_where." where process_id = :process_id";
      }
      else
      {
        // Query
        $get_project_budget_contract_list_squery_where = $get_project_budget_contract_list_squery_where." and process_id = :process_id";
      }

      // Data
      $get_project_contract_list_sdata[':process_id'] = $process_id;

      $filter_count++;
    }
    if($active != "")
    {
      if($filter_count == 0)
      {
        // Query
        $get_project_budget_contract_list_squery_where = $get_project_budget_contract_list_squery_where." where active = :active";
      }
      else
      {
        // Query
        $get_project_budget_contract_list_squery_where = $get_project_budget_contract_list_squery_where." and active = :active";
      }

      // Data
      $get_project_contract_list_sdata[':active'] = $active;

      $filter_count++;
    }


    $get_project_budget_contract_list_squery = $get_project_budget_contract_list_squery_base.$get_project_budget_contract_list_squery_where.$get_project_budget_contract_list_squery_limit;
    try
    {
      $dbConnection = get_conn_handle();

      $get_project_budget_contract_list_sstatement = $dbConnection->prepare($get_project_budget_contract_list_squery);

      $get_project_budget_contract_list_sstatement -> execute($get_project_contract_list_sdata);

      $get_project_budget_contract_list_sdetails = $get_project_budget_contract_list_sstatement -> fetchAll();

      if(FALSE === $get_project_budget_contract_list_sdetails)
      {
        $return["status"] = FAILURE;
        $return["data"]   = "";
      }
      else if(count($get_project_budget_contract_list_sdetails) <= 0)
      {
        $return["status"] = DB_NO_RECORD;
        $return["data"]   = "";
      }
      else
      {
        $return["status"] = DB_RECORD_ALREADY_EXISTS;
        $return["data"]   = $get_project_budget_contract_list_sdetails;
      }
    }
    catch(PDOException $e)
    {
      // Log the error
      $return["status"] = FAILURE;
      $return["data"] = "";
    }

    return $return;
   }
   		 /*
		 PURPOSE : To get Project budget details
		 INPUT 	: Project Id
		 OUTPUT 	: List of data
		 BY 		: Soankshi
		 */
		 function db_get_project_budget_material($project_budget_material_search_data)
		 {
			 if(array_key_exists("project_id",$project_budget_material_search_data))
			 {
				 $project_id = $project_budget_material_search_data["project_id"];
			 }
			 else
			 {
				 $project_id= "";
			 }

       if(array_key_exists("process_id",$project_budget_material_search_data))
      {
        $process_id = $project_budget_material_search_data["process_id"];
      }
      else
      {
        $process_id= "";
      }

			 $get_project_budget_material_list_squery_base = "select * from actual_material";

			 $get_project_budget_material_list_squery_where = "";
			 $filter_count = 0;

			 // Data
			 $get_project_material_list_sdata = array();

			 if($project_id != "")
			 {
				 if($filter_count == 0)
				 {
					 // Query
					 $get_project_budget_material_list_squery_where = $get_project_budget_material_list_squery_where." where project_id = :project_id";
				 }
				 else
				 {
					 // Query
					 $get_project_budget_material_list_squery_where = $get_project_budget_material_list_squery_where." and project_id = :project_id";
				 }

				 // Data
				 $get_project_material_list_sdata[':project_id'] = $project_id;

				 $filter_count++;
			 }

       if($process_id != "")
			 {
				 if($filter_count == 0)
				 {
					 // Query
					 $get_project_budget_material_list_squery_where = $get_project_budget_material_list_squery_where." where process_id = :process_id";
				 }
				 else
				 {
					 // Query
					 $get_project_budget_material_list_squery_where = $get_project_budget_material_list_squery_where." and process_id = :process_id";
				 }

				 // Data
				 $get_project_material_list_sdata[':process_id'] = $process_id;

				 $filter_count++;
			 }


			 $get_project_budget_material_list_squery = $get_project_budget_material_list_squery_base.$get_project_budget_material_list_squery_where;
			 try
			 {
				 $dbConnection = get_conn_handle();

				 $get_project_budget_material_list_sstatement = $dbConnection->prepare($get_project_budget_material_list_squery);

				 $get_project_budget_material_list_sstatement -> execute($get_project_material_list_sdata);

				 $get_project_budget_material_list_sdetails = $get_project_budget_material_list_sstatement -> fetchAll();

				 if(FALSE === $get_project_budget_material_list_sdetails)
				 {
					 $return["status"] = FAILURE;
					 $return["data"]   = "";
				 }
				 else if(count($get_project_budget_material_list_sdetails) <= 0)
				 {
					 $return["status"] = DB_NO_RECORD;
					 $return["data"]   = "";
				 }
				 else
				 {
					 $return["status"] = DB_RECORD_ALREADY_EXISTS;
					 $return["data"]   = $get_project_budget_material_list_sdetails;
				 }
			 }
			 catch(PDOException $e)
			 {
				 // Log the error
				 $return["status"] = FAILURE;
				 $return["data"] = "";
			 }

			 return $return;
			}
      /*
   PURPOSE : To get Project budget details
   INPUT 	: Project Id
   OUTPUT 	: List of data
   BY 		: Soankshi
   */
   function db_get_task_planning_list($project_task_planning_search_data)
   {
     if(array_key_exists("process_id",$project_task_planning_search_data))
     {
       $process_id = $project_task_planning_search_data["process_id"];
     }
     else
     {
       $process_id= "";
     }

     if(array_key_exists("project_id",$project_task_planning_search_data))
     {
       $project_id = $project_task_planning_search_data["project_id"];
     }
     else
     {
       $project_id= "";
     }

     $get_project_task_planning_list_squery_base = "select min(planned_start_date) as min_start_date,max(planned_end_date) as max_end_date,sum(planned_msmrt) as total_msmrt from task_planning_list";

     $get_project_task_planning_list_squery_where = " where planned_start_date != '000-00-00'";
     $get_project_task_planning_list_squery_limit = " limit 0,30";
     $filter_count = 0;

     // Data
     $get_project_task_planning_list_sdata = array();

     if($process_id != "")
     {
       if($filter_count == 0)
       {
         // Query
         $get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and plan_process_id = :process_id";
       }
       else
       {
         // Query
         $get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and plan_process_id = :process_id";
       }

       // Data
       $get_project_task_planning_list_sdata[':process_id'] = $process_id;

       $filter_count++;
     }

     if($project_id != "")
     {
       if($filter_count == 0)
       {
         // Query
         $get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and project_id = :project_id";
       }
       else
       {
         // Query
         $get_project_task_planning_list_squery_where = $get_project_task_planning_list_squery_where." and project_id = :project_id";
       }

       // Data
       $get_project_task_planning_list_sdata[':project_id'] = $project_id;

       $filter_count++;
     }


     $get_project_task_planning_list_squery = $get_project_task_planning_list_squery_base.$get_project_task_planning_list_squery_where.$get_project_task_planning_list_squery_limit;
     try
     {
       $dbConnection = get_conn_handle();

       $get_project_task_planning_list_sstatement = $dbConnection->prepare($get_project_task_planning_list_squery);

       $get_project_task_planning_list_sstatement -> execute($get_project_task_planning_list_sdata);

       $get_project_task_planning_list_sdetails = $get_project_task_planning_list_sstatement -> fetchAll();

       if(FALSE === $get_project_task_planning_list_sdetails)
       {
         $return["status"] = FAILURE;
         $return["data"]   = "";
       }
       else if(count($get_project_task_planning_list_sdetails) <= 0)
       {
         $return["status"] = DB_NO_RECORD;
         $return["data"]   = "";
       }
       else
       {
         $return["status"] = DB_RECORD_ALREADY_EXISTS;
         $return["data"]   = $get_project_task_planning_list_sdetails;
       }
     }
     catch(PDOException $e)
     {
       // Log the error
       $return["status"] = FAILURE;
       $return["data"] = "";
     }

     return $return;
    }

    /*
   PURPOSE : To get Project budget details
   INPUT 	: Project Id
   OUTPUT 	: List of data
   BY 		: Soankshi
   */
   function db_get_pause_report_list($project_pause_report_search_data)
   {
     if(array_key_exists("process_id",$project_pause_report_search_data))
     {
       $process_id = $project_pause_report_search_data["process_id"];
     }
     else
     {
       $process_id= "";
     }

     $get_project_pause_report_list_squery_base = "select *, min(start_date) as min_start_date from pause_report";

     $get_project_pause_report_list_squery_where = "";
     $get_project_pause_report_list_squery_order_by = "";
     $get_project_pause_report_list_squery_limit = " limit 0,30";

     $filter_count = 0;

     // Data
     $get_project_pause_report_list_sdata = array();

     if($process_id != "")
     {
       if($filter_count == 0)
       {
         // Query
         $get_project_pause_report_list_squery_where = $get_project_pause_report_list_squery_where." where process_id = :process_id";
       }
       else
       {
         // Query
         $get_project_pause_report_list_squery_where = $get_project_pause_report_list_squery_where." and process_id = :process_id";
       }

       // Data
       $get_project_pause_report_list_sdata[':process_id'] = $process_id;

       $filter_count++;
     }

     $get_project_pause_report_list_squery_order_by = " order by start_date DESC";
     $get_project_pause_report_list_squery = $get_project_pause_report_list_squery_base.$get_project_pause_report_list_squery_where.$get_project_pause_report_list_squery_order_by.$get_project_pause_report_list_squery_limit;
     try
     {
       $dbConnection = get_conn_handle();

       $get_project_pause_report_list_sstatement = $dbConnection->prepare($get_project_pause_report_list_squery);

       $get_project_pause_report_list_sstatement -> execute($get_project_pause_report_list_sdata);

       $get_project_pause_report_list_sdetails = $get_project_pause_report_list_sstatement -> fetchAll();

       if(FALSE === $get_project_pause_report_list_sdetails)
       {
         $return["status"] = FAILURE;
         $return["data"]   = "";
       }
       else if(count($get_project_pause_report_list_sdetails) <= 0)
       {
         $return["status"] = DB_NO_RECORD;
         $return["data"]   = "";
       }
       else
       {
         $return["status"] = DB_RECORD_ALREADY_EXISTS;
         $return["data"]   = $get_project_pause_report_list_sdetails;
       }
     }
     catch(PDOException $e)
     {
        // Log the error
       $return["status"] = FAILURE;
       $return["data"] = "";
     }

     return $return;
    }
    function db_get_project_process_list($key,$value) {
      $query = "select * from project_process_list where `$key` = ".$value;
        $query .= " ORDER BY process_order ASC";
        $get_project_process_list_sdata = array();
      try
      {
        $dbConnection = get_conn_handle();

        $get_project_process_list_sstatement = $dbConnection->prepare($query);

        $get_project_process_list_sstatement -> execute($get_project_process_list_sdata);

        $get_project_process_list_sdetails = $get_project_process_list_sstatement -> fetchAll();

        if(FALSE === $get_project_process_list_sdetails)
        {
          $return["status"] = FAILURE;
          $return["data"]   = "";
        }
        else if(count($get_project_process_list_sdetails) <= 0)
        {
          $return["status"] = DB_NO_RECORD;
          $return["data"]   = "";
        }
        else
        {
          $return["status"] = DB_RECORD_ALREADY_EXISTS;
          $return["data"]   = $get_project_process_list_sdetails;
        }
      }
      catch(PDOException $e)
      {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
      }

      return $return;
    }

    function db_get_project_task_list($process_id) {
      $query = "select * from project_task where `project_process_id` = ".$process_id;

        $get_project_task_list_sdata = array();
      try
      {
        $dbConnection = get_conn_handle();

        $get_project_task_list_sstatement = $dbConnection->prepare($query);

        $get_project_task_list_sstatement -> execute($get_project_task_list_sdata);

        $get_project_task_list_sdetails = $get_project_task_list_sstatement -> fetchAll();

        if(FALSE === $get_project_task_list_sdetails)
        {
          $return["status"] = FAILURE;
          $return["data"]   = "";
        }
        else if(count($get_project_task_list_sdetails) <= 0)
        {
          $return["status"] = DB_NO_RECORD;
          $return["data"]   = "";
        }
        else
        {
          $return["status"] = DB_RECORD_ALREADY_EXISTS;
          $return["data"]   = $get_project_task_list_sdetails;
        }
      }
      catch(PDOException $e)
      {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
      }

      return $return;
    }
    /*
    PURPOSE : To add Project process remarks
    INPUT 	: Project ID, Process ID,Mp,Mc,Cw added by and added on
    OUTPUT 	: Remarks ID, success or failure message
    BY 		: Soankshi
    */
    function db_add_process_remarks($project_id,$process_id,$task_id,$process_name,$material,$mp,$mc,$cw,$remarks,$added_by)
    {
      // Query
       $process_remarks_iquery = "insert into project_process_remarks_list
       (remarks_list_project_id,remarks_list_process_id,remarks_list_task_id,remarks_list_process_name,remarks_list_material,remarks_list_manpower,remarks_list_machine,remarks_list_contract,remarks_list_remarks,remarks_list_added_by,remarks_list_added_on)
       values(:project_id,:process_id,:task_id,:process_name,:material,:mp,:mc,:cw,:remarks,:added_by,:added_on)";
        try
        {
            $dbConnection = get_conn_handle();
            $process_remarks_istatement = $dbConnection->prepare($process_remarks_iquery);

            // Data
            $process_remarks_idata = array(':project_id'=>$project_id,':process_id'=>$process_id,':task_id'=>$task_id,':process_name'=>$process_name,':material'=>$material,':mp'=>$mp,':mc'=>$mc,':cw'=>$cw,':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));
        $dbConnection->beginTransaction();
            $process_remarks_istatement->execute($process_remarks_idata);
        $project_remarks_id = $dbConnection->lastInsertId();
        $dbConnection->commit();

            $return["status"] = SUCCESS;
        $return["data"]   = $project_remarks_id;
        }
        catch(PDOException $e)
        {
            // Log the error
            $return["status"] = FAILURE;
        $return["data"]   = "";
        }

        return $return;
    }

    function db_get_process_remarks($process_remarks_search_data) {

      if(array_key_exists("process_id",$process_remarks_search_data))
      {
        $process_id = $process_remarks_search_data["process_id"];
      }
      else
      {
        $process_id= "";
      }

      if(array_key_exists("project_id",$process_remarks_search_data))
      {
        $project_id = $process_remarks_search_data["project_id"];
      }
      else
      {
        $project_id= "";
      }

      if(array_key_exists("status",$process_remarks_search_data))
      {
        $status = $process_remarks_search_data["status"];
      }
      else
      {
        $status= "";
      }

      if(array_key_exists("process_master_id",$process_remarks_search_data))
      {
        $process_master_id = $process_remarks_search_data["process_master_id"];
      }
      else
      {
        $process_master_id= "";
      }

      if(array_key_exists("task_id",$process_remarks_search_data))
      {
        $task_id = $process_remarks_search_data["task_id"];
      }
      else
      {
        $task_id = "";
      }

      $get_process_remarks_list_squery_base = "select * from project_process_remarks_list PPR inner join project_management_project_master PM on PM.project_management_master_id=PPR.remarks_list_project_id inner join project_task_master PTM on PTM.project_task_master_id=PPR.remarks_list_task_id inner join users U on U.user_id=PPR.remarks_list_added_by inner join project_plan_process PPP on PPP.project_plan_process_id=PPR.remarks_list_process_id";

      $get_process_remarks_list_squery_where = "";


      $filter_count = 0;

      // Data
      $get_process_remarks_list_sdata = array();

      if($process_id != "")
      {
        if($filter_count == 0)
        {
          // Query
          $get_process_remarks_list_squery_where = $get_process_remarks_list_squery_where." where remarks_list_process_id = :process_id";
        }
        else
        {
          // Query
          $get_process_remarks_list_squery_where = $get_process_remarks_list_squery_where." and remarks_list_process_id = :process_id";
        }

        // Data
        $get_process_remarks_list_sdata[':process_id'] = $process_id;

        $filter_count++;
      }

      if($project_id != "")
      {
        if($filter_count == 0)
        {
          // Query
          $get_process_remarks_list_squery_where = $get_process_remarks_list_squery_where." where remarks_list_project_id = :project_id";
        }
        else
        {
          // Query
          $get_process_remarks_list_squery_where = $get_process_remarks_list_squery_where." and remarks_list_project_id = :project_id";
        }

        // Data
        $get_process_remarks_list_sdata[':project_id'] = $project_id;

        $filter_count++;
      }


      if($status != "")
      {
        if($filter_count == 0)
        {
          // Query
          $get_process_remarks_list_squery_where = $get_process_remarks_list_squery_where." where remarks_list_status = :status";
        }
        else
        {
          // Query
          $get_process_remarks_list_squery_where = $get_process_remarks_list_squery_where." and remarks_list_status = :status";
        }

        // Data
        $get_process_remarks_list_sdata[':status'] = $status;

        $filter_count++;
      }

      if($process_master_id != "")
      {
        if($filter_count == 0)
        {
          // Query
          $get_process_remarks_list_squery_where = $get_process_remarks_list_squery_where." where project_plan_process_name = :process_master_id";
        }
        else
        {
          // Query
          $get_process_remarks_list_squery_where = $get_process_remarks_list_squery_where." and project_plan_process_name = :process_master_id";
        }

        // Data
        $get_process_remarks_list_sdata[':process_master_id'] = $process_master_id;

        $filter_count++;
      }

      if($task_id != "")
      {
        if($filter_count == 0)
        {
          // Query
          $get_process_remarks_list_squery_where = $get_process_remarks_list_squery_where." where remarks_list_task_id = :task_id";
        }
        else
        {
          // Query
          $get_process_remarks_list_squery_where = $get_process_remarks_list_squery_where." and remarks_list_task_id = :task_id";
        }

        // Data
        $get_process_remarks_list_sdata[':task_id'] = $task_id;

        $filter_count++;
      }

      $get_project_pause_report_list_squery_order_by = " order by start_date DESC";
      $get_project_pause_report_list_squery = $get_process_remarks_list_squery_base.$get_process_remarks_list_squery_where;
      try
      {
        $dbConnection = get_conn_handle();

        $get_process_remarks_list_sstatement = $dbConnection->prepare($get_project_pause_report_list_squery);

        $get_process_remarks_list_sstatement -> execute($get_process_remarks_list_sdata);

        $get_process_remarks_list_sdetails = $get_process_remarks_list_sstatement -> fetchAll();

        if(FALSE === $get_process_remarks_list_sdetails)
        {
          $return["status"] = FAILURE;
          $return["data"]   = "";
        }
        else if(count($get_process_remarks_list_sdetails) <= 0)
        {
          $return["status"] = DB_NO_RECORD;
          $return["data"]   = "";
        }
        else
        {
          $return["status"] = DB_RECORD_ALREADY_EXISTS;
          $return["data"]   = $get_process_remarks_list_sdetails;
        }
      }
      catch(PDOException $e)
      {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
      }

      return $return;
    }
    /*
   PURPOSE : To update Project process remarks
   INPUT 	: Remarks ID, Project Process remarks Update Array
   OUTPUT 	: Remarks ID; Message of success or failure
   BY 		: Sonakshi
   */
   function db_update_process_remarks_task($remarks_id,$process_remarks_update_data)
   {
     if(array_key_exists("status",$process_remarks_update_data))
     {
       $status = $process_remarks_update_data["status"];
     }
     else
     {
       $status = "";
     }
     if(array_key_exists("completed_remarks",$process_remarks_update_data))
     {
       $completed_remarks = $process_remarks_update_data["completed_remarks"];
     }
     else
     {
       $completed_remarks = "";
     }

     if(array_key_exists("closed_by",$process_remarks_update_data))
     {
       $closed_by = $process_remarks_update_data["closed_by"];
     }
     else
     {
       $closed_by = "";
     }

     if(array_key_exists("closed_on",$process_remarks_update_data))
     {
       $closed_on = $process_remarks_update_data["closed_on"];
     }
     else
     {
       $closed_on = "";
     }
     // Query
       $project_process_remarks_uquery_base = "update project_process_remarks_list set";

     $project_process_remarks_update_uquery_set = "";

     $project_process_remarks_update_uquery_where = " where remarks_list_id = :remarks_id";

     $project_process_remarks_update_udata = array(":remarks_id"=>$remarks_id);

     $filter_count = 0;

     if($status != "")
     {
       $project_process_remarks_update_uquery_set = $project_process_remarks_update_uquery_set." remarks_list_status = :status,";
       $project_process_remarks_update_udata[":status"] = $status;
       $filter_count++;
     }

     if($completed_remarks != "")
     {
       $project_process_remarks_update_uquery_set = $project_process_remarks_update_uquery_set." remarks_list_completed_remarks = :completed_remarks,";
       $project_process_remarks_update_udata[":completed_remarks"] = $completed_remarks;
       $filter_count++;
     }

     if($closed_by != "")
     {
       $project_process_remarks_update_uquery_set = $project_process_remarks_update_uquery_set." remarks_list_closed_by = :closed_by,";
       $project_process_remarks_update_udata[":closed_by"] = $closed_by;
       $filter_count++;
     }

     if($closed_on != "")
     {
       $project_process_remarks_update_uquery_set = $project_process_remarks_update_uquery_set." remarks_list_closed_on = :closed_on,";
       $project_process_remarks_update_udata[":closed_on"] = $closed_on;
       $filter_count++;
     }

     if($filter_count > 0)
     {
       $project_process_remarks_update_uquery_set = trim($project_process_remarks_update_uquery_set,',');
     }

     $project_process_remarks_update_uquery = $project_process_remarks_uquery_base.$project_process_remarks_update_uquery_set.$project_process_remarks_update_uquery_where;


       try
       {
           $dbConnection = get_conn_handle();

           $project_process_remarks_update_ustatement = $dbConnection->prepare($project_process_remarks_update_uquery);

           $project_process_remarks_update_ustatement -> execute($project_process_remarks_update_udata);

           $return["status"] = SUCCESS;
       $return["data"]   = $remarks_id;
       }
       catch(PDOException $e)
       {
           // Log the error
           $return["status"] = FAILURE;
       $return["data"]   = "";
       }

     return $return;
   }

   function db_get_process_view_remarks($process_remarks_view_search_data) {

     if(array_key_exists("process_id",$process_remarks_view_search_data))
     {
       $process_id = $process_remarks_view_search_data["process_id"];
     }
     else
     {
       $process_id= "";
     }

     if(array_key_exists("project_id",$process_remarks_view_search_data))
     {
       $project_id = $process_remarks_view_search_data["project_id"];
     }
     else
     {
       $project_id= "";
     }

     if(array_key_exists("status",$process_remarks_view_search_data))
     {
       $status = $process_remarks_view_search_data["status"];
     }
     else
     {
       $status= "";
     }

     if(array_key_exists("process_master_id",$process_remarks_view_search_data))
     {
       $process_master_id = $process_remarks_view_search_data["process_master_id"];
     }
     else
     {
       $process_master_id= "";
     }

     if(array_key_exists("task_id",$process_remarks_view_search_data))
     {
       $task_id = $process_remarks_view_search_data["task_id"];
     }
     else
     {
       $task_id = "";
     }

     $get_process_remarks_view_list_squery_base = "select * from process_remarks_list";

     $get_process_remarks_view_list_squery_where = "";


     $filter_count = 0;

     // Data
     $get_process_remarks_view_list_sdata = array();

     if($process_id != "")
     {
       if($filter_count == 0)
       {
         // Query
         $get_process_remarks_view_list_squery_where = $get_process_remarks_view_list_squery_where." where remarks_list_process_id = :process_id";
       }
       else
       {
         // Query
         $get_process_remarks_view_list_squery_where = $get_process_remarks_view_list_squery_where." and remarks_list_process_id = :process_id";
       }

       // Data
       $get_process_remarks_view_list_sdata[':process_id'] = $process_id;

       $filter_count++;
     }

     if($project_id != "")
     {
       if($filter_count == 0)
       {
         // Query
         $get_process_remarks_view_list_squery_where = $get_process_remarks_view_list_squery_where." where remarks_list_project_id = :project_id";
       }
       else
       {
         // Query
         $get_process_remarks_view_list_squery_where = $get_process_remarks_view_list_squery_where." and remarks_list_project_id = :project_id";
       }

       // Data
       $get_process_remarks_view_list_sdata[':project_id'] = $project_id;

       $filter_count++;
     }


     if($status != "")
     {
       if($filter_count == 0)
       {
         // Query
         $get_process_remarks_view_list_squery_where = $get_process_remarks_view_list_squery_where." where remarks_list_status = :status";
       }
       else
       {
         // Query
         $get_process_remarks_view_list_squery_where = $get_process_remarks_view_list_squery_where." and remarks_list_status = :status";
       }

       // Data
       $get_process_remarks_view_list_sdata[':status'] = $status;

       $filter_count++;
     }

     if($process_master_id != "")
     {
       if($filter_count == 0)
       {
         // Query
         $get_process_remarks_view_list_squery_where = $get_process_remarks_view_list_squery_where." where process_master_id = :process_master_id";
       }
       else
       {
         // Query
         $get_process_remarks_view_list_squery_where = $get_process_remarks_view_list_squery_where." and process_master_id = :process_master_id";
       }

       // Data
       $get_process_remarks_view_list_sdata[':process_master_id'] = $process_master_id;

       $filter_count++;
     }

     if($task_id != "")
     {
       if($filter_count == 0)
       {
         // Query
         $get_process_remarks_view_list_squery_where = $get_process_remarks_view_list_squery_where." where remarks_list_task_id = :task_id";
       }
       else
       {
         // Query
         $get_process_remarks_view_list_squery_where = $get_process_remarks_view_list_squery_where." and remarks_list_task_id = :task_id";
       }

       // Data
       $get_process_remarks_view_list_sdata[':task_id'] = $task_id;

       $filter_count++;
     }

     $get_project_remarks_view_list_squery = $get_process_remarks_view_list_squery_base.$get_process_remarks_view_list_squery_where;

     try
     {
       $dbConnection = get_conn_handle();

       $get_process_remarks_view_list_sstatement = $dbConnection->prepare($get_project_remarks_view_list_squery);

       $get_process_remarks_view_list_sstatement -> execute($get_process_remarks_view_list_sdata);

       $get_process_remarks_view_list_sdetails = $get_process_remarks_view_list_sstatement -> fetchAll();

       if(FALSE === $get_process_remarks_view_list_sdetails)
       {
         $return["status"] = FAILURE;
         $return["data"]   = "";
       }
       else if(count($get_process_remarks_view_list_sdetails) <= 0)
       {
         $return["status"] = DB_NO_RECORD;
         $return["data"]   = "";
       }
       else
       {
         $return["status"] = DB_RECORD_ALREADY_EXISTS;
         $return["data"]   = $get_process_remarks_view_list_sdetails;
       }
     }
     catch(PDOException $e)
     {
       // Log the error
       $return["status"] = FAILURE;
       $return["data"] = "";
     }

     return $return;
   }
   /*
   PURPOSE : To add Project planned data
   INPUT 	: Project ID, Process ID,Mp,Mc,Cw added by and added on
   OUTPUT 	: Palnned ID, success or failure message
   BY 		: Soankshi
   */
   function db_add_planned_data($project_id,$process_id,$task_id,$uom,$road_id,$mp,$mc,$cw,$material,$added_by)
   {
     // Query
      $project_planned_iquery = "insert into budget_planning
      (planned_project_id,planned_process_id,planned_task_id,planned_uom,planned_road_id,planned_manpower,planned_machine,planned_contract,planned_material,planned_added_on,planned_added_by)
      values(:project_id,:process_id,:task_id,:uom,:road_id,:mp,:mc,:cw,:material,:added_on,:added_by)";
       try
       {
           $dbConnection = get_conn_handle();
           $project_planned_istatement = $dbConnection->prepare($project_planned_iquery);

           // Data
           $project_planned_idata = array(':project_id'=>$project_id,':process_id'=>$process_id,':task_id'=>$task_id,':uom'=>$uom,':road_id'=>$road_id,':mp'=>$mp,':mc'=>$mc,':cw'=>$cw,':material'=>$material,':added_on'=>date("Y-m-d H:i:s"),':added_by'=>$added_by);
       $dbConnection->beginTransaction();
           $project_planned_istatement->execute($project_planned_idata);
       $planned_id = $dbConnection->lastInsertId();
       $dbConnection->commit();

           $return["status"] = SUCCESS;
       $return["data"]   = $planned_id;
       }
       catch(PDOException $e)
       {
           // Log the error
           $return["status"] = FAILURE;
       $return["data"]   = "";
       }

       return $return;
   }
   function db_delete_planned_data($project_delete_data) {

     if(array_key_exists("task_id",$project_delete_data))
     {
       $task_id = $project_delete_data["task_id"];
     }
     else
     {
       $task_id= "";
     }

     if(array_key_exists("road_id",$project_delete_data))
     {
       $road_id = $project_delete_data["road_id"];
     }
     else
     {
       $road_id= "";
     }

     $delete_planned_data_squery_base = "delete from budget_planning";

     $get_planned_data_squery_where = "";


     $filter_count = 0;

     // Data
     $get_planned_data_list_sdata = array();

     if($task_id != "")
     {
       if($filter_count == 0)
       {
         // Query
         $get_planned_data_squery_where = $get_planned_data_squery_where." where planned_task_id = :task_id";
       }
       else
       {
         // Query
         $get_planned_data_squery_where = $get_planned_data_squery_where." and planned_task_id = :task_id";
       }

       // Data
       $get_planned_data_list_sdata[':task_id'] = $task_id;

       $filter_count++;
     }

     if($road_id != "")
     {
       if($filter_count == 0)
       {
         // Query
         $get_planned_data_squery_where = $get_planned_data_squery_where." where planned_road_id = :road_id";
       }
       else
       {
         // Query
         $get_planned_data_squery_where = $get_planned_data_squery_where." and planned_road_id = :road_id";
       }

       // Data
       $get_planned_data_list_sdata[':road_id'] = $road_id;

       $filter_count++;
     }

     $delete_planned_data_squery = $delete_planned_data_squery_base.$get_planned_data_squery_where;

     try
     {
       $dbConnection = get_conn_handle();

       $delete_planned_sstatement = $dbConnection->prepare($delete_planned_data_squery);

       $delete_planned_sstatement -> execute($get_planned_data_list_sdata);

       $delete_planned_sdetails = $delete_planned_sstatement -> fetchAll();

       if(FALSE === $delete_planned_sdetails)
       {
         $return["status"] = FAILURE;
         $return["data"]   = "";
       }
       else if(count($delete_planned_sdetails) <= 0)
       {
         $return["status"] = DB_NO_RECORD;
         $return["data"]   = "";
       }
       else
       {
         $return["status"] = DB_RECORD_ALREADY_EXISTS;
         $return["data"]   = $delete_planned_sdetails;
       }
     }
     catch(PDOException $e)
     {
       // Log the error
       $return["status"] = FAILURE;
       $return["data"] = "";
     }

     return $return;
   }


   function db_get_planned_data($project_planned_data) {

     if(array_key_exists("project_id",$project_planned_data))
     {
       $project_id = $project_planned_data["project_id"];
     }
     else
     {
       $project_id= "";
     }

     if(array_key_exists("road_id",$project_planned_data))
     {
       $road_id = $project_planned_data["road_id"];
     }
     else
     {
       $road_id= "";
     }

     if(array_key_exists("task_id",$project_planned_data))
     {
       $task_id = $project_planned_data["task_id"];
     }
     else
     {
       $task_id= "";
     }

     if(array_key_exists("process_id",$project_planned_data))
     {
       $process_id = $project_planned_data["process_id"];
     }
     else
     {
       $process_id= "";
     }

     $get_planned_data_squery_base = "select * from budget_planning";

     $get_planned_data_squery_where = "";


     $filter_count = 0;

     // Data
     $get_planned_data_list_sdata = array();

     if($project_id != "")
     {
       if($filter_count == 0)
       {
         // Query
         $get_planned_data_squery_where = $get_planned_data_squery_where." where planned_project_id = :project_id";
       }
       else
       {
         // Query
         $get_planned_data_squery_where = $get_planned_data_squery_where." and planned_project_id = :project_id";
       }

       // Data
       $get_planned_data_list_sdata[':project_id'] = $project_id;

       $filter_count++;
     }

     if($task_id != "")
     {
       if($filter_count == 0)
       {
         // Query
         $get_planned_data_squery_where = $get_planned_data_squery_where." where planned_task_id = :task_id";
       }
       else
       {
         // Query
         $get_planned_data_squery_where = $get_planned_data_squery_where." and planned_task_id = :task_id";
       }

       // Data
       $get_planned_data_list_sdata[':task_id'] = $task_id;

       $filter_count++;
     }

     if($road_id != "")
     {
       if($filter_count == 0)
       {
         // Query
         $get_planned_data_squery_where = $get_planned_data_squery_where." where planned_road_id = :road_id";
       }
       else
       {
         // Query
         $get_planned_data_squery_where = $get_planned_data_squery_where." and planned_road_id = :road_id";
       }

       // Data
       $get_planned_data_list_sdata[':road_id'] = $road_id;

       $filter_count++;
     }

     if($process_id != "")
     {
       if($filter_count == 0)
       {
         // Query
         $get_planned_data_squery_where = $get_planned_data_squery_where." where planned_process_id = :process_id";
       }
       else
       {
         // Query
         $get_planned_data_squery_where = $get_planned_data_squery_where." and planned_process_id = :process_id";
       }

       // Data
       $get_planned_data_list_sdata[':process_id'] = $process_id;

       $filter_count++;
     }

     $delete_planned_data_squery = $get_planned_data_squery_base.$get_planned_data_squery_where;

     try
     {
       $dbConnection = get_conn_handle();

       $delete_planned_sstatement = $dbConnection->prepare($delete_planned_data_squery);

       $delete_planned_sstatement -> execute($get_planned_data_list_sdata);

       $delete_planned_sdetails = $delete_planned_sstatement -> fetchAll();

       if(FALSE === $delete_planned_sdetails)
       {
         $return["status"] = FAILURE;
         $return["data"]   = "";
       }
       else if(count($delete_planned_sdetails) <= 0)
       {
         $return["status"] = DB_NO_RECORD;
         $return["data"]   = "";
       }
       else
       {
         $return["status"] = DB_RECORD_ALREADY_EXISTS;
         $return["data"]   = $delete_planned_sdetails;
       }
     }
     catch(PDOException $e)
     {
       // Log the error
       $return["status"] = FAILURE;
       $return["data"] = "";
     }

     return $return;
   }
   /*
   PURPOSE : To add Project actual material data
   INPUT 	: Project ID, Process ID,Mp,Mc,Cw added by and added on
   OUTPUT 	: Palnned ID, success or failure message
   BY 		: Soankshi
   */
   function db_add_actual_material($indent_id,$issue_id,$project_id,$process_id,$task_id,$road_id,$material_id,$machine_id,$qty,$remarks,$added_by)
   {
     // Query
      $project_actual_material_iquery = "insert into project_actual_material
      (actual_material_indent_id,actual_material_issue_item_id,actual_material_project_id,actual_material_process_id,actual_material_task_id,actual_material_road_id,actual_material_master_id,actual_material_machine_id,actual_material_qty,actual_material_remarks,actual_material_added_by,actual_material_added_on)
      values(:indent_id,:issue_id,:project_id,:process_id,:task_id,:road_id,:material_id,:machine_id,:qty,:remarks,:added_by,:added_on)";
       try
       {
           $dbConnection = get_conn_handle();
           $project_actual_material_istatement = $dbConnection->prepare($project_actual_material_iquery);

           // Data
           $project_actual_material_idata = array(':indent_id'=>$indent_id,':issue_id'=>$issue_id,':project_id'=>$project_id,':process_id'=>$process_id,':task_id'=>$task_id,':road_id'=>$road_id,':material_id'=>$material_id,':machine_id'=>$machine_id,':qty'=>$qty,':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));
           $dbConnection->beginTransaction();
           $project_actual_material_istatement->execute($project_actual_material_idata);
           $actual_material_id = $dbConnection->lastInsertId();
           $dbConnection->commit();

           $return["status"] = SUCCESS;
           $return["data"]   = $actual_material_id;
       }
       catch(PDOException $e)
       {
           // Log the error
           $return["status"] = FAILURE;
       $return["data"]   = "";
       }

       return $return;
   }
   /*
   PURPOSE : To get Project budget details
   INPUT 	: Project Id
   OUTPUT 	: List of data
   BY 		: Soankshi
   */
   function db_get_project_budget_planned_material($project_budget_planned_matreial_search_data)
   {
     if(array_key_exists("task_id",$project_budget_planned_matreial_search_data))
     {
       $task_id = $project_budget_planned_matreial_search_data["task_id"];
     }
     else
     {
       $task_id= "";
     }

     if(array_key_exists("road_id",$project_budget_planned_matreial_search_data))
     {
       $road_id = $project_budget_planned_matreial_search_data["road_id"];
     }
     else
     {
       $road_id= "";
     }

     if(array_key_exists("project_id",$project_budget_planned_matreial_search_data))
     {
       $project_id = $project_budget_planned_matreial_search_data["project_id"];
     }
     else
     {
       $project_id= "";
     }

     if(array_key_exists("active",$project_budget_planned_matreial_search_data))
     {
       $active = $project_budget_planned_matreial_search_data["active"];
     }
     else
     {
       $active= "";
     }


     $get_project_budget_planned_material_list_squery_base = "select *,sum(total_amt) as total_amt from actual_material";

     $get_project_budget_planned_material_list_squery_where = "";
     $filter_count = 0;

     // Data
     $get_project_budget_planned_material_list_sdata = array();

     if($task_id != "")
     {
       if($filter_count == 0)
       {
         // Query
         $get_project_budget_planned_material_list_squery_where = $get_project_budget_planned_material_list_squery_where." where actual_material_task_id = :task_id";
       }
       else
       {
         // Query
         $get_project_budget_planned_material_list_squery_where = $get_project_budget_planned_material_list_squery_where." and actual_material_task_id = :task_id";
       }

       // Data
       $get_project_budget_planned_material_list_sdata[':task_id'] = $task_id;

       $filter_count++;
     }

     if($road_id != "")
     {
       if($filter_count == 0)
       {
         // Query
         $get_project_budget_planned_material_list_squery_where = $get_project_budget_planned_material_list_squery_where." where actual_material_road_id = :road_id";
       }
       else
       {
         // Query
         $get_project_budget_planned_material_list_squery_where = $get_project_budget_planned_material_list_squery_where." and actual_material_road_id = :road_id";
       }

       // Data
       $get_project_budget_planned_material_list_sdata[':road_id'] = $road_id;

       $filter_count++;
     }

     if($project_id != "")
     {
       if($filter_count == 0)
       {
         // Query
         $get_project_budget_planned_material_list_squery_where = $get_project_budget_planned_material_list_squery_where." where actual_material_project_id = :project_id";
       }
       else
       {
         // Query
         $get_project_budget_planned_material_list_squery_where = $get_project_budget_planned_material_list_squery_where." and actual_material_project_id = :project_id";
       }

       // Data
       $get_project_budget_planned_material_list_sdata[':project_id'] = $project_id;

       $filter_count++;
     }

     if($active != "")
     {
       if($filter_count == 0)
       {
         // Query
         $get_project_budget_planned_material_list_squery_where = $get_project_budget_planned_material_list_squery_where." where active = :active";
       }
       else
       {
         // Query
         $get_project_budget_planned_material_list_squery_where = $get_project_budget_planned_material_list_squery_where." and active = :active";
       }

       // Data
       $get_project_budget_planned_material_list_sdata[':active'] = $active;

       $filter_count++;
     }

     $get_project_budget_planned_material_list_squery = $get_project_budget_planned_material_list_squery_base.$get_project_budget_planned_material_list_squery_where;
     try
     {
       $dbConnection = get_conn_handle();

       $get_project_budget_planned_material_list_sstatement = $dbConnection->prepare($get_project_budget_planned_material_list_squery);

       $get_project_budget_planned_material_list_sstatement -> execute($get_project_budget_planned_material_list_sdata);

       $get_project_budget_planned_material_list_sdetails = $get_project_budget_planned_material_list_sstatement -> fetchAll();

       if(FALSE === $get_project_budget_planned_material_list_sdetails)
       {
         $return["status"] = FAILURE;
         $return["data"]   = "";
       }
       else if(count($get_project_budget_planned_material_list_sdetails) <= 0)
       {
         $return["status"] = DB_NO_RECORD;
         $return["data"]   = "";
       }
       else
       {
         $return["status"] = DB_RECORD_ALREADY_EXISTS;
         $return["data"]   = $get_project_budget_planned_material_list_sdetails;
       }
     }
     catch(PDOException $e)
     {
       // Log the error
       $return["status"] = FAILURE;
       $return["data"] = "";
     }

     return $return;
    }

    function db_get_project_process_task_list($process_id) {
      $query = "select * from project_process_task_list where `project_process_id` = ".$process_id;
        $query .= " ORDER BY project_task_master_order ASC";

        $get_project_task_list_sdata = array();
      try
      {
        $dbConnection = get_conn_handle();

        $get_project_task_list_sstatement = $dbConnection->prepare($query);

        $get_project_task_list_sstatement -> execute($get_project_task_list_sdata);

        $get_project_task_list_sdetails = $get_project_task_list_sstatement -> fetchAll();

        if(FALSE === $get_project_task_list_sdetails)
        {
          $return["status"] = FAILURE;
          $return["data"]   = "";
        }
        else if(count($get_project_task_list_sdetails) <= 0)
        {
          $return["status"] = DB_NO_RECORD;
          $return["data"]   = "";
        }
        else
        {
          $return["status"] = DB_RECORD_ALREADY_EXISTS;
          $return["data"]   = $get_project_task_list_sdetails;
        }
      }
      catch(PDOException $e)
      {
        // Log the error
        $return["status"] = FAILURE;
        $return["data"] = "";
      }

      return $return;
    }

  ?>
