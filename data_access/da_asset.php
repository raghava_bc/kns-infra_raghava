<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
LIST OF TBDs
1. Wherever there is name search, there should be a wildcard
*/

/*
PURPOSE : To add new Asset Master
INPUT 	: Material ID, Remarks, Added By
OUTPUT 	: Asset ID, success or failure message
BY 		: Lakshmi
*/
function db_add_asset_master($material_id,$remarks,$added_by)
{
	// Query
   $asset_master_iquery = "insert into asset_master 
   (asset_master_material_id,asset_master_active,asset_master_remarks,asset_master_added_by,asset_master_added_on)values(:material_id,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $asset_master_istatement = $dbConnection->prepare($asset_master_iquery);
        
        // Data
        $asset_master_idata = array(':material_id'=>$material_id,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $asset_master_istatement->execute($asset_master_idata);
		$asset_master_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $asset_master_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Asset Master List
INPUT 	: Asset ID, Material ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Asset Master
BY 		: Lakshmi
*/
function db_get_asset_master($asset_master_search_data)
{  
	if(array_key_exists("asset_id",$asset_master_search_data))
	{
		$asset_id = $asset_master_search_data["asset_id"];
	}
	else
	{
		$asset_id= "";
	}
	
	if(array_key_exists("material_id",$asset_master_search_data))
	{
		$material_id = $asset_master_search_data["material_id"];
	}
	else
	{
		$material_id = "";
	}
	
	if(array_key_exists("active",$asset_master_search_data))
	{
		$active = $asset_master_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$asset_master_search_data))
	{
		$added_by = $asset_master_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$asset_master_search_data))
	{
		$start_date= $asset_master_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$asset_master_search_data))
	{
		$end_date= $asset_master_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_asset_master_list_squery_base = "select * from asset_master AM inner join users U on U.user_id = AM.asset_master_added_by inner join stock_material_master SMM on SMM.stock_material_id=AM.asset_master_material_id";
	
	$get_asset_master_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_asset_master_list_sdata = array();
	
	if($asset_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_master_list_squery_where = $get_asset_master_list_squery_where." where asset_master_id = :asset_id";								
		}
		else
		{
			// Query
			$get_asset_master_list_squery_where = $get_asset_master_list_squery_where." and asset_master_id = :asset_id";				
		}
		
		// Data
		$get_asset_master_list_sdata[':asset_id'] = $asset_id;
		
		$filter_count++;
	}
	
	if($material_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_master_list_squery_where = $get_asset_master_list_squery_where." where asset_master_material_id = :material_id";								
		}
		else
		{
			// Query
			$get_asset_master_list_squery_where = $get_asset_master_list_squery_where." and asset_master_material_id = :material_id";				
		}
		
		// Data
		$get_asset_master_list_sdata[':material_id'] = $material_id;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_master_list_squery_where = $get_asset_master_list_squery_where." where asset_master_active = :active";								
		}
		else
		{
			// Query
			$get_asset_master_list_squery_where = $get_asset_master_list_squery_where." and asset_master_active = :active";				
		}
		
		// Data
		$get_asset_master_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_master_list_squery_where = $get_asset_master_list_squery_where." where asset_master_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_asset_master_list_squery_where = $get_asset_master_list_squery_where." and asset_master_added_by = :added_by";
		}
		
		//Data
		$get_asset_master_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_master_list_squery_where = $get_asset_master_list_squery_where." where asset_master_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_asset_master_list_squery_where = $get_asset_master_list_squery_where." and asset_master_added_on >= :start_date";				
		}
		
		//Data
		$get_asset_master_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_master_list_squery_where = $get_asset_master_list_squery_where." where asset_master_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_asset_master_list_squery_where = $get_asset_master_list_squery_where." and asset_master_added_on <= :end_date";
		}
		
		//Data
		$get_asset_master_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_asset_master_list_squery = $get_asset_master_list_squery_base.$get_asset_master_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_asset_master_list_sstatement = $dbConnection->prepare($get_asset_master_list_squery);
		
		$get_asset_master_list_sstatement -> execute($get_asset_master_list_sdata);
		
		$get_asset_master_list_sdetails = $get_asset_master_list_sstatement -> fetchAll();
		
		if(FALSE === $get_asset_master_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_asset_master_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_asset_master_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update Asset Master 
INPUT 	: Asset ID, Asset Master Update Array
OUTPUT 	: Asset ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_asset_master($asset_id,$asset_master_update_data)
{
	if(array_key_exists("material_id",$asset_master_update_data))
	{	
		$material_id = $asset_master_update_data["material_id"];
	}
	else
	{
		$material_id = "";
	}
	
	if(array_key_exists("active",$asset_master_update_data))
	{	
		$active = $asset_master_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$asset_master_update_data))
	{	
		$remarks = $asset_master_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$asset_master_update_data))
	{	
		$added_by = $asset_master_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$asset_master_update_data))
	{	
		$added_on = $asset_master_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $asset_master_update_uquery_base = "update asset_master set ";  
	
	$asset_master_update_uquery_set = "";
	
	$asset_master_update_uquery_where = " where asset_master_id = :asset_id";
	
	$asset_master_update_udata = array(":asset_id"=>$asset_id);
	
	$filter_count = 0;
	
	if($material_id != "")
	{
		$asset_master_update_uquery_set = $asset_master_update_uquery_set." asset_master_material_id = :material_id,";
		$asset_master_update_udata[":material_id"] = $material_id;
		$filter_count++;
	}
	
	if($active != "")
	{
		$asset_master_update_uquery_set = $asset_master_update_uquery_set." asset_master_active = :active,";
		$asset_master_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$asset_master_update_uquery_set = $asset_master_update_uquery_set." asset_master_remarks = :remarks,";
		$asset_master_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$asset_master_update_uquery_set = $asset_master_update_uquery_set." asset_master_added_by = :added_by,";
		$asset_master_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$asset_master_update_uquery_set = $asset_master_update_uquery_set." asset_master_added_on = :added_on,";
		$asset_master_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$asset_master_update_uquery_set = trim($asset_master_update_uquery_set,',');
	}
	
	$asset_master_update_uquery = $asset_master_update_uquery_base.$asset_master_update_uquery_set.$asset_master_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $asset_master_update_ustatement = $dbConnection->prepare($asset_master_update_uquery);		
        
        $asset_master_update_ustatement -> execute($asset_master_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $asset_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new Asset Type Master
INPUT 	: Asset Type, Remarks, Added By
OUTPUT 	: Asset Type ID, success or failure message
BY 		: Lakshmi
*/
function db_add_asset_type_master($asset_type,$remarks,$added_by)
{
	// Query
   $asset_type_master_iquery = "insert into asset_type_master(asset_type_master_asset_type,asset_type_master_active,asset_type_master_remarks,asset_type_master_added_by,asset_type_master_added_on) values(:asset_type,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $asset_type_master_istatement = $dbConnection->prepare($asset_type_master_iquery);
        
        // Data
        $asset_type_master_idata = array(':asset_type'=>$asset_type,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $asset_type_master_istatement->execute($asset_type_master_idata);
		$asset_type_master_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $asset_type_master_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Asset Type Master List
INPUT 	: Asset Type ID, Material ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Asset Type Master
BY 		: Lakshmi
*/
function db_get_asset_type_master($asset_type_master_search_data)
{  
	if(array_key_exists("asset_type_id",$asset_type_master_search_data))
	{
		$asset_type_id = $asset_type_master_search_data["asset_type_id"];
	}
	else
	{
		$asset_type_id= "";
	}
	
	if(array_key_exists("asset_type",$asset_type_master_search_data))
	{
		$asset_type = $asset_type_master_search_data["asset_type"];
	}
	else
	{
		$asset_type = "";
	}
	
	if(array_key_exists("active",$asset_type_master_search_data))
	{
		$active = $asset_type_master_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$asset_type_master_search_data))
	{
		$added_by = $asset_type_master_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$asset_type_master_search_data))
	{
		$start_date= $asset_type_master_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$asset_type_master_search_data))
	{
		$end_date= $asset_type_master_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_asset_type_master_list_squery_base = "select * from asset_type_master ATM inner join users U on U.user_id = ATM.asset_type_master_added_by";
	
	$get_asset_type_master_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_asset_type_master_list_sdata = array();
	
	if($asset_type_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_type_master_list_squery_where = $get_asset_type_master_list_squery_where." where asset_type_master_id = :asset_type_id";								
		}
		else
		{
			// Query
			$get_asset_type_master_list_squery_where = $get_asset_type_master_list_squery_where." and asset_type_master_id = :asset_type_id";				
		}
		
		// Data
		$get_asset_type_master_list_sdata[':asset_type_id'] = $asset_type_id;
		
		$filter_count++;
	}
	
	if($asset_type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_type_master_list_squery_where = $get_asset_type_master_list_squery_where." where asset_type_master_asset_type = :asset_type";								
		}
		else
		{
			// Query
			$get_asset_type_master_list_squery_where = $get_asset_type_master_list_squery_where." and asset_type_master_asset_type = :asset_type";				
		}
		
		// Data
		$get_asset_type_master_list_sdata[':asset_type'] = $asset_type;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_type_master_list_squery_where = $get_asset_type_master_list_squery_where." where asset_type_master_active = :active";								
		}
		else
		{
			// Query
			$get_asset_type_master_list_squery_where = $get_asset_type_master_list_squery_where." and asset_type_master_active = :active";				
		}
		
		// Data
		$get_asset_type_master_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_type_master_list_squery_where = $get_asset_type_master_list_squery_where." where asset_type_master_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_asset_type_master_list_squery_where = $get_asset_type_master_list_squery_where." and asset_type_master_added_by = :added_by";
		}
		
		//Data
		$get_asset_type_master_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_type_master_list_squery_where = $get_asset_type_master_list_squery_where." where asset_type_master_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_asset_type_master_list_squery_where = $get_asset_type_master_list_squery_where." and asset_type_master_added_on >= :start_date";				
		}
		
		//Data
		$get_asset_type_master_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_type_master_list_squery_where = $get_asset_type_master_list_squery_where." where asset_type_master_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_asset_type_master_list_squery_where = $get_asset_type_master_list_squery_where." and asset_type_master_added_on <= :end_date";
		}
		
		//Data
		$get_asset_type_master_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_asset_type_master_list_squery = $get_asset_type_master_list_squery_base.$get_asset_type_master_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_asset_type_master_list_sstatement = $dbConnection->prepare($get_asset_type_master_list_squery);
		
		$get_asset_type_master_list_sstatement -> execute($get_asset_type_master_list_sdata);
		
		$get_asset_type_master_list_sdetails = $get_asset_type_master_list_sstatement -> fetchAll();
		
		if(FALSE === $get_asset_type_master_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_asset_type_master_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_asset_type_master_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update Asset Type Master 
INPUT 	: Asset Type ID, Asset Type Master Update Array
OUTPUT 	: Asset Type ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_asset_type_master($asset_type_id,$asset_type_master_update_data)
{
	if(array_key_exists("asset_type",$asset_type_master_update_data))
	{	
		$asset_type = $asset_type_master_update_data["asset_type"];
	}
	else
	{
		$asset_type = "";
	}
	
	if(array_key_exists("active",$asset_type_master_update_data))
	{	
		$active = $asset_type_master_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$asset_type_master_update_data))
	{	
		$remarks = $asset_type_master_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$asset_type_master_update_data))
	{	
		$added_by = $asset_type_master_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$asset_type_master_update_data))
	{	
		$added_on = $asset_type_master_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $asset_type_master_update_uquery_base = "update asset_type_master set ";  
	
	$asset_type_master_update_uquery_set = "";
	
	$asset_type_master_update_uquery_where = " where asset_type_master_id = :asset_type_id";
	
	$asset_type_master_update_udata = array(":asset_type_id"=>$asset_type_id);
	
	$filter_count = 0;
	
	if($asset_type != "")
	{
		$asset_type_master_update_uquery_set = $asset_type_master_update_uquery_set." asset_type_master_asset_type = :asset_type,";
		$asset_type_master_update_udata[":asset_type"] = $asset_type;
		$filter_count++;
	}
	
	if($active != "")
	{
		$asset_type_master_update_uquery_set = $asset_type_master_update_uquery_set." asset_type_master_active = :active,";
		$asset_type_master_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$asset_type_master_update_uquery_set = $asset_type_master_update_uquery_set." asset_type_master_remarks = :remarks,";
		$asset_type_master_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$asset_type_master_update_uquery_set = $asset_type_master_update_uquery_set." asset_type_master_added_by = :added_by,";
		$asset_type_master_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$asset_type_master_update_uquery_set = $asset_type_master_update_uquery_set." asset_type_master_added_on = :added_on,";
		$asset_type_master_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$asset_type_master_update_uquery_set = trim($asset_type_master_update_uquery_set,',');
	}
	
	$asset_type_master_update_uquery = $asset_type_master_update_uquery_base.$asset_type_master_update_uquery_set.$asset_type_master_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();
        
        $asset_type_master_update_ustatement = $dbConnection->prepare($asset_type_master_update_uquery);		
        
        $asset_type_master_update_ustatement -> execute($asset_type_master_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $asset_type_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new Asset Details
INPUT 	: Asset Master ID, Asset Type ID, Vendor ID, Invoice NO, Invoice Amount, Invoice Date, Other Charges, Location, Serial NO, Units, On Date, Life Of asset,
          Asset Used Till, Life Form, Depreciation Rate, Depreciation Amount, Wdv as on, Status, Remarks, Added By
OUTPUT 	: Details ID, success or failure message
BY 		: Lakshmi
*/
function db_add_asset_details($asset_master_id,$asset_type_id,$vendor_id,$invoice_no,$invoice_amount,$invoice_date,$other_charges,$location,$serial_no,$units,$on_date,$life_of_asset,$asset_used_till,$life_form,$depreciation_rate,$depreciation_amount,$wdv_as_on,$status,$remarks,$added_by)
{
	// Query
   $asset_details_iquery = "insert into asset_details
   (asset_details_asset_id,asset_details_type_id,asset_details_vendor_id,asset_details_invoice_no,asset_details_invoice_amount,asset_details_invoice_date,
   asset_details_other_charges,asset_details_location,asset_details_serial_no,asset_details_no_of_units,asset_details_capitalization_on_date,asset_details_life_of_asset,
   asset_details_life_of_asset_used_till,asset_details_remaining_life_from,asset_details_depreciation_rate,asset_details_depreciation_amount,asset_details_wdv_as_on,
   asset_details_status,asset_details_active,asset_details_remarks,asset_details_added_by,asset_details_added_on)
   values(:asset_master_id,:asset_type_id,:vendor_id,:invoice_no,:invoice_amount,:invoice_date,:other_charges,:location,:serial_no,:units,:on_date,:life_of_asset,
   :asset_used_till,:life_form,:depreciation_rate,:depreciation_amount,:wdv_as_on,:status,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $asset_details_istatement = $dbConnection->prepare($asset_details_iquery);
        
        // Data
        $asset_details_idata = array(':asset_master_id'=>$asset_master_id,':asset_type_id'=>$asset_type_id,':vendor_id'=>$vendor_id,
		':invoice_no'=>$invoice_no,':invoice_amount'=>$invoice_amount,':invoice_date'=>$invoice_date,':other_charges'=>$other_charges,':location'=>$location,':serial_no'=>$serial_no,
		':units'=>$units,':on_date'=>$on_date,':life_of_asset'=>$life_of_asset,':asset_used_till'=>$asset_used_till,':life_form'=>$life_form,':depreciation_rate'=>$depreciation_rate,
		':depreciation_amount'=>$depreciation_amount,':wdv_as_on'=>$wdv_as_on,':status'=>$status,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	  
		$dbConnection->beginTransaction();
        $asset_details_istatement->execute($asset_details_idata);
		$asset_details_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $asset_details_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Asset Details list
INPUT 	: Details ID, Asset Master ID, Asset Type ID, Vendor ID, Invoice NO, Invoice Amount, Invoice Date, Other Changes, Location, Serial NO, Units, On Date, Life Of asset,
          Asset Used Till, Life Form, Depreciation Rate, Depreciation Amount, Wdv as on, Status, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Asset Details
BY 		: Lakshmi
*/
function db_get_asset_details($asset_details_search_data)
{  
 
	if(array_key_exists("details_id",$asset_details_search_data))
	{
		$details_id = $asset_details_search_data["details_id"];
	}
	else
	{
		$details_id= "";
	}
	
	if(array_key_exists("asset_master_id",$asset_details_search_data))
	{
		$asset_master_id = $asset_details_search_data["asset_master_id"];
	}
	else
	{
		$asset_master_id = "";
	}
	
	if(array_key_exists("asset_type_id",$asset_details_search_data))
	{
		$asset_type_id = $asset_details_search_data["asset_type_id"];
	}
	else
	{
		$asset_type_id = "";
	}
	
	if(array_key_exists("vendor_id",$asset_details_search_data))
	{
		$vendor_id = $asset_details_search_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}
	
	if(array_key_exists("invoice_no",$asset_details_search_data))
	{
		$invoice_no = $asset_details_search_data["invoice_no"];
	}
	else
	{
		$invoice_no = "";
	}
	
	if(array_key_exists("invoice_amount",$asset_details_search_data))
	{
		$invoice_amount = $asset_details_search_data["invoice_amount"];
	}
	else
	{
		$invoice_amount = "";
	}
	
	if(array_key_exists("invoice_date",$asset_details_search_data))
	{
		$invoice_date = $asset_details_search_data["invoice_date"];
	}
	else
	{
		$invoice_date = "";
	}
	
	if(array_key_exists("other_charges",$asset_details_search_data))
	{
		$other_charges = $asset_details_search_data["other_charges"];
	}
	else
	{
		$other_charges = "";
	}
	
	if(array_key_exists("location",$asset_details_search_data))
	{
		$location = $asset_details_search_data["location"];
	}
	else
	{
		$location = "";
	}
	
	if(array_key_exists("serial_no",$asset_details_search_data))
	{
		$serial_no = $asset_details_search_data["serial_no"];
	}
	else
	{
		$serial_no = "";
	}
	
	if(array_key_exists("units",$asset_details_search_data))
	{
		$units = $asset_details_search_data["units"];
	}
	else
	{
		$units = "";
	}
	
	if(array_key_exists("on_date",$asset_details_search_data))
	{
		$on_date = $asset_details_search_data["on_date"];
	}
	else
	{
		$on_date = "";
	}
	
	if(array_key_exists("life_of_asset",$asset_details_search_data))
	{
		$life_of_asset = $asset_details_search_data["life_of_asset"];
	}
	else
	{
		$life_of_asset = "";
	}
	
	if(array_key_exists("asset_used_till",$asset_details_search_data))
	{
		$asset_used_till = $asset_details_search_data["asset_used_till"];
	}
	else
	{
		$asset_used_till = "";
	}
	
	if(array_key_exists("life_form",$asset_details_search_data))
	{
		$life_form = $asset_details_search_data["life_form"];
	}
	else
	{
		$life_form = "";
	}
	
	if(array_key_exists("depreciation_rate",$asset_details_search_data))
	{
		$depreciation_rate = $asset_details_search_data["depreciation_rate"];
	}
	else
	{
		$depreciation_rate = "";
	}
	
	if(array_key_exists("depreciation_amount",$asset_details_search_data))
	{
		$depreciation_amount = $asset_details_search_data["depreciation_amount"];
	}
	else
	{
		$depreciation_amount = "";
	}
	
	if(array_key_exists("wdv_as_on",$asset_details_search_data))
	{
		$wdv_as_on = $asset_details_search_data["wdv_as_on"];
	}
	else
	{
		$wdv_as_on = "";
	}
	
	if(array_key_exists("status",$asset_details_search_data))
	{
		$status = $asset_details_search_data["status"];
	}
	else
	{
		$status = "";
	}
	
	if(array_key_exists("active",$asset_details_search_data))
	{
		$active = $asset_details_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$asset_details_search_data))
	{
		$added_by = $asset_details_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$asset_details_search_data))
	{
		$start_date= $asset_details_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$asset_details_search_data))
	{
		$end_date= $asset_details_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_asset_details_list_squery_base = "select * from asset_details AD inner join users U on U.user_id = AD.asset_details_added_by inner join asset_type_master ATM on ATM.asset_type_master_id=AD.asset_details_type_id inner join stock_vendor_master SVM on SVM.stock_vendor_id=AD.asset_details_vendor_id inner join stock_location_master SLM on SLM.stock_location_id=AD.asset_details_location inner join asset_master AM on AM.asset_master_id=AD.asset_details_asset_id inner join stock_material_master SMM on SMM.stock_material_id=AM.asset_master_material_id";
	
	$get_asset_details_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_asset_details_list_sdata = array();
	
	if($details_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_id = :details_id";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_id = :details_id";				
		}
		
		// Data
		$get_asset_details_list_sdata[':details_id'] = $details_id;
		
		$filter_count++;
	}
	
	if($asset_master_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_asset_id = :asset_master_id";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_asset_id = :asset_master_id";				
		}
		
		// Data
		$get_asset_details_list_sdata[':asset_master_id'] = $asset_master_id;
		
		$filter_count++;
	}
	
	if($asset_type_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_type_id = :asset_type_id";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_type_id = :asset_type_id";				
		}
		
		// Data
		$get_asset_details_list_sdata[':asset_type_id'] = $asset_type_id;
		
		$filter_count++;
	}
	
	if($vendor_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_vendor_id = :vendor_id";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_vendor_id = :vendor_id";				
		}
		
		// Data
		$get_asset_details_list_sdata[':vendor_id'] = $vendor_id;
		
		$filter_count++;
	}
	
	if($invoice_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_invoice_no = :invoice_no";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_invoice_no = :invoice_no";				
		}
		
		// Data
		$get_asset_details_list_sdata[':invoice_no'] = $invoice_no;
		
		$filter_count++;
	}
	
	if($invoice_amount != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_invoice_amount = :invoice_amount";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_invoice_amount = :invoice_amount";				
		}
		
		// Data
		$get_asset_details_list_sdata[':invoice_amount'] = $invoice_amount;
		
		$filter_count++;
	}
	
	if($invoice_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_invoice_date = :invoice_date";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_invoice_date = :invoice_date";				
		}
		
		// Data
		$get_asset_details_list_sdata[':invoice_date'] = $invoice_date;
		
		$filter_count++;
	}
	
	if($other_charges != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_other_charges = :other_charges";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_other_charges = :other_charges";				
		}
		
		// Data
		$get_asset_details_list_sdata[':other_charges'] = $other_charges;
		
		$filter_count++;
	}
	
	if($location != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_location = :location";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_location = :location";				
		}
		
		// Data
		$get_asset_details_list_sdata[':location'] = $location;
		
		$filter_count++;
	}
	
	if($serial_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_serial_no = :serial_no";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_serial_no = :serial_no";				
		}
		
		// Data
		$get_asset_details_list_sdata[':serial_no'] = $serial_no;
		
		$filter_count++;
	}
	
	if($units != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_no_of_units = :units";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_no_of_units = :units";				
		}
		
		// Data
		$get_asset_details_list_sdata[':units'] = $units;
		
		$filter_count++;
	}
	
	if($on_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_capitalization_on_date = :on_date";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_capitalization_on_date = :on_date";				
		}
		
		// Data
		$get_asset_details_list_sdata[':on_date'] = $on_date;
		
		$filter_count++;
	}
	
	if($life_of_asset != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_life_of_asset = :life_of_asset";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_life_of_asset = :life_of_asset";				
		}
		
		// Data
		$get_asset_details_list_sdata[':life_of_asset'] = $life_of_asset;
		
		$filter_count++;
	}
	
	if($asset_used_till != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_life_of_asset_used_till = :asset_used_till";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_life_of_asset_used_till = :asset_used_till";				
		}
		
		// Data
		$get_asset_details_list_sdata[':asset_used_till']  = $asset_used_till;
		
		$filter_count++;
	}
	
	if($life_form != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_remaining_life_from = :life_form";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_remaining_life_from = :life_form";				
		}
		
		// Data
		$get_asset_details_list_sdata[':life_form']  = $life_form;
		
		$filter_count++;
	}
	
	if($depreciation_rate != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_depreciation_rate = :depreciation_rate";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_depreciation_rate = :depreciation_rate";				
		}
		
		// Data
		$get_asset_details_list_sdata[':depreciation_rate']  = $depreciation_rate;
		
		$filter_count++;
	}
	
	if($depreciation_amount != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_depreciation_amount = :depreciation_amount";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_depreciation_amount = :depreciation_amount";				
		}
		
		// Data
		$get_asset_details_list_sdata[':depreciation_amount']  = $depreciation_amount;
		
		$filter_count++;
	}
	
	if($wdv_as_on != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_wdv_as_on = :wdv_as_on";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_wdv_as_on = :wdv_as_on";				
		}
		
		// Data
		$get_asset_details_list_sdata[':wdv_as_on']  = $wdv_as_on;
		
		$filter_count++;
	}
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_status = :status";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_status = :status";				
		}
		
		// Data
		$get_asset_details_list_sdata[':status']  = $status;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_active = :active";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_active = :active";				
		}
		
		// Data
		$get_asset_details_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_added_by = :added_by";				
		}
		
		//Data
		$get_asset_details_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_added_on >= :start_date";				
		}
		
		//Data
		$get_asset_details_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." where asset_details_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_asset_details_list_squery_where = $get_asset_details_list_squery_where." and asset_details_added_on <= :end_date";				
		}
		
		//Data
		$get_asset_details_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_asset_details_list_squery = $get_asset_details_list_squery_base.$get_asset_details_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_asset_details_list_sstatement = $dbConnection->prepare($get_asset_details_list_squery);
		
		$get_asset_details_list_sstatement -> execute($get_asset_details_list_sdata);
		
		$get_asset_details_list_sdetails = $get_asset_details_list_sstatement -> fetchAll();
		
		if(FALSE === $get_asset_details_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_asset_details_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_asset_details_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
 /*
PURPOSE : To update Asset Details
INPUT 	: Details ID, Asset Details Update Array
OUTPUT 	: Details ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_asset_details($details_id,$asset_details_update_data)
{	
	if(array_key_exists("asset_master_id",$asset_details_update_data))
	{
		$asset_master_id = $asset_details_update_data["asset_master_id"];
	}
	else
	{
		$asset_master_id = "";
	}
	
	if(array_key_exists("asset_type_id",$asset_details_update_data))
	{
		$asset_type_id = $asset_details_update_data["asset_type_id"];
	}
	else
	{
		$asset_type_id = "";
	}
	
	if(array_key_exists("vendor_id",$asset_details_update_data))
	{
		$vendor_id = $asset_details_update_data["vendor_id"];
	}
	else
	{
		$vendor_id = "";
	}
	
	if(array_key_exists("invoice_no",$asset_details_update_data))
	{
		$invoice_no = $asset_details_update_data["invoice_no"];
	}
	else
	{
		$invoice_no = "";
	}
	
	if(array_key_exists("invoice_amount",$asset_details_update_data))
	{
		$invoice_amount = $asset_details_update_data["invoice_amount"];
	}
	else
	{
		$invoice_amount = "";
	}
	
	if(array_key_exists("invoice_date",$asset_details_update_data))
	{
		$invoice_date = $asset_details_update_data["invoice_date"];
	}
	else
	{
		$invoice_date = "";
	}
	
	if(array_key_exists("other_charges",$asset_details_update_data))
	{
		$other_charges = $asset_details_update_data["other_charges"];
	}
	else
	{
		$other_charges = "";
	}
	
	if(array_key_exists("location",$asset_details_update_data))
	{
		$location = $asset_details_update_data["location"];
	}
	else
	{
		$location = "";
	}
	
	if(array_key_exists("serial_no",$asset_details_update_data))
	{
		$serial_no = $asset_details_update_data["serial_no"];
	}
	else
	{
		$serial_no = "";
	}
	
	if(array_key_exists("units",$asset_details_update_data))
	{
		$units = $asset_details_update_data["units"];
	}
	else
	{
		$units = "";
	}
	
	if(array_key_exists("on_date",$asset_details_update_data))
	{
		$on_date = $asset_details_update_data["on_date"];
	}
	else
	{
		$on_date = "";
	}
	
	if(array_key_exists("life_of_asset",$asset_details_update_data))
	{
		$life_of_asset = $asset_details_update_data["life_of_asset"];
	}
	else
	{
		$life_of_asset = "";
	}
	
	if(array_key_exists("asset_used_till",$asset_details_update_data))
	{
		$asset_used_till = $asset_details_update_data["asset_used_till"];
	}
	else
	{
		$asset_used_till = "";
	}
	
	if(array_key_exists("life_form",$asset_details_update_data))
	{
		$life_form = $asset_details_update_data["life_form"];
	}
	else
	{
		$life_form = "";
	}
	
	if(array_key_exists("depreciation_rate",$asset_details_update_data))
	{
		$depreciation_rate = $asset_details_update_data["depreciation_rate"];
	}
	else
	{
		$depreciation_rate = "";
	}
	
	if(array_key_exists("depreciation_amount",$asset_details_update_data))
	{
		$depreciation_amount = $asset_details_update_data["depreciation_amount"];
	}
	else
	{
		$depreciation_amount = "";
	}
	
	if(array_key_exists("wdv_as_on",$asset_details_update_data))
	{
		$wdv_as_on = $asset_details_update_data["wdv_as_on"];
	}
	else
	{
		$wdv_as_on = "";
	}
	
	if(array_key_exists("status",$asset_details_update_data))
	{
		$status = $asset_details_update_data["status"];
	}
	else
	{
		$status = "";
	}
	
	if(array_key_exists("active",$asset_details_update_data))
	{
		$active = $asset_details_update_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("remarks",$asset_details_update_data))
	{
		$remarks = $asset_details_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$asset_details_update_data))
	{	
		$added_by = $asset_details_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$asset_details_update_data))
	{	
		$added_on = $asset_details_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $asset_details_update_uquery_base = "update asset_details set ";  
	
	$asset_details_update_uquery_set = "";
	
	$asset_details_update_uquery_where = " where asset_details_id = :details_id";
	
	$asset_details_update_udata = array(":details_id"=>$details_id);
	
	$filter_count = 0;
	
	if($asset_master_id != "")
	{
		$asset_details_update_uquery_set = $asset_details_update_uquery_set." asset_details_asset_id = :asset_master_id,";
		$asset_details_update_udata[":asset_master_id"] = $asset_master_id;
		$filter_count++;
	}
	
	if($asset_type_id != "")
	{
		$asset_details_update_uquery_set = $asset_details_update_uquery_set." asset_details_type_id = :asset_type_id,";
		$asset_details_update_udata[":asset_type_id"] = $asset_type_id;
		$filter_count++;
	}
	
	if($vendor_id != "")
	{
		$asset_details_update_uquery_set = $asset_details_update_uquery_set." asset_details_vendor_id = :vendor_id,";
		$asset_details_update_udata[":vendor_id"] = $vendor_id;
		$filter_count++;
	}
	
	if($invoice_no != "")
	{
		$asset_details_update_uquery_set = $asset_details_update_uquery_set." asset_details_invoice_no = :invoice_no,";
		$asset_details_update_udata[":invoice_no"] = $invoice_no;
		$filter_count++;
	}
	
	if($invoice_amount != "")
	{
		$asset_details_update_uquery_set = $asset_details_update_uquery_set." asset_details_invoice_amount = :invoice_amount,";
		$asset_details_update_udata[":invoice_amount"] = $invoice_amount;
		$filter_count++;
	}
	
	if($invoice_date != "")
	{
		$asset_details_update_uquery_set = $asset_details_update_uquery_set." asset_details_invoice_date = :invoice_date,";
		$asset_details_update_udata[":invoice_date"] = $invoice_date;
		$filter_count++;
	}
	
	if($other_charges != "")
	{
		$asset_details_update_uquery_set = $asset_details_update_uquery_set." asset_details_other_charges = :other_charges,";
		$asset_details_update_udata[":other_charges"] = $other_charges;
		$filter_count++;
	}
	
	if($location != "")
	{
		$asset_details_update_uquery_set = $asset_details_update_uquery_set." asset_details_location = :location,";
		$asset_details_update_udata[":location"] = $location;
		$filter_count++;
	}
	
	if($serial_no != "")
	{
		$asset_details_update_uquery_set = $asset_details_update_uquery_set." asset_details_serial_no = :serial_no,";
		$asset_details_update_udata[":serial_no"] = $serial_no;
		$filter_count++;
	}
	
	if($units != "")
	{
		$asset_details_update_uquery_set = $asset_details_update_uquery_set." asset_details_no_of_units = :units,";
		$asset_details_update_udata[":units"] = $units;
		$filter_count++;
	}
	
	if($on_date != "")
	{
		$asset_details_update_uquery_set = $asset_details_update_uquery_set." asset_details_capitalization_on_date = :on_date,";
		$asset_details_update_udata[":on_date"] = $on_date;
		$filter_count++;
	}
	
	if($life_of_asset != "")
	{
		$asset_details_update_uquery_set = $asset_details_update_uquery_set." asset_details_life_of_asset = :life_of_asset,";
		$asset_details_update_udata[":life_of_asset"] = $life_of_asset;
		$filter_count++;
	}
	
	if($asset_used_till != "")
	{
		$asset_details_update_uquery_set = $asset_details_update_uquery_set." asset_details_life_of_asset_used_till = :asset_used_till,";
		$asset_details_update_udata[":asset_used_till"] = $asset_used_till;
		$filter_count++;
	}
	
	if($life_form != "")
	{
		$asset_details_update_uquery_set = $asset_details_update_uquery_set." asset_details_remaining_life_from = :life_form,";
		$asset_details_update_udata[":life_form"] = $life_form;
		$filter_count++;
	}
	
	if($depreciation_rate != "")
	{
		$asset_details_update_uquery_set = $asset_details_update_uquery_set." asset_details_depreciation_rate = :depreciation_rate,";
		$asset_details_update_udata[":depreciation_rate"] = $depreciation_rate;
		$filter_count++;
	}
	
	if($depreciation_amount != "")
	{
		$asset_details_update_uquery_set = $asset_details_update_uquery_set." asset_details_depreciation_amount = :depreciation_amount,";
		$asset_details_update_udata[":depreciation_amount"] = $depreciation_amount;
		$filter_count++;
	}
	
	if($wdv_as_on != "")
	{
		$asset_details_update_uquery_set = $asset_details_update_uquery_set." asset_details_wdv_as_on = :wdv_as_on,";
		$asset_details_update_udata[":wdv_as_on"] = $wdv_as_on;
		$filter_count++;
	}
	
	if($status != "")
	{
		$asset_details_update_uquery_set = $asset_details_update_uquery_set." asset_details_status = :status,";
		$asset_details_update_udata[":status"] = $status;
		$filter_count++;
	}
	
	if($active != "")
	{
		$asset_details_update_uquery_set = $asset_details_update_uquery_set." asset_details_active = :active,";
		$asset_details_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$asset_details_update_uquery_set = $asset_details_update_uquery_set." asset_details_remarks = :remarks,";
		$asset_details_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$asset_details_update_uquery_set = $asset_details_update_uquery_set." asset_details_added_by = :added_by,";
		$asset_details_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$asset_details_update_uquery_set = $asset_details_update_uquery_set." asset_details_added_on = :added_on,";
		$asset_details_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$asset_details_update_uquery_set = trim($asset_details_update_uquery_set,',');
	}
	
	$asset_details_update_uquery = $asset_details_update_uquery_base.$asset_details_update_uquery_set.$asset_details_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();
        
        $asset_details_update_ustatement = $dbConnection->prepare($asset_details_update_uquery);		
        
        $asset_details_update_ustatement -> execute($asset_details_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $details_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
} 


/*
PURPOSE : To add new Asset Transfer
INPUT 	: Details ID, Source Location, Dest Location, Remarks, Added By
OUTPUT 	: Transfer ID, success or failure message
BY 		: Lakshmi
*/
function db_add_asset_transfer($details_id,$source_location,$dest_location,$remarks,$added_by)
{
	// Query
   $asset_transfer_iquery = "insert into asset_transfer 
   (asset_transfer_details_id,asset_transfer_source_location,asset_transfer_destination_location,asset_transfer_active,asset_transfer_remarks,
    asset_transfer_added_by,asset_transfer_added_on)values(:details_id,:source_location,:dest_location,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $asset_transfer_istatement = $dbConnection->prepare($asset_transfer_iquery);
        
        // Data
        $asset_transfer_idata = array(':details_id'=>$details_id,':source_location'=>$source_location,':dest_location'=>$dest_location,':active'=>'1',':remarks'=>$remarks,
		':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $asset_transfer_istatement->execute($asset_transfer_idata);
		$asset_transfer_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $asset_transfer_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Asset Transfer List
INPUT 	: Transfer ID, Details ID, Source Location, Dest Location, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Asset Transfer
BY 		: Lakshmi
*/
function db_get_asset_transfer($asset_transfer_search_data)
{  
	if(array_key_exists("transfer_id",$asset_transfer_search_data))
	{
		$transfer_id = $asset_transfer_search_data["transfer_id"];
	}
	else
	{
		$transfer_id= "";
	}
	
	if(array_key_exists("details_id",$asset_transfer_search_data))
	{
		$details_id = $asset_transfer_search_data["details_id"];
	}
	else
	{
		$details_id = "";
	}
	
	if(array_key_exists("source_location",$asset_transfer_search_data))
	{
		$source_location = $asset_transfer_search_data["source_location"];
	}
	else
	{
		$source_location = "";
	}
	
	if(array_key_exists("dest_location",$asset_transfer_search_data))
	{
		$dest_location = $asset_transfer_search_data["dest_location"];
	}
	else
	{
		$dest_location = "";
	}
	
	if(array_key_exists("active",$asset_transfer_search_data))
	{
		$active = $asset_transfer_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$asset_transfer_search_data))
	{
		$added_by = $asset_transfer_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$asset_transfer_search_data))
	{
		$start_date= $asset_transfer_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$asset_transfer_search_data))
	{
		$end_date= $asset_transfer_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_asset_transfer_list_squery_base = "select *,SL.stock_location_name as source_location,SLM.stock_location_name as dest_location from asset_transfer AT inner join users U on U.user_id = AT.asset_transfer_added_by inner join asset_details AD on AD.asset_details_id=AT.asset_transfer_details_id inner join asset_master AM on AM.asset_master_id=AD.asset_details_asset_id inner join stock_material_master SMM on SMM.stock_material_id=AM.asset_master_material_id inner join stock_location_master SLM on SLM.stock_location_id=AT.asset_transfer_destination_location inner join stock_location_master SL on SL.stock_location_id=AT.asset_transfer_source_location";
	
	$get_asset_transfer_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_asset_transfer_list_sdata = array();
	
	if($transfer_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_transfer_list_squery_where = $get_asset_transfer_list_squery_where." where asset_transfer_id = :transfer_id";								
		}
		else
		{
			// Query
			$get_asset_transfer_list_squery_where = $get_asset_transfer_list_squery_where." and asset_transfer_id = :transfer_id";				
		}
		
		// Data
		$get_asset_transfer_list_sdata[':transfer_id'] = $transfer_id;
		
		$filter_count++;
	}
	
	if($details_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_transfer_list_squery_where = $get_asset_transfer_list_squery_where." where asset_transfer_details_id = :details_id";								
		}
		else
		{
			// Query
			$get_asset_transfer_list_squery_where = $get_asset_transfer_list_squery_where." and asset_transfer_details_id = :details_id";				
		}
		
		// Data
		$get_asset_transfer_list_sdata[':details_id'] = $details_id;
		
		$filter_count++;
	}
	
	if($source_location != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_transfer_list_squery_where = $get_asset_transfer_list_squery_where." where asset_transfer_source_location = :source_location";								
		}
		else
		{
			// Query
			$get_asset_transfer_list_squery_where = $get_asset_transfer_list_squery_where." and asset_transfer_source_location = :source_location";				
		}
		
		// Data
		$get_asset_transfer_list_sdata[':source_location']  = $source_location;
		
		$filter_count++;
	}
	
	if($dest_location != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_transfer_list_squery_where = $get_asset_transfer_list_squery_where." where asset_transfer_destination_location = :dest_location";								
		}
		else
		{
			// Query
			$get_asset_transfer_list_squery_where = $get_asset_transfer_list_squery_where." and asset_transfer_destination_location = :dest_location";				
		}
		
		// Data
		$get_asset_transfer_list_sdata[':dest_location']  = $dest_location;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_transfer_list_squery_where = $get_asset_transfer_list_squery_where." where asset_transfer_active = :active";								
		}
		else
		{
			// Query
			$get_asset_transfer_list_squery_where = $get_asset_transfer_list_squery_where." and asset_transfer_active = :active";				
		}
		
		// Data
		$get_asset_transfer_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_transfer_list_squery_where = $get_asset_transfer_list_squery_where." where asset_transfer_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_asset_transfer_list_squery_where = $get_asset_transfer_list_squery_where." and asset_transfer_added_by = :added_by";
		}
		
		//Data
		$get_asset_transfer_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_transfer_list_squery_where = $get_asset_transfer_list_squery_where." where asset_transfer_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_asset_transfer_list_squery_where = $get_asset_transfer_list_squery_where." and asset_transfer_added_on >= :start_date";				
		}
		
		//Data
		$get_asset_transfer_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_asset_transfer_list_squery_where = $get_asset_transfer_list_squery_where." where asset_transfer_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_asset_transfer_list_squery_where = $get_asset_transfer_list_squery_where." and asset_transfer_added_on <= :end_date";
		}
		
		//Data
		$get_asset_transfer_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_asset_transfer_list_squery = $get_asset_transfer_list_squery_base.$get_asset_transfer_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_asset_transfer_list_sstatement = $dbConnection->prepare($get_asset_transfer_list_squery);
		
		$get_asset_transfer_list_sstatement -> execute($get_asset_transfer_list_sdata);
		
		$get_asset_transfer_list_sdetails = $get_asset_transfer_list_sstatement -> fetchAll();
		
		if(FALSE === $get_asset_transfer_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_asset_transfer_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_asset_transfer_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update Asset Transfer 
INPUT 	: Transfer ID, Asset Transfer Update Array
OUTPUT 	: Transfer ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_asset_transfer($transfer_id,$asset_transfer_update_data)
{
	if(array_key_exists("details_id",$asset_transfer_update_data))
	{	
		$details_id = $asset_transfer_update_data["details_id"];
	}
	else
	{
		$details_id = "";
	}
	
	if(array_key_exists("source_location",$asset_transfer_update_data))
	{	
		$source_location = $asset_transfer_update_data["source_location"];
	}
	else
	{
		$source_location = "";
	}
	
	if(array_key_exists("dest_location",$asset_transfer_update_data))
	{	
		$dest_location = $asset_transfer_update_data["dest_location"];
	}
	else
	{
		$dest_location = "";
	}
	
	if(array_key_exists("active",$asset_transfer_update_data))
	{	
		$active = $asset_transfer_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$asset_transfer_update_data))
	{	
		$remarks = $asset_transfer_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$asset_transfer_update_data))
	{	
		$added_by = $asset_transfer_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$asset_transfer_update_data))
	{	
		$added_on = $asset_transfer_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $asset_transfer_update_uquery_base = "update asset_transfer set";  
	
	$asset_transfer_update_uquery_set = "";
	
	$asset_transfer_update_uquery_where = " where asset_transfer_id = :transfer_id";
	
	$asset_transfer_update_udata = array(":transfer_id"=>$transfer_id);
	
	$filter_count = 0;
	
	if($details_id != "")
	{
		$asset_transfer_update_uquery_set = $asset_transfer_update_uquery_set." asset_transfer_details_id = :details_id,";
		$asset_transfer_update_udata[":details_id"] = $details_id;
		$filter_count++;
	}
	
	if($source_location != "")
	{
		$asset_transfer_update_uquery_set = $asset_transfer_update_uquery_set." asset_transfer_source_location = :source_location,";
		$asset_transfer_update_udata[":source_location"] = $source_location;
		$filter_count++;
	}
	
	if($dest_location != "")
	{
		$asset_transfer_update_uquery_set = $asset_transfer_update_uquery_set." asset_transfer_destination_location = :dest_location,";
		$asset_transfer_update_udata[":dest_location"] = $dest_location;
		$filter_count++;
	}
	
	if($active != "")
	{
		$asset_transfer_update_uquery_set = $asset_transfer_update_uquery_set." asset_transfer_active = :active,";
		$asset_transfer_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$asset_transfer_update_uquery_set = $asset_transfer_update_uquery_set." asset_transfer_remarks = :remarks,";
		$asset_transfer_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$asset_transfer_update_uquery_set = $asset_transfer_update_uquery_set." asset_transfer_added_by = :added_by,";
		$asset_transfer_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$asset_transfer_update_uquery_set = $asset_transfer_update_uquery_set." asset_transfer_added_on = :added_on,";
		$asset_transfer_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$asset_transfer_update_uquery_set = trim($asset_transfer_update_uquery_set,',');
	}
	
	$asset_transfer_update_uquery = $asset_transfer_update_uquery_base.$asset_transfer_update_uquery_set.$asset_transfer_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $asset_transfer_update_ustatement = $dbConnection->prepare($asset_transfer_update_uquery);		
        
        $asset_transfer_update_ustatement -> execute($asset_transfer_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $transfer_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}
?>