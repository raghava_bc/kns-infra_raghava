<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 16th Nov 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_config.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["profile"]))
	{
		$cust_details_id = $_GET["profile"];
	}
	else
	{
		$cust_details_id = "";
	}		
	/* QUERY STRING - END */
	
	// Get the customer details as captured already
	$cust_details = i_get_cust_profile_list($cust_details_id,'','','','','','','','','');
	if($cust_details["status"] == SUCCESS)
	{
		$cust_details_list = $cust_details["data"];
	}
	else
	{
		$alert = $cust_details["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>View Customer Profile</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Project: <?php echo $cust_details_list[0]["project_name"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Site No: <?php echo $cust_details_list[0]["crm_site_no"]; ?></h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">						
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">							
								<div class="tab-pane active" id="formcontrols">
									<div class="widget-header">
									<i class="icon-user"></i>
									<h3>Customer Name</h3>
									</div> <!-- /widget-header -->
									<br />									
									<fieldset>	
										<table class="table table-bordered" style="table-layout: fixed;">
										  <thead>			  
										  <tr>
										  <th>Name 1</th>
										  <th>Name 2</th>
										  <th>Name 3</th>
										  <th>Name 4</th>
										  </tr>
										  </thead>
										  <tbody>
										  <tr>
										  <td><?php echo $cust_details_list[0]["crm_customer_name_one"]; ?> <br /><?php echo $cust_details_list[0]["crm_customer_relationship_one"]; ?></td>
										  <td><?php echo $cust_details_list[0]["crm_customer_name_two"]; ?> <br /><?php echo $cust_details_list[0]["crm_customer_relationship_two"]; ?></td>
										  <td><?php echo $cust_details_list[0]["crm_customer_name_three"]; ?> <br /><?php echo $cust_details_list[0]["crm_customer_relationship_three"]; ?></td>
										  <td><?php echo $cust_details_list[0]["crm_customer_name_four"]; ?> <br /><?php echo $cust_details_list[0]["crm_customer_relationship_four"]; ?></td>
										  </tr>
										  </tbody>
										</table>
										<table class="table table-bordered" style="table-layout: fixed;">
										  <thead>			  
										  <tr>
										  <th>PAN No 1</th>
										  <th>PAN No 2</th>
										  <th>PAN No 3</th>
										  <th>PAN No 4</th>
										  </tr>
										  </thead>
										  <tbody>
										  <tr>
										  <td><?php echo $cust_details_list[0]["crm_customer_pan_no_one"]; ?></td>
										  <td><?php echo $cust_details_list[0]["crm_customer_pan_no_two"]; ?></td>
										  <td><?php echo $cust_details_list[0]["crm_customer_pan_no_three"]; ?></td>
										  <td><?php echo $cust_details_list[0]["crm_customer_pan_no_four"]; ?></td>
										  </tr>
										  </tbody>
										</table>										
									</fieldset>
									<div class="widget-header">
									<i class="icon-user"></i>
									<h3>Customer Contact Details</h3>
									</div> <!-- /widget-header -->
									<br />									
									<fieldset>
										<table class="table table-bordered" style="table-layout: fixed;">
										  <thead>			  
										  <tr>
										  <th>Contact No 1</th>
										  <th>Contact No 2</th>
										  <th>Contact No 3</th>
										  <th>Contact No 4</th>
										  </tr>
										  </thead>
										  <tbody>
										  <tr>
										  <td><?php echo $cust_details_list[0]["crm_customer_contact_no_one"]; ?></td>
										  <td><?php echo $cust_details_list[0]["crm_customer_contact_no_two"]; ?></td>
										  <td><?php echo $cust_details_list[0]["crm_customer_contact_no_three"]; ?></td>
										  <td><?php echo $cust_details_list[0]["crm_customer_contact_no_four"]; ?></td>
										  </tr>
										  </tbody>
										</table>
										<br />
										<table class="table table-bordered" style="table-layout: fixed;">
										  <thead>			  
										  <tr>
										  <th>Email 1</th>
										  <th>Email 2</th>
										  <th>Email 3</th>
										  <th>Email 4</th>
										  </tr>
										  </thead>
										  <tbody>
										  <tr>
										  <td><?php echo $cust_details_list[0]["crm_customer_email_id_one"]; ?></td>
										  <td><?php echo $cust_details_list[0]["crm_customer_email_id_two"]; ?></td>
										  <td><?php echo $cust_details_list[0]["crm_customer_email_id_three"]; ?></td>
										  <td><?php echo $cust_details_list[0]["crm_customer_email_id_four"]; ?></td>
										  </tr>
										  </tbody>
										</table>										
									</fieldset>
									<div class="widget-header">
									<i class="icon-user"></i>
									<h3>Customer Birthdays and Anniversaries</h3>
									</div> <!-- /widget-header -->
									<br />									
									<fieldset>	
										<table class="table table-bordered" style="table-layout: fixed;">
										  <thead>			  
										  <tr>
										  <th>DOB 1</th>
										  <th>DOB 2</th>
										  <th>DOB 3</th>
										  <th>DOB 4</th>
										  </tr>
										  </thead>
										  <tbody>
										  <tr>
										  <td><?php if($cust_details_list[0]["crm_customer_dob_one"] != "0000-00-00")
										  {
											echo date("d-M-Y",strtotime($cust_details_list[0]["crm_customer_dob_one"])); 
										  }
										  else
										  {
											echo "";
										  }?></td>
										  <td><?php if($cust_details_list[0]["crm_customer_dob_two"] != "0000-00-00")
										  {
											echo date("d-M-Y",strtotime($cust_details_list[0]["crm_customer_dob_two"])); 
										  }
										  else
										  {
											echo "";
										  }?></td>
										  <td><?php if($cust_details_list[0]["crm_customer_dob_three"] != "0000-00-00")
										  {
											echo date("d-M-Y",strtotime($cust_details_list[0]["crm_customer_dob_three"])); 
										  }
										  else
										  {
											echo "";
										  }?></td>
										  <td><?php if($cust_details_list[0]["crm_customer_dob_four"] != "0000-00-00")
										  {
											echo date("d-M-Y",strtotime($cust_details_list[0]["crm_customer_dob_four"])); 
										  }
										  else
										  {
											echo "";
										  }?></td>
										  </tr>
										  </tbody>
										</table>
										<br />
										<table class="table table-bordered" style="table-layout: fixed;">
										  <thead>			  
										  <tr>
										  <th>Anniversary 1</th>
										  <th>Anniversary 2</th>										  
										  </tr>
										  </thead>
										  <tbody>
										  <tr>
										  <td><?php if($cust_details_list[0]["crm_customer_anniversary_one"] != "0000-00-00")
										  {
											echo date("d-M-Y",strtotime($cust_details_list[0]["crm_customer_anniversary_one"])); 
										  }
										  else
										  {
											echo "";
										  }?></td>
										  <td><?php if($cust_details_list[0]["crm_customer_anniversary_two"] != "0000-00-00")
										  {
											echo date("d-M-Y",strtotime($cust_details_list[0]["crm_customer_anniversary_two"])); 
										  }
										  else
										  {
											echo "";
										  }?></td>
										  </tr>
										  </tbody>
										</table>																			
									</fieldset>
									<div class="widget-header">
									<i class="icon-user"></i>
									<h3>Buyer Info</h3>
									</div> <!-- /widget-header -->
									<br />									
									<fieldset>
										<table class="table table-bordered" style="table-layout: fixed;">
										  <thead>			  
										  <tr>
										  <th>Company</th>
										  <th>Designation</th>
										  <th>Funding</th>
										  <th>Bank</th>
										  <th>Agreement Address</th>
										  <th>Residential Address</th>
										  <th>Office Address</th>										  
										  <th>GPA Holder</th>
										  <th>GPA Contact No</th>
										  <th>GPA Address</th>
										  <th>GPA Email</th>
										  <th>GPA Remarks</th>
										  </tr>
										  </thead>
										  <tbody>
										  <tr>
										  <td style="word-wrap:break-word;"><?php echo $cust_details_list[0]["crm_customer_company"]; ?></td>
										  <td style="word-wrap:break-word;"><?php echo $cust_details_list[0]["crm_customer_designation"]; ?></td>
										  <td style="word-wrap:break-word;"><?php if($cust_details_list[0]["crm_customer_funding_type"] == "1") 
												{
													echo "Self";
												}
												else												
												{
													echo "Loan";
												} ?></td>
										  <td style="word-wrap:break-word;"><?php if($cust_details_list[0]["crm_customer_loan_bank"] != "")
										  {
											echo $cust_details_list[0]["crm_bank_name"]; 
										  }
										  else
										  {
											echo "";
										  }?></td>
										  <td style="word-wrap:break-word;"><?php echo $cust_details_list[0]["crm_customer_agreement_address"]; ?></td>
										  <td style="word-wrap:break-word;"><?php echo $cust_details_list[0]["crm_customer_residential_address"]; ?></td>
										  <td style="word-wrap:break-word;"><?php echo $cust_details_list[0]["crm_customer_office_address"]; ?></td>		<td style="word-wrap:break-word;"><?php echo $cust_details_list[0]["crm_customer_gpa_holder"]; ?></td>
										  <td style="word-wrap:break-word;"><?php echo $cust_details_list[0]["crm_customer_gpa_contact_no"]; ?></td>
										  <td style="word-wrap:break-word;"><?php echo $cust_details_list[0]["crm_customer_gpa_address"]; ?></td>
										  <td style="word-wrap:break-word;"><?php echo $cust_details_list[0]["crm_customer_gpa_email"]; ?></td>
										  <td style="word-wrap:break-word;"><?php echo $cust_details_list[0]["crm_customer_gpa_remarks"]; ?></td>
										  </tr>
										  </tbody>
										</table>																			
									</fieldset>								
								</div>																
								
							</div>
						  						  
						</div>
	
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
