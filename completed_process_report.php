<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: completed_project_list.php
CREATED ON	: 05-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Completed Process Plans
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	if(isset($_POST['process_search_submit']))
	{
		$process_type = $_POST['ddl_process'];
	}
	else
	{
		$process_type = '';
	}
	
	// Get list of process plans for this module
	$legal_process_plan_list = i_get_legal_process_plan_list('','',$process_type,'','','','','','','1','');

	if($legal_process_plan_list["status"] == SUCCESS)
	{
		$legal_process_plan_list_data = $legal_process_plan_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$legal_process_plan_list["data"];
	}
	
	// Get list of process types
	$process_type_list = i_get_process_type_list('','');

	if($process_type_list["status"] == SUCCESS)
	{
		$process_type_list_data = $process_type_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$process_type_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Completed Processes List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Completed Processes List&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Records are displayed in descending order of End Date i.e. latest completed process first</h3>
            </div>
            <!-- /widget-header -->
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="post" id="process_Search_form" action="completed_process_report.php">			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_process">
			  <option value="">- - Select Process - -</option>
			  <?php
			  for($process_count = 0; $process_count < count($process_type_list_data); $process_count++)
			  {
			  ?>
			  <option value="<?php echo $process_type_list_data[$process_count]["process_master_id"]; ?>" <?php if($process_type == $process_type_list_data[$process_count]["process_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $process_type_list_data[$process_count]["process_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>			  			  
			  <input type="submit" name="process_search_submit" />
			  </form>			  
            </div>
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
					<th>SL No</th>
				    <th>File Number</th>
				    <th>Survey Number</th>
					<th>Process</th>
					<th>Village</th>
					<th>Extent</th>					
					<th>Land Owner</th>															
					<th>Start Date</th>										
					<th>End Date</th>
					<th>Variance</th>								
				</tr>
				</thead>
				<tbody>
				 <?php
				if($legal_process_plan_list["status"] == SUCCESS)
				{		
					$sl_no = 0;
					$process_array = array();
					for($count = 0; $count < count($legal_process_plan_list_data); $count++)
					{ 						
						$task_plan_result = i_get_task_plan_list('','',$legal_process_plan_list_data[$count]["process_plan_legal_id"],'','','','','desc');
						if($task_plan_result["status"] == SUCCESS)
						{							
							$start_date = $task_plan_result["data"][(count($task_plan_result['data'])) - 1]["task_plan_actual_start_date"];
							$end_date   = $task_plan_result["data"][0]["task_plan_actual_end_date"];
						}
						else
						{
							$start_date = "0000-00-00";
							$end_date   = "0000-00-00";														
						}
						
						$variance = get_date_diff($start_date,$end_date);

						$process_array[$count]['file_number'] 		 = $legal_process_plan_list_data[$count]["file_number"];
						$process_array[$count]['file_survey_number'] = $legal_process_plan_list_data[$count]["file_survey_number"];
						$process_array[$count]['process_name'] 		 = $legal_process_plan_list_data[$count]["process_name"];
						$process_array[$count]['file_village'] 		 = $legal_process_plan_list_data[$count]["file_village"];
						$process_array[$count]['file_extent'] 		 = $legal_process_plan_list_data[$count]["file_extent"];
						$process_array[$count]['file_land_owner'] 	 = $legal_process_plan_list_data[$count]["file_land_owner"];
						$process_array[$count]['start_date'] 		 = $start_date;
						$process_array[$count]['fup_date'] 		     = $end_date;
						$process_array[$count]['variance'] 			 = $variance["data"];
					}
					
					array_sort_conditional($process_array,'sort_on_fup_dates');	
										
					for($count = (count($process_array) - 1); $count >= 0; $count--)
					{
						$sl_no++;
					?>
					<tr>
						<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
						<td style="word-wrap:break-word;"><?php echo $process_array[$count]["file_number"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $process_array[$count]["file_survey_number"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $process_array[$count]["process_name"]; ?></td>						
						<td style="word-wrap:break-word;"><?php echo $process_array[$count]["file_village"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $process_array[$count]["file_extent"]; ?></td>					
						<td style="word-wrap:break-word;"><?php echo $process_array[$count]["file_land_owner"]; ?></td>						
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($process_array[$count]['start_date'],"d-M-Y"); ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($process_array[$count]['fup_date'],"d-M-Y"); // This is end date ?></td>
						<td style="word-wrap:break-word;"><?php echo $process_array[$count]['variance']; ?></td>						
					</tr>
					<?php 
					}
				}
				else
				{
				?>
				<td colspan="14">No process completed yet</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
