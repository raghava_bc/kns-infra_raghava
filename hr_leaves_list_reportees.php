<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: user_list.php
CREATED ON	: 05-July-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Users
*/

/*
TBD: 
*/$_SESSION['module'] = 'HR';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_employee_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_attendance_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Data initialization
	$alert_type = -1;
	$alert      = "";
	
	if(isset($_POST["leave_search_submit"]))
	{
		$leave_date      = $_POST["dt_leave_date"];
		$search_employee = $_POST["ddl_employee"];
		$status          = $_POST["ddl_status"];
		$type            = $_POST["ddl_type"];
	}
	else
	{
		$leave_date      = "";
		$search_employee = "";
		$status          = "0";
		$type            = "";
	}

	// Get list of leaves applied by reportees
	if(($role == "13") || ($user == "143620071466608200"))
	{
		$leave_filter_data = array();
	}
	else
	{
		$leave_filter_data = array("manager"=>$user);
	}	
	if($search_employee != "")
	{
		$leave_filter_data["employee_id"] = $search_employee;
	}
	if($leave_date != "")
	{
		$leave_filter_data["date"] = $leave_date;
	}
	if($status != "")
	{
		$leave_filter_data["status"] = $status;
	}
	if($type != "")
	{
		$leave_filter_data["type"] = $type;
	}
	$leave_list = i_get_leave_list($leave_filter_data);

	if($leave_list["status"] == SUCCESS)
	{
		$leave_list_data = $leave_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$leave_list["data"];
	}
	
	// Get list of employees
	if(($role == "13") || ($user == "143620071466608200"))
	{
		$employee_filter_data = array("user_status"=>'1');
	}
	else
	{
		$employee_filter_data = array("employee_manager"=>$user,"user_status"=>'1');
	}	
	$employee_list = i_get_employee_list($employee_filter_data);

	if($employee_list["status"] == SUCCESS)
	{
		$employee_list_data = $employee_list["data"];
	}
	else
	{
		$alert      = $alert."Alert: ".$employee_list["data"];
		$alert_type = "1";
	}
	
	// Get list of attendance types
	$attendance_type_filter_data = array("active"=>'1',"leave_type"=>'1');
	$attendance_type_list = i_get_attendance_type($attendance_type_filter_data);
	if($attendance_type_list["status"] == SUCCESS)
	{
		$attendance_type_list_data = $attendance_type_list["data"];
	}
	else
	{
		$alert      = $alert."Alert: ".$attendance_type_list_data["data"];
		$alert_type = "1";
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Leave Applications By Reportees</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Leave Applications By Reportees</h3><span style="flost:right;" id="span_msg"></span>
            </div>
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="post" id="leave_search_form" action="hr_leaves_list_reportees.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_leave_date" value="<?php echo $leave_date; ?>" />
			  </span>			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_employee">
			  <option value="">- - Select Employee - -</option>
			  <?php
			  for($count = 0; $count < count($employee_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $employee_list_data[$count]["hr_employee_id"]; ?>" <?php if($search_employee == $employee_list_data[$count]["hr_employee_id"]) { ?> selected="selected" <?php } ?>><?php echo $employee_list_data[$count]["hr_employee_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>			  			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_status">			  
			  <option value="0" <?php if($status == "0"){?> selected <?php } ?>>Pending</option>
			  <option value="1" <?php if($status == "1"){?> selected <?php } ?>>Approved</option>
			  <option value="2" <?php if($status == "2"){?> selected <?php } ?>>Rejected</option>
			  <option value="3" <?php if($status == "3"){?> selected <?php } ?>>Cancelled</option>
			  </select>
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_type">
			  <option value="">- - Select Attendance Type - -</option>
			  <?php
			  for($count = 0; $count < count($attendance_type_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $attendance_type_list_data[$count]["hr_attendance_type_id"]; ?>" <?php if($type == $attendance_type_list_data[$count]["hr_attendance_type_id"]) { ?> selected="selected" <?php } ?>><?php echo $attendance_type_list_data[$count]["hr_attendance_type_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>			  
			  <input type="submit" name="leave_search_submit" />
			  </form>			  
            </div>            
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th>Employee Code</th>
					<th>Employee Name</th>					
					<th>Leave Date</th>
					<th>Leave Type</th>
					<th>Leave Duration</th>
					<th>Leave Remarks</th>
				    <th>Leave Status</th>
					<th>Leave Applied On</th>	
					<th>Manager Action On</th>
					<th>Manager</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>
				<?php
				if($leave_list["status"] == SUCCESS)
				{				
					for($count = 0; $count < count($leave_list_data); $count++)
					{						
					?>
					<tr>
						<td style="word-wrap:break-word;"><?php echo $leave_list_data[$count]["hr_employee_code"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $leave_list_data[$count]["hr_employee_name"]; ?></td>						
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($leave_list_data[$count]["hr_absence_date"],"d-M-Y"); ?></td>						
						<td style="word-wrap:break-word;"><?php echo $leave_list_data[$count]["hr_attendance_type_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo i_get_leave_duration_name($leave_list_data[$count]["hr_absence_duration"]); ?></td>
						<td style="word-wrap:break-word;"><?php echo $leave_list_data[$count]["hr_absence_remarks"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo i_get_leave_status_name($leave_list_data[$count]["hr_absence_approval_status"]); ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($leave_list_data[$count]["hr_absence_added_on"],"d-M-Y"); ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($leave_list_data[$count]["hr_absence_approval_action_on"],"d-M-Y"); ?></td>
						<td style="word-wrap:break-word;"><?php echo $leave_list_data[$count]["approver"]; ?></td>
						<td style="word-wrap:break-word;">
						<?php if(($leave_list_data[$count]["hr_absence_approval_status"] == LEAVE_STATUS_PENDING) || ($leave_list_data[$count]["hr_absence_approval_status"] == LEAVE_STATUS_REJECTED)){?>
						<a href="#" onclick="return approve_leave(<?php echo $leave_list_data[$count]["hr_absence_id"]; ?>);"><span style="color:black; text-decoration: underline;">Approve</span></a>
						<?php
						}
						?></td>
						<td style="word-wrap:break-word;">
						<?php if(($leave_list_data[$count]["hr_absence_approval_status"] == LEAVE_STATUS_PENDING) || ($leave_list_data[$count]["hr_absence_approval_status"] == LEAVE_STATUS_APPROVED)){?>
						<a href="#" onclick="return reject_leave(<?php echo $leave_list_data[$count]["hr_absence_id"]; ?>);"><span style="color:black; text-decoration: underline;">Reject</span></a>
						<?php
						}
						?></td>
					</tr>
					<?php 
					}
				}
				else
				{
				?>
				<tr>
				<td colspan="10">No leave applications!</td>
				</tr>
				<?php
				}
				?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function approve_leave(attendance_id)
{
	var ok = confirm("Are you sure you want to approve?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "hr_leaves_list_reportees.php";
					}
				}
			}

			xmlhttp.open("POST", "hr_update_leave.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("attendance=" + attendance_id + "&action=1");
		}
	}	
}

function reject_leave(attendance_id)
{
	var ok = confirm("Are you sure you want to reject?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{				
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{					
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "hr_leaves_list_reportees.php";
					}
				}
			}

			xmlhttp.open("POST", "hr_update_leave.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("attendance=" + attendance_id + "&action=2");
		}
	}	
}
</script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>
  </body>

</html>
