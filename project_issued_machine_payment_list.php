<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_actual_contract_payment_list.php
CREATED ON	: 09-May-2017
CREATED BY	: Ashwini
PURPOSE     : List of project for actual contract payment
*/

/*
TBD:
*/

/* DEFINES - START */
define('PROJECT_MACHINE_ACCEPT_PAYMENT_FUNC_ID','276');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');


if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	$alert_type = -1;
	$alert = "";

	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',PROJECT_MACHINE_ACCEPT_PAYMENT_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',PROJECT_MACHINE_ACCEPT_PAYMENT_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',PROJECT_MACHINE_ACCEPT_PAYMENT_FUNC_ID,'4','1');
	$add_perms_list    = i_get_user_perms($user,'',PROJECT_MACHINE_ACCEPT_PAYMENT_FUNC_ID,'1','1');

	// Query String Data
	// Nothing

	$search_machine_vendor  = "";
	$search_machine_project = "";

	if(isset($_POST["mac_acc_pay_search_submit"]))
	{
		$search_machine_vendor  = $_POST["search_machine_vendor"];
		$search_machine_project = $_POST["search_machine_project"];
	}

	// Get Project Actual Machine Payment modes already added
	$project_actual_machine_payment_search_data = array("active"=>'1',"vendor_id"=>$search_machine_vendor);
	$project_actual_machine_payment_list = i_get_project_payment_machine($project_actual_machine_payment_search_data);
	if($project_actual_machine_payment_list['status'] == SUCCESS)
	{
		$project_actual_machine_payment_list_data = $project_actual_machine_payment_list['data'];
	}
	else
	{
		$alert = $alert."Alert: ".$project_actual_machine_payment_list["data"];
	}

	// Machine Vendor data
	$project_machine_vendor_search_data = array("active"=>'1');
	$project_machine_vendor_list = i_get_project_machine_vendor_master_list($project_machine_vendor_search_data);
	if($project_machine_vendor_list["status"] == SUCCESS)
	{
		$project_machine_vendor_list_data = $project_machine_vendor_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_machine_vendor_list["data"];
	}

	// Project data
	$project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list["status"] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_management_master_list["data"];
	}
}
else
{
	header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Project Accept Machine Payment List</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>


<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

          <div class="span6" style="width:100%;">

          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Actual Machine Payment List &nbsp;&nbsp;&nbsp;&nbsp;Total Amount: <span id="total_amount"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;&nbsp;Total Issued Amount: <span id="total_issued_amount"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;&nbsp; Balance Amount: <span id="total_balance"><i>Calculating</i></span></h3>
            </div>
            <!-- /widget-header -->
			<div class="widget-header" style="height:50px; padding-top:10px;">
			  <form method="post" id="file_search_form" action="project_issued_machine_payment_list.php">

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_machine_vendor">
			  <option value="">- - Select Vendor - -</option>
			  <?php
			  for($vendor_count = 0; $vendor_count < count($project_machine_vendor_list_data); $vendor_count++)
			  {
			  ?>
			  <option value="<?php echo $project_machine_vendor_list_data[$vendor_count]["project_machine_vendor_master_id"]; ?>" <?php if($search_machine_vendor == $project_machine_vendor_list_data[$vendor_count]["project_machine_vendor_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_machine_vendor_list_data[$vendor_count]["project_machine_vendor_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_machine_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if($search_machine_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>

			  <input type="submit" name="mac_acc_pay_search_submit" />
			  </form>
            </div>
            <div class="widget-content">

		    <?php if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Project</th>
					<th>From Date</th>
					<th>To Date</th>
					<th>Bill No</th>
					<th>Billing Addess</th>
					<th>Vendor Name</th>
					<th>Total Amount</th>
					<th>Issued Amount</th>
					<th colspan="2" style="text-align:center;">Actions</th>

				</tr>
				</thead>
				<tbody>
				<?php
				if($project_actual_machine_payment_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					$total_issued_amount = 0;
					$total_deduction = 0;
					$total_balance = 0;
					$total_amount = 0;
					for($count = 0; $count < count($project_actual_machine_payment_list_data); $count++)
					{
						//Get Delay
						$start_date = date("Y-m-d");
						$end_date = $project_actual_machine_payment_list_data[$count]["project_payment_machine_accepted_on"];
						$delay = get_date_diff($end_date,$start_date);

						//Get total amount
						$amount = $project_actual_machine_payment_list_data[$count]["project_payment_machine_amount"];


						//Get Project Machine Vendor master List
						$issued_amount = 0;
						$deduction = 0;
						$project_machine_issue_payment_search_data = array("active"=>'1',"machine_id"=>$project_actual_machine_payment_list_data[$count]["project_payment_machine_id"]);
						$project_machine_issue_payment_list = i_get_project_machine_issue_payment($project_machine_issue_payment_search_data);
						if($project_machine_issue_payment_list["status"] == SUCCESS)
						{
							$project_machine_issue_payment_list_data = $project_machine_issue_payment_list["data"];
							for($issue_count = 0 ; $issue_count < count($project_machine_issue_payment_list_data) ; $issue_count++)
							{
								$issued_amount = $issued_amount + $project_machine_issue_payment_list_data[$issue_count]["project_machine_issue_payment_amount"];
							}
						}
						else
						{
							$issued_amount = 0;
						}
						$balance_amount = ($amount - $issued_amount);

						// Get machine payment mapping for project name
						$project_payment_machine_mapping_search_data = array("payment_id"=>$project_actual_machine_payment_list_data[$count]["project_payment_machine_id"]);
						$project_name_sresult = i_get_project_payment_machine_mapping($project_payment_machine_mapping_search_data);
						if($project_name_sresult['status'] == SUCCESS)
						{
							$actual_machine_plan_search_data = array('plan_id'=>$project_name_sresult['data'][0]['project_payment_machine_mapping_machine_actuals_id']);
							$project_name_data = i_get_machine_planning_list($actual_machine_plan_search_data);
							$project_name = $project_name_data['data'][0]['project_master_name'];
							$project_id   = $project_name_data['data'][0]['project_management_master_id'];
						}
						else
						{
							$project_name = 'INVALID PROJECT';
							$project_id   = '-1';
						}

						if(($search_machine_project == $project_id) || ($search_machine_project == ''))
						{
							if($balance_amount == 0)
							{
								$total_issued_amount = $total_issued_amount + $issued_amount;
								$total_balance  = $total_balance + $balance_amount;
								$total_amount = $total_amount + $amount;

								$sl_no++
								?>
								<tr>
								<td><?php echo $sl_no; ?></td>
								<td><?php echo $project_name; ?></td>
								<td><?php echo date("d-M-Y",strtotime($project_actual_machine_payment_list_data[$count]["project_payment_machine_from_date"])); ?></td>
								<td><?php echo date("d-M-Y",strtotime($project_actual_machine_payment_list_data[$count]["project_payment_machine_to_date"])); ?></td>
								<td><?php echo $project_actual_machine_payment_list_data[$count]["project_payment_machine_bill_no"]; ?></td>
								<td><?php echo $project_actual_machine_payment_list_data[$count]["stock_company_master_name"]; ?></td>
								<td><?php echo $project_actual_machine_payment_list_data[$count]["project_machine_vendor_master_name"]; ?></td>
								<td><?php echo $amount ;?></td>
								<td><?php echo $issued_amount ;?></td>
								<td style="word-wrap:break-word;"><?php if($project_actual_machine_payment_list_data[$count]["project_payment_machine_status"] == "Accepted"){ if($issued_amount < $amount){ ?><a href="#" onclick="return go_to_project_payment_issued_list('<?php echo $project_actual_machine_payment_list_data[$count]["project_payment_machine_id"]; ?>');">View Payment Issues</a><?php }
								} ?></td>
								<td><?php if(($view_perms_list['status'] == SUCCESS)){?><a href="#" target="_blank" onclick="return go_to_machine_print(<?php echo $project_actual_machine_payment_list_data[$count]["project_payment_machine_id"]; ?>);">Print</a><?php } ?></td>
								</tr>
								<?php
							}
						}
					}
				}
				else
				{
				?>
				<td colspan="19">No Payment Acceptance yet!</td>

				<?php
				}
				?>

                </tbody>
              </table>
			  <?php
			}
			?>
			   <script>
			  document.getElementById('total_issued_amount').innerHTML = <?php echo $total_issued_amount; ?>;document.getElementById('total_balance').innerHTML = <?php echo $total_balance; ?>;
			  document.getElementById('total_amount').innerHTML = <?php echo $total_amount; ?>;
			  </script>
            </div>
            <!-- /widget-content -->
          </div>
          <!-- /widget -->

          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 -->
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>




<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">

                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->




<div class="footer">

	<div class="footer-inner">

		<div class="container">

			<div class="row">

    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->

    		</div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /footer-inner -->

</div> <!-- /footer -->



<script src="js/jquery-1.7.2.min.js"></script>

<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_accept_contract_payment(contract_payment_id)
{
	var ok = confirm("Are you sure you want to Accept?")
	{
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					alert(xmlhttp.responseText);
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else
					{
					 window.location = "project_accept_contract_payment_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_accept_contract_payment.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("contract_payment_id=" + contract_payment_id + "&action=Accepted");
		}
	}
}
function go_to_project_issue_payment_machine(payment_machine_id,vendor_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_add_machine_issue_payment.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","payment_machine_id");
	hiddenField1.setAttribute("value",payment_machine_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","vendor_id");
	hiddenField2.setAttribute("value",vendor_id);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);

	document.body.appendChild(form);
    form.submit();
}

function go_to_project_view_payment_contract(payment_contract_id,vendor_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_view_contract_work_details.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","payment_contract_id");
	hiddenField1.setAttribute("value",payment_contract_id);

	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","vendor_id");
	hiddenField2.setAttribute("value",vendor_id);

	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);

	document.body.appendChild(form);
    form.submit();
}
function go_to_project_payment_issued_list(payment_machine_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_machine_issue_payment_list.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","payment_id");
	hiddenField1.setAttribute("value",payment_machine_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}
function go_to_project_edit_bill_details(payment_contract_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_edit_contract_bill_details.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","payment_contract_id");
	hiddenField1.setAttribute("value",payment_contract_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}
function go_to_machine_print(machine_payment_id)
{
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_machine_weekly_print.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","machine_payment_id");
	hiddenField1.setAttribute("value",machine_payment_id);

	form.appendChild(hiddenField1);

	document.body.appendChild(form);
    form.submit();
}
</script>
</body>

</html>
