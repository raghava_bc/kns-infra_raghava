<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 28-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET["rate_id"]))
	{
		$rate_id = $_GET["rate_id"];
	}
	else
	{
		$rate_id = "";
	}
	
	// Capture the form data
	if(isset($_POST["edit_man_power_rate_master_submit"]))
	{
		
		$rate_id                = $_POST["hd_rate_id"];
		$power_type_id          = $_POST["ddl_power_type_id"];
		$vendor_id 		        = $_POST["ddl_vendor"];
		$cost_per_hours         = $_POST["cost_per_hours"];
		$applicable_date        = $_POST["applicable_date"];
		$remarks 	            = $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($power_type_id != "") && ($cost_per_hours != "") && ($applicable_date != ""))
		{
			$project_man_power_rate_update_data = array("power_type_id"=>$power_type_id,"vendor_id"=>$vendor_id,"cost_per_hours"=>$cost_per_hours,"applicable_date"=>$applicable_date,"remarks"=>$remarks,"updated_by"=>$user,"updated_on"=>date("Y-m-d H:i:s"));
			$project_man_power_rate_iresult = i_update_project_man_power_rate_master($rate_id,$project_man_power_rate_update_data);
			
			if($project_man_power_rate_iresult["status"] == SUCCESS)
				
			{	
				$man_power_rate_history_iresults = i_add_man_power_rate_history($power_type_id,$vendor_id,$cost_per_hours,$user);
				$alert_type = 1;
		    
				header("location:project_master_man_power_rate_list.php");
			}
			else
			{
			   $alert = "Project Already Exists";
			  $alert_type = 0;	
			}
			
			$alert = $project_man_power_rate_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get Project Man Power Type Master modes already added
	$project_man_power_master_search_data = array("active"=>'1');
	$project_man_power_type_list = i_get_project_man_power_master($project_man_power_master_search_data);
	if($project_man_power_type_list['status'] == SUCCESS)
	{
		$project_man_power_type_list_data = $project_man_power_type_list['data'];
		
    }
     else
    {
		$alert = $project_man_power_type_list["data"];
		$alert_type = 0;
	}
	
	// Get Project Man Power Rate modes already added
	$project_man_power_rate_search_data = array("active"=>'1',"rate_id"=>$rate_id);
	$project_man_power_rate_list = i_get_project_man_power_rate_master($project_man_power_rate_search_data);
	if($project_man_power_rate_list['status'] == SUCCESS)
	{
		$project_man_power_rate_list_data = $project_man_power_rate_list['data'];
	}	
	
	// Get Project manpower_agency Master modes already added
	$project_manpower_agency_search_data = array("active"=>'1');
	$project_manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
	if($project_manpower_agency_list['status'] == SUCCESS)
	{
		$project_manpower_agency_list_data = $project_manpower_agency_list['data'];
	}	
	 else
	{
		$alert = $project_manpower_agency_list["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Master Edit Man Power Rate</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Project Master Edit Man Power Rate</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Project Master Edit Man Power Rate</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_master_edit_man_power_rate_form" class="form-horizontal" method="post" action="project_master_edit_man_power_rate.php">
								<input type="hidden" name="hd_rate_id" value="<?php echo $rate_id; ?>" />
									<fieldset>				
									
											<div class="control-group">											
											<label class="control-label" for="ddl_vendor">Man Power Vendor*</label>
											<div class="controls">
												<select name="ddl_vendor" required>
												<option value="">- - Select Agency - -</option>
												<?php
												for($count = 0; $count < count($project_manpower_agency_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_manpower_agency_list_data[$count]["project_manpower_agency_id"]; ?>" <?php if($project_manpower_agency_list_data[$count]["project_manpower_agency_id"] == $project_man_power_rate_list_data[0]["project_man_power_rate_vendor_id"]){ ?> selected="selected" <?php } ?>><?php echo $project_manpower_agency_list_data[$count]["project_manpower_agency_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->	
										
											<div class="control-group">											
											<label class="control-label" for="ddl_power_type_id">Power Type*</label>
											<div class="controls">
												<select name="ddl_power_type_id" required>
												<option value="">- - Select Man Power Type - -</option>
												<option value="1" <?php if($project_man_power_rate_list_data[0]["project_man_power_type_id"] == 1){ ?> selected="selected" <?php } ?>>- - Male - -</option>
												<option value="2" <?php if($project_man_power_rate_list_data[0]["project_man_power_type_id"] == 2){ ?> selected="selected" <?php } ?>>- - Female- -</option>
												<option value="3" <?php if($project_man_power_rate_list_data[0]["project_man_power_type_id"] == 3){ ?> selected="selected" <?php } ?>>- - Mason - -</option>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label class="control-label" for="cost_per_hours">Cost Per Hours</label>
											<div class="controls">
												<input type="number" class="span6" min="0" step="0.001" name="cost_per_hours" placeholder="Cost Per Hours" value="<?php echo $project_man_power_rate_list_data[0]["project_man_power_rate_cost_per_hours"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label class="control-label" for="applicable_date">Applicable Date</label>
											<div class="controls">
												<input type="date" class="span6" name="applicable_date" placeholder="Applicable Date" value="<?php echo $project_man_power_rate_list_data[0]["project_man_power_rate_applicable_date"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks" value="<?php echo $project_man_power_rate_list_data[0]["project_man_power_rate_remarks"] ;?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_man_power_rate_master_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
