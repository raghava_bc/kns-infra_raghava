$(document).ready(function() {

  var table = $('#example').dataTable({
    "sScrollX": "100%",
    "sScrollXInner": "110%",
    "bScrollCollapse": true,
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": 'datatable/project_process_remarks_datatable.php',
    "iDisplayLength": 10,
    "sScrollY": "50%",
    "bInfo": true,
    "iTabIndex": 1,
    "lengthChange": false,
    language: {
      search: "_INPUT_",
      searchPlaceholder: "Search...",
      "infoFiltered": ""
    },
    "fnServerParams": function(aoData) {
      aoData.push({
        "name": "table",
        "value": "process_remarks_list"
      });
      aoData.push({
        "name": "search_project",
        "value": getVars('search_project')
      });
      aoData.push({
        "name": "search_process",
        "value": getVars('search_process')
      });
      aoData.push({
        "name": "search_task",
        "value": getVars('search_task')
      });
      aoData.push({
        "name": "search_status",
        "value": getVars('search_status')
      });
    },
    "aaSorting": [
      [1, 'desc']
    ],
    "fnDrawCallback": function(oSettings) {
      /* Need to redo the counters if filtered or sorted */
      if (oSettings.bSorted || oSettings.bFiltered || oSettings.iDraw > 1) {
        for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
          $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + oSettings._iDisplayStart + 1);
        }
      }
    },
    "aoColumnDefs": [{
        "bSortable": false,
        "aTargets": [0]
      },
      {
        "bSortable": true,
        "aTargets": [1],
        "mData": `remarks_list_process_name`
      },
      {
        "bSortable": false,
        "aTargets": [2],
        "mData": 'project_task_master_name'
      },
      {
        "bSortable": false,
        "aTargets": [3],
        "mData": `remarks_list_manpower`
      },
      {
        "bSortable": false,
        "aTargets": [4],
        "mData": `remarks_list_machine`
      },
      {
        "bSortable": false,
        "aTargets": [5],
        "mData": `remarks_list_contract`
      },
      {
        "bSortable": false,
        "aTargets": [6],
        "mData": `remarks_list_material`
      },
      {
        "bSortable": false,
        "aTargets": [7],
        "mData": `remarks_list_remarks`
      },
      {
        "bSortable": false,
        "aTargets": [8],
        "mData": `user_name`
      },

      {
        "bSortable": false,
        "aTargets": [9],
        "mRender": function(data, type, full) {

          return moment(full['added_on']).format('DD-MM-YYYY');
        }
      },
      {
        "bSortable": true,
        "aTargets": [10],
        "mRender": function(data, type, full) {
          if (full['remarks_list_status'] == 'Pending') {
          return '<a href="#" id="complete">Complete</a>';
        }else {
          return 'Completed';
        }
        }
      },
    ],
  });

  $('#example tbody').on('click', 'tr', function(event) {
    if (event.target.tagName == 'A' && event.target.id == 'complete') {
        var rowData = table.api().row(this).data();
        var remarks_id = rowData['remarks_list_id'];
          var status = rowData['remarks_list_status'];
  $(document).ready(function(){
      $("#complete").click(function(){
        var remarks = prompt("Enter Remarks", "");
         if (remarks != null) {
           $.ajax({
             url: 'ajax/project_add_remarks.php',
             data: {remarks_id:remarks_id,remarks: remarks,status:status},
             dataType: 'json',
             success: function(response) {
                table.api().row().draw();
               }
           })
         }
      });
  });

    }

  });
});

// get hidden values saved from filters
function getVars(key) {
  var element = document.getElementById(key);
  if (element) {
    return element.value;
  } else {
    return '';
  }
}

// set search filters' as hidden value
function setVars(elements) {

  for (var element in elements) {
    if (element && elements[element].length) {
      var hiddenField1 = document.createElement("input");
      hiddenField1.setAttribute("type", "hidden");
      hiddenField1.setAttribute("id", element);
      hiddenField1.setAttribute("value", elements[element]);
      document.body.appendChild(hiddenField1);
    }
  }
}
