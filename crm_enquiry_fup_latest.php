<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: crm_enquiry_fup_list.php

CREATED ON	: 07-Sep-2015

CREATED BY	: Nitin Kashyap

PURPOSE     : List of Enquiries

*/

define('CRM_ENQUIRY_FOLLOW_UP_LIST_FUNC_ID','102');

/*

TBD:

*/
$_SESSION['module'] = 'CRM Transactions';


// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Get permission settings for this user for this page
	$add_perms_list     = i_get_user_perms($user,'',CRM_ENQUIRY_FOLLOW_UP_LIST_FUNC_ID,'1','1');
	$view_perms_list    = i_get_user_perms($user,'',CRM_ENQUIRY_FOLLOW_UP_LIST_FUNC_ID,'2','1');
	$edit_perms_list    = i_get_user_perms($user,'',CRM_ENQUIRY_FOLLOW_UP_LIST_FUNC_ID,'3','1');
	$delete_perms_list  = i_get_user_perms($user,'',CRM_ENQUIRY_FOLLOW_UP_LIST_FUNC_ID,'4','1');
	$approve_perms_list = i_get_user_perms($user,'',CRM_ENQUIRY_FOLLOW_UP_LIST_FUNC_ID,'6','1');


	// Query String / Get form Data

	if(isset($_GET["enquiry"]))

	{

		$enq_id = $_GET["enquiry"];

	}

	else

	{

		$enq_id = "";

	}

	if(isset($_GET['start']))
	{
		$start_page = $_GET['start'];
	}
	else
	{
		$start_page = 1;
	}
	$start = ''.($start_page - 1)*20;

	$day = "";

	if(isset($_POST["enquiry_fup_search_submit"]))

	{

		$follow_up_date = $_POST["day"];

		$status         = $_POST["ddl_search_int_status"];

		if(($role == 1) || ($role == 5))

		{

			$assigned_to = $_POST["ddl_search_assigned_to"];

		}

		else

		{

			$assigned_to = $user	;

		}

		$source         = $_POST['ddl_search_source'];
		$mobile 	 	= $_POST['num_mobile'];

	}

	else

	{

		if(isset($_GET['gfup_date']))

			{

					$follow_up_date = $_GET['gfup_date'];

				}

					else

						{
			$follow_up_date = "";

			}


			if(isset($_GET['gstatus']))

			{

					$status = $_GET['gstatus'];

				}

				else

				{

					$status = "";

				}


				if(isset($_GET['gassignee']))

					{

						$assigned_to = $_GET['gassignee'];

					}
		else

						{

							if(($role == 1) || ($role == 5))

							{

								$assigned_to = "-1";

							}

								else

								{

									$assigned_to = $user;

									}

		}


		if(isset($_GET['gsource']))

		{

														$source = $_GET['gsource'];

		}

		else

		{

			$source = "";

		}


													if(isset($_GET['gmobile']))

														{

																$mobile = $_GET['gmobile'];

		}

		else

		{

			$mobile = "";

															}
	}



	// Temp data

	$alert = "";



	if(($role == 1) || ($role == 5) || ($enq_id != ""))

	{

		$added_by = "";

	}

	else

	{

		$added_by = $user;

	}



	$enquiry_fup_latest = i_get_enquiry_fup_latest($enq_id,$follow_up_date,$status,'',$assigned_to,$added_by,'','','asc','assignee_or_assigner','','',$source,$user,'4','1','1',$start,20,$mobile);
	// var_dump(count($enquiry_fup_latest["data"]));
	if($enquiry_fup_latest["status"] == SUCCESS)

	{

		$enquiry_fup_latest_data = $enquiry_fup_latest["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$enquiry_fup_latest["data"];

	}



	// Interest Status List

	$int_status_list = i_get_interest_status_list('','1');

	if($int_status_list["status"] == SUCCESS)

	{

		$int_status_list_data = $int_status_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$int_status_list["data"];

	}



	// User List
	if($approve_perms_list['status'] == SUCCESS)
	{
		$manager = '';
	}
	else
	{
		$manager = $user;
	}
	$user_list = i_get_user_list('','','','','1','1',$manager);

	if($user_list["status"] == SUCCESS)

	{

		$user_list_data = $user_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$user_list["data"];

		$alert_type = 0; // Failure

	}



	// Source List

	$source_list = i_get_enquiry_source_list('','1');

	if($source_list["status"] == SUCCESS)

	{

		$source_list_data = $source_list["data"];

	}

	else

	{

		$alert = $alert."Alert: ".$source_list["data"];

	}

}

else

{

	header("location:login.php");

}

?>



<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="utf-8">

    <title>Enquiry Follow Up List</title>



    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">



    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">



    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">



    <link href="css/style.css" rel="stylesheet">







    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>



<div class="main">

  <div class="main-inner">

    <div class="container">

      <div class="row">



          <div class="span6" style="width:100%;">



          <div class="widget widget-table action-table">

            <div class="widget-header"> <i class="icon-th-list"></i>

              <h3>Enquiry Follow Up List&nbsp;&nbsp;&nbsp;&nbsp;<?php if($enq_id != ""){ ?><a href="crm_add_enquiry_fup.php?enquiry=<?php echo $enq_id; ?>">Add Follow Up</a><?php } ?><?php

				for($count = 0; $count < count($int_status_list_data); $count++)

				{

					?>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $int_status_list_data[$count]["crm_cust_interest_status_name"]; ?>: <span id="count_<?php echo $int_status_list_data[$count]["crm_cust_interest_status_id"]; ?>"></span><?php

				}

      		  ?></h3>

            </div>



			<div class="widget-header" style="height:80px; padding-top:10px;">

			  <form method="post" id="enquiry_fup_search" action="crm_enquiry_fup_latest.php">

			  <span style="padding-left:20px; padding-right:20px;">

			  <input type="date" name="day" value="<?php echo $follow_up_date; ?>" />

			  </span>

			  <span style="padding-left:20px; padding-right:20px;">

			  <input type="text" name="num_mobile" value="<?php echo $mobile; ?>" placeholder="Enter 10 digit mobile no" />

			  </span>

			  <span style="padding-left:20px; padding-right:20px;">

			  <select name="ddl_search_int_status">

			  <option value="">- - Select Interest Status - -</option>

			  <?php

				for($count = 0; $count < count($int_status_list_data); $count++)

				{

					?>

					<option value="<?php echo $int_status_list_data[$count]["crm_cust_interest_status_id"]; ?>" <?php

					if($status == $int_status_list_data[$count]["crm_cust_interest_status_id"])

					{

					?>

					selected="selected"

					<?php

					}?>><?php echo $int_status_list_data[$count]["crm_cust_interest_status_name"]; ?></option>

					<?php

				}

      		  ?>

			  </select>

			  </span>

			  <span style="padding-left:8px; padding-right:8px;">

			  <select name="ddl_search_source">

			  <option value="">- - Select Source - -</option>

			  <?php

				for($count = 0; $count < count($source_list_data); $count++)

				{

					?>

					<option value="<?php echo $source_list_data[$count]["enquiry_source_master_id"]; ?>" <?php

					if($source == $source_list_data[$count]["enquiry_source_master_id"])

					{

					?>

					selected="selected"

					<?php

					}?>><?php echo $source_list_data[$count]["enquiry_source_master_name"]; ?></option>

					<?php

				}

      		  ?>

			  </select>

			  </span>

			  <?php

			  if(($role == 1) || ($role == 5))

			  {

			  ?>

			  <span style="padding-left:20px; padding-right:20px;">

			  <select name="ddl_search_assigned_to">

			  <option value="">- - Select Assignee - -</option>

			  <?php

				for($count = 0; $count < count($user_list_data); $count++)

				{


					?>

					<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php

					if($assigned_to == $user_list_data[$count]["user_id"])

					{

					?>

					selected="selected"

					<?php

					}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>

					<?php


				}

      		  ?>

			  </select>

			  </span>

			  <?php

			  }

			  ?>

			  <input type="submit" name="enquiry_fup_search_submit" />

			  </form>

            </div>



            <!-- /widget-header -->

            <div class="widget-content">



              <table class="table table-bordered" style="table-layout: fixed;">

                <thead>

                  <tr>

					<th>SL No</th>

					<th>Enquiry No</th>

					<th>Source</th>

					<th>Project</th>

					<th>Name</th>

					<th>Mobile</th>

					<th>Walk In</th>

					<th>Latest Status</th>

					<th>Next FollowUp</th>

					<th>Lead Time</th>

					<th>Assigned To</th>

					<th>Assigned By</th>

					<th>Remarks</th>

					<th>&nbsp;</th>

				</tr>

				</thead>

				<tbody>

				<?php

				if($enquiry_fup_latest["status"] == SUCCESS)

				{

					$sl_no = 0;

					$unq_count = 0;

					$already_booked_count = 0;

					$unqualified_count    = 0;

					$count_array = array();

					$count_array = array_fill(1,count($int_status_list_data),0);
					$count_array[4] = "N.A";

					for($count = 0; $count < count($enquiry_fup_latest_data); $count++)

					{

						$enquiry_result = i_get_enquiry_list($enquiry_fup_latest_data[$count]["enquiry_id_for_follow_up"],'','','','','','','','','','','','','','','','','','','');
						$enquiry_data = $enquiry_result["data"];
						$difference = get_date_diff(date("Y-m-d",strtotime($enquiry_fup_latest_data[$count]["enquiry_follow_up_date"])),date("Y-m-d"));

						$sl_no++;
						$count_array[$enquiry_fup_latest_data[$count]["enquiry_follow_up_int_status"]] = $count_array[$enquiry_fup_latest_data[$count]["enquiry_follow_up_int_status"]] + 1;

						?>
						<tr <?php if($difference["data"] > 0){ ?>style="color:red;"<?php } ?>>
						<td style="word-wrap:break-word;"><?php echo ((($start_page - 1)*20) + $sl_no); ?></td>
						<td style="word-wrap:break-word;"><?php echo $enquiry_data[0]["enquiry_number"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $enquiry_data[0]["enquiry_source_master_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $enquiry_data[0]["project_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $enquiry_data[0]["name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $enquiry_data[0]["cell"]; ?></td>
						<td style="word-wrap:break-word;"><?php if($enquiry_data[0]["walk_in"] == "1")
						{
							echo "Yes";
						}
						else
						{
							echo "No";
						}?></td>
						<td style="word-wrap:break-word;"><?php echo $enquiry_fup_latest_data[$count]["crm_cust_interest_status_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo date("d-M-Y H:i",strtotime($enquiry_fup_latest_data[$count]["enquiry_follow_up_date"])); ?></td>
						<td style="word-wrap:break-word;"><?php echo $difference["data"];?></td>
						<td style="word-wrap:break-word;"><?php echo $enquiry_data[0]["user_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php $assigned_to_details = i_get_user_list($enquiry_data[0]["added_by"],'','','');
							echo $assigned_to_details["data"][0]["user_name"];?></td>
						<td style="word-wrap:break-word;"><a href="#" onclick="alert('<?php echo $enquiry_fup_latest_data[$count]["enquiry_follow_up_remarks"]; ?>');"><?php echo substr($enquiry_fup_latest_data[$count]["enquiry_follow_up_remarks"],0,30); ?></a></td>
						<td style="word-wrap:break-word;"><a href="crm_add_enquiry_fup.php?enquiry=<?php echo $enquiry_fup_latest_data[$count]["enquiry_id_for_follow_up"]; ?>&assigned_to=<?php echo $assigned_to ; ?>">Add Follow Up</a></td>
						</tr>
					<?php
					}
					$record_count = $count;

				}

				else

				{
					$record_count = 0;

				?>

				<td colspan="12">No enquiry follow ups!</td>

				<?php

				}

				 ?>



                </tbody>

				<?php

				for($count = 0; $count < count($int_status_list_data); $count++)

				{

				?>

				<script>

				document.getElementById("count_<?php echo $int_status_list_data[$count]["crm_cust_interest_status_id"]; ?>").innerHTML = <?php echo $count_array[$int_status_list_data[$count]["crm_cust_interest_status_id"]]; ?>;

				</script>

				<?php

				}

				?>

              </table>
			  <br>

			  <div>
			  <?php if($start_page > 1)
			  {
			  ?>
			  <span style="float:left; padding-left:20px;"><a href="crm_enquiry_fup_latest.php?start=<?php echo ($start_page - 1); ?>&gfup_date=<?php echo $follow_up_date; ?>&gstatus=<?php echo $status; ?>&gsource=<?php echo $source; ?>&gassignee=<?php echo $assigned_to; ?>">Prev</a></span>
			  <?php
			  }
			  ?>
			  <?php
			  if($record_count >= 20)
			  {
			  ?>
			  <span style="float:left; padding-left:20px;"><a href="crm_enquiry_fup_latest.php?start=<?php echo ($start_page + 1); ?>&gfup_date=<?php echo $follow_up_date; ?>&gstatus=<?php echo $status; ?>&gsource=<?php echo $source; ?>&gassignee=<?php echo $assigned_to; ?>">Next</a></span>
			  <?php
			  }
			  ?>
			  </div>

            </div>

            <!-- /widget-content -->

          </div>

          <!-- /widget -->



          </div>

          <!-- /widget -->

        </div>

        <!-- /span6 -->

      </div>

      <!-- /row -->

    </div>

    <!-- /container -->

  </div>

  <!-- /main-inner -->

</div>









<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">



                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->









<div class="footer">



	<div class="footer-inner">



		<div class="container">



			<div class="row">



    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.

    			</div> <!-- /span12 -->



    		</div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /footer-inner -->



</div> <!-- /footer -->







<script src="js/jquery-1.7.2.min.js"></script>



<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>

<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>



  </body>



</html>
