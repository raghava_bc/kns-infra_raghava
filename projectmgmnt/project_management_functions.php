<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_project_management.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_project_management_masters.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_mail.php');
/*
PURPOSE : To add Project Plan Process Task
INPUT 	: Process ID, Task Type, Actual Start Date, Actual End Date, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
        function i_add_project_process_task($process_id, $task_type, $location, $task_doc, $actual_start_date, $actual_end_date, $remarks, $added_by)
        {
            $project_process_task_search_data = array("process_id"=>$process_id,"task_type"=>$task_type,"location"=>$location,"active"=>'1');
            $project_process_task_sresult = db_get_project_process_task($project_process_task_search_data);
            if ($project_process_task_sresult["status"] == DB_NO_RECORD) {
                $project_process_task_iresult =  db_add_project_process_task($process_id, $task_type, $location, $task_doc, $actual_start_date, $actual_end_date, $remarks, $added_by);

                if ($project_process_task_iresult['status'] == SUCCESS) {
                    $return["data"]   = $project_process_task_iresult['data'];
                    $return["status"] = SUCCESS;
                } else {
                    $return["data"]   = "Internal Error. Please try again later";
                    $return["status"] = FAILURE;
                }
            } else {
                $return["data"]   = "This Project Process Task is already exist for this Project";
                $return["status"] = FAILURE;
            }

            return $return;
        }
/*
PURPOSE : To get Project Plan Process Task  list
INPUT 	: Task ID, Process ID, Task Type, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: Project Process Task List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_project_process_task($project_process_task_search_data)
{
    $project_process_task_sresult = db_get_project_process_task($project_process_task_search_data);
    if ($project_process_task_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_process_task_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Process Task Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Plan Process Task
INPUT 	: Task ID, Project Process Task Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_project_process_task($task_id, $project_process_task_update_data)
{
    $project_process_task_sresult = db_update_project_process_task($task_id, $project_process_task_update_data);

    if ($project_process_task_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Process Task Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To Delete Project Plan Process Task
INPUT 	: Task ID, Project Process Task Delete Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_project_process_task($task_id, $project_process_task_update_data)
{
    $project_process_task_sresult = db_update_project_process_task($task_id, $project_process_task_update_data);

    if ($project_process_task_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Process Task Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add Project Plan
INPUT 	: Project ID, User ID, Plan Start Date, Plan End Date, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_project_plan($project_id, $user_id, $plan_start_date, $plan_end_date, $remarks, $added_by)
{
    $project_plan_search_data = array("project_id"=>$project_id,"user_id"=>$user_id,"active"=>'1');
    $project_plan_sresult = db_get_project_plan($project_plan_search_data);
    if ($project_plan_sresult["status"] == DB_NO_RECORD) {
        $project_plan_iresult =  db_add_project_plan($project_id, $user_id, $plan_start_date, $plan_end_date, $remarks, $added_by);

        if ($project_plan_iresult['status'] == SUCCESS) {
            $return["data"]   = $project_plan_iresult['data'];
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Project Plan is already exist for this Project";
        $return["status"] = FAILURE;
    }


    return $return;
}


/*
PURPOSE : To get Project Plan list
INPUT 	: Plan ID, Project ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: project Plan List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_project_plan($project_plan_search_data)
{
    $project_plan_sresult = db_get_project_plan($project_plan_search_data);

    if ($project_plan_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_plan_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Plan Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Plan
INPUT 	: Plan ID, Project Plan Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_project_plan($plan_id, $project_id, $user_id, $project_plan_update_data)
{
    $project_plan_search_data = array("project_id"=>$project_id,"user_id"=>$user_id,"active"=>'1');
    $project_plan_sresult = db_get_project_plan($project_plan_search_data);
    if ($project_plan_sresult["status"] == DB_NO_RECORD) {
        $project_plan_sresult = db_update_project_plan($plan_id, $project_plan_update_data);

        if ($project_plan_sresult['status'] == SUCCESS) {
            $return["data"]   = "Project Plan Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Project Plan is already exist for this Project";
        $return["status"] = FAILURE;
    }


    return $return;
}

/*
PURPOSE : To Delete Project Plan
INPUT 	: Process ID, Project Plan Delete Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_delete_project_plan($plan_id, $project_plan_update_data)
{
    $project_plan_sresult = db_update_project_plan($plan_id, $project_plan_update_data);

    if ($project_plan_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Plan Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add Project Plan Process
INPUT 	: Plan ID, Name, Method, Process Start Date, Process End Date, Remarks, Added By, Assigned User
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_project_plan_process($plan_id, $name, $method, $process_start_date, $process_end_date, $remarks, $added_by, $approved_by, $assigned_user)
{
    $project_plan_process_search_data = array("plan_id"=>$plan_id,"name"=>$name,"active"=>'1');
    $project_plan_process_sresult = db_get_project_plan_process($project_plan_process_search_data);
    if ($project_plan_process_sresult["status"] == DB_NO_RECORD) {
        $project_plan_process_iresult =  db_add_project_plan_process($plan_id, $name, $method, $process_start_date, $process_end_date, $remarks, $added_by, $approved_by, $assigned_user);

        if ($project_plan_process_iresult['status'] == SUCCESS) {
            $return["data"]   = $project_plan_process_iresult['data'] ;
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Project Plan Process is already exist for this Project";
        $return["status"] = FAILURE;
    }


    return $return;
}

/*
PURPOSE : To get Project Plan Process list
INPUT 	: Process ID, Plan ID, Name, Method, Active, Status, Process Start Date, Process End Date, Added By, Approved By, Assigned User, Start Date(for added on), End Date(for added on)
OUTPUT 	: project Plan Process List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_project_plan_process($project_plan_process_search_data)
{
    $project_plan_process_sresult = db_get_project_plan_process($project_plan_process_search_data);

    if ($project_plan_process_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_plan_process_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Plan Process Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Plan Process
INPUT 	: Process ID, Project Plan Process Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_project_plan_process($process_id, $project_plan_process_update_data)
{
    $project_plan_process_sresult = db_update_project_plan_process($process_id, $project_plan_process_update_data);

    if ($project_plan_process_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Plan Process Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To Delete Project Plan Process
INPUT 	: Process ID, Project Plan Delete Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_delete_project_plan_process($process_id, $project_plan_process_update_data)
{
    $project_plan_process_sresult = db_update_project_plan_process($process_id, $project_plan_process_update_data);

    if ($project_plan_process_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Plan Process Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To Approve Project Plan Process
INPUT 	: Process ID, Project Plan Delete Approve Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_approve_project_plan_process($process_id, $project_plan_process_update_data)
{
    $project_plan_process_sresult = db_update_project_plan_process($process_id, $project_plan_process_update_data);

    if ($project_plan_process_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Plan Process Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To Delete Project Plan Process
INPUT 	: Process ID, Project Plan Delete Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_reject_project_plan_process($process_id, $project_plan_process_update_data)
{
    $project_plan_process_sresult = db_update_project_plan_process($process_id, $project_plan_process_update_data);

    if ($project_plan_process_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Plan Process Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add Project Task Required Item
INPUT 	: Material ID, Task ID, Quantity, Rate, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_project_task_required_item($material_id, $task_id, $quantity, $rate, $remarks, $added_by)
{
    $project_task_required_item_iresult =  db_add_project_task_required_item($material_id, $task_id, $quantity, $rate, $remarks, $added_by);

    if ($project_task_required_item_iresult['status'] == SUCCESS) {
        $return["data"]   = "Project Task Required Item Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
    return $return;
}
/*
PURPOSE : To get Project Task Required Item list
INPUT 	: Item ID, Material ID, Task ID, Quantity, Status, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: Project Task Required Item List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_project_task_required_item($project_task_required_item_search_data)
{
    $project_task_required_item_sresult = db_get_project_task_required_item($project_task_required_item_search_data);

    if ($project_task_required_item_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_task_required_item_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Task Required Item Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Task Required Item
INPUT 	: Item ID, Project Task Required Item Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_project_task_required_item($item_id, $project_task_required_item_update_data)
{
    $project_task_required_item_sresult = db_update_project_task_required_item($item_id, $project_task_required_item_update_data);

    if ($project_task_required_item_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Task Required Item Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}
/*
PURPOSE : To Delete Project Task Required Item
INPUT 	: Item ID, Project Task Required Item Delete Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_delete_project_task_required_item($item_id, $project_task_required_item_update_data)
{
    $project_task_required_item_sresult = db_update_project_task_required_item($item_id, $project_task_required_item_update_data);

    if ($project_task_required_item_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Task Required Item Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add new Project Task Man Power
INPUT 	: Task ID, Power Type ID, Hours, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_project_task_man_power($task_id, $power_type_id, $hours, $remarks, $added_by)
{
    $project_task_man_power_search_data = array("task_id"=>$task_id,"power_type_id"=>$power_type_id,"active"=>'1');
    $project_task_man_power_sresult = db_get_project_task_man_power($project_task_man_power_search_data);
    if ($project_task_man_power_sresult["status"] == DB_NO_RECORD) {
        $project_task_man_power_iresult =  db_add_project_task_man_power($task_id, $power_type_id, $hours, $remarks, $added_by);

        if ($project_task_man_power_iresult['status'] == SUCCESS) {
            $return["data"]   = "Project Task Man Power Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Project Task Man Power is already exist for this Project";
        $return["status"] = FAILURE;
    }
    return $return;
}

/*
PURPOSE : To get Project Task Man Power list
INPUT 	: Man Power ID, Task ID, Power Type ID, Hours, Status, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: Project Task Man Power List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_project_task_man_power($project_task_man_power_search_data)
{
    $project_task_man_power_sresult = db_get_project_task_man_power($project_task_man_power_search_data);

    if ($project_task_man_power_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_task_man_power_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Task Man Power added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Task Man Power
INPUT 	: Man Power ID, Project Task Man Power Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_project_task_man_power($man_power_id, $task_id, $power_type_id, $project_task_man_power_update_data)
{
    $project_task_man_power_search_data = array("task_id"=>$task_id,"power_type_id"=>$power_type_id,"active"=>'1');
    $project_task_man_power_sresult = db_get_project_task_man_power($project_task_man_power_search_data);
    if ($project_task_man_power_sresult["status"] == DB_NO_RECORD) {
        $project_task_man_power_sresult = db_update_project_task_man_power($man_power_id, $project_task_man_power_update_data);

        if ($project_task_man_power_sresult['status'] == SUCCESS) {
            $return["data"]   = "Project Task Man Power Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Project Task Man Power is already exist for this Project";
        $return["status"] = FAILURE;
    }
    return $return;
}

/*
PURPOSE : To Delete Project Task Man Power
INPUT 	: Man Power ID, Project Task Man Power Delete Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_delete_project_task_man_power($man_power_id, $project_task_man_power_update_data)
{
    $project_task_man_power_sresult = db_update_project_task_man_power($man_power_id, $project_task_man_power_update_data);

    if ($project_task_man_power_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Task Man Power Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add new Project Machine Vendor Mapping
INPUT 	: Machine ID, Vendor ID, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_project_machine_vendor_mapping($machine_id, $vendor_id, $remarks, $added_by)
{
    $project_machine_vendor_mapping_search_data = array("machine_id"=>$machine_id,"vendor_id"=>$vendor_id,"active"=>'1');
    $project_machine_vendor_mapping_sresult = db_get_project_machine_vendor_mapping($project_machine_vendor_mapping_search_data);
    if ($project_machine_vendor_mapping_sresult["status"] == DB_NO_RECORD) {
        $project_machine_vendor_mapping_iresult = db_add_project_machine_vendor_mapping($machine_id, $vendor_id, $remarks, $added_by);

        if ($project_machine_vendor_mapping_iresult['status'] == SUCCESS) {
            $return["data"]   = "Project Machine Vendor Mapping Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Project Machine Vendor Mapping is already exist for this Project";
        $return["status"] = FAILURE;
    }

    return $return;
}


/*
PURPOSE : To get Project Machine Vendor Mapping list
INPUT 	: Mapping ID, Machine ID, Vendor ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: Project Machine Vendor Mapping List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_project_machine_vendor_mapping($project_machine_vendor_mapping_search_data)
{
    $project_machine_vendor_mapping_sresult = db_get_project_machine_vendor_mapping($project_machine_vendor_mapping_search_data);

    if ($project_machine_vendor_mapping_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_machine_vendor_mapping_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Machine Vendor Mapping Added. Please contact the system admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Machine Vendor Mapping
INPUT 	: Mapping ID, Project Machine Vendor Mapping Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_project_machine_vendor_mapping($mapping_id, $machine_id, $vendor_id, $project_machine_vendor_mapping_update_data)
{
    $project_machine_vendor_mapping_search_data = array("machine_id"=>$machine_id,"vendor_id"=>$vendor_id,"active"=>'1');
    $project_machine_vendor_mapping_sresult = db_get_project_machine_vendor_mapping($project_machine_vendor_mapping_search_data);
    if ($project_machine_vendor_mapping_sresult["status"] == DB_NO_RECORD) {
        $project_machine_vendor_mapping_sresult =  db_update_project_machine_vendor_mapping($mapping_id, $project_machine_vendor_mapping_update_data);

        if ($project_machine_vendor_mapping_sresult['status'] == SUCCESS) {
            $return["data"]   = "Project Machine Vendor Mapping Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Project Machine Vendor Mapping is already exist for this Project";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To Delete Project Machine Vendor Mapping
INPUT 	: Mapping ID, Project Machine Vendor Mapping Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_delete_project_machine_vendor_mapping($mapping_id, $project_machine_vendor_mapping_update_data)
{
    $project_machine_vendor_mapping_sresult =  db_update_project_machine_vendor_mapping($mapping_id, $project_machine_vendor_mapping_update_data);

    if ($project_machine_vendor_mapping_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Machine Vendor Mapping Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add new Project Task Actual Man Power
INPUT 	: Task ID, Date, Men, Women, Mason, Others, Agency, No Of Man Hours, Completion Percent, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Punith
*/
function i_add_man_power($task_id, $road_id, $date, $men, $women, $mason, $others, $agency, $men_rate, $women_rate, $mason_rate, $no_of_man_hours, $completion_percent, $remarks, $added_by, $work_type="", $rework="")
{
    $man_power_search_data = array("date"=>$date,"agency"=>$agency,"task_id"=>$task_id,"road_id"=>$road_id);
    $man_power_sresult = db_get_man_power_list($man_power_search_data);
    if ($man_power_sresult['status'] == DB_NO_RECORD) {
        $man_power_iresult =  db_add_man_power($task_id, $road_id, $date, $men, $women, $mason, $others, $agency, $men_rate, $women_rate, $mason_rate, $no_of_man_hours, $completion_percent, $work_type, $rework, $remarks, $added_by);
        if ($man_power_iresult['status'] == SUCCESS) {
            $return["data"]   = $man_power_iresult['data'];
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
        return $return;
    }
}

/*
PURPOSE : To get Project Task Actual Man Power list
INPUT 	: man Power ID, Task ID, Date, Men, Women, Mason, Others, Agency, No Of Man Hours, Completion Percent, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: Project Task Actual Man Power List or Error Details, success or failure message
BY 		: Punith
*/
function i_get_man_power_list($man_power_search_data)
{
    $man_power_sresult = db_get_man_power_list($man_power_search_data);
    if ($man_power_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $man_power_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Actaul Man Power added yet!";
    }

    return $return;
}

/*
PURPOSE : To update Project Task Actual Man Power
INPUT 	: Man power ID, Man Power Update Array
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_update_man_power($man_power_id, $task_id, $man_power_update_data)
{
    $man_power_iresult = db_update_man_power($man_power_id, $man_power_update_data);

    if ($man_power_iresult['status'] == SUCCESS) {
        $return["data"]   = "man_power Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
}

/*
PURPOSE : To Delete Project Task Actual Man Power
INPUT 	: Man power ID, Man Power Update Array
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_delete_man_power($man_power_id, $man_power_update_data)
{
    $man_power_iresult = db_update_man_power($man_power_id, $man_power_update_data);

    if ($man_power_iresult['status'] == SUCCESS) {
        $return["data"]   = "man_power Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
    return $return;
}
/*
PURPOSE : To add new Project Man Power Estimate
INPUT 	: Task ID, Date, No of Men, No of Women, No of Mason, No of Others, Agency, Display Status, Remarks, Added By,
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_project_man_power_estimate($task_id, $date, $no_of_men, $no_of_women, $no_of_mason, $no_of_others, $agency, $remarks, $added_by)
{
    // Get man power estimate if alreaady added
    $project_man_power_estimate_search_data = array('task_id'=>$task_id,'active'=>'1');
    $project_man_power_estimate_sresult = db_get_project_man_power_estimate($project_man_power_estimate_search_data);

    if ($project_man_power_estimate_sresult['status'] != DB_RECORD_ALREADY_EXISTS) {
        $project_man_power_estimate_iresult = db_add_project_man_power_estimate($task_id, $date, $no_of_men, $no_of_women, $no_of_mason, $no_of_others, $agency, $remarks, $added_by);

        if ($project_man_power_estimate_iresult['status'] == SUCCESS) {
            $return["data"]   = "Man Power Estimate Successfully added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "Manpower Plan already exists for this task";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get project Man Power Estimate list
INPUT 	: Estimate ID, Task ID, Date, No of Men, No of Women, No of Mason, No of Others, Agency, Display Status, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of project Man Power Estimate, success or failure message
BY 		: Lakshmi
*/
function i_get_project_man_power_estimate($project_man_power_estimate_search_data)
{
    $project_man_power_estimate_sresult = db_get_project_man_power_estimate($project_man_power_estimate_search_data);

    if ($project_man_power_estimate_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_man_power_estimate_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Man Power Estimate Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Man Power Estimate
INPUT 	: Estimate ID, Project Man Power Estimate Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_project_man_power_estimate($estimate_id, $task_id="", $project_man_power_estimate_update_data)
{
    $project_man_power_estimate_sresult = db_update_project_man_power_estimate($estimate_id, $task_id, $project_man_power_estimate_update_data);

    if ($project_man_power_estimate_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Man Power Estimate Master Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To Delete Project Man Power Estimate
INPUT 	: Estimate ID, Project Man Power Estimate Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_delete_project_man_power_estimate($estimate_id, $project_man_power_estimate_update_data)
{
    $project_man_power_estimate_sresult = db_update_project_man_power_estimate($estimate_id, $project_man_power_estimate_update_data);

    if ($project_man_power_estimate_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Man Power Estimate Master Successfully deleted";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add Project User Mapping
INPUT 	: Project ID, User ID, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_project_user_mapping($project_id, $user_id, $remarks, $added_by)
{
    $project_user_mapping_search_data = array("project_id"=>$project_id,"user_id"=>$user_id,"active"=>'1');
    $project_user_mapping_sresult = db_get_project_user_mapping($project_user_mapping_search_data);
    if ($project_user_mapping_sresult["status"] == DB_NO_RECORD) {
        $project_user_mapping_iresult = db_add_project_user_mapping($project_id, $user_id, $remarks, $added_by);

        if ($project_user_mapping_iresult['status'] == SUCCESS) {
            $return["data"]   = "Project User Mapping Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Project User Mapping is already exist for this Project";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Project User Mapping list
INPUT 	: Mapping Id, Project ID,, User ID, Status, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: Project User Mapping List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_project_user_mapping($project_user_mapping_search_data)
{
    $project_user_mapping_sresult = db_get_project_user_mapping($project_user_mapping_search_data);

    if ($project_user_mapping_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_user_mapping_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project User Mapping Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project User Mapping
INPUT 	: Mapping ID,Project User Mapping Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_project_user_mapping($mapping_id, $project_id, $user_id, $project_user_mapping_update_data)
{
    $project_user_mapping_search_data = array("project_id"=>$project_id,"user_id"=>$user_id,"active"=>'1');
    $project_user_mapping_sresult = db_get_project_user_mapping($project_user_mapping_search_data);
    if ($project_user_mapping_sresult["status"] == DB_NO_RECORD) {
        $project_user_mapping_sresult = db_update_project_user_mapping($mapping_id, $project_user_mapping_update_data);

        if ($project_user_mapping_sresult['status'] == SUCCESS) {
            $return["data"]   = "Project User Mapping Successfully added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Project User Mapping is already exist for this Project";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To Delete Project User Mapping
INPUT 	: Mapping ID,Project User Mapping Delete Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_delete_project_user_mapping($mapping_id, $project_user_mapping_update_data)
{
    $project_user_mapping_sresult = db_update_project_user_mapping($mapping_id, $project_user_mapping_update_data);

    if ($project_user_mapping_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project User Mapping Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}
/*
PURPOSE : To add Project Task Indent
INPUT 	: Task Indent Id, Task Id, Actual Start Date, Actual End Date, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi
*/
function i_add_project_task_indent($task_id, $indent_id, $remarks, $added_by)
{
    $project_task_indent_iresult =  db_add_project_task_indent($task_id, $indent_id, $remarks, $added_by);

    if ($project_task_indent_iresult['status'] == SUCCESS) {
        $return["data"]   = $project_task_indent_iresult['data'];
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Project Task Indent list
INPUT 	: Indent Task ID,Task Id,Indent Id, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: Project Task Indent List or Error Details, success or failure message
BY 		: Sonakshi
*/
function i_get_project_task_indent($project_task_indent_search_data)
{
    $project_task_indent_sresult = db_get_project_task_indent_list($project_task_indent_search_data);
    if ($project_task_indent_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_task_indent_sresult	["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Process Task Added. Please Contact The System Admin";
    }

    return $return;
}
/*
PURPOSE : To add Project Machine Planning
INPUT 	: Task ID, Machine ID, Machine Rate, No of Hours, Additional Cost, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_project_machine_planning($task_id, $machine_id, $machine_rate, $no_of_hours, $additional_cost, $remarks, $added_by)
{
    $project_machine_planning_search_data = array("task_id"=>$task_id,"machine_id"=>$machine_id,"active"=>'1');
    $project_machine_planning_sresult = db_get_project_machine_planning($project_machine_planning_search_data);
    if ($project_machine_planning_sresult["status"] == DB_NO_RECORD) {
        $project_machine_planning_iresult = db_add_project_machine_planning($task_id, $machine_id, $machine_rate, $no_of_hours, $additional_cost, $remarks, $added_by);

        if ($project_machine_planning_iresult['status'] == SUCCESS) {
            $return["data"]   = "Project Machine Planning Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "This Project Machine Planning is already exist for this Project";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Project Machine Planning list
INPUT 	: Planning ID, Task ID, Machine ID, No of Hours, Additional Cost, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: Project Machine Planning List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_project_machine_planning($project_machine_planning_search_data)
{
    $project_machine_planning_sresult = db_get_project_machine_planning($project_machine_planning_search_data);

    if ($project_machine_planning_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_machine_planning_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Machine Planning Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Machine Planning
INPUT 	: Planning ID,Project Machine Planning Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_project_machine_planning($planning_id, $task_id, $machine_id, $project_machine_planning_update_data)
{
    $project_machine_planning_sresult = db_update_project_machine_planning($planning_id, $project_machine_planning_update_data);

    if ($project_machine_planning_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Machine Planning Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To update Project Machine Planning Cancelled
INPUT 	: Planning ID,Project Machine Planning Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_project_machine_plan_cancel($planning_id, $task_id="", $project_machine_planning_update_data)
{
    $project_machine_planning_sresult = db_update_project_machine_planning($planning_id, $task_id, $project_machine_planning_update_data);

    if ($project_machine_planning_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Machine Planning Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To Delete Project Machine Planning
INPUT 	: Planning ID,Project Machine Planning Delete Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_delete_project_machine_planning($planning_id, $project_machine_planning_update_data)
{
    $project_machine_planning_sresult = db_update_project_machine_planning($planning_id, $project_machine_planning_update_data);

    if ($project_machine_planning_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Machine Planning Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}
/*
PURPOSE : To add new Task Actual Machine plann
INPUT 	: Task ID,Machine Id,Start Date Time,Start End Time,Additional Cost, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_actual_machine_plan($task_id, $road_id, $machine_id, $start_date_time, $end_date_time, $additional_cost, $machine_vendor, $machine_number, $machine_fuel_charges, $machine_with_fuel_charges, $machine_bata, $machine_issued_fuel, $machine_type, $remarks, $added_by, $work_type="", $rework_completion="")
{
    // Check if machine is in use
    $actual_machine_plan_search_data = array("machine_id"=>$machine_id,"incomplete_check"=>'1',"active"=>'1');
    $machine_in_use_sresult = db_get_actual_machine_plan($actual_machine_plan_search_data);

    if ($machine_in_use_sresult['status'] == DB_NO_RECORD) {
        // Check for time overlap
        $actual_machine_plan_search_data = array("is_overlap"=>$start_date_time,"machine_id"=>$machine_id,"active"=>'1');
        $actual_machine_plan_sresult = db_get_actual_machine_plan($actual_machine_plan_search_data);
        if ($actual_machine_plan_sresult['status'] == DB_NO_RECORD) {
            $man_power_iresult =db_add_project_actual_machine_plan($task_id, $road_id, $machine_id, $start_date_time, $end_date_time, $additional_cost, $machine_vendor, $machine_number, $machine_fuel_charges, $machine_with_fuel_charges, $machine_bata, $machine_issued_fuel, $work_type, $rework_completion, $machine_type, $remarks, $added_by);
            if ($man_power_iresult['status'] == SUCCESS) {
                $return["data"]   = "Project Task actual Machine Plan Successfully added";
                $return["status"] = SUCCESS;
            } else {
                $return["data"]   = "Internal Error. Please try again later";
                $return["status"] = FAILURE;
            }
        } else {
            $return["data"]   = "Overlapping times not allowed";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "Machine in use. Please close the previous task before starting new task";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get actual task Machine Plan
INPUT 	: Man power Search Data
OUTPUT 	: Project Task Actual Machine Planning List or Error Details, success or failure message
BY 		: Sonakshi
*/
function i_get_machine_planning_list($actual_machine_plan_search_data)
{
    $actual_machine_plan_sresult = db_get_actual_machine_plan($actual_machine_plan_search_data);
    if ($actual_machine_plan_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $actual_machine_plan_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Actaul Machine plan added yet!";
    }

    return $return;
}

/*
PURPOSE : To update Project Task Actual Machine Plan
INPUT 	: Machine Planning ID, Machine Planning Update Array
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi
*/
function i_update_actual_machine_plan($plan_id, $actual_machine_plan_update_data)
{
    $allow = false;

    if ($actual_machine_plan_update_data['end_date_time'] != '0000-00-00 00:00:00') {
        // Check for time overlap
        $actual_machine_plan_search_data = array("is_overlap"=>$actual_machine_plan_update_data['end_date_time'],"machine_id"=>$actual_machine_plan_update_data['machine_id'],"active"=>'1');
        $actual_machine_plan_sresult = db_get_actual_machine_plan($actual_machine_plan_search_data);
        if ($actual_machine_plan_sresult['status'] == DB_NO_RECORD) {
            $allow = true;
        } else {
            for ($count = 0; $count < count($actual_machine_plan_sresult['data']); $count++) {
                if ($plan_id == $actual_machine_plan_sresult['data'][$count]['project_task_actual_machine_plan_id']) {
                    $allow = true;
                }
            }
        }
    } else {
        $allow = true;
    }

    if ($allow) {
        $man_power_iresult = db_update_actual_machine_plan($plan_id, $actual_machine_plan_update_data);

        if ($man_power_iresult['status'] == SUCCESS) {
            $return["data"]   = "Machine Plan Successfully Updated";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "Plan already exists";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To check Project Task Actual Machine Plan
INPUT 	: Machine Planning ID, Machine Planning Update Array
OUTPUT 	: Message, success or failure message
BY 		: Nitin
*/
function i_check_actual_machine_plan($plan_id, $actual_machine_plan_update_data)
{
    $man_power_iresult = db_update_actual_machine_plan($plan_id, $actual_machine_plan_update_data);

    if ($man_power_iresult['status'] == SUCCESS) {
        $return["data"]   = "Machine Plan Successfully checked";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
}

/*
PURPOSE : To approve Project Task Actual Machine Plan
INPUT 	: Machine Planning ID, Machine Planning Update Array
OUTPUT 	: Message, success or failure message
BY 		: Nitin
*/
function i_approve_actual_machine_plan($plan_id, $actual_machine_plan_update_data)
{
    $machine_aresult = db_update_actual_machine_plan($plan_id, $actual_machine_plan_update_data);

    if ($machine_aresult['status'] == SUCCESS) {
        $return["data"]   = "Machine Plan Successfully checked";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
}

/*
PURPOSE : To Delete Project Task Actual Machine Plan
INPUT 	: Machine Plan ID, Man Power Update Array
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi
*/
function i_delete_actual_machine_plan($plan_id, $actual_machine_plan_update_data)
{
    $man_power_iresult = db_update_actual_machine_plan($plan_id, $actual_machine_plan_update_data);

    if ($man_power_iresult['status'] == SUCCESS) {
        $return["data"]   = "Machine Plan Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
    return $return;
}
/*
PURPOSE : To add new Delay Reason Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_project_process_delay_reason($start_date, $end_date, $task_id, $name, $remarks, $added_by)
{
    $process_delay_reason_iresult =  db_add_project_process_delay_reason($start_date, $end_date, $task_id, $name, $remarks, $added_by);

    if ($process_delay_reason_iresult['status'] == SUCCESS) {
        $return["data"]   = "Process delay Reason Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Process Delay Reason List
INPUT 	: Reason ID, Name,Start Date ,End Date, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Delay Reason Master, success or failure message
BY 		: Sonakshi
*/
function i_get_project_process_delay_reason($process_delay_reason_search_data)
{
    $process_delay_reason_sresult = db_get_project_process_delay_reason($process_delay_reason_search_data);

    if ($process_delay_reason_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$process_delay_reason_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Process Delay Reason Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Process Delay Reason
INPUT 	: Reason ID, Process  Delay Reason Update Array
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi
*/

function i_update_project_process_delay_reason($reason_id, $process_delay_reason_update_data)
{
    $delay_reason_sresult = db_update_project_process_delay_reason($reason_id, $process_delay_reason_update_data);

    if ($delay_reason_sresult['status'] == SUCCESS) {
        $return["data"]   = "Process delay Reason Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add new Task Method Planning
INPUT 	: Task ID , Doc, Plan Type, Remarks, Added By
OUTPUT 	: Planning ID, success or failure message
BY 		: Lakshmi
*/
function i_add_project_task_method_planning($task_id, $document, $plan_type, $remarks, $added_by)
{
    $project_task_method_planning_iresult = db_add_project_task_method_planning($task_id, $document, $plan_type, $remarks, $added_by);

    if ($project_task_method_planning_iresult['status'] == SUCCESS) {
        $return["data"]   = "Task Method Planning Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
    return $return;
}

/*
PURPOSE : To get Project Task Method Planning List
INPUT 	: Planning ID, Task ID, Document, Plan Type, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Method Planning
BY 		: Lakshmi
*/
function i_get_project_task_method_planning($project_task_method_planning_search_data)
{
    $project_task_method_planning_sresult = db_get_project_task_method_planning($project_task_method_planning_search_data);

    if ($project_task_method_planning_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_task_method_planning_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Task Method Planning  added. Please contact the system admin";
    }

    return $return;
}

 /*
PURPOSE : To update Method Planning list
INPUT 	: Planning ID, Method Planning Update Array
OUTPUT 	: Message of success or failure
BY 		: Lakshmi
*/
function i_update_project_task_method_planning($planning_id, $project_task_method_planning_update_data)
{
    $project_task_method_planning_sresult = db_update_project_task_method_planning($planning_id, $project_task_method_planning_update_data);

    if ($project_task_method_planning_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Task Method Planning Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

 /*
PURPOSE : To Delete Method Planning list
INPUT 	: Planning ID, Method Planning Update Array
OUTPUT 	: Message of success or failure
BY 		: Lakshmi
*/
function i_delete_project_task_method_planning($planning_id, $project_task_method_planning_update_data)
{
    $project_task_method_planning_update_data = array('active'=>'0');
    $project_task_method_planning_sresult = db_update_project_task_method_planning($planning_id, $project_task_method_planning_update_data);

    if ($project_task_method_planning_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Task Method Planning Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add new Project Task BOQ
INPUT 	: UOM, Number, Remarks, Added By
OUTPUT 	: BOQ ID, success or failure message
BY 		: Lakshmi
*/
function i_add_project_task_boq($task_id, $process, $contract_task, $uom, $location, $number, $length, $breadth, $depth, $total_sqft, $amount, $remarks, $added_by)
{
    // Get BOQ
    $project_task_boq_search_data = array('task_id'=>$task_id,"contract_task"=>$contract_task,"active"=>'1',"location"=>$location);
    $project_task_boq_sresult = db_get_project_task_boq($project_task_boq_search_data);

    if ($project_task_boq_sresult['status'] != DB_RECORD_ALREADY_EXISTS) {
        $project_task_boq_iresult = db_add_project_task_boq($task_id, $process, $contract_task, $uom, $location, $number, $length, $breadth, $depth, $total_sqft, $amount, $remarks, $added_by);

        if ($project_task_boq_iresult['status'] == SUCCESS) {
            $return["data"]   = "Project Task BOQ Successfully added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "Plan already exists for this contract work for this task";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Project Task BOQ List
INPUT 	: BOQ ID, UOM, Number, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Task BOQ
BY 		: Lakshmi
*/
function i_get_project_task_boq($project_task_boq_search_data)
{
    $project_task_boq_sresult = db_get_project_task_boq($project_task_boq_search_data);

    if ($project_task_boq_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_task_boq_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Task BOQ Added. Please Contact The System Admin";
    }

    return $return;
}

 /*
PURPOSE : To update Project Task BOQ
INPUT 	: BOQ ID, Project Task BOQ Update Array
OUTPUT 	: BOQ ID; Message of success or failure
BY 		: Lakshmi
*/
function i_update_project_task_boq($boq_id, $task_id, $project_task_boq_update_data)
{
    $project_task_boq_sresult = db_update_project_task_boq($boq_id, $task_id, $project_task_boq_update_data);

    if ($project_task_boq_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Task BOQ Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add new Project Task BOQ Actual
INPUT 	: Task ID, UOM, Number, Amount, Completion, Remarks, Added By
OUTPUT 	: BOQ Actual ID, success or failure message
BY 		: Lakshmi
*/
function i_add_project_task_boq_actual($task_id, $road_id, $vendor_id, $date, $process, $contract_task, $uom, $location, $number, $length, $breadth, $depth, $total_sqft, $amount, $total_amount, $remarks, $added_by, $work_type="", $rework_completion="")
{
    /*$project_task_actual_boq_search_data = array('task_id'=>$task_id,'vendor_id'=>$vendor_id,'date'=>$date,'contract_task'=>$contract_task,'active'=>'1','location'=>$location);
    $project_task_boq_actual_sresult = db_get_project_task_actual_boq($project_task_actual_boq_search_data);*/

    //if($project_task_boq_actual_sresult['status'] == DB_NO_RECORD)
    //{
    $project_task_boq_actual_iresult = db_add_project_task_actual_boq($task_id, $road_id, $vendor_id, $date, $process, $contract_task, $uom, $location, $number, $length, $breadth, $depth, $total_sqft, $amount, $total_amount, $work_type, $rework_completion, $remarks, $added_by);
    if ($project_task_boq_actual_iresult['status'] == SUCCESS) {
        $return["data"]   = "Project Task BOQ Actual Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
    //}
    //else
    //{
    //$return["data"]   = "Data already exists for this contract task for this task";
    //$return["status"] = FAILURE;
    //}

    return $return;
}

/*
PURPOSE : To get Project Task BOQ Actual List
INPUT 	: BOQ  Actual ID, UOM, Number, Amount, Completion, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Task BOQ Actual
BY 		: Lakshmi
*/
function i_get_project_task_boq_actual($project_task_actual_boq_search_data)
{
    $project_task_boq_actual_sresult = db_get_project_task_actual_boq($project_task_actual_boq_search_data);

    if ($project_task_boq_actual_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_task_boq_actual_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Task BOQ Actual Added. Please Contact The System Admin";
    }

    return $return;
}

 /*
PURPOSE : To update Project Task BOQ Actual
INPUT 	: BOQ Actual ID, Project Task BOQ Actual Update Array
OUTPUT 	: BOQ Actual ID; Message of success or failure
BY 		: Lakshmi
*/
function i_update_project_task_boq_actual($boq_id, $project_task_actual_boq_update_data)
{
    $project_task_boq_actual_sresult = db_update_project_task_actual_boq($boq_id, $project_task_actual_boq_update_data);

    if ($project_task_boq_actual_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Task BOQ Actual Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}
/*
PURPOSE : To add new Project Actual Payment ManPower
INPUT 	: Task ManPower ID, Vendor ID, Amount, From date, To Date, Remarks, Added By
OUTPUT 	: ManPower ID, success or failure message
BY 		: Lakshmi
*/
function i_add_project_actual_payment_manpower($task_manpower_id, $vendor_id, $amount, $man_hrs, $from_date, $to_date, $remarks, $added_by, $bill_no="")
{
    $project_actual_payment_manpower_iresult = db_add_project_actual_payment_manpower($task_manpower_id, $vendor_id, $amount, $man_hrs, $bill_no, $from_date, $to_date, $remarks, $added_by);

    if ($project_actual_payment_manpower_iresult['status'] == SUCCESS) {
        $return["data"]   = $project_actual_payment_manpower_iresult['data'] ;
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
    return $return;
}

/*
PURPOSE : To get Project Actual Payment ManPower list
INPUT 	: ManPower ID, Task ManPower ID, Vendor ID, Amount, Status,  From date, To Date, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of project Actual Payment ManPower
BY 		: Lakshmi
*/
function i_get_project_actual_payment_manpower($project_actual_payment_manpower_search_data)
{
    $project_actual_payment_manpower_sresult = db_get_project_actual_payment_manpower($project_actual_payment_manpower_search_data);

    if ($project_actual_payment_manpower_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_actual_payment_manpower_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Payment ManPower added. Please contact the system admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Actual Payment ManPower
INPUT 	: ManPower ID, Project Actual Payment ManPower Update Array
OUTPUT 	: ManPower ID; Message of success or failure
BY 		: Lakshmi
*/
function i_update_project_actual_payment_manpower($manpower_id, $project_actual_payment_manpower_update_data)
{
    $machine_actual_payment_manpower_iresult = db_update_project_actual_payment_manpower($manpower_id, $project_actual_payment_manpower_update_data);

    if ($machine_actual_payment_manpower_iresult['status'] == SUCCESS) {
        $return["data"]   = "Payment ManPower Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
    return $return;
}
/*
PURPOSE : To add new Project Man Power Issue Payment
INPUT 	: Payment ID, Man Power ID, Amount, Vendor ID, Payment Mode, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Ashwini
*/
function i_add_project_man_power_issue_payment($man_power_id, $amount, $deduction, $vendor_id, $payment_mode, $instrument_details, $remarks, $added_by)
{
    $project_man_power_issue_payment_iresult = db_add_project_man_power_issue_payment($man_power_id, $amount, $deduction, $vendor_id, $payment_mode, $instrument_details, $remarks, $added_by);

    if ($project_man_power_issue_payment_iresult['status'] == SUCCESS) {
        $return["data"]   = "Man Power Issue Payment Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get project Man Power Issue Payment list
INPUT 	: Payment ID, Man Power ID, Amount, Vendor ID, Payment Mode, Active, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Man Power Issue Payment, success or failure message
BY 		: Ashwini
*/
function i_get_project_man_power_issue_payment($project_man_power_issue_payment_search_data)
{
    $project_man_power_issue_payment_sresult = db_get_project_man_power_issue_payment($project_man_power_issue_payment_search_data);

    if ($project_man_power_issue_payment_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_man_power_issue_payment_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Man Power Issue Payment Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Man Power Issue Payment
INPUT 	: Payment ID, Project Man Power Issue Payment Update Array
OUTPUT 	: Message, success or failure message
BY 		: Ashwini
*/

function i_update_project_man_power_issue_payment($payment_id, $project_man_power_issue_payment_update_data)
{
    $project_man_power_issue_payment_sresult = db_update_project_man_power_issue_payment($payment_id, $project_man_power_issue_payment_update_data);

    if ($project_man_power_issue_payment_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Man Power Issue Payment Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}
/*
PURPOSE : To Delete Project Man Power Issue Payment
INPUT 	: Payment ID,Project Man Power Issue Payment Delete Array
OUTPUT 	: Message, success or failure message
BY 		: Ashwini
*/

function i_delete_project_man_power_issue_payment($payment_id, $project_man_power_issue_payment_update_data)
{
    $project_man_power_issue_payment_sresult = db_update_project_man_power_issue_payment($payment_id, $project_man_power_issue_payment_update_data);

    if ($project_man_power_issue_payment_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Man Power Issue Payment Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add new Project Payment Manpower Mapping
INPUT 	: Manpower ID, Payment ID, Remarks, Added By
OUTPUT 	: Manpower Mapping ID, success or failure message
BY 		: Lakshmi
*/
function i_add_project_payment_manpower_mapping($manpower_id, $payment_id, $remarks, $added_by)
{
    $project_payment_manpower_mapping_iresult = db_add_project_payment_manpower_mapping($manpower_id, $payment_id, $remarks, $added_by);

    if ($project_payment_manpower_mapping_iresult['status'] == SUCCESS) {
        $return["data"]   = "Payment Payment Manpower Mapping Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
    return $return;
}

/*
PURPOSE : To get Project Payment Manpower  Mapping list
INPUT 	: Manpower Mapping ID, Manpower ID, Payment ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Payment Manpower  Mapping
BY 		: Lakshmi
*/
function i_get_project_payment_manpower_mapping($project_payment_manpower_mapping_search_data)
{
    $project_payment_manpower_mapping_sresult = db_get_project_payment_manpower_mapping($project_payment_manpower_mapping_search_data);

    if ($project_payment_manpower_mapping_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_payment_manpower_mapping_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Payment Manpower Mapping added. Please contact the system admin";
    }

    return $return;
}

 /*
PURPOSE : To update Project Payment Manpower Mapping
INPUT 	: Manpower Mapping ID, Project Payment Manpower Mapping Update Array
OUTPUT 	: Manpower Mapping ID; Message of success or failure
BY 		: Lakshmi
*/
function i_update_project_payment_manpower_mapping($manpower_mapping_id, $project_payment_manpower_mapping_update_data)
{
    $project_payment_manpower_mapping_iresult = db_update_project_payment_manpower_mapping($manpower_mapping_id, $project_payment_manpower_mapping_update_data);

    if ($project_payment_manpower_mapping_iresult['status'] == SUCCESS) {
        $return["data"]   = "Payment Payment Manpower Mapping Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
    return $return;
}
/*
PURPOSE : To add new Project Contract Issue Payment
INPUT 	: Payment ID, Contract ID, Amount, Deduction, Vendor ID, Payment Mode, Instrument Details, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Ashwini
*/
function i_add_project_contract_issue_payment($contract_id, $amount, $deduction, $vendor_id, $payment_mode, $instrument_details, $remarks, $added_by)
{
    $project_contract_issue_payment_iresult = db_add_project_contract_issue_payment($contract_id, $amount, $deduction, $vendor_id, $payment_mode, $instrument_details, $remarks, $added_by);

    if ($project_contract_issue_payment_iresult['status'] == SUCCESS) {
        $return["data"]   = "Contract Issue Payment Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get project Contract Issue Payment list
INPUT 	: Payment ID, Contract ID, Amount, Deduction, Vendor ID, Payment Mode, Instrument Details, Active, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Contract Issue Payment, success or failure message
BY 		: Ashwini
*/
function i_get_project_contract_issue_payment($project_contract_issue_payment_search_data)
{
    $project_contract_issue_payment_sresult = db_get_project_contract_issue_payment($project_contract_issue_payment_search_data);

    if ($project_contract_issue_payment_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_contract_issue_payment_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Contract Issue Payment Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Contract Issue Payment
INPUT 	: Payment ID, Project Contract Issue Payment Update Array
OUTPUT 	: Message, success or failure message
BY 		: Ashwini
*/

function i_update_project_contract_issue_payment($payment_id, $project_contract_issue_payment_update_data)
{
    $project_contract_issue_payment_sresult = db_update_project_contract_issue_payment($payment_id, $project_contract_issue_payment_update_data);

    if ($project_contract_issue_payment_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Contract Issue Payment Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}
/*
PURPOSE : To Delete Project Contract Issue Payment
INPUT 	: Payment ID, Project Contract Issue Payment Delete Array
OUTPUT 	: Message, success or failure message
BY 		: Ashwini
*/

function i_delete_project_contract_issue_payment($payment_id, $project_contract_issue_payment_update_data)
{
    $project_contract_issue_payment_sresult = db_update_project_contract_issue_payment($payment_id, $project_contract_issue_payment_update_data);

    if ($project_contract_issue_payment_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Contract Issue Payment Successfully deleted";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}/*
PURPOSE : To add new Project Payment Contract Mapping
INPUT 	: Contract ID, Payment ID, Remarks, Added By
OUTPUT 	: Contract Mapping ID, success or failure message
BY 		: Ashwini
*/
function i_add_project_payment_contract_mapping($contract_id, $payment_id, $remarks, $added_by)
{
    $project_payment_contract_mapping_iresult = db_add_project_payment_contract_mapping($contract_id, $payment_id, $remarks, $added_by);

    if ($project_payment_contract_mapping_iresult['status'] == SUCCESS) {
        $return["data"]   = "Payment Payment Contract Mapping Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
    return $return;
}

/*
PURPOSE : To get Project Payment Contract  Mapping list
INPUT 	: Contract Mapping ID, Contract ID, Payment ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Payment Contract  Mapping
BY 		: Ashwini
*/
function i_get_project_payment_contract_mapping($project_payment_contract_mapping_search_data)
{
    $project_payment_contract_mapping_sresult = db_get_project_payment_contract_mapping($project_payment_contract_mapping_search_data);

    if ($project_payment_contract_mapping_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_payment_contract_mapping_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Payment Contract Mapping added. Please contact the system admin";
    }

    return $return;
}

 /*
PURPOSE : To update Project Payment Contract Mapping
INPUT 	: Contract Mapping ID, Project Payment Contract Mapping Update Array
OUTPUT 	: Contract Mapping ID; Message of success or failure
BY 		: Ashwini
*/
function i_update_project_payment_contract_mapping($contract_mapping_id, $project_payment_contract_mapping_update_data)
{
    $project_payment_contract_mapping_iresult = db_update_project_payment_contract_mapping($contract_mapping_id, $project_payment_contract_mapping_update_data);

    if ($project_payment_contract_mapping_iresult['status'] == SUCCESS) {
        $return["data"]   = "Payment Payment Contract Mapping Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
    return $return;
}

/*
PURPOSE : To add new Project Actual Contract Payment
INPUT 	: Contract Payment ID, From Date, To Date, Contract ID, Vendor ID, Amount, Deposit Amount, Number, Length, Breadth, Depth, Total Measurement, UOM, Rate, Status, Remarks, Accepted By, Accepted On, Approved By, Approved On, Added By
OUTPUT 	: Contract Payment ID, success or failure message
BY 		: Ashwini
*/
function i_add_project_actual_contract_payment($contract_id, $from_date, $to_date, $vendor_id, $amount, $deposit_amount, $number, $length, $breadth, $depth, $total_measurement, $uom, $rate, $remarks, $added_by, $bill_no="")
{
    $project_actual_contract_payment_iresult = db_add_project_actual_contract_payment($contract_id, $from_date, $to_date, $vendor_id, $amount, $deposit_amount, $number, $length, $breadth, $depth, $total_measurement, $uom, $rate, $bill_no, $remarks, $added_by);

    if ($project_actual_contract_payment_iresult['status'] == SUCCESS) {
        $return["data"]   = $project_actual_contract_payment_iresult['data'];
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
    return $return;
}

/*
PURPOSE : To get Project Actual Contract Payment list
INPUT 	: Contract Payment ID, Contract ID, From Date, To Date, Vendor ID, Length, Breadth, Depth, Total Measurement, UOM, Rate, Status, Active, Accepted By, Accepted On, Approved By, Approved On, Added By Start Date(for Edited on), End Date(for Edited on)
OUTPUT 	: List of Project Actual Contract Payment
BY 		: Ashwini
*/
function i_get_project_actual_contract_payment($project_actual_contract_payment_search_data)
{
    $project_actual_contract_payment_sresult = db_get_project_actual_contract_payment($project_actual_contract_payment_search_data);

    if ($project_actual_contract_payment_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_actual_contract_payment_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Process Actual Contract Payment added. Please contact the system admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Actual Contract Payment
INPUT 	: Contract Payment ID, Project Actual Contract Payment Update Array
OUTPUT 	: Contract Payment ID; Message of success or failure
BY 		: Ashwini
*/
function i_update_project_actual_contract_payment($contract_payment_id, $project_actual_contract_payment_update_data)
{
    $project_actual_contract_payment_iresult = db_update_project_actual_contract_payment($contract_payment_id, $project_actual_contract_payment_update_data);

    if ($project_actual_contract_payment_iresult['status'] == SUCCESS) {
        $return["data"]   = "Actual Contract Payment Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
    return $return;
}

/*
PURPOSE : To Delete Project Actual Contract Payment
INPUT 	: Contract Payment ID,Project Actual Contract Payment Delete Array
OUTPUT 	: Message, success or failure message
BY 		: Ashwini
*/

function i_delete_project_actual_contract_payment($contract_payment_id, $project_actual_contract_payment_update_data)
{
    $project_actual_contract_payment_sresult = db_update_project_actual_contract_payment($contract_payment_id, $project_actual_contract_payment_update_data);

    if ($project_actual_contract_payment_sresult['status'] == SUCCESS) {
        $return["data"]   = "Actual Contract Payment Successfully Deleted";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add new Project Payment Machine Mapping
INPUT 	: Machine Actauls ID, Payment ID, Remarks, Added By
OUTPUT 	: Machine Mapping ID, success or failure message
BY 		: Lakshmi
*/
function i_add_project_payment_machine_mapping($machine_actuals_id, $payment_id, $remarks, $added_by)
{
    $project_payment_machine_mapping_iresult = db_add_project_payment_machine_mapping($machine_actuals_id, $payment_id, $remarks, $added_by);

    if ($project_payment_machine_mapping_iresult['status'] == SUCCESS) {
        $return["data"]   = "Payment Payment Machine Mapping Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
    return $return;
}

/*
PURPOSE : To get Project Payment Machine Mapping list
INPUT 	: Machine Mapping ID, Machine ID, Payment ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Payment Machine  Mapping
BY 		: Lakshmi
*/
function i_get_project_payment_machine_mapping($project_payment_machine_mapping_search_data)
{
    $project_payment_machine_mapping_sresult = db_get_project_payment_machine_mapping($project_payment_machine_mapping_search_data);

    if ($project_payment_machine_mapping_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_payment_machine_mapping_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Payment Machine Mapping added. Please contact the system admin";
    }

    return $return;
}

 /*
PURPOSE : To update Project Payment Machine Mapping
INPUT 	: Machine Mapping ID, Project Payment Machine Mapping Update Array
OUTPUT 	: Machine Mapping ID; Message of success or failure
BY 		: Lakshmi
*/
function i_update_project_payment_machine_mapping($machine_mapping_id, $project_payment_machine_mapping_update_data)
{
    $project_payment_machine_mapping_iresult = db_update_project_payment_machine_mapping($machine_mapping_id, $project_payment_machine_mapping_update_data);

    if ($project_payment_machine_mapping_iresult['status'] == SUCCESS) {
        $return["data"]   = "Payment Payment Machine Mapping Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
    return $return;
}

 /*
PURPOSE : To Delete Project Payment Machine Mapping
INPUT 	: Machine Mapping ID, Project Payment Machine Mapping Delete Array
OUTPUT 	: Machine Mapping ID; Message of success or failure
BY 		: Lakshmi
*/
function i_delete_project_payment_machine_mapping($machine_mapping_id, $project_payment_machine_mapping_update_data)
{
    $project_payment_machine_mapping_update_data = array('active'=>'0');
    $project_payment_machine_mapping_iresult = db_update_project_payment_machine_mapping($machine_mapping_id, $project_payment_machine_mapping_update_data);

    if ($project_payment_machine_mapping_iresult['status'] == SUCCESS) {
        $return["data"]   = "Payment Payment Machine Mapping Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add new Project Payment Machine
INPUT 	: Vendor ID, Amount, Status, Bill No, Billing Address, From Date, To Date, Remarks, Accepted By, Accepted On, Approved By, Approved On, Added By
OUTPUT 	: Payment Machine ID, success or failure message
BY 		: Lakshmi
*/
function i_add_project_payment_machine($vendor_id, $amount, $bill_no, $billing_address, $from_date, $to_date, $remarks, $accepted_by, $accepted_on, $approved_by, $approved_on, $added_by)
{
    $project_payment_machine_iresult = db_add_project_payment_machine($vendor_id, $amount, $bill_no, $billing_address, $from_date, $to_date, $remarks, $accepted_by, $accepted_on, $approved_by, $approved_on, $added_by);

    if ($project_payment_machine_iresult['status'] == SUCCESS) {
        $return["data"]   = $project_payment_machine_iresult['data'];
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
    return $return;
}

/*
PURPOSE : To get Project Payment Machine List
INPUT 	: Payment Machine ID, Vendor ID, Amount, Status, Bill No, Billing Address, From Date, To Date, Active, Accepted By, Accepted On, Approved By, Approved On, Added By
          Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Payment Machine
BY 		: Lakshmi
*/
function i_get_project_payment_machine($project_payment_machine_search_data)
{
    $project_payment_machine_sresult = db_get_project_payment_machine($project_payment_machine_search_data);

    if ($project_payment_machine_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_payment_machine_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Payment Machine added. Please contact the system admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Payment Machine
INPUT 	: Payment Machine ID, Project Payment Machine Update Array
OUTPUT 	: Payment Machine ID; Message of success or failure
BY 		: Lakshmi
*/
function i_update_project_payment_machine($payment_machine_id, $project_payment_machine_update_data)
{
    $project_payment_machine_iresult = db_update_project_payment_machine($payment_machine_id, $project_payment_machine_update_data);

    if ($project_payment_machine_iresult['status'] == SUCCESS) {
        $return["data"]   = "Payment Payment Machine Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
    return $return;
}

/*
PURPOSE : To Delete Project Payment Machine
INPUT 	: Payment Machine ID, Project Payment Machine Delete Array
OUTPUT 	: Payment Machine ID; Message of success or failure
BY 		: Lakshmi
*/
function i_delete_project_payment_machine($payment_machine_id, $project_payment_machine_update_data)
{
    $project_payment_machine_update_data = array('active'=>'0');
    $project_payment_machine_iresult = db_update_project_payment_machine($payment_machine_id, $project_payment_machine_update_data);

    if ($project_payment_machine_iresult['status'] == SUCCESS) {
        $return["data"]   = "Payment Payment Machine Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add new Project Machine Issue Payment
INPUT 	: Machine ID, Amount, Vendor ID, Payment Mode, Instrument Details, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_project_machine_issue_payment($machine_id, $amount, $vendor_id, $payment_mode, $instrument_details, $remarks, $added_by)
{
    $project_machine_issue_payment_iresult = db_add_project_machine_issue_payment($machine_id, $amount, $vendor_id, $payment_mode, $instrument_details, $remarks, $added_by);

    if ($project_machine_issue_payment_iresult['status'] == SUCCESS) {
        $return["data"]   = "Machine Issue Payment Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get project Machine Issue Payment list
INPUT 	: Payment ID, Machine ID, Amount, Vendor ID, Payment Mode, Instrument Details, Active, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Machine Issue Payment, success or failure message
BY 		: Lakshmi
*/
function i_get_project_machine_issue_payment($project_machine_issue_payment_search_data)
{
    $project_machine_issue_payment_sresult = db_get_project_machine_issue_payment($project_machine_issue_payment_search_data);

    if ($project_machine_issue_payment_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_machine_issue_payment_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Machine Issue Payment Added!";
    }

    return $return;
}

/*
PURPOSE : To update Project Machine Issue Payment
INPUT 	: Payment ID, Project Machine Issue Payment Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_project_machine_issue_payment($payment_id, $project_machine_issue_payment_update_data)
{
    $project_machine_issue_payment_sresult = db_update_project_machine_issue_payment($payment_id, $project_machine_issue_payment_update_data);

    if ($project_machine_issue_payment_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Machine Issue Payment Successfully Updated";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}
/*
PURPOSE : To Delete Project Machine Issue Payment
INPUT 	: Payment ID,Project Machine Issue Payment Delete Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_project_machine_issue_payment($payment_id, $project_machine_issue_payment_update_data)
{
    $project_machine_issue_payment_update_data = array('active'=>'0');
    $project_machine_issue_payment_sresult = db_update_project_machine_issue_payment($payment_id, $project_machine_issue_payment_update_data);

    if ($project_machine_issue_payment_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Machine Issue Payment Successfully Deleted";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Manpower planned total value
INPUT 	: Project , Active
OUTPUT 	: Total Manpower Planned cost
BY 		: Sonakshi
*/

function i_get_planned_manpower_value($project, $active)
{
    // Temp data
    $project_man_power_estimate_list = db_get_man_power_estimate($project, $active);
    if ($project_man_power_estimate_list["status"] == DB_RECORD_ALREADY_EXISTS) {
        $project_man_power_estimate_list_data = $project_man_power_estimate_list['data'];
        $man_power_men_rate = 0;
        $man_power_women_rate = 0;
        $man_power_mason_rate = 0;
        $project_man_power_rate_search_data = array("active"=>'1',"power_type_id"=>'1');
        $manpower_men_rate_list = i_get_project_man_power_max_rate_master($project_man_power_rate_search_data);
        if ($manpower_men_rate_list["status"] == SUCCESS) {
            $manpower_men_rate_list_data = $manpower_men_rate_list["data"];
            $man_power_men_rate = $manpower_men_rate_list_data[0]["max_rate"];
        }
        $project_man_power_rate_search_data = array("active"=>'1',"power_type_id"=>'2');
        $manpower_women_rate_list = i_get_project_man_power_max_rate_master($project_man_power_rate_search_data);
        if ($manpower_women_rate_list["status"] == SUCCESS) {
            $manpower_women_rate_list_data = $manpower_women_rate_list["data"];
            $man_power_women_rate = $manpower_women_rate_list_data[0]["max_rate"];
        }

        $project_man_power_rate_search_data = array("active"=>'1',"power_type_id"=>'3');
        $manpower_mason_rate_list = i_get_project_man_power_max_rate_master($project_man_power_rate_search_data);
        if ($manpower_mason_rate_list["status"] == SUCCESS) {
            $manpower_mason_rate_list_data = $manpower_mason_rate_list["data"];
            $man_power_mason_rate = $manpower_mason_rate_list_data[0]["max_rate"];
        }
        $total_men_hrs = 0;
        $total_women_hrs = 0;
        $total_mason_hrs = 0;

        for ($count = 0; $count < count($project_man_power_estimate_list_data); $count++) {
            $total_men_cost = $project_man_power_estimate_list_data[$count]["project_man_power_estimate_no_of_men"] * $man_power_men_rate;
            $total_women_cost = $project_man_power_estimate_list_data[$count]["project_man_power_estimate_no_of_women"] * $man_power_women_rate;
            $total_mason_cost = $project_man_power_estimate_list_data[$count]["project_man_power_estimate_no_of_mason"] * $man_power_mason_rate;
            $total_cost = $total_men_cost + $total_women_cost + $total_mason_cost;

            $men_hrs = $project_man_power_estimate_list_data[$count]["project_man_power_estimate_no_of_men"];
            $women_hrs = $project_man_power_estimate_list_data[$count]["project_man_power_estimate_no_of_women"];
            $mason_hrs = $project_man_power_estimate_list_data[$count]["project_man_power_estimate_no_of_mason"];

            $no_of_men = $men_hrs/8;
            $no_of_women = $women_hrs/8;
            $no_of_mason = $mason_hrs/8;

            //Total No of Man hours
            $total_men_hrs = $total_men_hrs  + $men_hrs ;
            $total_women_hrs = $total_women_hrs  + $women_hrs ;
            $total_mason_hrs = $total_mason_hrs  + $mason_hrs ;

            //Total no of Man
            $total_no_of_men = $total_men_hrs/8;
            $total_no_of_women = $total_women_hrs/8;
            $total_no_of_mason = $total_mason_hrs/8;

            $total_men_rate = $total_men_hrs * $man_power_men_rate;
            $total_women_rate = $total_women_hrs * $man_power_women_rate;
            $total_mason_rate = $total_mason_hrs * $man_power_mason_rate;

            $grand_total_cost = $total_men_rate + $total_women_rate + $total_mason_rate;
        }
    } else {
        $grand_total_cost = 0;
    }

    return $grand_total_cost;
}

/*
PURPOSE : To get Actual Manpower Total Value
INPUT 	: Project , Active
OUTPUT 	: Total Manpower Actual Cost
BY 		: Sonakshi
*/
function i_get_actual_manpower_value($project, $active)
{
    // Temp data
    $man_power_list = db_get_actual_manpower_list($project, $active);
    $grand_total_cost = 0;
    if ($man_power_list["status"] == DB_RECORD_ALREADY_EXISTS) {
        $man_power_list_data = $man_power_list["data"];
        $man_power_men_rate = 0;
        $man_power_women_rate = 0;
        $man_power_mason_rate = 0;
        for ($count = 0; $count < count($man_power_list_data); $count++) {
            $project_payment_manpower_mapping_search_data = array("manpower_id"=>$man_power_list_data[$count]["project_task_actual_manpower_id"]);
            $payment_manpower_mapping_list = i_get_project_payment_manpower_mapping($project_payment_manpower_mapping_search_data);
            if ($payment_manpower_mapping_list["status"] == SUCCESS) {
                $payment_manpower = true;
            } else {
                $payment_manpower = false;
            }

            //Get ManPower  Rate
            $project_man_power_rate_search_data = array("vendor_id"=>$man_power_list_data[$count]["project_task_actual_manpower_agency"]);
            $manpower_rate_list = i_get_project_man_power_rate_master($project_man_power_rate_search_data);
            if ($manpower_rate_list["status"] == SUCCESS) {
                for ($rate_count =0 ; $rate_count < count($manpower_rate_list["data"]) ; $rate_count++) {
                    $manpower_rate_list_data = $manpower_rate_list["data"];
                    if ($manpower_rate_list_data[$rate_count]["project_man_power_type_id"] == 1) {
                        $man_power_men_rate = $manpower_rate_list_data[$rate_count]["project_man_power_rate_cost_per_hours"];
                    }
                    if ($manpower_rate_list_data[$rate_count]["project_man_power_type_id"] == 2) {
                        $man_power_women_rate = $manpower_rate_list_data[$rate_count]["project_man_power_rate_cost_per_hours"];
                    }
                    if ($manpower_rate_list_data[$rate_count]["project_man_power_type_id"] == 3) {
                        $man_power_mason_rate = $manpower_rate_list_data[$rate_count]["project_man_power_rate_cost_per_hours"];
                    }
                }
            }

            $total_men_cost = $man_power_list_data[$count]["project_task_actual_manpower_no_of_men"] * $man_power_men_rate;
            $total_women_cost = $man_power_list_data[$count]["project_task_actual_manpower_no_of_women"] * $man_power_women_rate;
            $total_mason_cost = $man_power_list_data[$count]["project_task_actual_manpower_no_of_mason"] * $man_power_mason_rate;
            $total_cost = $total_men_cost + $total_women_cost + $total_mason_cost;
            $men_hrs = $man_power_list_data[$count]["project_task_actual_manpower_no_of_men"];
            $women_hrs = $man_power_list_data[$count]["project_task_actual_manpower_no_of_women"];
            $mason_hrs = $man_power_list_data[$count]["project_task_actual_manpower_no_of_mason"];

            $no_of_men = $men_hrs/8;
            $no_of_women = $women_hrs/8;
            $no_of_mason = $mason_hrs/8;

            $total_hrs = $men_hrs + $women_hrs + $mason_hrs;
            $no_of_people = $total_hrs/8;
            $grand_total_cost = $grand_total_cost + $total_cost;
        }
    } else {
        $grand_total_cost = 0;
    }

    return $grand_total_cost;
}

/*
PURPOSE : To get Planned Contract Total Value
INPUT 	: Project , Active
OUTPUT 	: Total Planned Contract Cost
BY 		: Sonakshi
*/
function i_get_planned_contract_value($project, $active)
{
    // Temp data
    $total_amount = 0;
    $project_task_boq_plan_list = db_get_project_task_boq_plan($project, $active);
    if ($project_task_boq_plan_list["status"] == DB_RECORD_ALREADY_EXISTS) {
        $project_task_boq_plan_list_data = $project_task_boq_plan_list["data"];
        for ($count = 0; $count < count($project_task_boq_plan_list_data); $count++) {
            $measurement = $project_task_boq_plan_list_data[$count]["project_task_boq_total_measurement"];
            $rate = $project_task_boq_plan_list_data[$count]["project_task_boq_amount"];
            $total = $measurement * $rate;
            $total_amount = $total_amount + $total;
        }
    } else {
        $total_amount = 0;
    }

    return $total_amount;
}

/*
PURPOSE : To get Actual Contract Total Value
INPUT 	: Project,Active
OUTPUT 	: Total Actual Contract Cost
BY 		: Sonakshi
*/
function i_get_actual_contract_value($project, $active)
{
    // Temp data
    $total_amount = 0;
    $project_task_boq_list = db_get_task_boq_actual($project, $active);
    if ($project_task_boq_list["status"] == DB_RECORD_ALREADY_EXISTS) {
        $project_task_boq_list_data = $project_task_boq_list["data"];
        for ($count = 0; $count < count($project_task_boq_list_data); $count++) {
            $sl_no++;
            $measurement = $project_task_boq_list_data[$count]["project_task_actual_boq_total_measurement"];
            $numbers = $project_task_boq_list_data[$count]["project_task_boq_actual_number"];
            $rate = $project_task_boq_list_data[$count]["project_task_actual_boq_amount"];
            $actual_total = $measurement * $rate;
            $total = $project_task_boq_list_data[$count]["project_task_actual_boq_lumpsum"];

            $total_amount = $total_amount + $total;
        }
    } else {
        $total_amount = 0;
    }

    return $total_amount;
}

/*
PURPOSE : To get Machine Plan Total Value
INPUT 	: Project , Active
OUTPUT 	: Total Planned Machine Cost
BY 		: Sonakshi
*/
function i_get_planned_machine_value($project, $active)
{
    // Temp data
    $total_cost_planned = 0;
    $project_machine_planning_list = db_get_task_boq_actual($project, $active);
    if ($project_machine_planning_list["status"] == DB_RECORD_ALREADY_EXISTS) {
        $sl_no = 0;
        $project_machine_planning_list_data = $project_machine_planning_list["data"];
        for ($count = 0; $count < count($project_machine_planning_list_data); $count++) {
            $total_cost_planned = $total_cost_planned + ($project_machine_planning_list_data[$count]["project_machine_planning_no_of_hours"] * $project_machine_planning_list_data[$count]["project_machine_planning_machine_rate"]) + $project_machine_planning_list_data[$count]["project_machine_planning_additional_cost"];
        }
    } else {
        $total_cost_planned = 0;
    }

    return $total_cost_planned;
}

/*
PURPOSE : To get Machine Actual Total Value
INPUT 	: Project , Active
OUTPUT 	: Total Actual Machine Cost
BY 		: Sonakshi
*/
function i_get_actual_machine_value($project, $active)
{
    // Temp data
    $total_cost = 0;
    $actual_machine_list = db_get_actual_machine_list($project, $active);
    if ($actual_machine_list["status"] == DB_RECORD_ALREADY_EXISTS) {
        $sl_no = 0;
        $actual_machine_list_data = $actual_machine_list["data"];
        for ($count = 0; $count < count($actual_machine_list_data); $count++) {
            //if($actual_machine_list_data[$count]["project_task_actual_machine_display_status"] == "not approved")
            //{
            $sl_no++;

            $machine_rate = $actual_machine_list_data[$count]['project_task_actual_machine_fuel_charges'];

            //get stock quantity
            $project_machine_rate_master_search_data = array("machine_id"=>$actual_machine_list_data[$count]["project_task_machine_id"]);
            $project_machine_rate_master_data =  i_get_project_machine_rate_master($project_machine_rate_master_search_data);
            if ($project_machine_rate_master_data["status"] == SUCCESS) {
                $vendor_machine_number = $project_machine_rate_master_data["data"][0]["project_machine_master_id_number"];
                $machine_type = $project_machine_rate_master_data["data"][0]["project_machine_type"];
            } else {
                $vendor_machine_number = 'NA';
                $machine_type = 'NA';
            }

            //get stock quantity
            $machine_vendor = $actual_machine_list_data[$count]['project_machine_vendor_master_name'];

            //No of hrs worked
            if ($actual_machine_list_data[$count]["project_task_actual_machine_plan_end_date_time"] != "0000-00-00 00:00:00") {
                $start_date_time = strtotime($actual_machine_list_data[$count]["project_task_actual_machine_plan_start_date_time"]);
                $end_date_time = strtotime($actual_machine_list_data[$count]["project_task_actual_machine_plan_end_date_time"]);
                $date_diff = $end_date_time - $start_date_time;
                $no_hrs_worked = $date_diff/3600;

                $end_date_display = date('d-M-Y H:i:s', strtotime($actual_machine_list_data[$count]["project_task_actual_machine_plan_end_date_time"]));
            } else {
                $start_date_time = strtotime($actual_machine_list_data[$count]["project_task_actual_machine_plan_start_date_time"]);
                $end_date_time = strtotime(date("Y-m-d H:i:s"));
                $date_diff = $end_date_time - $start_date_time;
                $no_hrs_worked = $date_diff/3600;

                $end_date_display = '';
            }

            // Off time related calculation
            $off_time = $actual_machine_list_data[$count]["project_task_actual_machine_plan_off_time"]/60;
            $eff_hrs = $no_hrs_worked - $off_time;

            $cost = ($machine_rate * $eff_hrs) + $actual_machine_list_data[$count]["project_task_actual_machine_bata"] + $actual_machine_list_data[$count]["project_task_actual_machine_plan_additional_cost"] - $actual_machine_list_data[$count]["project_task_actual_machine_issued_fuel"];
            $total_cost = $total_cost + $cost ;
            //}
        }
    } else {
        $total_cost = 0;
    }

    return $total_cost;
}
/* PRIVATE FUNCTIONS - START */
function p_generate_manpower_bill_no($bill_substr = '')
{
    // Get the last bill no
    $project_actual_payment_manpower_search_data = array("sort"=>'1',"empty_check"=>'1',"active"=>'1',"start"=>'-1');
    $bill_no_data = db_get_project_actual_payment_manpower($project_actual_payment_manpower_search_data);
    if ($bill_no_data["status"] == DB_RECORD_ALREADY_EXISTS) {
        $invoice_numeric_value_data = $bill_no_data["data"][0]["project_actual_payment_manpower_bill_no"];
        $invoice_numeric_value_array = explode(BILL_NO_DELIMITER, $invoice_numeric_value_data);
        $invoice_numeric_value = ($invoice_numeric_value_array[count($invoice_numeric_value_array)-1]) + 1;
    } else {
        $invoice_numeric_value = 1;
    }
    // KNS/NMR/2018-2019/06/1215
    if(isset($bill_substr) && !empty($bill_substr)){
      return BILL_NO_ROOT.BILL_NO_DELIMITER.$bill_substr.BILL_NO_DELIMITER.BILL_NO_MANPOWER.BILL_NO_DELIMITER.BILL_NO_YEAR.BILL_NO_DELIMITER1.BILL_NO_NEXT_YEAR.BILL_NO_DELIMITER.BILL_NO_MONTH.BILL_NO_DELIMITER.$invoice_numeric_value;
    }
    return BILL_NO_ROOT.BILL_NO_DELIMITER.BILL_NO_MANPOWER.BILL_NO_DELIMITER.BILL_NO_YEAR.BILL_NO_DELIMITER1.BILL_NO_NEXT_YEAR.BILL_NO_DELIMITER.BILL_NO_MONTH.BILL_NO_DELIMITER.$invoice_numeric_value;
}
/* PRIVATE FUNCTIONS - END */

/* PRIVATE FUNCTIONS - START */
function p_generate_contract_bill_no($bill_substr = '')
{
    // Get the last bill no
    $project_actual_contract_payment_search_data = array("sort"=>'1',"empty_check"=>'1',"active"=>'1',"start"=>'-1');
    $contract_bill_no_data = db_get_project_actual_contract_payment($project_actual_contract_payment_search_data);
    if ($contract_bill_no_data["status"] == DB_RECORD_ALREADY_EXISTS) {
        $contract_numeric_value_data = $contract_bill_no_data["data"][0]["project_actual_contract_payment_bill_no"];
        $contract_numeric_value_array = explode(BILL_NO_DELIMITER, $contract_numeric_value_data);
        $contract_numeric_value = end($contract_numeric_value_array) + 1;
    } else {
        $contract_numeric_value = 1;
    }
    if(isset($bill_substr) && !empty($bill_substr)){
        return BILL_NO_ROOT.BILL_NO_DELIMITER.$bill_substr.BILL_NO_DELIMITER.BILL_NO_CONTRACT.BILL_NO_DELIMITER.BILL_NO_YEAR.BILL_NO_DELIMITER1.BILL_NO_NEXT_YEAR.BILL_NO_DELIMITER.BILL_NO_MONTH.BILL_NO_DELIMITER.$contract_numeric_value;
       }
    return BILL_NO_ROOT.BILL_NO_DELIMITER.BILL_NO_CONTRACT.BILL_NO_DELIMITER.BILL_NO_YEAR.BILL_NO_DELIMITER1.BILL_NO_NEXT_YEAR.BILL_NO_DELIMITER.BILL_NO_MONTH.BILL_NO_DELIMITER.$contract_numeric_value;
}
/* PRIVATE FUNCTIONS - END */

/* PRIVATE FUNCTIONS - START */
function p_generate_machine_bill_no($bill_substr = '')
{
    // Get the last bill no
    $project_payment_machine_search_data = array("sort"=>'1',"empty_check"=>'1',"active"=>'1',"start"=>'-1');
    $machine_bill_no_data = db_get_project_payment_machine($project_payment_machine_search_data);
    if ($machine_bill_no_data["status"] == DB_RECORD_ALREADY_EXISTS) {
        $machine_numeric_value_data = $machine_bill_no_data["data"][0]["project_payment_machine_bill_no"];
        $machine_numeric_value_array = explode(BILL_NO_DELIMITER, $machine_numeric_value_data);
        $machine_numeric_value = end($machine_numeric_value_array) + 1;
    } else {
        $machine_numeric_value = 1;
    }
    if(isset($bill_substr) && !empty($bill_substr)){
        return BILL_NO_ROOT.BILL_NO_DELIMITER.$bill_substr.BILL_NO_DELIMITER.BILL_NO_MACHINE.BILL_NO_DELIMITER.BILL_NO_YEAR.BILL_NO_DELIMITER1.BILL_NO_NEXT_YEAR.BILL_NO_DELIMITER.BILL_NO_MONTH.BILL_NO_DELIMITER.$machine_numeric_value;
       }
    return BILL_NO_ROOT.BILL_NO_DELIMITER.BILL_NO_MACHINE.BILL_NO_DELIMITER.BILL_NO_YEAR.BILL_NO_DELIMITER1.BILL_NO_NEXT_YEAR.BILL_NO_DELIMITER.BILL_NO_MONTH.BILL_NO_DELIMITER.$machine_numeric_value;
}

/*
PURPOSE : To add new Project Budget
INPUT 	: Value, project ID, Remarks, Added By
OUTPUT 	: Budget ID, success or failure message
BY 		: Lakshmi
*/
function i_add_project_budget($value, $project_id, $remarks, $added_by)
{
    $project_budget_iresult =  db_add_project_budget($value, $project_id, $remarks, $added_by);

    if ($project_budget_iresult['status'] == SUCCESS) {
        $return["data"]   = "Project Budget Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Project Budget List
INPUT 	: Budget ID, Value, Project ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Budget
BY 		: Lakshmi
*/
function i_get_project_budget($project_budget_search_data)
{
    $project_budget_sresult = db_get_project_budget($project_budget_search_data);

    if ($project_budget_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_budget_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Budget Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Budget
INPUT 	: Budget ID, Project Budget Update Array
OUTPUT 	: Budget ID; Message of success or failure
BY 		: Lakshmi
*/
function i_update_project_budget($budget_id, $project_budget_update_data)
{
    $project_budget_sresult = db_update_project_budget($budget_id, $project_budget_update_data);

    if ($project_budget_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Budget Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To Delete Project Budget
INPUT 	: Budget ID, Project Budget Update Array
OUTPUT 	: Budget ID; Message of success or failure
BY 		: Lakshmi
*/
function i_delete_project_budget($budget_id, $project_budget_update_data)
{
    $project_budget_update_data = array('active'=>'0');
    $project_budget_sresult = db_update_project_budget($budget_id, $project_budget_update_data);

    if ($project_budget_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Budget Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Material Planned Total Value
INPUT 	: Project , Active
OUTPUT 	: Total Planned Material Cost
BY 		: Sonakshi
*/
function i_get_planned_material_value($project, $active)
{
    $project_task_required_item_list = db_get_project_task_material_list($project, $active);
    if ($project_task_required_item_list["status"] == DB_RECORD_ALREADY_EXISTS) {
        $total = 0;
        $project_task_required_item_list_data = $project_task_required_item_list["data"];
        for ($count = 0; $count < count($project_task_required_item_list_data); $count++) {
            $qty = $project_task_required_item_list_data[$count]["project_task_required_item_quantity"];
            $rate = $project_task_required_item_list_data[$count]["project_task_required_item_rate"];
            $total_value = $qty*$rate;
            $total = $total + $total_value;
        }
    } else {
        $total = 0;
    }

    return $total;
}

/*
PURPOSE : To get Material Actual Total Value
INPUT 	: Project , Active
OUTPUT 	: Total Actual Material Cost
BY 		: Sonakshi
*/
function i_get_actual_material_value($project, $active)
{
    $project_stock_module_mapping_search_data = array("mgmnt_project_id"=>$project);
    $project_stock_mapping_list = i_get_project_stock_module_mapping($project_stock_module_mapping_search_data);
    if ($project_stock_mapping_list["status"] == SUCCESS) {
        $stock_project = $project_stock_mapping_list["data"][0]["project_stock_module_mapping_stock_project_id"];
    } else {
        $stock_project = "";
    }
    $project_task_material_issued_list = db_get_stock_material_issued_list($stock_project, $active);
    if ($project_task_material_issued_list["status"] == DB_RECORD_ALREADY_EXISTS) {
        $total = 0;
        $project_task_material_issued_list_data = $project_task_material_issued_list["data"];
        for ($count = 0; $count < count($project_task_material_issued_list_data); $count++) {
            $material = $project_task_material_issued_list_data[$count]["stock_issue_item_material_id"];
            $item_qty = $project_task_material_issued_list_data[$count]["stock_issue_item_qty"];
            //Get Material List
            $material_stock_search_data = array("material_id"=>$material);
            $material_stock_list = i_get_material_stock($material_stock_search_data);
            if ($material_stock_list['status'] == SUCCESS) {
                $material_stock_list_data = $material_stock_list['data'];
                $material_rate  = $material_stock_list_data[0]["stock_material_price"];
                $total_value = $item_qty*$material_rate;
            } else {
                $total_value = 0;
            }

            $total = $total + $total_value;
        }
    } else {
        $total = 0;
    }

    return $total;
}

/*
PURPOSE : To add new Project Stock Module Mapping
INPUT 	: Mgmnt Project ID, Stock Project ID, Added By
OUTPUT 	: Mapping ID, success or failure message
BY 		: Lakshmi
*/
function i_add_project_stock_module_mapping($mgmnt_project_id, $stock_project_id, $added_by)
{
    $project_stock_module_mapping_search_data = array("mgmnt_project_id"=>$mgmnt_project_id,"stock_project_id"=>$stock_project_id,"active"=>'1');
    $project_stock_module_mapping_sresult = db_get_project_stock_module_mapping($project_stock_module_mapping_search_data);

    if ($project_stock_module_mapping_sresult["status"] == DB_NO_RECORD) {
        $project_stock_module_mapping_iresult = db_add_project_stock_module_mapping($mgmnt_project_id, $stock_project_id, $added_by);

        if ($project_stock_module_mapping_iresult['status'] == SUCCESS) {
            $return["data"]   = "Project Stock Module Mapping Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "Mapping project Name already exists!";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Project Stock Module Mapping list
INPUT 	: Mapping ID, Mgmnt Project ID, Stock Project ID, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Stock Module Mapping
BY 		: Lakshmi
*/
function i_get_project_stock_module_mapping($project_stock_module_mapping_search_data)
{
    $project_stock_module_mapping_sresult = db_get_project_stock_module_mapping($project_stock_module_mapping_search_data);

    if ($project_stock_module_mapping_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_stock_module_mapping_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project stock Module Mapping Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To add new Project Finance Dashboard
INPUT 	: Project ID, Material Planned Value, Material Actual Value, Material Variance Value, Machine Planned Value, Manpower Actual Value, Manpower Variance Value,
          Machine Planned Value, Machine Actual Value, Machine Variance Value, Contract Planned Value, Contract Actual Value, Contract Variance Value, Remarks, Added By
OUTPUT 	: Dashboard ID, success or failure message
BY 		: Lakshmi
*/
function i_add_project_finance_dashboard($project_id, $material_planned_value, $material_actual_value, $material_variance_value, $manpower_planned_value, $manpower_actual_value, $manpower_variance_value, $machine_planned_value, $machine_actual_value, $machine_variance_value, $contract_planned_value, $contract_actual_value, $contract_variance_value, $remarks, $added_by)
{
    $project_finance_dashboard_iresult =  db_add_project_finance_dashboard($project_id, $material_planned_value, $material_actual_value, $material_variance_value, $manpower_planned_value, $manpower_actual_value, $manpower_variance_value, $machine_planned_value, $machine_actual_value, $machine_variance_value, $contract_planned_value, $contract_actual_value, $contract_variance_value, $remarks, $added_by);

    if ($project_finance_dashboard_iresult['status'] == SUCCESS) {
        $return["data"]   = "Project Finance Dashboard Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Project Finance Dashboard List
INPUT 	: Dashboard ID, Project ID, Manpower Planned Value, Manpower Actual Value, Manpower Variance Value, Machine Planned Value, Machine Actual Value, Machine Variance Value,
          Contract Planned Value, Contract Actual Value, Contract Variance Value, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Finance Dashboard
BY 		: Lakshmi
*/
function i_get_project_finance_dashboard($project_finance_dashboard_search_data)
{
    $project_finance_dashboard_sresult = db_get_project_finance_dashboard($project_finance_dashboard_search_data);

    if ($project_finance_dashboard_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_finance_dashboard_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Finance Dashboard Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Finance Dashboard
INPUT 	: Dashboard ID, Project Finance Dashboard Update Array
OUTPUT 	: Dashboard ID; Message of success or failure
BY 		: Lakshmi
*/
function i_update_project_finance_dashboard($dashboard_id, $project_finance_dashboard_update_data)
{
    $project_finance_dashboard_sresult = db_update_project_finance_dashboard($dashboard_id, $project_finance_dashboard_update_data);

    if ($project_finance_dashboard_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Finance Dashboard Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To Delete Project Finance Dashboard
INPUT 	: Dashboard ID, Project Finance Dashboard Update Array
OUTPUT 	: Dashboard ID; Message of success or failure
BY 		: Lakshmi
*/
function i_delete_project_finance_dashboard($dashboard_id, $project_finance_dashboard_update_data)
{
    $project_finance_dashboard_update_data = array('active'=>'0');
    $project_finance_dashboard_sresult = db_update_project_finance_dashboard($dashboard_id, $project_finance_dashboard_update_data);

    if ($project_finance_dashboard_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Finance Dashboard Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}
/*
PURPOSE : To add new Project Plan Cancelation
INPUT 	: Task ID, Plan Type, Active, Remarks, Added By
OUTPUT 	: Cancelation ID, success or failure message
BY 		: Lakshmi
*/
function i_add_project_plan_cancelation($task_id, $plan_type, $remarks, $added_by)
{
    $project_plan_cancelation_iresult =  db_add_project_plan_cancelation($task_id, $plan_type, $remarks, $added_by);

    if ($project_plan_cancelation_iresult['status'] == SUCCESS) {
        $return["data"]   = "Project Plan Cancelation Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Project Plan Cancelation List
INPUT 	: Cancelation ID, Task ID, Plan type, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Plan Cancelation
BY 		: Lakshmi
*/
function i_get_project_plan_cancelation($project_plan_cancelation_search_data)
{
    $project_plan_cancelation_sresult = db_get_project_plan_cancelation($project_plan_cancelation_search_data);

    if ($project_plan_cancelation_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_plan_cancelation_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Plan Cancelation Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Project Plan Cancelation
INPUT 	: Cancelation ID, Project Plan Cancelation Update Array
OUTPUT 	: Cancelation ID; Message of success or failure
BY 		: Lakshmi
*/
function i_update_project_plan_cancelation($cancelation_id, $project_plan_cancelation_update_data)
{
    $project_plan_cancelation_sresult = db_update_project_plan_cancelation($cancelation_id, $project_plan_cancelation_update_data);

    if ($project_plan_cancelation_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Plan Cancelation Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To Delete Project Plan Cancelation
INPUT 	: Cancelation ID, Project Plan Cancelation Update Array
OUTPUT 	: Cancelation ID; Message of success or failure
BY 		: Lakshmi
*/
function i_delete_project_plan_cancelation($cancelation_id, $project_plan_cancelation_update_data)
{
    $project_plan_cancelation_update_data = array('active'=>'0');
    $project_plan_cancelation_sresult = db_update_project_plan_cancelation($cancelation_id, $project_plan_cancelation_update_data);

    if ($project_plan_cancelation_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Plan Cancelation Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Project Task BOQ Actual  Sum List
INPUT 	: BOQ  Actual ID, UOM, Number, Amount, Completion, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Task BOQ Actual
BY 		: Lakshmi
*/
function i_get_task_boq_sum($project_task_actual_boq_search_data)
{
    $project_task_boq_actual_sresult = db_get_actual_boq_sum($project_task_actual_boq_search_data);

    if ($project_task_boq_actual_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   =$project_task_boq_actual_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Task BOQ Actual Added. Please Contact The System Admin";
    }

    return $return;
}
/*
PURPOSE : To add new Project Manpower Rework
INPUT 	: Task ID, Date, Men, Women, Completion Percent, Checked By, Checked On, Approved By, Approved On, Remarks, Added By
OUTPUT 	: Rework ID, success or failure message
BY 		: Lakshmi
*/
function i_add_project_manpower_rework($task_id, $date, $men, $women, $mason, $vendor_id, $completion_percent, $checked_by, $checked_on, $approved_by, $approved_on, $remarks, $added_by)
{
    $project_manpower_rework_search_data = array("date"=>$date,"task_id"=>$task_id);
    $project_manpower_rework_sresult = db_get_project_manpower_rework($project_manpower_rework_search_data);
    if ($project_manpower_rework_sresult['status'] == DB_NO_RECORD) {
        $project_manpower_rework_iresult = db_add_project_manpower_rework($task_id, $date, $men, $women, $mason, $vendor_id, $completion_percent, $checked_by, $checked_on, $approved_by, $approved_on, $remarks, $added_by);
        if ($project_manpower_rework_iresult['status'] == SUCCESS) {
            $return["data"]   = $project_manpower_rework_iresult['data'];
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
        return $return;
    }
}

/*
PURPOSE : To get Project Manpower Rework list
INPUT 	: Rework ID, Task ID, Date, Men, Women, Mason, Completion Percent, Checked By, Checked On, Approved By, Approved On, Remarks, Added By, Start Date(for added on),
          End Date(for added on)
OUTPUT 	: List of Project Manpower Rework
BY 		: Lakshmi
*/
function i_get_project_manpower_rework($project_manpower_rework_search_data)
{
    $project_manpower_rework_sresult = db_get_project_manpower_rework($project_manpower_rework_search_data);
    if ($project_manpower_rework_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_manpower_rework_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Actaul ManPower Rework added yet!";
    }

    return $return;
}

/*
PURPOSE : To update Project Manpower Rework
INPUT 	: Rework ID, Project Manpower Rework Update Array
OUTPUT 	: Rework ID; Message of success or failure
BY 		: Lakshmi
*/
function i_update_project_manpower_rework($rework_id, $project_manpower_rework_update_data)
{
    $project_manpower_rework_iresult = db_update_project_manpower_rework($rework_id, $project_manpower_rework_update_data);

    if ($project_manpower_rework_iresult['status'] == SUCCESS) {
        $return["data"]   = "man_power Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
}

/*
PURPOSE : To Delete Project Manpower Rework
INPUT 	: Rework ID, Project Manpower Rework Update Array
OUTPUT 	: Rework ID; Message of success or failure
BY 		: Lakshmi
*/
function i_delete_project_manpower_rework($rework_id, $project_manpower_rework_update_data)
{
    $project_manpower_rework_update_data = array('active'=>'0');
    $project_manpower_rework_sresult = db_update_project_manpower_rework($rework_id, $project_manpower_rework_update_data);

    if ($project_manpower_rework_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Manpower Rework Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}
/*
PURPOSE : To add new Project Contract Rework
INPUT 	: Task ID, Vendor ID, Date, Contract process, Contract Task, Uom, Location, Number, Length, Breadth, Depth, Total Measurment, Amount, Lumpsum, Completion, Status, Remarks,
          Checked By, Checked On, Approved By, Approved On, Added By
OUTPUT 	: Rework ID, success or failure message
BY 		: Lakshmi
*/
function i_add_project_contract_rework($task_id, $vendor_id, $date, $contract_process, $contract_task, $uom, $location, $number, $length, $breadth, $depth, $total_measurment, $amount, $lumpsum, $completion, $remarks, $checked_by, $checked_on, $approved_by, $approved_on, $added_by)
{
    $project_contract_rework_iresult = db_add_project_contract_rework($task_id, $vendor_id, $date, $contract_process, $contract_task, $uom, $location, $number, $length, $breadth, $depth, $total_measurment, $amount, $lumpsum, $completion, $remarks, $checked_by, $checked_on, $approved_by, $approved_on, $added_by);
    if ($project_contract_rework_iresult['status'] == SUCCESS) {
        $return["data"]   = $project_contract_rework_iresult['data'];
        $return["status"] = SUCCESS;
    }
    return $return;
}

/*
PURPOSE : To get Project Contract Rework list
INPUT 	: Rework ID, Task ID, Vendor ID, Date, Contract process, Contract Task, Uom, Location, Number, Length, Breadth, Depth, Total Measurment, Amount, Lumpsum, Completion,
          Status, Remarks, Checked By, Checked On, Approved By, Approved On, Added By Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Contract Rework
BY 		: Lakshmi
*/
function i_get_project_contract_rework($project_contract_rework_search_data)
{
    $project_contract_rework_sresult = db_get_project_contract_rework($project_contract_rework_search_data);
    if ($project_contract_rework_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_contract_rework_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Contract Rework added yet!";
    }

    return $return;
}

/*
PURPOSE : To update Project Contract Rework
INPUT 	: Rework ID, Project Contract Rework Update Array
OUTPUT 	: Rework ID; Message of success or failure
BY 		: Lakshmi
*/
function i_update_project_contract_rework($rework_id, $project_contract_rework_update_data)
{
    $project_contract_rework_iresult = db_update_project_contract_rework($rework_id, $project_contract_rework_update_data);

    if ($project_contract_rework_iresult['status'] == SUCCESS) {
        $return["data"]   = "Contract Rework Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
}

/*
PURPOSE : To Delete Project Contract Rework
INPUT 	: Rework ID, Project Contract Rework Update Array
OUTPUT 	: Rework ID; Message of success or failure
BY 		: Lakshmi
*/
function i_delete_project_contract_rework($rework_id, $project_contract_rework_update_data)
{
    $project_contract_rework_update_data = array('active'=>'0');
    $project_contract_rework_sresult = db_update_project_contract_rework($rework_id, $project_contract_rework_update_data);

    if ($project_contract_rework_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Contract Rework Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}
/*
PURPOSE : To add new Project Machine Rework
INPUT 	: Task ID, Vendor ID, Date, Contract process, Contract Task, Uom, Location, Number, Length, Breadth, Depth, Total Measurment, Amount, Lumpsum, Completion, Status, Remarks,
          Checked By, Checked On, Approved By, Approved On, Added By
OUTPUT 	: Rework ID, success or failure message
BY 		: Lakshmi
*/
function i_add_project_machine_rework($task_id, $vendor_id, $machine_id, $start_date_time, $end_date_time, $plan_off_time, $additional_cost, $number, $fuel_charges, $with_fuel_charges, $bata, $issued_fuel, $display_status, $completion, $machine_type, $check_status, $remarks, $checked_by, $checked_on, $approved_by, $approved_on, $added_by)
{
    $project_machine_rework_search_data = array("date"=>$start_date_time,"vendor_id"=>$vendor_id,"task_id"=>$task_id);
    $project_machine_rework_sresult = db_get_project_machine_rework($project_machine_rework_search_data);
    if ($project_machine_rework_sresult['status'] == DB_NO_RECORD) {
        $project_machine_rework_iresult = db_add_project_machine_rework($task_id, $vendor_id, $machine_id, $start_date_time, $end_date_time, $plan_off_time, $additional_cost, $number, $fuel_charges, $with_fuel_charges, $bata, $issued_fuel, $display_status, $completion, $machine_type, $check_status, $remarks, $checked_by, $checked_on, $approved_by, $approved_on, $added_by);
        if ($project_machine_rework_iresult['status'] == SUCCESS) {
            $return["data"]   = $project_machine_rework_iresult['data'];
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
        return $return;
    }
}

/*
PURPOSE : To get Project Machine Rework list
INPUT 	: Rework ID, Task ID, Vendor ID, Date, Contract process, Contract Task, Uom, Location, Number, Length, Breadth, Depth, Total Measurment, Amount, Lumpsum, Completion,
          Status, Remarks, Checked By, Checked On, Approved By, Approved On, Added By Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Machine Rework
BY 		: Lakshmi
*/
function i_get_project_machine_rework($project_machine_rework_search_data)
{
    $project_machine_rework_sresult = db_get_project_machine_rework($project_machine_rework_search_data);
    if ($project_machine_rework_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_machine_rework_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Machine Rework added yet!";
    }

    return $return;
}

/*
PURPOSE : To update Project Machine Rework
INPUT 	: Rework ID, Project Machine Rework Update Array
OUTPUT 	: Rework ID; Message of success or failure
BY 		: Lakshmi
*/
function i_update_project_machine_rework($rework_id, $project_machine_rework_update_data)
{
    $project_machine_rework_iresult = db_update_project_machine_rework($rework_id, $project_machine_rework_update_data);

    if ($project_machine_rework_iresult['status'] == SUCCESS) {
        $return["data"]   = "Machine Rework Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
}

/*
PURPOSE : To Delete Project Machine Rework
INPUT 	: Rework ID, Project Machine Rework Update Array
OUTPUT 	: Rework ID; Message of success or failure
BY 		: Lakshmi
*/
function i_delete_project_machine_rework($rework_id, $project_machine_rework_update_data)
{
    $project_machine_rework_update_data = array('active'=>'0');
    $project_machine_rework_sresult = db_update_project_machine_rework($rework_id, $project_machine_rework_update_data);

    if ($project_machine_rework_sresult['status'] == SUCCESS) {
        $return["data"]   = "Project Machine Rework Successfully Added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}
function i_update_project_manpower()
{
    //get vendor master
    $project_manpower_agency_search_data = array();
    $manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
    if ($manpower_agency_list["status"] == SUCCESS) {
        for ($vendor_count = 0 ; $vendor_count < count($manpower_agency_list["data"]) ; $vendor_count++) {
            $man_power_men_rate = 0;
            $man_power_women_rate = 0;
            $man_power_mason_rate = 0;
            //Get Rate Master
            $project_man_power_rate_search_data = array("vendor_id"=>$manpower_agency_list["data"][$vendor_count]["project_manpower_agency_id"]);
            $manpower_rate_list = i_get_project_man_power_rate_master($project_man_power_rate_search_data);
            if ($manpower_rate_list["status"] == SUCCESS) {
                for ($rate_count =0 ; $rate_count < count($manpower_rate_list["data"]) ; $rate_count++) {
                    $manpower_rate_list_data = $manpower_rate_list["data"];
                    if ($manpower_rate_list_data[$rate_count]["project_man_power_type_id"] == 1) {
                        $man_power_men_rate = $manpower_rate_list_data[$rate_count]["project_man_power_rate_cost_per_hours"];
                    }
                    if ($manpower_rate_list_data[$rate_count]["project_man_power_type_id"] == 2) {
                        $man_power_women_rate = $manpower_rate_list_data[$rate_count]["project_man_power_rate_cost_per_hours"];
                    }
                    if ($manpower_rate_list_data[$rate_count]["project_man_power_type_id"] == 3) {
                        $man_power_mason_rate = $manpower_rate_list_data[$rate_count]["project_man_power_rate_cost_per_hours"];
                    }
                }
                $man_power_update_data = array("men_rate"=>$man_power_men_rate,"women_rate"=>$man_power_women_rate,"mason_rate"=>$man_power_mason_rate);
                $update_manpower_list = db_update_man_power_rate($manpower_agency_list["data"][$vendor_count]["project_manpower_agency_id"], $man_power_update_data);
            }
        }
    }
}
/*
PURPOSE : To add new Project Task Planning
INPUT 	: Task ID, Measurment, No of Roads, No of Object, Total Days, Object Type, Parallel Road Work, Planning Start Date, Planning End Date, Remarks, Added By
OUTPUT 	: Planning ID, success or failure message
BY 		: Lakshmi
*/
function i_add_project_task_planning($project_id,$task_id,$measurment,$no_of_roads,$no_of_object,$total_days,$object_type,$machine_type,$uom,$rate,$per_day_out,$planning_start_date,$planning_end_date,$remarks,$added_by)
{
	$project_task_planning_search_data = array("task_id"=>$task_id,"no_of_roads"=>$no_of_roads);
	$project_task_planning_sresult = db_get_project_task_planning($project_task_planning_search_data);
	if($project_task_planning_sresult['status'] == DB_NO_RECORD)
    {
		$project_task_planning_iresult = db_add_project_task_planning($project_id,$task_id,$measurment,$no_of_roads,$no_of_object,$total_days,$object_type,$machine_type,$uom,$rate,$per_day_out,$planning_start_date,$planning_end_date,$remarks,$added_by);
		if($project_task_planning_iresult['status'] == SUCCESS)
		{
			$return["data"]   = $project_task_planning_iresult['data'];
			$return["status"] = SUCCESS;
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "This road is already exists for this task";
		$return["status"] = FAILURE;
	}
	return $return;

}

/*
PURPOSE : To get Project Task Planning list
INPUT 	: Planning ID, Task ID, Measurment, No of Roads, No of Object, Total Days, Object Type, Parallel Road Work, Planning Start Date, Planning End Date, Remarks, Added By,
          Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Task Planning
BY 		: Lakshmi
*/
function i_get_project_task_planning($project_task_planning_search_data)
{
    $project_task_planning_sresult = db_get_project_task_planning($project_task_planning_search_data);
    if ($project_task_planning_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_task_planning_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Task Planning added yet!";
    }

    return $return;
}

 /*
PURPOSE : To update Project Task Planning
INPUT 	: Planning ID, Project Task Planning Update Array
OUTPUT 	: Planning ID; Message of success or failure
BY 		: Lakshmi
*/
function i_update_project_task_planning($planning_id, $project_id, $project_task_planning_update_data)
{
    $project_task_planning_iresult = db_update_project_task_planning($planning_id, $project_id, $project_task_planning_update_data);

    if ($project_task_planning_iresult['status'] == SUCCESS) {
        $return["data"]   = $project_task_planning_iresult['data'];
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
	return $return ;
}

/*
PURPOSE : To add new Project Task Planning Shadow
INPUT 	: Task ID, Measurment, No of Roads, No of Object, Total Days, Object Type, Parallel Road Work, Planning Start Date, Planning End Date, Remarks, Added By
OUTPUT 	: Planning ID, success or failure message
BY 		: Lakshmi
*/
function i_add_project_task_planning_shadow($task_id,$measurment,$no_of_roads,$no_of_object,$total_days,$object_type,$machine_type,$uom,$rate,$planning_start_date,$planning_shadow_end_date,$remarks,$added_by)
{
	$project_task_planning_shadow_iresult = db_add_project_task_planning_shadow($task_id,$measurment,$no_of_roads,$no_of_object,$total_days,$object_type,$machine_type,$uom,$rate,$planning_start_date,$planning_shadow_end_date,$remarks,$added_by);
	if($project_task_planning_shadow_iresult['status'] == SUCCESS)
	{
		$return["data"]   = $project_task_planning_shadow_iresult['data'];
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	return $return;

}
/*
PURPOSE : To get Project Task Planning list
INPUT 	: Planning ID, Task ID, Measurment, No of Roads, No of Object, Total Days, Object Type, Parallel Road Work, Planning Start Date, Planning End Date, Remarks, Added By,
          Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Task Planning
BY 		: Lakshmi
*/
function i_get_project_task_planning_shadow($project_task_planning_shadow_search_data)
{
    $project_task_planning_shadow_sresult =db_get_project_task_planning_shadow($project_task_planning_shadow_search_data);
    if ($project_task_planning_shadow_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_task_planning_shadow_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Task Planning added yet!";
    }

    return $return;
}

 /*
PURPOSE : To update Project Task Planning
INPUT 	: Planning ID, Project Task Planning Update Array
OUTPUT 	: Planning ID; Message of success or failure
BY 		: Lakshmi
*/
function i_update_project_task_planning_shadow($planning_shadow_id, $project_task_planning_shadow_update_data)
{
    $project_task_planning_shadow_iresult = db_update_project_task_planning_shadow($planning_shadow_id, $project_task_planning_shadow_update_data);

    if ($project_task_planning_shadow_iresult['status'] == SUCCESS) {
        $return["data"]   = "Task Planning Successfully Updated";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
}

/*
PURPOSE : To add new Plan Summary
INPUT 	: Type, Mapping ID, Measurment, UOM, Plan Start Date, Plan End date, Actual Start Date, Actual End Date, Actual Measurment, Forecasted Date, Remarks, Added By
OUTPUT 	: Summary ID, success or failure message
BY 		: Lakshmi
*/
function i_add_plan_summary($type, $mapping_id, $measurment, $uom, $plan_start_date, $plan_end_date, $actual_start_date, $actual_end_date, $actual_measurment, $forecasted_date, $remarks, $added_by)
{
    $plan_summary_search_data = array("type"=>$type,"mapping_id"=>$mapping_id,"active"=>'1');
    $plan_summary_sresult = db_get_plan_summary($plan_summary_search_data);

    if ($plan_summary_sresult["status"] == DB_NO_RECORD) {
        $plan_summary_iresult =  db_add_plan_summary($type, $mapping_id, $measurment, $uom, $plan_start_date, $plan_end_date, $actual_start_date, $actual_end_date, $actual_measurment, $forecasted_date, $remarks, $added_by);

        if ($plan_summary_iresult['status'] == SUCCESS) {
            $return["data"]   = "Plan Summary Successfully Added";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "Plan Summary already exists!";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To get Plan Summary List
INPUT 	: Summary ID, Type, Mapping ID, Measurment, UOM, Plan Start Date, Plan End date, Actual Start Date, Actual End Date, Actual Measurment, Forecasted Date, Active,
          Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Plan Summary
BY 		: Lakshmi
*/
function i_get_plan_summary($plan_summary_search_data)
{
    $plan_summary_sresult = db_get_plan_summary($plan_summary_search_data);

    if ($plan_summary_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $plan_summary_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Plan Summary Added. Please Contact The System Admin";
    }

    return $return;
}

/*
PURPOSE : To update Plan Summary
INPUT 	: Summary ID, Plan Summary Update Array
OUTPUT 	: Summary ID; Message of success or failure
BY 		: Lakshmi
*/
function i_update_plan_summary($summary_id, $plan_summary_update_data)
{
    $plan_summary_search_data = array("type"=>$plan_summary_update_data['type'],"mapping_id"=>$plan_summary_update_data['mapping_id'],"active"=>'1');
    $plan_summary_sresult = db_get_plan_summary($plan_summary_search_data);

    $allow_update = false;
    if ($plan_summary_sresult["status"] == DB_NO_RECORD) {
        $allow_update = true;
    } elseif ($plan_summary_sresult["status"] == DB_RECORD_ALREADY_EXISTS) {
        if ($plan_summary_sresult['data'][0]['plan_summary_id'] == $summary_id) {
            $allow_update = true;
        }
    }

    if ($allow_update == true) {
        $plan_summary_sresult = db_update_plan_summary($summary_id, $plan_summary_update_data);

        if ($plan_summary_sresult['status'] == SUCCESS) {
            $return["data"]   = "Plan Summary Successfully Updated";
            $return["status"] = SUCCESS;
        } else {
            $return["data"]   = "Internal Error. Please try again later";
            $return["status"] = FAILURE;
        }
    } else {
        $return["data"]   = "Plan Summary Already Exists";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To Delete Plan Summary
INPUT 	: Summary ID, Plan Summary Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_plan_summary($summary_id, $plan_summary_update_data)
{
    $plan_summary_update_data = array('active'=>'0');
    $plan_summary_sresult = db_update_plan_summary($summary_id, $plan_summary_update_data);

    if ($plan_summary_sresult['status'] == SUCCESS) {
        $return["data"]   = "Plan Summary Successfully Deleted";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }

    return $return;
}

/*
PURPOSE : To add new Project Plan Wish
INPUT 	: Project ID, Days, Start Date, Added By
OUTPUT 	: Plan Wish ID, success or failure message
BY 		: Lakshmi
*/
function i_add_project_plan_wish($project_id, $days, $plan_start_date, $lock, $locked_by, $locked_on, $added_by)
{
    $project_plan_wish_iresult = db_add_project_plan_wish($project_id, $days, $plan_start_date, $lock, $locked_by, $locked_on, $added_by);
    if ($project_plan_wish_iresult['status'] == SUCCESS) {
        $return["data"]   = $project_plan_wish_iresult['data'];
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
    return $return;
}

/*
PURPOSE : To get Project Plan Wish List
INPUT 	: Plan Wish ID, Project ID, Days, Start Date, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Project Plan Wish
BY 		: Lakshmi
*/
function i_get_project_plan_wish($project_plan_wish_search_data)
{
    $project_plan_wish_sresult = db_get_project_plan_wish($project_plan_wish_search_data);
    if ($project_plan_wish_sresult['status'] == DB_RECORD_ALREADY_EXISTS) {
        $return["status"] = SUCCESS;
        $return["data"]   = $project_plan_wish_sresult["data"];
    } else {
        $return["status"] = FAILURE;
        $return["data"]   = "No Project Plan Wish added yet!";
    }

    return $return;
}

 /*
PURPOSE : To update Project Plan Wish
INPUT 	: Plan Wish ID, Project Plan Wish Update Array
OUTPUT 	: Plan Wish ID; Message of success or failure
BY 		: Lakshmi
*/
function i_update_project_plan_wish($plan_wish_id, $project_plan_wish_update_data)
{
    $project_plan_wish_iresult = db_update_project_plan_wish($plan_wish_id, $project_plan_wish_update_data);

    if ($project_plan_wish_iresult['status'] == SUCCESS) {
        $return["data"]   = "Payment Plan Wish Successfully added";
        $return["status"] = SUCCESS;
    } else {
        $return["data"]   = "Internal Error. Please try again later";
        $return["status"] = FAILURE;
    }
    return $return;
}

function i_get_details($work_type, $task_id, $road_id)
{
    $project_process_task_search_data = array("task_id"=>$task_id,"active"=>'1');
    $project_plan_process_task_list = i_get_project_process_task($project_process_task_search_data);
    if ($project_plan_process_task_list["status"] == SUCCESS) {
        $project_plan_process_task_list_data = $project_plan_process_task_list["data"];
        $task_name = $project_plan_process_task_list_data[0]["project_task_master_name"];
        $project = $project_plan_process_task_list_data[0]["project_master_name"];
        $project_id = $project_plan_process_task_list_data[0]["project_management_master_id"];
        $process = $project_plan_process_task_list_data[0]["project_process_master_name"];
    } else {
        $alert = $project_plan_process_task_list["data"];
        $alert_type = 0;
        $task_name = "";
        $project = "";
        $project_id = "";
        $process = "";
    }

    // Get Project task planning list
    $project_task_planning_search_data = array("active"=>'1',"task_id"=>$task_id,"no_of_roads"=>$road_id);
    $project_task_planning_list = i_get_project_task_planning($project_task_planning_search_data);
    if ($project_task_planning_list["status"] == SUCCESS) {
        $project_task_planning_list_data = $project_task_planning_list["data"];
        $planned_msmrt = $project_task_planning_list_data[0]["project_task_planning_measurment"];
        $planned_end_date = $project_task_planning_list_data[0]["project_task_planning_end_date"];
        $uom = $project_task_planning_list_data[0]["project_uom_name"];
        if ($road_id == "No Roads") {
            $road = "No Roads";
        } else {
            $road = $project_task_planning_list_data[0]["project_site_location_mapping_master_name"];
        }
    } else {
        $planned_msmrt = "";
        $planned_end_date = "";
        $uom = "";
        $road = "";
    }
    //Get Man power List
    $man_power_search_data = array("task_id"=>$task_id,"road_id"=>$road_id,"active"=>'1',"work_type"=>$work_type);
    $man_power_actual_data = i_get_man_power_list($man_power_search_data);
    if ($man_power_actual_data["status"] == SUCCESS) {
        $total_mp_completed_msmrt = 0;
        for ($mp_count = 0 ; $mp_count < count($man_power_actual_data["data"]) ; $mp_count++) {
            $total_mp_completed_msmrt = $total_mp_completed_msmrt + $man_power_actual_data["data"][$mp_count]["project_task_actual_manpower_completed_msmrt"];
        }
        $manpower_status = "";
    } else {
        $total_mp_completed_msmrt = 0;
        $manpower_status = "NotStarted";
    }

    //Get Machine List
    $actual_machine_plan_search_data = array("task_id"=>$task_id,"road_id"=>$road_id,"active"=>'1',"work_type"=>$work_type);
    $machine_latest_list = i_get_machine_planning_list($actual_machine_plan_search_data);
    if ($machine_latest_list['status'] == SUCCESS) {
        $machine_latest_list_data = $machine_latest_list["data"];
        $total_mc_completed_msmrt = 0;
        for ($mc_count = 0 ; $mc_count < count($machine_latest_list_data) ; $mc_count++) {
            $total_mc_completed_msmrt = $total_mc_completed_msmrt + $machine_latest_list_data[$mc_count]["project_task_actual_machine_msmrt"];
        }
        $machine_status = "";
    } else {
        $total_mc_completed_msmrt = 0;
        $machine_status = "NotStarted";
    }

		//Get Contract List
		$project_task_actual_boq_search_data = array("task_id"=>$task_id, "active"=>'1', "work_type"=>$work_type, "road_id"=>$road_id);
		$contract_latest_list = i_get_project_task_boq_actual($project_task_actual_boq_search_data);

		if ($contract_latest_list['status'] == SUCCESS) {
		    $contract_status = "" ;
		    $contract_latest_list_data = $contract_latest_list["data"];
		    $total_cw_completed_msmrt = 0;
		    for ($cw_count = 0 ; $cw_count < count($contract_latest_list["data"]) ; $cw_count++) {
		        $total_cw_completed_msmrt = $total_cw_completed_msmrt + $contract_latest_list["data"][$cw_count]["project_task_actual_boq_total_measurement"];
		    }
		} else {
		  $contract_status = "NotStarted";
		  $total_cw_completed_msmrt = '0';
		}
		$overall_completed_msmrt = $total_mp_completed_msmrt + $total_mc_completed_msmrt + $total_cw_completed_msmrt;

		$data = array( "project" => $project,
		               "process" => $process,
		               "project_id" => $project_id,
		               "task" => $task_name,
		               "road"=>$road,
		               "uom" => $uom,
		               "planned_msmrt" => $planned_msmrt,
                   "planned_end_date" => $planned_end_date,
		               "mp_msmrt" => $total_mp_completed_msmrt,
		               "mc_msmrt" => $total_mc_completed_msmrt,
		               "cw_msmrt" => $total_cw_completed_msmrt,
		               "manpower_status" => $manpower_status,
		               "machine_status" =>  $machine_status,
		               "contract_status" => $contract_status,
		               "overall_msmrt" => $overall_completed_msmrt
		             );

		return $data;
}
function send_mail($project_id,$task_id,$machine_type,$msmrt,$user,$road_id,$remarks,$msg)
{
    //Get Regular Measurement
    $get_details_data = i_get_details('Regular', $task_id, $road_id);
    $reg_mp_msmrt = $get_details_data["mp_msmrt"];
    $planned_msmrt = $get_details_data["planned_msmrt"];
    $uom = $get_details_data["uom"];
    $reg_overall_msmrt = $get_details_data["overall_msmrt"];
    $road_name = $get_details_data["road"];
    $reg_mc_msmrt = $get_details_data["mc_msmrt"];
    $reg_cw_msmrt = $get_details_data["cw_msmrt"];
    $project = $get_details_data["project"];
    $process_name = $get_details_data["process"];
    $task_name = $get_details_data["task"];

    //Get Rework Measurement
    $get_details_data = i_get_details('Rework', $task_id, $road_id);
    $rework_mp_msmrt = $get_details_data["mp_msmrt"];
    $rework_mc_msmrt = $get_details_data["mc_msmrt"];
    $rework_cw_msmrt = $get_details_data["cw_msmrt"];
    $rework_overall_msmrt = $get_details_data["overall_msmrt"];
  //Get Email list
	$project_email_search_data = array("project_id"=>$project_id);
	$email_list = db_get_project_email_list($project_email_search_data);
	if ($email_list["status"] == DB_RECORD_ALREADY_EXISTS) {
			$email_list_data = $email_list["data"];
			$to = $email_list_data[0]["project_email_details_to"];
			$cc = $email_list_data[0]["project_email_details_cc"];
			$bcc = $email_list_data[0]["project_email_details_bcc"];
	}

  $subject = '[Notice] Project : '.$project . '  '. $msg;
  $heading = "<h3>
  						Startted  By : <span style='color:#00ba8b;'>".$user ."</span>
  						<h3>
  						<?h3>
  						Paused On: <span style='color:#00ba8b;'>". date('d-m-y h:m A') ."</span>
  						</h3>
  						<br/>";
  				$table = "<table  style=border-collapse:collapse;width:50%>
  				<tr>
  				<td style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;color: #2C4099; text-align:center;'>Project</th>
  				<td style='border: 1px solid #ddd;padding: 8px; text-align:center;'>".$project."</th>
  				</tr>
  		    <tr>
  				<td style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;color: #2C4099;text-align:center;'>Process</th>
  			 <td style='border: 1px solid #ddd;padding: 8px; text-align:center;'>".$process_name."</th>
  				</tr>
  		    <tr>
  				<td style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;color: #2C4099;text-align:center;'>Task</th>
   			 <td style='border: 1px solid #ddd;padding: 8px;text-align:center; '>".$task_name."</th>
  				</tr>
  				<tr>
  				<td style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;color: #2C4099;text-align:center;'>Road</th>
   			 <td style='border: 1px solid #ddd;padding: 8px;text-align:center; '>".$road_name."</th>
  				</tr>
          <tr>
  				<td style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;color: #2C4099;text-align:center;'>Planned Msmrt</th>
   			 <td style='border: 1px solid #ddd;padding: 8px;text-align:center; '>".$planned_msmrt."</th>
  				</tr>
          <tr>
  				<td style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;color: #2C4099;text-align:center;'>Actual Msmrt</th>
   			 <td style='border: 1px solid #ddd;padding: 8px;text-align:center; '>".' Total Msmrt '.$reg_overall_msmrt.' ( MP '.$reg_mp_msmrt.' MC '.$reg_mc_msmrt.' CW '.$reg_cw_msmrt.')'."</th>
  				</tr>
          <tr>
          <td style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;color: #2C4099;text-align:center;'>Rework Msmrt</th>
         <td style='border: 1px solid #ddd;padding: 8px;text-align:center; '>".' Total Msmrt '.$rework_overall_msmrt.' ( MP '.$rework_mp_msmrt.' MC '.$rework_mc_msmrt.' CW '.$rework_cw_msmrt.')'."</th>
          </tr>
  				<td style='border: 1px solid #ddd;padding-top: 12px;padding-bottom: 12px;color: #2C4099;text-align:center;'>Remarks</th>
   			 <td style='border: 1px solid #ddd;padding: 8px;text-align:center; '>".$remarks."</td>
  				</tr>";
  					$message = $heading;
  					$message .= $table;
            send_sendgrid_email($to,'',[$cc],$subject,$message,'',true);
}

function i_get_actuals_data($process_id)
{
    //Get Manpower Date and $msmrt
    $project_budget_manpower_search_data = array("process_id"=>$process_id);
    $mp_list =  db_get_project_budget_manpower($project_budget_manpower_search_data);
    if($mp_list["status"] == DB_RECORD_ALREADY_EXISTS)
    {
      $mp_min_start_date = $mp_list["data"][0]["min_start_date"];
      $mp_max_end_date = $mp_list["data"][0]["max_end_date"];
      $mp_msmrt = $mp_list["data"][0]["total_msmrt"];
    }
    else {
      $mp_min_start_date = "0000-00-00";
      $mp_max_end_date = "0000-00-00";
      $mp_msmrt = "0";
    }

    //Get Machine date and Msmrt
    $project_budget_machine_search_data = array("process_id"=>$process_id);
    $mc_list =  db_get_project_budget_machine($project_budget_machine_search_data);
    if($mc_list["status"] == DB_RECORD_ALREADY_EXISTS)
    {
      $mc_min_start_date = $mc_list["data"][0]["min_start_date"];
      $mc_max_end_date = $mc_list["data"][0]["max_end_date"];
      $mc_msmrt = $mc_list["data"][0]["total_msmrt"];
    }
    else {
      $mc_min_start_date = "0";
      $mc_max_end_date = "0000-00-00";
      $mc_msmrt = "0";
    }

    //Get contract date and msmrt
    $project_budget_contract_search_data = array("process_id"=>$process_id);
    $cw_list =  db_get_project_budget_contract($project_budget_contract_search_data);
    if($cw_list["status"] == DB_RECORD_ALREADY_EXISTS)
    {
      $cw_min_start_date = $cw_list["data"][0]["min_start_date"];
      $cw_max_end_date = $cw_list["data"][0]["max_end_date"] ;
      $cw_msmrt = $cw_list["data"][0]["msmrt"];
    }
    else {
      $cw_min_start_date = "0";
      $cw_max_end_date = "0000-00-00";
      $cw_msmrt = "0";
    }

    $total_actual_msmrt = $mp_msmrt + $mc_msmrt + $cw_msmrt ;
    $min_dates = array($mp_min_start_date,$mc_min_start_date,$cw_min_start_date);
    $max_dates = array($mp_max_end_date,$mc_max_end_date,$cw_max_end_date);
    $process_min_start_date = min(array_filter($min_dates));
    $process_max_end_date = max(array_filter($max_dates));

    $process_dates = array("process_min_start_date"=>$process_min_start_date,
                            "process_max_end_date"=>$process_max_end_date,
                            "total_actual_msmrt"=>$total_actual_msmrt);

    return $process_dates ;


}
function i_get_cp($process_id)
{
  $project_process_task_search_data = array("process_id"=>$process_id,"active"=>'1');
  $project_process_task_list = i_get_project_process_task($project_process_task_search_data);
  if($project_process_task_list["status"] == SUCCESS)
  {
    $project_process_task_list_data = $project_process_task_list["data"];
    $no_of_task = count($project_process_task_list["data"]);
    $total_value = 0;
    for($t_count = 0 ; $t_count < count($project_process_task_list_data) ; $t_count++)
    {
      $avg_count = 0 ;
      //Get actual man power List
      $man_power_search_data = array("active"=>'1',"max_percentage"=>'1',"task_id"=>$project_process_task_list_data[$t_count]["project_process_task_id"],"work_type"=>"Regular");
      $man_power_list = i_get_man_power_list($man_power_search_data);
      if($man_power_list["status"] == SUCCESS)
      {
        $man_power_list_data = $man_power_list["data"];
        $manpower_percentage = $man_power_list_data[0]["max_percentage"];
        $avg_count = $avg_count + 1;

      }
      else
      {
        $manpower_percentage = 0;
      }

      //Get Actual Machine List
      $actual_machine_plan_search_data = array("active"=>'1',"task_id"=>$project_process_task_list_data[$t_count]["project_process_task_id"],"work_type"=>"Regular");
      $actual_machine_plan_list = i_get_machine_planning_list($actual_machine_plan_search_data);
      if($actual_machine_plan_list["status"] == SUCCESS)
      {
        $actual_machine_plan_list_data = $actual_machine_plan_list["data"];
        $machine_percentage = $actual_machine_plan_list_data[0]["project_task_actual_machine_completion"];
        $avg_count = $avg_count + 1;

      }
      else
      {
        $machine_percentage = "0";
      }
      //Get contract List data
      $project_task_boq_actual_search_data = array("active"=>'1',"max_contract_percentage"=>'1',"task_id"=>$project_process_task_list_data[$t_count]["project_process_task_id"],"work_type"=>"Regular");
      $project_task_boq_actual_list = i_get_project_task_boq_actual($project_task_boq_actual_search_data);
      if($project_task_boq_actual_list['status'] == SUCCESS)
      {
        $project_task_boq_actual_list_data = $project_task_boq_actual_list['data'];
        $contract_percentage = $project_task_boq_actual_list_data[0]["max_cpercentage"];
        $avg_count = $avg_count + 1;

              }
      else
      {
        $contract_percentage = 0;
      }

      $total_percentage = $manpower_percentage + $machine_percentage + $contract_percentage ;
      if($avg_count != 0)
      {
        $avg_percentage = $total_percentage/$avg_count;
      }
      else
      {
        $avg_percentage = 0;
      }
      $total_value = $total_value + $avg_percentage;
    }
    $total_overall_percenatge = $total_value/$no_of_task;
    $overall_pending_percentage = 100 - $total_overall_percenatge;
  }
  else {
    //
  }
  $data = array("overall_cp"=>$total_overall_percenatge,"pending"=>$overall_pending_percentage);
  return $data ;
}
