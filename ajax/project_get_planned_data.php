<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$process_id      = $_GET["process_id"];

	$project_task_planning_search_data = array("process_id"=>$process_id);
	$project_task_planning_list =  db_get_task_planning_list($project_task_planning_search_data);
	if($project_task_planning_list["status"] == DB_RECORD_ALREADY_EXISTS)
	{
				$planned_start_date = $project_task_planning_list["data"][0]["min_start_date"] ? $project_task_planning_list["data"][0]["min_start_date"] : "0000-00-00";
				$planned_end_date = $project_task_planning_list["data"][0]["max_end_date"] ? $project_task_planning_list["data"][0]["max_end_date"] : "0000-00-00";
				$toatal_planned_msmrt = $project_task_planning_list["data"][0]["total_msmrt"] ;
			}
	else {
		$planned_start_date = "0000-00-00";
		$planned_end_date = "0000-00-00";
		$planned_days = "0";

	}

	// $cp_list = i_get_cp($process_id);
	// $cp = $cp_list["overall_cp"];
	//
	// //Get Min start date and max end date
	// $project_actuals_list =  i_get_actuals_data($process_id);
	// $orocess_min_start_date = $project_actuals_list["process_min_start_date"];
	// if($cp == 100)
	// {
	// 	$orocess_max_start_date = $project_actuals_list["process_max_end_date"];
	// }
	// else {
	// 	$orocess_max_start_date = date("d-M-Y");
	// }
	// $total_actual_msmrt = $project_actuals_list["total_actual_msmrt"];
	//
	// //Actual days calculation
	// if($orocess_max_start_date != "0000-00-00")
	// {
	// 	$actual_days = get_date_diff($orocess_min_start_date,$orocess_max_start_date);
	// }
	// else {
	// 	$actual_days = get_date_diff($orocess_min_start_date,date("Y-m-d"));
	// }

	//Completed percentage calculation
	//$cp = ($total_actual_msmrt/100000 ) * 100 ;
	 $planned_data = array("planned_start_date"=>$planned_start_date,
	 											 "planned_end_date"=>$planned_end_date) ;
	echo json_encode($planned_data);
}
else
{
	header("location:login.php");
}
?>
