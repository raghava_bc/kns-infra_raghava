<?php
session_start();
$_SESSION['module'] = 'PM Masters';

define('PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID', '253');

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '2', '1');
    $edit_perms_list   	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '3', '1');
    $delete_perms_list 	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '4', '1');
    $ok_perms_list   	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '5', '1');
    $approve_perms_list = i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '6', '1');

    $project_id = '';
    if (isset($_GET["project_id"])) {
      $project_id   = $_GET["project_id"];
    }

    // Project data
    $project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
    $project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
    if ($project_management_master_list["status"] == SUCCESS) {
        $project_management_master_list_data = $project_management_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_management_master_list["data"];
    }
} else {
    header("location:login.php");
}
?>
<html>
  <head>
    <meta charset="utf-8">
    <title>Project Budget Report List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js?21062018"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
	  <script src="datatable/project_budget_report.js?<?php echo time(); ?>"></script>
    <link href="./css/style.css?<?php echo time(); ?>" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bootstrap_aku.min.css" rel="stylesheet">
  </head>
  <body>
  <?php
    include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_header.php');
  ?>
  <div class="main margin-top">
    <div class="main-inner">
      <div class="container">
        <div class="row">
          <div class="widget widget-table action-table">
            <div class="widget-header">
              <h3>Project Budget Report List</h3>
              <?php if(isset($project_id) && !empty($project_id)){ ?>
              <!-- Export & Import buttons -->
                <form id="formImport" style="float:right;margin-top: 5px; margin-right: 5px;" class="form-horizontal" action="./import_project_budget_plan.php?project_id=<?php echo $project_id; ?>" method="post" enctype="multipart/form-data">
                  <div class="btn-group btn-group-sm pull-right" role="group">
                    <a href="budget_planning_excel.php?project_id=<?php echo $project_id; ?>" class="btn btn-primary">
                      <span class="glyphicon glyphicon-download"></span> Export
                    </a>
                  <label class="btn btn-default" style="margin-top: 0px;">
                      <span class="glyphicon glyphicon-upload"></span>
                      Import <input type="file" name="file" id="file" accept=".xls" onchange="checkFileAttached()" hidden>
                  </label>
                </div>
                </form>
            <?php } ?>
            </div>

            <div class="widget-header widget-toolbar">
              <form method="get" class="form-inline">
                <select name="project_id" id="project_id" class="form-control">
                 <option value="">- - Select Project - -</option>
              <?php
                for ($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++) { ?>
                 <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>"
                   <?php if ($project_id == $project_management_master_list_data[$project_count]["project_management_master_id"]) {
                    ?> selected="selected" <?php } ?>>
                    <?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?>
                 </option>
              <?php } ?>
              </select>
              <input type="submit" class="btn btn-primary" />
              </form>
            </div>
          </div>
            <div class="widget-content">
             <table class="table table-striped table-bordered display nowrap" id="example">
               <thead>
                 <tr>
                    <th colspan="6">&nbsp;</th>
                    <th colspan="4" class="center blue">Planned</th>
                    <th colspan="4" class="center">Actual</th>
                    <th colspan="4" class="center red">Variance</th>
                 </tr>
                 <tr>
                   <th>#</th>
                   <th>Project</th>
                   <th>Process</th>
                   <th>Task Name</th>
                   <th>Road Name</th>
                   <th>UOM</th>
                   <th class="blue">Manpower</th>
                   <th class="blue">Machine</th>
                   <th class="blue">Contract</th>
                   <th class="blue">Material</th>
                   <th>Manpower</th>
                   <th>Machine</th>
                   <th>Contract</th>
                   <th>Material</th>
                   <th class="red">Manpower</th>
                   <th class="red">Machine</th>
                   <th class="red">Contract</th>
                   <th class="red">Material</th>
                </tr>
             </thead>
               </tbody>
             </table>
            </div>
            <!-- widget-content -->
            </div>
          </div>
        </div>
      </div>
  </body>
</html>
