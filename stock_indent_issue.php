<?php
session_start();
$_SESSION['module'] = 'PM Masters';

define('PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID', '253');

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '2', '1');
    $edit_perms_list   	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '3', '1');
    $delete_perms_list 	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '4', '1');
    $ok_perms_list   	= i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '5', '1');
    $approve_perms_list = i_get_user_perms($user, '', PROJECT_TASK_ACTUAL_MANPOWER_LIST_FUNC_ID, '6', '1');

    $search_status = "Approved";
    $project_id = "";
    $search_requested_by = "";

    if (isset($_GET["search_status"])) {
  	$search_status = $_GET["search_status"];
    }

    if (isset($_GET["requested_by"])) {
    	$search_requested_by = $_GET["requested_by"];
    }

    if (isset($_GET["project_id"])) {
    	$project_id = $_GET["project_id"];
    }

    if (isset($_GET["indent_id"])) {
    	$indent_id = $_GET["indent_id"];
    }

    //Get Project List
    $stock_project_search_data = array();
    $project_list = i_get_project_list($stock_project_search_data);
    if($project_list["status"] == SUCCESS)
    {
      $project_list_data = $project_list["data"];
    } else { $alert = $project_list["data"]; $alert_type = 0;
    }

    $project_stock_module_mapping_search_data = array("stock_project_id"=>$project_id);
  	$project_stock_mapping_list = i_get_project_stock_module_mapping($project_stock_module_mapping_search_data);

  	if ($project_stock_mapping_list["status"] == SUCCESS) {
  			$pm_project = $project_stock_mapping_list["data"][0]["project_stock_module_mapping_mgmnt_project_id"];
  	} else {
  			$pm_project = "";
  	}
  	$project_process_master_list = db_get_project_process_list('project_id',$pm_project);
    if ($project_process_master_list["status"] == DB_RECORD_ALREADY_EXISTS) {
      $project_process_master_list_data = $project_process_master_list["data"];
    }

    // Get User List
    $user_list = i_get_user_list('','','','','1');
    if($user_list["status"] == SUCCESS)
    {
      $user_list_data = $user_list["data"];
    }

} else {
    header("location:login.php");
}
?>
<html>
  <head>
    <meta charset="utf-8">
    <title>Stock Indent Issue List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
	  <script src="datatable/stock_issue.js?<?php echo time(); ?>"></script>
    <link href="./css/style.css?<?php echo time(); ?>" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bootstrap_aku.min.css" rel="stylesheet">
  </head>
  <body>
  <?php
    include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_header.php');
  ?>
  <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Issue Material</h4>
      </div>

      <div class="widget-header" style="height:auto;">
        <div style="border-bottom: 1px solid #C0C0C0;">
          <span class="header-label">Material Name: </span><span id="material_name"></span>
          <span class="header-label">Material Code: </span><span id="material_code"></span>
        </div>
        <div style="border-bottom: 1px solid #FFFFFF;">
          <span class="header-label">Stock Qty: </span><span id="material_stock_quantity"></span>
          <span class="header-label">Issued Qty: </span><span id="material_issued_quantity"></span>
          <span class="header-label">Pending Qty: </span><span id="material_pending_quantity"></span>
        </div>
      </div>

      <div class="modal-body">
        <input type="hidden" id="pm_project_id" value="<?php echo $pm_project; ?>">
        <form id="project_add_reason_master_form">
        <input type="hidden" id="selected_row_id">
        <input type="hidden" id="hd_machine_id"/>

        <?php if(intval($project_id) !== 1){ ?>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label" for="ddl_process">Process*</label>
                <select name="ddl_process" id="ddl_process" required class="form-control" onchange="loadTasksAndRoads(this)">
                  <option>- - Select Process - -</option>
                  <?php for($process_count = 0; $process_count < count($project_process_master_list_data); $process_count++) { ?>
            			<option value="<?php echo $project_process_master_list_data[$process_count]["plan_process_id"]; ?>"<?php if($project_process_master_list_data[$process_count]["process_master_id"] == $process_id ){ ?> selected="selected" <?php } ?>><?php echo $project_process_master_list_data[$process_count]["process_name"]; ?></option>
            			<?php } ?>
                </select>
              </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="control-label" for="search_task">Task*</label>
              <div class="controls">
               <select id="search_task" name="search_task" required="" class="form-control">
                 <option>- - Select Task - -</option>
               </select>
             </div>
          </div>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label" for="ddl_road">Road*</label>
          <div class="controls">
           <select name="ddl_road" id="ddl_road" required="" class="form-control">
             <option>- - Select Road - -</option>
           </select>
          </div>
        </div>
      <? } ?>

        <div class="form-group">
          <label class="control-label" for="txt_remarks">Issue Qty</label>
          <div class="controls">
            <input type="number" minlength="3" class="form-control" min="0.01" step="0.01" name="issued_qty" id="issued_qty" onkeyup="return check_validity();">
          </div>
        </div>

        <div class="form-group" id="div-machine-id">
          <label class="control-label" for="txt_remarks">Machine</label>
           <div class="controls">
             <input type="text" class="form-control" name="stxt_machine"  autocomplete="off" id="stxt_machine" onkeyup="return get_machine_list('<?php echo $indent_item_list_data[0]["stock_material_name"]; ?>');" placeholder="Search Machine by name or code" />
          </div> <!-- /controls -->
        </div> <!-- /form-group -->
        <div id="search_results" class="dropdown-content" style="display:none;"></div>
          <div class="form-group">
            <label class="control-label" for="stxt_remarks">Remarks</label>
            <div class="controls">
              <input type="text" name="stxt_remarks" id="stxt_remarks" class="form-control" placeholder="Remarks">
            </div> <!-- /controls -->
          </div> <!-- /form-group -->

        </form>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="submit_issue_material_stock(this)">Submit</button>
        </div>
        </div>
     </div>
  </div>
  <!-- // modal -->


  <div class="main margin-top">
    <div class="main-inner">
      <div class="container">
        <div class="row">
          <div class="widget widget-table action-table">
            <div class="widget-header">
              <h3>Stock Indent Issue List</h3><span style="float:right; padding-right:20px;"><a href="stock_indent_issue_list.php?project_id=<?php echo $project_id ;?>"  class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-remove"></span></a></span>
            </div>
              <input type="hidden" name="indent_id" id="indent_id" value="<?php echo $indent_id ;?>" />
             </div>
          </div>
            <div class="widget-content">
             <table class="table table-striped table-bordered display nowrap" id="example" width="100%">
               <thead>
                 <tr>
                   <th>#</th>
                   <th>Project</th>
                   <th>Indent No</th>
                   <th>Indent Date</th>
                   <th>Material Name</th>
                   <th>Material Code</th>
                   <th>Quantity</th>
                   <th>Stock Qty</th>
                   <th>Issued</th>
                   <th>Action</th>
                </tr>
             </thead>
               </tbody>
             </table>
            </div>
            <!-- widget-content -->
            </div>
          </div>
        </div>
      </div>
  </body>
</html>
