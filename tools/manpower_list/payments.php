
<?php
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');

/* SESSION INITIATE - START */
//session_start();
/* SESSION INITIATE - END */

/*
FILE        : project_actual_payment_manpower_list.php
CREATED ON    : 07-Dec-2016
CREATED BY    : Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/* DEFINES - START */
//define('PROJECT_ACCEPT_PAYMENT_MANPOWER_LIST_FUNC_ID', '258');
/* DEFINES - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user            = $_SESSION["loggedin_user"];
    $role            = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Initialization
    $alert_type = -1;
    $alert = "";

    // Get permission settings for this user for this page
    $view_perms_list   = i_get_user_perms($user, '', PROJECT_ACCEPT_PAYMENT_MANPOWER_LIST_FUNC_ID, '2', '1');
    $edit_perms_list   = i_get_user_perms($user, '', PROJECT_ACCEPT_PAYMENT_MANPOWER_LIST_FUNC_ID, '3', '1');

    if(isset($_GET["search_vendor"])) {
      $search_vendor  = $_GET["search_vendor"];
    }


    // Get Project  Payment ManPower modes already added
    $project_actual_payment_manpower_search_data = array("active"=>'1',"status"=>"Approved","secondary_status"=>"Accepted","vendor_id"=>$search_vendor,"start"=>'-1');
    $project_actual_payment_manpower_list = i_get_project_actual_payment_manpower($project_actual_payment_manpower_search_data);
    if ($project_actual_payment_manpower_list['status'] == SUCCESS) {
        $project_actual_payment_manpower_list_data = $project_actual_payment_manpower_list['data'];
    }

    // Get Project  Payment ManPower modes already added
    $project_man_power_issue_payment_search_data = array();
    $project_actual_payment_issue_list = i_get_project_man_power_issue_payment($project_man_power_issue_payment_search_data);
    if ($project_actual_payment_issue_list['status'] == SUCCESS) {
        $project_actual_payment_issue_list_data = $project_actual_payment_issue_list['data'];
    }

    // Get Project manpower_agency Master modes already added
    $project_manpower_agency_search_data = array("active"=>'1');
    $project_manpower_agency_list = i_get_project_manpower_agency($project_manpower_agency_search_data);
    if ($project_manpower_agency_list['status'] == SUCCESS) {
        $project_manpower_agency_list_data = $project_manpower_agency_list['data'];
    } else {
    }

    // Project data
    $project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
    $project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
    if ($project_management_master_list["status"] == SUCCESS) {
        $project_management_master_list_data = $project_management_master_list["data"];
    } else {
        $alert = $alert."Alert: ".$project_management_master_list["data"];
    }
} else {
    header("location:login.php");
}
    ?>




 <link rel="stylesheet" type="text/css" href="tools/datatables/css/jquery.dataTables.min.css">
 <link rel="stylesheet" type="text/css" href="tools/datatables/css/buttons.dataTables.min.css">

  <script src="tools/datatables/js/jquery.dataTables.min.js"></script>
  <script src="tools/datatables/js/dataTables.buttons.min.js"></script>
  <script src="tools/datatables/js/jszip.min.js"></script>
  <script src="tools/datatables/js/pdfmake.min.js"></script>
  <script src="tools/datatables/js/vfs_fonts.js"></script>
  <script src="tools/datatables/js/buttons.html5.min.js "></script>

<style type="text/css">
 .modal{
    width: 80%; /* respsonsive width */
    margin-left:-40%; /* width/2) */
}
.modal-open {
    overflow: hidden;
}

.modal {
  overflow-y: auto;
}
/* custom class to override .modal-open */
.modal-noscrollbar {
    margin-right: 0 !important;
}
#ajax_loading {
    width: 100%;
    height: 100%;
    background: url('tools/img/loader.gif') top center no-repeat rgba(0,0,0,0.15);
    background-position: 50% 50%;
    text-align: center;
    font-size: 20px;
    position: absolute;
    left: 0;
    top: 0;
    z-index: 99999999;
    display: none;
}
</style>
<div id="ajax_loading"></div>
<div class="container">

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">


      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Payments Details</h4>
        </div>

        <div class="modal-body">
          <div id="details"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

</div>


<script>

 $(document).ready(function() {


    $( ".vendor" ).click(function() {
    $('#ajax_loading').show();
    vendor_id = $(this).attr('data-id');
      $.ajax({
             url: "tools/manpower_list/getVenderPaymentDetails.php?search_vendor="+vendor_id,
             success: function(result){
                 $('#ajax_loading').hide();
                $("#details").html(result);
                $("#myModal").modal();
                    $('#example').DataTable( {
                        dom: 'Bfrtip',
                        buttons: [
                            'excelHtml5',
                            'csvHtml5',
                            'pdfHtml5'
                        ]
                    } );
            }});
    });

} );
</script>
