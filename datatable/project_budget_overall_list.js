$(document).ready(function() {
  var ajaxRequestPool = [];

  var table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    pageLength: 10,
    scrollX: true,
    searchDelay: 1500,
    processing: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_budget_overall_datatable.php',
    preDrawCallback: function() {
      $.each(ajaxRequestPool, function() {
        arguments[1].abort();
      })
      ajaxRequestPool = [];
    },
    fnRowCallback: function(row, full, index) {
      // get the planned
      var plannedAjaxReq = $.ajax({
        url: 'ajax/project_get_overall_plan.php',
        data: "project_id=" + full['project_management_master_id'],
        dataType: 'json',
        success: function(planned_response) {
          console.log(planned_response);
          // print planned
          planned_response = Math.abs(parseInt(planned_response));
          $('span#planned_' + index).html(planned_response);

          // get the actuals
          var actualAjaxReq = $.ajax({
            url: 'ajax/project_get_overall_actual.php',
            data: "project_id=" + full['project_management_master_id'],
            dataType: 'json',
            success: function(actual_response) {
              actual_response = Math.abs(parseInt(actual_response));
              $('span#actual_' + index).html(actual_response);
              if ((planned_response - actual_response) >= 0)
                $('span#variant_' + index).html(planned_response - actual_response);
              else
                $('span#variant_' + index).css('color', 'red').html(planned_response - actual_response);
            }
          });
          // add to Ajax pool request
          ajaxRequestPool.push(plannedAjaxReq);
        }
      });

      // add to Ajax pool request
      ajaxRequestPool.push(plannedAjaxReq);
    },
    fnServerParams: function(aoData) {
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    columns: [{
        orderable: false,
        width: "1%",
        data: function() {
          return arguments[3].row + 1;
        }
      },
      {
        orderable: true,
        data: function(){
          return '<a target="_blank" href="project_budget_report_list.php?project_id=' + arguments[0].project_management_master_id + '">'+arguments[0].project_master_name+'</a>';
        }
      },
      {
        className: 'blue',
        orderable: false,
        data: function() {
          return '<span id="planned_' + arguments[3].row + '">loading..</span>';
        },
      },
      {
        orderable: false,
        data: function() {
          return '<span id="actual_' + arguments[3].row + '">loading..</span>';
        },
      },
      {
        className: 'red',
        orderable: false,
        data: function() {
          return '<span id="variant_' + arguments[3].row + '">loading..</span>';
        },
      },
      {
        orderable: false,
        width: "1%",
        data: function() {
          if (arguments[1] === 'display') {
            return '<a target="_blank" href="project_budget_report_list.php?project_id=' + arguments[0].project_management_master_id + '"><span class="glyphicon glyphicon-eye-open"></span></a>';
          }
          return '';
        }
      }
    ]
  });
});
