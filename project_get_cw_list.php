<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_process_master_list.php
CREATED ON	: 0*-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Query String Data
    if (isset($_REQUEST['object_type'])) {
        $object_type = $_REQUEST['object_type'];
    } else {
        $object_type = "";
    }
    if (isset($_REQUEST['task_id'])) {
        $task_id = $_REQUEST['task_id'];
    } else {
        $task_id = "";
    }
    // Nothing
    if ($object_type == 'MC') {
        $project_object_output_search_data = array("active"=>'1',"object_type"=>"MC","task_id"=>$task_id);

        $project_object_output_master_list = i_get_project_object_output_task($project_object_output_search_data);
        if ($project_object_output_master_list["status"] == SUCCESS) {
            $project_object_output_master_list_data = $project_object_output_master_list["data"];
            for ($machine_count = 0 ; $machine_count < count($project_object_output_master_list_data); $machine_count++) {
                $result[$machine_count] = $project_object_output_master_list_data[$machine_count]["project_machine_type_master_name"];
            }
        }
    } elseif ($object_type == 'CW') {
        // Get Project CW Master List
        $project_object_output_search_data = array("active"=>'1',"object_type"=>"CW","task_id"=>$task_id);
        $project_object_output_cw_master_list = i_get_project_object_output_task($project_object_output_search_data);
        if ($project_object_output_cw_master_list['status'] == SUCCESS) {
            $project_object_output_cw_master_list_data = $project_object_output_cw_master_list["data"];
            for ($cw_count = 0 ; $cw_count < count($project_object_output_cw_master_list_data); $cw_count++) {
                $result[$cw_count] = $project_object_output_cw_master_list_data[$cw_count]["project_cw_master_name"];
            }
        }
    }

    echo json_encode($result);
} else {
    header("location:login.php");
}
