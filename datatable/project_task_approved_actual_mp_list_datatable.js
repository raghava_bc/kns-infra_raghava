var table;

function toggleSelection(ele) {
  if (!enableGenerateBilling()) {
    $(ele).prop("checked", false);
  }
  $("input.input-checkbox").prop("checked", ele.checked);
}

function drawTable() {
  table.draw();
}

var tmpManPowerData = [];

function generateBill() {

  var manpower = [];
  var tableData = table.data();

  // get all the 'checked' input:checkbox(s)
  $("table.DTFC_Cloned input.input-checkbox:checked").each(function(i, ele) {
    manpower.push(tableData[parseInt(ele.value)]);
  });

  if ($('input#selectAllCheckbox').is(':checked')) {
    manpower = tableData;
  } else if (!manpower.length) {
    manpower = [];
  }

  if (!manpower.length) {
    alert('Error: Select atleast one row to Bill');
    return false;
  }

  $.each(manpower, function(i, value) {
    tmpManPowerData.push(value);
  });

  $('#exampleModal').modal('show');

  // flush the previously loaded modal content
  $('#exampleModal').on('hidden.bs.modal', function(e) {
    $('form#add-bill-form')[0].reset();
  });
}

function generateManPowerId() {

  if (($('#billing_address option:selected').val().trim() == '') ||
    ($('#remarks').val().trim() == '')) {
    alert('Please input required field');
    return false;
  }

  var bill_substr = $("#billing_address option:selected").text().trim().replace(/\./g, '').replace('KNS', '').trim().substr(0, 5);
  bill_substr = (bill_substr === "KN SU") ? "SUREN" : bill_substr;

  $.ajax({
    url: 'ajax/project_select_payment.php',
    type: 'POST',
    data: {
      val: tmpManPowerData,
      bill_substr: bill_substr,
      vendor_id: $('#search_vendor').val(),
    },
    dataType: 'json',
    success: function(response) {
      tmpManPowerData = [];
      submitBilling(response.id)
    }
  });
}

function submitBilling(manpower_id) {
  $.ajax({
    url: 'project_add_bill_no.php',
    type: 'POST',
    data: {
      add_bill_no_submit: 'yes',
      manpower_id: manpower_id,
      billing_address: $('#billing_address').val(),
      remarks: $('#remarks').val()
    },
    success: function(bill_number) {
      $('#exampleModal').modal('hide');
      drawTable();
      setTimeout(function() {
        alert('Bill generated: ' + bill_number);
      }, 750);
    }
  });
}

function removeModal() {
  $(this).find('.modal-content').empty();
  $(this).data('bs.modal', null);
}

function hasValue(name) {
  var value = $(name).val();
  return (value !== '' && value.length);
}

function enableGenerateBilling() {
  return hasValue('#search_project') &&
    hasValue('#search_vendor') &&
    hasValue('#start_date') &&
    hasValue('#end_date') &&
    table.data().length;
}

$(document).ready(function() {

  // get hidden values saved from filters
  function getVars(key) {
    var element = document.getElementById(key);
    if (element) {
      return element.value;
    } else {
      return '';
    }
  }

  // set search filters' as hidden value
  function setVars(elements) {

    for (var element in elements) {
      if (element && elements[element].length) {
        var hiddenField1 = document.createElement("input");
        hiddenField1.setAttribute("type", "hidden");
        hiddenField1.setAttribute("id", element);
        hiddenField1.setAttribute("value", elements[element]);
        document.body.appendChild(hiddenField1);
      }
    }
  }

  table = $('#example').DataTable({
    stateSave: true,
    serverSide: true,
    dataSrc: 'aaData',
    ajax: 'datatable/project_task_approved_actual_mp_list.php',
    pageLength: 10,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    drawCallback: function() {
      manpower = [];
      $("input#selectAllCheckbox").prop("checked", false);
      $("input.input-checkbox").prop("checked", false);
      // $('#generateBillBtn').addClass('hide');
      // if(enableGenerateBilling()){
      //   $('#generateBillBtn').removeClass('hide');
      // }
    },
    fnServerParams: function(aoData) {
      aoData.aaSorting = aoData.order;
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.table = "project_task_actual_manpower_list";
      aoData.search_project = $('#search_project').val();
      aoData.search_vendor = $('#search_vendor').val();
      aoData.start_date = $('#start_date').val();
      aoData.end_date = $('#end_date').val();

      if (aoData.search && aoData.search.value) {
        aoData.sSearch = aoData.search.value;
      }
    },
    buttons: [{
      extend: 'colvis',
      columns: ':not(.noVis)'
    }],
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 4
    },
    scrollX: true,
    columns: [{
        className: 'noVis',
        "orderable": false,
        "data": function() {
          return arguments[3].settings._iDisplayStart + arguments[3].row + 1;
        }
      },
      {
        "data": "project_manpower_agency_name",
        "orderable": false
      },
      {
        "data": "project_master_name",
        "orderable": false
      },
      {
        "data": "project_process_master_name",
        "orderable": false
      },
      {
        "data": "project_task_master_name",
        "orderable": false
      },
      {
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          if (data.project_task_actual_manpower_road_id == 'No Roads') {
            return 'No Roads';
          }
          return data.project_site_location_mapping_master_name;
        },
        "orderable": false
      },
      {
        "data": "project_task_actual_manpower_work_type",
        "orderable": false
      },
      {
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }

          if (data.project_task_actual_manpower_work_type === 'Regular') {
            return data.project_task_actual_manpower_completion_percentage;
          }
          return data.project_task_actual_manpower_rework_completion;
        },
        "orderable": false
      },
      {
        "data": "project_task_actual_manpower_completed_msmrt",
        "orderable": false
      },
      // Men
      {
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return data.project_task_actual_manpower_no_of_men;
        },
        "orderable": false
      },

      {
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return data.project_task_actual_manpower_men_rate;
        },
        "orderable": false
      },

      {
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return data.project_task_actual_manpower_no_of_men / 8;
        },
        "orderable": false
      },
      // Women
      {
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return data.project_task_actual_manpower_no_of_women;
        },
        "orderable": false
      },
      {
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return data.project_task_actual_manpower_women_rate;
        },
        "orderable": false
      },
      {
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return data.project_task_actual_manpower_no_of_women / 8;
        },
        "orderable": false
      },
      // Mason
      {
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return data.project_task_actual_manpower_no_of_mason;
        },
        "orderable": false
      },
      {
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return data.project_task_actual_manpower_mason_rate;
        },
        "orderable": false
      },
      {
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return data.project_task_actual_manpower_no_of_mason / 8;
        },
        "orderable": false
      },

      {
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return parseFloat(data.project_task_actual_manpower_no_of_men / 8) + parseFloat(data.project_task_actual_manpower_no_of_women / 8) + parseFloat(data.project_task_actual_manpower_no_of_mason / 8);
        },
        "orderable": false
      },
      {
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return (parseFloat(data.project_task_actual_manpower_no_of_men) * parseFloat(data.project_task_actual_manpower_men_rate)) + (parseFloat(data.project_task_actual_manpower_no_of_women) * parseFloat(data.project_task_actual_manpower_women_rate)) + (parseFloat(data.project_task_actual_manpower_no_of_mason) * parseFloat(data.project_task_actual_manpower_mason_rate));
        },
        "orderable": false
      },
      {
        "data": "project_task_actual_manpower_remarks",
        "orderable": false
      },
      {
        "data": "user_name",
        "orderable": false
      },
      {
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return moment(data.project_task_actual_manpower_added_on).format('DD-MM-YYYY');
        },
        "orderData": [22, 23],
        "orderable": true
      },
      {
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return moment(data.project_task_actual_manpower_date).format('DD-MM-YYYY');
        },
        "orderData": [23, 22],
        "orderable": true
      },
      {
        "orderable": false,
        "className": 'noVis',
        "data": function(data, type, full) {
          if (type !== 'display') {
            return '';
          }
          return '<a target="_blank" href="project_labour_report_print.php?manpower_id=' + data.project_task_actual_manpower_id + '"><span class="glyphicon glyphicon-print"></span></a>';
        }
      },
      {
        "orderable": false,
        "className": 'noVis',
        "data": function() {
          if ((arguments[1] !== 'display') || (!enableGenerateBilling())) {
            return '';
          }
          return '<input type="checkbox" class="input-checkbox" value="' + arguments[3].row + '" >';
        }
      }
    ]
  });
});