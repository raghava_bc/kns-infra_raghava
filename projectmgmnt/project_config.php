<?php
define('PETROL',9);
define('BILL_NO_DELIMITER','/');
define('BILL_NO_DELIMITER1','-');
define('BILL_NO_ROOT','KNS');
define('BILL_NO_MANPOWER','NMR');
define('BILL_NO_CONTRACT','CW');
define('BILL_NO_MACHINE','MC');
define('BILL_NO_MONTH',date('m'));
define('BILL_NO_YEAR',date('Y'));
define('BILL_NO_NEXT_YEAR',date('Y', strtotime('+1 year')));
?>