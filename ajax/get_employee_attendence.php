<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

$_SESSION['module'] = 'PM Masters';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_hr_attendance.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_hr_leaves.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_config.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_attendance_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_employee_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_masters_functions.php');


if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Query String Data
    if (isset($_REQUEST['user_id'])) {
        $user_id = $_REQUEST['user_id'];
    } else {
        $user_id = "";
    }
    if (isset($_REQUEST['start_date'])) {
        $start_date = $_REQUEST['start_date'];
    } else {
        $start_date = "";
    }
    if (isset($_REQUEST['end_date'])) {
        $end_date = $_REQUEST['end_date'];
    } else {
        $end_date = "";
    }

    // $user_id = '143620071466608200';
    // $start_date = '2018-01-01';
    // $end_date = '2018-01-31';

    $attendance_filter_data = array("user_id"=>$user_id,"attendance_start_date"=>$start_date, "attendance_end_date"=>$end_date);
  	// $attendance_sresult = db_get_attendance_list($attendance_filter_data);
    $attendance_list = db_get_attendance_list($attendance_filter_data);

  	if($attendance_list["status"] == '-103')
  	{
  		$attendance_list_data = $attendance_list["data"];
  	}

    $total_present   = 0;
    $total_absent    = 0;
    $total_half_days = 0;
    $el_count        = 0;
    $sl_count        = 0;
    $cl_count        = 0;
    $col_count       = 0;
    $week_off        = 0;
    $public_holiday  = 0;
    if($attendance_list["status"] == '-103')
    {
     for($count = 0; $count < count($attendance_list_data); $count++)
     {

     $out_pass_filter_data = array('employee_id'=>$attendance_list_data[$count]["hr_attendance_employee"],"date"=>$attendance_list_data[$count]["hr_attendance_date"],"status"=>'1');
     $out_pass_sresult = i_get_out_pass_list($out_pass_filter_data);

     $op_in_time = false;
     if(($out_pass_sresult['status'] == SUCCESS) && ($attendance_list_data[$count]["hr_attendance_in_time"] != '00:00') && ($attendance_list_data[$count]["hr_attendance_in_time"] != '0:0') && ($attendance_list_data[$count]["hr_attendance_in_time"] != '0:00') && ($attendance_list_data[$count]["hr_attendance_out_time"] != '00:00') && ($attendance_list_data[$count]["hr_attendance_out_time"] != '0:0') && ($attendance_list_data[$count]["hr_attendance_out_time"] != '0:00'))
     {
       $op_in_time = t_check_in_time_proper($out_pass_sresult['data'][0]["hr_out_pass_out_time"],$out_pass_sresult['data'][0]["hr_out_pass_finish_time"]);

       $total_working_hours = i_get_total_working_hours($attendance_list_data[$count]["hr_attendance_in_time"],$attendance_list_data[$count]["hr_attendance_out_time"],$out_pass_sresult['data'][0]["hr_out_pass_out_time"],$out_pass_sresult['data'][0]["hr_out_pass_finish_time"]);
     }
     else
     {
       $total_working_hours = i_get_total_working_hours($attendance_list_data[$count]["hr_attendance_in_time"],$attendance_list_data[$count]["hr_attendance_out_time"],'00:00','00:00');
     }

     $disp_working = $total_working_hours['work_duration'];
     $disp_ot      = $total_working_hours['ot_duration'];
     $disp_total   = $total_working_hours['duration'];

     // Check for clean swipe by employee
     $is_swipe_clean = i_get_employee_swipe($attendance_list_data[$count]["hr_attendance_in_time"],$attendance_list_data[$count]["hr_attendance_out_time"]);

     // Get attendance type
     if($is_swipe_clean != 0)
     {
       $attendance_type = '2';

       $disp_working = '00:00';
       $disp_ot      = '00:00';
       $disp_total   = '00:00';
     }
     else
     {
       $attendance_type = i_get_attendance_type_from_value($total_working_hours['mins']);
     }

     $attendance_type_filter_data = array('type_id'=>$attendance_type);
     $attendance_type_name = i_get_attendance_type($attendance_type_filter_data);


       switch($attendance_type)
       {
       case ATTENDANCE_TYPE_PRESENT:
       $total_present++;
       $holiday_filter_data = array("date_start"=>$attendance_list_data[$count]["hr_attendance_date"],"date_end"=>$attendance_list_data[$count]["hr_attendance_date"],"status"=>'1');
       $holiday_list = i_get_holiday($holiday_filter_data);
       if(date("l",strtotime($attendance_list_data[$count]["hr_attendance_date"])) == get_day($attendance_list_data[$count]["hr_employee_week_off"]))
       {
        //  echo "Worked on a Week Off";
       }
       else if($holiday_list["status"] == SUCCESS)
       {
        //  echo "Worked on Public Holiday (".$holiday_list["data"][0]["hr_holiday_name"].")";
       }
       else
       {
        //  echo $attendance_type_name['data'][0]["hr_attendance_type_name"];
       }
       break;

       case ATTENDANCE_TYPE_HALFDAY:
       $total_half_days++;
       $holiday_filter_data = array("date_start"=>$attendance_list_data[$count]["hr_attendance_date"],"date_end"=>$attendance_list_data[$count]["hr_attendance_date"],"status"=>'1');
       $holiday_list = i_get_holiday($holiday_filter_data);
       if(date("l",strtotime($attendance_list_data[$count]["hr_attendance_date"])) == get_day($attendance_list_data[$count]["hr_employee_week_off"]))
       {
         if($total_working_hours['mins'] >= 510)
         {
          //  echo "Worked on a Week Off";
           $week_off = $week_off + 1;
         }
         else
         {
          //  echo "Half Day Worked on a Week Off";
           $week_off = $week_off + 0.5;
         }
       }
       else if($holiday_list["status"] == SUCCESS)
       {
        //  echo "Half Day Worked on Public Holiday (".$holiday_list["data"][0]["hr_holiday_name"].")";
         $public_holiday = $public_holiday + 0.5;
       }
       else
       {
         $leave_filter_data = array("employee_id"=>$attendance_list_data[$count]["hr_employee_id"],"date"=>$attendance_list_data[$count]["hr_attendance_date"],"status"=>LEAVE_STATUS_APPROVED);
         $leave_sresult = i_get_leave_list($leave_filter_data);
         if($leave_sresult["status"] == SUCCESS)
         {
          //  echo $leave_sresult["data"][0]["hr_attendance_type_name"]." ".$attendance_type_name['data'][0]["hr_attendance_type_name"];;

           switch($leave_sresult["data"][0]["hr_absence_type"])
           {
           case LEAVE_TYPE_EARNED:
           $el_count = $el_count + 0.5;
           break;

           case LEAVE_TYPE_SICK:
           $sl_count = $sl_count + 0.5;
           break;

           case LEAVE_TYPE_CASUAL:
           $cl_count = $cl_count + 0.5;
           break;

           case LEAVE_TYPE_COMP_OFF:
           $col_count = $col_count + 0.5;
           break;
           }
         }
         else
         {
           // echo $attendance_type_name['data'][0]["hr_attendance_type_name"];
         }
       }
       break;

       case ATTENDANCE_TYPE_ABSENT:
       $holiday_filter_data = array("date_start"=>$attendance_list_data[$count]["hr_attendance_date"],"date_end"=>$attendance_list_data[$count]["hr_attendance_date"],"status"=>'1');
       $holiday_list = i_get_holiday($holiday_filter_data);
       if(date("l",strtotime($attendance_list_data[$count]["hr_attendance_date"])) == get_day($attendance_list_data[$count]["hr_employee_week_off"]))
       {
        //  echo "Weekly Off";
         $week_off++;
       }
       else if($holiday_list["status"] == SUCCESS)
       {
        //  echo "Holiday (".$holiday_list["data"][0]["hr_holiday_name"].")";
         $public_holiday++;
       }
       else
       {
         $leave_filter_data = array("employee_id"=>$attendance_list_data[$count]["hr_employee_id"],"date"=>$attendance_list_data[$count]["hr_attendance_date"],"status"=>LEAVE_STATUS_APPROVED);
         $leave_sresult = i_get_leave_list($leave_filter_data);
         if($leave_sresult["status"] == SUCCESS)
         {
           for($lt_count = 0; $lt_count < count($leave_sresult['data']); $lt_count++)
           {
             if($lt_count == 0)
             {
              //  echo $leave_sresult["data"][$lt_count]["hr_attendance_type_name"];
             }
             else
             {
              //  echo ' '.$leave_sresult["data"][$lt_count]["hr_attendance_type_name"];
             }

             switch($leave_sresult["data"][$lt_count]["hr_absence_type"])
             {
             case LEAVE_TYPE_EARNED:
             if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_PRESENT)
             {
               $el_count++;
             }
             else if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_HALFDAY)
             {
               $el_count = $el_count + 0.5;
              //  echo " Half Day";
             }
             break;

             case LEAVE_TYPE_SICK:
             if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_PRESENT)
             {
               $sl_count++;
             }
             else if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_HALFDAY)
             {
               $sl_count = $sl_count + 0.5;
              //  echo " Half Day";
             }
             break;

             case LEAVE_TYPE_CASUAL:
             if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_PRESENT)
             {
               $cl_count++;
             }
             else if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_HALFDAY)
             {
               $cl_count = $cl_count + 0.5;
              //  echo " Half Day";
             }
             break;

             case LEAVE_TYPE_COMP_OFF:
             if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_PRESENT)
             {
               $col_count++;
             }
             else if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_HALFDAY)
             {
               $col_count = $col_count + 0.5;
              //  echo " Half Day";
             }
             break;
             }
           }
         }
         else
         {
          //  echo $attendance_type_name['data'][0]["hr_attendance_type_name"];
           $total_absent++;
         }
       }
       break;
       }
     }
    }
    else
    {
    }

    // var_dump($attendance_sresult['data']);
    $total_payable = $total_present + ($total_half_days*0.5) + $week_off + $public_holiday + $el_count + $sl_count + $col_count;
    echo json_encode($total_payable);
} else {
    header("location:login.php");
}
