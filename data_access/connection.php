<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'db_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');

date_default_timezone_set("Asia/Kolkata");

// database connection configuration
function get_conn_handle()
{
    $db     = DATABASE_NAME;
    $dbhost = DATABASE_HOST;
    $dbuser = DATABASE_USERNAME;
    $dbpass = DATABASE_PASSWORD;

    try {
        $db_conn_handle = new PDO('mysql:host='.$dbhost.';dbname='.$db, $dbuser, $dbpass);
    } catch (PDOException $e) {
        $db_conn_handle = DB_CONN_FAILURE;
        $error = $e->getMessage();
    }
    return $db_conn_handle;
}
?>
