<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/
$_SESSION['module'] = 'PM Masters';
/* DEFINES - START */
define('PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID', '359');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if ((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != "")) {
    // Session Data
    $user 		   = $_SESSION["loggedin_user"];
    $role 		   = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    // Get permission settings for this user for this page
    $view_perms_list   = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '2', '1');
    $edit_perms_list   = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '3', '1');
    $delete_perms_list = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '4', '1');
    $add_perms_list    = i_get_user_perms($user, '', PROJECT_MASTER_PROJECT_PROCESS_FUNC_ID, '1', '1');

    // Get Already added process
    $project_process_master_search_data = array("active"=>'1');
    $project_process_master_list = i_get_project_process_master($project_process_master_search_data);
    $project_process_master_list_data = array();
    if ($project_process_master_list["status"] == SUCCESS) {
        $project_process_master_list_data = $project_process_master_list["data"];
    }

    // Get Project Management Master modes already added
    $project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
    $project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
    $project_management_master_list_data = array();
    if ($project_management_master_list['status'] == SUCCESS) {
        $project_management_master_list_data = $project_management_master_list['data'];
    }
} else {
    header("location:login.php");
}
?>

<html>
<head>
    <meta charset="utf-8">
    <title>Project Management - Project Task Planning List</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/jquery-2.2.4.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/datatables.min.js"></script>
    <script type="text/javascript" src="./js_devel/datatables-1.10.16/moment.min.js"></script>
    <script src="datatable/project_task_planning_list.js?<?php echo time();?>"></script>
    <!-- <script src="moment.js"></script> -->
    <link href="./css/style.css?<?php echo time();?>" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/datatables.min.css" rel="stylesheet">
    <link href="./js_devel/datatables-1.10.16/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="./bootstrap_aku.min.css" rel="stylesheet">
</head>
<body>
    <?php
      include_once($base . DIRECTORY_SEPARATOR . 'kns' . DIRECTORY_SEPARATOR . 'Legal' . DIRECTORY_SEPARATOR . 'users' . DIRECTORY_SEPARATOR . 'menu_header.php');?>
       <div class="main margin-top">
            <div class="main-inner">
                <div class="container">
                    <div class="row">
                        <div class="widget widget-table action-table">
                            <div class="widget-header">
                                <h3>Project Management - Project Task Planning List</h3>
                            </div><div class="widget-header widget-toolbar">
                              <form method="get" class="form-inline">
                                <select name="project_id" id="project_id" class="form-control">
                                 <option value="">- - Select Project - -</option>
                              <?php
                                for ($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++) {
                                    ?>
                                 <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>">
                                    <?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?>
                                 </option>
                              <?php
                                } ?>
                              </select>
                              <select name="process_id" id="process_id" class="form-control">
                               <option value="">- - Select Process - -</option>
                               <?php
                                     for ($process_count = 0; $process_count < count($project_process_master_list_data); $process_count++) {
                                         ?>
                       			  <option value="<?php echo $project_process_master_list_data[$process_count]["project_process_master_id"]; ?>">
                                <?php echo $project_process_master_list_data[$process_count]["project_process_master_name"]; ?></option>
                       			  <?php
                                     }
                                     ?>
                            </select>
                            <input type="date" id="start_date" name="start_date" value="<?php echo $start_date ;?>" class="form-control"/>
                            <input type="date" id="end_date" name="end_date" value="<?php echo $end_date ;?>" class="form-control"/>
                              <button type="button" class="btn btn-primary" onclick="redrawTable();">Submit</button>
                              </form>
                            </div>
                            <div class="widget-content">
                            <table class="table table-striped table-bordered display nowrap" id="example" width="100%">
                                <thead>
                                  <tr>
                                     <th colspan="6">&nbsp;</th>
                                     <th colspan="2" class="center">Measurement</th>
                                     <th colspan="3" class="center">Plan Budget</th>
                                     <th colspan="3" class="center">Actual Budget</th>
                                     <th colspan="8" class="center">&nbsp;</th>
                                  </tr>
                                  <tr>
                                    <th>#</th>
                                    <th>Project</th>
                                    <th>Process</th>
                                    <th>Task Name</th>
                                    <th>Road Name</th>
                                    <th>UOM</th>
                                    <th>Plan</th>
                                    <th>Actual</th>
                                    <th>Man Power</th>
                                    <th>Machine</th>
                                    <th>Contract</th>
                                    <th>Man Power</th>
                                    <th>Machine</th>
                                    <th>Contract</th>
                                    <th>Planned By</th>
                                    <th>Planned On</th>
                                    <th>Total Days</th>
                                    <th>P Start Date</th>
                                    <th>P End Date</th>
                                    <th>Plan</th>
                                    <th>Actuals</th>
                                    <th>Pause</th>
                                 </tr>
                                </thead>
                                </tbody>
                            </table>
                            <!-- Modal for Update Actual -->
                            <div class="modal fade" id="myModal1" tabindex="-1" role="dialog">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Update Actual Details</h4>
                                  </div>
                                  <div class="modal-body">
                                    <input type="hidden" id="row_id" >
                                  <table class="table table-bordered" id="actuals_table_released">
                                  <tr>
                                   <td><a style="padding-right:10px" href="#" onclick="return go_to_project_task_actual();">Add Actual Man Power</a></td>
                                   <td><a style="padding-right:10px" href="#" onclick="return go_to_project_task_actual_machine_planning();">Add Actual Machine</a></td>
                                   <td><a style="padding-right:10px" href="#" onclick="return go_to_project_task_boq_actuals();">Add Contract Work</a></td>
                                   </tr>
                                   </table>
                                  </div>
                                </div><!-- /.modal-content -->
                              </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->

                            <!-- Alert Message -->
                            <div class="modal fade bs-example-modal-sm" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                              <div class="modal-dialog modal-sm" role="document">
                                <div class="modal-content">
                                  <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <div id="modalContent" style="display:none;color:red;font-weight: bold;font-size: large;"></div>
                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                        <!-- widget-content -->
                    </div>
                </div>
            </div>
        </div>
</body>
</html>
