<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE        : project_actual_contract_payment_list.php
CREATED ON    : 09-May-2017
CREATED BY    : Ashwini
PURPOSE     : List of project for actual contract payment
*/

/*
TBD:
*/

/* DEFINES - START */
define('PROJECT_MACHINE_ACCEPT_PAYMENT_FUNC_ID','277');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');


if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
    // Session Data
    $user            = $_SESSION["loggedin_user"];
    $role            = $_SESSION["loggedin_role"];
    $loggedin_name = $_SESSION["loggedin_user_name"];

    $alert_type = -1;
    $alert = "";

    // Get permission settings for this user for this page
    $view_perms_list   = i_get_user_perms($user,'',PROJECT_MACHINE_ACCEPT_PAYMENT_FUNC_ID,'2','1');
    $edit_perms_list   = i_get_user_perms($user,'',PROJECT_MACHINE_ACCEPT_PAYMENT_FUNC_ID,'3','1');
    $delete_perms_list = i_get_user_perms($user,'',PROJECT_MACHINE_ACCEPT_PAYMENT_FUNC_ID,'4','1');
    $add_perms_list    = i_get_user_perms($user,'',PROJECT_MACHINE_ACCEPT_PAYMENT_FUNC_ID,'1','1');

    // Query String Data
    // Nothing


     $search_machine_vendor  = $_GET["search_vendor"];
    // Get Project Actual Machine Payment modes already added
    $project_actual_machine_payment_search_data = array("active"=>'1',"status"=>"Accepted","vendor_id"=>$search_machine_vendor);
    $project_actual_machine_payment_list = i_get_project_payment_machine($project_actual_machine_payment_search_data);
    if($project_actual_machine_payment_list['status'] == SUCCESS)
    {
        $project_actual_machine_payment_list_data = $project_actual_machine_payment_list['data'];
    }
    else
    {
        $alert = $alert."Alert: ".$project_actual_machine_payment_list["data"];
    }

    // Machine Vendor data
    $project_machine_vendor_search_data = array("active"=>'1');
    $project_machine_vendor_list = i_get_project_machine_vendor_master_list($project_machine_vendor_search_data);
    if($project_machine_vendor_list["status"] == SUCCESS)
    {
        $project_machine_vendor_list_data = $project_machine_vendor_list["data"];
    }
    else
    {
        $alert = $alert."Alert: ".$project_machine_vendor_list["data"];
    }

    // Project data
    $project_management_master_search_data = array("active"=>'1', "user_id"=>$user);
    $project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
    if($project_management_master_list["status"] == SUCCESS)
    {
        $project_management_master_list_data = $project_management_master_list["data"];
    }
    else
    {
        $alert = $alert."Alert: ".$project_management_master_list["data"];
    }
}
else
{
    header("location:login.php");
}


                    if($project_actual_machine_payment_list["status"] == SUCCESS)
                {
                    $sl_no = 0;
                    $total_issued_amount = 0;
                    $total_deduction = 0;
                    $total_balance = 0;
                    $total_amount = 0;  $issued_amount = 0;
                    for($count = 0; $count < count($project_actual_machine_payment_list_data); $count++)
                    {
                        //Get Delay
                        $start_date = date("Y-m-d");
                        $end_date = $project_actual_machine_payment_list_data[$count]["project_payment_machine_accepted_on"];
                        $delay = get_date_diff($end_date,$start_date);

                        //Get total amount
                       // $amount = $project_actual_machine_payment_list_data[$count]["project_payment_machine_amount"];
                        $amount_before_tds = $project_actual_machine_payment_list_data[$count]["project_payment_machine_amount"];
						$manpower_tds = $project_actual_machine_payment_list_data[$count]["project_payment_machine_tds"];
						$tds_amount = ($manpower_tds/100) * $amount_before_tds;
						$amount = $amount_before_tds - $tds_amount;

                        //Get Project Machine Vendor master List

                        $deduction = 0;
                        $project_machine_issue_payment_search_data = array("active"=>'1',"machine_id"=>$project_actual_machine_payment_list_data[$count]["project_payment_machine_id"]);
                        $project_machine_issue_payment_list = i_get_project_machine_issue_payment($project_machine_issue_payment_search_data);
                        if($project_machine_issue_payment_list["status"] == SUCCESS)
                        {
                            $project_machine_issue_payment_list_data = $project_machine_issue_payment_list["data"];
                            for($issue_count = 0 ; $issue_count < count($project_machine_issue_payment_list_data) ; $issue_count++)
                            {
                                $issued_amount = $issued_amount + $project_machine_issue_payment_list_data[$issue_count]["project_machine_issue_payment_amount"];
                                $total_issued_amount = $total_issued_amount + $project_machine_issue_payment_list_data[$issue_count]["project_machine_issue_payment_amount"];
                            }
                        }
                        else
                        {
                            $issued_amount = 0;
                        }
                        $balance_amount = ($amount - $issued_amount);

                        // Get machine payment mapping for project name
                        $project_payment_machine_mapping_search_data = array("payment_id"=>$project_actual_machine_payment_list_data[$count]["project_payment_machine_id"]);
                        $project_name_sresult = i_get_project_payment_machine_mapping($project_payment_machine_mapping_search_data);
                        if($project_name_sresult['status'] == SUCCESS)
                        {
                            $actual_machine_plan_search_data = array('plan_id'=>$project_name_sresult['data'][0]['project_payment_machine_mapping_machine_actuals_id']);
                            $project_name_data = i_get_machine_planning_list($actual_machine_plan_search_data);
                            $project_name = $project_name_data['data'][0]['project_master_name'];
                            $project_id   = $project_name_data['data'][0]['project_management_master_id'];
                        }
                        else
                        {
                            $project_name = 'INVALID PROJECT';
                            $project_id   = '-1';
                        }

                        if(($search_machine_project == $project_id) || ($search_machine_project == ''))
                        {
                            if($balance_amount != 0)
                            {
                                $total_issued_amount = $total_issued_amount ;
                                $total_balance  = $total_balance + $balance_amount;
                                $total_amount = $total_amount + $amount;

                                $sl_no++;

                            }
                        }
                         $vendor = $project_actual_machine_payment_list_data[$count]["project_machine_vendor_master_name"];
                    }
                }



    ?>
                </tbody>
              </table>



                  <div class="widget ">

                      <div class="widget-header">
                          <i class="icon-user"></i>
                          <h3>Vendor : <?php echo $vendor ; ?>&nbsp;&nbsp;&nbsp;&nbsp;Total Amount : <?php echo round($total_amount) ; ?>&nbsp;&nbsp;&nbsp;&nbsp;Total Issued Amount : <?php echo round($total_issued_amount) ; ?>&nbsp;&nbsp;&nbsp;&nbsp;Deduction: <?php echo round($total_deduction) ; ?>&nbsp;&nbsp;&nbsp;&nbsp;Balance: <?php echo round($total_balance) ; ?> </h3>
                      </div> <!-- /widget-header -->

                    <div class="widget-content">



                        <div class="tabbable">

                            <div class="tab-content">
                                <div class="tab-pane active" id="formcontrols">
                                           <form id="add_project_machine_issue_payment_form" class="form-horizontal" method="post" action="project_add_machine_issue_payment.php">
                                <input type="hidden" value="<?php echo $machine_payment_id; ?>" name="hd_payment_id" />
                                <input type="hidden" value="<?php echo $search_machine_vendor; ?>" name="hd_machine_vendor" />
                                    <fieldset>

                                        <div class="control-group">
                                            <label class="control-label" for="amount">Amount</label>
                                            <div class="controls">
                                                <input type="number" class="span6" required name="amount" min="0.01" step="0.01" id="amount" onkeyup="return check_validity('<?php echo $total_amount ;?>','<?php echo $total_issued_amount ;?>');" placeholder="Amount">
                                            </div> <!-- /controls -->
                                        </div> <!-- /control-group -->

                                        <div class="control-group">
                                            <label class="control-label" for="ddl_mode">Payment Mode*</label>
                                            <div class="controls">
                                                 <select name="ddl_mode" required="required">
                                                    <option value="1">Online-NEFT</option>
                                                    <option value="2">Cheque</option>
                                                    <option value="3">Demand Draft</option>
                                                    <option value="4">Cash</option>
                                                    <option value="5">Other</option>
                                                    <option value="6">TDS (via challan)</option>
                                                    <option value="7">Online-RTGS</option>
                                                </select>
                                            </div> <!-- /controls -->
                                        </div> <!-- /control-group -->

                                        <div class="control-group">
                                            <label class="control-label" for="instrument_details">Instrument Details</label>
                                            <div class="controls">
                                                <input type="text" class="span6" required name="instrument_details" placeholder="Instrument Details">
                                            </div> <!-- /controls -->
                                        </div> <!-- /control-group -->

                                        <div class="control-group">
                                            <label class="control-label" for="txt_remarks">Remarks</label>
                                            <div class="controls">
                                                <input type="text" class="span6" required name="txt_remarks" placeholder="Remarks">
                                            </div> <!-- /controls -->
                                        </div> <!-- /control-group -->
                                        <br />


                                        <div class="form-actions">
                                            <input type="submit" class="btn btn-primary pull-right" name="add_project_machine_issue_payment_submit" value="Pay Now" />

                                        </div> <!-- /form-actions -->
                                    </fieldset>
                                </form>
                                </div>
                            </div>

                    </div> <!-- /widget-content -->

                </div> <!-- /widget -->





                          <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                  <tr>
                   <th>Project Name</th>
                    <th>Bill NO</th>
                    <th>Vendor Name</th>
                    <th>Amount</th>
                    <th>Payment Mode</th>
                    <th>Instrument</th>
                    <th>Remarks</th>
                    <th>Payment Date</th>
                    <th>Payment By</th>
                </tr>
                </thead>
                <tbody>
                <?php

                if($project_actual_machine_payment_list["status"] == SUCCESS)
                {
                    $sl_no = 0;
                    $total_issued_amount = 0;
                    $total_deduction = 0;
                    $total_balance = 0;
                    $total_amount = 0;
                    for($count = 0; $count < count($project_actual_machine_payment_list_data); $count++)
                    {
                        //Get Delay
                        $start_date = date("Y-m-d");
                        $end_date = $project_actual_machine_payment_list_data[$count]["project_payment_machine_accepted_on"];
                        $delay = get_date_diff($end_date,$start_date);

                        //Get total amount
                        $amount = $project_actual_machine_payment_list_data[$count]["project_payment_machine_amount"];


                        //Get Project Machine Vendor master List
                        $issued_amount = 0;
                        $deduction = 0;
                        $project_machine_issue_payment_search_data = array("active"=>'1',"machine_id"=>$project_actual_machine_payment_list_data[$count]["project_payment_machine_id"]);
                        $project_machine_issue_payment_list = i_get_project_machine_issue_payment($project_machine_issue_payment_search_data);
                        if($project_machine_issue_payment_list["status"] == SUCCESS)
                        {
                            $project_machine_issue_payment_list_data = $project_machine_issue_payment_list["data"];
                            for($issue_count = 0 ; $issue_count < count($project_machine_issue_payment_list_data) ; $issue_count++)
                            {
                                $issued_amount = $issued_amount + $project_machine_issue_payment_list_data[$issue_count]["project_machine_issue_payment_amount"];



                            $user_list = i_get_user_list($project_machine_issue_payment_list_data[$issue_count]["project_machine_issue_payment_added_by"],$user_name,$user_email,$user_role);


                            if($user_list["status"] == SUCCESS)
                            {
                                $user_list_data = $user_list["data"];
                            }

     // Get machine payment mapping for project name
                        $project_payment_machine_mapping_search_data = array("payment_id"=>$project_actual_machine_payment_list_data[$count]["project_payment_machine_id"]);
                        $project_name_sresult = i_get_project_payment_machine_mapping($project_payment_machine_mapping_search_data);
                        if($project_name_sresult['status'] == SUCCESS)
                        {
                            $actual_machine_plan_search_data = array('plan_id'=>$project_name_sresult['data'][0]['project_payment_machine_mapping_machine_actuals_id']);
                            $project_name_data = i_get_machine_planning_list($actual_machine_plan_search_data);
                            $project_name = $project_name_data['data'][0]['project_master_name'];
                            $project_id   = $project_name_data['data'][0]['project_management_master_id'];
                        }
                        else
                        {
                            $project_name = 'INVALID PROJECT';
                            $project_id   = '-1';
                        }
                                ?>
                                <tr>
                                <td><?php echo $project_name; ?></td>
                                <td><?php echo $project_actual_machine_payment_list_data[$count]["project_payment_machine_bill_no"]; ?></td>
                            <td><?php echo $project_actual_machine_payment_list_data[$count]["project_machine_vendor_master_name"]; ?></td>
                            <td><?php echo $project_machine_issue_payment_list_data[$issue_count]["project_machine_issue_payment_amount"]?></td>
                            <td><?php echo $project_machine_issue_payment_list_data[$issue_count]["payment_mode_name"]?></td>
                            <td><?php echo $project_machine_issue_payment_list_data[$issue_count]["project_machine_issue_payment_instrument_details"]?></td>
                            <td><?php echo $project_machine_issue_payment_list_data[$issue_count]["project_machine_issue_payment_remarks"]?></td>
                            <td><?php echo date('Y-M-d',strtotime($project_machine_issue_payment_list_data[$issue_count]["project_machine_issue_payment_added_on"]))?></td>
                            <td><?php echo $user_list_data[0]['user_name'];?></td>

                            </tr>
                                <?php


                            }
                        }
                    }
                }
                ?>


                </tbody>
              </table>


<script type="text/javascript">
    $("#add_project_machine_issue_payment_form").submit(function(e){


            e.preventDefault();
        $('#ajax_loading').show();

        formdata = new FormData($("#add_project_machine_issue_payment_form")[0]);

             $.ajax({
                     url: "tools/machine_payment_list/addMachineIssuePayment.php",
                     data: formdata,
                     processData: false,
                     contentType: false,
                     type: 'POST',
                     dataType:"JSON",
                     success: function(result){
                           location.reload();
                     }
            });
    });
</script>
    <script src="js/base.js"></script>
<script>

$("input[name*='amount'], input[name*='instrument_details']").on("keypress", function(evt) {
  var keycode = evt.charCode || evt.keyCode;
  if (keycode == 46 || event.which == 45 || event.which == 189) {
    $(this).val('');
        alert('please enter proper value');
    return false;
  }
});


function check_validity(total_amount,issued_amount)
{
    var amount = parseInt(document.getElementById('amount').value);
    var cur_amount = parseInt(amount);
    var total_amount  = parseInt(total_amount);
    var issued_amount      = parseInt(issued_amount);
    if((cur_amount + issued_amount) > total_amount)
    {
        document.getElementById('amount').value = 0;
        alert('Cannot release greater than total value');
    }
}
</script>
