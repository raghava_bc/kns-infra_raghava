<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Initialization
	$alert = "";
	$alert_type = -1; // No alert
	$assigned_to = "";

	// Query String
	if(isset($_GET["task"]))
	{
		$task_id = $_GET["task"];
	}	
	else
	{
		$task_id = "";
	}

	if(isset($_POST["assign_user_submit"]))
	{
		// Capture all form data
		$task_id      = $_POST["task_id"];
		$assigned_to  = $_POST["assigned_to"];
		
		if(($assigned_to != "") && ($task_id != ""))
		{
			$task_update_result = i_update_assignee($task_id,$assigned_to);
			
			if($task_update_result["status"] == SUCCESS)
			{
				$alert = "Assignee Successfully Changed";
				$alert_type = 1; // Success
				
				header("Location: general_task_summary.php");
			}
			else
			{
				$alert = "File Invalid!";
				$alert_type = 0; // Failure
			}
		}
		else	
		{
			$alert = "Please fill all the mandatory fields. Mandatory fields are marked with *";
			$alert_type = 0; // Failure
		}
	}
	
	// Get task details
	$task_list = i_get_gen_task_plan_list($task_id,'','','','','','','','');
	if($task_list["status"] == SUCCESS)
	{
		$task_list_data = $task_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$task_list["data"];
		$alert_type = 0; // Failure
	}

	// Get list of users
	$user_list = i_get_user_list('','','','');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Assign Task to User</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?> 

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Assign Task</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Assign Task to User</a>
						  </li>						  
						</ul>
						
						<br>
						    <div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="assign_task" class="form-horizontal" method="post" action="general_task_assign.php">
								<input type="hidden" name="task_id" value="<?php echo $task_id; ?>" />
									<fieldset>
										
										<div class="control-group">											
											<label class="control-label" for="process_type">Task</label>
											<div class="controls">
												<p class="help-block"><?php echo $task_list_data[0]["general_task_details"]; ?></p>												
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->																														<div class="control-group">											
											<label class="control-label" for="assigned_to">Assign To</label>
											<div class="controls">
												<select name="assigned_to">
												<?php 
												for($count = 0; $count < count($user_list_data); $count++)
												{
												?>
												<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php if($assigned_to == $user_list_data[$count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$count]["user_name"]; ?></option>												
												<?php
												}
												?>
												</select>
												<p class="help-block">The user to whom this task has to be assigned</p>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->										
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<button type="submit" class="btn btn-primary" name="assign_user_submit">Submit</button> 
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
