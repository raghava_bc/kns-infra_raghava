<?php
/* FILE HEADER - START */
// LAST UPDATED ON: 7th June 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* INTERFACES IN THIS FILE - START */
/*
1. i_add_user - To add a user
2. i_is_valid_credentials - To check whether a email - password combination is valid
3. i_change_password - To change the password of a user
4. i_forgot_password_request - To initiate a request for change in password since password was forgotten
*/
/* INTERFACES IN THIS FILE - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'security'.DIRECTORY_SEPARATOR.'security_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_mail.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_user.php');
/* INCLUDES - END */

/* INTERFACE FUNCTIONS - START */

// Author : Nitin Kashyap
// Purpose: To add a new user
// Input  : Name, Email, Unencrypted password, Department, Location, Role
// Output : User ID of the added user; DB_RECORD_ALREADY_EXISTS if the email ID already exists; FAILURE otherwise
function i_add_user($name,$email,$password,$role,$department,$location,$manager,$added_by)
{
	// Check if user already exists with this email ID
	$user_result = db_get_user_list('','',$email,'','','','');
	
	// Add user, if conditions satisfy
	if($user_result["status"] == DB_NO_RECORD)
	{		
		// Encrypt Password
		$enc_password = encrypt_password($password);
		
		// Add user to database
		$user_result = db_add_user($name,$email,$enc_password,$role,$department,$location,$manager,$added_by);
				
		if($user_result["status"] == SUCCESS)
		{
			$return["status"] = $user_result["status"];
			$return["data"]   = $user_result["data"];
			
			// Configure mail properties
			$to = $email;
			$cc = "";
			$subject = "User ID created for KNS Legal application";
			$message = "Hello $name <br /><br /> Please find below the login details for logging in into KNS Legal application: <br /><br />
			URL: http://demo.iweavesolutions.com/kns/Legal/login.php <br />
			Username: $email <br />
			Password: $password";
			$attachment = "";
			$mail_result = send_email($to,$cc,$subject,$message,$attachment);			
		}
		else
		{
			$return["status"] = FAILURE;
			$return["data"]   = "There was an internal error. Please try later";
		}	
	}	
	else if($user_result["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$return["status"] = DB_RECORD_ALREADY_EXISTS;
		$return["data"] = "A user with this email ID already exists. Please register with a different email ID";
	}
	else
	{
		$return["status"] = FAILURE;
		$return["data"] = "There was an internal error. Please try later";
	}
	
	return $return;
}

// Author : Nitin Kashyap
// Purpose: To validate whether the email-password combination is valid
// Input  : Email, Unencrypted password
// Output : Status; Data - user record if status is success or no data if any other status
function i_is_valid_credentials($email,$password)
{
	$enc_password = encrypt_password($password);
	
	$user_details = db_get_user_list('','',$email,'','','','');
	
	if($user_details["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($enc_password == $user_details["data"][0]["user_password"])
		{
			$return["status"] = SUCCESS;
			$return["data"] = $user_details["data"][0];
		}
		else
		{
			$return["status"] = INVALID_PASSWORD;
			$return["data"] = "Invalid Password. Please enter again";
		}
	}
	else if($user_details["status"] == DB_NO_RECORD)
	{
		$return["status"] = INVALID_USERNAME;
		$return["data"] = "Invalid Username. Please try again";
	}	
	else
	{
		$return["status"] = FAILURE;
		$return["data"] = "There was an internal error. Please try again later";
	}
	
	return $return;
}

// Author : Nitin Kashyap
// Purpose: To reset password of a user
// Input  : User ID, New Password
// Output : SUCCESS if password is successfully reset; DB_NO_RECORD if the user ID is invalid; INVALID_PASSWORD if the old password is not correct
function i_change_password($user_id,$new_pw)
{
	$user_details = db_get_user_details($user_id);
	
	if($user_details["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$pw_change_result = db_change_password($user_id,encrypt_password($new_pw));
		$return = $pw_change_result["status"];
	}
	else
	{
		$return = DB_NO_RECORD;
	}
	
	return $return;
}

// Author : Nitin Kashyap
// Purpose: To get the list of users
// Input  : User ID, User Name, Email, Role, Status, Department, Manager
// Output : User List
function i_get_user_list($user_id,$user_name,$user_email,$user_role,$user_active='',$user_department='',$user_manager='')
{
	$user_sresult = db_get_user_list($user_id,$user_name,$user_email,$user_role,'','',$user_active,$user_department,$user_manager);
	
	if($user_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$return["status"] = SUCCESS;
		$return["data"]   = $user_sresult["data"]; 
	}
	else
	{
		$return["status"] = DB_NO_RECORD;
		$return["data"]   = "There are no users with the provided details!";
	}
	
	return $return;
}

// Author : Nitin Kashyap
// Purpose: To get user role
// Input  : User Role
// Output : Role Name
function i_get_user_role($user_role)
{
	switch($user_role)
	{
		case "1":
		$role = "Admin";
		break;
		
		case "2":
		$role = "Legal HOD";
		break;
		
		case "3":
		$role = "Legal User";
		break;
		
		case "4":
		$role = "Other User";
		break;
		
		case "5":
		$role = "Sales HOD";
		break;
		
		case "6":
		$role = "CRM Telecaller";
		break;
		
		case "7":
		$role = "CRM Sales";
		break;
		
		case "8":
		$role = "CRM Channel Partner";
		break;
		
		case "9":
		$role = "Cab Manager";
		break;
		
		case "10":
		$role = "CRM HOD";
		break;
		
		case "11":
		$role = "CRM User";
		break;
		
		case "12":
		$role = "Finance Manager";
		break;
		
		case "13":
		$role = "HR Manager";
		break;
		
		default:
		$role = "Not Defined";
		break;
	}
	
	return $role;
}

// Author : Nitin Kashyap
// Purpose: To get active status
// Input  : ID
// Output : active status
function i_get_user_active_status($status)
{
	switch($status)
	{
		case "1":
		$status_name = "Active";
		break;
		
		case "0":
		$status_name = "Inactive";
		break;
		
		default:
		$status_name = "Not Defined";
		break;
	}
	
	return $status_name;
}

// Author : Nitin Kashyap
// Purpose: To enable/disable user
// Input  : ID, Action
// Output : User ID
function i_enable_disable_user($user_id,$action)
{
	$user_uresult = db_enable_disable_user($user_id,$action);
	
	return $user_uresult;
}

// Author : Nitin Kashyap
// Purpose: To edit user details
// Input  : ID, User Data Array
// Output : User ID
function i_edit_user($user_id,$user_data)
{
	$user_uresult = db_update_user_details($user_id,$user_data);
	
	if($user_uresult["status"] == SUCCESS)
	{
		$return["status"] = SUCCESS;
		$return["data"]   = "User Details updated successfully";
	}
	else
	{
		$return["status"] = FAILURE;
		$return["data"]   = "Internal Error. Please try again later";
	}
	
	return $return;
}

// Author : Sonakshi D
// Purpose: To add a new user login schedule
// Input  : User ID
// Output : User Login Schedule ID
function i_add_user_login_schedule($user_id)
{
	// Add user to database
	$user_login_result = db_add_login_schedule($user_id);
			
	if($user_login_result["status"] == SUCCESS)
	{
		$return["status"] = $user_login_result["status"];
		$return["data"]   = $user_login_result["data"];
	}
	else
	{
		$return["status"] = FAILURE;
		$return["data"]   = "There was an internal error. Please try later";
	}	

	return $return;
}

// Author : Sonakshi D
// Purpose: To get the list of user login schedule
// Input  : User ID, Start Date Time, End Date Time, IP Address
// Output : User Login Schedule List
function i_get_user_login_schedule($user_id,$start_date_time,$end_date_time,$ip_address)
{
	$user_sresult = db_get_login_schedule_list($user_id,$start_date_time,$end_date_time,$ip_address);
	
	if($user_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$return["status"] = SUCCESS;
		$return["data"]   = $user_sresult["data"]; 
	}
	else
	{
		$return["status"] = DB_NO_RECORD;
		$return["data"]   = "There are no users with the provided details!";
	}
	
	return $return;
}

/*
PURPOSE : To get master_function list
INPUT 	: Function, Department, URL, Added by, Start date, End date
OUTPUT 	: List of functionalities
BY 		: Punith
*/
function i_get_functionality_master($function,$department,$url,$active,$added_by)
{
	$functionality_master_result = db_get_functionality_master($function,$department,$url,$active,$added_by,'','');
	
	if($functionality_master_result['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $functionality_master_result["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No functionalities added yet!"; 
    }
	
	return $return;
}

// Author : Nitin Kashyap
// Purpose: To add a new user permission
// Input  : User ID, Functionality, CRUD type
// Output : Permission ID of the added permission
function i_add_user_perms($user_id,$functionality,$rights)
{
	// Check if user already exists with this email ID
	$user_perms_result = db_get_user_perms_list($user_id,'',$functionality,$rights,'1');
	
	// Add user, if conditions satisfy
	if($user_perms_result["status"] == DB_NO_RECORD)
	{				
		// Add user permissions to database
		$user_perms_result = db_add_user_permission($user_id,$functionality,$rights);
				
		if($user_perms_result["status"] == SUCCESS)
		{
			$return["status"] = $user_perms_result["status"];
			$return["data"]   = $user_perms_result["data"];			
		}
		else
		{
			$return["status"] = FAILURE;
			$return["data"]   = "There was an internal error. Please try later";
		}	
	}	
	else if($user_perms_result["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$return["status"] = DB_RECORD_ALREADY_EXISTS;
		$return["data"] = "User already has these permissions";
	}
	else
	{
		$return["status"] = FAILURE;
		$return["data"] = "There was an internal error. Please try later";
	}
	
	return $return;
}

// Author : Nitin Kashyap
// Purpose: To get user permissions
// Input  : User ID, Module, Functionality, CRUD Type, Active, Is Part of menu
// Output : Perms Data
function i_get_user_perms($user_id,$module,$functionality,$crud_type,$active,$is_menu='')
{
	$user_perms_details = db_get_user_perms_list($user_id,$module,$functionality,$crud_type,$active,$is_menu);
	
	if($user_perms_details["status"] == DB_RECORD_ALREADY_EXISTS)
	{		
		$return["status"] = SUCCESS;
		$return["data"]   = $user_perms_details["data"];
	}	
	else
	{
		$return["status"] = FAILURE;
		$return["data"] = "No permissions for this user for this functionality";
	}
	
	return $return;
}

// Author : Nitin Kashyap
// Purpose: To enable/disable permissions
// Input  : User ID, Functionality, CRUD Type, Action
// Output : User ID
function i_enable_disable_perms($user_id,$functionality,$crud_type,$active)
{
	$user_perms_uresult = db_enable_disable_perms($user_id,$functionality,$crud_type,$active);
	
	return $user_perms_uresult;
}/*PURPOSE : To get email masters listINPUT 	: Function, Department, Activity, Added by, Start date, End dateOUTPUT 	: List of functionalitiesBY 		: Punith*/function i_get_email_master($function,$department,$active,$added_by){	$email_functionality_master_result = db_get_email_functionality_master($function,$department,$active,$added_by,'','');		if($email_functionality_master_result['status'] == DB_RECORD_ALREADY_EXISTS)    {		$return["status"] = SUCCESS;        $return["data"]   = $email_functionality_master_result["data"];     }    else    {	    $return["status"] = FAILURE;        $return["data"]   = "No email alerts added yet!";     }		return $return;}// Author : Nitin Kashyap// Purpose: To add a new user permission for emails// Input  : User ID, Functionality, CRUD type// Output : Permission ID of the added permissionfunction i_add_user_email_perms($user_id,$functionality,$rights){	// Check if user already exists with this email ID	$user_perms_result = db_get_user_email_perms_list($user_id,'',$functionality,$rights,'1');		// Add user, if conditions satisfy	if($user_perms_result["status"] == DB_NO_RECORD)	{						// Add user email permissions to database		$user_perms_result = db_add_user_email_permission($user_id,$functionality,$rights);						if($user_perms_result["status"] == SUCCESS)		{			$return["status"] = $user_perms_result["status"];			$return["data"]   = $user_perms_result["data"];					}		else		{			$return["status"] = FAILURE;			$return["data"]   = "There was an internal error. Please try later";		}		}		else if($user_perms_result["status"] == DB_RECORD_ALREADY_EXISTS)	{		$return["status"] = DB_RECORD_ALREADY_EXISTS;		$return["data"] = "User already has these permissions";	}	else	{		$return["status"] = FAILURE;		$return["data"] = "There was an internal error. Please try later";	}		return $return;}// Author : Nitin Kashyap// Purpose: To get user email permissions// Input  : User ID, Module, Functionality, CRUD Type, Active, Is Part of menu// Output : Perms Datafunction i_get_user_email_perms($user_id,$module,$functionality,$crud_type,$active){	$user_perms_details = db_get_user_email_perms_list($user_id,$module,$functionality,$crud_type,$active);		if($user_perms_details["status"] == DB_RECORD_ALREADY_EXISTS)	{				$return["status"] = SUCCESS;		$return["data"]   = $user_perms_details["data"];	}		else	{		$return["status"] = FAILURE;		$return["data"] = "No permissions for this user for this functionality";	}		return $return;}// Author : Nitin Kashyap// Purpose: To enable/disable email permissions// Input  : User ID, Functionality, CRUD Type, Action// Output : User IDfunction i_enable_disable_email_perms($user_id,$functionality,$crud_type,$active){	$user_perms_uresult = db_enable_disable_email_perms($user_id,$functionality,$crud_type,$active);		return $user_perms_uresult;}
/* INTERFACE FUNCTIONS - END */
?>
