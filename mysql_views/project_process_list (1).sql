-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 22, 2018 at 01:38 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kns_erp_live`
--

-- --------------------------------------------------------

--
-- Structure for view `project_process_list`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `project_process_list` AS select `PMPM`.`project_management_master_id` AS `project_id`,`PMPM`.`project_master_name` AS `project_name`,`PPM`.`project_process_master_id` AS `process_master_id`,`PPM`.`project_process_master_name` AS `process_name`,`PPP`.`project_plan_process_id` AS `plan_process_id`,`PPP`.`project_plan_process_start_date` AS `start_date`,`PPP`.`project_plan_process_end_date` AS `end_date`,`PPM`.`project_process_master_order` AS `process_order` from (((`project_plan_process` `PPP` join `project_process_master` `PPM` on((`PPM`.`project_process_master_id` = `PPP`.`project_plan_process_name`))) join `project_plan` `PP` on((`PP`.`project_plan_id` = `PPP`.`project_plan_process_plan_id`))) join `project_management_project_master` `PMPM` on((`PMPM`.`project_management_master_id` = `PP`.`project_plan_project_id`)));

--
-- VIEW  `project_process_list`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
