<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$search  = $_POST["search"];
	// $machine_type_id  = $_POST["machine_type_id"];

	$project_machine_master_search_data = array("number"=>$search,"machine_name_check"=>'2',"active"=>'1');
	$machine_list_result = i_get_project_machine_master($project_machine_master_search_data);
	if($machine_list_result["status"] == SUCCESS)
	{
		$machine_list = '';

		for($count = 0; $count < count($machine_list_result['data']); $count++)
		{
			$machine_list = $machine_list.'<a style="display:block; width:100%; border-bottom:1px solid #efefef; text-decoration:none; cursor:pointer; padding:5px;" onclick="return select_machine(\''.$machine_list_result['data'][$count]['project_machine_master_id'].'\',\''.$machine_list_result['data'][$count]['project_machine_master_name'].'\',\''.$machine_list_result['data'][$count]['project_machine_master_id_number'].'\',\''.$machine_list_result['data'][$count]['project_machine_vendor_master_name'].'\');">'.$machine_list_result['data'][$count]['project_machine_master_name'].' '.$machine_list_result['data'][$count]['project_machine_master_id_number'].'</a>';
		}
	}
	else
	{
		$machine_list = 'FAILURE';
	}

	echo $machine_list;
}
else
{
	header("location:login.php");
}
?>
