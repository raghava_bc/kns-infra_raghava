function checkFileAttached(){
  $('#formImport').submit();
}

$(document).ready(function() {
  function createToolTip(str, length){
    var substr = (str.length <= 20)? str : str.substr(0, 20)+'...';
    return '<abbr data-toggle="tooltip" data-placement="right" title="'+str+'">'+substr+'</abbr>'
  }

  var table = $('#example').dataTable({
    stateSave: true,
    serverSide: true,
    pageLength: 10,
    scrollX: true,
    scrollY: 600,
    processing: true,
    scrollCollapse: true,
    fixedHeader: true,
    searchDelay: 1200,
    dataSrc: 'aaData',
    ajax: 'datatable/project_actual_material_datatable.php',
    fixedHeader: true,
    fnRowCallback: function(row, full, index){
      $(row).attr('id', 'row_'+index);
      // get the Indent qty
      $.ajax({
       url: 'ajax/stock_get_indent_qty.php',
       data: "indent_id=" + full['stock_issue_indent_id'] + "&material_id=" + full['stock_issue_item_material_id'],
       dataType: 'json',
       success: function(stock_response) {
         $('span#indent_qty_' + index).html(stock_response);
       }
       });
    },
    fnServerParams: function(aoData) {
      aoData.project_id = $('#project_id').val();
      aoData.requested_by = $('#requested_by').val();
      aoData.table = "issued_items_list";
      aoData.iDisplayLength = aoData.length;
      aoData.iDisplayStart = aoData.start;
      aoData.iDisplayStart = aoData.start;
      if (aoData.search && aoData.search.value) {
     aoData.sSearch = aoData.search.value;
   }
    },
    // fixedColumns: {
    //   leftColumns: 4
    // },
    drawCallback: function(){
      $('[data-toggle="tooltip"]').tooltip();
    },
    buttons: [{
     extend: 'colvis',
     columns: ':not(.noVis)'
   }],
    fixedColumns: {
      leftColumns: 3,
      rightColumns: 3
    },
    scrollX: true,
    columns: [{
        orderable: false,
        data: function(){
          return arguments[3].row + 1;
        }
      },
      {
        orderable: true,
        data: 'stock_project_name'
      },
      {
        orderable: true,
        data: 'process_name'
      },
      {
        orderable: true,
        data: 'task_name'
      },
      {
       orderable: false,
       data: function(data, type, full) {
         if (data.road_id == 'No Roads') {
           return 'No Roads';
         }
         return data.road_name;
       }
     },
      {
        orderable: true,
        data: 'stock_indent_no'
      },
      {
        orderable: true,
        data: 'stock_indent_added_on'
      },
      {
        orderable: true,
        data: 'indent_by'
      },

      {
        orderable: true,
        data: 'user_name'
      },
      {
        orderable: true,
        data: 'stock_material_name'
      },
      {
        orderable: true,
        data: 'stock_material_code'
      },
      {
        orderable: true,
        data: 'uom_name'
      },
      {
        orderable: true,
        data: 'stock_issue_item_qty'
      },
      {
        orderable: false,
        data: function() {
          return '<span id="indent_qty_' + arguments[3].row + '">loading..</span>';
        },
      },
      {
        orderable: true,
        data: 'stock_material_price'
      },
      {
        orderable: true,
        data: function(data) {
          return (data.stock_material_price * data.stock_issue_item_qty);
        },
      },
      {
        orderable: true,
        data: 'stock_machine_master_name'
      },
      {
        orderable: true,
        data: 'stock_machine_master_id_number'
      },
      {
        orderable: true,
        data: 'stock_issue_no'
      },
      {
        orderable: true,
        data: 'stock_issue_item_issued_on'
      },
      {
        orderable: false,
        data : function(data, type, full) {
          return '<a href="stock_issue_slip.php?indent_id='+data.stock_issue_indent_id+'&issue_id='+data.stock_issue_item_issue_id+' " target=_blank>  <span class="glyphicon glyphicon-print"></span></a>';
        }
      },
    ],
  });
});

// get hidden values saved from filters
function getVars(key) {
  var element = document.getElementById(key);
  if (element) {
    return element.value;
  } else {
    return '';
  }
}
