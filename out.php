<?php
session_start();

// Clear the session variables and destroy the session
session_unset(); 
session_destroy();

// Redirect to home page
header("Location:login.php");
?>