<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
 
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update Project Machine Vendor Master
	$manpower_id  = $_POST["manpower_id"];
	$action   	  = $_POST["action"];
	$accepted_by  = $user;
	$accepted_on  = date("Y-m-d H:i:s");
	$project_actual_payment_manpower_update_data = array("status"=>$action,"accepted_by"=>$accepted_by,"accepted_on"=>$accepted_on);
	$approve_payment_manpower_result = i_update_project_actual_payment_manpower($manpower_id,$project_actual_payment_manpower_update_data);
	
	if($approve_payment_manpower_result["status"] == FAILURE)
	{
		echo $approve_payment_manpower_result["data"];
	}
	else
	{
		echo "SUCCESS";
	}
}
else
{
	header("location:login.php");
}
?>