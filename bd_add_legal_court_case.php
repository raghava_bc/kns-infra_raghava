<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 10th Nov 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */$_SESSION['module'] = 'BD';

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_court_case_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type    = -1;
	$alert         = "";
	$survey_number = '';
	$village       = '';
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET['file']))
	{
		$legal_court_case_file_id = $_GET['file'];
		// Get File details
		$bd_file_data =  i_get_bd_files_list($legal_court_case_file_id,'','','','','','','');
		if($bd_file_data['status'] == SUCCESS)
		{
			$survey_number = $bd_file_data['data'][0]['bd_file_survey_no'];
			$village       = $bd_file_data['data'][0]['village_id'];
		}
	}
	else
	{
		$legal_court_case_file_id = '';
	}

	// Capture the form data
	if(isset($_POST["add_legal_court_case"]))
	{
		$legal_court_case_file_id 	= $_POST["hd_file_id"];		
		$legal_court_case_date		= $_POST["date_date"];
		$legal_court_case_type	    = $_POST["ddl_case_type"];
		$legal_court_case_date		= $_POST["date_date"];
		$case_number				= $_POST["num_case"];
		$survey_number				= $_POST["stxt_survey_no"];
		$village      				= $_POST["ddl_village"];
		$case_establishment			= $_POST["case_establishment"];
		$case_status				= $_POST["case_status"];
		$case_year					= $_POST["case_year"];
		$case_plaintiff				= $_POST["case_plaintiff"];
		$case_diffident				= $_POST["case_diffident"];
		$legal_court_case_details 	= $_POST["txt_remarks"];
		$legal_court_case_added_by	= $user;
		
		// Check for mandatory fields
		if($legal_court_case_type != "")
		{
			$legal_court_iresult = i_add_legal_court_case($legal_court_case_file_id,$legal_court_case_type,$legal_court_case_date,$case_number,$survey_number,$village,$case_establishment,$case_status,$case_year,$case_plaintiff,$case_diffident,$legal_court_case_details,$legal_court_case_added_by);
			
			if($legal_court_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				$alert      = "Court Case Details were successfully added";
			}
			else
			{
				$alert_type = 0;
				$alert      = $legal_court_iresult["data"];
			}						
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get list of court case types
	$court_case_type_list = i_get_court_case_type('','1');
	if($court_case_type_list['status'] == SUCCESS)
	{
		$court_case_type_list_data = $court_case_type_list['data'];
	}
	else
	{
		$alert_type = 0;
		$alert      = $alert.'Alert: No case type added <br />';
	}
	
	// Get list of Establishment
	$establishment_list = i_get_court_establishment('','','','','','');
	if($establishment_list["status"] == SUCCESS)
	{
		$establishment_list_data = $establishment_list["data"];
	}
	else
	{
		$alert      = $alert."Alert: ".$establishment_list["data"].'<br />';
		$alert_type = 0;
	}
	
	// Get list of Status
	$status_list = i_get_court_status_list('','');
	if($status_list["status"] == SUCCESS)
	{
		$status_list_data = $status_list["data"];
	}
	else
	{
		$alert      = $alert."Alert: ".$status_list["data"].'<br />';
		$alert_type = 0;
	}

	// Get list of villages
	$village_list = i_get_village_list('');
	if($village_list["status"] == SUCCESS)
	{
		$village_list_data = $village_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$village_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Legal Court Case</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Add Court Case</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Court Case</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_land_status" class="form-horizontal" method="post" action="bd_add_legal_court_case.php">
								<input type="hidden" name="hd_file_id" value="<?php echo $legal_court_case_file_id; ?>" />
									<fieldset>										
																				
										<div class="control-group">											
											<label class="control-label" for="ddl_case_type">Case Type *</label>
											<div class="controls">
												<select name="ddl_case_type" class="span6" required>
												<option value="">- - Select Court Case Type - -</option>												
												<?php
												for($count = 0; $count < count($court_case_type_list_data); $count++)
												{
												?>
												<option value="<?php echo $court_case_type_list_data[$count]['bd_court_case_type_master_id']; ?>"><?php echo $court_case_type_list_data[$count]['bd_court_case_type_master_type']; ?></option>												
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="num_case">Case Number</label>
											<div class="controls">
												<input type="text" class="span6" name="num_case" placeholder="Case Number" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_survey_no">Survey Number</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_survey_no" value="<?php echo $survey_number; ?>" placeholder="Survey Number for which the case is filed" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_village">Village</label>
											<div class="controls">
												<select name="ddl_village" class="span6" required>
												<option value="">- - Select Village - -</option>												
												<?php
												for($count = 0; $count < count($village_list_data); $count++)
												{
												?>
												<option value="<?php echo $village_list_data[$count]['village_id']; ?>" <?php if($village == $village_list_data[$count]['village_id']) {?> selected <?php } ?>><?php echo $village_list_data[$count]['village_name']; ?></option>												
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="case_establishment">Case Establishment *</label>
												<div class="controls">
											    <select name="case_establishment" required>
												<option value="">- - Select Case Establishment - -</option>
												<?php
												for($count = 0; $count < count($establishment_list_data); $count++)
												{
												?>
												<option value="<?php echo $establishment_list_data[$count]["bd_court_establishment_id"]; ?>"><?php echo $establishment_list_data[$count]["bd_court_establishment_name"]; ?></option>					
												<?php
												}
												?>
												</select>												
											</div> <!-- /controls -->									
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="case_status">Status *</label>
												<div class="controls">
											    <select name="case_status" required>
												<option value="">- - Select Current Case Status - -</option>
												<?php
												for($count = 0; $count < count($status_list_data); $count++)
												{
												?>
												<option value="<?php echo $status_list_data[$count]["bd_court_status_master_id"]; ?>"><?php echo $status_list_data[$count]["bd_court_status_name"]; ?></option>					
												<?php
												}
												?>
												</select>												
											</div> <!-- /controls -->									
										</div> <!-- /control-group -->
										
										<div class="control-group">																					
											<label class="control-label" for="case_year">Filing Year *</label>
											<div class="controls">
												<select name="case_year">
												<?php 
												$year = date("Y");
												for($y = date("Y"); $y >= 1900; $y--)
												{
												?>
												<option><?php echo $y ;?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
									  <div class="control-group">																					
											<label class="control-label" for="date_date">Filing Date *</label>
											<div class="controls">
												<input type="date" class="span6" name="date_date" required="required">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
																				
										<div class="control-group">	
											<label class="control-label" for="case_plaintiff">Plaintiff *</label>
											<div class="controls">											    
												<input type="text" class="span6" name="case_plaintiff" required="required">
											</div> <!-- /controls -->									
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="case_diffident">Defendant *</label>
										    <div class="controls">											
												<input type="text" class="span6" name="case_diffident" required="required">
											</div> <!-- /controls -->									
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Enter case details" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
                             			 <br />
										
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_legal_court_case" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						</div>
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>
