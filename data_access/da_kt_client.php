<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
PURPOSE : To add new KT Client Process Master
INPUT 	: Name, Remarks, Added By
OUTPUT 	: Process Master ID, success or failure message
BY 		: Lakshmi
*/
function db_add_kt_client_process_master($name,$remarks,$added_by)
{
	// Query
   $kt_client_process_master_iquery = "insert into kt_client_process_master (kt_client_process_master_name,kt_client_process_master_active,kt_client_process_master_remarks,kt_client_process_master_added_by,kt_client_process_master_added_on) values(:name,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $kt_client_process_master_istatement = $dbConnection->prepare($kt_client_process_master_iquery);
        
        // Data
        $kt_client_process_master_idata = array(':name'=>$name,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $kt_client_process_master_istatement->execute($kt_client_process_master_idata);
		$kt_client_process_master_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $kt_client_process_master_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get KT Client Process Master list
INPUT 	: Process Master ID, Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of kt Client Process Master
BY 		: Lakshmi
*/
function db_get_kt_client_process_master($kt_client_process_master_search_data)
{  
	if(array_key_exists("process_master_id",$kt_client_process_master_search_data))
	{
		$process_master_id = $kt_client_process_master_search_data["process_master_id"];
	}
	else
	{
		$process_master_id= "";
	}
	
	if(array_key_exists("name",$kt_client_process_master_search_data))
	{
		$name = $kt_client_process_master_search_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("process_name_check",$kt_client_process_master_search_data))
	{
		$process_name_check = $kt_client_process_master_search_data["process_name_check"];
	}
	else
	{
		$process_name_check = "";
	}
	
	if(array_key_exists("active",$kt_client_process_master_search_data))
	{
		$active = $kt_client_process_master_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$kt_client_process_master_search_data))
	{
		$added_by = $kt_client_process_master_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$kt_client_process_master_search_data))
	{
		$start_date= $kt_client_process_master_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$kt_client_process_master_search_data))
	{
		$end_date= $kt_client_process_master_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_kt_client_process_master_list_squery_base = "select * from kt_client_process_master KCPM inner join users U on U.user_id = KCPM.kt_client_process_master_added_by";
	
	$get_kt_client_process_master_list_squery_where = "";
	$get_kt_client_process_master_list_squery_order_by = " order by kt_client_process_master_name ASC";
	
	$filter_count = 0;
	
	// Data
	$get_kt_client_process_master_list_sdata = array();
	
	if($process_master_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_process_master_list_squery_where = $get_kt_client_process_master_list_squery_where." where kt_client_process_master_id = :process_master_id";
		}
		else
		{
			// Query
			$get_kt_client_process_master_list_squery_where = $get_kt_client_process_master_list_squery_where." and kt_client_process_master_id = :process_master_id";
		}
		
		// Data
		$get_kt_client_process_master_list_sdata[':process_master_id'] = $process_master_id;
		
		$filter_count++;
	}
	
	if($name != "")
	{
		if($filter_count == 0)
		{
		    // Query
			if($process_name_check == '1')
			{
				$get_kt_client_process_master_list_squery_where = $get_kt_client_process_master_list_squery_where." where kt_client_process_master_name = :name";		
				// Data
				$get_kt_client_process_master_list_sdata[':name']  = $name;
			}
			else if($process_name_check == '2')
			{
				$get_kt_client_process_master_list_squery_where = $get_kt_client_process_master_list_squery_where." where kt_client_process_master_name like :name";						

				// Data
				$get_kt_client_process_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_kt_client_process_master_list_squery_where = $get_kt_client_process_master_list_squery_where." where kt_client_process_master_name like :name";						

				// Data
				$get_kt_client_process_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}
		else
		{
			// Query
			if($process_name_check == '1')
			{
				$get_kt_client_process_master_list_squery_where = $get_kt_client_process_master_list_squery_where." and kt_client_process_master_name = :name";	
					
				// Data
				$get_kt_client_process_master_list_sdata[':name']  = $name;
			}
			else if($process_name_check == '2')
			{
				$get_kt_client_process_master_list_squery_where = $get_kt_client_process_master_list_squery_where." or kt_client_process_master_name like :name";				
				// Data
				$get_kt_client_process_master_list_sdata[':name']  = $name.'%';
			}
			else
			{
				$get_kt_client_process_master_list_squery_where = $get_kt_client_process_master_list_squery_where." and kt_client_process_master_name like :name";
				
				// Data
				$get_kt_client_process_master_list_sdata[':name']  = '%'.$name.'%';
			}
		}				
		
		$filter_count++;
	}
	

	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_process_master_list_squery_where = $get_kt_client_process_master_list_squery_where." where kt_client_process_master_active = :active";
		}
		else
		{
			// Query
			$get_kt_client_process_master_list_squery_where = $get_kt_client_process_master_list_squery_where." and kt_client_process_master_active = :active";
		}
		
		// Data
		$get_kt_client_process_master_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_process_master_list_squery_where = $get_kt_client_process_master_list_squery_where." where kt_client_process_master_added_by = :added_by";
		}
		else
		{
			// Query
			$get_kt_client_process_master_list_squery_where = $get_kt_client_process_master_list_squery_where." and kt_client_process_master_added_by = :added_by";
		}
		
		//Data
		$get_kt_client_process_master_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_process_master_list_squery_where = $get_kt_client_process_master_list_squery_where." where kt_client_process_master_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_kt_client_process_master_list_squery_where = $get_kt_client_process_master_list_squery_where." and kt_client_process_master_added_on >= :start_date";
		}
		
		//Data
		$get_kt_client_process_master_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_process_master_list_squery_where = $get_kt_client_process_master_list_squery_where." where kt_client_process_master_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_kt_client_process_master_list_squery_where = $get_kt_client_process_master_list_squery_where." and kt_client_process_master_added_on <= :end_date";
		}
		
		//Data
		$get_kt_client_process_master_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_kt_client_process_master_list_squery = $get_kt_client_process_master_list_squery_base.$get_kt_client_process_master_list_squery_where.$get_kt_client_process_master_list_squery_order_by;		
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_kt_client_process_master_list_sstatement = $dbConnection->prepare($get_kt_client_process_master_list_squery);
		
		$get_kt_client_process_master_list_sstatement -> execute($get_kt_client_process_master_list_sdata);
		
		$get_kt_client_process_master_list_sdetails = $get_kt_client_process_master_list_sstatement -> fetchAll();
		
		if(FALSE === $get_kt_client_process_master_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_kt_client_process_master_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_kt_client_process_master_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}
 
/*
PURPOSE : To update KT Client Process Master
INPUT 	: Process Master ID, KT Client Process Master Update Array
OUTPUT 	: Process Master ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_kt_client_process_master($process_master_id,$kt_client_process_master_update_data)
{
	if(array_key_exists("name",$kt_client_process_master_update_data))
	{	
		$name = $kt_client_process_master_update_data["name"];
	}
	else
	{
		$name = "";
	}
	
	if(array_key_exists("active",$kt_client_process_master_update_data))
	{	
		$active = $kt_client_process_master_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$kt_client_process_master_update_data))
	{	
		$remarks = $kt_client_process_master_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$kt_client_process_master_update_data))
	{	
		$added_by = $kt_client_process_master_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$kt_client_process_master_update_data))
	{	
		$added_on = $kt_client_process_master_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $kt_client_process_master_update_uquery_base = "update kt_client_process_master set ";  
	
	$kt_client_process_master_update_uquery_set = "";
	
	$kt_client_process_master_update_uquery_where = " where kt_client_process_master_id = :process_master_id";
	
	$kt_client_process_master_update_udata = array(":process_master_id"=>$process_master_id);
	
	$filter_count = 0;
	
	if($name != "")
	{
		$kt_client_process_master_update_uquery_set = $kt_client_process_master_update_uquery_set." kt_client_process_master_name = :name,";
		$kt_client_process_master_update_udata[":name"] = $name;
		$filter_count++;
	}
	
	if($active != "")
	{
		$kt_client_process_master_update_uquery_set = $kt_client_process_master_update_uquery_set." kt_client_process_master_active = :active,";
		$kt_client_process_master_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$kt_client_process_master_update_uquery_set = $kt_client_process_master_update_uquery_set." kt_client_process_master_remarks = :remarks,";
		$kt_client_process_master_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$kt_client_process_master_update_uquery_set = $kt_client_process_master_update_uquery_set." kt_client_process_master_added_by = :added_by,";
		$kt_client_process_master_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$kt_client_process_master_update_uquery_set = $kt_client_process_master_update_uquery_set." kt_client_process_master_added_on = :added_on,";
		$kt_client_process_master_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$kt_client_process_master_update_uquery_set = trim($kt_client_process_master_update_uquery_set,',');
	}
	
	$kt_client_process_master_update_uquery = $kt_client_process_master_update_uquery_base.$kt_client_process_master_update_uquery_set.$kt_client_process_master_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $kt_client_process_master_update_ustatement = $dbConnection->prepare($kt_client_process_master_update_uquery);		
        
        $kt_client_process_master_update_ustatement -> execute($kt_client_process_master_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $process_master_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new KT Client Request
INPUT 	: Client ID, Booking ID, Request Date, Requested By, Requested On, Remarks, Added By
OUTPUT 	: Request ID, success or failure message
BY 		: Lakshmi
*/
function db_add_kt_client_request($client_id,$booking_id,$request_date,$requested_by,$requested_on,$remarks,$added_by)
{
	// Query
   $kt_client_request_iquery = "insert into kt_client_request 
   (kt_client_request_client_id,kt_client_request_booking_id,kt_client_request_date,kt_client_requested_by,kt_client_requested_on,kt_client_request_active,
   kt_client_request_remarks,kt_client_request_added_by,kt_client_request_added_on)
   values(:client_id,:booking_id,:request_date,:requested_by,:requested_on,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $kt_client_request_istatement = $dbConnection->prepare($kt_client_request_iquery);
        
        // Data
        $kt_client_request_idata = array(':client_id'=>$client_id,':booking_id'=>$booking_id,':request_date'=>$request_date,':requested_by'=>$requested_by,
		':requested_on'=>$requested_on,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $kt_client_request_istatement->execute($kt_client_request_idata);
		$kt_client_request_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $kt_client_request_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get KT Client Request List
INPUT 	: Request ID, Client ID, Booking ID, Request Date, Requested By, Requested On, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of KT Client Request
BY 		: Lakshmi
*/
function db_get_kt_client_request($kt_client_request_search_data)
{  
	if(array_key_exists("request_id",$kt_client_request_search_data))
	{
		$request_id = $kt_client_request_search_data["request_id"];
	}
	else
	{
		$request_id= "";
	}
	
	if(array_key_exists("client_id",$kt_client_request_search_data))
	{
		$client_id = $kt_client_request_search_data["client_id"];
	}
	else
	{
		$client_id = "";
	}
	
	if(array_key_exists("booking_id",$kt_client_request_search_data))
	{
		$booking_id = $kt_client_request_search_data["booking_id"];
	}
	else
	{
		$booking_id = "";
	}
	
	if(array_key_exists("request_start_date",$kt_client_request_search_data))
	{
		$request_start_date = $kt_client_request_search_data["request_start_date"];
	}
	else
	{
		$request_start_date = "";
	}
	
	if(array_key_exists("request_end_date",$kt_client_request_search_data))
	{
		$request_end_date = $kt_client_request_search_data["request_end_date"];
	}
	else
	{
		$request_end_date = "";
	}
	
	
	if(array_key_exists("requested_by",$kt_client_request_search_data))
	{
		$requested_by = $kt_client_request_search_data["requested_by"];
	}
	else
	{
		$requested_by = "";
	}
	
	if(array_key_exists("requested_on",$kt_client_request_search_data))
	{
		$requested_on = $kt_client_request_search_data["requested_on"];
	}
	else
	{
		$requested_on = "";
	}
	
	if(array_key_exists("active",$kt_client_request_search_data))
	{
		$active = $kt_client_request_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$kt_client_request_search_data))
	{
		$added_by = $kt_client_request_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$kt_client_request_search_data))
	{
		$start_date= $kt_client_request_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$kt_client_request_search_data))
	{
		$end_date= $kt_client_request_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_kt_client_request_list_squery_base = "select * from kt_client_request KCR inner join users U on U.user_id=KCR.kt_client_request_added_by";
	
	$get_kt_client_request_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_kt_client_request_list_sdata = array();
	
	if($request_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_request_list_squery_where = $get_kt_client_request_list_squery_where." where kt_client_request_id = :request_id";								
		}
		else
		{
			// Query
			$get_kt_client_request_list_squery_where = $get_kt_client_request_list_squery_where." and kt_client_request_id = :request_id";				
		}
		
		// Data
		$get_kt_client_request_list_sdata[':request_id'] = $request_id;
		
		$filter_count++;
	}
	
	if($client_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_request_list_squery_where = $get_kt_client_request_list_squery_where." where kt_client_request_client_id = :client_id";								
		}
		else
		{
			// Query
			$get_kt_client_request_list_squery_where = $get_kt_client_request_list_squery_where." and kt_client_request_client_id = :client_id";				
		}
		
		// Data
		$get_kt_client_request_list_sdata[':client_id'] = $client_id;
		
		$filter_count++;
	}
	
	if($booking_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_request_list_squery_where = $get_kt_client_request_list_squery_where." where kt_client_request_booking_id = :booking_id";								
		}
		else
		{
			// Query
			$get_kt_client_request_list_squery_where = $get_kt_client_request_list_squery_where." and kt_client_request_booking_id = :booking_id";				
		}
		
		// Data
		$get_kt_client_request_list_sdata[':booking_id']  = $booking_id;
		
		$filter_count++;
	}
	
	if($request_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_request_list_squery_where = $get_kt_client_request_list_squery_where." where kt_client_request_date >= :request_start_date";								
		}
		else
		{
			// Query
			$get_kt_client_request_list_squery_where = $get_kt_client_request_list_squery_where." and kt_client_request_date >= :request_start_date";				
		}
		
		// Data
		$get_kt_client_request_list_sdata[':request_start_date']  = $request_start_date;
		
		$filter_count++;
	}
	
	if($request_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_request_list_squery_where = $get_kt_client_request_list_squery_where." where kt_client_request_date <= :request_end_date";								
		}
		else
		{
			// Query
			$get_kt_client_request_list_squery_where = $get_kt_client_request_list_squery_where." and kt_client_request_date <= :request_end_date";				
		}
		
		// Data
		$get_kt_client_request_list_sdata[':request_end_date']  = $request_end_date;
		
		$filter_count++;
	}
	
	if($requested_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_request_list_squery_where = $get_kt_client_request_list_squery_where." where kt_client_requested_by = :requested_by";								
		}
		else
		{
			// Query
			$get_kt_client_request_list_squery_where = $get_kt_client_request_list_squery_where." and kt_client_requested_by = :requested_by";				
		}
		
		// Data
		$get_kt_client_request_list_sdata[':requested_by']  = $requested_by;
		
		$filter_count++;
	}
	
	if($requested_on != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_request_list_squery_where = $get_kt_client_request_list_squery_where." where kt_client_requested_on = :requested_on";								
		}
		else
		{
			// Query
			$get_kt_client_request_list_squery_where = $get_kt_client_request_list_squery_where." and kt_client_requested_on = :requested_on";				
		}
		
		// Data
		$get_kt_client_request_list_sdata[':requested_on']  = $requested_on;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_request_list_squery_where = $get_kt_client_request_list_squery_where." where kt_client_request_active = :active";								
		}
		else
		{
			// Query
			$get_kt_client_request_list_squery_where = $get_kt_client_request_list_squery_where." and kt_client_request_active = :active";				
		}
		
		// Data
		$get_kt_client_request_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_request_list_squery_where = $get_kt_client_request_list_squery_where." where kt_client_request_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_kt_client_request_list_squery_where = $get_kt_client_request_list_squery_where." and kt_client_request_added_by = :added_by";
		}
		
		//Data
		$get_kt_client_request_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_request_list_squery_where = $get_kt_client_request_list_squery_where." where kt_client_request_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_kt_client_request_list_squery_where = $get_kt_client_request_list_squery_where." and kt_client_request_added_on >= :start_date";				
		}
		
		//Data
		$get_kt_client_request_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_request_list_squery_where = $get_kt_client_request_list_squery_where." where kt_client_request_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_kt_client_request_list_squery_where = $get_kt_client_request_list_squery_where." and kt_client_request_added_on <= :end_date";
		}
		
		//Data
		$get_kt_client_request_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_kt_client_request_list_squery = $get_kt_client_request_list_squery_base.$get_kt_client_request_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_kt_client_request_list_sstatement = $dbConnection->prepare($get_kt_client_request_list_squery);
		
		$get_kt_client_request_list_sstatement -> execute($get_kt_client_request_list_sdata);
		
		$get_kt_client_request_list_sdetails = $get_kt_client_request_list_sstatement -> fetchAll();
		
		if(FALSE === $get_kt_client_request_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_kt_client_request_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_kt_client_request_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update KT Client Request
INPUT 	: Request ID, KT Client Request Update Array
OUTPUT 	: Request ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_kt_client_request($request_id,$kt_client_request_update_data)
{
	if(array_key_exists("client_id",$kt_client_request_update_data))
	{	
		$client_id = $kt_client_request_update_data["client_id"];
	}
	else
	{
		$client_id = "";
	}
	
	if(array_key_exists("booking_id",$kt_client_request_update_data))
	{	
		$booking_id = $kt_client_request_update_data["booking_id"];
	}
	else
	{
		$booking_id = "";
	}
	
	if(array_key_exists("request_date",$kt_client_request_update_data))
	{	
		$request_date = $kt_client_request_update_data["request_date"];
	}
	else
	{
		$request_date = "";
	}
	
	if(array_key_exists("requested_by",$kt_client_request_update_data))
	{	
		$requested_by = $kt_client_request_update_data["requested_by"];
	}
	else
	{
		$requested_by = "";
	}
	
	if(array_key_exists("requested_on",$kt_client_request_update_data))
	{	
		$requested_on = $kt_client_request_update_data["requested_on"];
	}
	else
	{
		$requested_on = "";
	}
	
	if(array_key_exists("active",$kt_client_request_update_data))
	{	
		$active = $kt_client_request_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$kt_client_request_update_data))
	{	
		$remarks = $kt_client_request_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$kt_client_request_update_data))
	{	
		$added_by = $kt_client_request_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$kt_client_request_update_data))
	{	
		$added_on = $kt_client_request_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $kt_client_request_update_uquery_base = "update kt_client_request set";  
	
	$kt_client_request_update_uquery_set = "";
	
	$kt_client_request_update_uquery_where = " where kt_client_request_id = :request_id";
	
	$kt_client_request_update_udata = array(":request_id"=>$request_id);
	
	$filter_count = 0;
	
	if($client_id != "")
	{
		$kt_client_request_update_uquery_set = $kt_client_request_update_uquery_set." kt_client_request_client_id = :client_id,";
		$kt_client_request_update_udata[":client_id"] = $client_id;
		$filter_count++;
	}
	
	if($booking_id != "")
	{
		$kt_client_request_update_uquery_set = $kt_client_request_update_uquery_set." kt_client_request_booking_id = :booking_id,";
		$kt_client_request_update_udata[":booking_id"] = $booking_id;
		$filter_count++;
	}
	
	if($request_date != "")
	{
		$kt_client_request_update_uquery_set = $kt_client_request_update_uquery_set." kt_client_request_date = :request_date,";
		$kt_client_request_update_udata[":request_date"] = $request_date;
		$filter_count++;
	}
	
	if($requested_by != "")
	{
		$kt_client_request_update_uquery_set = $kt_client_request_update_uquery_set." kt_client_requested_by = :requested_by,";
		$kt_client_request_update_udata[":requested_by"] = $requested_by;
		$filter_count++;
	}
	
	if($requested_on != "")
	{
		$kt_client_request_update_uquery_set = $kt_client_request_update_uquery_set." kt_client_requested_on = :requested_on,";
		$kt_client_request_update_udata[":requested_on"] = $requested_on;
		$filter_count++;
	}
	
	if($active != "")
	{
		$kt_client_request_update_uquery_set = $kt_client_request_update_uquery_set." kt_client_request_active = :active,";
		$kt_client_request_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$kt_client_request_update_uquery_set = $kt_client_request_update_uquery_set." kt_client_request_remarks = :remarks,";
		$kt_client_request_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$kt_client_request_update_uquery_set = $kt_client_request_update_uquery_set." kt_client_request_added_by = :added_by,";
		$kt_client_request_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$kt_client_request_update_uquery_set = $kt_client_request_update_uquery_set." kt_client_request_added_on = :added_on,";
		$kt_client_request_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$kt_client_request_update_uquery_set = trim($kt_client_request_update_uquery_set,',');
	}
	
	$kt_client_request_update_uquery = $kt_client_request_update_uquery_base.$kt_client_request_update_uquery_set.$kt_client_request_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $kt_client_request_update_ustatement = $dbConnection->prepare($kt_client_request_update_uquery);		
        
        $kt_client_request_update_ustatement -> execute($kt_client_request_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $request_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new KT Client KT Process
INPUT 	: Process Master ID, Request ID, Process Start Date, Process End Date, Remarks, Added By
OUTPUT 	: Process ID, success or failure message
BY 		: Lakshmi
*/
function db_add_kt_client_kt_process($process_master_id,$request_id,$process_start_date,$process_end_date,$remarks,$added_by)
{
	// Query
   $kt_client_kt_process_iquery = "insert into kt_client_kt_process 
   (kt_client_kt_process_master_id,kt_client_kt_process_request_id,kt_client_kt_process_start_date,kt_client_kt_process_end_date,kt_client_kt_process_active,
   kt_client_kt_process_remarks,kt_client_kt_process_added_by,kt_client_kt_process_added_on)
   values(:process_master_id,:request_id,:process_start_date,:process_end_date,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $kt_client_kt_process_istatement = $dbConnection->prepare($kt_client_kt_process_iquery);
        
        // Data
        $kt_client_kt_process_idata = array(':process_master_id'=>$process_master_id,':request_id'=>$request_id,':process_start_date'=>$process_start_date,
		':process_end_date'=>$process_end_date,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $kt_client_kt_process_istatement->execute($kt_client_kt_process_idata);
		$kt_client_kt_process_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $kt_client_kt_process_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get KT Client KT Process List
INPUT 	: Process ID, Client ID, Process Master ID, Request ID, Process Start Date, Process End Date, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of KT Client KT Process
BY 		: Lakshmi
*/
function db_get_kt_client_kt_process($kt_client_kt_process_search_data)
{  
	if(array_key_exists("process_id",$kt_client_kt_process_search_data))
	{
		$process_id = $kt_client_kt_process_search_data["process_id"];
	}
	else
	{
		$process_id= "";
	}
	
	if(array_key_exists("process_master_id",$kt_client_kt_process_search_data))
	{
		$process_master_id = $kt_client_kt_process_search_data["process_master_id"];
	}
	else
	{
		$process_master_id = "";
	}
	
	if(array_key_exists("request_id",$kt_client_kt_process_search_data))
	{
		$request_id = $kt_client_kt_process_search_data["request_id"];
	}
	else
	{
		$request_id = "";
	}
	
	if(array_key_exists("process_start_date",$kt_client_kt_process_search_data))
	{
		$process_start_date = $kt_client_kt_process_search_data["process_start_date"];
	}
	else
	{
		$process_start_date = "";
	}
	
	if(array_key_exists("process_end_date",$kt_client_kt_process_search_data))
	{
		$process_end_date = $kt_client_kt_process_search_data["process_end_date"];
	}
	else
	{
		$process_end_date = "";
	}
	
	if(array_key_exists("active",$kt_client_kt_process_search_data))
	{
		$active = $kt_client_kt_process_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$kt_client_kt_process_search_data))
	{
		$added_by = $kt_client_kt_process_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$kt_client_kt_process_search_data))
	{
		$start_date= $kt_client_kt_process_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$kt_client_kt_process_search_data))
	{
		$end_date= $kt_client_kt_process_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_kt_client_kt_process_list_squery_base = "select * from kt_client_kt_process KCKP inner join users U on U.user_id=KCKP.kt_client_kt_process_added_by inner join kt_client_process_master KCPM on KCPM.kt_client_process_master_id=KCKP.kt_client_kt_process_master_id inner join kt_client_request KCR on KCR.kt_client_request_id=KCKP.kt_client_kt_process_request_id";
	
	$get_kt_client_kt_process_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_kt_client_kt_process_list_sdata = array();
	
	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_kt_process_list_squery_where = $get_kt_client_kt_process_list_squery_where." where kt_client_kt_process_id = :process_id";								
		}
		else
		{
			// Query
			$get_kt_client_kt_process_list_squery_where = $get_kt_client_kt_process_list_squery_where." and kt_client_kt_process_id = :process_id";				
		}
		
		// Data
		$get_kt_client_kt_process_list_sdata[':process_id'] = $process_id;
		
		$filter_count++;
	}
	
	if($process_master_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_kt_process_list_squery_where = $get_kt_client_kt_process_list_squery_where." where kt_client_kt_process_master_id = :process_master_id";								
		}
		else
		{
			// Query
			$get_kt_client_kt_process_list_squery_where = $get_kt_client_kt_process_list_squery_where." and kt_client_kt_process_master_id = :process_master_id";				
		}
		
		// Data
		$get_kt_client_kt_process_list_sdata[':process_master_id'] = $process_master_id;
		
		$filter_count++;
	}
	
	if($request_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_kt_process_list_squery_where = $get_kt_client_kt_process_list_squery_where." where kt_client_kt_process_request_id = :request_id";								
		}
		else
		{
			// Query
			$get_kt_client_kt_process_list_squery_where = $get_kt_client_kt_process_list_squery_where." and kt_client_kt_process_request_id = :request_id";				
		}
		
		// Data
		$get_kt_client_kt_process_list_sdata[':request_id']  = $request_id;
		
		$filter_count++;
	}
	
	if($process_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_kt_process_list_squery_where = $get_kt_client_kt_process_list_squery_where." where kt_client_kt_process_start_date = :process_start_date";								
		}
		else
		{
			// Query
			$get_kt_client_kt_process_list_squery_where = $get_kt_client_kt_process_list_squery_where." and kt_client_kt_process_start_date = :process_start_date";				
		}
		
		// Data
		$get_kt_client_kt_process_list_sdata[':process_start_date']  = $process_start_date;
		
		$filter_count++;
	}
	
	if($process_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_kt_process_list_squery_where = $get_kt_client_kt_process_list_squery_where." where kt_client_kt_process_end_date = :process_end_date";								
		}
		else
		{
			// Query
			$get_kt_client_kt_process_list_squery_where = $get_kt_client_kt_process_list_squery_where." and kt_client_kt_process_end_date = :process_end_date";				
		}
		
		// Data
		$get_kt_client_kt_process_list_sdata[':process_end_date']  = $process_end_date;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_kt_process_list_squery_where = $get_kt_client_kt_process_list_squery_where." where kt_client_kt_process_active = :active";								
		}
		else
		{
			// Query
			$get_kt_client_kt_process_list_squery_where = $get_kt_client_kt_process_list_squery_where." and kt_client_kt_process_active = :active";				
		}
		
		// Data
		$get_kt_client_kt_process_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_kt_process_list_squery_where = $get_kt_client_kt_process_list_squery_where." where kt_client_kt_process_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_kt_client_kt_process_list_squery_where = $get_kt_client_kt_process_list_squery_where." and kt_client_kt_process_added_by = :added_by";
		}
		
		//Data
		$get_kt_client_kt_process_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_kt_process_list_squery_where = $get_kt_client_kt_process_list_squery_where." where kt_client_kt_process_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_kt_client_kt_process_list_squery_where = $get_kt_client_kt_process_list_squery_where." and kt_client_kt_process_added_on >= :start_date";				
		}
		
		//Data
		$get_kt_client_kt_process_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_kt_process_list_squery_where = $get_kt_client_kt_process_list_squery_where." where kt_client_kt_process_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_kt_client_kt_process_list_squery_where = $get_kt_client_kt_process_list_squery_where." and kt_client_kt_process_added_on <= :end_date";
		}
		
		//Data
		$get_kt_client_kt_process_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_kt_client_kt_process_list_squery = $get_kt_client_kt_process_list_squery_base.$get_kt_client_kt_process_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_kt_client_kt_process_list_sstatement = $dbConnection->prepare($get_kt_client_kt_process_list_squery);
		
		$get_kt_client_kt_process_list_sstatement -> execute($get_kt_client_kt_process_list_sdata);
		
		$get_kt_client_kt_process_list_sdetails = $get_kt_client_kt_process_list_sstatement -> fetchAll();
		
		if(FALSE === $get_kt_client_kt_process_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_kt_client_kt_process_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_kt_client_kt_process_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update KT Client KT Process
INPUT 	: Process ID, KT Client KT Process Update Array
OUTPUT 	: Process ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_kt_client_kt_process($process_id,$kt_client_kt_process_update_data)
{
	if(array_key_exists("process_master_id",$kt_client_kt_process_update_data))
	{	
		$process_master_id = $kt_client_kt_process_update_data["process_master_id"];
	}
	else
	{
		$process_master_id = "";
	}
	
	if(array_key_exists("request_id",$kt_client_kt_process_update_data))
	{	
		$request_id = $kt_client_kt_process_update_data["request_id"];
	}
	else
	{
		$request_id = "";
	}
	
	if(array_key_exists("process_start_date",$kt_client_kt_process_update_data))
	{	
		$process_start_date = $kt_client_kt_process_update_data["process_start_date"];
	}
	else
	{
		$process_start_date = "";
	}
	
	if(array_key_exists("process_end_date",$kt_client_kt_process_update_data))
	{	
		$process_end_date = $kt_client_kt_process_update_data["process_end_date"];
	}
	else
	{
		$process_end_date = "";
	}
	
	if(array_key_exists("active",$kt_client_kt_process_update_data))
	{	
		$active = $kt_client_kt_process_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$kt_client_kt_process_update_data))
	{	
		$remarks = $kt_client_kt_process_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$kt_client_kt_process_update_data))
	{	
		$added_by = $kt_client_kt_process_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$kt_client_kt_process_update_data))
	{	
		$added_on = $kt_client_kt_process_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $kt_client_kt_process_update_uquery_base = "update kt_client_kt_process set";  
	
	$kt_client_kt_process_update_uquery_set = "";
	
	$kt_client_kt_process_update_uquery_where = " where kt_client_kt_process_id = :process_id";
	
	$kt_client_kt_process_update_udata = array(":process_id"=>$process_id);
	
	$filter_count = 0;
	
	if($process_master_id != "")
	{
		$kt_client_kt_process_update_uquery_set = $kt_client_kt_process_update_uquery_set." kt_client_kt_process_master_id = :process_master_id,";
		$kt_client_kt_process_update_udata[":process_master_id"] = $process_master_id;
		$filter_count++;
	}
	
	if($request_id != "")
	{
		$kt_client_kt_process_update_uquery_set = $kt_client_kt_process_update_uquery_set." kt_client_kt_process_request_id = :request_id,";
		$kt_client_kt_process_update_udata[":request_id"] = $request_id;
		$filter_count++;
	}
	
	if($process_start_date != "")
	{
		$kt_client_kt_process_update_uquery_set = $kt_client_kt_process_update_uquery_set." kt_client_kt_process_start_date = :process_start_date,";
		$kt_client_kt_process_update_udata[":process_start_date"] = $process_start_date;
		$filter_count++;
	}
	
	if($process_end_date != "")
	{
		$kt_client_kt_process_update_uquery_set = $kt_client_kt_process_update_uquery_set." kt_client_kt_process_end_date = :process_end_date,";
		$kt_client_kt_process_update_udata[":process_end_date"] = $process_end_date;
		$filter_count++;
	}
	
	if($active != "")
	{
		$kt_client_kt_process_update_uquery_set = $kt_client_kt_process_update_uquery_set." kt_client_kt_process_active = :active,";
		$kt_client_kt_process_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$kt_client_kt_process_update_uquery_set = $kt_client_kt_process_update_uquery_set." kt_client_kt_process_remarks = :remarks,";
		$kt_client_kt_process_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$kt_client_kt_process_update_uquery_set = $kt_client_kt_process_update_uquery_set." kt_client_kt_process_added_by = :added_by,";
		$kt_client_kt_process_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$kt_client_kt_process_update_uquery_set = $kt_client_kt_process_update_uquery_set." kt_client_kt_process_added_on = :added_on,";
		$kt_client_kt_process_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$kt_client_kt_process_update_uquery_set = trim($kt_client_kt_process_update_uquery_set,',');
	}
	
	$kt_client_kt_process_update_uquery = $kt_client_kt_process_update_uquery_base.$kt_client_kt_process_update_uquery_set.$kt_client_kt_process_update_uquery_where;
    
    try
    {
        $dbConnection = get_conn_handle();
        
        $kt_client_kt_process_update_ustatement = $dbConnection->prepare($kt_client_kt_process_update_uquery);		
        
        $kt_client_kt_process_update_ustatement -> execute($kt_client_kt_process_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $process_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new KT Client Document
INPUT 	: Process ID, File Path ID, Remarks, Added By
OUTPUT 	: Document ID, success or failure message
BY 		: Lakshmi
*/
function db_add_kt_client_document($process_id,$file_path_id,$remarks,$added_by)
{
	// Query
   $kt_client_document_iquery = "insert into kt_client_document
   (kt_client_document_process_id,kt_client_document_file_path,kt_client_document_active,kt_client_document_remarks,kt_client_document_added_by,kt_client_document_added_on) values(:process_id,:file_path_id,:active,:remarks,:added_by,:added_on)";  
  
    try
    {
        $dbConnection = get_conn_handle();
        $kt_client_document_istatement = $dbConnection->prepare($kt_client_document_iquery);
        
        // Data
        $kt_client_document_idata = array(':process_id'=>$process_id,':file_path_id'=>$file_path_id,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,
		':added_on'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $kt_client_document_istatement->execute($kt_client_document_idata);
		$kt_client_document_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $kt_client_document_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get KT Client Document list
INPUT 	: Document ID, Process ID, File Path ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of KT Client Document
BY 		: Lakshmi
*/
function db_get_kt_client_document($kt_client_document_search_data)
{  
	if(array_key_exists("document_id",$kt_client_document_search_data))
	{
		$document_id = $kt_client_document_search_data["document_id"];
	}
	else
	{
		$document_id = "";
	}
	
	if(array_key_exists("process_id",$kt_client_document_search_data))
	{
		$process_id = $kt_client_document_search_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("request_id",$kt_client_document_search_data))
	{
		$request_id = $kt_client_document_search_data["request_id"];
	}
	else
	{
		$request_id = "";
	}
	
	if(array_key_exists("file_path_id",$kt_client_document_search_data))
	{
		$file_path_id = $kt_client_document_search_data["file_path_id"];
	}
	else
	{
		$file_path_id = "";
	}
	
	if(array_key_exists("active",$kt_client_document_search_data))
	{
		$active = $kt_client_document_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$kt_client_document_search_data))
	{
		$added_by = $kt_client_document_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	if(array_key_exists("start_date",$kt_client_document_search_data))
	{
		$start_date = $kt_client_document_search_data["start_date"];
	}
	else
	{
		$start_date = "";
	}
	
	if(array_key_exists("end_date",$kt_client_document_search_data))
	{
		$end_date = $kt_client_document_search_data["end_date"];
	}
	else
	{
		$end_date = "";
	}

	$get_kt_client_document_list_squery_base = "select * from kt_client_document KCD inner join users U on U.user_id = KCD.kt_client_document_added_by inner join kt_client_kt_process KCKP on KCKP.kt_client_kt_process_id=KCD.kt_client_document_process_id inner join kt_client_request KCR on KCR.kt_client_request_id=
	KCKP.kt_client_kt_process_request_id inner join kt_client_process_master KCPM on KCPM.kt_client_process_master_id = KCD.kt_client_document_process_id";
	
	$get_kt_client_document_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_kt_client_document_list_sdata = array();
	
	if($document_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_document_list_squery_where = $get_kt_client_document_list_squery_where." where kt_client_document_id = :document_id";								
		}
		else
		{
			// Query
			$get_kt_client_document_list_squery_where = $get_kt_client_document_list_squery_where." and kt_client_document_id = :document_id";				
		}
		
		// Data
		$get_kt_client_document_list_sdata[':document_id'] = $document_id;
		
		$filter_count++;
	}
	
	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_document_list_squery_where = $get_kt_client_document_list_squery_where." where kt_client_document_process_id = :process_id";								
		}
		else
		{
			// Query
			$get_kt_client_document_list_squery_where = $get_kt_client_document_list_squery_where." and kt_client_document_process_id = :process_id";				
		}
		
		// Data
		$get_kt_client_document_list_sdata[':process_id'] = $process_id;
		
		$filter_count++;
	}
	
	if($request_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_document_list_squery_where = $get_kt_client_document_list_squery_where." where KCKP.kt_client_kt_process_request_id = :request_id";								
		}
		else
		{
			// Query
			$get_kt_client_document_list_squery_where = $get_kt_client_document_list_squery_where." and KCKP.kt_client_kt_process_request_id = :request_id";				
		}
		
		// Data
		$get_kt_client_document_list_sdata[':request_id'] = $request_id;
		
		$filter_count++;
	}
	
	if($file_path_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_document_list_squery_where = $get_kt_client_document_list_squery_where." where kt_client_document_file_path = :file_path_id";								
		}
		else
		{
			// Query
			$get_kt_client_document_list_squery_where = $get_kt_client_document_list_squery_where." and kt_client_document_file_path = :file_path_id";				
		}
		
		// Data
		$get_kt_client_document_list_sdata[':file_path_id'] = $file_path_id;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_document_list_squery_where = $get_kt_client_document_list_squery_where." where kt_client_document_active = :active";								
		}
		else
		{
			// Query
			$get_kt_client_document_list_squery_where = $get_kt_client_document_list_squery_where." and kt_client_document_active = :active";				
		}
		
		// Data
		$get_kt_client_document_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_document_list_squery_where = $get_kt_client_document_list_squery_where." where kt_client_document_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_kt_client_document_list_squery_where = $get_kt_client_document_list_squery_where." and kt_client_document_added_by = :added_by";
		}
		
		//Data
		$get_kt_client_document_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_document_list_squery_where = $get_kt_client_document_list_squery_where." where kt_client_document_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_kt_client_document_list_squery_where = $get_kt_client_document_list_squery_where." and kt_client_document_added_on >= :start_date";				
		}
		
		//Data
		$get_kt_client_document_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_kt_client_document_list_squery_where = $get_kt_client_document_list_squery_where." where kt_client_document_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_kt_client_document_list_squery_where = $get_kt_client_document_list_squery_where." and kt_client_document_added_on <= :end_date";
		}
		
		//Data
		$get_kt_client_document_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_kt_client_document_list_squery = " order by kt_client_document_added_on DESC";
	$get_kt_client_document_list_squery = $get_kt_client_document_list_squery_base.$get_kt_client_document_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_kt_client_document_list_sstatement = $dbConnection->prepare($get_kt_client_document_list_squery);
		
		$get_kt_client_document_list_sstatement -> execute($get_kt_client_document_list_sdata);
		
		$get_kt_client_document_list_sdetails = $get_kt_client_document_list_sstatement -> fetchAll();
		
		if(FALSE === $get_kt_client_document_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_kt_client_document_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_kt_client_document_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
 /*
PURPOSE : To update KT Client Document 
INPUT 	: Document ID, KT Client Document Update Array
OUTPUT 	: Document ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_kt_client_document($document_id,$kt_client_document_update_data)
{
	
	if(array_key_exists("process_id",$kt_client_document_update_data))
	{	
		$process_id = $kt_client_document_update_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("file_path_id",$kt_client_document_update_data))
	{	
		$file_path_id = $kt_client_document_update_data["file_path_id"];
	}
	else
	{
		$file_path_id = "";
	}
	
	if(array_key_exists("active",$kt_client_document_update_data))
	{	
		$active = $kt_client_document_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$kt_client_document_update_data))
	{	
		$remarks = $kt_client_document_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$kt_client_document_update_data))
	{	
		$added_by = $kt_client_document_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$kt_client_document_update_data))
	{	
		$added_on = $kt_client_document_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $kt_client_document_update_uquery_base = "update kt_client_document set";  
	
	$kt_client_document_update_uquery_set = "";
	
	$kt_client_document_update_uquery_where = " where kt_client_document_id = :document_id";
	
	$kt_client_document_update_udata = array(":document_id"=>$document_id);
	
	$filter_count = 0;
	
	if($process_id != "")
	{
		$kt_client_document_update_uquery_set = $kt_client_document_update_uquery_set." kt_client_document_process_id = :process_id,";
		$kt_client_document_update_udata[":process_id"] = $process_id;
		$filter_count++;
	}
	
	if($file_path_id != "")
	{
		$kt_client_document_update_uquery_set = $kt_client_document_update_uquery_set." kt_client_document_file_path = :file_path_id,";
		$kt_client_document_update_udata[":file_path_id"] = $file_path_id;
		$filter_count++;
	}
	
	if($active != "")
	{
		$kt_client_document_update_uquery_set = $kt_client_document_update_uquery_set." kt_client_document_active = :active,";
		$kt_client_document_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$kt_client_document_update_uquery_set = $kt_client_document_update_uquery_set." kt_client_document_remarks = :remarks,";
		$kt_client_document_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$kt_client_document_update_uquery_set = $kt_client_document_update_uquery_set." kt_client_document_added_by = :added_by,";
		$kt_client_document_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$kt_client_document_update_uquery_set = $kt_client_document_update_uquery_set." kt_client_document_added_on = :added_on,";
		$kt_client_document_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$kt_client_document_update_uquery_set = trim($kt_client_document_update_uquery_set,',');
	}
	
	$kt_client_document_update_uquery = $kt_client_document_update_uquery_base.$kt_client_document_update_uquery_set.$kt_client_document_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();
        
        $kt_client_document_update_ustatement = $dbConnection->prepare($kt_client_document_update_uquery);		
        
        $kt_client_document_update_ustatement -> execute($kt_client_document_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $document_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

?>
